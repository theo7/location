# SecondHand

## Filters

Lorsque qu'on touche aux filtres depuis les composants filtres on additionne tous les filtres
Quand on clique sur un lien du détail des produits (Catégories, Sous-catégories, type et marques), on reset tous les filtres en cours. Cependant, les catégories, sous-catégories et types étant intimement liés, il restent ensemble.

## Chat

### Messages

- Chat messages are sent by Firebase and all have a type.
- Each type of message has an associated component.
- The message types are defined in the structure `SystemMessageTypeEnum`.
- Example: `LOT_PREPAID` => `ChatMessageLotPrepaidComponent`