import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { Capacitor } from '@capacitor/core';
import { defineCustomElements } from '@ionic/pwa-elements/loader';
import { RecaptchaComponent } from 'ng-recaptcha';
import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

// fix the router issues in native app
// https://archive.thinktecture.com/thomas/2017/02/cordova-vs-zonejs-or-why-is-angulars-document-event-listener-not-in-a-zone.html
if (Capacitor.isNativePlatform()) {
    window.addEventListener = function () {
        // @ts-ignore
        EventTarget.prototype.addEventListener.apply(this, arguments);
    };

    window.removeEventListener = function () {
        // @ts-ignore
        EventTarget.prototype.removeEventListener.apply(this, arguments);
    };

    document.addEventListener = function () {
        // @ts-ignore
        EventTarget.prototype.addEventListener.apply(this, arguments);
    };

    document.removeEventListener = function () {
        // @ts-ignore
        EventTarget.prototype.removeEventListener.apply(this, arguments);
    };
}

if (environment.production) {
    enableProdMode();
}

platformBrowserDynamic()
    .bootstrapModule(AppModule)
    .catch(err => console.error(err));

// fix recaptcha error when a component is destroyed
// https://github.com/DethAriel/ng-recaptcha/issues/123
RecaptchaComponent.prototype.ngOnDestroy = function () {
    // @ts-ignore
    if (this.subscription) {
        // @ts-ignore
        this.subscription.unsubscribe();
    }
};

// Call the element loader after the platform has been bootstrapped
defineCustomElements(window);

// check that Mondial Relay script is up to date
if (!environment.production) {
    fetch('https://widget.mondialrelay.com/parcelshop-picker/version').then(async response => {
        const currentVersion = await response.text();
        const olderVersion = '4.0.7';

        if (currentVersion !== olderVersion) {
            console.warn(
                `%c🚨 MONDIAL RELAY widget is not up to date. Upgrade to v${currentVersion} 🚨`,
                'color: red; background: yellow; font-size: 24px;',
            );
            console.warn(
                `Please upgrade file in src/assets/scripts/parcelshop-picker-${olderVersion}.min.js and don't forget the lazy loading part!`,
            );
        }
    });
}
