import globalEnvironment from './global.environment';

export const environment = {
    ...globalEnvironment,
    production: true,
    firebase: {
        apiKey: 'AIzaSyBhnWyFu9UPwoARD9fMj8DWdQPeHSShBQU',
        authDomain: 'secondhand-a5852.firebaseapp.com',
        databaseURL: 'https://secondhand-a5852.firebaseio.com',
        kiabiDatabaseUrl: 'https://secondhand-a5852-kiabi.firebaseio.com/',
        notificationDatabaseUrl: 'https://secondhand-a5852-notification.europe-west1.firebasedatabase.app/',
        projectId: 'secondhand-a5852',
        storageBucket: 'secondhand-a5852.appspot.com',
        messagingSenderId: '455983381351',
        appId: '1:455983381351:web:6a288bcda7706b788bcdc2',
        measurementId: 'G-0QKZ1V4CXS',
        vapidKey: 'BEjOhCrsnqvKuMbSlaizMmegRze85djqOJna3DyfDIzxpiTcNztDzu68m3V0rUc8gWKBRGQtvfV8Oq99T52e6P4',
    },
    appVersion: process.env.APP_VERSION!,
    apiUrl: 'https://api.secondemain.kiabi.com/api',
    healthUrl: 'https://api.secondemain.kiabi.com/actuator/health',
    mondialRelais: {
        brand: 'MRKIAB2M',
        country: 'FR',
        zipCode: '59000',
        colisMode: '24R',
        nbResult: 10,
    },
    cguModificationDate: {
        secondHand: 1624364187,
        lemonWay: 1599466964,
    },
    requireAuthorization: false,
    contactAddress: 'contact@secondemain.kiabi.com',
    s3url: 'https://pictures.secondemain.kiabi.com/',
    feedbackFormUrl: 'https://kiabi.eu.qualtrics.com/jfe/form/SV_5w1FlZByZb67Zgp',
    googleAnalyticsUID: 'UA-12239303-34', // Production-ID
    facebookPixelID: '466369091452353', // Production-ID
    gtags: ['AW-443485052', 'AW-443483159', 'AW-310926333'], // Production-ID
    logs: {
        console: false,
        sendErrors: true,
    },
    reCaptchaKey: '6Lf4Bd8ZAAAAAB66eUf5vW_Y9EvhF1YjEouE8Vhj',
    errorNotificationLevel: 'SIMPLIFIED',
    fraudForm:
        'https://forms.office.com/Pages/ResponsePage.aspx?id=DSQvDuwR0EiNvohxzywXcCIb2h-r7zBAsjnJrRlDvrJUMEJaSFJXS0lQUzNGOUMwRzNFN1Q3REFKSi4u',
    domainExtension: '.com',
    thumbnailsOptimization: true,
    disableKiabiBag: false,
    baseUrl: 'https://secondemain.kiabi.com',
    displayEnableNotificationsBanner: true,
    allowNativeEditPicture: false,
    enableNewHomePage: true,
    professional: {
        kiabi: {
            id: 55445,
        },
    },
    otherBrandId: 51,
};

export const envModules = [];
