import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import globalEnvironment from './global.environment';

export const environment = {
    ...globalEnvironment,
    production: true,
    firebase: {
        apiKey: 'AIzaSyCNXXPm5S5jm2s8R9RP4iGo2wGgBX5ReIo',
        authDomain: 'secondhand-recette.firebaseapp.com',
        databaseURL: 'https://secondhand-recette-chat.europe-west1.firebasedatabase.app/',
        kiabiDatabaseUrl: 'https://secondhand-recette-info.europe-west1.firebasedatabase.app/',
        notificationDatabaseUrl: 'https://secondhand-recette-notification.europe-west1.firebasedatabase.app/',
        projectId: 'secondhand-recette',
        storageBucket: 'secondhand-recette.appspot.com',
        messagingSenderId: '549584416003',
        appId: '1:549584416003:web:0055634fb6ed0cad3e8d36',
        measurementId: 'G-BD57BEH03K',
        vapidKey: 'BAKhfhG12bvR_XlAzwmQ_MyrlpP4fjCayuMBAHlbaAsCdeAcvxKcHaK9xftLql44H9-gizEitE0YXv45vsPMq5Q',
    },
    appVersion: process.env.APP_VERSION + '-staging',
    apiUrl: 'https://api-staging.second-hand.aws.kiabi.pro/api',
    healthUrl: 'https://api-staging.second-hand.aws.kiabi.pro/actuator/health',
    mondialRelais: {
        brand: 'BDTEST  ',
        country: 'FR',
        zipCode: '59000',
        colisMode: '24R',
        nbResult: 10,
    },
    cguModificationDate: {
        secondHand: 1624364187,
        lemonWay: 1599466964,
    },
    requireAuthorization: true,
    contactAddress: 'contact@secondhand.kiabi.fr',
    s3url: 'https://pictures-staging.second-hand.aws.kiabi.pro/',
    feedbackFormUrl: 'https://kiabi.eu.qualtrics.com/jfe/form/SV_5w1FlZByZb67Zgp',
    googleAnalyticsUID: 'UA-61207873-20', // Préprod-ID
    facebookPixelID: null, // Prod Only
    gtags: [], // Prod only
    logs: {
        console: true,
        sendErrors: true,
    },
    reCaptchaKey: '6LcjBt8ZAAAAANTMQj6yLZ0dt-IV8yz6EAXdbsVD',
    errorNotificationLevel: 'DETAILED',
    fraudForm:
        'https://forms.office.com/Pages/ResponsePage.aspx?id=DSQvDuwR0EiNvohxzywXcCIb2h-r7zBAsjnJrRlDvrJUMEJaSFJXS0lQUzNGOUMwRzNFN1Q3REFKSi4u',
    domainExtension: '.pro',
    thumbnailsOptimization: true,
    disableKiabiBag: false,
    baseUrl: 'https://staging.second-hand.aws.kiabi.pro',
    displayEnableNotificationsBanner: true,
    allowNativeEditPicture: false,
    enableNewHomePage: true,
    professional: {
        kiabi: {
            id: 261,
        },
    },
    otherBrandId: 51,
};

export const envModules = [
    StoreDevtoolsModule.instrument({
        maxAge: 25,
    }),
];
