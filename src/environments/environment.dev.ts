import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import globalEnvironment from './global.environment';

export const environment = {
    ...globalEnvironment,
    production: true,
    firebase: {
        apiKey: 'AIzaSyBdpixfuUPtPn85Xn09j3AfbTpKj9jT1WY',
        authDomain: 'secondhand-dev.firebaseapp.com',
        databaseURL: 'https://secondhand-dev-chat.europe-west1.firebasedatabase.app',
        kiabiDatabaseUrl: 'https://secondhand-dev-info.europe-west1.firebasedatabase.app/',
        notificationDatabaseUrl: 'https://secondhand-dev-notification-1befb.europe-west1.firebasedatabase.app/',
        projectId: 'secondhand-dev',
        storageBucket: 'secondhand-dev.appspot.com',
        messagingSenderId: '614616481130',
        appId: '1:614616481130:web:446727f31d55e8b836f913',
        measurementId: 'G-R7QLMK68KY',
        vapidKey: 'BKYmlulgvP4W2cAdnSuLe3XoP0qedK3OePptF-9AXQtMbHWrr0NIjYKOwwdT-D0qMm6f62WKcBKu3WxqcTnt77I',
    },
    appVersion: process.env.APP_VERSION + '-dev',
    apiUrl: 'https://api-dev.second-hand.aws.kiabi.pro/api',
    healthUrl: 'https://api-dev.second-hand.aws.kiabi.pro/actuator/health',
    mondialRelais: {
        brand: 'BDTEST  ',
        country: 'FR',
        zipCode: '59000',
        colisMode: '24R',
        nbResult: 10,
    },
    cguModificationDate: {
        secondHand: 1624364187,
        lemonWay: 1599466964,
    },
    requireAuthorization: true,
    contactAddress: 'contact@secondhand.kiabi.fr',
    s3url: 'https://pictures-dev.second-hand.aws.kiabi.pro/',
    feedbackFormUrl: 'https://kiabi.eu.qualtrics.com/jfe/form/SV_5w1FlZByZb67Zgp',
    googleAnalyticsUID: 'UA-61207873-20', // Préprod-ID
    facebookPixelID: null, // Prod Only
    gtags: [], // Prod only
    logs: {
        console: true,
        sendErrors: true,
    },
    reCaptchaKey: '6LcjBt8ZAAAAANTMQj6yLZ0dt-IV8yz6EAXdbsVD',
    errorNotificationLevel: 'DETAILED',
    fraudForm:
        'https://forms.office.com/Pages/ResponsePage.aspx?id=DSQvDuwR0EiNvohxzywXcCIb2h-r7zBAsjnJrRlDvrJUMEJaSFJXS0lQUzNGOUMwRzNFN1Q3REFKSi4u',
    domainExtension: '.pro',
    thumbnailsOptimization: true,
    disableKiabiBag: false,
    baseUrl: 'https://dev.second-hand.aws.kiabi.pro',
    displayEnableNotificationsBanner: true,
    allowNativeEditPicture: false,
    enableNewHomePage: true,
    professional: {
        kiabi: {
            id: 174,
        },
    },
    otherBrandId: 51,
};

export const envModules = [
    StoreDevtoolsModule.instrument({
        maxAge: 25,
    }),
];
