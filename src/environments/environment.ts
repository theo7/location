// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// `ng build --configuration staging` replaces `environment.ts` with `environment.staging.ts`.
// `ng build --configuration dev` replaces `environment.ts` with `environment.dev.ts`.

// The list of file replacements can be found in `angular.json`.
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import globalEnvironment from './global.environment';

const domain = process.env.SECOND_HAND_LOCAL_DOMAIN || window.location.hostname;

export const environment = {
    ...globalEnvironment,
    production: false,
    domain,
    firebase: {
        apiKey: 'AIzaSyDpuI-h4J7iWgq27NBk_V3xqDugT172ydc',
        authDomain: 'secondhand-local-82502.firebaseapp.com',
        databaseURL:
            process.env.APP_FIREBASE_DATABASE_URL || 'https://secondhand-local-chat.europe-west1.firebasedatabase.app/',
        kiabiDatabaseUrl: 'https://secondhand-local-info.europe-west1.firebasedatabase.app/',
        notificationDatabaseUrl: 'https://secondhand-local-notification.europe-west1.firebasedatabase.app/',
        projectId: 'secondhand-local-82502',
        storageBucket: 'secondhand-local-82502.appspot.com',
        messagingSenderId: '332433188351',
        appId: '1:332433188351:web:0dd11752bcfb9892fdf3ca',
        measurementId: 'G-QZHSHV9L43',
        vapidKey: 'BAQ9ix-GDZyap6qKmQHO-aubDE4HbB1_2ke5uJbMhJOCoVhTmbyXhk4UrC4IzN4ywTMJLdH50Muknm0CwhDMn_Y',
    },
    appVersion: require('../../package.json').version + '-local',
    // apiUrl: `http://10.0.2.2:8080/api`,
    apiUrl: `http://${domain}:8080/api`,
    // healthUrl: `http://10.0.2.2:8080/actuator/health`,
    healthUrl: `http://${domain}:8080/actuator/health`,
    mondialRelais: {
        brand: 'BDTEST  ',
        country: 'FR',
        zipCode: '59000',
        colisMode: '24R',
        nbResult: 10,
    },
    cguModificationDate: {
        secondHand: 1624364187,
        lemonWay: 1599466964,
    },
    requireAuthorization: true,
    contactAddress: 'contact@location.kiabi.fr',
    s3url: 'https://pictures-dev.second-hand.aws.kiabi.pro/',
    feedbackFormUrl: 'https://kiabi.eu.qualtrics.com/jfe/form/SV_5w1FlZByZb67Zgp',
    googleAnalyticsUID: 'UA-61207873-20', // Préprod-ID
    facebookPixelID: null, // Prod Only
    gtags: [], // Prod only
    logs: {
        console: true,
        sendErrors: false,
    },
    reCaptchaKey: '6LcjBt8ZAAAAANTMQj6yLZ0dt-IV8yz6EAXdbsVD',
    errorNotificationLevel: 'DETAILED',
    fraudForm:
        'https://forms.office.com/Pages/ResponsePage.aspx?id=DSQvDuwR0EiNvohxzywXcCIb2h-r7zBAsjnJrRlDvrJUMEJaSFJXS0lQUzNGOUMwRzNFN1Q3REFKSi4u',
    domainExtension: '',
    thumbnailsOptimization: true,
    disableKiabiBag: false,
    baseUrl: 'http://localhost:4200',
    displayEnableNotificationsBanner: true,
    allowNativeEditPicture: false,
    enableNewHomePage: true,
    professional: {
        kiabi: {
            id: 5,
        },
    },
    otherBrandId: 51,
};

export const envModules = [
    StoreDevtoolsModule.instrument({
        maxAge: 25,
    }),
];

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
