const domain = typeof window !== 'undefined' ? window.location.hostname : '';

const environment = {
    showBanner: true,
    domain,
    picture: {
        acceptedFormats: ['image/jpeg', 'jpeg', 'image/png', 'png'],
        maxSize: 2000000,
    },
    bankDetails: {
        acceptedTypes: ['image/jpeg', 'jpeg', 'image/png', 'png', 'application/pdf', 'pdf'],
        maxSize: 3000000,
    },
    unauthorizedNames: ['kiabi', 'klabl', 'kiabl', 'klabi', 'admin', 'admln', 'adm1n'],
    homeDisplayedArticles: {
        desktop: 40,
        handset: 20,
    },
    maxChatImages: 4,
    kiabiBagUrl: 'https://bag.kiabi.com',
    voucherAdditionRate: 0.2,
    nbHourToConfirmSell: 48,
    howItWorksYoutubeId: 'O-nA2b8kuhU',
    homeCarouselIntervalTime: 6000,
    ipService: 'https://api64.ipify.org/?format=json',
    ipSalt: 'wt5U8U6c8QT76ywN',
    requireCookieConsent: !isNative,
    removeCookiesOnDeny: false,
    displayProductViews: true,
    share: {
        enableFileShare: false,
    },
    storeCredit: {
        minimumVoucherAmount: 1,
    },
    minimumDressingProducts: 20,
    useEnhancedEcommercePlugin: true,
};

export default environment;
