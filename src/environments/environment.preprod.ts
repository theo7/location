import globalEnvironment from './global.environment';

export const environment = {
    ...globalEnvironment,
    production: true,
    firebase: {
        apiKey: 'AIzaSyBQ7HIoR27gtIf8-FHIWGSXNJIUYTs6zDA',
        authDomain: 'secondhand-preprod.firebaseapp.com',
        databaseURL: 'https://secondhand-preprod-chat.europe-west1.firebasedatabase.app/',
        kiabiDatabaseUrl: 'https://secondhand-preprod-info.europe-west1.firebasedatabase.app/',
        notificationDatabaseUrl: 'https://secondhand-preprod-notification.europe-west1.firebasedatabase.app/',
        projectId: 'secondhand-preprod',
        storageBucket: 'secondhand-preprod.appspot.com',
        messagingSenderId: '639677748646',
        appId: '1:639677748646:web:094f551b7d10b290d22b67',
        measurementId: 'G-CX4VRK890Z',
        vapidKey: 'BPL2xyxEdMU5G9xL4v2HuT6MYnDoFGhB-xEX_jhBKrV10Ycyu99Fde678ppqSveHF5K3BpUzOaSr7SEps-GF34E',
    },
    appVersion: process.env.APP_VERSION + '-preprod',
    apiUrl: 'https://api-preprod.secondemain.kiabi.com/api',
    healthUrl: 'https://api-preprod.secondemain.kiabi.com/actuator/health',
    mondialRelais: {
        brand: 'BDTEST  ',
        country: 'FR',
        zipCode: '59000',
        colisMode: '24R',
        nbResult: 10,
    },
    cguModificationDate: {
        secondHand: 1624364187,
        lemonWay: 1599466964,
    },
    requireAuthorization: true,
    contactAddress: 'contact@secondhand.kiabi.fr',
    s3url: 'https://pictures-preprod.secondemain.kiabi.com/',
    feedbackFormUrl: 'https://kiabi.eu.qualtrics.com/jfe/form/SV_5w1FlZByZb67Zgp',
    googleAnalyticsUID: 'UA-61207873-20', // Préprod-ID
    facebookPixelID: null, // Prod Only
    gtags: [], // Prod only
    logs: {
        console: true,
        sendErrors: true,
    },
    reCaptchaKey: '6Lf4Bd8ZAAAAAB66eUf5vW_Y9EvhF1YjEouE8Vhj',
    errorNotificationLevel: 'DETAILED',
    fraudForm:
        'https://forms.office.com/Pages/ResponsePage.aspx?id=DSQvDuwR0EiNvohxzywXcCIb2h-r7zBAsjnJrRlDvrJUMEJaSFJXS0lQUzNGOUMwRzNFN1Q3REFKSi4u',
    domainExtension: '.com',
    thumbnailsOptimization: true,
    disableKiabiBag: false,
    baseUrl: 'https://preprod.secondemain.kiabi.com',
    displayEnableNotificationsBanner: true,
    allowNativeEditPicture: false,
    enableNewHomePage: true,
    professional: {
        kiabi: {
            id: 1747,
        },
    },
    otherBrandId: 51,
};

export const envModules = [];
