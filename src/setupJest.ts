// This file is used to setup Jest configuration among all test files
import * as dayjs from 'dayjs';
import 'dayjs/locale/fr';
import * as customParseFormat from 'dayjs/plugin/customParseFormat';
import * as relativeTime from 'dayjs/plugin/relativeTime';

dayjs.extend(customParseFormat);
dayjs.extend(relativeTime);

class LocalStorageMock {
    store: any = {};

    get length() {
        return Object.keys(this.store).length;
    }

    clear() {
        this.store = {};
    }

    getItem(key: string) {
        return this.store[key] || null;
    }

    setItem(key: string, value: any) {
        this.store[key] = String(value);
    }

    removeItem(key: string) {
        delete this.store[key];
    }

    key(index: number) {
        const keys = Object.keys(this.store);
        return keys[index];
    }
}

global.localStorage = new LocalStorageMock();
