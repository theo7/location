// 💡 used to unregister service worker in naked domain (preprod/production env)
self.addEventListener('install', () => self.skipWaiting());

// 💡 used to unregister service worker in naked domain (preprod/production env)
self.addEventListener('activate', () => {
    function applyRedirectToWww(nakedDomain, wwwDomain) {
        const isNakedDomain = self.registration.scope === nakedDomain;
        if (isNakedDomain) {
            self.clients.claim().then(function() {
                self.registration.unregister().then(function() {
                    self.clients.matchAll({ type: 'window', includeUncontrolled: true }).then(function(clientList) {
                        for (const client of clientList) {
                            // rewrite url from naked domain to `www` domain
                            const clientUri = new URL(client.url);
                            const redirectUri = new URL(clientUri.pathname, wwwDomain);
            
                            client.navigate(redirectUri.href);
                        }
                    });
                });
            });
        }
    }

    applyRedirectToWww("https://secondemain.kiabi.com/", "https://www.secondemain.kiabi.com/");
    applyRedirectToWww("https://preprod.secondemain.kiabi.com/", "https://www-preprod.secondemain.kiabi.com/");
});

var firebaseVersion = '8.3.0';

// Import and configure the Firebase SDK
// These scripts are made available when the app is served or deployed on Firebase Hosting
// If you do not serve/host your project using Firebase Hosting see https://firebase.google.com/docs/web/setup
// Version should match the one in the package.json apparently... Background push may not work if they don't
importScripts('https://www.gstatic.com/firebasejs/' + firebaseVersion + '/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/' + firebaseVersion + '/firebase-messaging.js');

importScripts('sw-firebase-init.js');

if (firebase.messaging.isSupported()) {
    function getWindowClient() {
        return clients.matchAll({
            type: 'window',
        }).then((clientList) => {
            var url = new URL('/', location).href;

            for (var index = 0; index < clientList.length; index++) {
                var client = clientList[index];

                if (client.url.indexOf(url) === 0) {
                    return client;
                }
            }

            return undefined;
        });
    }

    class CustomPushEvent extends Event {
        constructor(data) {
            super('push');
        
            Object.assign(this, data);
            this.custom = true;
        }
    }
      
    /*
     * overrides push notification data, to avoid having 'notification' key and firebase blocking
     * the message handler from being called
     */
    self.addEventListener('push', (e) => {
        // stop event propagation
        e.stopImmediatePropagation();

        if (e.custom) {
            // skip if event is our own custom event
            // display notification if wanted 

            var promiseChain = getWindowClient()
                .then(function (client) {
                    if (client) {
                        // display in foreground
                        const { title, ...restPayload } = e.data.sh.notification;

                        const { click_action } = restPayload;

                        const onSamePage = 
                            client.visibilityState === "visible" 
                            && client.url === click_action;

                        // do not display notification if already on the page (example: chat message)
                        if (onSamePage) {
                            return;
                        }

                        const notificationOptions = {
                            ...restPayload,
                        };

                        return self.registration.showNotification(title, notificationOptions);
                    } else {
                        // display in background
                        const { title, ...restPayload } = e.data.sh.notification;

                        const notificationOptions = {
                            ...restPayload,
                        };

                        return self.registration.showNotification(title, notificationOptions);
                    }
                });
            
            // check if the current window is already open and focuses if it is
            e.waitUntil(promiseChain);

            return;
        }
        
        // keep old event data to override
        const oldData = e.data;
        
        // create a new event to dispatch, pull values from notification key and put it in data key,
        // and then remove notification key
        const newEvent = new CustomPushEvent({
            data: {
                sh: oldData.json(),
                json() {
                    const newData = oldData.json();
                    newData.data = {
                        ...newData.data,
                        ...newData.notification,
                    };
                    delete newData.notification;
                    return newData;
                },
            },
            waitUntil: e.waitUntil.bind(e),
        });
    
        // dispatch the new wrapped event
        dispatchEvent(newEvent);
    });

    self.addEventListener('notificationclick', function (event) {
        event.notification.close();

        var action1 = event.notification.click_action;
        var action2 = event.notification.data && 
            event.notification.data.click_action;
        var action3 = event.notification.data && 
            event.notification.data.FCM_MSG &&
            event.notification.data.FCM_MSG.notification && 
            event.notification.data.FCM_MSG.notification.click_action;
        var clickAction = action1 || action2 || action3;

        var absoluteUri = clickAction;

        // get relative url from uri
        var uri = absoluteUri
            ? new URL("", absoluteUri).pathname
            : undefined;

        var promiseChain = getWindowClient()
            .then(function (client) {
                if (client) {
                    return client.focus().then(function (c) {
                        // don't want to use navigateTo here because it reloads the PWA and that's poor UX
                        if (uri) {
                            c.postMessage({
                                type: 'redirect',
                                uri: uri,
                            });
                        }
                    });
                }

                return clients.openWindow(absoluteUri);
            });
            
        // check if the current window is already open and focuses if it is
        event.waitUntil(promiseChain);
    });

    // should be after addEventListener notificationclick because bug on Firefox
    // source: https://stackoverflow.com/a/54100379/2719044
    var messaging = firebase.messaging();
}
