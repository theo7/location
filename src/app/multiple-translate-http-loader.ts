import { HttpClient } from '@angular/common/http';
import { TranslateLoader } from '@ngx-translate/core';
import * as merge from 'deepmerge';
import { forkJoin, Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

interface TranslationResource {
    prefix: string;
    suffix: string;
}

export class MultipleTranslateHttpLoader implements TranslateLoader {
    constructor(private http: HttpClient, private resources: TranslationResource[]) {}

    getTranslation(lang: string): Observable<any> {
        const requests = this.resources.map(resource => {
            const path = resource.prefix + lang + resource.suffix;
            return this.http.get(path).pipe(
                catchError(() => {
                    console.error(`Could not find translation file: ${path}`);
                    return of({});
                }),
            );
        });

        return forkJoin(requests).pipe(map(response => merge.all(response)));
    }
}
