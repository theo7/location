import { NgModule } from '@angular/core';
import { AngularFireAuthGuard, redirectUnauthorizedTo } from '@angular/fire/auth-guard';
import { NoPreloading, PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { environment } from 'src/environments/environment';
import { OfflineGuard } from './features/offline/offline.guard';
import { NotBlockedGuard } from './shared/guards/not-blocked.guard';
import { SignedInGuard } from './shared/guards/signed-in.guard';

const redirectUnauthorizedToLogin = () => redirectUnauthorizedTo(['login']);

const routes: Routes = [
    {
        path: '',
        canActivate: [OfflineGuard],
        canActivateChild: [OfflineGuard],
        children: [
            {
                path: 'login',
                loadChildren: () => import('./pages/login/login.module').then(m => m.LoginModule),
            },
            {
                path: 'sign-up',
                loadChildren: () => import('./pages/sign-up/sign-up.module').then(m => m.SignUpModule),
            },
            {
                path: 'reset-password',
                loadChildren: () =>
                    import('./pages/reset-password/reset-password.module').then(m => m.ResetPasswordModule),
            },
            {
                path: '',
                loadChildren: () => import('./pages/home/home.module').then(m => m.HomeModule),
            },
            {
                path: 'add',
                loadChildren: () => import('./pages/add-product/add-product.module').then(m => m.AddProductModule),
                canActivate: [SignedInGuard, AngularFireAuthGuard, NotBlockedGuard],
                data: { authGuardPipe: redirectUnauthorizedToLogin },
            },
            {
                path: 'product',
                loadChildren: () => import('./pages/product/product.module').then(m => m.ProductModule),
            },
            {
                path: 'chat',
                loadChildren: () => import('./pages/chat/chat.module').then(m => m.ChatModule),
                canActivate: [SignedInGuard],
                canActivateChild: [SignedInGuard],
            },
            {
                path: 'search',
                loadChildren: () => import('./pages/search/search.module').then(m => m.SearchModule),
            },
            {
                path: 'marque',
                loadChildren: () => import('./pages/search-brand/search-brand.module').then(m => m.SearchBrandModule),
            },
            {
                path: 'validate-email/:token',
                loadChildren: () =>
                    import('./pages/validate-email/validate-email.module').then(m => m.ValidateEmailModule),
            },
            {
                path: 'profile',
                loadChildren: () => import('./pages/profil/profil.module').then(m => m.ProfilModule),
            },
            {
                path: 'payment-callback',
                loadChildren: () =>
                    import('./pages/payment-callback/payment-callback.module').then(m => m.PaymentCallbackModule),
            },
            {
                path: 'other',
                loadChildren: () => import('./pages/other/other.module').then(m => m.OtherModule),
            },
            {
                path: 'suggestions',
                loadChildren: () => import('./pages/suggestion/suggestion.module').then(m => m.SuggestionModule),
            },
            {
                path: 'how-it-works',
                loadChildren: () => import('./pages/how-it-works/how-it-works.module').then(m => m.HowItWorksModule),
            },
            {
                path: 'guide',
                loadChildren: () => import('./pages/guide/guide.module').then(m => m.GuideModule),
            },
            {
                path: 'maintenance',
                loadChildren: () => import('./pages/maintenance/maintenance.module').then(m => m.MaintenanceModule),
            },
        ].map(route => ({ ...route, canLoad: [OfflineGuard] })),
    },
    {
        path: '**',
        loadChildren: () => import('./pages/page-not-found/page-not-found.module').then(m => m.PageNotFoundModule),
    },
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, {
            anchorScrolling: 'enabled',
            scrollPositionRestoration: 'top',
            preloadingStrategy: environment.production ? PreloadAllModules : NoPreloading,
        }),
    ],
    exports: [RouterModule],
})
export class AppRoutingModule {}
