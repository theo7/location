import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root',
})
export class WebNotificationService {
    isSupported() {
        return typeof window !== 'undefined' && 'Notification' in window;
    }

    isPermissionGranted() {
        return Notification.permission === 'granted';
    }

    isEnabled() {
        return this.isSupported() && this.isPermissionGranted();
    }

    openNotification$(payload: any, uri?: string) {
        return new Observable<string>(observer => {
            const notification = payload.notification;
            const title = environment.production ? notification.title : `${notification.title} -- FOREGROUND`;

            const notify = new Notification(title, { ...notification });

            const onclick = () => {
                if (uri) {
                    observer.next(uri);
                }
                observer.complete();
            };
            const onclose = () => {
                observer.complete();
            };
            const onerror = (error: any) => {
                observer.error(error);
            };

            notify.addEventListener('click', onclick);
            notify.addEventListener('close', onclose);
            notify.addEventListener('error', onerror);

            return () => {
                notify.removeEventListener('click', onclick);
                notify.removeEventListener('close', onclose);
                notify.removeEventListener('error', onerror);
            };
        });
    }
}
