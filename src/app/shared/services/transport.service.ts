import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { TransportFee, TransportMode } from '../models/models';

@Injectable({
    providedIn: 'root',
})
export class TransportService {
    private readonly baseUrl = `${environment.apiUrl}/transports`;

    constructor(private http: HttpClient) {}

    getTransportModes(): Observable<TransportMode[]> {
        return this.http.get<TransportMode[]>(`${this.baseUrl}/transport-modes`);
    }

    getShippingFees(): Observable<TransportFee> {
        return this.http.get<TransportFee>(`${this.baseUrl}/fee`);
    }
}
