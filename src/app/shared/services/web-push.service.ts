import { Injectable } from '@angular/core';
import { Capacitor } from '@capacitor/core';
import { StorageMap } from '@ngx-pwa/local-storage';
import { default as firebase } from 'firebase/app';
import { EMPTY, from, of } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { LOCAL_STORAGE_NOTIFICATIONS_SETTINGS } from '../models/models';
import { PushService } from './push.service';
import { ServiceWorkerService } from './service-worker.service';
import { WebNotificationService } from './web-notification.service';

@Injectable({
    providedIn: 'root',
})
export class WebPushService extends PushService {
    private alreadyUseServiceWorker = false;

    constructor(
        private readonly webNotificationService: WebNotificationService,
        private readonly storage: StorageMap,
        private readonly serviceWorkerService: ServiceWorkerService,
    ) {
        super();
    }

    isSupported() {
        const isBraveBrowser = (navigator as any).brave;

        return (
            !Capacitor.isNativePlatform() &&
            this.webNotificationService.isSupported() &&
            firebase.messaging.isSupported() &&
            !isBraveBrowser
        );
    }

    isEnabled() {
        return this.isSupported() && this.webNotificationService.isEnabled();
    }

    isActivated() {
        if (!this.isEnabled()) {
            return false;
        }

        const pushActivated = this.storage.get(LOCAL_STORAGE_NOTIFICATIONS_SETTINGS, { type: 'boolean' });
        return !!pushActivated;
    }

    useServiceWorker() {
        if (!this.isSupported()) {
            return EMPTY;
        }

        if (this.alreadyUseServiceWorker) {
            return this.serviceWorkerService.getCurrent();
        }

        this.alreadyUseServiceWorker = true;

        return this.serviceWorkerService.getCurrent().pipe(
            switchMap(swr => {
                if (swr) {
                    return of(firebase.messaging().useServiceWorker(swr)).pipe(map(() => swr));
                }
                return EMPTY;
            }),
        );
    }

    getNotificationToken() {
        if (!this.isSupported()) {
            return EMPTY;
        }

        return this.useServiceWorker().pipe(
            switchMap(serviceWorkerRegistration => {
                return from(
                    firebase.messaging().getToken({
                        vapidKey: environment.firebase.vapidKey,
                        serviceWorkerRegistration,
                    }),
                );
            }),
        );
    }

    deleteToken(token: string) {
        if (!this.isSupported()) {
            return EMPTY;
        }

        return this.useServiceWorker().pipe(
            switchMap(() => {
                return from(firebase.messaging().deleteToken(token));
            }),
        );
    }
}
