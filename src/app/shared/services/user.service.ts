import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { share } from 'rxjs/operators';
import { GlobalSettings } from 'src/app/store/actions/settings.actions';
import { environment } from '../../../environments/environment';
import {
    AccountStatus,
    Address,
    CreateSearch,
    Document,
    DocumentType,
    Iban,
    ModerationStatus,
    Page,
    Search,
    UploadDocumentResponse,
    UserDocuments,
    UserFollowingProfile,
    UserFollowingSummary,
    UserProfile,
    UserRating,
    UserRatingSummary,
} from '../models/models';
import { ImageService } from './image.service';

@Injectable({
    providedIn: 'root',
})
export class UserService {
    private readonly userUrl = `${environment.apiUrl}/users`;

    constructor(private readonly http: HttpClient, private readonly imageService: ImageService) {}

    getCurrentUser(): Observable<UserProfile> {
        return this.http.get<UserProfile>(`${this.userUrl}/current`).pipe(share());
    }

    getProfile(id: number | string): Observable<UserProfile> {
        let headers = new HttpHeaders();
        headers = headers.append('without_auth', 'true');
        return this.http.get<UserProfile>(`${this.userUrl}/${id}`, { headers });
    }

    deleteOwnProfile() {
        return this.http.delete<void>(`${this.userUrl}`);
    }

    updateProfil(user: UserProfile): Observable<UserProfile> {
        return this.http.put<UserProfile>(`${this.userUrl}`, user);
    }

    checkNickname(nickname: string): Observable<boolean> {
        return this.http.get<boolean>(`${this.userUrl}/check-nickname?nickname=${encodeURIComponent(nickname)}`);
    }

    checkEmail(email: string): Observable<boolean> {
        return this.http.get<boolean>(`${this.userUrl}/check-email?email=${encodeURIComponent(email)}`);
    }

    postUserAddress(address: Address): Observable<Address> {
        return this.http.post<Address>(`${this.userUrl}/addresses`, address);
    }

    deleteUserAddress(addressId: number): Observable<UserProfile> {
        return this.http.delete<UserProfile>(`${this.userUrl}/addresses/${addressId}`);
    }

    editUserAddress(address: Address): Observable<Address> {
        return this.http.put<Address>(`${this.userUrl}/addresses/`, address);
    }

    loadBankDetails(): Observable<UserDocuments> {
        return this.http.get<UserDocuments>(`${this.userUrl}/documents`);
    }

    getIdentityDocument(): Observable<Document> {
        return this.http.get<Document>(`${this.userUrl}/documents/identity`);
    }

    getRibDocument(): Observable<Document> {
        return this.http.get<Document>(`${this.userUrl}/documents/rib`);
    }

    updateIban(holder: string, iban: string) {
        const formData = new FormData();
        formData.append('holder', holder);
        formData.append('iban', iban);

        return this.http.post<Iban>(`${this.userUrl}/ibans`, formData);
    }

    sendRibDocument(file: File) {
        const formData = new FormData();
        formData.append('documentType', DocumentType.RIB);
        formData.append('multipartFile', file);

        return this.http.post<UploadDocumentResponse>(`${this.userUrl}/documents`, formData);
    }

    sendIdentityDocument(file: File) {
        const formData = new FormData();
        formData.append('documentType', DocumentType.ID_CARD);
        formData.append('multipartFile', file);

        return this.http.post<UploadDocumentResponse>(`${this.userUrl}/documents`, formData);
    }

    getIban(): Observable<Iban> {
        return this.http.get<Iban>(`${this.userUrl}/ibans`);
    }

    appAuthorization(password): Observable<boolean> {
        return this.http.post<any>(`${this.userUrl}/app-authorization`, { password });
    }

    sendConfirmation(email: string): Observable<void> {
        return this.http.post<void>(`${this.userUrl}/send-confirmation`, { mail: email });
    }

    validateEmail(token: string): Observable<void> {
        return this.http.post<void>(`${this.userUrl}/validate-email`, { token });
    }

    updateCguModificationDate(type: 'secondHand' | 'lemonWay'): Observable<UserProfile> {
        return this.http.post<UserProfile>(`${this.userUrl}/cgu/${type}`, {});
    }

    getAvatarUrl(user: UserProfile | UserFollowingProfile, isCurrentUser = false): string {
        const avatarFileName = user.picture?.fileName;
        if (!!user?.picture?.static) {
            return this.imageService.getStaticUrl(avatarFileName);
        }
        if (avatarFileName && (isCurrentUser || user.picture?.status === ModerationStatus.OK)) {
            return this.imageService.getUrl(avatarFileName);
        } else {
            return '';
        }
    }

    createRating(userId: number, lotId: number, rating: UserRating): Observable<UserRating> {
        return this.http.post<UserRating>(`${this.userUrl}/${userId}/rating/${lotId}`, rating);
    }

    editRating(userId: number, rateId: number, rating: UserRating): Observable<UserRating> {
        return this.http.put<UserRating>(`${this.userUrl}/${userId}/rating/${rateId}`, rating);
    }

    answerRating(userId: number, rateId: number, answer: string): Observable<UserRating> {
        return this.http.post<UserRating>(`${this.userUrl}/${userId}/rating/${rateId}/answer`, answer);
    }

    findRatingSummary(userId: number | string): Observable<UserRatingSummary> {
        let headers = new HttpHeaders();
        headers = headers.append('without_auth', 'true');
        return this.http.get<UserRatingSummary>(`${this.userUrl}/${userId}/rating/summary`, { headers });
    }

    findRating(userId: number | string, page: number, size = 15): Observable<Page<UserRating>> {
        return this.http.get<Page<UserRating>>(
            `${this.userUrl}/${userId}/rating?page=${page}&size=${size}&sort=creationDate,desc`,
        );
    }

    haveAlreadyRatedUser(userId: number, lotId: number): Observable<boolean> {
        return this.http.get<boolean>(`${this.userUrl}/${userId}/rating/${lotId}/exist`);
    }

    blockUser(userId: number): Observable<void> {
        return this.http.post<void>(`${this.userUrl}/${userId}/block`, null);
    }

    unblockUser(userId: number): Observable<void> {
        return this.http.post<void>(`${this.userUrl}/${userId}/unblock`, null);
    }

    hasBlockedMe(userId: number): Observable<boolean> {
        return this.http.get<boolean>(`${this.userUrl}/${userId}/hasblockedme`);
    }

    switchVacationMode(): Observable<void> {
        return this.http.put<void>(`${this.userUrl}/vacationMode`, null);
    }

    getPaymentAccountStatus(): Observable<AccountStatus> {
        return this.http.get<AccountStatus>(`${this.userUrl}/payment-account/status`);
    }

    getSettings() {
        return this.http.get<GlobalSettings>(`${this.userUrl}/settings`);
    }

    updateSettings(settings: GlobalSettings) {
        return this.http.post<void>(`${this.userUrl}/settings`, settings);
    }

    getSearchHistory(): Observable<Search[]> {
        return this.http.get<Search[]>(`${this.userUrl}/search`);
    }

    addSearchHistory(search: CreateSearch): Observable<Search> {
        return this.http.post<Search>(`${this.userUrl}/search`, search);
    }

    updateSearch(id: number, search: Partial<Search>): Observable<Search> {
        return this.http.patch<Search>(`${this.userUrl}/search/${id}`, search);
    }

    getFollowingSummary(userId: number) {
        let headers = new HttpHeaders();
        headers = headers.append('without_auth', 'true');
        return this.http.get<UserFollowingSummary>(`${this.userUrl}/${userId}/following/summary`, { headers });
    }
}
