import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { Discussion, Lot, LotStatusEnum, Product, RemunerationMode } from '../models/models';

@Injectable({
    providedIn: 'root',
})
export class LotService {
    private readonly lotUrl = `${environment.apiUrl}/lots`;

    constructor(private readonly http: HttpClient) {}

    addProductToLot(lotId: number, productId: number): Observable<Lot> {
        return this.http.post<Lot>(`${this.lotUrl}/${lotId}/add/${productId}`, null);
    }

    removeProductFromLot(lotId: number, productId: number): Observable<Lot> {
        return this.http.post<Lot>(`${this.lotUrl}/${lotId}/remove/${productId}`, null);
    }

    setProductList(lotId: number, products: Product[]): Observable<Lot> {
        return this.http.post<Lot>(`${this.lotUrl}/${lotId}/products`, products);
    }

    getCart(lotId: number): Observable<Lot> {
        return this.http.get<Lot>(`${this.lotUrl}/${lotId}/cart`);
    }

    buy(lotId: number, idRelay: string, isNative: boolean): Observable<Lot> {
        const params = new HttpParams().set('fromNative', String(isNative));
        return this.http.post<Lot>(`${this.lotUrl}/${lotId}/buy`, idRelay, { params });
    }

    getLotStatus(lotId: number): Observable<LotStatusEnum> {
        return this.http.get<LotStatusEnum>(`${this.lotUrl}/${lotId}/status`);
    }

    approveSell(lotId: number, remunerationMode: RemunerationMode): Observable<Lot> {
        return this.http.post<Lot>(`${this.lotUrl}/${lotId}/approve-sell`, { remunerationMode });
    }

    approveProfessionalSell(lotId: number, trackingNumber: number): Observable<Lot> {
        return this.http.post<Lot>(`${this.lotUrl}/${lotId}/approve-professional-sell`, { trackingNumber });
    }

    declineProfessionalSell(lotId: number): Observable<Lot> {
        return this.http.post<Lot>(`${this.lotUrl}/${lotId}/decline-professional-sell`, null);
    }

    /**
     * Buyer cancel the sell (before seller validation)
     */
    cancelSell(lotId: number): Observable<Lot> {
        return this.http.post<Lot>(`${this.lotUrl}/${lotId}/cancel-sell`, null);
    }

    /**
     * Seller decline sell (after prepaid from buyer)
     */
    declineSell(lotId: number): Observable<Lot> {
        return this.http.post<Lot>(`${this.lotUrl}/${lotId}/decline-sell`, null);
    }

    /**
     * Seller cancel the sell just before package sent
     * (after both buyer & seller validation and just before package sent)
     */
    cancelPackage(lotId: number) {
        return this.http.post<Lot>(`${this.lotUrl}/${lotId}/cancel-package`, null);
    }

    confirmPackageCompliant(lotId: number): Observable<Lot> {
        return this.http.post<Lot>(`${this.lotUrl}/${lotId}/compliant`, null);
    }

    confirmPackageNonCompliant(lotId: number): Observable<Lot> {
        return this.http.post<Lot>(`${this.lotUrl}/${lotId}/notcompliant`, null);
    }

    buyCallback(lotId: number, token: string | undefined): Observable<Discussion> {
        return this.http.get<Discussion>(`${this.lotUrl}/${lotId}/buy/callback?response_wkToken=${token}`);
    }

    getUserPurchases(): Observable<Lot[]> {
        return this.http.get<Lot[]>(`${this.lotUrl}/purchases`);
    }

    getUserSales(): Observable<Lot[]> {
        return this.http.get<Lot[]>(`${this.lotUrl}/sales`);
    }

    changeRemunerationMode(lotId: number, remunerationMode: RemunerationMode): Observable<Lot> {
        return this.http.put<Lot>(`${this.lotUrl}/${lotId}/remuneration-mode`, {
            remunerationMode,
        });
    }

    getLotFromTransactionId(transactionId: number): Observable<Lot> {
        return this.http.get<Lot>(`${this.lotUrl}/transactionId/${transactionId}`);
    }

    getLotFromRatingId(ratingId: number): Observable<Lot> {
        return this.http.get<Lot>(`${this.lotUrl}/rating/${ratingId}`);
    }
}
