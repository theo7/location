import { Injectable } from '@angular/core';
import { Capacitor } from '@capacitor/core';
import { LocalNotifications, LocalNotificationSchema } from '@capacitor/local-notifications';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root',
})
export class NativeNotificationService {
    isSupported() {
        const platform = Capacitor.getPlatform();
        return platform === 'ios' || platform === 'android';
    }

    isEnabled() {
        return this.isSupported();
    }

    listenLocalNotificationPerformed() {
        return new Observable<LocalNotificationSchema>(observer => {
            const handler = LocalNotifications.addListener('localNotificationActionPerformed', actionPerformed => {
                observer.next(actionPerformed.notification);
            });

            return () => {
                handler.remove();
            };
        });
    }
}
