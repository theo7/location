import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
    providedIn: 'root',
})
export class ScrollStatesService {
    private scrollStates = new Map<string, number>();
    private home$ = new Subject<void>();

    getScroll(key) {
        return this.scrollStates.get(key) ?? 0;
    }
    setScrollState(key: string, value: number) {
        this.scrollStates.set(key, value);
    }

    getHome$() {
        return this.home$.asObservable();
    }
    nextHome() {
        this.home$.next();
    }
}
