import { Injectable } from '@angular/core';
import { CustomShareData, ShareService } from './share.service';

const navigator: any = window.navigator;

@Injectable({
    providedIn: 'root',
})
export class WebShareService extends ShareService {
    canShare(): boolean {
        return !!navigator.share;
    }

    canShareFiles(files: File[]): boolean {
        return navigator.canShare && navigator.canShare({ files });
    }

    share(data: CustomShareData) {
        if (this.canShare()) {
            if (data.files && this.canShareFiles(data.files)) {
                navigator.share(data);
                return;
            }

            const { files, ...dataWithoutFiles } = data;
            navigator.share(dataWithoutFiles);
        }
    }
}
