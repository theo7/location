import { CurrencyPipe } from '@angular/common';
import { TestBed } from '@angular/core/testing';
import { CategoriesSelectors } from 'src/app/store/services/categories-selectors.service';
import { RepositoriesSelectors } from 'src/app/store/services/repositories-selectors.service';
import { SearchService } from './search.service';

describe('SearchService', () => {
    let service: SearchService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                {
                    provide: CategoriesSelectors,
                    useValue: {},
                },
                {
                    provide: RepositoriesSelectors,
                    useValue: {},
                },
                CurrencyPipe,
            ],
        });
        service = TestBed.inject(SearchService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });
});
