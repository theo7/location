import { Injectable } from '@angular/core';
import { PushNotifications, PushNotificationSchema } from '@capacitor/push-notifications';
import { StorageMap } from '@ngx-pwa/local-storage';
import { Observable } from 'rxjs';
import { LOCAL_STORAGE_NOTIFICATIONS_SETTINGS } from '../models/models';
import { NativeNotificationService } from './native-notification.service';
import { PushService } from './push.service';

@Injectable({
    providedIn: 'root',
})
export class NativePushService extends PushService {
    constructor(
        private readonly storage: StorageMap,
        private readonly nativeNotificationService: NativeNotificationService,
    ) {
        super();
    }

    isSupported() {
        return this.nativeNotificationService.isSupported();
    }

    isEnabled() {
        return this.nativeNotificationService.isEnabled();
    }

    isActivated() {
        if (!this.isEnabled()) {
            return false;
        }

        const pushActivated = this.storage.get(LOCAL_STORAGE_NOTIFICATIONS_SETTINGS, { type: 'boolean' });
        return !!pushActivated;
    }

    getPushNotificationToken() {
        return new Observable<string | undefined>(observer => {
            const valueHandler = PushNotifications.addListener('registration', token => {
                observer.next(token.value);
                observer.complete();
            });

            const errorHandler = PushNotifications.addListener('registrationError', error => {
                observer.error(error);
            });

            PushNotifications.register().catch(error => {
                observer.error(error);
            });

            const timer = setTimeout(() => {
                observer.error('Time out...');
            }, 5000);

            return () => {
                clearTimeout(timer);
                valueHandler.remove();
                errorHandler.remove();
            };
        });
    }

    listenPushNotificationReceived() {
        return new Observable<PushNotificationSchema>(observer => {
            const handler = PushNotifications.addListener('pushNotificationReceived', notification => {
                observer.next(notification);
            });

            return () => {
                handler.remove();
            };
        });
    }

    listenPushNotificationPerformed() {
        return new Observable<PushNotificationSchema>(observer => {
            const handler = PushNotifications.addListener('pushNotificationActionPerformed', actionPerformed => {
                observer.next(actionPerformed.notification);
            });

            return () => {
                handler.remove();
            };
        });
    }
}
