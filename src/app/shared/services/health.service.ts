import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { Health } from '../models/models';

@Injectable({
    providedIn: 'root',
})
export class HealthService {
    constructor(private http: HttpClient) {}

    healthCheck(): Observable<Health> {
        let headers = new HttpHeaders();
        headers = headers.append('without_auth', 'true');
        return this.http.get<Health>(`${environment.healthUrl}`, { headers });
    }
}
