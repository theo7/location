import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

type FrontVersionRequirement = {
    frontVersion: string;
};

@Injectable({
    providedIn: 'root',
})
export class RequirementsService {
    private readonly requirementsUrl = `${environment.apiUrl}/requirements`;

    constructor(private readonly http: HttpClient) {}

    public frontVersion() {
        return this.http.get<FrontVersionRequirement>(`${this.requirementsUrl}`);
    }
}
