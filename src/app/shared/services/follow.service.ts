import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { Page, UserFollowingProfile } from '../models/models';

@Injectable({
    providedIn: 'root',
})
export class FollowService {
    private readonly baseUrl = `${environment.apiUrl}/users`;

    constructor(private http: HttpClient) {}

    follow(userId: number): Observable<UserFollowingProfile> {
        return this.http.post<UserFollowingProfile>(`${this.baseUrl}/follow/${userId}`, null);
    }

    unfollow(userId: number): Observable<void> {
        return this.http.post<void>(`${this.baseUrl}/unfollow/${userId}`, null);
    }

    isFollowing(userId: number): Observable<UserFollowingProfile> {
        return this.http.get<UserFollowingProfile>(`${this.baseUrl}/following/${userId}`);
    }

    getUserFollowing(userId: number, page: number): Observable<Page<UserFollowingProfile> & { totalPages: number }> {
        const httpParams = new HttpParams()
            .set('page', page.toString())
            .set('size', '10')
            .set('sort', `lastModifiedDate,DESC`);

        return this.http.get<Page<UserFollowingProfile> & { totalPages: number }>(
            `${this.baseUrl}/${userId}/following`,
            {
                params: httpParams,
            },
        );
    }

    getUserFollower(userId: number, page: number): Observable<Page<UserFollowingProfile> & { totalPages: number }> {
        const httpParams = new HttpParams()
            .set('page', page.toString())
            .set('size', '10')
            .set('sort', `lastModifiedDate,DESC`);

        return this.http.get<Page<UserFollowingProfile> & { totalPages: number }>(
            `${this.baseUrl}/${userId}/follower`,
            {
                params: httpParams,
            },
        );
    }
}
