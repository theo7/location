import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { ProductsService } from './products.service';

describe('ProductsService', () => {
    let service: ProductsService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
        });

        service = TestBed.inject(ProductsService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    describe('getTwoDigitPrice', () => {
        const testCases: {
            value: number;
            expected: number;
        }[] = [
            { value: 1, expected: 1 },
            { value: 1.9, expected: 1.9 },
            { value: 1.11, expected: 1.11 },
            { value: 1.111, expected: 1.11 },
            { value: 1.186, expected: 1.18 },
            { value: 1.199999999, expected: 1.19 },
            { value: 1.00009, expected: 1.0 },
        ];

        testCases.forEach(({ value, expected }) => {
            it(`${value} should return ${expected}`, () => {
                const result = service.getTwoDigitPrice(value);
                expect(result).toBe(expected);
            });
        });
    });
});
