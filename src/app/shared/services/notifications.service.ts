import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { filter, switchMap } from 'rxjs/operators';
import { NotificationAngularFireDatabase } from 'src/app/multiple-database-factory';
import { environment } from '../../../environments/environment';
import { UserProfile } from '../models/models';
import { PushNotificationList } from './../models/models';
import { UserService } from './user.service';

@Injectable({
    providedIn: 'root',
})
export class NotificationsService {
    private readonly notificationsUrl = `${environment.apiUrl}/notifications`;

    constructor(
        private readonly http: HttpClient,
        private readonly notificationDatabase: NotificationAngularFireDatabase,
        private readonly userService: UserService,
    ) {}

    register(token: string): Observable<void> {
        return this.http.post<void>(`${this.notificationsUrl}/register`, { token });
    }

    unregister(token: string): Observable<void> {
        return this.http.delete<void>(`${this.notificationsUrl}/unregister/${encodeURIComponent(token)}`);
    }

    isRegistered(token: string) {
        return this.http.get<boolean>(`${this.notificationsUrl}/registered/${encodeURIComponent(token)}`);
    }

    watchNotifications(): Observable<PushNotificationList | null> {
        const watchUserNotifications = (user: UserProfile) => {
            return this.notificationDatabase.object<PushNotificationList>(`/${user.firebaseId}`).valueChanges();
        };
        return this.userService.getCurrentUser().pipe(
            filter(u => !!u),
            switchMap(user => watchUserNotifications(user)),
        );
    }

    setNotificationRead(): Observable<void> {
        return this.userService.getCurrentUser().pipe(
            switchMap(user =>
                this.notificationDatabase.object<PushNotificationList>(`/${user.firebaseId}`).update({
                    read: true,
                }),
            ),
        );
    }
}
