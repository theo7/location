import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { CashHistory, Page, PageLw, StoreCredit, Voucher } from '../models/models';

@Injectable({
    providedIn: 'root',
})
export class StoreCreditService {
    private readonly baseUrl = `${environment.apiUrl}/users/store-credit`;

    constructor(private http: HttpClient) {}

    getStoreCredit(): Observable<StoreCredit> {
        return this.http.get<StoreCredit>(this.baseUrl);
    }

    redeemVoucher(): Observable<void> {
        return this.http.post<void>(`${this.baseUrl}/redeem-voucher`, null);
    }

    redeemCash(amount: number): Observable<void> {
        return this.http.post<void>(`${this.baseUrl}/redeem-cash`, amount);
    }

    getStoreCreditHistory(page: number): Observable<Page<Voucher>> {
        const httpParams = new HttpParams()
            .set('page', page.toString())
            .set('size', '20')
            .set('sort', `lastModifiedDate,DESC`);
        return this.http.get<Page<Voucher>>(`${this.baseUrl}/vouchers`, { params: httpParams });
    }

    getCashCreditHistory(page: number): Observable<PageLw<CashHistory>> {
        const httpParams = new HttpParams()
            .set('page', page.toString())
            .set('size', '20')
            .set('sort', `lastModifiedDate,DESC`);
        return this.http.get<PageLw<CashHistory>>(`${this.baseUrl}/cash-history`, { params: httpParams });
    }
}
