export type CustomShareData = {
    title?: string;
    text?: string;
    url?: string;
    files?: File[];
};

export abstract class ShareService {
    abstract canShare(): boolean;
    abstract share(data: CustomShareData): void;
}
