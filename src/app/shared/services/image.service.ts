import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import Compressor from 'compressorjs';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { Picture } from '../models/models';

@Injectable({
    providedIn: 'root',
})
export class ImageService {
    private readonly s3Url = environment.s3url;
    private readonly imageUrl = `${environment.apiUrl}/pictures`;

    constructor(private readonly httpClient: HttpClient) {}

    getUrl(fileName: string | undefined): string {
        return fileName ? `${this.s3Url}${fileName}` : '';
    }

    getStaticUrl(fileName: string | undefined): string {
        return !!fileName ? `assets/images/${fileName}` : '';
    }

    fetchImageAsBlob(imageUrl: string): Observable<Blob> {
        return this.httpClient.get(imageUrl, { responseType: 'blob' });
    }

    fetchImageAsFile(imageUrl: string) {
        return this.fetchImageAsBlob(imageUrl).pipe(map(blob => new File([blob], imageUrl, { type: blob.type })));
    }

    findChatByName(roomId: number, imageName: string): Observable<Picture> {
        return this.httpClient.get<Picture>(`${this.imageUrl}/chat/${roomId}/${imageName}`);
    }

    compress(file: File | Blob): Observable<Blob> {
        return new Observable(observer => {
            const compressor = new Compressor(file, {
                maxWidth: 800,
                mimeType: 'image/jpeg',
                success(newFile: Blob) {
                    observer.next(newFile);
                    observer.complete();
                },
                error(error: Error) {
                    observer.error(error);
                },
            });

            return () => compressor.abort();
        });
    }
}
