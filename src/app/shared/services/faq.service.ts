import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { Faq } from '../models/models';

@Injectable({
    providedIn: 'root',
})
export class FaqService {
    private readonly faqUrl = `${environment.apiUrl}/faq`;

    constructor(private readonly http: HttpClient) {}

    listFaqs(): Observable<Faq[]> {
        return this.http.get<Faq[]>(this.faqUrl);
    }
}
