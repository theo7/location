import { Location } from '@angular/common';
import { TestBed } from '@angular/core/testing';
import { NavigationEnd, NavigationStart, Router } from '@angular/router';
import { StorageMap } from '@ngx-pwa/local-storage';
import { cold, hot } from 'jest-marbles';
import { OfflineNotificationService } from 'src/app/features/offline/offline-notification.service';
import { ConnectionSelectors } from 'src/app/store/services/connection-selectors.service';
import { NavigationHistoryService } from './navigation-history.service';

describe('NavigationHistoryService', () => {
    let service: NavigationHistoryService;
    let storage: StorageMap;
    let router: Router;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [
                {
                    provide: StorageMap,
                    useValue: {
                        get: jest.fn(),
                        set: jest.fn(),
                    },
                },
                {
                    provide: Router,
                    useValue: {},
                },
                {
                    provide: OfflineNotificationService,
                    useValue: {},
                },
                {
                    provide: ConnectionSelectors,
                    useValue: {},
                },
                {
                    provide: Location,
                    useValue: {},
                },
            ],
        });

        storage = TestBed.inject(StorageMap);
        router = TestBed.inject(Router);
    });

    it('should be created', () => {
        service = TestBed.inject(NavigationHistoryService);

        expect(service).toBeTruthy();
    });

    describe('load', () => {
        it('should be on home page at first visit', () => {
            jest.spyOn(storage, 'get').mockReturnValue(cold('-(h|)', { h: undefined }));

            (router as any).events = hot('-ab|', {
                a: new NavigationStart(1, '/', 'imperative'),
                b: new NavigationEnd(1, '/', '/'),
            });

            service = TestBed.inject(NavigationHistoryService);

            service.load();

            expect(router.events).toSatisfyOnFlush(() => {
                expect(service.history).toEqual([{ id: 1, url: '/' }]);
            });
        });

        it('should move to chat page', () => {
            jest.spyOn(storage, 'get').mockReturnValue(cold('-(h|)', { h: undefined }));

            (router as any).events = hot('-abcd|', {
                a: new NavigationStart(1, '/', 'imperative'),
                b: new NavigationEnd(1, '/', '/'),
                c: new NavigationStart(2, '/chat', 'imperative'),
                d: new NavigationEnd(2, '/chat', '/chat'),
            });

            service = TestBed.inject(NavigationHistoryService);

            service.load();

            expect(router.events).toSatisfyOnFlush(() => {
                expect(service.history).toEqual([
                    { id: 1, url: '/' },
                    { id: 2, url: '/chat' },
                ]);
            });
        });

        it('should move to chat page and then move back', () => {
            jest.spyOn(storage, 'get').mockReturnValue(cold('-(h|)', { h: undefined }));

            (router as any).events = hot('-abcdef|', {
                a: new NavigationStart(1, '/', 'imperative'),
                b: new NavigationEnd(1, '/', '/'),
                c: new NavigationStart(2, '/chat', 'imperative'),
                d: new NavigationEnd(2, '/chat', '/chat'),
                e: new NavigationStart(3, '/', 'popstate'),
                f: new NavigationEnd(3, '/', '/'),
            });

            service = TestBed.inject(NavigationHistoryService);

            service.load();

            expect(router.events).toSatisfyOnFlush(() => {
                expect(service.history).toEqual([{ id: 1, url: '/' }]);
            });
        });

        it('skip first navigation if history already full', () => {
            const history = [
                { id: 1, url: '/' },
                { id: 2, url: '/chat' },
                { id: 3, url: '/product/4' },
                { id: 4, url: '/profile' },
            ];
            jest.spyOn(storage, 'get').mockReturnValue(cold('-(h|)', { h: history }));

            (router as any).events = hot('-ab|', {
                a: new NavigationStart(1, '/profile', 'imperative'),
                b: new NavigationEnd(1, '/profile', '/profile'),
            });

            service = TestBed.inject(NavigationHistoryService);

            service.load();

            expect(router.events).toSatisfyOnFlush(() => {
                expect(service.history).toEqual(history);
            });
        });

        it('should remove replaced history entries when moving back', () => {
            jest.spyOn(storage, 'get').mockReturnValue(cold('-(h|)', { h: undefined }));

            (router as any).events = hot('-abcdefghij|', {
                a: new NavigationStart(1, '/', 'imperative'),
                b: new NavigationEnd(1, '/', '/'),
                c: new NavigationStart(2, '/search', 'imperative'),
                d: new NavigationEnd(2, '/search', '/search'),
                e: new NavigationStart(3, '/login', 'imperative'),
                f: new NavigationEnd(3, '/login', '/login'),
                g: new NavigationStart(4, '/', 'imperative'),
                h: new NavigationEnd(4, '/', '/'),
                i: new NavigationStart(5, '/search', 'popstate', { navigationId: 2 }),
                j: new NavigationEnd(5, '/search', '/search'),
            });

            service = TestBed.inject(NavigationHistoryService);

            service.load();

            expect(router.events).toSatisfyOnFlush(() => {
                expect(service.history).toEqual([
                    { id: 1, url: '/' },
                    { id: 2, url: '/search' },
                ]);
            });
        });

        it('should remove replaced history entries when moving forward', () => {
            jest.spyOn(storage, 'get').mockReturnValue(cold('-(h|)', { h: undefined }));

            (router as any).events = hot('-abcdefghijkl|', {
                a: new NavigationStart(1, '/', 'imperative'),
                b: new NavigationEnd(1, '/', '/'),
                c: new NavigationStart(2, '/search', 'imperative'),
                d: new NavigationEnd(2, '/search', '/search'),
                e: new NavigationStart(3, '/login', 'imperative'),
                f: new NavigationEnd(3, '/login', '/login'),
                g: new NavigationStart(4, '/', 'imperative'),
                h: new NavigationEnd(4, '/', '/'),
                i: new NavigationStart(5, '/search', 'popstate', { navigationId: 2 }),
                j: new NavigationEnd(5, '/search', '/search'),
                k: new NavigationStart(6, '/login', 'popstate', { navigationId: 3 }),
                l: new NavigationEnd(6, '/login', '/login'),
            });

            service = TestBed.inject(NavigationHistoryService);

            service.load();

            expect(router.events).toSatisfyOnFlush(() => {
                expect(service.history).toEqual([
                    { id: 1, url: '/' },
                    { id: 2, url: '/search' },
                    { id: 6, url: '/login' },
                ]);
            });
        });
    });
});
