import { CurrencyPipe } from '@angular/common';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { first, map } from 'rxjs/operators';
import { CategoriesSelectors } from '../../store/services/categories-selectors.service';
import { RepositoriesSelectors } from '../../store/services/repositories-selectors.service';
import { toNumberArray } from '../functions/number.functions';
import { Criterion, Search } from '../models/models';

@Injectable({
    providedIn: 'root',
})
export class SearchService {
    constructor(
        private readonly categoriesSelectors: CategoriesSelectors,
        private readonly repositoriesSelectors: RepositoriesSelectors,
        private readonly currencyPipe: CurrencyPipe,
    ) {}

    public getTranslationKey(
        search: Search,
        key: string,
    ): Observable<Criterion | undefined> | Observable<Criterion | undefined>[] {
        const value = search[key];

        switch (key) {
            case 'categoryId':
                return this.categoriesSelectors.category$(value).pipe(
                    first(),
                    map(c => (c ? { key: `product.category.${c.labelKey}` } : undefined)),
                );
            case 'subCategoryId': {
                const { categoryId, subCategoryId } = search;
                return this.categoriesSelectors.subCategory$(categoryId!, subCategoryId!).pipe(
                    first(),
                    map(c => (c ? { key: `product.sub_category.${c.labelKey}` } : undefined)),
                );
            }
            case 'productTypeIds': {
                const { categoryId, subCategoryId } = search;
                return toNumberArray(value).map(id =>
                    this.categoriesSelectors.productType$(categoryId!, subCategoryId!, id).pipe(
                        first(),
                        map(t => (t ? { key: `product.type.${t.labelKey}` } : undefined)),
                    ),
                );
            }
            case 'brandIds':
                return toNumberArray(value).map(id =>
                    this.repositoriesSelectors.brandById$(id).pipe(
                        first(),
                        map(c => (c ? { key: c.label } : undefined)),
                    ),
                );
            case 'sizeIds': {
                const { categoryId } = search;
                return toNumberArray(value).map(id =>
                    this.categoriesSelectors.size$(categoryId!, id).pipe(
                        first(),
                        map(s => (s ? { key: `product.size.${s.labelKey}` } : undefined)),
                    ),
                );
            }
            case 'conditionIds':
                return toNumberArray(value).map(id =>
                    this.repositoriesSelectors.conditionById$(id).pipe(
                        first(),
                        map(c => (c ? { key: `condition.label.${c.labelKey}` } : undefined)),
                    ),
                );
            case 'colorIds':
                return toNumberArray(value).map(id =>
                    this.repositoriesSelectors.colorById$(id).pipe(
                        first(),
                        map(c => (c ? { key: `color.${c.labelKey}` } : undefined)),
                    ),
                );
            case 'minPrice': {
                const price = this.currencyPipe.transform(value);
                return of({ key: 'filter.price.start', params: { price } });
            }
            case 'maxPrice': {
                const price = this.currencyPipe.transform(value);
                return of({ key: 'filter.price.stop', params: { price } });
            }
            case 'seasonality':
                return of({ key: `product.seasonality.${value}` });
            default:
                return of(undefined);
        }
    }
}
