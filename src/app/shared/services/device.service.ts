import { BreakpointObserver } from '@angular/cdk/layout';
import { Injectable } from '@angular/core';
import { fromEvent, Observable } from 'rxjs';
import { distinctUntilChanged, map, shareReplay, tap } from 'rxjs/operators';

export type DeviceMode = 'handset' | 'desktop';

@Injectable({
    providedIn: 'root',
})
export class DeviceService {
    public mode?: DeviceMode;

    public mode$: Observable<DeviceMode>;

    public isHandset$ = this.breakpointObserver.observe(['(max-width: 959.99px)']).pipe(
        tap(e =>
            console.log(
                `Device mode : ${e.matches ? '📱 %c Handset ' : '💻 %c Desktop '}`,
                'background: #374046; color: #FF5722; font-weight bold;',
            ),
        ),
        map(e => e.matches),
        shareReplay(),
    );

    public isDesktop$ = this.isHandset$.pipe(
        map(e => !e),
        shareReplay(),
    );

    constructor(private readonly breakpointObserver: BreakpointObserver) {
        this.mode$ = this.isHandset$.pipe(
            map(isHandset => (isHandset ? 'handset' : 'desktop')),
            distinctUntilChanged<DeviceMode>(),
        );

        this.mode$.subscribe(mode => (this.mode = mode));
    }

    private matchUserAgent(patterns: RegExp[]) {
        return patterns.some(toMatchItem => {
            return navigator.userAgent.match(toMatchItem);
        });
    }

    public isAndroidWebBrowser() {
        return this.matchUserAgent([/Android/i]);
    }

    public isiOSWebBrowser() {
        return this.matchUserAgent([/iPhone/i, /iPad/i, /iPod/i]);
    }

    public ready$() {
        return fromEvent(document, 'deviceready');
    }
}
