import { Injectable } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { TranslateService } from '@ngx-translate/core';
import { Brand, Product, UserProfile } from '../models/models';
import { ImageService } from './image.service';

type FacebookTagPayload = {
    url?: string;
    type?: string;
    title: string;
    description: string;
    image?: string;
};

@Injectable({
    providedIn: 'root',
})
export class SeoService {
    constructor(
        private readonly title: Title,
        private readonly meta: Meta,
        private readonly translate: TranslateService,
        private readonly imageService: ImageService,
    ) {}

    reset(): void {
        this.setTitle(this.translate.instant('seo.default.title'));
        this.setDescription(this.translate.instant('seo.default.description'));
        this.resetFacebookTags();
    }

    configureBrandPage(brand: Brand): void {
        const brandLabel = brand.label;

        this.setTitle(this.translate.instant('seo.brand.title', { brandLabel }));
        this.setDescription(this.translate.instant('seo.brand.description', { brandLabel }));
    }

    configureDressingPage(user: UserProfile) {
        this.setFacebookTags({
            url: `${window.location.origin}/profile/${user.id}`,
            title: this.translate.instant('profile.view.dressing.share.title', {
                nickname: user.nickname,
            }),
            description: this.translate.instant('profile.view.dressing.share.text', {
                nickname: user.nickname,
            }),
            image: this.imageService.getUrl(user.picture?.fileName),
        });
    }

    configureProductPage(product: Product) {
        this.setFacebookTags({
            url: `${window.location.origin}/product/${product.id}`,
            title: this.translate.instant('product.detail.share.title', {
                productName: product.label,
            }),
            description: this.translate.instant('product.detail.share.text', {
                productName: product.label,
            }),
            image: this.imageService.getUrl(product.pictures[0]?.fileName),
        });
    }

    private setTitle(title: string): void {
        this.title.setTitle(title);
    }

    private setDescription(description: string): void {
        this.meta.updateTag({ name: 'description', content: description });
    }

    private removeTagByName(tagName: string): void {
        this.meta.removeTag(`name='${tagName}'`);
    }

    private removeTagByProperty(tagName: string): void {
        this.meta.removeTag(`property='${tagName}'`);
    }

    private setFacebookTags(payload: FacebookTagPayload) {
        if (payload.url) {
            this.meta.updateTag({ property: 'og:url', content: payload.url });
        }
        this.meta.updateTag({ property: 'og:type', content: payload.type || 'website' });
        this.meta.updateTag({ property: 'og:title', content: payload.title });
        this.meta.updateTag({ property: 'og:description', content: payload.description });
        if (payload.image) {
            this.meta.updateTag({ property: 'og:image', content: payload.image });
        }
    }

    private resetFacebookTags() {
        this.removeTagByProperty('og:url');
        this.removeTagByProperty('og:type');
        this.removeTagByProperty('og:title');
        this.removeTagByProperty('og:description');
        this.removeTagByProperty('og:image');
    }
}
