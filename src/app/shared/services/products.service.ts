import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import {
    Brand,
    Category,
    Color,
    Condition,
    Discussion,
    NewProduct,
    PackagingType,
    Page,
    Picture,
    PictureType,
    PriceAverage,
    Product,
    ProductFilter,
} from '../models/models';

@Injectable({
    providedIn: 'root',
})
export class ProductsService {
    private readonly attributesUrl = `${environment.apiUrl}/attributes`;
    private readonly productsUrl = `${environment.apiUrl}/products`;
    private readonly picturesUrl = `${environment.apiUrl}/pictures`;
    private readonly lotsUrl = `${environment.apiUrl}/lots`;
    private readonly ownProductsUrl = `${environment.apiUrl}/users/my-products`;
    private readonly userProductsUrl = userId => `${environment.apiUrl}/users/${userId}/products`;

    constructor(private readonly http: HttpClient) {}

    getProducts(page = 0, limit = 20): Observable<Page<Product>> {
        const headers = new HttpHeaders().append('without_auth', 'true');
        const params = new HttpParams().append('page', String(page)).append('size', String(limit));
        return this.http.get<Page<Product>>(this.productsUrl, { headers, params });
    }

    getUserProducts(userId: number, filter: ProductFilter, page: number, limit: number): Observable<Page<Product>> {
        const sort = filter.sort ?? { field: 'creationDate', order: 'desc' };

        const headers = new HttpHeaders().append('without_auth', 'true');
        const params = new HttpParams()
            .set('page', String(page))
            .set('size', String(limit))
            .set('sort', [sort.field, sort.order].join(','));

        return this.http.post<Page<Product>>(`${this.userProductsUrl(userId)}/search`, this.cleanFilter(filter), {
            headers,
            params,
        });
    }

    getOwnProducts(page: number, limit: number): Observable<Page<Product>> {
        const params = new HttpParams()
            .set('page', String(page))
            .set('size', String(limit))
            .set('sort', `productStatus,DESC`)
            .append('sort', `creationDate,DESC`);
        return this.http.get<Page<Product>>(this.ownProductsUrl, { params });
    }

    getProduct(id: number): Observable<Product> {
        let headers = new HttpHeaders();
        headers = headers.append('without_auth', 'true');
        return this.http.get<Product>(`${this.productsUrl}/${id}`, { headers });
    }

    getProductViews(id: number) {
        let headers = new HttpHeaders();
        headers = headers.append('without_auth', 'true');
        return this.http.get<number>(`${this.productsUrl}/statistics/${id}/unique-page-views`, { headers });
    }

    getCategories(indexOnly = false): Observable<Category[]> {
        const params = new HttpParams().set('indexedOnly', `${indexOnly}`);
        return this.http.get<Category[]>(`${this.attributesUrl}/categories`, { params });
    }

    getBrands(indexOnly = false): Observable<Brand[]> {
        const params = new HttpParams().set('indexedOnly', `${indexOnly}`);
        return this.http.get<Brand[]>(`${this.attributesUrl}/brands`, { params });
    }

    getColors(indexOnly = false): Observable<Color[]> {
        const params = new HttpParams().set('indexedOnly', `${indexOnly}`);
        return this.http.get<Color[]>(`${this.attributesUrl}/colors`, { params });
    }

    getConditions(indexOnly = false): Observable<Condition[]> {
        const params = new HttpParams().set('indexedOnly', `${indexOnly}`);
        return this.http.get<Condition[]>(`${this.attributesUrl}/conditions`, { params });
    }

    getPopularBrands(): Observable<Brand[]> {
        return this.http.get<Brand[]>(`${this.attributesUrl}/brands/popular`);
    }

    getPackagingTypes(indexOnly = false): Observable<PackagingType[]> {
        const params = new HttpParams().set('indexedOnly', `${indexOnly}`);
        return this.http.get<PackagingType[]>(`${this.attributesUrl}/packaging-types`, { params });
    }

    uploadPicture(file: Blob, pictureType: PictureType): Observable<Picture> {
        const formData = new FormData();
        formData.append('file', file);
        formData.append('pictureType', pictureType);
        return this.http.post<Picture>(this.picturesUrl, formData);
    }

    saveProduct(product: NewProduct): Observable<Product> {
        return this.http.post<Product>(this.productsUrl, product);
    }

    updateProduct(product: Product): Observable<Product> {
        return this.http.put<Product>(`${this.productsUrl}/${product.id}`, product);
    }

    startDiscussion(productId: number): Observable<Discussion> {
        return this.http.post<Discussion>(`${this.lotsUrl}/new/${productId}`, {});
    }

    startListDiscussion(productIds: number[]): Observable<Discussion> {
        return this.http.post<Discussion>(`${this.lotsUrl}/new/`, productIds);
    }

    searchProducts(filter: ProductFilter, page = 0, limit = 20): Observable<Page<Product>> {
        const sort = filter.sort ?? { field: 'creationDate', order: 'desc' };

        const headers = new HttpHeaders().append('without_auth', 'true');
        const params = new HttpParams()
            .set('page', page.toString())
            .set('size', limit.toString())
            .set('sort', [sort.field, sort.order].join(','));

        return this.http.post<Page<Product>>(`${this.productsUrl}/search`, this.cleanFilter(filter), {
            headers,
            params,
        });
    }

    searchProductsCount(filter: ProductFilter): Observable<number> {
        const headers = new HttpHeaders().append('without_auth', 'true');

        return this.http.post<number>(`${this.productsUrl}/search/count`, this.cleanFilter(filter), {
            headers,
        });
    }

    deactivateProduct(productId: number): Observable<Product> {
        return this.http.post<Product>(`${this.productsUrl}/${productId}/deactivate`, {});
    }

    getDefaultBrand(): Observable<Brand> {
        const headers = new HttpHeaders().append('without_auth', 'true');

        return this.http.get<Brand>(`${this.attributesUrl}/brands/default`, {
            headers,
        });
    }

    reportProduct(productId: number, reason: string, content: string | undefined) {
        return this.http.post(`${this.productsUrl}/${productId}/report`, { reason, content });
    }

    like(productId: number) {
        return this.http.post<void>(`${this.productsUrl}/${productId}/like`, {});
    }

    deleteLike(productId: number) {
        return this.http.delete<void>(`${this.productsUrl}/${productId}/dislike`);
    }

    getWishlist(page = 0, limit = 20) {
        const params = new HttpParams().append('page', String(page)).append('size', String(limit));
        return this.http.get<Page<Product>>(`${this.productsUrl}/wishlist`, { params });
    }

    getSuggestions(page = 0, limit = 20) {
        const params = new HttpParams()
            .append('page', String(page))
            .append('size', String(limit))
            .append('sort', `creationDate,DESC`);
        return this.http.get<Page<Product>>(`${this.productsUrl}/suggestion`, { params });
    }

    getTwoDigitPrice(value: number): number {
        const valueStr = value.toString();
        const regex = new RegExp('^-?\\d+(?:.\\d{0,2})?');
        const twoDigit = valueStr.match(regex)![0];

        return Number(twoDigit);
    }

    getNbFavorites(productId: number): Observable<number> {
        return this.http.get<number>(`${this.productsUrl}/${productId}/like`);
    }

    private keepOnlyIdProperty(value: any): any {
        return value ? { id: value.id } : null;
    }

    private cleanFilter(filter: ProductFilter): ProductFilter & { search?: string } {
        const lightFilter: ProductFilter & { search?: string } = {};

        if (filter.searchText) lightFilter.search = filter.searchText;
        if (filter.category) lightFilter.category = this.keepOnlyIdProperty(filter.category);
        if (filter.subCategory) lightFilter.subCategory = this.keepOnlyIdProperty(filter.subCategory);
        if (filter.productsTypes?.length) lightFilter.productsTypes = filter.productsTypes.map(this.keepOnlyIdProperty);
        if (filter.brands?.length) lightFilter.brands = filter.brands.map(this.keepOnlyIdProperty);
        if (filter.colors?.length) lightFilter.colors = filter.colors.map(this.keepOnlyIdProperty);
        if (filter.conditions?.length) lightFilter.conditions = filter.conditions.map(this.keepOnlyIdProperty);
        if (filter.sizes?.length) lightFilter.sizes = filter.sizes.map(this.keepOnlyIdProperty);
        if (filter.minPrice) lightFilter.minPrice = filter.minPrice;
        if (filter.maxPrice) lightFilter.maxPrice = filter.maxPrice;
        if (filter.seasonality) lightFilter.seasonality = filter.seasonality;
        if (!!filter.excludedIds && filter.excludedIds.length > 0) lightFilter.excludedIds = filter.excludedIds;

        return lightFilter;
    }

    getPriceAverage(filter: ProductFilter) {
        return this.http.post<PriceAverage>(`${this.productsUrl}/price-average`, this.cleanFilter(filter));
    }

    suggestBrand(value: string, productId?: number) {
        return this.http.post<void>(`${this.attributesUrl}/brands/suggest`, { value, productId });
    }
}
