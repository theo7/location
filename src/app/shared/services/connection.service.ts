import { Injectable } from '@angular/core';
import { fromEvent } from 'rxjs';

@Injectable({
    providedIn: 'root',
})
export class ConnectionService {
    online$ = () => fromEvent(window, 'online');
    offline$ = () => fromEvent(window, 'offline');
}
