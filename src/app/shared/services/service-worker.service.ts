import { Injectable } from '@angular/core';
import { EMPTY, from, fromEvent } from 'rxjs';

@Injectable({
    providedIn: 'root',
})
export class ServiceWorkerService {
    isSupported() {
        return !!navigator.serviceWorker;
    }

    getCurrent() {
        if (!this.isSupported()) {
            return EMPTY;
        }

        return from(navigator.serviceWorker.getRegistration());
    }

    message$() {
        if (!this.isSupported()) {
            return EMPTY;
        }

        return fromEvent(navigator.serviceWorker, 'message');
    }
}
