export abstract class PushService {
    abstract isSupported(): boolean;
    abstract isEnabled(): boolean;
    abstract isActivated(): boolean;
}
