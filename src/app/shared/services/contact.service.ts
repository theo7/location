import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ContactSubject, ContactSupportInfo, ContactTheme } from '../models/models';

@Injectable({
    providedIn: 'root',
})
export class ContactService {
    private readonly contactUrl = `${environment.apiUrl}/contact`;

    constructor(private readonly http: HttpClient) {}

    sendEmailContactSupport(contactInfo: ContactSupportInfo): Observable<ContactSupportInfo> {
        return this.http.post<ContactSupportInfo>(`${this.contactUrl}/support`, contactInfo);
    }

    getContactSupportThemes(): Observable<ContactTheme[]> {
        return this.http.get<ContactTheme[]>(`${this.contactUrl}/theme`);
    }

    getContactSupportSubjects(themeId: number): Observable<ContactSubject[]> {
        return this.http.get<ContactSubject[]>(`${this.contactUrl}/theme/${themeId}/subject`);
    }
}
