import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { ProductProperties, StoreCreditProperties } from '../models/models';

@Injectable({
    providedIn: 'root',
})
export class SettingService {
    private readonly url = `${environment.apiUrl}/settings`;

    constructor(private http: HttpClient) {}

    getProductProperties(): Observable<ProductProperties> {
        return this.http.get<ProductProperties>(`${this.url}/product`);
    }

    getStoreCreditProperties(): Observable<StoreCreditProperties> {
        return this.http.get<StoreCreditProperties>(`${this.url}/store-credit`);
    }
}
