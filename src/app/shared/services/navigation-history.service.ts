import { Location } from '@angular/common';
import { Injectable } from '@angular/core';
import { NavigationEnd, NavigationStart, Router } from '@angular/router';
import { Capacitor } from '@capacitor/core';
import { StorageMap } from '@ngx-pwa/local-storage';
import { max } from 'lodash-es';
import { filter, first, map, shareReplay, skip, switchMap, withLatestFrom } from 'rxjs/operators';
import { OfflineNotificationService } from '../../features/offline/offline-notification.service';
import { ConnectionSelectors } from '../../store/services/connection-selectors.service';

export const LOCAL_STORAGE_NAVIGATION_HISTORY = 'location.navigationHistory';

type HistoryEntry = {
    id: number;
    url: string;
};

@Injectable({
    providedIn: 'root',
})
export class NavigationHistoryService {
    public history: HistoryEntry[] = [];

    constructor(
        private readonly storage: StorageMap,
        private readonly router: Router,
        private readonly offlineNotificationService: OfflineNotificationService,
        private readonly connectionSelectors: ConnectionSelectors,
        private readonly location: Location,
    ) {}

    load() {
        const navigationStart$ = this.router.events.pipe(
            filter(event => event instanceof NavigationStart),
            map(e => e as NavigationStart),
        );

        const navigationEnd$ = this.router.events.pipe(
            filter(event => event instanceof NavigationEnd),
            map(e => e as NavigationEnd),
        );

        const startNavigateBack$ = navigationStart$.pipe(
            map(event => event.navigationTrigger === 'popstate'),
            shareReplay(),
        );
        const popstateRestoredState$ = navigationStart$.pipe(
            map(event => event.restoredState),
            shareReplay(),
        );

        const getHistory$ = this.storage.get<HistoryEntry[]>(LOCAL_STORAGE_NAVIGATION_HISTORY, {
            type: 'array',
            items: {
                type: 'object',
                properties: {
                    id: { type: 'number' },
                    url: { type: 'string' },
                },
                required: ['id', 'url'],
            },
        });

        getHistory$
            .pipe(
                switchMap(history => {
                    this.history = history || [];

                    return navigationEnd$.pipe(
                        skip(this.history.length > 0 ? 1 : 0), // skip first navigation (ex: app reload / F5)
                        withLatestFrom(startNavigateBack$),
                        filter(([_, navigateBack]) => !navigateBack),
                        map(([e]) => e),
                    );
                }),
            )
            .subscribe(event => {
                this.history.push({
                    id: event.id,
                    url: event.urlAfterRedirects,
                });

                this.storage.set(LOCAL_STORAGE_NAVIGATION_HISTORY, this.history);
            });

        navigationEnd$
            .pipe(
                withLatestFrom(startNavigateBack$),
                filter(([_, navigateBack]) => !!navigateBack),
                map(([e]) => e),
                withLatestFrom(popstateRestoredState$),
            )
            .subscribe(([event, popstateRestoredState]) => {
                if (popstateRestoredState?.navigationId !== undefined) {
                    const currentId = max(this.history.map(e => e.id)) || 0;

                    if (currentId < popstateRestoredState.navigationId) {
                        this.history.push({
                            id: event.id,
                            url: event.urlAfterRedirects,
                        });
                    } else {
                        this.history = this.history.filter(h => h.id <= popstateRestoredState.navigationId);
                    }
                } else {
                    this.history.pop();
                }

                this.storage.set(LOCAL_STORAGE_NAVIGATION_HISTORY, this.history);
            });
    }

    async back(previousRoute?: string): Promise<void> {
        const isOffline = await this.connectionSelectors.isOffline$.pipe(first()).toPromise();

        if (isOffline) {
            this.offlineNotificationService.displayNotification();
            return;
        }

        if (this.history.length > 1) {
            const previousRouteInHistory = this.history[this.history.length - 2];

            if (previousRoute && previousRoute !== previousRouteInHistory.url) {
                // handle back navigation using app back button
                this.router.navigateByUrl(previousRoute, { replaceUrl: true });
                return;
            }

            this.location.back();
            return;
        }

        if (Capacitor.isNativePlatform()) {
            const { App } = await import('@capacitor/app');
            App.exitApp();
        } else {
            this.location.back();

            // Closing window/tab only apply
            // if window opened with target="_blank" & on the first window page (no history back)
            // https://www.thesitewizard.com/javascripts/close-browser-tab-or-window.shtml
            window.close();
        }
    }
}
