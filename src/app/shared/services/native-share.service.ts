import { Injectable } from '@angular/core';
import { Share } from '@capacitor/share';
import { CustomShareData, ShareService } from './share.service';

@Injectable({
    providedIn: 'root',
})
export class NativeShareService extends ShareService {
    canShare() {
        return true;
    }

    share(data: CustomShareData) {
        Share.share({
            title: data.title,
            text: data.text,
            url: data.url,
        });
    }
}
