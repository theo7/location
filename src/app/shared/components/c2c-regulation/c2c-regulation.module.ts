import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { PipesModule } from '../../pipes/pipes.module';
import { C2cRegulationComponent } from './c2c-regulation.component';

@NgModule({
    declarations: [C2cRegulationComponent],
    imports: [CommonModule, TranslateModule, PipesModule],
    exports: [C2cRegulationComponent],
})
export class C2cRegulationModule {}
