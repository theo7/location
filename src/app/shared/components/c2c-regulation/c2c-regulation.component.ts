import { Component } from '@angular/core';

@Component({
    selector: 'app-c2c-regulation',
    templateUrl: './c2c-regulation.component.html',
    styleUrls: ['./c2c-regulation.component.scss'],
})
export class C2cRegulationComponent {
    hidden: boolean = true;

    viewAll(): void {
        this.hidden = false;
    }
}
