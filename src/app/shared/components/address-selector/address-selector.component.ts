import { Component, forwardRef, OnInit } from '@angular/core';
import { FormControl, NG_VALUE_ACCESSOR } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { orderBy } from 'lodash-es';
import { addAddressDialogComponent$ } from 'src/app/imports.dynamic';
import { UserProfileDispatchers } from '../../../store/services/user-dispatchers.service';
import { UserProfileSelectors } from '../../../store/services/user-selectors.service';
import { Address, UserProfile } from '../../models/models';
import { DeviceService } from '../../services/device.service';

@UntilDestroy()
@Component({
    selector: 'app-address-selector',
    templateUrl: './address-selector.component.html',
    styleUrls: ['./address-selector.component.scss'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => AddressSelectorComponent),
            multi: true,
        },
    ],
})
export class AddressSelectorComponent implements OnInit {
    addresses: Address[] = [];
    currentValueControl?: FormControl;
    showAll = false;

    private user?: UserProfile;

    constructor(
        private userSelectors: UserProfileSelectors,
        private userDispatchers: UserProfileDispatchers,
        private dialog: MatDialog,
        private deviceService: DeviceService,
    ) {}

    ngOnInit(): void {
        this.userSelectors.userProfile$.pipe(untilDestroyed(this)).subscribe(user => {
            this.user = user;
            this.addresses = orderBy(user?.addresses || [], 'isDefault', 'desc');
            this.currentValueControl = new FormControl(user?.addresses?.find(a => a.isDefault));
            this.currentValueControl.valueChanges.subscribe(value => {
                this.setDefaultAddress(value);
            });
        });
    }

    setDefaultAddress(address: Address) {
        const newAddress = { ...address };
        newAddress.isDefault = true;
        this.userDispatchers.updateUserAddress(newAddress);
    }

    addNewAddress() {
        const dialogDeviceParams = device =>
            ({
                handset: {
                    width: '100vw',
                    maxWidth: '100vw',
                    height: '100vh',
                    panelClass: 'full-screen-dialog',
                },
                desktop: {
                    height: '80vh',
                    width: '600px',
                    maxWidth: '600px',
                    disableClose: true,
                    panelClass: 'no-padding-dialog',
                },
            }[device]);

        addAddressDialogComponent$.subscribe(AddAddressDialogComponent => {
            this.dialog.open(AddAddressDialogComponent, {
                autoFocus: false,
                data: {
                    user: this.user,
                },
                ...dialogDeviceParams(this.deviceService.mode),
            });
        });
    }
}
