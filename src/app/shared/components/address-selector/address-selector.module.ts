import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { MatRadioModule } from '@angular/material/radio';
import { TranslateModule } from '@ngx-translate/core';
import { AddAddressDialogModule } from '../add-address-dialog/add-address-dialog.module';
import { AddressSelectorComponent } from './address-selector.component';

@NgModule({
    declarations: [AddressSelectorComponent],
    imports: [
        CommonModule,
        MatRadioModule,
        ReactiveFormsModule,
        MatDividerModule,
        TranslateModule,
        MatButtonModule,
        AddAddressDialogModule,
    ],
    exports: [AddressSelectorComponent],
})
export class AddressSelectorModule {}
