import { ChangeDetectionStrategy, Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { ModerationStatus, Product } from '../../models/models';
import { ImageService } from '../../services/image.service';

@Component({
    selector: 'app-images-mini',
    templateUrl: './images-mini.component.html',
    styleUrls: ['./images-mini.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ImagesMiniComponent implements OnChanges {
    @Input() products: Product[] = [];

    picturesUrl: string[] = [];

    constructor(private imageService: ImageService) {}

    ngOnChanges(changes: SimpleChanges) {
        const numberOfImagesToTake = this.products.length > 4 ? 3 : 4;

        this.picturesUrl = this.products
            .slice(0, numberOfImagesToTake)
            .map(product =>
                product?.pictures[0]?.status !== ModerationStatus.MODERATED
                    ? this.imageService.getUrl(product?.pictures[0]?.fileName)
                    : '',
            );
    }
}
