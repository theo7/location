import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DirectivesModule } from '../../directives/directives.module';
import { ImagesMiniComponent } from './images-mini.component';

@NgModule({
    declarations: [ImagesMiniComponent],
    imports: [CommonModule, DirectivesModule],
    exports: [ImagesMiniComponent],
})
export class ImagesMiniModule {}
