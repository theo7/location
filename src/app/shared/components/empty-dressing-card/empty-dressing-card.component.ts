import { Component } from '@angular/core';

@Component({
    selector: 'app-empty-dressing-card',
    templateUrl: './empty-dressing-card.component.html',
    styleUrls: ['./empty-dressing-card.component.scss'],
})
export class EmptyDressingCardComponent {}
