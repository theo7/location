import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { IconsModule } from '../../../features/icons/icons.module';
import { EmptyDressingCardComponent } from './empty-dressing-card.component';

@NgModule({
    declarations: [EmptyDressingCardComponent],
    imports: [CommonModule, TranslateModule, RouterModule, IconsModule, MatButtonModule],
    exports: [EmptyDressingCardComponent],
})
export class EmptyDressingCardModule {}
