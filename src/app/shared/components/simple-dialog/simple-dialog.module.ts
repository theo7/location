import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { SimpleDialogComponent } from './simple-dialog.component';

@NgModule({
    declarations: [SimpleDialogComponent],
    imports: [CommonModule, MatDialogModule, MatButtonModule],
    exports: [SimpleDialogComponent],
})
export class SimpleDialogModule {}
