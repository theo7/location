import { ChangeDetectionStrategy, Component, HostBinding, Input } from '@angular/core';

export type ChipVariant = 'primary' | 'success' | 'danger' | 'light-blue' | 'pink';

/* tslint:disable:component-selector */
@Component({
    selector: 'app-chip',
    templateUrl: './chip.component.html',
    styleUrls: ['./chip.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ChipComponent {
    @Input() variant: ChipVariant = 'primary';

    @HostBinding('class') get class() {
        return this.variant;
    }
}
