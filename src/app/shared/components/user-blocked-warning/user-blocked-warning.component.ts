import { ChangeDetectionStrategy, Component, Input, OnChanges } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Observable } from 'rxjs';
import { LotDispatchers } from '../../../store/services/lot-dispatchers.service';
import { UserProfileSelectors } from '../../../store/services/user-selectors.service';
import { UserProfile } from '../../models/models';

@UntilDestroy()
@Component({
    selector: 'app-user-blocked-warning',
    templateUrl: './user-blocked-warning.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserBlockedWarningComponent implements OnChanges {
    @Input()
    user?: UserProfile;

    isBlocked$?: Observable<boolean>;

    constructor(
        private readonly userProfileSelectors: UserProfileSelectors,
        private readonly lotDispatchers: LotDispatchers,
    ) {}

    ngOnChanges() {
        if (this.user?.id) {
            this.isBlocked$ = this.userProfileSelectors.isBlocked$(this.user?.id).pipe(untilDestroyed(this));
        }
    }

    unblockUser() {
        if (this.user?.id) {
            this.lotDispatchers.unblockUser(this.user.id);
        }
    }
}
