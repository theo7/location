import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { TranslateModule } from '@ngx-translate/core';
import { IconsModule } from '../../../features/icons/icons.module';
import { AppCoreModule } from '../../components/app-core/app-core.module';
import { UserBlockedWarningComponent } from './user-blocked-warning.component';

@NgModule({
    declarations: [UserBlockedWarningComponent],
    imports: [CommonModule, AppCoreModule, TranslateModule, MatButtonModule, IconsModule],
    exports: [UserBlockedWarningComponent],
})
export class UserBlockedWarningModule {}
