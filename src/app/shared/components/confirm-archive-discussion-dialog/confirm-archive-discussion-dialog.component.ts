import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ConfirmArchiveDiscussionDialogResult } from '../../models/models';

type ConfirmArchiveDiscussionDialogComponentData = {
    discussionId: number;
};

@Component({
    selector: 'app-confirm-archive-discussion-dialog',
    templateUrl: './confirm-archive-discussion-dialog.component.html',
    styleUrls: ['./confirm-archive-discussion-dialog.component.scss'],
})
export class ConfirmArchiveDiscussionDialogComponent {
    constructor(
        private readonly dialog: MatDialogRef<
            ConfirmArchiveDiscussionDialogComponent,
            ConfirmArchiveDiscussionDialogResult
        >,
        @Inject(MAT_DIALOG_DATA) private data: ConfirmArchiveDiscussionDialogComponentData,
    ) {}

    onYesButtonClicked() {
        this.dialog.close({
            discussionId: this.data.discussionId,
        });
    }
}
