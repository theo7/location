import {
    ChangeDetectionStrategy,
    Component,
    EventEmitter,
    Input,
    OnInit,
    Output,
    ViewEncapsulation,
} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Observable } from 'rxjs';
import { first, withLatestFrom } from 'rxjs/operators';
import { DiscussionsDispatchers } from 'src/app/store/services/discussions-dispatchers.service';
import { DiscussionsSelectors } from 'src/app/store/services/discussions-selectors.service';
import { LotDispatchers } from 'src/app/store/services/lot-dispatchers.service';
import { ProductsDispatchers } from 'src/app/store/services/products-dispatchers.service';
import { RouterSelectors } from 'src/app/store/services/router-selectors.service';
import { UserProfileSelectors } from 'src/app/store/services/user-selectors.service';
import { Product, ProductStatusEnum, UserType } from '../../models/models';
import { DeviceService } from '../../services/device.service';
import { ImageService } from '../../services/image.service';
import { GoToLoginDialogComponent } from '../go-to-login-dialog/go-to-login-dialog.component';

@UntilDestroy()
@Component({
    selector: 'app-product-card',
    templateUrl: './product-card.component.html',
    styleUrls: ['./product-card.component.scss'],
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProductCardComponent implements OnInit {
    @Output() productClickedEvent = new EventEmitter<Product>();

    @Input() product?: Product;
    @Input() displayLotAction = false;
    @Input() displayOwner = true;

    ProductStatusEnum = ProductStatusEnum;
    UserType = UserType;

    loadingImage = true;
    isHandset$ = this.deviceService.isHandset$.pipe(untilDestroyed(this));
    isCurrentUser$?: Observable<boolean>;
    isAdmin$: Observable<boolean>;
    isProductFavorite$?: Observable<boolean>;
    isProductInLot$?: Observable<boolean>;
    canCreateLot$?: Observable<boolean>;
    canAddToLot$?: Observable<boolean>;
    canRemoveFromLot$?: Observable<boolean>;

    constructor(
        private readonly deviceService: DeviceService,
        private readonly imageService: ImageService,
        private readonly userProfileSelectors: UserProfileSelectors,
        private readonly routerSelectors: RouterSelectors,
        private readonly dialog: MatDialog,
        private readonly productsDispatchers: ProductsDispatchers,
        private readonly discussionsSelectors: DiscussionsSelectors,
        private readonly discussionsDispatchers: DiscussionsDispatchers,
        private readonly lotDispatchers: LotDispatchers,
    ) {
        this.isAdmin$ = this.userProfileSelectors.isAdmin$.pipe(untilDestroyed(this));
    }

    ngOnInit(): void {
        this.isCurrentUser$ = this.userProfileSelectors
            .isCurrentUser$(this.product!.owner.id!)
            .pipe(untilDestroyed(this));

        this.isProductFavorite$ = this.userProfileSelectors
            .isProductFavorite$(this.product!.id)
            .pipe(untilDestroyed(this));

        this.isProductInLot$ = this.discussionsSelectors.isProductInLot$(this.product!.id).pipe(untilDestroyed(this));
        this.canCreateLot$ = this.discussionsSelectors
            .canCreateLot$(this.product!.owner.id!)
            .pipe(untilDestroyed(this));
        this.canAddToLot$ = this.discussionsSelectors
            .canAddToLot$(this.product!.owner.id!, this.product!.id)
            .pipe(untilDestroyed(this));
        this.canRemoveFromLot$ = this.discussionsSelectors
            .canRemoveFromLot$(this.product!.owner.id!, this.product!.id)
            .pipe(untilDestroyed(this));
    }

    productClicked() {
        this.productClickedEvent.emit(this.product);
    }

    onImageLoad() {
        this.loadingImage = false;
    }

    onCartButtonClicked(e: Event) {
        e.stopPropagation();

        if (this.isProductInLot$) {
            this.isProductInLot$
                .pipe(first(), withLatestFrom(this.discussionsSelectors.lotOpenBySellerId$(this.product!.owner.id!)))
                .subscribe(([isProductInLot, lot]) => {
                    if (lot?.id) {
                        if (this.product?.id) {
                            if (isProductInLot) {
                                this.discussionsDispatchers.removeProductFromLot(lot.id, this.product.id);
                            } else {
                                this.discussionsDispatchers.addProductToLot(lot.id, this.product.id);
                            }
                        }
                    } else {
                        if (this.product) {
                            this.lotDispatchers.createLot(this.product);
                        }
                    }
                });
        }
    }

    onLikeButtonClicked(e: Event) {
        e.stopPropagation();

        this.userProfileSelectors.isLogged$
            .pipe(first(), withLatestFrom(this.routerSelectors.url$, this.isProductFavorite$!))
            .subscribe(([isLogged, url, isProductFavorite]) => {
                if (isLogged) {
                    if (this.product) {
                        if (isProductFavorite) {
                            this.productsDispatchers.deleteLike(this.product.id);
                        } else {
                            this.productsDispatchers.like(this.product.id);
                        }
                    }
                } else {
                    this.dialog.open(GoToLoginDialogComponent, {
                        data: { redirectUrl: url },
                    });
                }
            });
    }

    getFullUrl(fileName: string): string {
        return this.imageService.getUrl(fileName);
    }
}
