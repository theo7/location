import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { IconsModule } from 'src/app/features/icons/icons.module';
import { DirectivesModule } from '../../directives/directives.module';
import { PipesModule } from '../../pipes/pipes.module';
import { AppCoreModule } from '../app-core/app-core.module';
import { AvatarModule } from '../avatar/avatar.module';
import { ButtonModule } from '../button/button.module';
import { ChipModule } from '../chip/chip.module';
import { ModeratedProductButtonModule } from '../moderated-product-button/moderated-product-button.module';
import { ProductCardComponent } from './product-card.component';

@NgModule({
    declarations: [ProductCardComponent],
    imports: [
        CommonModule,
        PipesModule,
        AvatarModule,
        MatProgressSpinnerModule,
        TranslateModule,
        MatButtonModule,
        MatIconModule,
        AppCoreModule,
        ButtonModule,
        ModeratedProductButtonModule,
        IconsModule,
        DirectivesModule,
        RouterModule,
        ChipModule,
    ],
    exports: [ProductCardComponent],
})
export class ProductCardModule {}
