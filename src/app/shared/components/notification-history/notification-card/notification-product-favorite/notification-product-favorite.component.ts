import { ChangeDetectionStrategy, Component } from '@angular/core';
import { NotificationCardComponent } from '../notification-card.component';

@Component({
    selector: 'app-notification-product-favorite',
    templateUrl: './notification-product-favorite.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NotificationProductFavoriteComponent extends NotificationCardComponent {}
