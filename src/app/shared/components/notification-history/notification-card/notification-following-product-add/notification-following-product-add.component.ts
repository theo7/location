import { ChangeDetectionStrategy, Component } from '@angular/core';
import { NotificationCardComponent } from '../notification-card.component';

@Component({
    selector: 'app-notification-following-product-add',
    templateUrl: './notification-following-product-add.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NotificationFollowingProductAddComponent extends NotificationCardComponent {}
