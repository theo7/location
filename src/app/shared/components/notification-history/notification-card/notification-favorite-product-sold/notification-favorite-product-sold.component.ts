import { ChangeDetectionStrategy, Component } from '@angular/core';
import { NotificationCardComponent } from '../notification-card.component';

@Component({
    selector: 'app-notification-favorite-product-sold',
    templateUrl: './notification-favorite-product-sold.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NotificationFavoriteProductSoldComponent extends NotificationCardComponent {}
