import { ChangeDetectionStrategy, Component } from '@angular/core';
import { NotificationCardComponent } from '../notification-card.component';

@Component({
    selector: 'app-notification-user-following',
    templateUrl: './notification-user-following.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NotificationUserFollowingComponent extends NotificationCardComponent {}
