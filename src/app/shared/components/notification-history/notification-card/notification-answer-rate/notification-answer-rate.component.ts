import { ChangeDetectionStrategy, Component } from '@angular/core';
import { NotificationCardComponent } from '../notification-card.component';

@Component({
    selector: 'app-notification-answer-rate',
    templateUrl: './notification-answer-rate.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NotificationAnswerRateComponent extends NotificationCardComponent {}
