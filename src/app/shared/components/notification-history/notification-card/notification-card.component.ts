import { Component, Input } from '@angular/core';
import { PushNotification } from 'src/app/shared/models/models';

@Component({
    template: '',
})
export abstract class NotificationCardComponent {
    @Input()
    pushNotification!: PushNotification;

    constructor() {}

    getInternalUrl(urlAsString: string): string {
        return new URL(urlAsString).pathname;
    }
}
