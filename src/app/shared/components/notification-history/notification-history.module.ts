import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { IconsModule } from 'src/app/features/icons/icons.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { DirectivesModule } from '../../directives/directives.module';
import { AvatarModule } from '../avatar/avatar.module';
import { NotificationAnswerRateComponent } from './notification-card/notification-answer-rate/notification-answer-rate.component';
import { NotificationFavoriteProductSoldComponent } from './notification-card/notification-favorite-product-sold/notification-favorite-product-sold.component';
import { NotificationFollowingProductAddComponent } from './notification-card/notification-following-product-add/notification-following-product-add.component';
import { NotificationProductFavoriteComponent } from './notification-card/notification-product-favorite/notification-product-favorite.component';
import { NotificationUserFollowingComponent } from './notification-card/notification-user-following/notification-user-following.component';
import { NotificationHistoryComponent } from './notification-history.component';

@NgModule({
    declarations: [
        NotificationHistoryComponent,
        NotificationProductFavoriteComponent,
        NotificationFavoriteProductSoldComponent,
        NotificationUserFollowingComponent,
        NotificationFollowingProductAddComponent,
        NotificationAnswerRateComponent,
    ],
    imports: [CommonModule, SharedModule, IconsModule, AvatarModule, DirectivesModule],
    exports: [NotificationHistoryComponent],
})
export class NotificationHistoryModule {}
