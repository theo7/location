import { Observable } from '@angular-devkit/core/node_modules/rxjs';
import { Component, Input, ViewEncapsulation } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { sortBy } from 'lodash-es';
import { NotificationsDispatchers } from 'src/app/store/services/notifications-dispatchers.service';
import { NotificationsSelectors } from 'src/app/store/services/notifications-selectors.service';
import { DeviceService } from '../../services/device.service';
import { PushNotification, SystemMessageTypeEnum } from './../../models/models';

@UntilDestroy()
@Component({
    selector: 'app-notification-history',
    templateUrl: './notification-history.component.html',
    styleUrls: ['./notification-history.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class NotificationHistoryComponent {
    @Input()
    isFocused: boolean = false;

    notifications?: PushNotification[];

    systemMessageType = SystemMessageTypeEnum;
    loading$: Observable<boolean>;

    trackByUuid = (index: number, pushNotification: PushNotification): string => pushNotification.uuid;

    constructor(
        public readonly device: DeviceService,
        private readonly notificationsSelectors: NotificationsSelectors,
        private readonly notificationsDispatchers: NotificationsDispatchers,
    ) {
        this.loading$ = this.notificationsSelectors.loading$;
        this.notificationsDispatchers.loadNotificationHistory();
        this.notificationsSelectors.getNotificationHistory$.pipe(untilDestroyed(this)).subscribe(notifications => {
            if (this.isFocused) {
                this.setNotificationRead();
            }
            const notificationsUnsorted: PushNotification[] = Object.entries(notifications ?? {}).map(
                ([_, notification]) => notification,
            );
            this.notifications = sortBy(notificationsUnsorted, n => -1 * n.timestamp);
        });
    }

    setNotificationRead() {
        this.notificationsDispatchers.setNotificationRead();
    }
}
