import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ConfirmDeclineSellDialogResult } from '../../models/models';

type ConfirmDeclineSellDialogComponentData = {
    lotId: number;
};

@Component({
    selector: 'app-confirm-decline-sell-dialog',
    templateUrl: './confirm-decline-sell-dialog.component.html',
    styleUrls: ['./confirm-decline-sell-dialog.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ConfirmDeclineSellDialogComponent {
    constructor(
        private readonly dialog: MatDialogRef<ConfirmDeclineSellDialogComponent, ConfirmDeclineSellDialogResult>,
        @Inject(MAT_DIALOG_DATA) private data: ConfirmDeclineSellDialogComponentData,
    ) {}

    onYesButtonClicked() {
        this.dialog.close({
            lotId: this.data.lotId,
        });
    }
}
