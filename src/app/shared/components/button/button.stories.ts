import { MatButtonModule } from '@angular/material/button';
import { MatRippleModule } from '@angular/material/core';
import { moduleMetadata } from '@storybook/angular';
import { ButtonComponent } from './button.component';

export default {
    title: 'Components/Button',
    component: ButtonComponent,
    decorators: [
        moduleMetadata({
            imports: [MatButtonModule, MatRippleModule],
            declarations: [ButtonComponent],
        }),
    ],
};

export const Button = () => ({
    component: ButtonComponent,
    props: {
        disabled: false,
    },

    template: `
<style type="text/css">
    table {
      margin: auto;
      width: 70%;
      border-collapse: collapse;
      border: 1px solid grey;
    }

    td {
      border-right: 1px solid grey;
      text-align: center;
      padding: 20px;
    }

    td:first-child {
      text-align: left;
      width: 20%;
    }
</style>

<table>
     <tr>
        <td></td>
        <td>Enabled</td>
        <td>Disabled</td>
    </tr>
    <tr>
        <td>Primary</td>
        <td><button kia-button variant="primary">Primary - enabled</button></td>
        <td><button kia-button variant="primary" disabled>Primary - disabled</button></td>
    </tr>
    <tr>
        <td>Secondary (No using)</td>
        <td><button kia-button variant="secondary">Secondary - enabled</button></td>
        <td><button kia-button variant="secondary" disabled>Secondary - disabled</button></td>
    </tr>
    <tr>
        <td>Bordered</td>
        <td><button kia-button variant="bordered">Bordered - enabled</button></td>
        <td><button kia-button variant="bordered" disabled>Bordered - disabled</button></td>
    </tr>
    <tr>
        <td>Success</td>
        <td><button kia-button variant="success">Success - enabled</button></td>
        <td><button kia-button variant="success" disabled>Success - disabled</button></td>
    </tr>
    <tr>
        <td>Warm</td>
        <td><button kia-button variant="warn">Warn - enabled</button></td>
        <td><button kia-button variant="warn" disabled>Warn - disabled</button></td>
    </tr>
    <tr>
        <td>Link</td>
        <td><button kia-button variant="link">Link - enabled</button></td>
        <td><button kia-button variant="link" disabled>Link - disabled</button></td>
    </tr>
</table>
    `,
});

Button.storyName = 'Buttons styles';
