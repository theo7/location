import { ChangeDetectionStrategy, Component, HostBinding, Input } from '@angular/core';

/* eslint-disable @angular-eslint/component-selector */
@Component({
    selector: 'button[kia-button], a[kia-button]',
    templateUrl: './button.component.html',
    styleUrls: ['./button.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ButtonComponent {
    @Input() disabled: boolean | '' | undefined = false;

    @HostBinding('attr.disabled')
    get isDisabled() {
        return this.disabled || this.disabled === '' || null;
    }

    @Input()
    @HostBinding('attr.data-variant')
    variant: 'primary' | 'secondary' | 'bordered' | 'bordered-bold' | 'white-bordered' | 'success' | 'warn' | 'link' =
        'primary';

    @Input()
    @HostBinding('attr.compact')
    compact: boolean | '' = false;
}
