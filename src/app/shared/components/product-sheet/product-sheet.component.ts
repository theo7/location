import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Actions, ofType } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { filter, first, map, withLatestFrom } from 'rxjs/operators';
import { ReportProductDialogComponent } from 'src/app/pages/product/report-product-dialog/report-product-dialog.component';
import { deleteLikeSuccess, likeSuccess } from 'src/app/store/actions/products.actions';
import { DiscussionsDispatchers } from 'src/app/store/services/discussions-dispatchers.service';
import { DiscussionsSelectors } from 'src/app/store/services/discussions-selectors.service';
import { ProductsDispatchers } from 'src/app/store/services/products-dispatchers.service';
import { RouterSelectors } from 'src/app/store/services/router-selectors.service';
import { SettingsDispatchers } from 'src/app/store/services/settings-dispatchers.service';
import { SettingsSelectors } from 'src/app/store/services/settings-selectors.service';
import { UserProfileSelectors } from '../../../store/services/user-selectors.service';
import { Product, ProductStatusEnum, UserProfileStatus } from '../../models/models';
import { DeviceService } from '../../services/device.service';
import { ProductsService } from '../../services/products.service';
import { ShareService } from '../../services/share.service';
import { GoToLoginDialogComponent } from '../go-to-login-dialog/go-to-login-dialog.component';

@UntilDestroy()
@Component({
    selector: 'app-product-sheet',
    templateUrl: './product-sheet.component.html',
    styleUrls: ['./product-sheet.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProductSheetComponent implements OnInit {
    canShareProduct = this.shareService.canShare();

    @Input() product?: Product;
    @Input() disableLinks = false;

    @ViewChild('content') content;

    ProductStatusEnum = ProductStatusEnum;
    UserProfileStatus = UserProfileStatus;

    isLogged$: Observable<boolean>;
    isCurrentUser$?: Observable<boolean>;
    isInLot$?: Observable<boolean | undefined>;
    hasCartForSellerId$?: Observable<boolean>;
    isProductFavorite$?: Observable<boolean>;
    loadingLikes = false;
    nbLikes?: number;

    constructor(
        private readonly userProfileSelectors: UserProfileSelectors,
        public readonly device: DeviceService,
        private readonly dialog: MatDialog,
        private readonly routerSelectors: RouterSelectors,
        private readonly productsDispatchers: ProductsDispatchers,
        private readonly actions$: Actions,
        private readonly shareService: ShareService,
        private readonly discussionsSelectors: DiscussionsSelectors,
        private readonly discussionsDispatchers: DiscussionsDispatchers,
        private readonly productsService: ProductsService,
        private cd: ChangeDetectorRef,
        private readonly settingsSelectors: SettingsSelectors,
        private readonly settingsDispatchers: SettingsDispatchers,
    ) {
        this.isLogged$ = userProfileSelectors.isLogged$.pipe(untilDestroyed(this));
    }

    ngOnInit() {
        this.isCurrentUser$ = this.userProfileSelectors
            .isCurrentUser$(this.product!.owner!.id!)
            .pipe(untilDestroyed(this));

        this.isInLot$ = this.discussionsSelectors.productIsInLot$(this.product!.id).pipe(untilDestroyed(this));

        this.hasCartForSellerId$ = this.discussionsSelectors
            .hasCartForSellerId$(this.product!.owner!.id!)
            .pipe(untilDestroyed(this));

        this.isProductFavorite$ = this.userProfileSelectors
            .isProductFavorite$(this.product!.id)
            .pipe(untilDestroyed(this));

        this.actions$
            .pipe(
                ofType(likeSuccess),
                filter(({ productId }) => productId === this.product?.id),
                untilDestroyed(this),
            )
            .subscribe(() => {
                if (this.nbLikes !== undefined) {
                    this.nbLikes++;
                }
            });

        this.actions$
            .pipe(
                ofType(deleteLikeSuccess),
                filter(({ productId }) => productId === this.product?.id),
                untilDestroyed(this),
            )
            .subscribe(() => {
                if (this.nbLikes !== undefined) {
                    this.nbLikes--;
                }
            });

        this.loadingLikes = true;
        this.productsService.getNbFavorites(this.product!.id).subscribe(
            nbLikes => {
                this.nbLikes = nbLikes;
                this.loadingLikes = false;
                this.cd.detectChanges();
            },
            () => {
                this.loadingLikes = false;
                this.cd.detectChanges();
            },
        );
        this.settingsDispatchers.getShippingFees();
    }

    onReportButtonClicked() {
        const dialogDeviceParams = device =>
            ({
                handset: {
                    width: '100vw',
                    maxWidth: '100vw',
                    height: '100vh',
                    panelClass: 'full-screen-dialog',
                },
                desktop: {
                    width: '90vw',
                    maxWidth: '700px',
                    minHeight: '50vh',
                    panelClass: 'no-padding-dialog',
                    disableClose: true,
                },
            }[device]);

        this.dialog.open(ReportProductDialogComponent, {
            ...dialogDeviceParams(this.device.mode),
        });
    }

    onLikeButtonClicked(e: Event) {
        e.stopPropagation();

        this.userProfileSelectors.isLogged$
            .pipe(first(), withLatestFrom(this.routerSelectors.url$, this.isProductFavorite$!))
            .subscribe(([isLogged, url, isProductFavorite]) => {
                if (isLogged) {
                    if (this.product) {
                        if (isProductFavorite) {
                            this.productsDispatchers.deleteLike(this.product.id);
                        } else {
                            this.productsDispatchers.like(this.product.id);
                        }
                    }
                } else {
                    this.dialog.open(GoToLoginDialogComponent, {
                        data: { redirectUrl: url },
                    });
                }
            });
    }

    onAddToLotButtonClicked() {
        if (this.product?.owner?.id) {
            this.discussionsSelectors
                .lotOpenBySellerId$(this.product.owner.id)
                .pipe(first())
                .subscribe(lot => {
                    if (lot?.id && this.product?.id) {
                        this.discussionsDispatchers.addProductToLot(lot.id, this.product.id);
                    }
                });
        }
    }

    onRemoveFromLotButtonClicked() {
        if (this.product?.owner?.id) {
            this.discussionsSelectors
                .lotOpenBySellerId$(this.product.owner.id)
                .pipe(first())
                .subscribe(lot => {
                    if (lot?.id && this.product?.id) {
                        this.discussionsDispatchers.removeProductFromLot(lot.id, this.product.id);
                    }
                });
        }
    }

    onShareButtonClicked() {
        if (this.product) {
            this.productsDispatchers.shareProduct(this.product);
        }
    }

    get transportFee$() {
        return this.settingsSelectors.shippingFees$.pipe(map(tFee => tFee?.fee));
    }
}
