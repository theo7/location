import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';
import { TranslateModule } from '@ngx-translate/core';
import { IconsModule } from 'src/app/features/icons/icons.module';
import { DirectivesModule } from '../../directives/directives.module';
import { PipesModule } from '../../pipes/pipes.module';
import { SharedModule } from '../../shared.module';
import { C2cRegulationModule } from '../c2c-regulation/c2c-regulation.module';
import { ProductBonusStickerModule } from '../product-bonus-sticker/product-bonus-sticker.module';
import { ProductCarouselModule } from '../product-carousel/product-carousel.module';
import { ProductDetailModule } from '../product-detail/product-detail.module';
import { ShippingPromoModule } from '../shipping-promo/shipping-promo.module';
import { ProductSheetComponent } from './product-sheet.component';

@NgModule({
    declarations: [ProductSheetComponent],
    imports: [
        SharedModule,
        CommonModule,
        TranslateModule,
        ShippingPromoModule,
        MatIconModule,
        ProductDetailModule,
        ProductCarouselModule,
        ProductBonusStickerModule,
        C2cRegulationModule,
        IconsModule,
        DirectivesModule,
        PipesModule,
    ],
    exports: [ProductSheetComponent],
})
export class ProductSheetModule {}
