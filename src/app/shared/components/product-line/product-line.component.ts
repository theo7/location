import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Product, ProductStatusEnum } from '../../models/models';

@Component({
    selector: 'app-product-line',
    templateUrl: './product-line.component.html',
    styleUrls: ['./product-line.component.scss'],
})
export class ProductLineComponent {
    @Input() product?: Product;
    @Input() showRemove = false;
    @Input() showArrow = true;
    @Input() compact = false;
    @Input() showPrice = true;
    @Input() isUnavailable = false;

    @Output() clicked = new EventEmitter<any>();
    @Output() removeProductEvent = new EventEmitter<any>();

    productStatusEnum = ProductStatusEnum;

    click() {
        this.clicked.emit();
    }

    removeFromLot() {
        this.removeProductEvent.emit();
    }
}
