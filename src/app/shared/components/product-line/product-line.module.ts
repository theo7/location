import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';
import { TranslateModule } from '@ngx-translate/core';
import { ImagesMiniModule } from '../images-mini/images-mini.module';
import { ProductLineComponent } from './product-line.component';

@NgModule({
    declarations: [ProductLineComponent],
    imports: [CommonModule, TranslateModule, MatIconModule, ImagesMiniModule],
    exports: [ProductLineComponent],
})
export class ProductLineModule {}
