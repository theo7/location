import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
    selector: 'app-unsave-changes-dialog',
    templateUrl: './unsave-changes-dialog.component.html',
    styleUrls: ['./unsave-changes-dialog.component.scss'],
})
export class UnsaveChangesDialogComponent {
    constructor(@Inject(MAT_DIALOG_DATA) public data: any) {}
}
