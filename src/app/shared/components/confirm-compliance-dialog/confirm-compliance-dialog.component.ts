import { Component, ViewEncapsulation } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { ConfirmationEnum } from '../../models/models';

@Component({
    selector: 'app-confirm-compliance-dialog',
    templateUrl: './confirm-compliance-dialog.component.html',
    styleUrls: ['./confirm-compliance-dialog.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class ConfirmComplianceDialogComponent {
    compliancy = new FormControl('', Validators.required);
    confirmationEnum = ConfirmationEnum;

    constructor(public dialogRef: MatDialogRef<ConfirmComplianceDialogComponent>) {}
}
