import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';

@Component({
    selector: 'app-go-to-login-dialog',
    templateUrl: './go-to-login-dialog.component.html',
    styleUrls: ['./go-to-login-dialog.component.scss'],
})
export class GoToLoginDialogComponent {
    private readonly redirectUrl = this.data.redirectUrl;

    constructor(
        @Inject(MAT_DIALOG_DATA) private readonly data: { redirectUrl: string },
        private readonly router: Router,
    ) {}

    navigateToLogin() {
        this.router.navigate(['/login'], { queryParams: { redirectUrl: this.redirectUrl } });
    }
}
