import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ConfirmCancelSellDialogResult } from '../../models/models';

type ConfirmCancelSellDialogComponentData = {
    lotId: number;
};

@Component({
    selector: 'app-confirm-cancel-sell-dialog',
    templateUrl: './confirm-cancel-sell-dialog.component.html',
    styleUrls: ['./confirm-cancel-sell-dialog.component.scss'],
})
export class ConfirmCancelSellDialogComponent {
    constructor(
        private readonly dialog: MatDialogRef<ConfirmCancelSellDialogComponent, ConfirmCancelSellDialogResult>,
        @Inject(MAT_DIALOG_DATA) private data: ConfirmCancelSellDialogComponentData,
    ) {}

    onYesButtonClicked() {
        this.dialog.close({
            lotId: this.data.lotId,
        });
    }
}
