import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
    selector: 'app-error-notification-dialog',
    templateUrl: './error-notification-dialog.component.html',
    styleUrls: ['./error-notification-dialog.component.scss'],
})
export class ErrorNotificationDialogComponent {
    constructor(
        @Inject(MAT_DIALOG_DATA)
        public readonly data: {
            title: string;
            content: string;
            buttonContent: string;
        },
    ) {}
}
