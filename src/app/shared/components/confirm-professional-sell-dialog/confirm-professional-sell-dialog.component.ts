import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { filter } from 'rxjs/operators';
import { ConfirmationEnum, Lot, PaymentType } from '../../models/models';
import { RefuseSellDialogComponent } from '../confirm-individual-sell-dialog/refuse-sell-dialog/refuse-sell-dialog.component';

@Component({
    selector: 'app-confirm-professional-sell-dialog',
    templateUrl: './confirm-professional-sell-dialog.component.html',
    styleUrls: ['./confirm-professional-sell-dialog.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ConfirmProfessionalSellDialogComponent {
    trackingNumber = new FormControl('', [Validators.required, Validators.pattern(/^[0-9]{8,}$/)]);

    lot: Lot;
    step = 0;

    constructor(
        public dialogRef: MatDialogRef<ConfirmProfessionalSellDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private dialog: MatDialog,
    ) {
        this.lot = this.data.lot;
    }

    get totalProductsWithNegociation() {
        return this.lot.paymentLines?.find(p => p.type === PaymentType.PRODUCT)?.amount ?? 0;
    }

    refuse() {
        this.dialog
            .open(RefuseSellDialogComponent, {
                maxWidth: '90vw',
                disableClose: true,
            })
            .afterClosed()
            .pipe(filter(e => e))
            .subscribe(() => {
                this.dialogRef.close({
                    confirmationEnum: ConfirmationEnum.REFUSE,
                    trackingNumber: undefined,
                });
            });
    }

    confirm() {
        this.dialogRef.close({
            confirmationEnum: ConfirmationEnum.CONFIRM,
            trackingNumber: this.trackingNumber.value,
        });
    }

    next() {
        this.step += 1;
    }

    previous() {
        this.step -= 1;
    }
}
