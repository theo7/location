import { Component, Inject, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UntilDestroy } from '@ngneat/until-destroy';
import {
    Discussion,
    DiscussionReportReason,
    PictureType,
    ReportUserDialogResult,
    UserProfile,
} from '../../models/models';
import { ChatService } from '../../services/chat.service';
import { UserService } from '../../services/user.service';

type ReportUserDialogComponentData = {
    discussion: Discussion;
};

@UntilDestroy()
@Component({
    selector: 'app-report-user-dialog',
    templateUrl: './report-user-dialog.component.html',
    styleUrls: ['./report-user-dialog.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class ReportUserDialogComponent {
    discussion: Discussion;
    otherUser?: UserProfile;
    avatarUrl?: string;
    reportForm: FormGroup;

    discussionReportReasons: string[];

    PictureType = PictureType;

    constructor(
        @Inject(MAT_DIALOG_DATA) private data: ReportUserDialogComponentData,
        private dialogRef: MatDialogRef<ReportUserDialogComponent>,
        private userService: UserService,
        private fb: FormBuilder,
        private readonly chatService: ChatService,
    ) {
        this.discussion = data.discussion;
        this.otherUser = this.discussion.currentUserBuyer ? this.discussion.seller : this.discussion.buyer;
        this.avatarUrl = this.otherUser && this.userService.getAvatarUrl(this.otherUser);
        this.discussionReportReasons = Object.keys(DiscussionReportReason);
        this.reportForm = this.fb.group({
            reason: ['', Validators.required],
            content: ['', Validators.required],
            pictures: [[]],
        });
    }

    validate() {
        if (this.discussion.id && this.reportForm.valid) {
            const result: ReportUserDialogResult = {
                discussionId: this.discussion.id,
                newDiscussionReport: this.reportForm.value,
            };
            this.dialogRef.close(result);
        } else {
            this.reportForm.markAsTouched();
        }
    }
}
