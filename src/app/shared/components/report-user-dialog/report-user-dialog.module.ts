import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { TranslateModule } from '@ngx-translate/core';
import { DirectivesModule } from '../../directives/directives.module';
import { PipesModule } from '../../pipes/pipes.module';
import { SharedModule } from '../../shared.module';
import { AppCoreModule } from '../app-core/app-core.module';
import { ButtonModule } from '../button/button.module';
import { ImagesMiniModule } from '../images-mini/images-mini.module';
import { ImageUploaderModule } from '../product-form/image-uploader/image-uploader.module';
import { ReportUserDialogComponent } from './report-user-dialog.component';

@NgModule({
    declarations: [ReportUserDialogComponent],
    imports: [
        CommonModule,
        ImagesMiniModule,
        TranslateModule,
        MatIconModule,
        PipesModule,
        DirectivesModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatInputModule,
        ImageUploaderModule,
        MatDialogModule,
        ButtonModule,
        MatButtonModule,
        AppCoreModule,
        SharedModule,
    ],
    exports: [ReportUserDialogComponent],
})
export class ReportUserDialogModule {}
