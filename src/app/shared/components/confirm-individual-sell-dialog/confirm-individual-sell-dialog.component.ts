import { Component, Inject, ViewEncapsulation } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { filter } from 'rxjs/operators';
import { environment } from '../../../../environments/environment';
import { ConfirmationEnum, Lot, PaymentType, RemunerationMode } from '../../models/models';
import { RefuseSellDialogComponent } from './refuse-sell-dialog/refuse-sell-dialog.component';

@Component({
    selector: 'app-confirm-individual-sell-dialog',
    templateUrl: './confirm-individual-sell-dialog.component.html',
    styleUrls: ['./confirm-individual-sell-dialog.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class ConfirmIndividualSellDialogComponent {
    remunerationMode = new FormControl('', Validators.required);
    remunerationModeEnum = RemunerationMode;
    lot: Lot;
    voucherAdditionRate: number;
    step = 0;

    constructor(
        public dialogRef: MatDialogRef<ConfirmIndividualSellDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private dialog: MatDialog,
    ) {
        this.lot = this.data.lot;
        this.voucherAdditionRate = environment.voucherAdditionRate;
    }

    get totalProductsWithNegociation() {
        return this.lot.paymentLines?.find(p => p.type === PaymentType.PRODUCT)?.amount ?? 0;
    }

    refuse() {
        this.dialog
            .open(RefuseSellDialogComponent, {
                maxWidth: '90vw',
                disableClose: true,
            })
            .afterClosed()
            .pipe(filter(e => e))
            .subscribe(() => {
                this.dialogRef.close({
                    confirmationEnum: ConfirmationEnum.REFUSE,
                    remunerationMode: undefined,
                });
            });
    }

    confirm() {
        this.dialogRef.close({
            confirmationEnum: ConfirmationEnum.CONFIRM,
            remunerationMode: this.remunerationMode.value,
        });
    }

    next() {
        this.step += 1;
    }

    previous() {
        this.step -= 1;
    }
}
