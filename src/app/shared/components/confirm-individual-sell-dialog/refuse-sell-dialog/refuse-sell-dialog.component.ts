import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
    selector: 'app-refuse-sell-dialog',
    templateUrl: './refuse-sell-dialog.component.html',
    styleUrls: ['./refuse-sell-dialog.component.scss'],
})
export class RefuseSellDialogComponent {
    constructor(@Inject(MAT_DIALOG_DATA) private data: any) {}
}
