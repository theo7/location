import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import {
    getHandlingFees,
    getPaidInCard,
    getPaidInCash,
    getShippingFees,
    getTotalPaidAfterRefund,
    getTotalProductsWithNegociation,
    getTransactionRefund,
} from '../../functions/transaction.functions';
import { Lot } from '../../models/models';

@Component({
    selector: 'app-payment-lines',
    templateUrl: './payment-lines.component.html',
    styleUrls: ['./payment-lines.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PaymentLinesComponent {
    @Input() lot?: Lot;
    @Input() recap = false;

    get shippingFees(): number {
        return this.lot ? getShippingFees(this.lot) : 0;
    }

    get handlingFees(): number {
        return this.lot ? getHandlingFees(this.lot) : 0;
    }

    get totalProductsWithNegociation(): number {
        return this.lot ? getTotalProductsWithNegociation(this.lot) : 0;
    }

    get refund(): number {
        return this.lot ? getTransactionRefund(this.lot) : 0;
    }

    get total(): number {
        return this.lot ? getTotalPaidAfterRefund(this.lot, true) : 0;
    }

    get cash(): number {
        return this.lot ? getPaidInCash(this.lot) : 0;
    }

    get leftToPay(): number {
        return this.lot ? getPaidInCard(this.lot) : 0;
    }
}
