import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatDividerModule } from '@angular/material/divider';
import { MatTooltipModule } from '@angular/material/tooltip';
import { TranslateModule } from '@ngx-translate/core';
import { PaymentLinesComponent } from './payment-lines.component';

@NgModule({
    declarations: [PaymentLinesComponent],
    imports: [CommonModule, MatDividerModule, TranslateModule, MatTooltipModule],
    exports: [PaymentLinesComponent],
})
export class PaymentLinesModule {}
