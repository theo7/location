import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ConfirmBlockUserDialogResult } from '../../models/models';

type ConfirmBlockUserDialogComponentData = {
    userId: number;
};

@Component({
    selector: 'app-confirm-block-user-dialog',
    templateUrl: './confirm-block-user-dialog.component.html',
    styleUrls: ['./confirm-block-user-dialog.component.scss'],
})
export class ConfirmBlockUserDialogComponent {
    constructor(
        private readonly dialog: MatDialogRef<ConfirmBlockUserDialogComponent, ConfirmBlockUserDialogResult>,
        @Inject(MAT_DIALOG_DATA) private data: ConfirmBlockUserDialogComponentData,
    ) {}

    onYesButtonClicked() {
        this.dialog.close({
            userId: this.data.userId,
        });
    }
}
