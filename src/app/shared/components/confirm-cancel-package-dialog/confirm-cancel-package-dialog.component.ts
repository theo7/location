import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ConfirmCancelPackageDialogResult } from '../../models/models';

type ConfirmCancelPackageDialogComponentData = {
    lotId: number;
};

@Component({
    selector: 'app-confirm-cancel-package-dialog',
    templateUrl: './confirm-cancel-package-dialog.component.html',
    styleUrls: ['./confirm-cancel-package-dialog.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ConfirmCancelPackageDialogComponent {
    constructor(
        private readonly dialog: MatDialogRef<ConfirmCancelPackageDialogComponent, ConfirmCancelPackageDialogResult>,
        @Inject(MAT_DIALOG_DATA) private data: ConfirmCancelPackageDialogComponentData,
    ) {}

    onYesButtonClicked() {
        this.dialog.close({
            lotId: this.data.lotId,
        });
    }
}
