import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { TranslateModule } from '@ngx-translate/core';
import { PipesModule } from '../../pipes/pipes.module';
import { AppCoreModule } from '../app-core/app-core.module';
import { ButtonModule } from '../button/button.module';
import { AddAddressDialogComponent } from './add-address-dialog.component';

@NgModule({
    declarations: [AddAddressDialogComponent],
    imports: [
        CommonModule,
        AppCoreModule,
        TranslateModule,
        MatButtonModule,
        MatDialogModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatInputModule,
        MatSlideToggleModule,
        MatProgressSpinnerModule,
        MatIconModule,
        ButtonModule,
        PipesModule,
    ],
    exports: [AddAddressDialogComponent],
})
export class AddAddressDialogModule {}

export { AddAddressDialogComponent };
