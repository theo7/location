import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { TranslateService } from '@ngx-translate/core';
import { filter, tap } from 'rxjs/operators';
import { environment } from '../../../../environments/environment';
import { UserProfileDispatchers } from '../../../store/services/user-dispatchers.service';
import { UserProfileSelectors } from '../../../store/services/user-selectors.service';
import { Address, UserProfile } from '../../models/models';

@UntilDestroy()
@Component({
    selector: 'app-add-address-dialog',
    templateUrl: './add-address-dialog.component.html',
    styleUrls: ['./add-address-dialog.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class AddAddressDialogComponent implements OnInit {
    contactEmail = environment.contactAddress;
    addAsDefaultAddress?: boolean;
    addressForm?: FormGroup;
    address?: Address;
    user?: UserProfile;
    loading?: boolean;

    private CP_REGEXP = '^(([0-8][0-9])|(9[0-5])|(2[ab]))[0-9]{3}$';

    constructor(
        private formBuilder: FormBuilder,
        private dialogRef: MatDialogRef<AddAddressDialogComponent>,
        @Inject(MAT_DIALOG_DATA) private data: any,
        private userDispatchers: UserProfileDispatchers,
        private userSelectors: UserProfileSelectors,
        private readonly translateService: TranslateService,
    ) {}

    ngOnInit() {
        this.addAsDefaultAddress = this.data?.addAsDefaultAddress;
        this.address = this.data.address;
        this.user = this.data?.user;
        this.initForm();
    }

    save() {
        if (this.addressForm?.valid) {
            if (!this.addressForm.get('addressName')?.value) {
                this.addressForm.get('addressName')?.setValue(this.generateDefaultAddressName());
            }
            const newAddress = this.addressForm.getRawValue();
            if (this.address) {
                this.userDispatchers.updateUserAddress(newAddress);
            } else {
                this.userDispatchers.postUserAddress(newAddress);
            }
            this.userSelectors.userProfileState$
                .pipe(
                    tap(state => (this.loading = state.loading)),
                    filter(state => !state.loading && !state.error),
                    untilDestroyed(this),
                )
                .subscribe(() => {
                    this.dialogRef.close(newAddress);
                });
        }
    }

    private initForm() {
        this.addressForm = this.formBuilder.group({
            line1: ['', [Validators.required, Validators.maxLength(30)]],
            line2: '',
            city: ['', Validators.required],
            zipCode: ['', [Validators.required, Validators.pattern(this.CP_REGEXP)]],
            country: [{ value: 'France', disabled: true }],
            countryCode: 'FR',
            isDefault: [true, Validators.required],
            addressName: [''],
            firstName: [this.user?.firstName, Validators.required],
            lastName: [this.user?.lastName, Validators.required],
        });
        // updating an existing field
        if (this.address) {
            this.addressForm = this.formBuilder.group({
                ...this.addressForm.controls,
                id: ['', Validators.required],
                country: [{ value: 'France', disabled: true }],
                countryCode: 'FR',
            });
            this.addressForm.patchValue(this.address);
        }
    }

    private generateDefaultAddressName(): string {
        let index = 0;
        let matchFound: boolean | undefined;
        let name: string;
        do {
            index++;
            name = this.translateService.instant('profile.addAddressDialog.defaultAddressName', { index: index });
            matchFound = this.user?.addresses?.some(a => a.addressName.trim() === name);
        } while (matchFound);
        return name;
    }
}
