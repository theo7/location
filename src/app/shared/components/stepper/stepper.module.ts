import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { StepperComponent } from './stepper.component';

@NgModule({
    declarations: [StepperComponent],
    imports: [CommonModule, TranslateModule],
    exports: [StepperComponent],
})
export class StepperModule {}
