import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Step } from '../../models/models';

@Component({
    selector: 'app-stepper',
    templateUrl: './stepper.component.html',
    styleUrls: ['./stepper.component.scss'],
})
export class StepperComponent {
    @Output() stepClicked = new EventEmitter<number>();

    @Input() steps: Step[] = [];
    @Input() activeStep = 0;
    @Input() doneSteps = 0;

    clicked(index) {
        this.stepClicked.emit(index);
    }
}
