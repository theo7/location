import { Component, Input, ViewEncapsulation } from '@angular/core';

@Component({
    selector: 'app-shipping-promo',
    templateUrl: './shipping-promo.component.html',
    styleUrls: ['./shipping-promo.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class ShippingPromoComponent {
    @Input() light = false;
    @Input() content?: string;
}
