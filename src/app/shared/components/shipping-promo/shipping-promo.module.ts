import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MondialRelayIconComponent } from './mondial-relay-icon/mondial-relay-icon.component';
import { ShippingPromoComponent } from './shipping-promo.component';

@NgModule({
    declarations: [ShippingPromoComponent, MondialRelayIconComponent],
    imports: [CommonModule],
    exports: [ShippingPromoComponent],
})
export class ShippingPromoModule {}
