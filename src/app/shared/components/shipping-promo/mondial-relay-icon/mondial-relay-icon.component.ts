import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
    selector: 'app-mondial-relay-icon',
    templateUrl: './mondial-relay-icon.component.html',
    styleUrls: ['./mondial-relay-icon.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MondialRelayIconComponent {
    @Input() width = 35;
    @Input() height = 36;
}
