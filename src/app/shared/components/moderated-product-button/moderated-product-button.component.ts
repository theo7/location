import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Product } from '../../models/models';

@Component({
    selector: 'app-moderated-product-button',
    templateUrl: './moderated-product-button.component.html',
    styleUrls: ['./moderated-product-button.component.scss'],
})
export class ModeratedProductButtonComponent {
    @Input() product?: Product;

    constructor(private readonly router: Router) {}

    modifyProduct(event: MouseEvent) {
        event.stopPropagation();

        if (this.product) {
            this.router.navigate([`/product/${this.product.id}/update`]);
        }
    }
}
