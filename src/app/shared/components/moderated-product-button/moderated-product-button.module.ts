import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { TranslateModule } from '@ngx-translate/core';
import { ButtonModule } from '../button/button.module';
import { ModeratedProductButtonComponent } from './moderated-product-button.component';

@NgModule({
    declarations: [ModeratedProductButtonComponent],
    imports: [TranslateModule, MatButtonModule, ButtonModule],
    exports: [ModeratedProductButtonComponent],
})
export class ModeratedProductButtonModule {}
