import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';
import { filter, first, map, switchMap } from 'rxjs/operators';
import { suggestBranchDialogComponent$ } from 'src/app/imports.dynamic';
import { ProductsSelectors } from 'src/app/store/services/products-selectors.service';
import { RouterSelectors } from 'src/app/store/services/router-selectors.service';
import { UserProfileSelectors } from 'src/app/store/services/user-selectors.service';
import { environment } from 'src/environments/environment';
import { Product, SearchRouterParams } from '../../models/models';
import { DeviceService } from '../../services/device.service';

@UntilDestroy()
@Component({
    selector: 'app-product-detail',
    templateUrl: './product-detail.component.html',
    styleUrls: ['./product-detail.component.scss'],
})
export class ProductDetailComponent implements OnInit, OnChanges {
    displayProductViews = environment.displayProductViews;
    otherBrandId = environment.otherBrandId;

    @Input() product?: Product;
    @Input() disableLinks = false;

    color?: string;

    viewsLoading$: Observable<boolean>;
    views$: Observable<number | undefined>;
    isLogged$: Observable<boolean>;

    constructor(
        private readonly router: Router,
        private readonly translateService: TranslateService,
        private readonly deviceService: DeviceService,
        userProfileSelectors: UserProfileSelectors,
        productsSelectors: ProductsSelectors,
        private readonly dialog: MatDialog,
        private readonly snackBar: MatSnackBar,
        private readonly routerSelectors: RouterSelectors,
    ) {
        this.viewsLoading$ = productsSelectors.viewsLoading$.pipe(untilDestroyed(this));
        this.views$ = productsSelectors.views$.pipe(untilDestroyed(this));
        this.isLogged$ = userProfileSelectors.isLogged$.pipe(untilDestroyed(this));
    }

    ngOnInit() {
        this.updateView();
    }

    ngOnChanges() {
        this.updateView();
    }

    updateView() {
        this.color = this.product?.color.map(c => this.translateService.instant(`color.${c.labelKey}`)).join(', ');
    }

    search(prefix: 'brands' | 'category' | 'subCategory' | 'productType'): Promise<boolean> {
        if (this.disableLinks) {
            return Promise.resolve(false);
        }

        if (prefix === 'brands' && this.product?.brand.slug) {
            return this.router.navigate(['marque', this.product.brand.slug]);
        }

        const filter: Partial<SearchRouterParams> = {};
        switch (prefix) {
            case 'category':
                filter.category = this.product?.productType?.subCategory?.category?.id;
                break;
            case 'subCategory':
                filter.category = this.product?.productType?.subCategory?.category?.id;
                filter.subCategory = this.product?.productType?.subCategory?.id;
                break;
            case 'productType':
                filter.category = this.product?.productType?.subCategory?.category?.id;
                filter.subCategory = this.product?.productType?.subCategory?.id;
                filter.productsTypes = String(this.product?.productType?.id);
                break;
        }
        return this.router.navigate(['search'], { queryParams: filter });
    }

    onSuggestBrandButtonClicked(event: Event): void {
        event.preventDefault();
        event.stopPropagation();

        const dialogDeviceParams = device =>
            ({
                handset: {
                    width: '100vw',
                    maxWidth: '100vw',
                    height: '100vh',
                    panelClass: 'full-screen-dialog',
                },
                desktop: {
                    width: '400px',
                    maxWidth: '400px',
                },
            }[device]);

        this.routerSelectors.productId$
            .pipe(
                first(),
                switchMap(productId => {
                    return suggestBranchDialogComponent$.pipe(
                        map(
                            SuggestBranchDialogComponent =>
                                [productId, SuggestBranchDialogComponent] as [
                                    number | undefined,
                                    typeof SuggestBranchDialogComponent,
                                ],
                        ),
                    );
                }),
                switchMap(([productId, SuggestBranchDialogComponent]) => {
                    return this.dialog
                        .open(SuggestBranchDialogComponent, {
                            data: {
                                productId,
                            },
                            ...dialogDeviceParams(this.deviceService.mode),
                        })
                        .afterClosed();
                }),
                filter(result => !!result),
            )
            .subscribe(_ => {
                this.snackBar.open(this.translateService.instant('suggestBrand.notification.success'));
            });
    }
}
