import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
    selector: 'app-notification-dialog',
    templateUrl: './notification-dialog.component.html',
    styleUrls: ['./notification-dialog.component.scss'],
})
export class NotificationDialogComponent implements OnInit {
    type: 'success' | 'warning' = 'warning';
    title?: string;
    content?: string;
    buttonContent?: string;
    backButtonEnable = false;

    constructor(@Inject(MAT_DIALOG_DATA) private data: any) {}

    ngOnInit() {
        this.type = this.data.type;
        this.title = this.data.title;
        this.content = this.data.content;
        this.buttonContent = this.data.buttonContent;
        this.backButtonEnable = this.data.backButtonEnable;
    }
}
