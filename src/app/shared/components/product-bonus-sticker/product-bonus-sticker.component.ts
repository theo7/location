import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { environment } from '../../../../environments/environment';

@Component({
    selector: 'app-product-bonus-sticker',
    templateUrl: './product-bonus-sticker.component.html',
    styleUrls: ['./product-bonus-sticker.component.scss'],
})
export class ProductBonusStickerComponent implements OnChanges {
    @Input() price = 0;

    bonusPercentage = environment.voucherAdditionRate;
    bonus = 0;

    ngOnChanges(changes: SimpleChanges) {
        this.bonus = this.price * this.bonusPercentage;
    }
}
