import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { ProductBonusStickerComponent } from './product-bonus-sticker.component';

@NgModule({
    declarations: [ProductBonusStickerComponent],
    imports: [CommonModule, TranslateModule],
    exports: [ProductBonusStickerComponent],
})
export class ProductBonusStickerModule {}
