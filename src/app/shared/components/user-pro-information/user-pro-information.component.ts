import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
    selector: 'app-user-pro-information',
    templateUrl: './user-pro-information.component.html',
    styleUrls: ['./user-pro-information.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserProInformationComponent {}
