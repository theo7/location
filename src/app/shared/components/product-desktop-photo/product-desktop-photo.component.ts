import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Actions, ofType } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { filter, first, map, withLatestFrom } from 'rxjs/operators';
import { ReportProductDialogComponent } from 'src/app/pages/product/report-product-dialog/report-product-dialog.component';
import { deleteLikeSuccess, likeSuccess } from 'src/app/store/actions/products.actions';
import { ProductsDispatchers } from 'src/app/store/services/products-dispatchers.service';
import { RouterSelectors } from 'src/app/store/services/router-selectors.service';
import { UserProfileSelectors } from 'src/app/store/services/user-selectors.service';
import { Picture, Product, ProductStatusEnum, UserType } from '../../models/models';
import { DeviceService } from '../../services/device.service';
import { ImageService } from '../../services/image.service';
import { ProductsService } from '../../services/products.service';
import { GoToLoginDialogComponent } from '../go-to-login-dialog/go-to-login-dialog.component';
import { ImageFullscreenDialogComponent } from '../product-carousel/image-fullscreen-dialog/image-fullscreen-dialog.component';

@UntilDestroy()
@Component({
    selector: 'app-product-desktop-photo',
    templateUrl: './product-desktop-photo.component.html',
    styleUrls: ['./product-desktop-photo.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProductDesktopPhotoComponent implements OnChanges {
    @Input() product?: Product;

    ProductStatusEnum = ProductStatusEnum;
    UserType = UserType;

    isLogged$: Observable<boolean>;
    viewHisProduct$?: Observable<boolean>;
    isProductFavorite$?: Observable<boolean>;

    pictures: Picture[] = [];
    selectedIndex = 0;
    selectedPicture?: Picture;
    selectedPictureUrl?: string;
    isProductUnavailable?: boolean;

    isSelectedPictureLoading = true;
    loadingLikes = false;
    nbLikes = 0;

    constructor(
        private readonly imageService: ImageService,
        private readonly dialog: MatDialog,
        private readonly device: DeviceService,
        private readonly userProfileSelectors: UserProfileSelectors,
        private readonly routerSelectors: RouterSelectors,
        private readonly productsDispatchers: ProductsDispatchers,
        private readonly actions$: Actions,
        private readonly productsService: ProductsService,
        private readonly cd: ChangeDetectorRef,
    ) {
        this.isLogged$ = userProfileSelectors.isLogged$.pipe(untilDestroyed(this));
    }

    ngOnChanges(changes: SimpleChanges) {
        this.viewHisProduct$ = this.userProfileSelectors.userProfile$.pipe(
            map(u => !!u && this.product?.owner.id === u.id),
            untilDestroyed(this),
        );

        this.isProductFavorite$ = this.userProfileSelectors
            .isProductFavorite$(this.product!.id)
            .pipe(untilDestroyed(this));

        this.actions$
            .pipe(
                ofType(likeSuccess),
                filter(({ productId }) => productId === this.product?.id),
                untilDestroyed(this),
            )
            .subscribe(() => {
                this.nbLikes++;
            });
        this.actions$
            .pipe(
                ofType(deleteLikeSuccess),
                filter(({ productId }) => productId === this.product?.id),
                untilDestroyed(this),
            )
            .subscribe(() => {
                this.nbLikes--;
            });

        this.pictures =
            this.product?.pictures?.map(p => {
                return {
                    ...p,
                    loading: true,
                };
            }) || [];
        this.isProductUnavailable = this.product?.productStatus === ProductStatusEnum.UNAVAILABLE;

        this.selectedIndex = 0;
        this.updateSelectedPicture(this.pictures[this.selectedIndex]);

        this.loadingLikes = true;
        this.productsService.getNbFavorites(this.product!.id).subscribe(
            nbLikes => {
                this.nbLikes = nbLikes;
                this.loadingLikes = false;
                this.cd.detectChanges();
            },
            () => {
                this.loadingLikes = false;
                this.cd.detectChanges();
            },
        );
    }

    onImageLoaded(picture?: Picture) {
        if (picture) {
            picture.loading = false;
        } else {
            this.isSelectedPictureLoading = false;
        }
    }

    getFileUrl(fileName: string) {
        return this.imageService.getUrl(fileName);
    }

    onPictureClicked(picture: Picture, index: number) {
        this.updateSelectedPicture(picture);
        this.selectedIndex = index;
    }

    onReportButtonClicked() {
        const dialogDeviceParams = device =>
            ({
                handset: {
                    width: '100vw',
                    maxWidth: '100vw',
                    height: '100vh',
                    panelClass: 'full-screen-dialog',
                },
                desktop: {
                    width: '90vw',
                    maxWidth: '700px',
                    panelClass: 'no-padding-dialog',
                    disableClose: true,
                },
            }[device]);

        this.dialog.open(ReportProductDialogComponent, {
            ...dialogDeviceParams(this.device.mode),
        });
    }

    onLikeButtonClicked(e: Event) {
        e.stopPropagation();

        this.userProfileSelectors.isLogged$
            .pipe(first(), withLatestFrom(this.routerSelectors.url$, this.isProductFavorite$!))
            .subscribe(([isLogged, url, isProductFavorite]) => {
                if (isLogged) {
                    if (this.product) {
                        if (isProductFavorite) {
                            this.productsDispatchers.deleteLike(this.product.id);
                        } else {
                            this.productsDispatchers.like(this.product.id);
                        }
                    }
                } else {
                    this.dialog.open(GoToLoginDialogComponent, {
                        data: { redirectUrl: url },
                    });
                }
            });
    }

    openImageInFullScreen(picture?: Picture) {
        if (this.product && picture) {
            this.dialog.open(ImageFullscreenDialogComponent, {
                width: '600px',
                maxWidth: '600px',
                height: '100vh',
                panelClass: 'transparent-dialog-container',
                backdropClass: 'darker-backdrop',
                data: {
                    pictures: this.product.pictures,
                    index: this.product.pictures.findIndex(p => p.id === picture.id),
                },
            });
        }
    }

    updateSelectedPicture(picture: Picture) {
        this.selectedPicture = picture;
        this.selectedPictureUrl = this.getFileUrl(this.selectedPicture?.fileName!);
    }
}
