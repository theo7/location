import { Component, HostBinding, Input } from '@angular/core';

@Component({
    selector: 'app-report-product-icon',
    templateUrl: './report-product-icon.component.html',
    styleUrls: ['./report-product-icon.component.scss'],
})
export class ReportProductIconComponent {
    @Input() width = 50;
    @Input() height = 50;
    @Input() variant?: 'light';
    @Input() hasShadow = true;

    @HostBinding('class') get class() {
        const variantClasses: string[] = this.variant ? [this.variant] : [];
        const shadowClasses = this.hasShadow ? ['shadow'] : [];

        return variantClasses.concat(shadowClasses);
    }
}
