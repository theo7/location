import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { IconsModule } from 'src/app/features/icons/icons.module';
import { DirectivesModule } from '../../directives/directives.module';
import { SharedModule } from '../../shared.module';
import { C2cRegulationModule } from '../c2c-regulation/c2c-regulation.module';
import { ProductBonusStickerModule } from '../product-bonus-sticker/product-bonus-sticker.module';
import { ProductDesktopPhotoComponent } from './product-desktop-photo.component';
import { ReportProductIconComponent } from './report-product-icon/report-product-icon.component';

@NgModule({
    declarations: [ProductDesktopPhotoComponent, ReportProductIconComponent],
    imports: [
        SharedModule,
        CommonModule,
        MatProgressSpinnerModule,
        DirectivesModule,
        ProductBonusStickerModule,
        IconsModule,
        C2cRegulationModule,
    ],
    exports: [ProductDesktopPhotoComponent],
})
export class ProductDesktopPhotoModule {}
