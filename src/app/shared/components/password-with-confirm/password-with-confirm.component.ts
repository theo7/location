import { Component, ElementRef, forwardRef, Input, OnInit, ViewChild } from '@angular/core';
import {
    AbstractControl,
    ControlValueAccessor,
    FormBuilder,
    FormGroup,
    NG_VALIDATORS,
    NG_VALUE_ACCESSOR,
    ValidationErrors,
    Validator,
    Validators,
} from '@angular/forms';
import { CustomValidators } from '../../helpers/custom-validators';

@Component({
    selector: 'app-password-with-confirm',
    templateUrl: './password-with-confirm.component.html',
    styleUrls: ['./password-with-confirm.component.scss'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => PasswordWithConfirmComponent),
            multi: true,
        },
        {
            provide: NG_VALIDATORS,
            useExisting: forwardRef(() => PasswordWithConfirmComponent),
            multi: true,
        },
    ],
})
export class PasswordWithConfirmComponent implements OnInit, ControlValueAccessor, Validator {
    showPassword = false;
    showPasswordConfirmation = false;
    form?: FormGroup;

    @Input() resettingPassword = false;

    @ViewChild('passwordInputEl') passwordInputEl?: ElementRef;
    @ViewChild('passwordConfirmationInputEl') passwordConfirmationInputEl?: ElementRef;

    constructor(private formBuilder: FormBuilder) {}

    ngOnInit() {
        const passwordValidationRegExp = new RegExp('(?=.{10,})(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])');
        this.form = this.formBuilder.group(
            {
                value: ['', [Validators.required, Validators.pattern(passwordValidationRegExp)]],
                confirmValue: ['', [Validators.required]],
            },
            {
                validator: CustomValidators.mustMatch('value', 'confirmValue'),
            },
        );
    }

    onTouched: () => void = () => {};

    writeValue(value: any) {
        if (!value) return;

        if (this.form) {
            this.form.setValue(value, { emitEvent: false });
        }
    }

    registerOnTouched(fn: () => void): void {
        this.onTouched = fn;
    }

    registerOnChange(fn: (_: any) => void): void {
        if (this.form) {
            this.form.valueChanges.subscribe(fn);
        }
    }

    validate(c: AbstractControl): ValidationErrors | null {
        return this.form?.valid
            ? null
            : {
                  password: {
                      valid: false,
                      message: 'password fields are invalid',
                  },
              };
    }

    showOrHidePassword() {
        this.showPassword = !this.showPassword;

        if (this.passwordInputEl?.nativeElement) {
            if (this.showPassword) {
                this.passwordInputEl.nativeElement.removeAttribute('type');
            } else {
                this.passwordInputEl.nativeElement.setAttribute('type', 'password');
            }
        }
    }

    showOrHidePasswordConfirmation() {
        this.showPasswordConfirmation = !this.showPasswordConfirmation;

        if (this.passwordConfirmationInputEl?.nativeElement) {
            if (this.showPasswordConfirmation) {
                this.passwordConfirmationInputEl.nativeElement.removeAttribute('type');
            } else {
                this.passwordConfirmationInputEl.nativeElement.setAttribute('type', 'password');
            }
        }
    }
}
