import { Component, Input, OnChanges } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { UserProfileSelectors } from '../../../store/services/user-selectors.service';
import { ModerationStatus, Picture } from '../../models/models';
import { ImageService } from '../../services/image.service';

@UntilDestroy()
@Component({
    selector: 'app-avatar',
    templateUrl: './avatar.component.html',
    styleUrls: ['./avatar.component.scss'],
})
export class AvatarComponent implements OnChanges {
    @Input() avatar?: Picture;
    @Input() diameter = 35;
    @Input() shape: 'round' | 'square' = 'round';

    pictureUrl$?: Observable<string>;

    constructor(
        private readonly imageService: ImageService,
        private readonly userProfileSelectors: UserProfileSelectors,
    ) {}

    ngOnChanges(): void {
        this.pictureUrl$ = this.userProfileSelectors.userProfile$.pipe(
            map(userProfile => {
                // static avatar file
                if (this.avatar?.static) {
                    return this.imageService.getStaticUrl(this.avatar.fileName);
                }

                const currentUserAvatar = !!userProfile && userProfile.picture?.id === this.avatar?.id;
                const noStatusPresent = this.avatar && !this.avatar.status;
                const displayImage =
                    this.avatar?.fileName &&
                    (currentUserAvatar || this.avatar.status === ModerationStatus.OK || noStatusPresent);
                return displayImage ? this.imageService.getUrl(this.avatar?.fileName) : '';
            }),
            untilDestroyed(this),
        );
    }
}
