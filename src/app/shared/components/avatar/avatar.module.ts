import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DirectivesModule } from '../../directives/directives.module';
import { AvatarComponent } from './avatar.component';

@NgModule({
    declarations: [AvatarComponent],
    imports: [CommonModule, DirectivesModule],
    exports: [AvatarComponent],
})
export class AvatarModule {}
