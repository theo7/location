import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Observable } from 'rxjs';
import { first, map } from 'rxjs/operators';
import { DiscussionsDispatchers } from 'src/app/store/services/discussions-dispatchers.service';
import { DiscussionsSelectors } from 'src/app/store/services/discussions-selectors.service';
import { LotDispatchers } from 'src/app/store/services/lot-dispatchers.service';
import { ProductsDispatchers } from 'src/app/store/services/products-dispatchers.service';
import { ProductsSelectors } from 'src/app/store/services/products-selectors.service';
import { SettingsDispatchers } from 'src/app/store/services/settings-dispatchers.service';
import { SettingsSelectors } from 'src/app/store/services/settings-selectors.service';
import { UserProfileSelectors } from '../../../store/services/user-selectors.service';
import { Product, ProductStatusEnum, UserProfileStatus, UserType } from '../../models/models';
import { ShareService } from '../../services/share.service';

@UntilDestroy()
@Component({
    selector: 'app-product-desktop-description',
    templateUrl: './product-desktop-description.component.html',
    styleUrls: ['./product-desktop-description.component.scss'],
})
export class ProductDesktopDescriptionComponent implements OnInit, OnChanges {
    canShareProduct = this.shareService.canShare();

    @Output() closeEvent = new EventEmitter<void>();

    @Input() product?: Product;
    @Input() disableLinks = false;
    @Input() isInLot = false;
    @Input() displayCancelLotButton = false;
    @Input() creatingLot = false;

    ProductStatusEnum = ProductStatusEnum;
    UserType = UserType;
    UserProfileStatus = UserProfileStatus;

    isCurrentUser$?: Observable<boolean>;
    hasMoreThanOneProductInDressing$?: Observable<boolean>;
    hasCartForSellerId$?: Observable<boolean>;
    canCreateLot$?: Observable<boolean>;
    canAddToLot$?: Observable<boolean>;
    canRemoveFromLot$?: Observable<boolean>;

    constructor(
        private readonly userProfileSelectors: UserProfileSelectors,
        private readonly productsSelectors: ProductsSelectors,
        private readonly shareService: ShareService,
        private readonly lotDispatchers: LotDispatchers,
        private readonly productsDispatchers: ProductsDispatchers,
        private readonly discussionsSelectors: DiscussionsSelectors,
        private readonly discussionsDispatchers: DiscussionsDispatchers,
        private readonly settingsSelectors: SettingsSelectors,
        private readonly settingsDispatchers: SettingsDispatchers,
    ) {}

    ngOnInit(): void {
        this.hasMoreThanOneProductInDressing$ = this.productsSelectors.hasMoreThanOneProductInDressing$.pipe(
            untilDestroyed(this),
        );
        this.settingsDispatchers.getShippingFees();
    }

    ngOnChanges(): void {
        this.isCurrentUser$ = this.userProfileSelectors
            .isCurrentUser$(this.product!.owner.id!)
            .pipe(untilDestroyed(this));

        this.hasCartForSellerId$ = this.discussionsSelectors
            .hasCartForSellerId$(this.product!.owner.id!)
            .pipe(untilDestroyed(this));

        this.canCreateLot$ = this.discussionsSelectors
            .canCreateLot$(this.product!.owner.id!)
            .pipe(untilDestroyed(this));
        this.canAddToLot$ = this.discussionsSelectors
            .canAddToLot$(this.product!.owner.id!, this.product!.id)
            .pipe(untilDestroyed(this));
        this.canRemoveFromLot$ = this.discussionsSelectors
            .canRemoveFromLot$(this.product!.owner.id!, this.product!.id)
            .pipe(untilDestroyed(this));
    }

    startDiscussion(): void {
        if (this.product) {
            this.lotDispatchers.startDiscussion(this.product.id);
        }
    }

    buy(): void {
        if (this.product) {
            this.lotDispatchers.startPaymentProcess(this.product.id);
        }
    }

    createLot(): void {
        if (this.product) {
            this.lotDispatchers.createLot(this.product);
        }
    }

    close(): void {
        this.closeEvent.emit();
    }

    delete(): void {
        if (this.product) {
            this.productsDispatchers.deleteProduct(this.product.id);
        }
    }

    onAddToLotButtonClicked(): void {
        if (this.product?.owner.id) {
            this.discussionsSelectors
                .lotOpenBySellerId$(this.product.owner.id)
                .pipe(first())
                .subscribe(lot => {
                    if (lot?.id && this.product) {
                        this.discussionsDispatchers.addProductToLot(lot.id, this.product.id);
                    }
                });
        }
    }

    onRemoveFromLotButtonClicked(): void {
        if (this.product?.owner.id) {
            this.discussionsSelectors
                .lotOpenBySellerId$(this.product.owner.id)
                .pipe(first())
                .subscribe(lot => {
                    if (lot?.id && this.product) {
                        this.discussionsDispatchers.removeProductFromLot(lot.id, this.product.id);
                    }
                });
        }
    }

    onShareButtonClicked(): void {
        if (this.product) {
            this.productsDispatchers.shareProduct(this.product);
        }
    }

    get transportFee$(): Observable<number> {
        return this.settingsSelectors.shippingFees$.pipe(map(tFee => tFee?.fee || 0));
    }
}
