import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { DirectivesModule } from '../../directives/directives.module';
import { PipesModule } from '../../pipes/pipes.module';
import { SharedModule } from '../../shared.module';
import { AvatarModule } from '../avatar/avatar.module';
import { ButtonModule } from '../button/button.module';
import { C2cRegulationModule } from '../c2c-regulation/c2c-regulation.module';
import { ChipModule } from '../chip/chip.module';
import { ProductDetailModule } from '../product-detail/product-detail.module';
import { ShippingPromoModule } from '../shipping-promo/shipping-promo.module';
import { UserBlockedWarningModule } from '../user-blocked-warning/user-blocked-warning.module';
import { RatingModule } from '../user-rating/rating.module';
import { ProductDesktopDescriptionComponent } from './product-desktop-description.component';

@NgModule({
    declarations: [ProductDesktopDescriptionComponent],
    imports: [
        CommonModule,
        AvatarModule,
        MatIconModule,
        TranslateModule,
        RouterModule,
        ProductDetailModule,
        ButtonModule,
        ShippingPromoModule,
        SharedModule,
        RatingModule,
        DirectivesModule,
        UserBlockedWarningModule,
        ChipModule,
        PipesModule,
        C2cRegulationModule,
    ],
    exports: [ProductDesktopDescriptionComponent],
})
export class ProductDesktopDescriptionModule {}
