import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FilterType } from '../../../models/models';

@Component({
    selector: 'app-product-filter-dialog',
    templateUrl: './product-filter-dialog.component.html',
    styleUrls: ['./product-filter-dialog.component.scss'],
})
export class ProductFilterDialogComponent {
    constructor(@Inject(MAT_DIALOG_DATA) readonly data: { key: FilterType }) {}
}
