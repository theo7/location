import { OverlayModule } from '@angular/cdk/overlay';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatRadioModule } from '@angular/material/radio';
import { MatStepperModule } from '@angular/material/stepper';
import { MatTabsModule } from '@angular/material/tabs';
import { MatTooltipModule } from '@angular/material/tooltip';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { VirtualScrollerModule } from 'ngx-virtual-scroller';
import { IconsModule } from '../../../features/icons/icons.module';
import { AppCoreModule } from '../app-core/app-core.module';
import { ButtonModule } from '../button/button.module';
import { ColorPreviewModule } from '../color-preview/color-preview.module';
import { FilterItemNonSelectableComponent } from './components/common/filter-item-non-selectable/filter-item-non-selectable.component';
import { FilterItemSelectableComponent } from './components/common/filter-item-selectable/filter-item-selectable.component';
import { FilterBrandsComponent } from './components/filter-brands/filter-brands.component';
import { FilterCategoriesComponent } from './components/filter-categories/filter-categories.component';
import { FilterColorsComponent } from './components/filter-colors/filter-colors.component';
import { FilterConditionsComponent } from './components/filter-conditions/filter-conditions.component';
import { FilterPriceRangeComponent } from './components/filter-price-range/filter-price-range.component';
import { FilterSearchComponent } from './components/filter-search/filter-search.component';
import { FilterSeasonalityComponent } from './components/filter-seasonality/filter-seasonality.component';
import { FilterSizesComponent } from './components/filter-sizes/filter-sizes.component';
import { FilterSubCategoriesComponent } from './components/filter-subcategories/filter-sub-categories.component';
import { FilterTypesComponent } from './components/filter-types/filter-types.component';
import { SearchCardComponent } from './components/search-history/search-card/search-card.component';
import { SearchHistoryComponent } from './components/search-history/search-history.component';
import { FilterCategoryDesktopComponent } from './product-filter-desktop/filter-category-desktop/filter-category-desktop.component';
import { FilterDesktopComponent } from './product-filter-desktop/filter-desktop/filter-desktop.component';
import { FilterSortByDesktopComponent } from './product-filter-desktop/filter-sort-by-desktop/filter-sort-by-desktop.component';
import { ProductFilterDesktopComponent } from './product-filter-desktop/product-filter-desktop.component';
import { ProductFilterDialogComponent } from './product-filter-dialog/product-filter-dialog.component';
import { FilterEntryComponent } from './product-filters/filter-tab-filters/filter-entry/filter-entry.component';
import { FilterSeasonalityEntryComponent } from './product-filters/filter-tab-filters/filter-seasonality-entry/filter-seasonality-entry.component';
import { FilterSortByHandsetComponent } from './product-filters/filter-tab-filters/filter-sort-by-handset/filter-sort-by-handset.component';
import { FilterTabFiltersComponent } from './product-filters/filter-tab-filters/filter-tab-filters.component';
import { FilterTabBrandsComponent } from './product-filters/filter-tab-template/filter-tab-brands/filter-tab-brands.component';
import { FilterTabCategoryComponent } from './product-filters/filter-tab-template/filter-tab-category/filter-tab-category.component';
import { FilterTabColorsComponent } from './product-filters/filter-tab-template/filter-tab-colors/filter-tab-colors.component';
import { FilterTabConditionsComponent } from './product-filters/filter-tab-template/filter-tab-conditions/filter-tab-conditions.component';
import { FilterTabSearchHistoryComponent } from './product-filters/filter-tab-template/filter-tab-search-history/filter-tab-search-history.component';
import { FilterTabSeasonalityComponent } from './product-filters/filter-tab-template/filter-tab-seasonality/filter-tab-seasonality.component';
import { FilterTabSizesComponent } from './product-filters/filter-tab-template/filter-tab-sizes/filter-tab-sizes.component';
import { FilterTabSubCategoryComponent } from './product-filters/filter-tab-template/filter-tab-sub-category/filter-tab-sub-category.component';
import { FilterTabTemplateComponent } from './product-filters/filter-tab-template/filter-tab-template.component';
import { FilterTabTypesComponent } from './product-filters/filter-tab-template/filter-tab-types/filter-tab-types.component';
import { ProductFiltersComponent } from './product-filters/product-filters.component';
import { SearchHistoryDialogComponent } from './search-history-dialog/search-history-dialog.component';
import { SelectedFiltersComponent } from './selected-filters/selected-filters.component';

@NgModule({
    declarations: [
        ProductFilterDesktopComponent,
        ProductFilterDialogComponent,
        FilterSortByHandsetComponent,
        FilterDesktopComponent,
        FilterSortByDesktopComponent,
        FilterCategoryDesktopComponent,
        FilterCategoriesComponent,
        FilterSubCategoriesComponent,
        FilterTypesComponent,
        FilterSizesComponent,
        FilterBrandsComponent,
        FilterColorsComponent,
        FilterConditionsComponent,
        FilterPriceRangeComponent,
        FilterSeasonalityComponent,
        FilterItemSelectableComponent,
        FilterItemNonSelectableComponent,
        FilterTabFiltersComponent,
        FilterEntryComponent,
        FilterTabTemplateComponent,
        FilterTabTypesComponent,
        FilterTabBrandsComponent,
        FilterTabCategoryComponent,
        FilterTabColorsComponent,
        FilterTabConditionsComponent,
        FilterTabSizesComponent,
        FilterTabSubCategoryComponent,
        SelectedFiltersComponent,
        FilterTabSeasonalityComponent,
        FilterSeasonalityEntryComponent,
        FilterSearchComponent,
        SearchHistoryComponent,
        SearchCardComponent,
        FilterTabSearchHistoryComponent,
        SearchHistoryDialogComponent,
        ProductFiltersComponent,
    ],
    imports: [
        CommonModule,
        OverlayModule,
        MatButtonModule,
        MatIconModule,
        TranslateModule,
        MatCheckboxModule,
        ColorPreviewModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatInputModule,
        VirtualScrollerModule,
        AppCoreModule,
        MatRadioModule,
        MatStepperModule,
        ButtonModule,
        MatDialogModule,
        IconsModule,
        MatTooltipModule,
        MatTabsModule,
        MatProgressSpinnerModule,
        MatMenuModule,
        RouterModule,
    ],
    exports: [
        ProductFilterDesktopComponent,
        ProductFilterDialogComponent,
        SelectedFiltersComponent,
        FilterSearchComponent,
        ProductFiltersComponent,
        SearchHistoryComponent,
    ],
})
export class ProductFilterModule {}
