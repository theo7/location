import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { untilDestroyed } from '@ngneat/until-destroy';
import { combineLatest, Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { FilterDispatchers } from '../../../../../store/services/filter-dispatchers.service';
import { FilterSelectors } from '../../../../../store/services/filter-selectors.service';
import { RepositoriesSelectors } from '../../../../../store/services/repositories-selectors.service';
import { Brand } from '../../../../models/models';
import { ProductFilterComponent } from '../../product-filter.component';

@Component({
    selector: 'app-filter-brands',
    templateUrl: './filter-brands.component.html',
    styleUrls: ['./filter-brands.component.scss'],
})
export class FilterBrandsComponent extends ProductFilterComponent implements OnInit {
    selectedBrands$?: Observable<Brand[] | undefined>;
    filteredBrands$?: Observable<Brand[]>;

    inputSearch: FormControl;

    trackById = (index: number, brand: any) => brand.id;

    constructor(
        private readonly formBuilder: FormBuilder,
        private readonly repositoriesSelectors: RepositoriesSelectors,
        private readonly filterDispatchers: FilterDispatchers,
        private readonly filterSelectors: FilterSelectors,
    ) {
        super();
        this.inputSearch = this.formBuilder.control('');
    }

    ngOnInit(): void {
        this.selectedBrands$ = this.filterSelectors.brands$(this.key).pipe(untilDestroyed(this));
        this.selectedBrands$.pipe(untilDestroyed(this)).subscribe(() => {
            this.clearFilter();
        });
        const brands$ = this.repositoriesSelectors.brands$.pipe(untilDestroyed(this));
        const search$: Observable<string> = this.inputSearch.valueChanges.pipe(startWith(''), untilDestroyed(this));
        this.filteredBrands$ = combineLatest([brands$, search$, this.selectedBrands$]).pipe(
            map(([brands, search, selectedBrands]) => {
                return brands
                    .filter(brand => brand.label.toUpperCase().includes(search.toUpperCase()))
                    .filter(brand => selectedBrands?.every(bif => bif.id !== brand.id));
            }),
            untilDestroyed(this),
        );
    }

    clearFilter(): void {
        this.inputSearch.setValue('');
    }

    selectBrand(brand: Brand): void {
        this.filterDispatchers.addBrand(this.key, brand);
    }

    removeBrandFromFilter(brand: Brand): void {
        this.filterDispatchers.removeBrand(this.key, brand);
    }
}
