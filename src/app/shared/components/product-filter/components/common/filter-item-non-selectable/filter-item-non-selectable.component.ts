import { Component, EventEmitter, Output } from '@angular/core';

@Component({
    selector: 'app-filter-item-non-selectable',
    templateUrl: './filter-item-non-selectable.component.html',
    styleUrls: ['./filter-item-non-selectable.component.scss'],
})
export class FilterItemNonSelectableComponent {
    @Output() selected = new EventEmitter<void>();

    select(): void {
        this.selected.emit();
    }
}
