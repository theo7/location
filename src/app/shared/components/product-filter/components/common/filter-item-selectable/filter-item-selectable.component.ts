import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { MatCheckboxChange } from '@angular/material/checkbox';

@Component({
    selector: 'app-filter-item-selectable',
    templateUrl: './filter-item-selectable.component.html',
    styleUrls: ['./filter-item-selectable.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FilterItemSelectableComponent {
    @Input() value = false;
    @Input() groupItem = false;
    @Output() valueChange = new EventEmitter<boolean>();

    toggle(change: MatCheckboxChange): void {
        this.valueChange.emit(change.checked);
    }
}
