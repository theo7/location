import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { untilDestroyed } from '@ngneat/until-destroy';
import { Observable } from 'rxjs';
import { Seasonality, SelectableValue } from 'src/app/shared/models/models';
import { FilterDispatchers } from 'src/app/store/services/filter-dispatchers.service';
import { SearchFiltersSelectors } from 'src/app/store/services/search-filters-selectors.service';
import { ProductFilterComponent } from '../../product-filter.component';

@Component({
    selector: 'app-filter-seasonality',
    templateUrl: './filter-seasonality.component.html',
    styleUrls: ['./filter-seasonality.component.scss'],
})
export class FilterSeasonalityComponent extends ProductFilterComponent implements OnInit {
    @Output() selected = new EventEmitter<void>();

    seasonalityList$?: Observable<SelectableValue<Seasonality>[]>;

    constructor(
        private readonly searchFiltersSelectors: SearchFiltersSelectors,
        private readonly filterDispatchers: FilterDispatchers,
    ) {
        super();
    }

    ngOnInit(): void {
        this.seasonalityList$ = this.searchFiltersSelectors
            .selectableSeasonalityList$(this.key)
            .pipe(untilDestroyed(this));
    }

    onValueChanged(value: Seasonality): void {
        this.filterDispatchers.setSeasonality(this.key, value);
        this.selected.emit();
    }
}
