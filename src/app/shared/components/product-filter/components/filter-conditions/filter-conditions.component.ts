import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { untilDestroyed } from '@ngneat/until-destroy';
import { Observable } from 'rxjs';
import { FilterDispatchers } from '../../../../../store/services/filter-dispatchers.service';
import { SearchFiltersSelectors } from '../../../../../store/services/search-filters-selectors.service';
import { Condition, Selectable } from '../../../../models/models';
import { ProductFilterComponent } from '../../product-filter.component';

@Component({
    selector: 'app-filter-conditions',
    templateUrl: './filter-conditions.component.html',
    styleUrls: ['./filter-conditions.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FilterConditionsComponent extends ProductFilterComponent implements OnInit {
    conditions$?: Observable<Selectable<Condition>[]>;

    trackById = (index: number, condition: Selectable<Condition>) => condition.id;

    constructor(
        private readonly filterDispatchers: FilterDispatchers,
        private readonly searchFiltersSelectors: SearchFiltersSelectors,
    ) {
        super();
    }

    ngOnInit(): void {
        this.conditions$ = this.searchFiltersSelectors.selectableConditions$(this.key).pipe(untilDestroyed(this));
    }

    toggleCondition(condition: Condition, selected: boolean): void {
        if (selected) {
            this.filterDispatchers.addCondition(this.key, condition);
        } else {
            this.filterDispatchers.removeCondition(this.key, condition);
        }
    }
}
