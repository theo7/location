import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { untilDestroyed } from '@ngneat/until-destroy';
import { Observable } from 'rxjs';
import { FilterDispatchers } from '../../../../../store/services/filter-dispatchers.service';
import { SearchFiltersSelectors } from '../../../../../store/services/search-filters-selectors.service';
import { Category, Selectable, Size, SizeGrid } from '../../../../models/models';
import { ProductFilterComponent } from '../../product-filter.component';

type SelectableSizeGrid = {
    id: number;
    labelKey: string;
    sizes?: Selectable<Size>[];
    orderNumber?: number;
    category?: Category;
};

@Component({
    selector: 'app-filter-sizes',
    templateUrl: './filter-sizes.component.html',
    styleUrls: ['./filter-sizes.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FilterSizesComponent extends ProductFilterComponent implements OnInit {
    sizeGrids$?: Observable<(SizeGrid & { sizes: Selectable<Size>[] })[]>;

    trackBySizeGridId = (index: number, item: SelectableSizeGrid): number => item.id;
    trackBySizeId = (index: number, item: Selectable<Size>): number => item.id;

    constructor(
        private readonly filterDispatchers: FilterDispatchers,
        private readonly searchFiltersSelectors: SearchFiltersSelectors,
    ) {
        super();
    }

    ngOnInit(): void {
        this.sizeGrids$ = this.searchFiltersSelectors.selectableSizeGrids$(this.key).pipe(untilDestroyed(this));
    }

    toggleSize(size: Size, selected: boolean): void {
        if (selected) {
            this.filterDispatchers.addSize(this.key, size);
        } else {
            this.filterDispatchers.removeSize(this.key, size);
        }
    }
}
