import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { untilDestroyed } from '@ngneat/until-destroy';
import { debounceTime, first, map, tap } from 'rxjs/operators';
import { DeviceService } from 'src/app/shared/services/device.service';
import { ProductsService } from 'src/app/shared/services/products.service';
import { FilterDispatchers } from '../../../../../store/services/filter-dispatchers.service';
import { FilterSelectors } from '../../../../../store/services/filter-selectors.service';
import { CustomValidators } from '../../../../helpers/custom-validators';
import { ProductFilterComponent } from '../../product-filter.component';

@Component({
    selector: 'app-filter-price-range',
    templateUrl: './filter-price-range.component.html',
    styleUrls: ['./filter-price-range.component.scss'],
})
export class FilterPriceRangeComponent extends ProductFilterComponent implements OnInit {
    @Output() valid = new EventEmitter<boolean>();

    priceForm: FormGroup;

    constructor(
        private readonly filterSelectors: FilterSelectors,
        private readonly filterDispatcher: FilterDispatchers,
        private readonly formBuilder: FormBuilder,
        private readonly productsService: ProductsService,
        public readonly device: DeviceService,
    ) {
        super();
        this.priceForm = this.formBuilder.group(
            {
                minPrice: [null],
                maxPrice: [null],
            },
            { validators: CustomValidators.validPriceFilter },
        );
    }

    ngOnInit(): void {
        // initial value
        this.filterSelectors
            .minPrice$(this.key)
            .pipe(
                first(value => this.minPriceControl?.value !== value),
                untilDestroyed(this),
            )
            .subscribe(minPrice => {
                this.minPriceControl?.setValue(minPrice);
            });

        this.filterSelectors
            .maxPrice$(this.key)
            .pipe(
                first(value => this.maxPriceControl?.value !== value),
                untilDestroyed(this),
            )
            .subscribe(maxPrice => {
                this.maxPriceControl?.setValue(maxPrice);
            });

        // form listeners
        if (this.minPriceControl) {
            this.minPriceControl.valueChanges
                .pipe(
                    map(value => {
                        if (value !== null && value !== undefined) {
                            const twoDigitPrice = this.productsService.getTwoDigitPrice(value);
                            return { value, expected: twoDigitPrice };
                        }
                        return { value };
                    }),
                    tap(({ value, expected }) => {
                        if (value != null && value !== expected) {
                            this.minPriceControl.setValue(expected, { emitEvent: false });
                        }
                    }),
                    debounceTime(250),
                    untilDestroyed(this),
                )
                .subscribe(({ expected }) => {
                    if (expected !== null && expected !== undefined) {
                        this.filterDispatcher.setMinPrice(this.key, expected);
                        return;
                    }

                    this.filterDispatcher.resetMinPrice(this.key);
                });
        }

        if (this.maxPriceControl) {
            this.maxPriceControl.valueChanges
                .pipe(
                    map(value => {
                        if (value !== null && value !== undefined) {
                            const twoDigitPrice = this.productsService.getTwoDigitPrice(value);
                            return { value, expected: twoDigitPrice };
                        }
                        return { value };
                    }),
                    tap(({ value, expected }) => {
                        if (value != null && value !== expected) {
                            this.maxPriceControl.setValue(expected, { emitEvent: false });
                        }
                    }),
                    debounceTime(250),
                    untilDestroyed(this),
                )
                .subscribe(({ expected }) => {
                    if (expected !== null && expected !== undefined) {
                        this.filterDispatcher.setMaxPrice(this.key, expected);
                        return;
                    }

                    this.filterDispatcher.resetMaxPrice(this.key);
                });
        }

        if (this.priceForm) {
            this.priceForm.valueChanges.pipe(untilDestroyed(this)).subscribe(() => {
                if (this.priceForm) {
                    this.valid.emit(this.priceForm.valid);
                }
            });
        }
    }

    get minPriceControl() {
        return this.priceForm.controls?.minPrice;
    }

    get maxPriceControl() {
        return this.priceForm.controls?.maxPrice;
    }

    removeMinPriceFilter(): void {
        if (this.priceForm) {
            this.priceForm.patchValue({ minPrice: null });
        }
    }

    removeMaxPriceFilter(): void {
        if (this.priceForm) {
            this.priceForm.patchValue({ maxPrice: null });
        }
    }
}
