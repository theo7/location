import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { untilDestroyed } from '@ngneat/until-destroy';
import { combineLatest, Observable } from 'rxjs';
import { first, map, withLatestFrom } from 'rxjs/operators';
import { CategoriesSelectors } from '../../../../../store/services/categories-selectors.service';
import { FilterDispatchers } from '../../../../../store/services/filter-dispatchers.service';
import { FilterSelectors } from '../../../../../store/services/filter-selectors.service';
import { ProductsSelectors } from '../../../../../store/services/products-selectors.service';
import { Category, ProductType, Selectable, SubCategory } from '../../../../models/models';
import { ProductFilterComponent } from '../../product-filter.component';

@Component({
    selector: 'app-filter-types',
    templateUrl: './filter-types.component.html',
    styleUrls: ['./filter-types.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FilterTypesComponent extends ProductFilterComponent implements OnInit {
    @Output() quit = new EventEmitter<void>();

    @Input()
    subCategory!: SubCategory;

    @Input()
    category!: Category;

    allSelected$?: Observable<boolean>;
    productsTypes$?: Observable<Selectable<ProductType>[]>;

    constructor(
        private readonly filterDispatchers: FilterDispatchers,
        private readonly filterSelectors: FilterSelectors,
        private readonly productsSelectors: ProductsSelectors,
        private readonly categoriesSelectors: CategoriesSelectors,
    ) {
        super();
    }

    ngOnInit(): void {
        this.allSelected$ = combineLatest([
            this.filterSelectors.isCurrentSubCategorySelected$(this.key, this.subCategory.id),
            this.filterSelectors.productTypes$(this.key),
        ]).pipe(
            map(([subCategorySelected, productTypesSelected]) => {
                return subCategorySelected && productTypesSelected?.length === 0;
            }),
            untilDestroyed(this),
        );
        this.productsTypes$ = combineLatest([
            this.categoriesSelectors.productTypes$(this.category.id, this.subCategory.id),
            this.filterSelectors.productTypes$(this.key),
        ]).pipe(
            map(([types, selectedTypes]) =>
                types.map(t => ({
                    ...t,
                    selected: !!selectedTypes?.find(s => s.id === t.id),
                })),
            ),
            untilDestroyed(this),
        );
    }

    toggleProductType(productType: ProductType, selected: boolean): void {
        if (selected) {
            this.filterDispatchers.addProductType(this.key, productType, this.subCategory, this.category);
        } else {
            this.filterDispatchers.removeProductType(this.key, productType);
        }
    }

    selectAll(): void {
        this.filterSelectors
            .subCategory$(this.key)
            .pipe(first(), withLatestFrom(this.filterSelectors.productTypes$(this.key)))
            .subscribe(([selectedSubCategory, selectedProductTypes]) => {
                if (
                    !!selectedSubCategory &&
                    selectedSubCategory.id === this.subCategory.id &&
                    selectedProductTypes?.length === 0
                ) {
                    this.filterDispatchers.resetSubCategory(this.key);
                } else {
                    this.filterDispatchers.setSubCategory(this.key, this.subCategory, this.category);
                }
                this.quit.emit();
            });
    }
}
