import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Observable } from 'rxjs';
import { SearchService } from 'src/app/shared/services/search.service';
import { FilterDispatchers } from '../../../../../../store/services/filter-dispatchers.service';
import { Criterion, Search } from '../../../../../models/models';

@Component({
    selector: 'app-search-card',
    templateUrl: './search-card.component.html',
    styleUrls: ['./search-card.component.scss'],
})
export class SearchCardComponent implements OnInit {
    @Input()
    search!: Search;

    @Output()
    clicked = new EventEmitter<Search>();

    criteria: Observable<Criterion | undefined>[] = [];

    constructor(private readonly filterDispatchers: FilterDispatchers, private readonly searchService: SearchService) {}

    ngOnInit(): void {
        this.criteria = Object.keys(this.search)
            .filter(key => !['id', 'creationDate', 'lastModifiedDate', 'userId', 'saved', 'keyword'].includes(key))
            .filter(key => !!this.search[key])
            .map(key => this.searchService.getTranslationKey(this.search, key))
            .flatMap(key => key);
    }

    save(event: Event): void {
        event.preventDefault();
        event.stopPropagation();

        this.filterDispatchers.saveSearch(this.search.id);
    }

    unSave(event: Event): void {
        event.preventDefault();
        event.stopPropagation();

        this.filterDispatchers.unSaveSearch(this.search.id);
    }

    applyFilter(): void {
        this.filterDispatchers.applySearchFilter(this.search);
    }
}
