import { Component, EventEmitter, Output } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Observable } from 'rxjs';
import { FilterSelectors } from '../../../../../store/services/filter-selectors.service';
import { Search } from '../../../../models/models';

@UntilDestroy()
@Component({
    selector: 'app-search-history',
    templateUrl: './search-history.component.html',
    styleUrls: ['./search-history.component.scss'],
})
export class SearchHistoryComponent {
    recentSearch$: Observable<Search[]>;
    savedSearch$: Observable<Search[]>;
    loading$: Observable<boolean>;

    @Output()
    selected = new EventEmitter<Search>();

    trackById = (index: number, object: Search) => object.id;

    constructor(private readonly filterSelectors: FilterSelectors) {
        this.recentSearch$ = this.filterSelectors.recentSearch$.pipe(untilDestroyed(this));
        this.savedSearch$ = this.filterSelectors.savedSearch$.pipe(untilDestroyed(this));
        this.loading$ = this.filterSelectors.searchLoading$.pipe(untilDestroyed(this));
    }
}
