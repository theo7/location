import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { debounceTime, distinctUntilChanged, filter, first } from 'rxjs/operators';
import { FilterDispatchers } from '../../../../../store/services/filter-dispatchers.service';
import { FilterSelectors } from '../../../../../store/services/filter-selectors.service';
import { ProductFilterComponent } from '../../product-filter.component';

@UntilDestroy()
@Component({
    selector: 'app-filter-search',
    templateUrl: './filter-search.component.html',
    styleUrls: ['./filter-search.component.scss'],
})
export class FilterSearchComponent extends ProductFilterComponent implements OnInit {
    searchControl: FormControl;

    constructor(
        private readonly formBuilder: FormBuilder,
        private readonly filterDispatchers: FilterDispatchers,
        private readonly filterSelectors: FilterSelectors,
    ) {
        super();
        this.searchControl = this.formBuilder.control('');
    }

    ngOnInit(): void {
        this.filterSelectors
            .keyword$(this.key)
            .pipe(first())
            .subscribe(keyword => {
                this.searchControl.patchValue(keyword);
            });
        this.searchControl.valueChanges
            .pipe(
                debounceTime(500),
                distinctUntilChanged((s1: string, s2: string) => s1?.trim() === s2?.trim()),
                filter(value => !value || (!!value && value.length > 3)),
                untilDestroyed(this),
            )
            .subscribe(value => {
                this.filterDispatchers.setKeyword(this.key, value);
            });
    }
}
