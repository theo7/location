import { ChangeDetectionStrategy, Component, EventEmitter, OnInit, Output } from '@angular/core';
import { orderBy } from 'lodash-es';
import { Observable } from 'rxjs';
import { first, map } from 'rxjs/operators';
import { CategoriesSelectors } from '../../../../../store/services/categories-selectors.service';
import { Category, Indexable } from '../../../../models/models';
import { ProductFilterComponent } from '../../product-filter.component';

@Component({
    selector: 'app-filter-categories',
    templateUrl: './filter-categories.component.html',
    styleUrls: ['./filter-categories.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FilterCategoriesComponent extends ProductFilterComponent implements OnInit {
    @Output() goNext = new EventEmitter<Category>();

    categories$?: Observable<Indexable<Category>[]>;

    trackByCategoryId = (index: number, category: Category): number => category.id;

    constructor(private readonly categoriesSelectors: CategoriesSelectors) {
        super();
    }

    ngOnInit(): void {
        this.categories$ = this.categoriesSelectors.allCategories$.pipe(
            first(),
            map((categories): Indexable<Category>[] => orderBy(categories, 'orderNumber')),
        );
    }

    toggleCategory(category: Category): void {
        this.goNext.emit(category);
    }
}
