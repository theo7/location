import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { untilDestroyed } from '@ngneat/until-destroy';
import { Observable } from 'rxjs';
import { first, map, withLatestFrom } from 'rxjs/operators';
import { CategoriesSelectors } from '../../../../../store/services/categories-selectors.service';
import { FilterDispatchers } from '../../../../../store/services/filter-dispatchers.service';
import { FilterSelectors } from '../../../../../store/services/filter-selectors.service';
import { SearchFiltersSelectors } from '../../../../../store/services/search-filters-selectors.service';
import { Category, SubCategory } from '../../../../models/models';
import { ProductFilterComponent } from '../../product-filter.component';

@Component({
    selector: 'app-filter-sub-categories',
    templateUrl: './filter-sub-categories.component.html',
    styleUrls: ['./filter-sub-categories.component.scss'],
})
export class FilterSubCategoriesComponent extends ProductFilterComponent implements OnInit {
    @Output() goNext = new EventEmitter<SubCategory>();
    @Output() quit = new EventEmitter<void>();

    @Input()
    category!: Category;

    allSelected$?: Observable<boolean>;
    subCategories$?: Observable<SubCategory[]>;

    trackBySubCategoryId = (index: number, subCategory: SubCategory) => subCategory.id;

    constructor(
        private readonly filterDispatchers: FilterDispatchers,
        private readonly filterSelectors: FilterSelectors,
        private readonly searchFiltersSelectors: SearchFiltersSelectors,
        private readonly categoriesSelectors: CategoriesSelectors,
    ) {
        super();
    }

    ngOnInit(): void {
        this.allSelected$ = this.filterSelectors.isCurrentCategorySelected$(this.key, this.category.id).pipe(
            withLatestFrom(this.filterSelectors.subCategory$(this.key)),
            map(([categorySelected, subCategorySelected]) => categorySelected && !subCategorySelected),
            untilDestroyed(this),
        );
        this.subCategories$ = this.categoriesSelectors.subCategories$(this.category.id).pipe(untilDestroyed(this));
    }

    toggleSubCategory(subCategory: SubCategory): void {
        this.goNext.emit(subCategory);
    }

    selectAll(): void {
        this.filterSelectors
            .category$(this.key)
            .pipe(first(), withLatestFrom(this.filterSelectors.subCategory$(this.key)))
            .subscribe(([selectedCategory, selectedSubCategory]) => {
                if (!!selectedCategory && selectedCategory.id === this.category.id && !selectedSubCategory) {
                    this.filterDispatchers.resetCategory(this.key);
                } else {
                    this.filterDispatchers.setCategory(this.key, this.category);
                }
                this.quit.emit();
            });
    }
}
