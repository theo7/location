import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { untilDestroyed } from '@ngneat/until-destroy';
import { Observable } from 'rxjs';
import { FilterDispatchers } from '../../../../../store/services/filter-dispatchers.service';
import { SearchFiltersSelectors } from '../../../../../store/services/search-filters-selectors.service';
import { Color, Selectable } from '../../../../models/models';
import { ProductFilterComponent } from '../../product-filter.component';

@Component({
    selector: 'app-filter-colors',
    templateUrl: './filter-colors.component.html',
    styleUrls: ['./filter-colors.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FilterColorsComponent extends ProductFilterComponent implements OnInit {
    colors$?: Observable<Selectable<Color>[]>;

    trackByColorId = (index: number, color: Selectable<Color>) => color.id;

    constructor(
        private readonly filterDispatchers: FilterDispatchers,
        private readonly searchFiltersSelectors: SearchFiltersSelectors,
    ) {
        super();
    }

    ngOnInit(): void {
        this.colors$ = this.searchFiltersSelectors.selectableColors$(this.key).pipe(untilDestroyed(this));
    }

    toggleColor(color: Color, selected: boolean): void {
        if (selected) {
            this.filterDispatchers.addColor(this.key, color);
        } else {
            this.filterDispatchers.removeColor(this.key, color);
        }
    }
}
