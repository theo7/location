import { Component, OnInit } from '@angular/core';
import { untilDestroyed } from '@ngneat/until-destroy';
import { Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { FilterDispatchers } from '../../../../store/services/filter-dispatchers.service';
import { FilterSelectors } from '../../../../store/services/filter-selectors.service';
import { createSearchWithFilter, shouldSaveFilter } from '../../../functions/product-filter.functions';
import {
    Brand,
    Category,
    Color,
    Condition,
    ProductFilterSort,
    ProductType,
    Seasonality,
    Size,
    SubCategory,
} from '../../../models/models';
import { ProductFilterComponent } from '../product-filter.component';

@Component({
    selector: 'app-selected-filters',
    templateUrl: './selected-filters.component.html',
    styleUrls: ['./selected-filters.component.scss'],
})
export class SelectedFiltersComponent extends ProductFilterComponent implements OnInit {
    keyword$?: Observable<string | undefined>;
    category$?: Observable<Category | undefined>;
    subCategory$?: Observable<SubCategory | undefined>;
    productsTypes$?: Observable<ProductType[] | undefined>;
    brands$?: Observable<Brand[] | undefined>;
    colors$?: Observable<Color[] | undefined>;
    sizes$?: Observable<Size[] | undefined>;
    conditions$?: Observable<Condition[] | undefined>;
    minPrice$?: Observable<number | undefined>;
    maxPrice$?: Observable<number | undefined>;
    orderBy$?: Observable<ProductFilterSort | undefined>;
    seasonality$?: Observable<Seasonality | undefined>;
    hasSortFilter$?: Observable<boolean>;
    displayClearLink$?: Observable<boolean>;
    displaySaveSearchButton$?: Observable<boolean>;

    constructor(
        private readonly filterSelectors: FilterSelectors,
        private readonly filterDispatchers: FilterDispatchers,
    ) {
        super();
    }

    ngOnInit(): void {
        this.keyword$ = this.filterSelectors.keyword$(this.key).pipe(untilDestroyed(this));
        this.category$ = this.filterSelectors.category$(this.key).pipe(untilDestroyed(this));
        this.subCategory$ = this.filterSelectors.subCategory$(this.key).pipe(untilDestroyed(this));
        this.productsTypes$ = this.filterSelectors.productTypes$(this.key).pipe(untilDestroyed(this));
        this.sizes$ = this.filterSelectors.sizes$(this.key).pipe(untilDestroyed(this));
        this.brands$ = this.filterSelectors.brands$(this.key).pipe(untilDestroyed(this));
        this.colors$ = this.filterSelectors.colors$(this.key).pipe(untilDestroyed(this));
        this.conditions$ = this.filterSelectors.conditions$(this.key).pipe(untilDestroyed(this));
        this.minPrice$ = this.filterSelectors.minPrice$(this.key).pipe(untilDestroyed(this));
        this.maxPrice$ = this.filterSelectors.maxPrice$(this.key).pipe(untilDestroyed(this));
        this.orderBy$ = this.filterSelectors.sort$(this.key).pipe(untilDestroyed(this));
        this.seasonality$ = this.filterSelectors.seasonality$(this.key).pipe(untilDestroyed(this));
        this.hasSortFilter$ = this.filterSelectors.hasSort$(this.key).pipe(untilDestroyed(this));
        this.displayClearLink$ = this.filterSelectors.count$(this.key).pipe(
            map(count => count > 0),
            untilDestroyed(this),
        );
        this.displaySaveSearchButton$ = this.filterSelectors.current$(this.key).pipe(
            switchMap(filter => {
                const search = createSearchWithFilter(filter);
                const shouldSaveSearch = shouldSaveFilter(filter);

                return this.filterSelectors
                    .getSearchInHistory$(search)
                    .pipe(
                        map(
                            existingSearch =>
                                ((existingSearch && !existingSearch.saved) || !existingSearch) &&
                                shouldSaveSearch &&
                                this.key === 'search',
                        ),
                    );
            }),
            untilDestroyed(this),
        );
    }

    removeSearchTextFromFilter(): void {
        this.filterDispatchers.setKeyword(this.key, '');
    }

    removeBrandFromFilter(brand: Brand): void {
        this.filterDispatchers.removeBrand(this.key, brand);
    }

    removeColorFromFilter(color: Color): void {
        this.filterDispatchers.removeColor(this.key, color);
    }

    removeSizeFromFilter(size: Size): void {
        this.filterDispatchers.removeSize(this.key, size);
    }

    removeConditionFromFilter(condition: Condition): void {
        this.filterDispatchers.removeCondition(this.key, condition);
    }

    removeCategoryFromFilter(): void {
        this.filterDispatchers.resetCategory(this.key);
    }

    removeSubCategoryFromFilter(): void {
        this.filterDispatchers.resetSubCategory(this.key);
    }

    removeProductTypeFromFilter(productType: ProductType): void {
        this.filterDispatchers.removeProductType(this.key, productType);
    }

    removeMinPriceFromFilter(): void {
        this.filterDispatchers.resetMinPrice(this.key);
    }

    removeMaxPriceFromFilter(): void {
        this.filterDispatchers.resetMaxPrice(this.key);
    }

    removeOrderByFromFilter(): void {
        this.filterDispatchers.resetSort(this.key);
    }

    removeSeasonalityFilter(): void {
        this.filterDispatchers.resetSeasonality(this.key);
    }

    resetFilter(): void {
        this.filterDispatchers.reset(this.key);
    }

    saveCurrentSearch(): void {
        this.filterDispatchers.saveCurrentSearch();
    }
}
