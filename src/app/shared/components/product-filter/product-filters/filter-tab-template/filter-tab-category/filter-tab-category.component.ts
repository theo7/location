import { Component, EventEmitter, Output } from '@angular/core';
import { Category } from '../../../../../models/models';
import { ProductFilterComponent } from '../../../product-filter.component';

@Component({
    selector: 'app-filter-tab-category',
    templateUrl: './filter-tab-category.component.html',
    styleUrls: ['./filter-tab-category.component.scss'],
})
export class FilterTabCategoryComponent extends ProductFilterComponent {
    @Output() goBack = new EventEmitter<void>();
    @Output() goNext = new EventEmitter<Category>();
}
