import { Component, EventEmitter, Output } from '@angular/core';
import { ProductFilterComponent } from '../../../product-filter.component';

@Component({
    selector: 'app-filter-tab-sizes',
    templateUrl: './filter-tab-sizes.component.html',
    styleUrls: ['./filter-tab-sizes.component.scss'],
})
export class FilterTabSizesComponent extends ProductFilterComponent {
    @Output() goBack = new EventEmitter<void>();
    @Output() goFilters = new EventEmitter<void>();
}
