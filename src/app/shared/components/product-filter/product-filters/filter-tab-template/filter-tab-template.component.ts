import { Component, EventEmitter, HostBinding, Input, Output } from '@angular/core';
import { DeviceService } from '../../../../services/device.service';

@Component({
    selector: 'app-filter-tab-template',
    templateUrl: './filter-tab-template.component.html',
    styleUrls: ['./filter-tab-template.component.scss'],
})
export class FilterTabTemplateComponent {
    @Input() title = '';
    @Input() displayValidateButton = false;

    @Output() goBack = new EventEmitter<void>();
    @Output() goFilters = new EventEmitter<void>();

    @HostBinding('class') get class() {
        return this.device.mode;
    }

    constructor(private readonly device: DeviceService) {}
}
