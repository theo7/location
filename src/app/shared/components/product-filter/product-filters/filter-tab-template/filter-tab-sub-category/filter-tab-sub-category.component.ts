import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Category, SubCategory } from '../../../../../models/models';
import { ProductFilterComponent } from '../../../product-filter.component';

@Component({
    selector: 'app-filter-tab-sub-category',
    templateUrl: './filter-tab-sub-category.component.html',
    styleUrls: ['./filter-tab-sub-category.component.scss'],
})
export class FilterTabSubCategoryComponent extends ProductFilterComponent {
    @Output() goBack = new EventEmitter<void>();
    @Output() goNext = new EventEmitter<SubCategory>();
    @Output() goFilters = new EventEmitter<void>();

    @Input()
    category!: Category;

    constructor() {
        super();
    }
}
