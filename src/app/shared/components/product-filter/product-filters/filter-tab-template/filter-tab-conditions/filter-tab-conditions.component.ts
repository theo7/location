import { Component, EventEmitter, Output } from '@angular/core';
import { ProductFilterComponent } from '../../../product-filter.component';

@Component({
    selector: 'app-filter-tab-conditions',
    templateUrl: './filter-tab-conditions.component.html',
    styleUrls: ['./filter-tab-conditions.component.scss'],
})
export class FilterTabConditionsComponent extends ProductFilterComponent {
    @Output() goBack = new EventEmitter<void>();
    @Output() goFilters = new EventEmitter<void>();
}
