import { ChangeDetectionStrategy, Component, EventEmitter, Output } from '@angular/core';
import { ProductFilterComponent } from '../../../product-filter.component';

@Component({
    selector: 'app-filter-tab-seasonality',
    templateUrl: './filter-tab-seasonality.component.html',
    styleUrls: ['./filter-tab-seasonality.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FilterTabSeasonalityComponent extends ProductFilterComponent {
    @Output() goBack = new EventEmitter<void>();
    @Output() goFilters = new EventEmitter<void>();
}
