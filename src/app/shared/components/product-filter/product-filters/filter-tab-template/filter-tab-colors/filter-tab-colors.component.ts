import { ChangeDetectionStrategy, Component, EventEmitter, Output } from '@angular/core';
import { ProductFilterComponent } from '../../../product-filter.component';

@Component({
    selector: 'app-filter-tab-colors',
    templateUrl: './filter-tab-colors.component.html',
    styleUrls: ['./filter-tab-colors.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FilterTabColorsComponent extends ProductFilterComponent {
    @Output() goBack = new EventEmitter<void>();
    @Output() goFilters = new EventEmitter<void>();
}
