import { Component, EventEmitter, Output } from '@angular/core';
import { Search } from 'src/app/shared/models/models';
import { FilterDispatchers } from 'src/app/store/services/filter-dispatchers.service';

@Component({
    selector: 'app-filter-tab-search-history',
    templateUrl: './filter-tab-search-history.component.html',
    styleUrls: ['./filter-tab-search-history.component.scss'],
})
export class FilterTabSearchHistoryComponent {
    @Output() goBack = new EventEmitter<void>();

    constructor(private readonly filterDispatchers: FilterDispatchers) {}

    onSearchSelected(search: Search) {
        this.filterDispatchers.applySearchFilter(search);
    }
}
