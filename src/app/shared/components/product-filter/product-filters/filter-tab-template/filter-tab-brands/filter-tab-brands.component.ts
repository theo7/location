import { Component, EventEmitter, Output } from '@angular/core';
import { ProductFilterComponent } from '../../../product-filter.component';

@Component({
    selector: 'app-filter-tab-brands',
    templateUrl: './filter-tab-brands.component.html',
    styleUrls: ['./filter-tab-brands.component.scss'],
})
export class FilterTabBrandsComponent extends ProductFilterComponent {
    @Output() goBack = new EventEmitter<void>();
    @Output() goFilters = new EventEmitter<void>();
}
