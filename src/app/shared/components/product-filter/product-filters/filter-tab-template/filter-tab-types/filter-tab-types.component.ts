import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Category, SubCategory } from '../../../../../models/models';
import { ProductFilterComponent } from '../../../product-filter.component';

@Component({
    selector: 'app-filter-tab-types',
    templateUrl: './filter-tab-types.component.html',
    styleUrls: ['./filter-tab-types.component.scss'],
})
export class FilterTabTypesComponent extends ProductFilterComponent {
    @Output() goBack = new EventEmitter<void>();
    @Output() goFilters = new EventEmitter<void>();

    @Input()
    category!: Category;

    @Input()
    subCategory!: SubCategory;

    constructor() {
        super();
    }
}
