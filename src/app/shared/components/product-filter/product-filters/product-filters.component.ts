import { Component, Input, ViewEncapsulation } from '@angular/core';
import { FilterDispatchers } from '../../../../store/services/filter-dispatchers.service';
import { FilterSelectors } from '../../../../store/services/filter-selectors.service';
import { Category, SubCategory } from '../../../models/models';
import { ProductFilterComponent } from '../product-filter.component';

export enum Step {
    'FILTERS' = 0,
    'CATEGORY' = 1,
    'SUB_CATEGORY' = 2,
    'TYPE' = 3,
    'SIZE' = 4,
    'BRAND' = 5,
    'COLOR' = 6,
    'CONDITION' = 7,
    'SEASONALITY' = 8,
    'SEARCH_HISTORY' = 9,
}

@Component({
    selector: 'app-product-filters',
    templateUrl: './product-filters.component.html',
    styleUrls: ['./product-filters.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class ProductFiltersComponent extends ProductFilterComponent {
    @Input()
    isDialog = false;

    readonly Steps = Step;

    selectedStep: Step;
    selectedCategory?: Category;
    selectedSubCategory?: SubCategory;

    constructor(
        private readonly filterSelectors: FilterSelectors,
        private readonly filterDispatchers: FilterDispatchers,
    ) {
        super();
        this.selectedStep = Step.FILTERS;
    }

    onCategorySelected(category: Category): void {
        this.selectedCategory = category;
        this.selectedStep = Step.SUB_CATEGORY;
    }

    onSubCategorySelected(subCategory: SubCategory): void {
        this.selectedSubCategory = subCategory;
        this.selectedStep = Step.TYPE;
    }

    search(): void {
        this.filterDispatchers.search(this.key);
    }
}
