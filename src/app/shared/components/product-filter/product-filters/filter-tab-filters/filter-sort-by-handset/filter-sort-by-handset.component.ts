import { Component, OnInit } from '@angular/core';
import { untilDestroyed } from '@ngneat/until-destroy';
import { FilterDispatchers } from '../../../../../../store/services/filter-dispatchers.service';
import { FilterSelectors } from '../../../../../../store/services/filter-selectors.service';
import { ProductFilterSort } from '../../../../../models/models';
import { ProductFilterComponent } from '../../../product-filter.component';

@Component({
    selector: 'app-filter-sort-by-handset',
    templateUrl: './filter-sort-by-handset.component.html',
    styleUrls: ['./filter-sort-by-handset.component.scss'],
})
export class FilterSortByHandsetComponent extends ProductFilterComponent implements OnInit {
    sorts: (ProductFilterSort & { label: string; selected: boolean })[] = [
        { field: 'creationDate', order: 'desc', label: 'filter.sortByLatestProducts', selected: true },
        { field: 'relevance', order: 'desc', label: 'filter.sortByRelevanceDesc', selected: false },
        { field: 'price', order: 'asc', label: 'filter.sortByPriceAsc', selected: false },
        { field: 'price', order: 'desc', label: 'filter.sortByPriceDesc', selected: false },
    ];

    constructor(
        private readonly filterDispatchers: FilterDispatchers,
        private readonly filterSelectors: FilterSelectors,
    ) {
        super();
    }

    ngOnInit(): void {
        this.filterSelectors
            .sort$(this.key)
            .pipe(untilDestroyed(this))
            .subscribe(sortInFilter => {
                this.sorts = this.flagSelectedSort(this.sorts, sortInFilter);
            });
    }

    selectSort(sort: ProductFilterSort): void {
        this.filterDispatchers.setSort(this.key, sort);
    }

    private flagSelectedSort(
        sorts: (ProductFilterSort & { label: string; selected: boolean })[],
        sortInFilter: ProductFilterSort | undefined,
    ): (ProductFilterSort & { label: string; selected: boolean })[] {
        return sorts.map(sort => ({
            ...sort,
            selected: sort.field === sortInFilter?.field && sort.order === sortInFilter?.order,
        }));
    }
}
