import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
    selector: 'app-filter-entry',
    templateUrl: './filter-entry.component.html',
    styleUrls: ['./filter-entry.component.scss'],
})
export class FilterEntryComponent {
    @Input() title = '';
    @Input() placeholder = '';
    @Input() disabledPlaceholder = '';
    @Input() hasEntry = false;
    @Input() disabled = false;
    @Output() selected = new EventEmitter<void>();
}
