import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { ProductFilterComponent } from '../../../product-filter.component';

@Component({
    selector: 'app-filter-seasonality-entry',
    templateUrl: './filter-seasonality-entry.component.html',
    styleUrls: ['./filter-seasonality-entry.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FilterSeasonalityEntryComponent extends ProductFilterComponent {
    @Input() title: string = '';
    @Input() placeholder: string = '';

    @Output() selected = new EventEmitter<void>();
}
