import { Component, EventEmitter, HostBinding, Input, OnInit, Output } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { combineLatest, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { SearchSelectors } from 'src/app/store/services/search-selectors.service';
import { FilterDispatchers } from '../../../../../store/services/filter-dispatchers.service';
import { FilterSelectors } from '../../../../../store/services/filter-selectors.service';
import { RouterSelectors } from '../../../../../store/services/router-selectors.service';
import { UserProfileSelectors } from '../../../../../store/services/user-selectors.service';
import { ProductFilter, Seasonality } from '../../../../models/models';
import { DeviceService } from '../../../../services/device.service';
import { ProductFilterComponent } from '../../product-filter.component';
import { Step } from '../product-filters.component';

@UntilDestroy()
@Component({
    selector: 'app-filter-tab-filters',
    templateUrl: './filter-tab-filters.component.html',
    styleUrls: ['./filter-tab-filters.component.scss'],
})
export class FilterTabFiltersComponent extends ProductFilterComponent implements OnInit {
    @Input()
    isDialog = false;

    @Output()
    stepChange = new EventEmitter<Step>();

    @Output()
    search = new EventEmitter<void>();

    @HostBinding('class') get class() {
        return this.device.mode;
    }

    steps = Step;
    filterPriceValid = true;

    hasCategoryAttribute$?: Observable<boolean>;
    hasNoFilter$?: Observable<boolean>;
    filter$?: Observable<ProductFilter>;
    seasonality$?: Observable<Seasonality | undefined>;
    preSearchTotalElements$: Observable<number>;
    showProductCount$: Observable<boolean>;
    isLogged$: Observable<boolean>;

    constructor(
        private readonly routerSelectors: RouterSelectors,
        private readonly filterDispatchers: FilterDispatchers,
        private readonly filterSelectors: FilterSelectors,
        private readonly device: DeviceService,
        private readonly searchSelectors: SearchSelectors,
        private readonly userProfileSelectors: UserProfileSelectors,
    ) {
        super();
        this.preSearchTotalElements$ = this.searchSelectors.getPreSearchTotalElements$.pipe(untilDestroyed(this));

        this.showProductCount$ = combineLatest([
            this.routerSelectors.isSearchRouteActive$,
            this.preSearchTotalElements$,
        ]).pipe(
            map(([isSearchRouteActive, productCount]) => isSearchRouteActive && productCount < 10000),
            untilDestroyed(this),
        );

        this.isLogged$ = this.userProfileSelectors.isLogged$.pipe(untilDestroyed(this));
    }

    ngOnInit(): void {
        this.hasCategoryAttribute$ = this.filterSelectors.hasCategoryAttribute$(this.key).pipe(untilDestroyed(this));
        this.hasNoFilter$ = this.filterSelectors.isEmpty$(this.key).pipe(untilDestroyed(this));
        this.filter$ = this.filterSelectors.current$(this.key).pipe(untilDestroyed(this));
        this.seasonality$ = this.filterSelectors.seasonality$(this.key).pipe(untilDestroyed(this));
    }

    goToStep(step: Step): void {
        this.stepChange.emit(step);
    }

    clearFilters(): void {
        this.filterDispatchers.reset(this.key);
    }
}
