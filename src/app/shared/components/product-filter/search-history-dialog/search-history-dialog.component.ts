import { Component } from '@angular/core';
import { Search } from 'src/app/shared/models/models';
import { FilterDispatchers } from 'src/app/store/services/filter-dispatchers.service';

@Component({
    selector: 'app-search-history-dialog',
    templateUrl: './search-history-dialog.component.html',
    styleUrls: ['./search-history-dialog.component.scss'],
})
export class SearchHistoryDialogComponent {
    constructor(private readonly filterDispatchers: FilterDispatchers) {}

    onSearchSelected(search: Search) {
        this.filterDispatchers.applySearchFilter(search);
    }
}
