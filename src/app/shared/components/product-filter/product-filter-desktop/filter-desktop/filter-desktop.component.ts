import {
    AfterViewInit,
    ChangeDetectionStrategy,
    Component,
    EventEmitter,
    Input,
    ViewChild,
    ViewEncapsulation,
} from '@angular/core';
import { MatMenu, MatMenuTrigger } from '@angular/material/menu';
import { filter } from 'rxjs/operators';

@Component({
    selector: 'app-filter-desktop',
    templateUrl: './filter-desktop.component.html',
    styleUrls: ['./filter-desktop.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    encapsulation: ViewEncapsulation.None,
})
export class FilterDesktopComponent implements AfterViewInit {
    @ViewChild('menu') filterMenu!: MatMenu;
    @ViewChild(MatMenuTrigger) filterMenuTrigger!: MatMenuTrigger;

    @Input() title = '';
    @Input() disabled = false;

    ngAfterViewInit(): void {
        (this.filterMenu as any).closed = (this.filterMenu.close as any) = this.configureMenuClose(
            this.filterMenu.close,
        );
    }

    closeMenu(): void {
        this.filterMenuTrigger.closeMenu();
    }

    private configureMenuClose(old: MatMenu['close']): MatMenu['close'] {
        const closedEvent = new EventEmitter();

        closedEvent.pipe(filter(event => event !== 'click')).subscribe(old);

        return closedEvent;
    }
}
