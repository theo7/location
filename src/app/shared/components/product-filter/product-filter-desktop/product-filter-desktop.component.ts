import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { untilDestroyed } from '@ngneat/until-destroy';
import { Observable } from 'rxjs';
import { FilterSelectors } from '../../../../store/services/filter-selectors.service';
import { ProductFilterComponent } from '../product-filter.component';

@Component({
    selector: 'app-product-filter-desktop',
    templateUrl: './product-filter-desktop.component.html',
    styleUrls: ['./product-filter-desktop.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProductFilterDesktopComponent extends ProductFilterComponent implements OnInit {
    hasCategorySelected$?: Observable<boolean>;

    constructor(private readonly filterSelectors: FilterSelectors) {
        super();
    }

    ngOnInit(): void {
        this.hasCategorySelected$ = this.filterSelectors.hasCategory$(this.key).pipe(untilDestroyed(this));
    }
}
