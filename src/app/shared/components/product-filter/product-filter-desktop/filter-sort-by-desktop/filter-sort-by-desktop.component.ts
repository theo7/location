import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { MatRadioChange } from '@angular/material/radio';
import { untilDestroyed } from '@ngneat/until-destroy';
import { FilterDispatchers } from '../../../../../store/services/filter-dispatchers.service';
import { FilterSelectors } from '../../../../../store/services/filter-selectors.service';
import { ProductFilterSort } from '../../../../models/models';
import { ProductFilterComponent } from '../../product-filter.component';

type SelectableProductFilterSort = ProductFilterSort & { label: string; selected: boolean };

@Component({
    selector: 'app-filter-sort-by-desktop',
    templateUrl: './filter-sort-by-desktop.component.html',
    styleUrls: ['./filter-sort-by-desktop.component.scss'],
})
export class FilterSortByDesktopComponent extends ProductFilterComponent implements OnInit {
    @Output() selected = new EventEmitter<void>();

    public sorts: SelectableProductFilterSort[] = [
        { field: 'creationDate', order: 'desc', label: 'filter.sortByLatestProducts', selected: false },
        { field: 'relevance', order: 'desc', label: 'filter.sortByRelevanceDesc', selected: false },
        { field: 'price', order: 'asc', label: 'filter.sortByPriceAsc', selected: false },
        { field: 'price', order: 'desc', label: 'filter.sortByPriceDesc', selected: false },
    ];

    trackBySortId = (index: number, sort: SelectableProductFilterSort): string => `${sort.field}-${sort.order}`;

    constructor(
        private readonly filterSelectors: FilterSelectors,
        private readonly filterDispatchers: FilterDispatchers,
    ) {
        super();
    }

    ngOnInit(): void {
        this.filterSelectors
            .sort$(this.key)
            .pipe(untilDestroyed(this))
            .subscribe(selectedSort => {
                this.sorts = this.sorts.map(sort => ({
                    ...sort,
                    selected: selectedSort?.field === sort.field && selectedSort?.order === sort.order,
                }));
            });
    }

    select(sort: MatRadioChange): void {
        this.filterDispatchers.setSort(this.key, sort.value as ProductFilterSort);
        this.selected.emit();
    }
}
