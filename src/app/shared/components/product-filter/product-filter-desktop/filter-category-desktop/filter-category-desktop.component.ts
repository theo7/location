import { ChangeDetectionStrategy, Component, EventEmitter, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Actions, ofType } from '@ngrx/effects';
import { filtersActions } from '../../../../../store/actions/filters.actions';
import { Category, SubCategory } from '../../../../models/models';
import { ProductFilterComponent } from '../../product-filter.component';

enum Step {
    'CATEGORIES' = 0,
    'SUBCATEGORIES' = 1,
    'TYPE' = 2,
}

@UntilDestroy()
@Component({
    selector: 'app-filter-category-desktop',
    templateUrl: './filter-category-desktop.component.html',
    styleUrls: ['./filter-category-desktop.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    encapsulation: ViewEncapsulation.None,
})
export class FilterCategoryDesktopComponent extends ProductFilterComponent implements OnInit {
    @Output() quit = new EventEmitter<void>();

    step = Step.CATEGORIES;
    steps = Step;

    selectedCategory?: Category;
    selectedSubCategory?: SubCategory;

    constructor(private readonly actions$: Actions) {
        super();
    }

    ngOnInit(): void {
        // reset category
        this.actions$
            .pipe(ofType(filtersActions.resetCategory, filtersActions.reset), untilDestroyed(this))
            .subscribe(() => {
                this.selectedCategory = undefined;
                this.selectedSubCategory = undefined;
                this.step = Step.CATEGORIES;
            });
        // reset sub category
        this.actions$.pipe(ofType(filtersActions.resetSubCategory), untilDestroyed(this)).subscribe(() => {
            this.selectedSubCategory = undefined;
            this.step = Step.SUBCATEGORIES;
        });
    }

    onCategorySelected(category: Category): void {
        this.selectedCategory = category;
        this.step = Step.SUBCATEGORIES;
    }

    onSubCategorySelected(subCategory: SubCategory): void {
        this.selectedSubCategory = subCategory;
        this.step = Step.TYPE;
    }

    onQuit(): void {
        this.quit.emit();
    }

    goBackToCategories(): void {
        this.step = Step.CATEGORIES;
    }

    goBackToSubCategories(): void {
        this.step = Step.SUBCATEGORIES;
    }
}
