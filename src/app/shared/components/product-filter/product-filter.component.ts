import { Component, Input } from '@angular/core';
import { UntilDestroy } from '@ngneat/until-destroy';
import { FilterType } from '../../models/models';

// 💡 : This component is abstract and extended by every product-filter-* components
@UntilDestroy()
@Component({ template: '' })
export abstract class ProductFilterComponent {
    @Input() key!: FilterType;
}
