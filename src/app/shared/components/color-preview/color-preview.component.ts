import { Component, Input, OnInit } from '@angular/core';
import { Color } from '../../models/models';

@Component({
    selector: 'app-color-preview',
    template: '<div [ngStyle]="style"></div>',
})
export class ColorPreviewComponent implements OnInit {
    @Input() color!: Color;
    @Input() size = 24;

    style: { [key: string]: string } = {
        height: `${this.size}px`,
        width: `${this.size}px`,
        'border-radius': '50%',
        border: '1px solid #00000050',
    };

    ngOnInit() {
        if (!!this.color.hexCode) {
            this.style['background-color'] = `#${this.color.hexCode}`;
        } else if (!!this.color.imagePath) {
            this.style = {
                ...this.style,
                'background-image': `url(/assets/images/${this.color.imagePath})`,
                'background-position': 'center',
                'background-size': 'cover',
            };
        }
    }
}
