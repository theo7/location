import { animate, AnimationBuilder, AnimationFactory, AnimationPlayer, style } from '@angular/animations';
import { AfterViewInit, Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Picture } from '../../models/models';
import { DeviceService } from '../../services/device.service';
import { ImageService } from '../../services/image.service';
import { ImageFullscreenDialogComponent } from './image-fullscreen-dialog/image-fullscreen-dialog.component';

@Component({
    selector: 'app-product-carousel',
    templateUrl: './product-carousel.component.html',
    styleUrls: ['./product-carousel.component.scss'],
})
export class ProductCarouselComponent implements OnInit, AfterViewInit {
    @ViewChild('carouselItem') private carouselItem?: ElementRef;
    @ViewChild('carousel') private carousel?: ElementRef;

    @Input() pictures: Picture[] = [];

    timing = '250ms ease-in';
    carouselWrapperStyle = {};
    currentSlide = 0;

    private player?: AnimationPlayer;
    private itemWidth?: number;
    private nbItems?: number;

    constructor(
        private builder: AnimationBuilder,
        private imageService: ImageService,
        private dialog: MatDialog,
        private deviceService: DeviceService,
    ) {}

    ngOnInit(): void {
        this.nbItems = this.pictures.length;
        this.pictures = this.pictures.map(p => {
            return {
                ...p,
                loading: true,
            };
        });
    }

    ngAfterViewInit() {
        setTimeout(() => {
            if (this.carouselItem?.nativeElement) {
                this.itemWidth = this.carouselItem.nativeElement.getBoundingClientRect().width;
                this.carouselWrapperStyle = {
                    width: `${this.itemWidth}px`,
                };
            }
        });
    }

    next() {
        if (this.nbItems && this.itemWidth) {
            this.currentSlide = (this.currentSlide + 1) % this.nbItems;
            const offset = this.currentSlide * this.itemWidth;
            const myAnimation: AnimationFactory = this.buildAnimation(offset);

            if (this.carousel) {
                this.player = myAnimation.create(this.carousel.nativeElement);
                this.player.play();
            }
        }
    }

    prev() {
        if (this.currentSlide === 0) {
            return;
        }

        if (this.nbItems && this.itemWidth) {
            this.currentSlide = (this.currentSlide - 1 + this.nbItems) % this.nbItems;
            const offset = this.currentSlide * this.itemWidth;

            const myAnimation: AnimationFactory = this.buildAnimation(offset);

            if (this.carousel) {
                this.player = myAnimation.create(this.carousel.nativeElement);
                this.player.play();
            }
        }
    }

    onImageLoaded(picture: Picture): void {
        picture.loading = false;
    }

    getFullUrl(fileName: string): string {
        return this.imageService.getUrl(fileName);
    }

    openImageInFullscreen(picture: Picture) {
        const dialogParams = device =>
            ({
                handset: {
                    height: '100vh',
                    width: '100vw',
                    maxWidth: '100vw',
                },
                desktop: {
                    width: '90vw',
                    maxWidth: '700px',
                    height: '100vh',
                },
            }[device]);

        // disable body scroll
        window.document.body.style.overflow = 'hidden';
        // open modal
        this.dialog
            .open(ImageFullscreenDialogComponent, {
                panelClass: 'transparent-dialog-container',
                backdropClass: 'darker-backdrop',
                data: {
                    pictures: this.pictures,
                    index: this.pictures.indexOf(picture),
                },
                ...dialogParams(this.deviceService.mode),
            })
            .afterClosed()
            .subscribe(() => {
                // enable body scroll
                window.document.body.style.overflow = 'auto';
            });
    }

    private buildAnimation(offset) {
        return this.builder.build([animate(this.timing, style({ transform: `translateX(-${offset}px)` }))]);
    }
}
