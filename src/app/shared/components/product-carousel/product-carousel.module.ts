import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { TranslateModule } from '@ngx-translate/core';
import { DirectivesModule } from '../../directives/directives.module';
import { ImageFullscreenDialogComponent } from './image-fullscreen-dialog/image-fullscreen-dialog.component';
import { ProductCarouselComponent } from './product-carousel.component';

@NgModule({
    declarations: [ProductCarouselComponent, ImageFullscreenDialogComponent],
    imports: [
        CommonModule,
        DirectivesModule,
        MatProgressSpinnerModule,
        MatButtonModule,
        TranslateModule,
        MatDialogModule,
        MatIconModule,
    ],
    exports: [ProductCarouselComponent],
})
export class ProductCarouselModule {}
