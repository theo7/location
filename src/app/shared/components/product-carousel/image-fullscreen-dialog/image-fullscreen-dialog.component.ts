import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Picture } from '../../../models/models';
import { DeviceService } from '../../../services/device.service';
import { ImageService } from '../../../services/image.service';

@Component({
    selector: 'app-image-fullscreen-dialog',
    templateUrl: './image-fullscreen-dialog.component.html',
    styleUrls: ['./image-fullscreen-dialog.component.scss'],
})
export class ImageFullscreenDialogComponent implements OnInit {
    pictures: (Picture & { url: string | undefined })[] = [];
    currentIndex = 0;

    constructor(
        @Inject(MAT_DIALOG_DATA) private readonly data: { pictures: Picture[]; index: number },
        private readonly imageService: ImageService,
        public readonly deviseService: DeviceService,
    ) {}

    ngOnInit() {
        this.pictures = this.data.pictures.map(picture => ({
            ...picture,
            url: !picture.static ? this.imageService.getUrl(picture.fileName) : picture.fileName,
        }));
        this.currentIndex = this.data.index ?? 0;
    }

    next() {
        this.currentIndex = (this.currentIndex + 1) % this.pictures.length;
    }

    previous() {
        this.currentIndex = (this.currentIndex - 1 + this.pictures.length) % this.pictures.length;
    }
}
