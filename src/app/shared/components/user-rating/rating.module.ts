import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';
import { PipesModule } from '../../pipes/pipes.module';
import { RatingComponent } from './rating.component';

@NgModule({
    declarations: [RatingComponent],
    imports: [CommonModule, MatIconModule, PipesModule],
    exports: [RatingComponent],
})
export class RatingModule {}
