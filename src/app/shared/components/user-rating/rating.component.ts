import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';

@Component({
    selector: 'app-rating',
    templateUrl: './rating.component.html',
    styleUrls: ['./rating.component.scss'],
})
export class RatingComponent implements OnInit, OnChanges {
    @Input() rate = 0;
    @Input() max = 5;

    fullStarNumber = 0;
    emptyStarNumber: number = this.max;
    showHalfStar = false;

    ngOnInit() {
        this.handleUpdate();
    }

    ngOnChanges(changes: SimpleChanges) {
        this.handleUpdate();
    }

    private handleUpdate() {
        if (this.rate) {
            this.fullStarNumber = Math.floor(this.rate);

            const ratio = Math.round(2 * (this.rate - this.fullStarNumber)) * 0.5;

            if (ratio === 0.5) {
                this.showHalfStar = true;
            } else if (ratio === 1) {
                this.fullStarNumber++;
            }

            this.emptyStarNumber = this.max - this.fullStarNumber;

            if (this.showHalfStar) {
                this.emptyStarNumber--;
            }
        }
    }
}
