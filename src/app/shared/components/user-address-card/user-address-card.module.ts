import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { TranslateModule } from '@ngx-translate/core';
import { UserAddressCardComponent } from './user-address-card.component';

@NgModule({
    declarations: [UserAddressCardComponent],
    imports: [CommonModule, TranslateModule, MatButtonModule],
    exports: [UserAddressCardComponent],
})
export class UserAddressCardModule {}
