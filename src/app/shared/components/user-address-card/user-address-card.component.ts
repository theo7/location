import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Address } from '../../models/models';

@Component({
    selector: 'app-user-address-card',
    templateUrl: './user-address-card.component.html',
    styleUrls: ['./user-address-card.component.scss'],
})
export class UserAddressCardComponent {
    @Input() address?: Address;

    @Output() changeDefaultAddressEvent = new EventEmitter<void>();
    @Output() deleteEvent = new EventEmitter<void>();
    @Output() modifyEvent = new EventEmitter<void>();
}
