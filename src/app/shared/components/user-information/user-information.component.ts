import { Component, Input } from '@angular/core';
import { UserProfile } from '../../models/models';

@Component({
    selector: 'app-user-information',
    templateUrl: './user-information.component.html',
    styleUrls: ['./user-information.component.scss'],
})
export class UserInformationComponent {
    @Input() user?: UserProfile;
}
