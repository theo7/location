import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ConfirmBlockUserDialogResult } from '../../models/models';

type ConfirmUnblockUserDialogComponentData = {
    userId: number;
};

@Component({
    selector: 'app-confirm-unblock-user-dialog',
    templateUrl: './confirm-unblock-user-dialog.component.html',
    styleUrls: ['./confirm-unblock-user-dialog.component.scss'],
})
export class ConfirmUnblockUserDialogComponent {
    constructor(
        private readonly dialog: MatDialogRef<ConfirmUnblockUserDialogComponent, ConfirmBlockUserDialogResult>,
        @Inject(MAT_DIALOG_DATA) private data: ConfirmUnblockUserDialogComponentData,
    ) {}

    onYesButtonClicked() {
        this.dialog.close({
            userId: this.data.userId,
        });
    }
}
