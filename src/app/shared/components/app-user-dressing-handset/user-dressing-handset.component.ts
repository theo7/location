import { Component, ElementRef, Input, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Actions, ofType } from '@ngrx/effects';
import { combineLatest, Observable } from 'rxjs';
import { first, map, tap, withLatestFrom } from 'rxjs/operators';
import { createLotSuccess } from 'src/app/store/actions/lot.actions';
import { DiscussionsSelectors } from 'src/app/store/services/discussions-selectors.service';
import { FilterDispatchers } from '../../../store/services/filter-dispatchers.service';
import { FilterSelectors } from '../../../store/services/filter-selectors.service';
import { ProductsDispatchers } from '../../../store/services/products-dispatchers.service';
import { ProductsSelectors } from '../../../store/services/products-selectors.service';
import { RouterSelectors } from '../../../store/services/router-selectors.service';
import { UserProfileSelectors } from '../../../store/services/user-selectors.service';
import { FilterType, Product, UserProfile } from '../../models/models';
import { ShareService } from '../../services/share.service';

@UntilDestroy()
@Component({
    selector: 'app-user-dressing-handset',
    templateUrl: './user-dressing-handset.component.html',
    styleUrls: ['./user-dressing-handset.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class UserDressingHandsetComponent implements OnInit {
    @Input() user?: UserProfile;
    @Input() hiddenProducts: Product[] = [];
    @Input() scrollEl?: HTMLElement;

    @ViewChild('dressing') dressingElement!: ElementRef;

    isCurrentUser$?: Observable<boolean>;
    canCreateLot$?: Observable<boolean>;
    hasCartForSellerId$?: Observable<boolean>;
    products$?: Observable<Product[]>;
    productsCount$?: Observable<number>;
    filterCount$?: Observable<number>;
    hasProductInDressing$?: Observable<boolean>;
    isCurrentUserWithoutProduct$?: Observable<boolean>;
    showDressingFilter$?: Observable<boolean>;

    private lockInfiniteScroll = false;
    private readonly filterKey: FilterType = 'dressing';

    canShareDressing = this.shareService.canShare();

    constructor(
        readonly productsSelectors: ProductsSelectors,
        private readonly userProfileSelectors: UserProfileSelectors,
        private readonly productsDispatchers: ProductsDispatchers,
        private readonly shareService: ShareService,
        private readonly discussionsSelectors: DiscussionsSelectors,
        private readonly filterDispatchers: FilterDispatchers,
        private readonly filterSelectors: FilterSelectors,
        private readonly routerSelectors: RouterSelectors,
        private readonly actions$: Actions,
    ) {}

    ngOnInit(): void {
        this.isCurrentUser$ = this.userProfileSelectors.isCurrentUser$(this.user!.id!).pipe(untilDestroyed(this));
        this.canCreateLot$ = this.userProfileSelectors.canCreateLot$(this.user!.id!).pipe(untilDestroyed(this));
        this.filterCount$ = this.filterSelectors.count$(this.filterKey).pipe(untilDestroyed(this));

        this.isCurrentUserWithoutProduct$ = this.productsSelectors
            .isCurrentUserWithoutProduct$(this.user!.id!)
            .pipe(untilDestroyed(this));

        this.hasCartForSellerId$ = this.discussionsSelectors
            .hasCartForSellerId$(this.user!.id!)
            .pipe(untilDestroyed(this));

        this.products$ = this.productsSelectors.productsInDressingToDisplay$(this.user!.id!, this.hiddenProducts).pipe(
            tap(() => (this.lockInfiniteScroll = false)),
            untilDestroyed(this),
        );

        this.productsCount$ = this.productsSelectors.dressingCount$.pipe(untilDestroyed(this));

        this.hasProductInDressing$ = this.productsSelectors.hasOneProductInDressing$.pipe(untilDestroyed(this));

        this.showDressingFilter$ = this.productsSelectors.showDressingFilter$.pipe(
            withLatestFrom(this.userProfileSelectors.isCurrentUser$(this.user!.id!)),
            map(([showDressingFilter, isCurrentUser]) => showDressingFilter && !isCurrentUser),
            untilDestroyed(this),
        );
        this.actions$.pipe(ofType(createLotSuccess), untilDestroyed(this)).subscribe(() => {
            this.dressingElement?.nativeElement.scrollIntoView({ behavior: 'smooth' });
        });
    }

    share(): void {
        if (this.user) {
            this.productsDispatchers.shareDressing(this.user);
        }
    }

    onScroll(): void {
        combineLatest([
            this.productsSelectors.totalProducts$,
            this.productsSelectors.productsInDressing$,
            this.userProfileSelectors.userProfile$,
            this.routerSelectors.isUserProfileRouteActive$,
        ])
            .pipe(first())
            .subscribe(([total, products, currentUser, isProfileRouteActive]) => {
                if (!this.lockInfiniteScroll && total > products.length) {
                    this.lockInfiniteScroll = true;

                    if (this.user?.id) {
                        if (currentUser?.id === this.user.id && isProfileRouteActive) {
                            this.productsDispatchers.getOwnNextDressing();
                        } else {
                            this.productsDispatchers.getNextDressing(this.user.id);
                        }
                    }
                }
            });
    }

    openFilterDialog(): void {
        this.filterDispatchers.openDialog(this.filterKey);
    }
}
