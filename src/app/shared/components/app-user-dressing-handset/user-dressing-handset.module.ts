import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { TranslateModule } from '@ngx-translate/core';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { IconsModule } from 'src/app/features/icons/icons.module';
import { PipesModule } from '../../pipes/pipes.module';
import { SharedModule } from '../../shared.module';
import { EmptyDressingCardModule } from '../empty-dressing-card/empty-dressing-card.module';
import { ProductCardModule } from '../product-card/product-card.module';
import { ProductFilterModule } from '../product-filter/product-filter.module';
import { ProductListModule } from '../product-list/product-list.module';
import { UserDressingHandsetComponent } from './user-dressing-handset.component';

@NgModule({
    declarations: [UserDressingHandsetComponent],
    imports: [
        CommonModule,
        TranslateModule,
        InfiniteScrollModule,
        MatButtonModule,
        MatIconModule,
        ProductCardModule,
        MatProgressSpinnerModule,
        PipesModule,
        ProductListModule,
        SharedModule,
        IconsModule,
        EmptyDressingCardModule,
        ProductFilterModule,
        IconsModule,
    ],
    exports: [UserDressingHandsetComponent],
})
export class UserDressingHandsetModule {}
