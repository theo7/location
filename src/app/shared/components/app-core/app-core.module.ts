import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DeviceClassesDirective } from './device-classes.directive';

@NgModule({
    declarations: [DeviceClassesDirective],
    imports: [CommonModule],
    exports: [DeviceClassesDirective],
})
export class AppCoreModule {}
