import { Directive, ElementRef, Renderer2 } from '@angular/core';
import { DeviceService } from '../../services/device.service';

@Directive({
    selector: '[appDeviceClasses]',
})
export class DeviceClassesDirective {
    constructor(renderer: Renderer2, hostElement: ElementRef, deviceService: DeviceService) {
        deviceService.isHandset$.subscribe(isHandset => {
            if (hostElement.nativeElement) {
                if (isHandset) {
                    renderer.addClass(hostElement.nativeElement, 'handset');
                } else {
                    renderer.removeClass(hostElement.nativeElement, 'handset');
                }
            }
        });

        deviceService.isDesktop$.subscribe(isDesktop => {
            if (hostElement.nativeElement) {
                if (isDesktop) {
                    renderer.addClass(hostElement.nativeElement, 'desktop');
                } else {
                    renderer.removeClass(hostElement.nativeElement, 'desktop');
                }
            }
        });
    }
}
