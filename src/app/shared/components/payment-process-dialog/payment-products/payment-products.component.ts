import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output, ViewEncapsulation } from '@angular/core';
import { Lot } from '../../../models/models';

@Component({
    selector: 'app-payment-products',
    templateUrl: './payment-products.component.html',
    styleUrls: ['./payment-products.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    encapsulation: ViewEncapsulation.None,
})
export class PaymentProductsComponent {
    @Input() lot?: Lot;
    @Output() validate = new EventEmitter<void>();

    next() {
        this.validate.emit();
    }
}
