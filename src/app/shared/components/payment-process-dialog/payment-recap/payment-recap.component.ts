import { Component, EventEmitter, Input, Output, ViewEncapsulation } from '@angular/core';
import { Lot, Relay } from '../../../models/models';

@Component({
    selector: 'app-payment-recap',
    templateUrl: './payment-recap.component.html',
    styleUrls: ['./payment-recap.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class PaymentRecapComponent {
    @Output() next = new EventEmitter<string>();
    @Output() back = new EventEmitter<void>();

    @Input() lot?: Lot;
    @Input() relay?: Relay;

    buy() {
        this.next.emit();
    }

    goBack() {
        this.back.emit();
    }
}
