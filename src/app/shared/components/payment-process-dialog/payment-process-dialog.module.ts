import { ObserversModule } from '@angular/cdk/observers';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDividerModule } from '@angular/material/divider';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSelectModule } from '@angular/material/select';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { AddAddressDialogModule } from '../add-address-dialog/add-address-dialog.module';
import { AppCoreModule } from '../app-core/app-core.module';
import { ButtonModule } from '../button/button.module';
import { PaymentLinesModule } from '../payment-lines/payment-lines.module';
import { ProductLineModule } from '../product-line/product-line.module';
import { StepperModule } from '../stepper/stepper.module';
import { PaymentConfirmationCashCreditsComponent } from './payment-final-step/payment-confirmation-cash-credits/payment-confirmation-cash-credits.component';
import { PaymentFinalStepComponent } from './payment-final-step/payment-final-step.component';
import { PaymentRedirectionComponent } from './payment-final-step/payment-redirection/payment-redirection.component';
import { PaymentProcessDialogComponent } from './payment-process-dialog.component';
import { PaymentProductsComponent } from './payment-products/payment-products.component';
import { PaymentRecapComponent } from './payment-recap/payment-recap.component';
import { PaymentShippingComponent } from './payment-shipping/payment-shipping.component';

@NgModule({
    declarations: [
        PaymentProcessDialogComponent,
        PaymentShippingComponent,
        PaymentRecapComponent,
        PaymentProductsComponent,
        PaymentFinalStepComponent,
        PaymentRedirectionComponent,
        PaymentConfirmationCashCreditsComponent,
    ],
    imports: [
        CommonModule,
        MatIconModule,
        MatProgressSpinnerModule,
        MatDividerModule,
        TranslateModule,
        ProductLineModule,
        PaymentLinesModule,
        StepperModule,
        MatFormFieldModule,
        MatSelectModule,
        ReactiveFormsModule,
        AddAddressDialogModule,
        MatButtonModule,
        MatDialogModule,
        AppCoreModule,
        RouterModule,
        ButtonModule,
        ObserversModule,
    ],
    exports: [PaymentProcessDialogComponent],
})
export class PaymentProcessDialogModule {}

export { PaymentProcessDialogComponent };
