import {
    ChangeDetectorRef,
    Component,
    EventEmitter,
    Input,
    OnChanges,
    OnInit,
    Output,
    SimpleChanges,
    ViewChild,
    ViewEncapsulation,
} from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { interval, Observable, race, Subject } from 'rxjs';
import { filter, first, mapTo, startWith, switchMap } from 'rxjs/operators';
import { addAddressDialogComponent$ } from 'src/app/imports.dynamic';
import { UserProfileSelectors } from 'src/app/store/services/user-selectors.service';
import { environment } from '../../../../../environments/environment';
import { Address, Relay, UserProfile } from '../../../models/models';
import { DeviceService } from '../../../services/device.service';

declare var $;

type WidgetStatus = 'loading' | 'loaded' | 'failed';

const LOAD_WIDGET_TIMEOUT = 8 * 1000;

@UntilDestroy()
@Component({
    selector: 'app-payment-shipping',
    templateUrl: './payment-shipping.component.html',
    styleUrls: ['./payment-shipping.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class PaymentShippingComponent implements OnInit, OnChanges {
    @ViewChild('idRelay') idRelay;

    @Output() next = new EventEmitter<Relay>();
    @Output() back = new EventEmitter<void>();

    @Input() user?: UserProfile | null;

    addresses$?: Observable<Address[]>;
    hasAddresses$?: Observable<boolean>;

    displayWidget = true; // used to reload MR widget (if loading failed)

    loadingWidget$ = new Subject<void>(); // used to detect when user starts loading MR widget
    widgetLoaded$ = new Subject<boolean>(); // used to detect when MR widget is loaded
    widgetStatus$?: Observable<WidgetStatus>; // current status of MR widget displayed (loading/loaded/failed)

    addressControl?: FormControl;

    private lastRelaySelected?: Relay;

    constructor(
        private readonly cd: ChangeDetectorRef,
        private readonly dialog: MatDialog,
        private readonly deviceService: DeviceService,
        private readonly userProfileSelectors: UserProfileSelectors,
    ) {}

    ngOnInit(): void {
        this.addresses$ = this.userProfileSelectors.addresses$.pipe(untilDestroyed(this));
        this.hasAddresses$ = this.userProfileSelectors.hasAddresses$.pipe(untilDestroyed(this));

        this.userProfileSelectors.defaultAddress$.pipe(first()).subscribe(defaultAddress => {
            this.addressControl = new FormControl(defaultAddress, Validators.required);
        });

        if (this.addressControl) {
            this.addressControl.valueChanges.pipe(untilDestroyed(this)).subscribe((address: Address) => {
                $('#zone-widget').trigger('MR_DoSearch', [address.zipCode, 'FR']);
            });
        }

        this.widgetStatus$ = this.loadingWidget$.pipe(
            switchMap(() => {
                const loaded$ = this.widgetLoaded$.pipe(
                    filter(loaded => loaded),
                    mapTo('loaded' as WidgetStatus),
                );
                const failed$ = interval(LOAD_WIDGET_TIMEOUT).pipe(mapTo('failed' as WidgetStatus));

                return race(loaded$, failed$).pipe(startWith('loading' as WidgetStatus));
            }),
            untilDestroyed(this),
        );

        // first loading of widget
        if (this.addressControl) {
            this.addressControl.valueChanges
                .pipe(
                    startWith(this.addressControl.value),
                    first(value => !!value),
                    untilDestroyed(this),
                )
                .subscribe(() => {
                    this.initMRWidget(this.addressControl?.value?.zipCode);
                });
        }
    }

    ngOnChanges(changes: SimpleChanges) {
        if (!changes.user.firstChange && changes.user.currentValue.addresses.length > 0 && this.addressControl) {
            this.addressControl.setValue(this.user?.addresses?.find(a => a.isDefault));
        }
    }

    goBack() {
        this.back.emit();
    }

    confirm() {
        this.next.emit(this.lastRelaySelected);
    }

    detectChanges() {
        this.cd.detectChanges();
    }

    openAddAddressDialog() {
        const dialogDeviceParams = device =>
            ({
                handset: {
                    width: '100vw',
                    maxWidth: '100vw',
                    height: '100vh',
                    panelClass: 'full-screen-dialog',
                },
                desktop: {
                    height: '80vh',
                    width: '600px',
                    maxWidth: '600px',
                    autoFocus: false,
                    disableClose: true,
                    panelClass: 'no-padding-dialog',
                },
            }[device]);

        addAddressDialogComponent$.subscribe(AddAddressDialogComponent => {
            this.dialog.open(AddAddressDialogComponent, {
                data: {
                    addAsDefaultAddress: true,
                    user: this.user,
                },
                ...dialogDeviceParams(this.deviceService.mode),
            });
        });
    }

    onParcelShopContentChanged(records: MutationRecord[]) {
        this.widgetLoaded$.next(records.length > 0);
    }

    reloadMRButtonClicked() {
        this.initMRWidget(this.addressControl?.value?.zipCode);
    }

    private initMRWidget(zipCode: string) {
        this.displayWidget = false;

        this.detectChanges(); // remove MR widget from the DOM (before reload)

        this.displayWidget = true;

        this.detectChanges(); // recreate MR widget in the DOM

        this.loadingWidget$.next();

        this.detectChanges(); // apply loading state of the MR widget

        // start MR widget initialization once everything is done
        setTimeout(() => {
            $('#zone-widget').MR_ParcelShopPicker({
                Target: '#parcel-shop-code', // Selecteur JQuery de l'élément dans lequel sera renvoyé l'ID du Point Relay sélectionné (généralement un champ input hidden)
                Brand: environment.mondialRelais.brand, // Votre code client Mondial Relay
                Country: environment.mondialRelais.country, // Code ISO 2 lettres du pays utilisé pour la recherche
                PostCode: zipCode ?? environment.mondialRelais.zipCode,
                // Delivery mode (Standard [24R], XL [24L], XXL [24X], Drive [DRI])
                ColLivMod: environment.mondialRelais.colisMode,
                // Number of parcelshops requested (must be less than 20)
                NbResults: environment.mondialRelais.nbResult,
                Responsive: true,
                // Show the results on Leaflet map usng OpenStreetMap.
                ShowResultsOnMap: false,
                OnParcelShopSelected: data => {
                    this.lastRelaySelected = {
                        ID: this.idRelay.nativeElement?.value,
                        Nom: data.Nom,
                        Adresse1: data.Adresse1,
                        Adresse2: data.Adresse2,
                        CP: data.CP,
                        Ville: data.Ville,
                        Pays: data.Pays,
                        HoursHtmlTable: data.HoursHtmlTable,
                    };
                },
            });
        }, 0);
    }
}
