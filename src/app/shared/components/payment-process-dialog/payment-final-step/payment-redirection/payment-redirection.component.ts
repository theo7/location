import { Component } from '@angular/core';

@Component({
    selector: 'app-payment-redirection',
    templateUrl: './payment-redirection.component.html',
    styleUrls: ['./payment-redirection.component.scss'],
})
export class PaymentRedirectionComponent {}
