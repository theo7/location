import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Lot, PaymentMode, PaymentType } from 'src/app/shared/models/models';

@Component({
    selector: 'app-payment-final-step',
    templateUrl: './payment-final-step.component.html',
    styleUrls: ['./payment-final-step.component.scss'],
})
export class PaymentFinalStepComponent implements OnInit {
    @Output() next = new EventEmitter<void>();
    @Output() back = new EventEmitter<void>();

    @Input() lot?: Lot;

    hasCardPayment?: boolean;
    @Input() loading = false;

    ngOnInit(): void {
        this.hasCardPayment = !!this.lot?.paymentLines?.find(
            pl => pl.type === PaymentType.TOTAL_AMOUNT && pl.mode === PaymentMode.CARD,
        );

        if (this.hasCardPayment) {
            this.next.emit();
        }
    }
}
