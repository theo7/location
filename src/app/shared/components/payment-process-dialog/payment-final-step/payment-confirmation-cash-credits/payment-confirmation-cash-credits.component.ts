import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
    selector: 'app-payment-confirmation-cash-credits',
    templateUrl: './payment-confirmation-cash-credits.component.html',
    styleUrls: ['./payment-confirmation-cash-credits.component.scss'],
})
export class PaymentConfirmationCashCreditsComponent {
    @Output() next = new EventEmitter<void>();
    @Output() back = new EventEmitter<void>();

    @Input() loading = false;

    goBack() {
        this.back.emit();
    }

    buy() {
        this.next.emit();
    }
}
