import { Component, Inject, NgZone, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Capacitor } from '@capacitor/core';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { withLatestFrom } from 'rxjs/operators';
import { GoogleAnalyticsService } from '../../../features/analytics/google-analytics.service';
import { GoogleTagService } from '../../../features/analytics/google-tag.service';
import { DiscussionsSelectors } from '../../../store/services/discussions-selectors.service';
import { UserProfileSelectors } from '../../../store/services/user-selectors.service';
import { Discussion, Lot, PaymentMode, PaymentType, Relay, Step } from '../../models/models';
import { LotService } from '../../services/lot.service';

@UntilDestroy()
@Component({
    selector: 'app-payment-process-dialog',
    templateUrl: './payment-process-dialog.component.html',
    styleUrls: ['./payment-process-dialog.component.scss'],
})
export class PaymentProcessDialogComponent implements OnInit {
    steps: Step[] = [
        { label: 'payment.products', icon: 'Tunnel_commande_Step_EtapeArticles.svg' },
        { label: 'payment.shipping', icon: 'Tunnel_commande_Step_EtapeLivraison.svg' },
        { label: 'payment.recap', icon: 'Tunnel_commande_Step_EtapeRecap.svg' },
        { label: 'payment.payment', icon: 'Tunnel_commande_Step_EtapePaiement.svg' },
    ];
    activeStep = 0;
    doneSteps = -1;
    lot: Lot;

    resume?: FormControl;
    shipping?: FormControl;
    payment?: FormControl;

    relay?: Relay;
    buyLoading = false;

    user$ = this.userProfileSelectors.userProfile$;

    constructor(
        public readonly dialogRef: MatDialogRef<PaymentProcessDialogComponent>,
        @Inject(MAT_DIALOG_DATA) private readonly data: any,
        private readonly lotService: LotService,
        private readonly userProfileSelectors: UserProfileSelectors,
        private readonly discussionsSelectors: DiscussionsSelectors,
        private readonly router: Router,
        private readonly gaService: GoogleAnalyticsService,
        private readonly gtagService: GoogleTagService,
        private readonly iab: InAppBrowser,
        private readonly ngZone: NgZone,
    ) {
        this.lot = data.lot;

        if (this.lot.products) {
            this.lot.products.forEach(product => {
                if (this.lot.seller?.id) {
                    this.gaService.trackAddToCart(product, this.lot.seller.id);
                }
            });
            this.lot.products.forEach(() => this.gtagService.trackAddToCart());
        }
    }

    ngOnInit(): void {
        this.resume = new FormControl('', Validators.requiredTrue);
        this.shipping = new FormControl('', Validators.requiredTrue);
        this.payment = new FormControl('', Validators.requiredTrue);
    }

    goToStep(stepIndex: number) {
        if (stepIndex < this.activeStep) {
            this.activeStep = stepIndex;
            this.doneSteps = stepIndex - 1;
        }
    }

    async next() {
        const isBeforeDeliveryStep = this.activeStep === 0;

        if (isBeforeDeliveryStep) {
            await import('../../../../assets/scripts/parcelshop-picker-4.0.7.min.js');
        }

        this.activeStep++;
        this.doneSteps++;
    }

    back() {
        this.activeStep--;
        this.doneSteps--;
    }

    buy() {
        this.buyLoading = true;

        const isNative = Capacitor.isNativePlatform();

        this.lotService
            .buy(this.lot.id!, this.relay!.ID!, isNative)
            .pipe(withLatestFrom(this.discussionsSelectors.discussionByLotId$(this.lot.id!)), untilDestroyed(this))
            .subscribe(
                ([lot, discussion]) => {
                    this.buyLoading = false;
                    this.lot = lot;

                    const cardPaymentUrl = this.lot.paymentLines?.find(
                        pl => pl.type === PaymentType.TOTAL_AMOUNT && pl.mode === PaymentMode.CARD,
                    )?.moneyInCardWebUrl;

                    if (cardPaymentUrl) {
                        if (!Capacitor.isNativePlatform()) {
                            window.location.href = cardPaymentUrl;
                        } else {
                            const inAppBrowserOptions: InAppBrowserOptions = {
                                location: 'no',
                                hardwareback: 'no',
                                fullscreen: 'yes',
                                hidenavigationbuttons: 'yes',
                            };

                            this.dialogRef.close(true);

                            const browser = this.iab.create(cardPaymentUrl, '_blank', inAppBrowserOptions);

                            browser.on('loadstop').subscribe(event => {
                                console.log(event.url);

                                const isSuccess = event.url.includes('/buy/native-callback/success');

                                if (isSuccess && discussion?.lot) {
                                    this.gaService.trackPurchase(discussion.lot);
                                    this.gtagService.trackPurchase(discussion.lot);
                                }

                                if (
                                    event.url.includes('/buy/native-callback/success') ||
                                    event.url.includes('/buy/native-callback/error')
                                ) {
                                    this.ngZone.run(() => {
                                        this.router.navigate(['chat', discussion?.id]);
                                    });

                                    browser.close();
                                } else if (event.url.includes('/buy/native-callback/cancel')) {
                                    this.ngZone.run(() => {
                                        this.router.navigate(['']);
                                    });
                                    browser.close();
                                }
                            });
                        }
                    } else {
                        if (discussion?.lot) {
                            this.gaService.trackPurchase(discussion.lot);
                            this.gtagService.trackPurchase(discussion.lot);
                        }

                        this.closeDialogAndNavigateToChat(discussion);
                    }
                },
                () => {
                    this.closeDialogAndNavigateToChat();
                },
            );
    }

    relaySelected(relay: Relay) {
        this.relay = relay;
        this.activeStep++;
        this.doneSteps++;
    }

    private closeDialogAndNavigateToChat(discussion?: Discussion): void {
        if (this.router.url.indexOf('chat') !== -1) {
            this.dialogRef.close(true);
        } else if (discussion) {
            this.router.navigate(['chat', discussion.id]);
            this.dialogRef.close(true);
        } else {
            this.router.navigate(['']);
        }
    }
}
