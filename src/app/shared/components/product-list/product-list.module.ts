import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { TranslateModule } from '@ngx-translate/core';
import { VirtualScrollerModule } from 'ngx-virtual-scroller';
import { DirectivesModule } from '../../directives/directives.module';
import { PipesModule } from '../../pipes/pipes.module';
import { AppCoreModule } from '../app-core/app-core.module';
import { ButtonModule } from '../button/button.module';
import { ProductCardModule } from '../product-card/product-card.module';
import { KiabiBagCardDecorationComponent } from './kiabi-bag-card/kiabi-bag-card-decoration/kiabi-bag-card-decoration.component';
import { KiabiBagCardComponent } from './kiabi-bag-card/kiabi-bag-card.component';
import { ProductListComponent } from './product-list.component';

@NgModule({
    declarations: [ProductListComponent, KiabiBagCardComponent, KiabiBagCardDecorationComponent],
    imports: [
        CommonModule,
        AppCoreModule,
        TranslateModule,
        MatProgressSpinnerModule,
        PipesModule,
        MatButtonModule,
        ButtonModule,
        ProductCardModule,
        VirtualScrollerModule,
        DirectivesModule,
    ],
    exports: [ProductListComponent],
})
export class ProductListModule {}
