import {
    Component,
    EventEmitter,
    Input,
    OnChanges,
    OnInit,
    Output,
    SimpleChanges,
    ViewChild,
    ViewEncapsulation,
} from '@angular/core';
import { Router } from '@angular/router';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { IPageInfo, VirtualScrollerComponent } from 'ngx-virtual-scroller';
import { Observable } from 'rxjs';
import { distinctUntilChanged, filter, first, map, pairwise, withLatestFrom } from 'rxjs/operators';
import { FilterSelectors } from '../../../store/services/filter-selectors.service';
import { RouterSelectors } from '../../../store/services/router-selectors.service';
import { Product } from '../../models/models';
import { DeviceService } from '../../services/device.service';
import { ScrollStatesService } from '../../services/scroll-states.service';

const KIABI_BAG_CARD_INDEX = 6;

@UntilDestroy()
@Component({
    selector: 'app-product-list',
    templateUrl: './product-list.component.html',
    styleUrls: ['./product-list.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class ProductListComponent implements OnInit, OnChanges {
    @ViewChild('scroll') scroll!: VirtualScrollerComponent;

    @Input() products: Product[] | null = [];
    @Input() loading: boolean | null = false;
    @Input() displayOwner = true;
    @Input() displayLotAction = false;
    @Input() parentScrollEl?: Element | Window = window;
    @Input() totalElements?: number | null;
    @Input() showPublicity = false;

    @Output() fetchProducts = new EventEmitter<void>();

    isProfileSidePanelVisible$: Observable<boolean>;

    scrollKey?: string;
    hasAlreadyKiabiBagCard = false;
    items: (Product | null)[] = [];

    constructor(
        private readonly router: Router,
        private readonly routerSelectors: RouterSelectors,
        private readonly filterSelectors: FilterSelectors,
        private readonly deviceService: DeviceService,
        private readonly scrollStatesService: ScrollStatesService,
    ) {
        this.isProfileSidePanelVisible$ = this.routerSelectors.url$.pipe(
            map(url => url.startsWith('/profile') && this.deviceService.mode === 'desktop'),
            distinctUntilChanged(),
            untilDestroyed(this),
        );
    }

    ngOnInit(): void {
        // keep scroll state when navigating to other page
        this.routerSelectors.url$
            .pipe(
                pairwise(),
                map(([oldUrl]) => oldUrl),
                filter(oldUrl => /^\/search/.test(oldUrl)),
                untilDestroyed(this),
            )
            .subscribe(oldUrl => {
                this.scrollStatesService.setScrollState(oldUrl, this.scroll.viewPortInfo.scrollStartPosition);
                this.scrollKey = oldUrl;
            });
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.products) {
            this.items = [...(this.products ?? [])];
            // add kiabi bag card
            if (!changes.products.firstChange) {
                this.hasAlreadyKiabiBagCard = false;
            }
            if (this.items.length > KIABI_BAG_CARD_INDEX && !this.hasAlreadyKiabiBagCard && this.showPublicity) {
                this.hasAlreadyKiabiBagCard = true;
                this.items.splice(KIABI_BAG_CARD_INDEX, 0, null);
            }
            if (this.showPublicity) {
                const totalElements = this.totalElements || 0;
                const productLength = this.products?.length || 0;
                const shouldHideLastItem = this.items.length % 12 !== 0 && productLength < totalElements;

                if (shouldHideLastItem) {
                    this.items.splice(this.items.length - 1, 1);
                }
            }
        }
    }

    fetchMore(event: IPageInfo): void {
        let totalElements = this.totalElements || 0;
        if (this.hasAlreadyKiabiBagCard && this.showPublicity) {
            totalElements += 1;
        }

        const hasReachedBottomScroll = event.endIndex === this.items.length - 1;
        const hasMoreProductToFetch = this.items.length < totalElements;

        if (!this.loading && hasReachedBottomScroll && hasMoreProductToFetch) {
            this.fetchProducts.emit();
        }
    }

    scrollToPreviousState(): void {
        const scrollPosition = this.scrollStatesService.getScroll(this.scrollKey);
        if (scrollPosition === 0) {
            this.scroll.scrollToIndex(0, false, 0, 0);
        } else {
            setTimeout(() => {
                this.scroll.scrollToPosition(scrollPosition, 0);
            });
        }
    }

    navigateToProduct(product: Product): void {
        this.filterSelectors
            .currentRouterParams$('dressing')
            .pipe(
                first(),
                withLatestFrom(
                    this.routerSelectors.isProductRouteActive$,
                    this.routerSelectors.isUserProfileRouteActive$,
                ),
            )
            .subscribe(([queryParams, isProductRouteActive, isUserProfileRouteActive]) => {
                if (isProductRouteActive || isUserProfileRouteActive) {
                    this.router.navigate(['product', product.id], { queryParams });
                } else {
                    this.router.navigate(['product', product.id]);
                }
            });
    }
}
