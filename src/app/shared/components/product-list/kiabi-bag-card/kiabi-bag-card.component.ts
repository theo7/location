import { Component, ViewEncapsulation } from '@angular/core';
import { environment } from '../../../../../environments/environment';

@Component({
    selector: 'app-kiabi-bag-card',
    templateUrl: './kiabi-bag-card.component.html',
    styleUrls: ['./kiabi-bag-card.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class KiabiBagCardComponent {
    kiabiBagUrl = environment.kiabiBagUrl;
}
