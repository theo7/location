export type ProductId = number;

export interface Product {
    id: ProductId;
    label: string;
    price: number;
    description: string;
    pictures: Picture[];
    category?: Category;
    subCategory?: SubCategory;
    productType?: ProductType;
    brand: Brand;
    size: Size;
    color: Color[];
    condition: Condition;
    packagingType: PackagingType;
    selectedTransportModes: TransportMode[];
    productStatus: ProductStatusEnum;
    owner: UserProfile;
    lastModifiedDate: number;
    creationDate: number;
    labelStatus: ModerationStatus;
    descriptionStatus: ModerationStatus;
    likes?: number;
    seasonality?: Seasonality;
    searchScore?: number;
}

export enum Seasonality {
    ALL = 'ALL',
    SUMMER = 'SUMMER',
    WINTER = 'WINTER',
}

export interface NewProduct {
    label: string;
    price: number;
    description: string;
    pictures: Picture[];
    productType: ProductType;
    brand: Brand;
    size: Size;
    colors: Color[];
    condition: Condition;
    seasonality: Seasonality;
}

export interface PriceAverage {
    totalProducts: number;
    price: number;
}

export type Indexable<T> = T & { indexed: boolean };

export interface SelectableValue<T> {
    value: T;
    selected: boolean;
}

export type Selectable<T> = T & { selected: boolean };

export interface Brand {
    id: number;
    label: string;
    luxury: boolean;
    orderNumber: number;
    slug: string;
}

export interface Category {
    id: number;
    labelKey: string;
    subCategories?: SubCategory[];
    sizeGrids?: SizeGrid[];
    orderNumber: number;
}

export interface SubCategory {
    id: number;
    labelKey: string;
    category?: Category;
    productTypes?: ProductType[];
    orderNumber: number;
}

export interface ProductType {
    id: number;
    labelKey: string;
    subCategory: SubCategory;
    sizeGrid: SizeGrid;
    orderNumber: number;
}

export interface Color {
    id: number;
    labelKey: string;
    hexCode?: string;
    imagePath?: string;
    orderNumber: number;
}

export interface Condition {
    id: number;
    labelKey: string;
    descriptionKey: string;
    orderNumber: number;
}

export interface PackagingType {
    id: number;
    labelKey: string;
    descriptionKey: string;
    orderNumber: number;
}

export interface Size {
    id: number;
    labelKey: string;
    orderNumber: number;
    sizeGrid?: SizeGrid;
}

export interface SizeGrid {
    id: number;
    labelKey: string;
    sizes?: Size[];
    orderNumber?: number;
    category?: Category;
}

export interface UserProfile {
    id?: number;
    lastName?: string;
    firstName?: string;
    nickname: string;
    gender?: 'F' | 'M' | 'U';
    email?: string;
    phoneNumber?: string;
    presentation?: string;
    picture?: Picture;
    firebaseId: string;
    addresses?: Address[];
    birthDate?: string;
    nationality?: string;
    creationDate?: number;
    lastModifiedDate?: number;
    admin?: boolean;
    defaultAddress?: Address;
    cguSecondHandValidationDate?: number;
    cguLemonWayValidationDate?: number;
    presentationStatus?: ModerationStatus;
    dressingModerated?: boolean;
    favoriteProductsIds?: ProductId[];
    blockedUserIds?: number[];
    hasBlockedMe?: boolean;
    status?: UserProfileStatus;
    type?: UserType;
    role?: UserRole;

    /**
     * Average rating note of the user
     */
    rate?: number;

    /**
     * Total number of notes given for the user
     */
    totalRate?: number;

    /**
     * Number of people you follow
     */
    totalFollowing?: number;

    /**
     * Number of people that follows you
     */
    totalFollowers?: number;
    /**
     * is followed by current user
     */
    isFollowing?: boolean;
}

export interface UserFollowingSummary {
    totalFollower: number;
    totalFollowing: number;
}

export interface UserFollowingProfile {
    id: number;
    nickname: string;
    status?: UserProfileStatus;
    picture?: Picture;
    totalRating: number;
    averageRating: number;
}

export enum UserType {
    INDIVIDUAL = 'INDIVIDUAL',
    PROFESSIONAL = 'PROFESSIONAL',
}

export enum UserRole {
    USER = 'USER',
    ADMIN = 'ADMIN',
}

export interface UserRatingSummary {
    totalRating: number;
    averageRating: number;
    detailedRating: Map<number, number>;
}

export interface UserRating {
    id?: number;
    value: number;
    evaluated: UserProfile;
    evaluatedBy: UserProfile;
    pictures: Picture[];
    comment?: string;
    creationDate: number;
    lastModifiedDate: number;
    date: string;
    answerRate: UserRating;
}

export interface Page<T> {
    content: T[];
    totalElements: number;
    numberOfElements: number;
}

export interface Picture {
    id?: number;
    fileName?: string;
    orderNumber?: number;
    loading?: boolean;
    status?: ModerationStatus;
    static?: boolean;
    pictureType: PictureType;
}

export enum PictureType {
    CHAT = 'CHAT',
    AVATAR = 'AVATAR',
    RATE = 'RATE',
    PRODUCT = 'PRODUCT',
    REPORT = 'REPORT',
    LOGO = 'LOGO',
}

export enum ModerationStatus {
    OK = 'OK',
    AWAIT_MODERATION = 'AWAIT_MODERATION',
    MODERATED = 'MODERATED',
}

export interface Reservation {
    id: number;
    user: UserProfile;
    product: Product;
    startTime: Date;
    endTime: Date;
    transportFees: TransportFee[];
    negotiatedPrice: number;
}

export interface TransportFee {
    id: number;
    transportMode: TransportMode;
    packagingType: PackagingType;
    fee: number;
}

export interface TransportMode {
    id: number;
    logo: Picture;
    labelKey: string;
}

export interface Purchase {
    id?: number;
    reservation?: Reservation;
    transportMode?: TransportMode;
    buyer?: UserProfile;
    remunerationMode?: RemunerationMode;
    product?: Product;
}

export interface ProductFilter {
    searchText?: string;
    category?: Category;
    subCategory?: SubCategory;
    productsTypes?: ProductType[];
    brands?: Brand[];
    colors?: Color[];
    conditions?: Condition[];
    sizes?: Size[];
    minPrice?: number;
    maxPrice?: number;
    seasonality?: Seasonality;
    sort?: ProductFilterSort;
    excludedIds?: number[];
}

export type ProductField = 'creationDate' | 'price' | 'relevance';
export type ProductOrder = 'asc' | 'desc';

export type ProductFilterSort = {
    field: ProductField;
    order: ProductOrder;
};

export type FilterType = 'search' | 'dressing';

export interface SearchRouterParams {
    category?: number;
    subCategory?: number;
    productsTypes?: string; // 💡: if multiple, separated by ','. ex: 1,2,3
    brands?: string; // 💡: if multiple, separated by ','. ex: 1,2,3
    sizes?: string; // 💡: if multiple, separated by ','. ex: 1,2,3
    colors?: string; // 💡: if multiple, separated by ','. ex: 1,2,3
    conditions?: string; // 💡: if multiple, separated by ','. ex: 1,2,3
    seasonality?: Seasonality;
    minPrice?: number;
    maxPrice?: number;
    searchText?: string;
    sortField?: ProductField;
    sortOrder?: ProductOrder;
}

export interface StoreCredit {
    voucherAmount: number;
    voucherAmountOnHold: number;
    cashAmount: number;
    cashAmountOnHold: number;
    yearlyTotalContribution: number;
    yearlyMaxContribution: number;
}

export interface Voucher {
    id: number;
    value: number;
    user: UserProfile;
    status: VoucherStatus;
    idCard: string;
    lastModifiedDate: number;
}

export enum VoucherStatus {
    INIT = 'INIT',
    SEND = 'SEND',
    ERROR = 'ERROR',
}

export enum RemunerationMode {
    VOUCHER = 'VOUCHER',
    CASH = 'CASH',
}

export class WaitingValidationPurchases {
    waitingSentValidation: Lot[] = [];
    waitingReceivedValidation: Lot[] = [];
}

export interface ChatMessage {
    id?: string;
    content: string;
    type?: SystemMessageTypeEnum;
    param?: any;
    timestamp?: number;
    user?: string;
    firebaseId?: string;
    you?: boolean;
    offer?: Offer;
    picturesUrl?: string[];
}

export interface NewChatMessage {
    discussionId: number;
    content: string;
    pictures: Picture[];
}

export interface PageLw<T> {
    content: T[];
    totalPages: number;
}

export interface CashHistory {
    id: number;
    amount: number;
    date: number;
    type: TransactionHistoryType;
}

export enum TransactionHistoryType {
    P2P = 'P2P',
    MONEY_OUT = 'MONEY_OUT',
}

export enum DiscussionStatus {
    DISCUSSION_TEMP = 'DISCUSSION_TEMP',
    DISCUSSION_PENDING = 'DISCUSSION_PENDING',
    DISCUSSION_CANCEL = 'DISCUSSION_CANCEL',
}

export interface Discussion {
    id?: number | string;
    lot?: Lot;
    seller?: UserProfile;
    buyer?: UserProfile;
    offers?: Offer[];
    currentUserBuyer?: boolean;
    lastModifiedDate?: number;
    readByBuyer?: boolean;
    readBySeller?: boolean;
    read?: boolean;
    alreadyRated?: boolean;
    status?: DiscussionStatus;
    lastModifiedStatusDate?: number;
    negociatedPrice?: number;
    readOnly?: boolean;
    messageOnly?: boolean;
    messages?: ChatMessageMap;
    archivedByBuyer?: boolean;
    archivedBySeller?: boolean;
}

export type ChatMessageMap = { [id: string]: ChatMessage };

export interface ChatDiscussion {
    id?: number | string;
    seller?: string;
    buyer?: string;
    creationDate?: number;
    lastModifiedDate?: number;
    readByBuyer?: boolean;
    readBySeller?: boolean;
    messages?: ChatMessageMap;
    lotId?: number;
    archivedByBuyer?: boolean;
    archivedBySeller?: boolean;
}

export interface KiabiChatMessage {
    content: string;
    timestamp: number;
}

export enum LotStatusEnum {
    OPEN = 'OPEN',
    PREPAID = 'PREPAID',
    SENT = 'SENT',
    IN_RELAIS = 'IN_RELAIS',
    RECEIVED = 'RECEIVED',
    COMPLIANT = 'COMPLIANT',
    PAID = 'PAID',
    SOLD = 'SOLD',
    BLOCKED = 'BLOCKED',
    CANCELLED = 'CANCELLED',
    RESOLVED = 'RESOLVED',
}

export enum ProductStatusEnum {
    AVAILABLE = 'AVAILABLE',
    UNAVAILABLE = 'UNAVAILABLE',
    AWAIT_MODERATION = 'AWAIT_MODERATION',
    MODERATED = 'MODERATED',
    MODERATED_BY_ADMIN = 'MODERATED_BY_ADMIN',
    MANUALLY_MODERATED = 'MANUALLY_MODERATED',
}

export interface Tracking {
    id: number;
    paymentDate: Date;
    vendorValidationDate: Date;
    relayDeliveredDate: Date;
    buyerDeliveredDate: Date;
}

export enum CancelReasonAuthor {
    ADMIN = 'ADMIN',
    BUYER = 'BUYER',
    SELLER = 'SELLER',
    BATCH = 'BATCH',
}

export enum CancelReasonEnum {
    NOT_DELIVERED = 'NOT_DELIVERED',
    NOT_SENT = 'NOT_SENT',
    TEMP_DISCUSSION = 'TEMP_DISCUSSION',
    NOT_TAKEN = 'NOT_TAKEN',
    NOT_VALIDATED = 'NOT_VALIDATED',
    PRODUCTS_REMOVED = 'PRODUCTS_REMOVED',
    CANCEL = 'CANCEL',
}

export interface CancelReason {
    id: number;
    author: CancelReasonAuthor;
    reason: CancelReasonEnum;
    comment: string;
}

export interface Lot {
    id?: number;
    products: Product[];
    remunerationMode?: RemunerationMode;
    transportMode?: TransportMode;
    buyer?: UserProfile;
    seller?: UserProfile;
    productsFinalPrice?: number;
    orderDate?: Date;
    status?: LotStatusEnum;
    parcel?: Parcel;
    /**
     * Total price of the transaction (total paid)
     */
    totalAmount?: number;
    /**
     * Total price of products (without taxes, fees, etc...)
     */
    totalPrice?: number;
    paymentLines?: PaymentLine[];
    tracking?: Tracking;
    cancelReason?: CancelReason;
    creationDate?: number;
    lastModifiedDate?: number;
}

export interface Parcel {
    id: number;
    status: ParcelStatus;
    link: string;
    externalId: string;
    relaisCode: string;
    trackingUrl: string;
}

export enum ParcelStatus {
    INIT = 'INIT',
    SENT = 'SENT',
    RELAY = 'RELAY',
    DELIVERED = 'DELIVERED',
    RETURN = 'RETURN',
    LOST = 'LOST',
}

export enum OfferStatusEnum {
    OFFER_PENDING = 'OFFER_PENDING',
    OFFER_APPROVED = 'OFFER_APPROVED',
    OFFER_DECLINED = 'OFFER_DECLINED',
    OFFER_OUTDATED = 'OFFER_OUTDATED',
    OFFER_CANCELED = 'OFFER_CANCELED',
}

export interface Offer {
    key: string;
    id: number;
    firebaseId: string;
    price: number;
    counterOffers?: CounterOffer[];
    lastCounterOffer?: CounterOffer;
    status: OfferStatusEnum;
}

export interface CounterOffer {
    id: number;
    price: number;
    status: OfferStatusEnum;
    creationDate: number;
}

export enum SystemMessageTypeEnum {
    PRODUCT_ADDED = 'PRODUCT_ADDED',
    PRODUCT_REMOVED = 'PRODUCT_REMOVED',
    OFFER_MADE = 'OFFER_MADE',
    OFFER_ACCEPTED = 'OFFER_ACCEPTED',
    OFFER_DECLINED = 'OFFER_DECLINED',
    COUNTER_OFFER_MADE = 'COUNTER_OFFER_MADE',
    COUNTER_OFFER_CANCELED = 'COUNTER_OFFER_CANCELED',
    COUNTER_OFFER_UPDATED = 'COUNTER_OFFER_UPDATED',
    COUNTER_OFFER_ACCEPTED = 'COUNTER_OFFER_ACCEPTED',
    COUNTER_OFFER_DECLINED = 'COUNTER_OFFER_DECLINED',
    PRODUCT_LIST_UPDATED = 'PRODUCT_LIST_UPDATED',
    PRODUCT_UNAVAILABLE = 'PRODUCT_UNAVAILABLE',
    LOT_PREPAID = 'LOT_PREPAID',
    PRODUCT_MODIFIED = 'PRODUCT_MODIFIED',
    SELL_CONFIRMATION = 'SELL_CONFIRMATION',
    SELL_ABORTED = 'SELL_ABORTED',
    PACKAGE_ABORTED = 'PACKAGE_ABORTED',
    PACKAGE_SENT = 'PACKAGE_SENT',
    PACKAGE_NOT_SENT = 'PACKAGE_NOT_SENT',
    PACKAGE_RECEIVED = 'PACKAGE_RECEIVED',
    PACKAGE_COMPLIANT = 'PACKAGE_COMPLIANT',
    PACKAGE_NOT_COMPLIANT = 'PACKAGE_NOT_COMPLIANT',
    PACKAGE_IN_RELAIS = 'PACKAGE_IN_RELAIS',
    PAYMENT_TIMEOUT = 'PAYMENT_TIMEOUT',
    PAYMENT_ERROR = 'PAYMENT_ERROR',
    USER_RATED = 'USER_RATED',
    PACKAGE_RETURN = 'PACKAGE_RETURN',
    DISPUTE_RESOLVED = 'DISPUTE_RESOLVED',
    OTHER_REFUND = 'OTHER_REFUND',
    PACKAGE_NOT_RECEIVED = 'PACKAGE_NOT_RECEIVED',
    SEND_MESSAGE = 'SEND_MESSAGE',
    KIABI_MESSAGE = 'KIABI_MESSAGE',
    SEND_MESSAGE_PICTURE = 'SEND_MESSAGE_PICTURE',
    SEND_EMPTY_MESSAGE = 'SEND_EMPTY_MESSAGE',
    PRODUCT_FAVORITE = 'PRODUCT_FAVORITE',
    PRODUCT_FAVORITE_SOLD = 'PRODUCT_FAVORITE_SOLD',
    USER_SUBSCRIPTION = 'USER_SUBSCRIPTION',
    SUBSCRIPTION_PRODUCT_ADD = 'SUBSCRIPTION_PRODUCT_ADD',
    ANSWER_RATE = 'ANSWER_RATE',
}

export enum ChatActionState {
    CAN_DO_OFFER,
    NO_ACTION_POSSIBLE,
    OFFER_PENDING,
    NEGOTIATION_DONE,
    CAN_DO_COUNTER_OFFER,
    COUNTER_OFFER_PENDING,
    COMPLIANT,
    VIEW_JACKPOT,
    ONLY_BUY,
    CAN_CANCEL_COUNTER_OFFER,
}

export interface Address {
    id?: number;
    isDefault: boolean;
    addressName: string;
    firstName: string;
    lastName: string;
    line1?: string;
    line2?: string;
    city: string;
    zipCode: string;
    country: string;
    countryCode: string;
}

export interface UserAction {
    id: number;
    type: ActionTypeEnum;
    buyer: UserProfile;
    purchaseStatus: ProductStatusEnum | LotStatusEnum;
    lot: Lot;
    isBuyer: boolean;
}

export interface PaymentLine {
    id: number;
    status: PaymentStatus;
    mode: PaymentMode;
    type: PaymentType;
    fee: boolean;
    amount: number;
    moneyInCardWebToken: string;
    moneyInCardWebUrl: string;
}

export enum PaymentStatus {
    INIT = 'INIT',
    WEB_GENERATED = 'WEB_GENERATED',
    PREPAID = 'PREPAID',
    PAID = 'PAID',
    CANCEL = 'CANCEL',
    ERROR = 'ERROR',
}

export enum PaymentMode {
    CARD = 'CARD',
    P2P = 'P2P',
}

export enum PaymentType {
    FIXED_FEES = 'FIXED_FEES',
    VARIABLE_FEES = 'VARIABLE_FEES',
    TRANSPORT_FEES = 'TRANSPORT_FEES',
    TOTAL_AMOUNT = 'TOTAL_AMOUNT',
    TOTAL_REFUND = 'TOTAL_REFUND',
    PRODUCT_REFUND = 'PRODUCT_REFUND',
    PRODUCT = 'PRODUCT',
    PARTIAL_REFUND = 'PARTIAL_REFUND',
    COMMERCIAL_GESTURE = 'COMMERCIAL_GESTURE',
    PACKAGE_LOST = 'PACKAGE_LOST',
}

export interface Step {
    label: string;
    icon: string;
}

export interface ImagePreview {
    url: any;
    loading: boolean;
    status: ModerationStatus;
}

export interface ProductProperties {
    minPrice: number;
    maxPrice: number;
    maxPicture: number;
}

export interface StoreCreditProperties {
    buyPercentage: number;
    salePercentage: number;
    maximumYearlyContribution: number;
    minimumVoucherAmount: number;
    minimumMoneyOutAmount: number;
}

export interface Relay {
    ID?: string;
    Nom?: string;
    Adresse1?: string;
    Adresse2?: string;
    CP?: string;
    Ville?: string;
    Pays?: string;
    HoursHtmlTable?: string;
}

export enum ConfirmationEnum {
    CONFIRM = 'CONFIRM',
    REFUSE = 'REFUSE',
}

export interface UploadUserIban {
    holder: string;
    iban: string;
    rib: File;
}

export interface UploadDocumentResponse {
    id: number;
    status: DocumentStatus;
    accountstatus: AccountStatus;
}

export enum AccountStatus {
    NOT_REGISTERED = 'NOT_REGISTERED',
    REGISTERED_KYC_INCOMPLETE = 'REGISTERED_KYC_INCOMPLETE',
    REGISTERED_KYC_REJECTED = 'REGISTERED_KYC_REJECTED',
    REGISTERED_KYC1 = 'REGISTERED_KYC1',
    REGISTERED_KYC2 = 'REGISTERED_KYC2',
    REGISTERED_KYC3 = 'REGISTERED_KYC3',
    BLOCKED = 'BLOCKED',
    CLOSED = 'CLOSED',
    ONE_TIME_CUSTOMER = 'ONE_TIME_CUSTOMER',
    TECHNICAL = 'TECHNICAL',
}

export interface BankDetailsAccount {
    status?: AccountStatus;
}

export interface UserDocuments {
    iban?: Iban;
    identity?: Document;
    rib?: Document;
    account?: BankDetailsAccount;
}

export interface Document {
    id?: number;
    status?: DocumentStatus;
    type?: DocumentType;
    comment?: string;
}

export enum SimplifiedDocumentStatus {
    NOT_FILLED = 'NOT_FILLED',
    TO_BE_VERIFIED = 'TO_BE_VERIFIED',
    ACCEPTED = 'ACCEPTED',
    REJECTED = 'REJECTED',
}

export enum DocumentStatus {
    ON_HOLD = 'ON_HOLD',
    TO_BE_VERIFIED = 'TO_BE_VERIFIED',
    ACCEPTED = 'ACCEPTED',
    REJECTED = 'REJECTED',
    ILLEGIBLE = 'ILLEGIBLE',
    EXPIRED = 'EXPIRED',
    WRONG_TYPE = 'WRONG_TYPE',
    WRONG_HOLDER = 'WRONG_HOLDER',
    DUPLICATE = 'DUPLICATE',
}

export enum DocumentType {
    ID_CARD = 'ID_CARD',
    PROOF_ADDRESS = 'PROOF_ADDRESS',
    RIB = 'IBAN',
    PASSPORT_EU = 'PASSPORT_EU',
    PASSPORT_OUTSIDE_EU = 'PASSPORT_OUTSIDE_EU',
    RESIDENCE_PERMIT = 'RESIDENCE_PERMIT',
    KBIS = 'KBIS',
    DRIVER_LICENCE = 'DRIVER_LICENCE',
    OTHER = 'OTHER',
    STATUTS = 'STATUTS',
    SELFIE = 'SELFIE',
    OTHER_14 = 'OTHER_14',
    OTHER_15 = 'OTHER_15',
    OTHER_16 = 'OTHER_16',
    OTHER_17 = 'OTHER_17',
    OTHER_18 = 'OTHER_18',
    OTHER_19 = 'OTHER_19',
    OTHER_20 = 'OTHER_20',
    MANDAT = 'MANDAT',
}

export enum IbanStatus {
    NONE = 'NONE',
    INTERNAL = 'INTERNAL',
    NOT_USED = 'NOT_USED',
    WAITING_VERIFICATION = 'WAITING_VERIFICATION',
    ACTIVATED = 'ACTIVATED',
    REJECTED_BY_BANK = 'REJECTED_BY_BANK',
    REJECTED_NO_OWNER = 'REJECTED_NO_OWNER',
    DEACTIVATED = 'DEACTIVATED',
    REJECTED = 'REJECTED',
}

export interface Iban {
    id?: number;
    holder?: string;
    iban?: string;
    status?: IbanStatus;
}

export enum ActionTypeEnum {
    NEW_OFFER = 'NEW_OFFER',
    NEW_COUNTER_OFFER = 'NEW_COUNTER_OFFER',
    OFFER_REFUSED = 'OFFER_REFUSED',
    COUNTER_OFFER_REFUSED = 'COUNTER_OFFER_REFUSED',
    OFFER_ACCEPTED = 'OFFER_ACCEPTED',
    COUNTER_OFFER_ACCEPTED = 'COUNTER_OFFER_ACCEPTED',
    VALIDATE_SALE = 'VALIDATE_SALE',
    SALE_ACCEPTED = 'SALE_ACCEPTED',
    PACKAGE_SENT = 'PACKAGE_SENT',
    PACKAGE_RECEIVED = 'PACKAGE_RECEIVED',
}

export interface Health {
    status: 'DOWN' | 'UP';
}

export enum CguType {
    SECOND_HAND = 'secondHand',
    LEMON_WAY = 'lemonWay',
}

export enum ProductReportAbuseReason {
    WRONG_DESCRIPTION = 'WRONG_DESCRIPTION',
    INAPPROPRIATE = 'INAPPROPRIATE',
    PROFESSIONAL = 'PROFESSIONAL',
    UNAUTHORIZED = 'UNAUTHORIZED',
    UNAUTHORIZED_PICTURE = 'UNAUTHORIZED_PICTURE',
    FAKE = 'FAKE',
    OTHER = 'OTHER',
}

export type ProductReportAbuse = {
    reason: keyof typeof ProductReportAbuseReason;
    content?: string;
};

export type MoreActionsOnLotModalResult =
    | {
          type: 'CANCEL_SELL';
          lotId: number;
      }
    | {
          type: 'DECLINE_SELL';
          lotId: number;
      }
    | {
          type: 'CANCEL_PACKAGE';
          lotId: number;
      }
    | {
          type: 'BLOCK_USER';
          userId: number;
      }
    | {
          type: 'UNBLOCK_USER';
          userId: number;
      }
    | {
          type: 'ARCHIVE_DISCUSSION';
          discussionId: string | number;
      }
    | {
          type: 'REPORT_USER';
          discussion: Discussion;
      };

export type ConfirmCancelSellDialogResult = {
    lotId: number;
};

export type ConfirmDeclineSellDialogResult = {
    lotId: number;
};

export type ConfirmCancelPackageDialogResult = {
    lotId: number;
};

export type ConfirmBlockUserDialogResult = {
    userId: number;
};

export type ConfirmArchiveDiscussionDialogResult = {
    discussionId: number;
};

export type ReportUserDialogResult = {
    discussionId: number | string;
    newDiscussionReport: NewDiscussionReport;
};

export type NewDiscussionReport = {
    reason: DiscussionReportReason;
    content: string;
    pictures: Picture[];
};

export type DiscussionReport = {
    id: number;
    reason: DiscussionReportReason;
    content: string;
    reportedBy: UserProfile;
    reported: UserProfile;
    discussion: Discussion;
    pictures: Picture[];
};

export enum UserProfileStatus {
    ACTIVE = 'ACTIVE',
    CLOSED = 'CLOSED',
    BLOCKED = 'BLOCKED',
    VACATION = 'VACATION',
}

export interface JsonError {
    timestamp: number;
    status: number;
    error: string;
    message: string;
}

export enum DiscussionReportReason {
    FRAUD = 'FRAUD',
    PROFESSIONAL_ACCOUNT = 'PROFESSIONAL_ACCOUNT',
    BULLYING = 'BULLYING',
    ADVERTISEMENT = 'ADVERTISEMENT',
    COUNTERFEIT = 'COUNTERFEIT',
    OTHER = 'OTHER',
}

export interface ContactSupportInfo {
    theme: ContactTheme;
    subject: ContactSubject;
    firstName: string;
    lastName: string;
    email: string;
    message: string;
    commandNumber?: string;
    emailConnection?: string;
    userAgent: string;
}

export interface ContactTheme {
    id: number;
    labelKey: string;
}

export interface ContactSubject {
    id: number;
    labelKey: string;
    theme: ContactTheme;
}

/**
 * Key of the settings used to define if use has activated notification on this device (or not)
 */
export const LOCAL_STORAGE_NOTIFICATIONS_SETTINGS = 'location.settings.notifications';

/**
 * Key of the settings used to define if install panel should be hidden (visible if !undefined)
 */
export const LOCAL_STORAGE_INSTALL_PANEL = 'location.settings.install-panel';

export enum FileType {
    PRODUCT = 'product',
    CHAT = 'chat',
    RATING = 'rating',
    KYC_RECTO = 'recto',
    KYC_VERSO = 'verso',
    KYC_DOUBLE_PAGE = 'doublePage',
    KYC_RIB = 'rib',
}

export interface Auditable {
    creationDate: number;
    lastModifiedDate: number;
}

export interface Search extends Auditable {
    id: number;
    userId: number;
    keyword?: string;
    categoryId?: number;
    subCategoryId?: number;
    productTypeIds?: string; // 💡: if multiple, separated by ','. ex: 1,2,3
    conditionIds?: string; // 💡: if multiple, separated by ','. ex: 1,2,3
    brandIds?: string; // 💡: if multiple, separated by ','. ex: 1,2,3
    colorIds?: string; // 💡: if multiple, separated by ','. ex: 1,2,3
    sizeIds?: string; // 💡: if multiple, separated by ','. ex: 1,2,3
    minPrice?: number;
    maxPrice?: number;
    seasonality?: Seasonality;
    saved: boolean;
}

export type CreateSearch = Create<Search, 'saved' | 'userId'>;

export type Create<T, Keys extends keyof any = never> = Omit<T, Keys | 'id' | 'creationDate' | 'lastModifiedDate'>;

export type Update<T> = Partial<T>;

export type LoadingState<T> = T & { loading: boolean };

export interface TestCase<T, V> {
    params: T;
    expected: V;
}

export type BreadcrumbLink = {
    commands: any[] | string | null | undefined;
    labelKey: string;
    translateParams?: any;
};

export type PushNotificationMap = { [uuid: string]: PushNotification };

export interface PushNotification {
    uuid: string;
    type: SystemMessageTypeEnum;
    tokens: string[];
    url: string;
    pictureFileName: string;
    sender: string;
    content: string;
    timestamp: number;
}

export interface PushNotificationList {
    lastModifiedDate: number;
    read: boolean;
    notifications: PushNotificationMap;
}

export interface Criterion {
    key: string;
    params?: { [key: string]: any };
}

export interface Faq {
    id: number;
    codeLang: string;
    title: string;
    content: string;
    category: FaqCategory;
    orderNumber: number;
}

export enum FaqCategory {
    SELL = 'SELL',
    BUY = 'BUY',
    ACCOUNT = 'ACCOUNT',
}
