import { AfterViewInit, Directive, ElementRef, Renderer2 } from '@angular/core';
import { filter } from 'rxjs/operators';
import { UserProfileSelectors } from '../../store/services/user-selectors.service';

@Directive({
    selector: '[appDisableIfUserBlocked]',
})
export class DisableIfUserBlockedDirective implements AfterViewInit {
    constructor(
        private el: ElementRef,
        private userProfileSelectors: UserProfileSelectors,
        private renderer: Renderer2,
    ) {}

    ngAfterViewInit() {
        this.userProfileSelectors.isBlockedByAdmin$.pipe(filter(isBlocked => isBlocked)).subscribe(() => {
            if (this.el.nativeElement.tagName === 'A') {
                this.renderer.addClass(this.el.nativeElement, 'disabledLink');
            } else {
                this.renderer.setAttribute(this.el.nativeElement, 'disabled', 'true');
            }
        });
    }
}
