import { Directive, ElementRef, HostListener, Input, OnChanges, OnInit, Renderer2 } from '@angular/core';
import { environment } from '../../../environments/environment';

type MarkFunctionProperties<Component> = {
    [Key in keyof Component]: Component[Key] extends Function ? never : Key;
};

type ExcludeFunctionPropertyNames<T> = MarkFunctionProperties<T>[keyof T];

type ExcludeFunctions<T> = Pick<T, ExcludeFunctionPropertyNames<T>>;

type NgChanges<Component, Props = ExcludeFunctions<Component>> = {
    [Key in keyof Props]: {
        previousValue: Props[Key];
        currentValue: Props[Key];
        firstChange: boolean;
        isFirstChange(): boolean;
    };
};

// chat :           62x62
// listing :        273×300
// product-medium : 394x315
// product-large :  500x660
type ImageFormat = 'chat' | 'listing' | 'product-medium' | 'product-large' | '';
type ImageFallback = 'default' | 'avatar';
type Params = {
    origin: string | undefined;
    format?: ImageFormat;
    fallback?: ImageFallback;
};

const imgFallback = '/assets/images/photo-fallback.jpg';
const avatarFallback = '/assets/images/defaut-avatar.svg';

const isThumbnail = (src: string): boolean => src.includes('/thumbnails/');

export const isStatic = (src: string): boolean => {
    return src.startsWith(`${window.location.origin}/assets/`) || src.startsWith('assets/') || src.startsWith('data:');
};

const getThumbnailSrc = (src: string, format: ImageFormat): string => {
    const url = new URL(src);
    return `${url.origin}/thumbnails/${format}${url.pathname}`;
};

const getOriginalSrc = (thumbnailSrc: string): string => {
    const thumbnailUrl = new URL(thumbnailSrc);
    return `${thumbnailUrl.origin}/${thumbnailUrl.pathname.split('/').pop()}`;
};

const getExpectedFormat = (params: Params | undefined): ImageFormat | undefined => {
    return typeof params === 'string' ? params : params?.format || undefined;
};

const getFallbackUrl = (params: Params | undefined): string => {
    return typeof params === 'object' && params?.fallback === 'avatar' ? avatarFallback : imgFallback;
};

const getCandidatesImages = (
    src: string | undefined,
    expectedFormat: ImageFormat | undefined,
    fallbackUrl: string,
): string[] => {
    // 💡 : <img [src]="''" /> => src = window.location.origin
    if (!src || src === `${window.location.origin}/`) {
        return [fallbackUrl];
    }
    if (isStatic(src) || !environment.thumbnailsOptimization) {
        return [src];
    }
    if (isThumbnail(src)) {
        return [src, getOriginalSrc(src), fallbackUrl];
    }
    if (expectedFormat) {
        return [getThumbnailSrc(src, expectedFormat), src, fallbackUrl];
    }
    return [src, fallbackUrl];
};

abstract class CandidateImages {
    protected candidateImages: string[] = [];

    protected constructor(
        protected readonly el: ElementRef<HTMLImageElement>,
        protected readonly renderer: Renderer2,
    ) {}

    // 💡 : Lorsque la valeur de `src` (pour les <img>) change, le navigateur recharge l'image (appel HTTP, ...)
    tryNextCandidateImageAndReturnOthers(): string[] {
        if (this.candidateImages.length > 0) {
            this.renderer.setAttribute(this.el.nativeElement, 'src', this.candidateImages[0]);
            return this.candidateImages.slice(1);
        }

        return this.candidateImages;
    }
}

// 💡 : Cette directive permet, sur la base d'un format attendu, de :
// - Définir une chaine de fallbacks :
//    - 1) Format demandé
//    - 2) Format original
//    - 3) Fallback générique
@Directive({
    selector: 'img[appThumbnailFormat]',
})
export class ThumbnailFormatDirective extends CandidateImages implements OnInit, OnChanges {
    @Input() appThumbnailFormat?: Params;

    constructor(readonly el: ElementRef<HTMLImageElement>, readonly renderer: Renderer2) {
        super(el, renderer);
    }

    ngOnInit(): void {
        this.onSrcAttributeChange();
    }

    ngOnChanges(changes: NgChanges<ThumbnailFormatDirective>): void {
        const params = changes.appThumbnailFormat;

        if (
            params &&
            params.previousValue &&
            params.currentValue &&
            params.currentValue.origin !== params.previousValue.origin
        ) {
            this.onSrcAttributeChange();
        }
    }

    @HostListener('error')
    onError(): void {
        if (this.candidateImages.length === 0) {
            const fallbackUrl = getFallbackUrl(this.appThumbnailFormat);
            this.candidateImages.push(fallbackUrl);
        }

        this.candidateImages = this.tryNextCandidateImageAndReturnOthers();
    }

    private onSrcAttributeChange(): void {
        const params = this.appThumbnailFormat;
        const src = params?.origin;

        const expectedFormat = getExpectedFormat(params);
        const fallbackUrl = getFallbackUrl(params);
        this.candidateImages = getCandidatesImages(src, expectedFormat, fallbackUrl);

        this.candidateImages = this.tryNextCandidateImageAndReturnOthers();
    }
}

// 💡 : Cette directive permet de définir un tableau de fallback personnalisées
@Directive({
    selector: 'img[appFallbacks]',
})
export class FallbacksDirective extends CandidateImages implements OnInit {
    @Input() appFallbacks: string[] = [];

    constructor(readonly el: ElementRef<HTMLImageElement>, readonly renderer: Renderer2) {
        super(el, renderer);
    }

    ngOnInit(): void {
        this.candidateImages = this.appFallbacks;
        this.candidateImages = this.tryNextCandidateImageAndReturnOthers();
    }

    @HostListener('error') onError(): void {
        this.candidateImages = this.tryNextCandidateImageAndReturnOthers();
    }
}
