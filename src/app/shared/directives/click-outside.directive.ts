import { ApplicationRef, Directive, ElementRef, EventEmitter, HostListener, Output } from '@angular/core';

@Directive({
    selector: '[appClickOutside]',
})
export class ClickOutsideDirective {
    @Output() appClickOutside = new EventEmitter<void>();

    constructor(private elementRef: ElementRef, private applicationRef: ApplicationRef) {}

    @HostListener('document:click', ['$event.target'])
    public onClick(target) {
        const application = this.applicationRef.components[0].location;

        const isClickedElementInsideHostElement =
            this.elementRef.nativeElement && this.elementRef.nativeElement.contains(target);
        const isClickedElementInsideApplication =
            application.nativeElement && application.nativeElement.contains(target);

        if (isClickedElementInsideApplication && !isClickedElementInsideHostElement) {
            this.appClickOutside.emit();
        }
    }
}
