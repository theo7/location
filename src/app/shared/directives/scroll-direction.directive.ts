import { AfterViewInit, Directive, ElementRef, EventEmitter, Output } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { fromEvent } from 'rxjs';
import { distinctUntilChanged, map, pairwise, throttleTime } from 'rxjs/operators';

export enum ScrollDirection {
    UP = 'UP',
    DOWN = 'DOWN',
}

@UntilDestroy()
@Directive({
    selector: '[appScrollDirection]',
})
export class ScrollDirectionDirective implements AfterViewInit {
    @Output()
    scrollDirectionChanged = new EventEmitter<ScrollDirection>();

    constructor(private readonly el: ElementRef<HTMLElement>) {}

    ngAfterViewInit(): void {
        fromEvent(this.el.nativeElement, 'scroll')
            .pipe(
                throttleTime(10),
                map(e => (e.target as HTMLDivElement).scrollTop),
                pairwise(),
                map(([y1, y2]) => (y2 < y1 ? ScrollDirection.UP : ScrollDirection.DOWN)),
                distinctUntilChanged(),
                untilDestroyed(this),
            )
            .subscribe(e => {
                this.scrollDirectionChanged.emit(e);
            });
    }
}
