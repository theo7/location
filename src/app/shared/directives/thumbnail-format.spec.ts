import { isStatic } from './thumbnail-format.directive';

describe('ThumbnailFormatDirective', () => {
    describe('isStatic', () => {
        let windowSpy: jest.SpyInstance<any, any>;

        beforeEach(() => {
            windowSpy = jest.spyOn(window, 'window', 'get');
            windowSpy.mockReturnValue({
                location: {
                    origin: 'http://localhost:4200',
                },
            });
        });

        afterEach(() => {
            windowSpy.mockRestore();
        });

        it('empty string should be false', () => {
            const result = isStatic('');

            expect(result).toBeFalsy();
        });

        it('web url should be false', () => {
            const result = isStatic(
                'https://pictures.secondemain.kiabi.com/1615474136558_e2042050-5851-4383-8b6c-bfc71f35947b.jpg?format=listing',
            );

            expect(result).toBeFalsy();
        });

        it('assets url should be true', () => {
            const result = isStatic('http://localhost:4200/assets/svg/logo.svg');

            expect(result).toBeTruthy();
        });

        it('base64 url should be true', () => {
            const result = isStatic('data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD...');

            expect(result).toBeTruthy();
        });
    });
});
