import { Directive, ElementRef, HostListener, Renderer2 } from '@angular/core';

@Directive({
    selector: '[appAutoExpandable]',
})
export class AutoExpandableTextareaDirective {
    private heightLimit = 300;

    constructor(private elementRef: ElementRef, private renderer: Renderer2) {}

    @HostListener('keydown')
    public onChange() {
        this.renderer.setStyle(this.elementRef.nativeElement, 'height', '');
        this.renderer.setStyle(
            this.elementRef.nativeElement,
            'height',
            Math.min(this.elementRef.nativeElement.scrollHeight, this.heightLimit) + 'px',
        );
    }
}
