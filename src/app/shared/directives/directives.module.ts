import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AutoExpandableTextareaDirective } from './auto-expandable-textarea.directive';
import { ClickOutsideDirective } from './click-outside.directive';
import { DisableIfUserBlockedDirective } from './disable-if-user-blocked.directive';
import { ScrollDirectionDirective } from './scroll-direction.directive';
import { FallbacksDirective, ThumbnailFormatDirective } from './thumbnail-format.directive';

@NgModule({
    declarations: [
        ClickOutsideDirective,
        DisableIfUserBlockedDirective,
        ThumbnailFormatDirective,
        FallbacksDirective,
        ScrollDirectionDirective,
        AutoExpandableTextareaDirective,
    ],
    imports: [CommonModule],
    exports: [
        ClickOutsideDirective,
        DisableIfUserBlockedDirective,
        ThumbnailFormatDirective,
        FallbacksDirective,
        ScrollDirectionDirective,
        AutoExpandableTextareaDirective,
    ],
})
export class DirectivesModule {}
