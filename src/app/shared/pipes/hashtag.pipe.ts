import { Pipe, PipeTransform } from '@angular/core';
import { matchHashtags } from '../functions/hashtag.functions';
import { encodeHtml } from '../functions/html.functions';

@Pipe({ name: 'hashtag' })
export class HashtagPipe implements PipeTransform {
    transform(text: string | undefined) {
        const hashtagsMatches = matchHashtags(text);

        if (hashtagsMatches) {
            let result = encodeHtml(text || '') + ' ';

            for (let index = 0; index < hashtagsMatches.length; index++) {
                const hashtagValue = hashtagsMatches[index].replace(' ', '');

                result = result.replace(
                    encodeHtml(hashtagsMatches[index]),
                    `<internal-link link="/search?searchText=${hashtagValue}">${hashtagValue}</internal-link><span> </span>`,
                );
            }

            return result;
        }

        return text || '';
    }
}
