import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'truncateWithDots',
})
export class TruncateWithDotsPipe implements PipeTransform {
    transform(value: string | undefined, maxCharacter: number = 14) {
        if (value && value.length > maxCharacter) {
            return `${value.slice(0, maxCharacter + 1)}...`;
        }
        return value;
    }
}
