import { Pipe, PipeTransform } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Pipe({
    name: 'deletedUser',
})
export class DeletedUserPipe implements PipeTransform {
    constructor(private readonly translateService: TranslateService) {}

    transform(value: string | undefined) {
        if (value === 'DELETED') {
            return this.translateService.instant('common.deletedUser');
        }
        return value;
    }
}
