import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'iterate',
})
export class IteratePipe implements PipeTransform {
    transform(value: number): number[] {
        return [...Array(value).keys()];
    }
}
