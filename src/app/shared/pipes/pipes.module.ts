import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DeletedUserPipe } from './deleted-user.pipe';
import { HashtagPipe } from './hashtag.pipe';
import { IteratePipe } from './iterate.pipe';
import { SafeHtmlPipe } from './safe-html.pipe';
import { TruncateWithDotsPipe } from './truncate-with-dots.pipe';

@NgModule({
    declarations: [TruncateWithDotsPipe, IteratePipe, DeletedUserPipe, SafeHtmlPipe, HashtagPipe],
    imports: [CommonModule],
    exports: [TruncateWithDotsPipe, IteratePipe, DeletedUserPipe, SafeHtmlPipe, HashtagPipe],
})
export class PipesModule {}
