import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatRippleModule } from '@angular/material/core';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDividerModule } from '@angular/material/divider';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatStepperModule } from '@angular/material/stepper';
import { MatTooltipModule } from '@angular/material/tooltip';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { IconsModule } from '../features/icons/icons.module';
import { AppCoreModule } from './components/app-core/app-core.module';
import { AvatarModule } from './components/avatar/avatar.module';
import { ButtonModule } from './components/button/button.module';
import { ConfirmActionDialogComponent } from './components/confirm-action-dialog/confirm-action-dialog.component';
import { ConfirmArchiveDiscussionDialogComponent } from './components/confirm-archive-discussion-dialog/confirm-archive-discussion-dialog.component';
import { ConfirmBlockUserDialogComponent } from './components/confirm-block-user-dialog/confirm-block-user-dialog.component';
import { ConfirmCancelPackageDialogComponent } from './components/confirm-cancel-package-dialog/confirm-cancel-package-dialog.component';
import { ConfirmCancelSellDialogComponent } from './components/confirm-cancel-sell-dialog/confirm-cancel-sell-dialog.component';
import { ConfirmComplianceDialogComponent } from './components/confirm-compliance-dialog/confirm-compliance-dialog.component';
import { ConfirmDeclineSellDialogComponent } from './components/confirm-decline-sell-dialog/confirm-decline-sell-dialog.component';
import { ConfirmIndividualSellDialogComponent } from './components/confirm-individual-sell-dialog/confirm-individual-sell-dialog.component';
import { RefuseSellDialogComponent } from './components/confirm-individual-sell-dialog/refuse-sell-dialog/refuse-sell-dialog.component';
import { ConfirmProfessionalSellDialogComponent } from './components/confirm-professional-sell-dialog/confirm-professional-sell-dialog.component';
import { ConfirmUnblockUserDialogComponent } from './components/confirm-unblock-user-dialog/confirm-unblock-user-dialog.component';
import { ConfirmationDialogComponent } from './components/confirmation-dialog/confirmation-dialog.component';
import { GoToLoginDialogComponent } from './components/go-to-login-dialog/go-to-login-dialog.component';
import { ImagesMiniModule } from './components/images-mini/images-mini.module';
import { ProductCardModule } from './components/product-card/product-card.module';
import { ProductDetailModule } from './components/product-detail/product-detail.module';
import { ProductLineModule } from './components/product-line/product-line.module';
import { ShippingPromoModule } from './components/shipping-promo/shipping-promo.module';
import { SuccessNotificationComponent } from './components/success-notification/success-notification.component';
import { UnsaveChangesDialogComponent } from './components/unsave-changes-dialog/unsave-changes-dialog.component';
import { UserProInformationComponent } from './components/user-pro-information/user-pro-information.component';
import { DirectivesModule } from './directives/directives.module';
import { PipesModule } from './pipes/pipes.module';

@NgModule({
    declarations: [
        GoToLoginDialogComponent,
        ConfirmationDialogComponent,
        ConfirmComplianceDialogComponent,
        RefuseSellDialogComponent,
        UnsaveChangesDialogComponent,
        ConfirmCancelSellDialogComponent,
        ConfirmBlockUserDialogComponent,
        ConfirmUnblockUserDialogComponent,
        ConfirmArchiveDiscussionDialogComponent,
        ConfirmActionDialogComponent,
        ConfirmCancelPackageDialogComponent,
        ConfirmDeclineSellDialogComponent,
        SuccessNotificationComponent,
        ConfirmProfessionalSellDialogComponent,
        ConfirmIndividualSellDialogComponent,
        UserProInformationComponent,
    ],
    imports: [
        CommonModule,
        RouterModule,
        MatDialogModule,
        MatIconModule,
        MatButtonModule,
        MatDividerModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatProgressSpinnerModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        MatRadioModule,
        MatStepperModule,
        MatRippleModule,
        MatListModule,
        MatTooltipModule,
        ProductLineModule,
        ShippingPromoModule,
        ImagesMiniModule,
        ProductCardModule,
        AvatarModule,
        ButtonModule,
        ProductDetailModule,
        AppCoreModule,
        PipesModule,
        DirectivesModule,
        IconsModule,
    ],
    exports: [
        CommonModule,
        RouterModule,
        GoToLoginDialogComponent,
        ConfirmationDialogComponent,
        MatIconModule,
        MatButtonModule,
        ButtonModule,
        MatDialogModule,
        MatDividerModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatProgressSpinnerModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        ConfirmIndividualSellDialogComponent,
        MatStepperModule,
        AppCoreModule,
        ConfirmProfessionalSellDialogComponent,
        UserProInformationComponent,
    ],
})
export class SharedModule {}
