export enum HttpStatus {
    MISSING_RESPONSE = 0,
    UNAUTHORIZED = 401,
    FORBIDDEN = 403,
    NOT_FOUND = 404,
    INTERNAL = 500,
}
