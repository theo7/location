import {
    AbstractControl,
    AbstractControlOptions,
    FormBuilder,
    FormGroup,
    ValidationErrors,
    Validators,
} from '@angular/forms';
import * as dayjs from 'dayjs';
import { cold, Scheduler } from 'jest-marbles';
import { RepositoriesSelectors } from 'src/app/store/services/repositories-selectors.service';
import { Brand } from '../models/models';
import { UserService } from '../services/user.service';
import { CustomValidators } from './custom-validators';

jest.mock('./custom-validators.constants', () => ({
    get DEFAULT_DATE_FORMAT() {
        return 'DD/MM/YYYY';
    },
}));

describe('CustomValidators', () => {
    const fb = new FormBuilder();

    describe('strictEqual', () => {
        const cases = [
            { controlValue: '', checkValue: '', expected: true },
            { controlValue: 'abcd', checkValue: '', expected: false },
            { controlValue: '', checkValue: 'abcd', expected: false },
            { controlValue: 'abcd', checkValue: 'abcd', expected: true },
        ];

        cases.forEach(({ controlValue, checkValue, expected }) => {
            it(`${controlValue} and ${checkValue} should ${expected ? '' : 'not'} be strictEqual`, () => {
                const control = { value: controlValue } as AbstractControl;
                const result = CustomValidators.strictEqual(checkValue)(control);

                if (expected) {
                    expect(result).toBeNull();
                } else {
                    expect(result).toBeDefined();
                    expect(result?.notEqual).toBeTruthy();
                }
            });
        });
    });

    describe('validDateFormat', () => {
        it('empty string should not be valid', () => {
            const control = { value: '' } as AbstractControl;
            const result = CustomValidators.validDateFormat()(control);

            expect(result).toBeDefined();
            expect(result?.invalidFormat).toBeTruthy();
        });

        it('01/01/2010 should be valid', () => {
            const control = { value: '01/01/2010' } as AbstractControl;
            const result = CustomValidators.validDateFormat()(control);

            expect(result).toBeNull();
        });

        it('42/01/2010 should not be valid', () => {
            const control = { value: '42/01/2010' } as AbstractControl;
            const result = CustomValidators.validDateFormat()(control);

            expect(result).toBeDefined();
            expect(result?.invalidFormat).toBeTruthy();
        });

        it('01/42/2010 should not be valid', () => {
            const control = { value: '01/42/2010' } as AbstractControl;
            const result = CustomValidators.validDateFormat()(control);

            expect(result).toBeDefined();
            expect(result?.invalidFormat).toBeTruthy();
        });
    });

    describe('legalDate', () => {
        it('empty string should not be valid', () => {
            const control = { value: '' } as AbstractControl;
            const result = CustomValidators.legalDate()(control);

            expect(result).toBeDefined();
            expect(result?.invalidFormat).toBeTruthy();
        });

        it('should be valid if 19 years old', () => {
            const old19years = dayjs().subtract(19, 'year').format('DD/MM/YYYY');

            const control = { value: old19years } as AbstractControl;
            const result = CustomValidators.legalDate()(control);

            expect(result).toBeNull();
        });

        it('should have at least 18 years', () => {
            const today = dayjs().format('DD/MM/YYYY');

            const control = { value: today } as AbstractControl;
            const result = CustomValidators.legalDate()(control);

            expect(result).toBeDefined();
            expect(result?.invalidLegalDate).toBeTruthy();
        });

        it('42/01/2010 should not be valid', () => {
            const control = { value: '42/01/2010' } as AbstractControl;
            const result = CustomValidators.legalDate()(control);

            expect(result).toBeDefined();
            expect(result?.invalidFormat).toBeTruthy();
        });

        it('01/42/2010 should not be valid', () => {
            const control = { value: '01/42/2010' } as AbstractControl;
            const result = CustomValidators.legalDate()(control);

            expect(result).toBeDefined();
            expect(result?.invalidFormat).toBeTruthy();
        });
    });

    describe('minimumDate', () => {
        it('empty string should not be valid', () => {
            const control = { value: '' } as AbstractControl;
            const result = CustomValidators.minimumDate('01/01/1900')(control);

            expect(result).toBeDefined();
            expect(result?.invalidFormat).toBeTruthy();
        });

        it('should be valid if 19 years old', () => {
            const old19years = dayjs().subtract(19, 'year').format('DD/MM/YYYY');

            const control = { value: old19years } as AbstractControl;
            const result = CustomValidators.minimumDate('01/01/1900')(control);

            expect(result).toBeNull();
        });

        it('should not be valid if before 01/01/1900', () => {
            const control = { value: '01/01/1899' } as AbstractControl;
            const result = CustomValidators.minimumDate('01/01/1900')(control);

            expect(result).toBeDefined();
            expect(result?.invalidDate).toBeTruthy();
        });

        it('42/01/2010 should not be valid', () => {
            const control = { value: '42/01/2010' } as AbstractControl;
            const result = CustomValidators.minimumDate('01/01/1900')(control);

            expect(result).toBeDefined();
            expect(result?.invalidFormat).toBeTruthy();
        });

        it('01/42/2010 should not be valid', () => {
            const control = { value: '01/42/2010' } as AbstractControl;
            const result = CustomValidators.minimumDate('01/01/1900')(control);

            expect(result).toBeDefined();
            expect(result?.invalidFormat).toBeTruthy();
        });
    });

    describe('strictMax', () => {
        const testCases: {
            name: string;
            value: string | number;
            expected: ValidationErrors | null;
        }[] = [
            { name: `'12' should be null`, value: 12, expected: null },
            { name: `'19.99' should be null`, value: 19.99, expected: null },
            { name: `'0' should be null`, value: 0, expected: null },
            { name: `'-1' should be null`, value: -1, expected: null },
            { name: `'20' should be invalid`, value: 20, expected: { strictMax: true } },
            { name: `'20.01' should be invalid`, value: 20.01, expected: { strictMax: true } },
            { name: `'30' should be invalid`, value: 30, expected: { strictMax: true } },
        ];

        testCases.forEach(testCase => {
            it(testCase.name, () => {
                const control = { value: testCase.value } as AbstractControl;
                const result = CustomValidators.strictMax(20)(control);

                expect(result).toEqual(testCase.expected);
            });
        });
    });

    describe('mustMatch', () => {
        let signUpForm: FormGroup;

        beforeEach(() => {
            signUpForm = fb.group(
                {
                    email: ['', [Validators.required, Validators.email]],
                    emailConfirmation: ['', [Validators.required, Validators.email]],
                },
                {
                    validators: [CustomValidators.mustMatch('email', 'emailConfirmation', { caseInsensitive: true })],
                } as AbstractControlOptions,
            );
        });

        it('should be ok if both email matches with case sensitivity', () => {
            signUpForm.patchValue({
                email: 'xyz@abc.com',
                emailConfirmation: 'xyz@abc.com',
            });

            expect(signUpForm.valid).toBeTruthy();
            expect(signUpForm.controls.email.valid).toBeTruthy();
            expect(signUpForm.controls.emailConfirmation.valid).toBeTruthy();
        });

        it('should not be ok if email does not match', () => {
            signUpForm.patchValue({
                email: 'xyz@abc.com',
                emailConfirmation: 'xyz@abc.net',
            });

            expect(signUpForm.valid).toBeFalsy();
            expect(signUpForm.controls.email.valid).toBeTruthy();
            expect(signUpForm.controls.emailConfirmation.valid).toBeFalsy();
        });

        it('should be ok if both email matches with case insensitivity', () => {
            signUpForm.patchValue({
                email: 'xyz@ABC.com',
                emailConfirmation: 'xyz@abc.com',
            });

            expect(signUpForm.valid).toBeTruthy();
            expect(signUpForm.controls.email.valid).toBeTruthy();
            expect(signUpForm.controls.emailConfirmation.valid).toBeTruthy();
        });
    });

    describe('validIban', () => {
        it('should not be undefined', () => {
            const control = { value: undefined } as AbstractControl;
            const result = CustomValidators.validIban()(control);

            expect(result).toBeDefined();
            expect(result?.required).toBeTruthy();
        });

        it('should not be empty', () => {
            const control = { value: '' } as AbstractControl;
            const result = CustomValidators.validIban()(control);

            expect(result).toBeDefined();
            expect(result?.required).toBeTruthy();
        });

        it('should have correct length (FR)', () => {
            const control = { value: 'FR630006000011234567890189' } as AbstractControl;
            const result = CustomValidators.validIban()(control);

            expect(result).toBeDefined();
            expect(result?.invalidPattern).toBeTruthy();
        });

        it('should have correct checksum', () => {
            const control = { value: 'FR8630006000011234567890189' } as AbstractControl;
            const result = CustomValidators.validIban()(control);

            expect(result).toBeDefined();
            expect(result?.invalidChecksum).toBeTruthy();
        });

        it('FR7630006000011234567890189 should be valid', () => {
            const control = { value: 'FR7630006000011234567890189' } as AbstractControl;
            const result = CustomValidators.validIban()(control);

            expect(result).toBeNull();
        });

        it('BE71096123456769 should be valid', () => {
            const control = { value: 'BE71096123456769' } as AbstractControl;
            const result = CustomValidators.validIban()(control);

            expect(result).toBeNull();
        });

        it('should have correct length (BE)', () => {
            const control = { value: 'BE630006000011234567890189' } as AbstractControl;
            const result = CustomValidators.validIban()(control);

            expect(result).toBeDefined();
            expect(result?.invalidPattern).toBeTruthy();
        });

        it('BE71096123456770 should not be valid', () => {
            const control = { value: 'BE71096123456770' } as AbstractControl;
            const result = CustomValidators.validIban()(control);

            expect(result).toBeDefined();
            expect(result?.invalidChecksum).toBeTruthy();
        });

        it('CI03R74567346892533958799866 should not be valid', () => {
            const control = { value: 'CI03R74567346892533958799866' } as AbstractControl;
            const result = CustomValidators.validIban()(control);

            expect(result).toBeDefined();
            expect(result?.invalidPattern).toBeTruthy();
        });
    });

    describe('validPriceFilter', () => {
        let priceForm: FormGroup;

        beforeEach(() => {
            priceForm = fb.group(
                {
                    minPrice: [null],
                    maxPrice: [null],
                },
                { validators: CustomValidators.validPriceFilter },
            );
        });

        it('should be ok if no price set', () => {
            expect(priceForm.valid).toBeTruthy();
            expect(priceForm.controls.minPrice.valid).toBeTruthy();
            expect(priceForm.controls.maxPrice.valid).toBeTruthy();
        });

        it('should be ok if max price is greater than or equal to 0', () => {
            priceForm.patchValue({
                minPrice: 0,
            });

            expect(priceForm.valid).toBeTruthy();
            expect(priceForm.controls.minPrice.valid).toBeTruthy();
            expect(priceForm.controls.maxPrice.valid).toBeTruthy();
        });

        it('should set error if max price is 0', () => {
            priceForm.patchValue({
                maxPrice: 0,
            });

            expect(priceForm.valid).toBeFalsy();
            expect(priceForm.controls.minPrice.valid).toBeTruthy();
            expect(priceForm.controls.maxPrice.valid).toBeFalsy();
        });

        it('should set error if max price is 0 and min price is also 0', () => {
            priceForm.patchValue({
                minPrice: 0,
                maxPrice: 0,
            });

            expect(priceForm.valid).toBeFalsy();
            expect(priceForm.controls.minPrice.valid).toBeTruthy();
            expect(priceForm.controls.maxPrice.valid).toBeFalsy();
        });

        it('should be ok if max price is greater than or equal to 1', () => {
            priceForm.patchValue({
                maxPrice: 1,
            });

            expect(priceForm.valid).toBeTruthy();
            expect(priceForm.controls.minPrice.valid).toBeTruthy();
            expect(priceForm.controls.maxPrice.valid).toBeTruthy();
        });

        it('should be ok if max price is greater than min price', () => {
            priceForm.patchValue({
                minPrice: 5,
                maxPrice: 10,
            });

            expect(priceForm.valid).toBeTruthy();
            expect(priceForm.controls.minPrice.valid).toBeTruthy();
            expect(priceForm.controls.maxPrice.valid).toBeTruthy();
        });

        it('should not be ok if max price is less than min price', () => {
            priceForm.patchValue({
                minPrice: 15,
                maxPrice: 10,
            });

            expect(priceForm.valid).toBeFalsy();
            expect(priceForm.controls.minPrice.valid).toBeFalsy();
            expect(priceForm.controls.maxPrice.valid).toBeFalsy();
        });
    });

    describe('uniqueNicknameAsyncValidator', () => {
        let userService: UserService;

        beforeEach(() => {
            userService = {
                checkNickname: jest.fn(),
            } as any as UserService;
        });

        it('should not be unique', () => {
            Scheduler.instance!.run(({ expectObservable, flush }) => {
                const input = fb.control('Aplha');

                jest.spyOn(userService, 'checkNickname').mockReturnValue(cold('-(t|)', { t: true }));

                const result$ = CustomValidators.uniqueNicknameAsyncValidator(userService)(input);

                expectObservable(result$).toBe('-(r|)', {
                    r: { notUnique: true },
                });

                flush();

                expect(userService.checkNickname).toHaveBeenCalledWith('Aplha');
            });
        });

        it('should be unique', () => {
            Scheduler.instance!.run(({ expectObservable, flush }) => {
                const input = fb.control('Aplha');

                jest.spyOn(userService, 'checkNickname').mockReturnValue(cold('-(f|)', { f: false }));

                const result$ = CustomValidators.uniqueNicknameAsyncValidator(userService)(input);

                expectObservable(result$).toBe('-(r|)', {
                    r: null,
                });

                flush();

                expect(userService.checkNickname).toHaveBeenCalledWith('Aplha');
            });
        });
    });

    describe('uniqueEmailAsyncValidator', () => {
        let userService: UserService;

        beforeEach(() => {
            userService = {
                checkEmail: jest.fn(),
            } as any as UserService;
        });

        it('should not be unique', () => {
            Scheduler.instance!.run(({ expectObservable, flush }) => {
                const input = fb.control('x@y.z');

                jest.spyOn(userService, 'checkEmail').mockReturnValue(cold('-(t|)', { t: true }));

                const result$ = CustomValidators.uniqueEmailAsyncValidator(userService)(input);

                expectObservable(result$).toBe('-(r|)', {
                    r: { notUnique: true },
                });

                flush();

                expect(userService.checkEmail).toHaveBeenCalledWith('x@y.z');
            });
        });

        it('should be unique', () => {
            Scheduler.instance!.run(({ expectObservable, flush }) => {
                const input = fb.control('x@y.z');

                jest.spyOn(userService, 'checkEmail').mockReturnValue(cold('-(f|)', { f: false }));

                const result$ = CustomValidators.uniqueEmailAsyncValidator(userService)(input);

                expectObservable(result$).toBe('-(r|)', {
                    r: null,
                });

                flush();

                expect(userService.checkEmail).toHaveBeenCalledWith('x@y.z');
            });
        });
    });

    describe('uniqueBrandAsyncValidators', () => {
        const brands: Brand[] = [
            {
                id: 1,
                label: 'Dior',
            } as Brand,
            {
                id: 2,
                label: 'Kiabi',
            } as Brand,
            {
                id: 3,
                label: 'Nike',
            } as Brand,
        ];

        let repositoriesSelectors: RepositoriesSelectors;

        it('should be unique if undefined', () => {
            Scheduler.instance!.run(({ expectObservable }) => {
                repositoriesSelectors = {
                    brands$: cold('-b', { b: brands }),
                } as any as RepositoriesSelectors;

                const input = fb.control(undefined);

                const result$ = CustomValidators.uniqueBrandAsyncValidators(repositoriesSelectors)(input);

                expectObservable(result$).toBe('-(r|)', {
                    r: null,
                });
            });
        });

        it('should not be unique', () => {
            Scheduler.instance!.run(({ expectObservable }) => {
                repositoriesSelectors = {
                    brands$: cold('-b', { b: brands }),
                } as any as RepositoriesSelectors;

                const input = fb.control('Dior');

                const result$ = CustomValidators.uniqueBrandAsyncValidators(repositoriesSelectors)(input);

                expectObservable(result$).toBe('-(r|)', {
                    r: { notUnique: true },
                });
            });
        });

        it('should not be unique without the same casing', () => {
            Scheduler.instance!.run(({ expectObservable }) => {
                repositoriesSelectors = {
                    brands$: cold('-b', { b: brands }),
                } as any as RepositoriesSelectors;

                const input = fb.control('diOr');

                const result$ = CustomValidators.uniqueBrandAsyncValidators(repositoriesSelectors)(input);

                expectObservable(result$).toBe('-(r|)', {
                    r: { notUnique: true },
                });
            });
        });

        it('should return null if unique', () => {
            Scheduler.instance!.run(({ expectObservable }) => {
                repositoriesSelectors = {
                    brands$: cold('-b', { b: brands }),
                } as any as RepositoriesSelectors;

                const input = fb.control('Adidas');

                const result$ = CustomValidators.uniqueBrandAsyncValidators(repositoriesSelectors)(input);

                expectObservable(result$).toBe('-(r|)', {
                    r: null,
                });
            });
        });
    });
});
