import { AbstractControl, FormControl, FormGroup, ValidationErrors, ValidatorFn } from '@angular/forms';
import * as dayjs from 'dayjs';
import { of } from 'rxjs';
import { catchError, first, map } from 'rxjs/operators';
import { RepositoriesSelectors } from 'src/app/store/services/repositories-selectors.service';
import { environment } from '../../../environments/environment';
import { toNumber } from '../functions/number.functions';
import { UserService } from '../services/user.service';
import { DEFAULT_DATE_FORMAT } from './custom-validators.constants';

type MustMatchOptions = {
    caseInsensitive: boolean;
};

export class CustomValidators {
    static NAME_PATTERN = "([A-Za-zÀ-ÖØ-öø-ÿ][-,a-z. ']*)+";
    static MAIL_PATTERN =
        '^(([^<>()[\\]\\\\.,;:\\s@\\"]+(\\.[^<>()[\\]\\\\.,;:\\s@\\"]+)*)|(\\".+\\"))@((\\[\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\])|(([a-zA-Z\\0-9]+\\.)+[a-zA-Z]{2,}))$';

    static strictEqual(value: any): ValidatorFn {
        return control => {
            return control.value === value ? null : { notEqual: true };
        };
    }

    static validDateFormat(): ValidatorFn {
        return control => {
            const date = dayjs(control.value.toString(), DEFAULT_DATE_FORMAT, true);
            return date.isValid() ? null : { invalidFormat: true };
        };
    }

    static legalDate(): ValidatorFn {
        const minimumLegalDate = dayjs().subtract(18, 'year');
        return control => {
            const date = dayjs(control.value.toString(), DEFAULT_DATE_FORMAT, true);

            if (!date.isValid()) {
                return { invalidFormat: true };
            }
            return date.isBefore(minimumLegalDate) ? null : { invalidLegalDate: true };
        };
    }

    static minimumDate(minimumDate: string): ValidatorFn {
        const minDate = dayjs(minimumDate, DEFAULT_DATE_FORMAT);
        return control => {
            const date = dayjs(control.value.toString(), DEFAULT_DATE_FORMAT, true);

            if (!date.isValid()) {
                return { invalidFormat: true };
            }
            return date.isAfter(minDate) ? null : { invalidDate: true };
        };
    }

    static strictMax(max: number): ValidatorFn {
        return control => {
            const value = toNumber(control.value);
            if (!value || isNaN(value)) {
                return null;
            }

            return value >= max ? { strictMax: true } : null;
        };
    }

    static unauthorizedName(): ValidatorFn {
        return control => {
            const value = control.value?.toLowerCase();
            if (!value) {
                return null;
            }

            return environment.unauthorizedNames.map(n => n.toLowerCase()).some(n => value.includes(n))
                ? { unauthorizedName: true }
                : null;
        };
    }

    static mustMatch(controlName: string, matchingControlName: string, options?: MustMatchOptions) {
        return (controls: AbstractControl) => {
            const control = controls.get(controlName);
            const matchingControl = controls.get(matchingControlName);

            if (matchingControl?.errors && !matchingControl.errors.mustMatch) {
                return;
            }

            const withCaseInsensitive = options?.caseInsensitive;

            const hasError = withCaseInsensitive
                ? control?.value?.toLocaleLowerCase() !== matchingControl?.value?.toLocaleLowerCase()
                : control?.value !== matchingControl?.value;

            if (hasError) {
                matchingControl?.setErrors({ mustMatch: true });
            } else {
                matchingControl?.setErrors(null);
            }
        };
    }

    static validIban(): ValidatorFn {
        const isValidChecksum = (iban: string) => {
            const replace1 = iban.slice(4) + iban.slice(0, 4);
            const checksumValue = replace1.replace(/[A-Z]/g, letter => {
                return (letter.charCodeAt(0) - 55).toString();
            });

            return mod97(checksumValue) === 1;
        };

        const mod97 = (str: string) => {
            let checksum: string | number = str.slice(0, 2);
            let fragment: any;
            for (let offset = 2; offset < str.length; offset += 7) {
                fragment = String(checksum) + str.substring(offset, offset + 7);
                checksum = parseInt(fragment, 10) % 97;
            }
            return checksum;
        };

        return control => {
            const iban = control.value;

            if (!iban) {
                return { required: true };
            }

            // regex from with bordering countries : http://ht5ifv.serprest.pt/extensions/tools/IBAN/index.html
            // FR, DE, IT, BE, LU, ES, AD, MC, CH
            const matchRegex =
                /^(?=[0-9A-Z]{24}$)AD\d{10}[0-9A-Z]{12}$|^(?=[0-9A-Z]{16}$)BE\d{14}$|^(?=[0-9A-Z]{27}$)FR\d{12}[0-9A-Z]{11}\d{2}$|^(?=[0-9A-Z]{22}$)DE\d{20}$|^(?=[0-9A-Z]{27}$)IT\d{2}[A-Z]\d{10}[0-9A-Z]{12}$|^(?=[0-9A-Z]{20}$)LU\d{5}[0-9A-Z]{13}$|^(?=[0-9A-Z]{27}$)MC\d{12}[0-9A-Z]{11}\d{2}$|^(?=[0-9A-Z]{24}$)ES\d{22}$|^(?=[0-9A-Z]{21}$)CH\d{7}[0-9A-Z]{12}$/.test(
                    iban,
                );
            if (!matchRegex) {
                return { invalidPattern: true };
            }

            if (!isValidChecksum(iban)) {
                return { invalidChecksum: true };
            }

            return null;
        };
    }

    static moderatedField(oldValue: string | undefined, fieldValid: boolean): ValidatorFn {
        return control => {
            const value = control.value;
            if (!fieldValid && value && oldValue === value) {
                return { fieldModerated: true };
            }
            return null;
        };
    }

    static validPriceFilter(formGroup: FormGroup): ValidationErrors | null {
        const minPriceControl = formGroup.get('minPrice');
        const maxPriceControl = formGroup.get('maxPrice');

        const minPrice = toNumber(minPriceControl?.value);
        const maxPrice = toNumber(maxPriceControl?.value);

        const apply = (validMinPrice: boolean, validMaxPrice: boolean) => {
            if (validMinPrice) {
                minPriceControl?.setErrors(null);
            } else {
                minPriceControl?.setErrors({ invalid: true });
            }

            if (validMaxPrice) {
                maxPriceControl?.setErrors(null);
            } else {
                maxPriceControl?.setErrors({ invalid: true });
            }

            const isValid = validMinPrice && validMaxPrice;

            return isValid ? null : { invalid: true };
        };

        if (minPrice !== undefined && maxPrice !== undefined) {
            if (maxPrice < minPrice) {
                return apply(false, false);
            }
            if (maxPrice < 1) {
                return apply(true, false);
            }

            return apply(true, true);
        }

        if (maxPrice !== undefined) {
            if (maxPrice < 1) {
                return apply(true, false);
            }

            return apply(true, true);
        }

        if (minPrice !== undefined) {
            if (minPrice < 0) {
                return apply(false, true);
            }

            return apply(true, true);
        }

        return apply(true, true);
    }

    static uniqueNicknameAsyncValidator(userService: UserService, currentNickname?: string) {
        return (input: FormControl) => {
            if (currentNickname != null && currentNickname === input.value) {
                return of(null);
            }

            return userService.checkNickname(input.value).pipe(
                map(isTaken => (isTaken ? { notUnique: true } : null)),
                catchError(() => of({ retry: true })),
            );
        };
    }

    static uniqueEmailAsyncValidator(userService: UserService) {
        return (input: FormControl) => {
            return userService.checkEmail(input.value).pipe(
                map(isTaken => (isTaken ? { notUnique: true } : null)),
                catchError(() => of({ retry: true })),
            );
        };
    }

    /**
     * Check if suggested brand is not one of the existing
     */
    static uniqueBrandAsyncValidators(repositoriesSelectors: RepositoriesSelectors) {
        return (input: AbstractControl) => {
            return repositoriesSelectors.brands$.pipe(
                first(),
                map(brands => {
                    const value: string = input.value || '';
                    const lowercaseValue = value.toLowerCase();

                    const relatedBrand = brands.find(b => b.label.toLowerCase() === lowercaseValue);
                    if (relatedBrand) {
                        return { notUnique: true };
                    }

                    return null;
                }),
                catchError(() => of({ retry: true })),
            );
        };
    }
}
