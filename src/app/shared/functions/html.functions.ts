export const encodeHtml = (str: string) => {
    return str.replace(/[\u00A0-\u9999<>\&]/g, i => {
        return '&#' + i.charCodeAt(0) + ';';
    });
};
