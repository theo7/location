export const ARRAY_DELIMITER = ',';

export const randomNumber = (min: number, max: number): number => {
    return Math.floor(Math.random() * (max - min + 1)) + min;
};

export const toNumber = (value: string | number | undefined): number | undefined => {
    if (value === undefined || value === '' || value === null || isNaN(Number(value))) {
        return undefined;
    }
    return Number(value);
};

export function toNumberArray(value: string): number[] {
    return value
        .split(ARRAY_DELIMITER)
        .filter(idStr => !!idStr)
        .map(idStr => Number(idStr))
        .filter(id => !isNaN(id));
}
