import { isUpdateRequired } from './versions.functions';

describe('versions.functions', () => {
    describe('isUpdateRequired', () => {
        const cases = [
            { requiredVersion: '2.2.0', currentVersion: '2.0.0', expected: true },
            { requiredVersion: '2.2.0', currentVersion: '2.4.0', expected: false },
            { requiredVersion: '2.2.0', currentVersion: '2.2.0', expected: false },
            { requiredVersion: '2.12.0', currentVersion: '2.2.0', expected: true },
            { requiredVersion: '2.2.0', currentVersion: '2.12.0', expected: false },
        ];

        cases.forEach(({ requiredVersion, currentVersion, expected }) => {
            it(`${currentVersion} >= ${requiredVersion} should be ${expected}`, () => {
                const result = isUpdateRequired(requiredVersion, currentVersion);
                expect(result).toBe(expected);
            });
        });
    });
});
