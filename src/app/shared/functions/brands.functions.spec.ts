import { Brand } from '../models/models';
import { filterBrands } from './brands.functions';

const kiabiBrand = {
    id: 1,
    label: 'Kiabi',
} as Brand;

const gemoBrand = {
    id: 2,
    label: 'Gémo',
} as Brand;

const adidasBrand = {
    id: 3,
    label: 'Adidas',
} as Brand;

const abacoBrand = {
    id: 4,
    label: 'Abaco',
} as Brand;

const brands = [
    kiabiBrand,
    gemoBrand,
    adidasBrand,
    abacoBrand,
    {
        id: 5,
        label: 'About Ariane',
    } as Brand,
];

const defaultBrand = kiabiBrand;

describe('brands.functions', () => {
    describe('filterBrands', () => {
        it('should be empty array if empty search', () => {
            const result = filterBrands(brands, defaultBrand, '', 10);

            expect(result).toEqual([]);
        });

        it('should be default brand if no match', () => {
            const result = filterBrands(brands, defaultBrand, 'Abraracourcix', 10);

            expect(result).toEqual([defaultBrand]);
        });

        it('should be [matched brand, default brand] if exact match', () => {
            const result = filterBrands(brands, defaultBrand, 'Adidas', 10);

            expect(result).toEqual([adidasBrand, defaultBrand]);
        });

        it('should only show 2 results, and default brand', () => {
            const result = filterBrands(brands, defaultBrand, 'A', 2);

            expect(result).toEqual([adidasBrand, abacoBrand, defaultBrand]);
        });

        it('should not display twice the same brand if default brand match', () => {
            const result = filterBrands(brands, defaultBrand, 'Kiabi', 10);

            expect(result).toEqual([defaultBrand]);
        });

        it('should be case insensitive', () => {
            const result = filterBrands(brands, defaultBrand, 'a', 2);

            expect(result).toEqual([adidasBrand, abacoBrand, defaultBrand]);
        });

        it('should be accent insensitive', () => {
            const result = filterBrands(brands, defaultBrand, 'Gemo', 2);

            expect(result).toEqual([gemoBrand, defaultBrand]);
        });
    });
});
