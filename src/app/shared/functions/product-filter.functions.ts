import { isEqual, mapValues, omit, omitBy } from 'lodash-es';
import {
    Brand,
    Category,
    Color,
    Condition,
    CreateSearch,
    ProductFilter,
    ProductType,
    Search,
    SearchRouterParams,
    Seasonality,
    Size,
    SubCategory,
} from '../models/models';
import { toNumber } from './number.functions';

export const convertParamsToFilter = (
    params: SearchRouterParams,
    categories: Category[],
    brands: Brand[],
    colors: Color[],
    conditions: Condition[],
    sizes: Size[],
): ProductFilter => {
    const filter: ProductFilter = {};

    if (params.category) {
        filter.category = categories.find(c => c.id === toNumber(params.category));
    }

    const subCategories = categories
        .map(c => c.subCategories)
        .filter(sc => sc != undefined && sc.length > 0)
        .flat() as SubCategory[];
    if (params.subCategory) {
        filter.subCategory = subCategories.find(sc => sc.id === toNumber(params.subCategory));
    }

    const productTypes = subCategories
        .map(p => p.productTypes)
        .filter(pt => pt != undefined && pt.length > 0)
        .flat() as ProductType[];
    if (params.productsTypes) {
        const productsTypesArr = params.productsTypes.split(',');
        filter.productsTypes = productTypes.filter(pt => productsTypesArr.some(qpPT => +qpPT === pt.id));
    }
    if (params.brands) {
        const brandsArr = params.brands.split(',');
        const selectedBrands: Brand[] = [];

        brandsArr.forEach(brandId => {
            const brand = brands.find(b => b.id === Number(brandId) || b.slug === brandId);
            if (!!brand) {
                selectedBrands.push(brand);
            }
        });
        filter.brands = selectedBrands;
    }
    if (params.sizes) {
        const sizesArr = params.sizes.split(',');
        filter.sizes = sizes.filter(size => sizesArr.some(qpSize => +qpSize === size.id));
    }
    if (params.colors) {
        const colorsArr = params.colors.split(',');
        filter.colors = colors.filter(color => colorsArr.some(qpColor => +qpColor === color.id));
    }
    if (params.conditions) {
        const conditionsArr = params.conditions.split(',');
        filter.conditions = conditions.filter(condition =>
            conditionsArr.some(qpCondition => +qpCondition === condition.id),
        );
    }
    if (params.seasonality) {
        filter.seasonality = params.seasonality;
    }
    const minPrice = toNumber(params.minPrice);
    if (minPrice !== undefined) {
        filter.minPrice = minPrice;
    }
    const maxPrice = toNumber(params.maxPrice);
    if (maxPrice !== undefined) {
        filter.maxPrice = maxPrice;
    }
    if (params.searchText) {
        filter.searchText = params.searchText;
    }
    if (params.sortField && params.sortOrder) {
        filter.sort = {
            field: params.sortField,
            order: params.sortOrder,
        };
    }

    return filter;
};

export const countFilter = (filter: ProductFilter): number => {
    let counter = 0;

    if (filter.searchText) counter++;
    if (filter.category) counter++;
    if (filter.subCategory) counter++;
    if (filter.minPrice) counter++;
    if (filter.maxPrice) counter++;
    if (filter.seasonality && filter.seasonality !== Seasonality.ALL) counter++;
    if (filter.sort && (filter.sort.field !== 'creationDate' || filter.sort.order !== 'desc')) counter++;

    counter += (filter.productsTypes || []).length;
    counter += (filter.sizes || []).length;
    counter += (filter.brands || []).length;
    counter += (filter.colors || []).length;
    counter += (filter.conditions || []).length;

    return counter;
};

export const shouldSaveFilter = (filter: ProductFilter): boolean => {
    let counter = 0;

    if (filter.searchText) counter++;
    if (filter.subCategory) counter++;
    if (filter.minPrice) counter++;
    if (filter.maxPrice) counter++;
    if (filter.seasonality && filter.seasonality !== Seasonality.ALL) counter++;

    counter += (filter.productsTypes || []).length;
    counter += (filter.sizes || []).length;
    counter += (filter.brands || []).length;
    counter += (filter.colors || []).length;
    counter += (filter.conditions || []).length;

    return counter > 0;
};

export const createSearchWithFilter = (filter: ProductFilter): CreateSearch => {
    const search: CreateSearch = {
        keyword: filter.searchText,
        categoryId: filter.category?.id,
        subCategoryId: filter.subCategory?.id,
        productTypeIds: filter.productsTypes?.map(t => t.id).join(','),
        brandIds: filter.brands?.map(b => b.id).join(','),
        colorIds: filter.colors?.map(c => c.id).join(','),
        conditionIds: filter.conditions?.map(c => c.id).join(','),
        sizeIds: filter.sizes?.map(s => s.id).join(','),
        minPrice: filter.minPrice,
        maxPrice: filter.maxPrice,
        seasonality: filter.seasonality !== Seasonality.ALL ? filter.seasonality : undefined,
    };

    // 💡 : clean undefined, null & empty string / array properties
    return omitBy(search, isEmptyProperty);
};

const optionalSearchProperties = ['id', 'userId', 'saved', 'creationDate', 'lastModifiedDate'];

export const isSearchEqual = (s1: Partial<Search>, s2: Partial<Search>): boolean => {
    // remove undefined & null from object
    let cleanedS1 = omitBy(s1, isEmptyProperty);
    let cleanedS2 = omitBy(s2, isEmptyProperty);

    // trim string properties
    cleanedS1 = mapValues(cleanedS1, cleanProperties);
    cleanedS2 = mapValues(cleanedS2, cleanProperties);

    // keep only useful properties
    cleanedS1 = omit(cleanedS1, optionalSearchProperties);
    cleanedS2 = omit(cleanedS2, optionalSearchProperties);

    // check equality
    return isEqual(cleanedS1, cleanedS2);
};

export const cleanProperties = (property: unknown): any => {
    if (typeof property === 'string') {
        return property.trim();
    }

    return property;
};

export const isEmptyProperty = (property: unknown): boolean => {
    return (
        property === null ||
        property === undefined ||
        (typeof property === 'string' && property.length === 0) ||
        (Array.isArray(property) && property.length === 0)
    );
};

export const convertSearchToQueryParams = (search: Search) => {
    return {
        category: search.categoryId,
        subCategory: search.subCategoryId,
        productsTypes: search.productTypeIds,
        brands: search.brandIds,
        sizes: search.sizeIds,
        colors: search.colorIds,
        conditions: search.conditionIds,
        seasonality: search.seasonality,
        minPrice: search.minPrice,
        maxPrice: search.maxPrice,
        searchText: search.keyword,
    };
};
