import { get as applyLevenshtein } from 'fast-levenshtein';
import { Brand } from '../models/models';

const accentFold = (src: string) => {
    return src.normalize('NFD').replace(/[\u0300-\u036f]/g, '');
};

export const filterBrands = (brands: Brand[], defaultBrand: Brand | undefined, search: string, limit = 10): Brand[] => {
    if (brands && search) {
        const valueLength = search.length;
        const lowercaseValue = accentFold(search.toLowerCase());

        const filteredBrands = brands
            .filter(b => b.id !== defaultBrand?.id)
            .filter(b => {
                const lowercaseLabel = accentFold(b.label.toLowerCase());

                if (valueLength > 3) {
                    const distanceMinimum = valueLength > 6 ? 2 : 1;
                    const distance = applyLevenshtein(lowercaseValue, lowercaseLabel);

                    if (distance <= distanceMinimum) {
                        return true;
                    }
                }

                return accentFold(lowercaseLabel).startsWith(lowercaseValue);
            })
            .slice(0, limit);

        return filteredBrands.concat(defaultBrand ? [defaultBrand] : []);
    }

    return [];
};
