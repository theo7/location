import { isDefined } from './filter.functions';

describe('filter.functions', () => {
    describe('isDefined', () => {
        const cases = [
            {
                arg: null,
                expected: false,
            },
            {
                arg: undefined,
                expected: false,
            },
            {
                arg: '',
                expected: true,
            },
            {
                arg: 'undefined',
                expected: true,
            },
            {
                arg: 0,
                expected: true,
            },
            {
                arg: false,
                expected: true,
            },
        ];

        cases.forEach(({ arg, expected }) => {
            it(`'${arg}' should be ${expected}`, () => {
                const result = isDefined(arg);
                expect(result).toBe(expected);
            });
        });
    });
});
