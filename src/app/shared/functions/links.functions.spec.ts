import { extractRelativePath, isExternalLink } from './links.functions';

describe('links.functions', () => {
    describe('isExternalLink', () => {
        const cases = [
            {
                link: 'http://localhost:4200/chat/kiabi',
                selfOrigin: 'http://localhost:4200',
                expected: false,
            },
            {
                link: 'https://secondemain.kiabi.com/chat/kiabi',
                selfOrigin: 'https://secondemain.kiabi.com',
                expected: false,
            },
            {
                link: 'https://kiabi.com/',
                selfOrigin: 'https://secondemain.kiabi.com',
                expected: true,
            },
            {
                link: '/chat/kiabi',
                selfOrigin: 'https://secondemain.kiabi.com',
                expected: false,
            },
            {
                link: 'mailto:support@kiabi.com',
                selfOrigin: 'https://secondemain.kiabi.com',
                expected: true,
            },
        ];

        cases.forEach(({ link, selfOrigin, expected }) => {
            it(`'${link}' of origin '${selfOrigin}' should be ${expected}`, () => {
                const result = isExternalLink(link, selfOrigin);
                expect(result).toBe(expected);
            });
        });
    });

    describe('extractRelativePath', () => {
        const cases = [
            {
                link: 'https://secondemain.kiabi.com/chat/kiabi',
                expected: '/chat/kiabi',
            },
            {
                link: '/profile',
                expected: '/profile',
            },
        ];

        cases.forEach(({ link, expected }) => {
            it(`'${link}' should be ${expected}`, () => {
                const result = extractRelativePath(link);
                expect(result).toBe(expected);
            });
        });
    });
});
