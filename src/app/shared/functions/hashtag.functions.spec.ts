import { matchHashtags } from './hashtag.functions';

describe('hashtag.functions', () => {
    describe('matchHashtags', () => {
        const cases = [
            {
                arg: undefined,
                expected: null,
            },
            {
                arg: '',
                expected: null,
            },
            {
                arg: 'no hashtag',
                expected: null,
            },
            {
                arg: '#normal',
                expected: ['#normal '],
            },
            {
                arg: '#two #hashtag',
                expected: ['#two ', '#hashtag '],
            },
            {
                arg: '#no<a>',
                expected: null,
            },
        ];

        cases.forEach(({ arg, expected }) => {
            it(`'${arg}' should be ${expected}`, () => {
                const result = matchHashtags(arg);
                expect(result).toEqual(expected);
            });
        });
    });
});
