import * as semverDiff from 'semver-diff';

export const isUpdateRequired = (requiredVersion: string, currentVersion: string): boolean => {
    return semverDiff(currentVersion, requiredVersion) !== undefined;
};
