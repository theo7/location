import { AbstractControl, ValidationErrors } from '@angular/forms';
import { currencyPatternValidator } from './custom-validator.functions';

describe('custom-validator.functions', () => {
    describe('currencyPatternValidator', () => {
        const testCases: {
            name: string;
            value: string;
            expected: ValidationErrors | null;
        }[] = [
            { name: `'12' should be null`, value: '12', expected: null },
            { name: `'12.2' should be null`, value: '12.2', expected: null },
            { name: `'12,99' should be null`, value: '12,99', expected: null },
            { name: `'0,50' should be null`, value: '0,50', expected: null },
            { name: `'' should be invalid`, value: '', expected: { currencyPattern: true } },
            { name: `'1.' should be invalid`, value: '1.', expected: { currencyPattern: true } },
            { name: `'1.201' should be invalid`, value: '1.201', expected: { currencyPattern: true } },
            { name: `'1,201' should be invalid`, value: '1,201', expected: { currencyPattern: true } },
            { name: `',2' should be invalid`, value: ',2', expected: { currencyPattern: true } },
        ];

        testCases.forEach(testCase => {
            it(testCase.name, () => {
                const control = { value: testCase.value } as AbstractControl;
                const result = currencyPatternValidator()(control);

                expect(result).toEqual(testCase.expected);
            });
        });
    });
});
