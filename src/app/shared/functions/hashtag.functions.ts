export const matchHashtags = (text: string | undefined) => {
    if (!text || text.indexOf('#') === -1) {
        return null;
    }

    return (text + ' ').match(/#(\w*[0-9a-zA-Z]+\w*[0-9a-zA-Z]) /g);
};
