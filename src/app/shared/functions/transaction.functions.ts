import { sortBy } from 'lodash-es';
import { CancelReasonEnum, Lot, LotStatusEnum, PaymentMode, PaymentType } from '../models/models';

export const getTransactionId = (lot: Lot) => lot.id;

/**
 * Get total amount paid of the transaction
 */
export const getTransactionAmount = (lot: Lot, isBuyer: boolean) => {
    if (isBuyer) {
        return lot.totalAmount ?? 0;
    }

    return lot.productsFinalPrice ?? 0;
};

export const getShippingFees = (lot: Lot) => {
    const line = lot.paymentLines?.find(p => p.type === PaymentType.TRANSPORT_FEES);
    return line?.amount ?? 0;
};

export const getFixedFees = (lot: Lot) => {
    const line = lot.paymentLines?.find(p => p.type === PaymentType.FIXED_FEES);
    return line?.amount ?? 0;
};

export const getVariableFees = (lot: Lot) => {
    const line = lot.paymentLines?.find(p => p.type === PaymentType.VARIABLE_FEES);
    return line?.amount ?? 0;
};

/**
 * Get second hand fees of the transaction (both variable and fixed fees)
 */
export const getHandlingFees = (lot: Lot) => {
    return getFixedFees(lot) + getVariableFees(lot);
};

export const getTransactionPaymentOptions = (lot: Lot) => {
    const totalAmountLines = lot.paymentLines?.filter(p => p.type === PaymentType.TOTAL_AMOUNT) || [];
    const paymentOptions = totalAmountLines.map(p => p.mode);
    const orderedPaymentOptions = sortBy(paymentOptions, p => p);

    return orderedPaymentOptions.join(',');
};

export const getTotalProductsWithNegociation = (lot: Lot) => {
    const line = lot.paymentLines?.find(p => p.type === PaymentType.PRODUCT);
    return line?.amount ?? 0;
};

export const getPaidInCash = (lot: Lot) => {
    const line = lot.paymentLines?.find(p => p.type === PaymentType.TOTAL_AMOUNT && p.mode === PaymentMode.P2P);
    return line?.amount ?? 0;
};

export const getPaidInCard = (lot: Lot) => {
    const line = lot.paymentLines?.find(p => p.type === PaymentType.TOTAL_AMOUNT && p.mode === PaymentMode.CARD);
    return line?.amount ?? 0;
};

const getFullRefund = (lot: Lot) => {
    return lot.paymentLines?.find(p => p.type === PaymentType.TOTAL_REFUND)?.amount ?? 0;
};
const getPartialRefund = (lot: Lot) => {
    return (lot.paymentLines || [])
        .filter(p => p.type === PaymentType.PRODUCT_REFUND || p.type === PaymentType.PARTIAL_REFUND)
        .map(p => p.amount)
        .reduce((acc, curr) => acc + curr, 0);
};
const getOtherRefund = (lot: Lot) => {
    return (lot.paymentLines || [])
        .filter(p => p.type === PaymentType.COMMERCIAL_GESTURE || p.type === PaymentType.PACKAGE_LOST)
        .map(p => p.amount)
        .reduce((acc, curr) => acc + curr, 0);
};

export const getTransactionRefund = (lot: Lot) => {
    return getFullRefund(lot) + getPartialRefund(lot) + getOtherRefund(lot);
};

export const getTotalPaidAfterRefund = (lot: Lot, isBuyer: boolean) => {
    if (
        lot.status === LotStatusEnum.CANCELLED &&
        lot.cancelReason?.reason === CancelReasonEnum.NOT_VALIDATED &&
        isBuyer
    ) {
        return lot.productsFinalPrice ?? 0;
    }

    if (isBuyer) {
        return getTransactionAmount(lot, true) - getTransactionRefund(lot);
    }

    return getTransactionAmount(lot, false);
};
