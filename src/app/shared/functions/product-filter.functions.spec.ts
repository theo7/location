import { Search, Seasonality, TestCase } from '../models/models';
import {
    cleanProperties,
    convertSearchToQueryParams,
    isEmptyProperty,
    isSearchEqual,
} from './product-filter.functions';

describe('product-filter.functions', () => {
    describe('isEmptyProperty', () => {
        const testCases: TestCase<any, boolean>[] = [
            { params: null, expected: true },
            { params: undefined, expected: true },
            { params: '', expected: true },
            { params: 'test', expected: false },
            { params: [], expected: true },
            { params: [0], expected: false },
            { params: [0, 1], expected: false },
            { params: 0, expected: false },
            { params: 1, expected: false },
        ];

        testCases.forEach(testCase => {
            it(`it should return ${testCase.expected} with ${testCase.params}`, () => {
                const result = isEmptyProperty(testCase.params);
                expect(result).toEqual(testCase.expected);
            });
        });
    });

    describe('isSearchEqual', () => {
        const testCases: TestCase<{ s1: Partial<Search>; s2: Partial<Search> }, boolean>[] = [
            {
                params: {
                    s1: { keyword: undefined, minPrice: 10 },
                    s2: { minPrice: 10 },
                },
                expected: true,
            },
            {
                params: {
                    s1: { colorIds: '1', seasonality: Seasonality.ALL, saved: false },
                    s2: { colorIds: '1', seasonality: Seasonality.ALL, userId: 1, saved: true },
                },
                expected: true,
            },
            {
                params: {
                    s1: { keyword: undefined, minPrice: 10 },
                    s2: { keyword: 'test', minPrice: 10, userId: 1, saved: false },
                },
                expected: false,
            },
            {
                params: {
                    s1: { colorIds: '1,2', minPrice: 8 },
                    s2: { minPrice: 10 },
                },
                expected: false,
            },
        ];

        testCases.forEach(testCase => {
            const { s1, s2 } = testCase.params;

            it(`it should return ${testCase.expected}`, () => {
                const result = isSearchEqual(s1, s2);
                expect(result).toEqual(testCase.expected);
            });
        });
    });

    describe('convertSearchToQueryParams', () => {
        it('should have searchText', () => {
            const input = {
                keyword: 'seller1',
            } as Search;

            const result = convertSearchToQueryParams(input);

            expect(result).toEqual({
                searchText: 'seller1',
            });
        });
    });

    describe('cleanProperties', () => {
        it('should trim string properties', () => {
            const property = 'test ';
            const result = cleanProperties(property);

            expect(result).toBe('test');
        });
    });
});
