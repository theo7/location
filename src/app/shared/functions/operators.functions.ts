import { filter } from 'rxjs/operators';
import { isDefined } from './filter.functions';

export const filterDefined = () => filter(isDefined);
