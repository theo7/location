import { randomNumber, toNumber, toNumberArray } from './number.functions';

describe('number.functions', () => {
    describe('toNumber', () => {
        const testCases: {
            name: string;
            value: any;
            expected: number | undefined;
        }[] = [
            { name: `12 should be 12`, value: 12, expected: 12 },
            { name: `'12' should be 12`, value: '12', expected: 12 },
            { name: `'' should be undefined`, value: '', expected: undefined },
            { name: `'bad' should be undefined`, value: 'bad', expected: undefined },
            { name: `null should be undefined`, value: null, expected: undefined },
            { name: `undefined should be undefined`, value: undefined, expected: undefined },
            { name: `NaN should be undefined`, value: NaN, expected: undefined },
        ];

        testCases.forEach(testCase => {
            it(testCase.name, () => {
                const result = toNumber(testCase.value);
                expect(result).toEqual(testCase.expected);
            });
        });
    });

    describe('toNumberArray', () => {
        const testCases: {
            name: string;
            value: string;
            expected: number[];
        }[] = [
            { name: `'' should be []`, value: '', expected: [] },
            { name: `'12' should be [12]`, value: '12', expected: [12] },
            { name: `'12,' should be [12]`, value: '12,', expected: [12] },
            { name: `'12,2,,1,' should be [12,2,1]`, value: '12,2,,1,', expected: [12, 2, 1] },
            { name: `',,,2,null,' should be [2]`, value: ',,,2,null,', expected: [2] },
        ];

        testCases.forEach(testCase => {
            it(testCase.name, () => {
                const result = toNumberArray(testCase.value);
                expect(result).toEqual(testCase.expected);
            });
        });
    });

    describe('randomNumber', () => {
        const testCases: {
            name: string;
            param: { min: number; max: number };
        }[] = [
            { name: `it should be in [1, 2]`, param: { min: 1, max: 2 } },
            { name: `it should be in [1, 5]`, param: { min: 1, max: 5 } },
            { name: `it should be in [10, 20]`, param: { min: 10, max: 20 } },
        ];
        const ITERATION = 100;

        testCases.forEach(testCase => {
            it(testCase.name, () => {
                for (let i = 0; i < ITERATION; i++) {
                    const result = randomNumber(testCase.param.min, testCase.param.max);
                    expect(result).toBeGreaterThanOrEqual(testCase.param.min);
                    expect(result).toBeLessThanOrEqual(testCase.param.max);
                }
            });
        });
    });
});
