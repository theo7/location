const isRelativeLink = (link: string) => {
    return link[0] === '/';
};

export const isExternalLink = (link: string, selfOrigin: string) => {
    if (isRelativeLink(link)) {
        return false;
    }

    const linkUrl = new URL(link);
    if (linkUrl.origin === selfOrigin) {
        return false;
    }

    return true;
};

export const extractRelativePath = (link: string) => {
    if (isRelativeLink(link)) {
        return link;
    }

    const linkUrl = new URL(link);
    return linkUrl.pathname;
};
