import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';

export function currencyPatternValidator(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
        const currencyRegexp = RegExp('^[0-9]+((,|.)[0-9]{1,2})?$');
        const valid = currencyRegexp.test(control.value);

        return valid ? null : { currencyPattern: true };
    };
}
