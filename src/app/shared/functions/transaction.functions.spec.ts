import { Lot, PaymentLine, PaymentMode, PaymentType } from '../models/models';
import {
    getFixedFees,
    getHandlingFees,
    getPaidInCard,
    getPaidInCash,
    getShippingFees,
    getTotalPaidAfterRefund,
    getTotalProductsWithNegociation,
    getTransactionAmount,
    getTransactionId,
    getTransactionPaymentOptions,
    getTransactionRefund,
    getVariableFees,
} from './transaction.functions';

type TransactionTestCase = {
    name: string;
    lot: Lot;
    expected: {
        shippingFees: number;
        fixedFees: number;
        variableFees: number;
        handlingFees: number;
        paymentOptions: string;
        totalProductsWithNegociation: number;
        paidInCash: number;
        paidInCard: number;
        refund: number;
        totalPaidAfterRefundAsBuyer: number;
        totalPaidAfterRefundAsSeller: number;
    };
};

const transactions = [
    {
        name: 'empty transaction',
        lot: {
            id: 1,
            totalAmount: 0,
            productsFinalPrice: 0,
            paymentLines: [] as PaymentLine[],
        } as Lot,
        expected: {
            shippingFees: 0,
            fixedFees: 0,
            variableFees: 0,
            handlingFees: 0,
            paymentOptions: '',
            totalProductsWithNegociation: 0,
            paidInCash: 0,
            paidInCard: 0,
            refund: 0,
            totalPaidAfterRefundAsBuyer: 0,
            totalPaidAfterRefundAsSeller: 0,
        },
    },
    {
        name: 'transaction with card payment',
        lot: {
            id: 2,
            totalAmount: 100,
            productsFinalPrice: 75,
            paymentLines: [
                {
                    id: 1,
                    type: PaymentType.TRANSPORT_FEES,
                    amount: 20,
                } as PaymentLine,
                {
                    id: 2,
                    type: PaymentType.FIXED_FEES,
                    amount: 5,
                },
                {
                    id: 3,
                    type: PaymentType.VARIABLE_FEES,
                    amount: 10,
                },
                {
                    id: 4,
                    type: PaymentType.PRODUCT,
                    amount: 65,
                },
                {
                    id: 5,
                    type: PaymentType.TOTAL_AMOUNT,
                    mode: PaymentMode.CARD,
                    amount: 100,
                } as PaymentLine,
            ],
        } as Lot,
        expected: {
            shippingFees: 20,
            fixedFees: 5,
            variableFees: 10,
            handlingFees: 15,
            paymentOptions: PaymentMode.CARD,
            totalProductsWithNegociation: 65,
            paidInCash: 0,
            paidInCard: 100,
            refund: 0,
            totalPaidAfterRefundAsBuyer: 100,
            totalPaidAfterRefundAsSeller: 75,
        },
    },
    {
        name: 'transaction with cash payment',
        lot: {
            id: 3,
            totalAmount: 100,
            productsFinalPrice: 75,
            paymentLines: [
                {
                    id: 1,
                    type: PaymentType.TRANSPORT_FEES,
                    amount: 20,
                } as PaymentLine,
                {
                    id: 2,
                    type: PaymentType.FIXED_FEES,
                    amount: 5,
                },
                {
                    id: 3,
                    type: PaymentType.VARIABLE_FEES,
                    amount: 10,
                },
                {
                    id: 4,
                    type: PaymentType.PRODUCT,
                    amount: 65,
                },
                {
                    id: 5,
                    type: PaymentType.TOTAL_AMOUNT,
                    mode: PaymentMode.P2P,
                    amount: 100,
                } as PaymentLine,
            ],
        } as Lot,
        expected: {
            shippingFees: 20,
            fixedFees: 5,
            variableFees: 10,
            handlingFees: 15,
            paymentOptions: PaymentMode.P2P,
            totalProductsWithNegociation: 65,
            paidInCash: 100,
            paidInCard: 0,
            refund: 0,
            totalPaidAfterRefundAsBuyer: 100,
            totalPaidAfterRefundAsSeller: 75,
        },
    },
    {
        name: 'transaction with mixed (cash + card) payment',
        lot: {
            id: 4,
            totalAmount: 100,
            productsFinalPrice: 75,
            paymentLines: [
                {
                    id: 1,
                    type: PaymentType.TRANSPORT_FEES,
                    amount: 20,
                } as PaymentLine,
                {
                    id: 2,
                    type: PaymentType.FIXED_FEES,
                    amount: 5,
                },
                {
                    id: 3,
                    type: PaymentType.VARIABLE_FEES,
                    amount: 10,
                },
                {
                    id: 4,
                    type: PaymentType.PRODUCT,
                    amount: 65,
                },
                {
                    id: 5,
                    type: PaymentType.TOTAL_AMOUNT,
                    mode: PaymentMode.CARD,
                    amount: 60,
                } as PaymentLine,
                {
                    id: 6,
                    type: PaymentType.TOTAL_AMOUNT,
                    mode: PaymentMode.P2P,
                    amount: 40,
                } as PaymentLine,
                {
                    id: 7,
                    type: PaymentType.TOTAL_REFUND,
                    amount: 100,
                } as PaymentLine,
            ],
        } as Lot,
        expected: {
            shippingFees: 20,
            fixedFees: 5,
            variableFees: 10,
            handlingFees: 15,
            paymentOptions: PaymentMode.CARD + ',' + PaymentMode.P2P,
            totalProductsWithNegociation: 65,
            paidInCash: 40,
            paidInCard: 60,
            refund: 100,
            totalPaidAfterRefundAsBuyer: 0,
            totalPaidAfterRefundAsSeller: 75,
        },
    },
] as TransactionTestCase[];

describe('transaction.functions', () => {
    describe('getTransactionId', () => {
        const lot = {
            id: 14,
        } as Lot;

        it(`should be 14`, () => {
            const result = getTransactionId(lot);
            expect(result).toBe(14);
        });
    });

    describe('getTransactionAmount', () => {
        const lot = {
            totalAmount: 100,
            productsFinalPrice: 75,
        } as Lot;

        it(`should be 100 if buyer`, () => {
            const result = getTransactionAmount(lot, true);
            expect(result).toBe(100);
        });

        it(`should be 75 if seller`, () => {
            const result = getTransactionAmount(lot, false);
            expect(result).toBe(75);
        });
    });

    describe('getShippingFees', () => {
        transactions.forEach(({ name, lot, expected }) => {
            const { shippingFees } = expected;

            it(`should be ${shippingFees} if ${name}`, () => {
                const result = getShippingFees(lot);
                expect(result).toBe(shippingFees);
            });
        });
    });

    describe('getFixedFees', () => {
        transactions.forEach(({ name, lot, expected }) => {
            const { fixedFees } = expected;

            it(`should be ${fixedFees} if ${name}`, () => {
                const result = getFixedFees(lot);
                expect(result).toBe(fixedFees);
            });
        });
    });

    describe('getVariableFees', () => {
        transactions.forEach(({ name, lot, expected }) => {
            const { variableFees } = expected;

            it(`should be ${variableFees} if ${name}`, () => {
                const result = getVariableFees(lot);
                expect(result).toBe(variableFees);
            });
        });
    });

    describe('getHandlingFees', () => {
        transactions.forEach(({ name, lot, expected }) => {
            const { handlingFees } = expected;

            it(`should be ${handlingFees} if ${name}`, () => {
                const result = getHandlingFees(lot);
                expect(result).toBe(handlingFees);
            });
        });
    });

    describe('getTransactionPaymentOptions', () => {
        transactions.forEach(({ name, lot, expected }) => {
            const { paymentOptions } = expected;

            it(`should be ${paymentOptions} if ${name}`, () => {
                const result = getTransactionPaymentOptions(lot);
                expect(result).toBe(paymentOptions);
            });
        });

        it(`should be "CARD,P2P" if unordered mixed payment of card and cash`, () => {
            const lot = {
                paymentLines: [
                    {
                        id: 3,
                        amount: 20,
                        type: PaymentType.TOTAL_AMOUNT,
                        mode: PaymentMode.P2P,
                    },
                    {
                        id: 4,
                        amount: 80,
                        type: PaymentType.TOTAL_AMOUNT,
                        mode: PaymentMode.CARD,
                    },
                ] as PaymentLine[],
            } as Lot;

            const result = getTransactionPaymentOptions(lot);
            expect(result).toEqual('CARD,P2P');
        });
    });

    describe('getTotalProductsWithNegociation', () => {
        transactions.forEach(({ name, lot, expected }) => {
            const { totalProductsWithNegociation } = expected;

            it(`should be ${totalProductsWithNegociation} if ${name}`, () => {
                const result = getTotalProductsWithNegociation(lot);
                expect(result).toBe(totalProductsWithNegociation);
            });
        });
    });

    describe('getPaidInCash', () => {
        transactions.forEach(({ name, lot, expected }) => {
            const { paidInCash } = expected;

            it(`should be ${paidInCash} if ${name}`, () => {
                const result = getPaidInCash(lot);
                expect(result).toBe(paidInCash);
            });
        });
    });

    describe('getPaidInCard', () => {
        transactions.forEach(({ name, lot, expected }) => {
            const { paidInCard } = expected;

            it(`should be ${paidInCard} if ${name}`, () => {
                const result = getPaidInCard(lot);
                expect(result).toBe(paidInCard);
            });
        });
    });

    describe('getTransactionRefund', () => {
        transactions.forEach(({ name, lot, expected }) => {
            const { refund } = expected;

            it(`should be ${refund} if ${name}`, () => {
                const result = getTransactionRefund(lot);
                expect(result).toBe(refund);
            });
        });
    });

    describe('getTotalPaidAfterRefund', () => {
        describe('as buyer', () => {
            transactions.forEach(({ name, lot, expected }) => {
                const { totalPaidAfterRefundAsBuyer } = expected;

                it(`should be ${totalPaidAfterRefundAsBuyer} if ${name}`, () => {
                    const result = getTotalPaidAfterRefund(lot, true);
                    expect(result).toBe(totalPaidAfterRefundAsBuyer);
                });
            });
        });

        describe('as seller', () => {
            transactions.forEach(({ name, lot, expected }) => {
                const { totalPaidAfterRefundAsSeller } = expected;

                it(`should be ${totalPaidAfterRefundAsSeller} if ${name}`, () => {
                    const result = getTotalPaidAfterRefund(lot, false);
                    expect(result).toBe(totalPaidAfterRefundAsSeller);
                });
            });
        });
    });
});
