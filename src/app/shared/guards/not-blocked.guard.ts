import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';
import { UserProfileSelectors } from 'src/app/store/services/user-selectors.service';

@Injectable({
    providedIn: 'root',
})
export class NotBlockedGuard implements CanActivate, CanActivateChild {
    constructor(private readonly userProfileSelectors: UserProfileSelectors) {}

    canActivate(): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
        return this.userProfileSelectors.isNotBlockedByAdmin$.pipe(first());
    }

    canActivateChild(): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
        return this.userProfileSelectors.isNotBlockedByAdmin$.pipe(first());
    }
}
