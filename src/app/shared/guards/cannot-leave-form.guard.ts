import { Location } from '@angular/common';
import { Injectable, NgZone } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRouteSnapshot, CanDeactivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { UnsaveChangesDialogComponent } from '../components/unsave-changes-dialog/unsave-changes-dialog.component';

export interface ILeavableFormComponent {
    hasUnsavedData: boolean;
}

@Injectable({
    providedIn: 'root',
})
export class CannotLeaveFormGuard implements CanDeactivate<ILeavableFormComponent> {
    constructor(
        private readonly dialog: MatDialog,
        private readonly translateService: TranslateService,
        private readonly ngZone: NgZone,
        private readonly router: Router,
        private readonly location: Location,
    ) {}

    canDeactivate(
        component: ILeavableFormComponent,
        currentRoute: ActivatedRouteSnapshot,
        currentState: RouterStateSnapshot,
        nextState?: RouterStateSnapshot,
    ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
        if (component.hasUnsavedData) {
            const data = {
                title: this.translateService.instant(currentRoute.data.confirmUnsaveChangesDialogTitle),
                validateLabel: this.translateService.instant('common.validate'),
                cancelLabel: this.translateService.instant('common.cancel'),
            };

            return this.ngZone.run(() => {
                return this.dialog
                    .open(UnsaveChangesDialogComponent, {
                        data,
                    })
                    .afterClosed()
                    .pipe(
                        map(shouldGoBack => {
                            if (shouldGoBack === false) {
                                // @ts-ignore
                                const currentUrlTree = this.router.createUrlTree([], currentRoute);
                                const currentUrl = currentUrlTree.toString();
                                this.location.go(currentUrl);
                                return false;
                            }

                            return true;
                        }),
                    );
            });
        }
        return true;
    }
}
