import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRouteSnapshot, CanActivate, CanActivateChild, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { first, tap } from 'rxjs/operators';
import { UserProfileSelectors } from '../../store/services/user-selectors.service';
import { GoToLoginDialogComponent } from '../components/go-to-login-dialog/go-to-login-dialog.component';

@Injectable({
    providedIn: 'root',
})
export class SignedInGuard implements CanActivate, CanActivateChild {
    constructor(private readonly userProfileSelectors: UserProfileSelectors, private readonly dialog: MatDialog) {}

    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot,
    ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
        return this.userProfileSelectors.isLogged$.pipe(
            first(),
            tap(isLogged => {
                if (!isLogged) {
                    this.dialog.open(GoToLoginDialogComponent, {
                        data: { redirectUrl: state.url },
                    });
                }
            }),
        );
    }

    canActivateChild(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot,
    ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
        return this.canActivate(next, state);
    }
}
