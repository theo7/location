import { CurrencyPipe, LOCATION_INITIALIZED } from '@angular/common';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import '@angular/common/locales/global/fr';
import {
    APP_INITIALIZER,
    CUSTOM_ELEMENTS_SCHEMA,
    DEFAULT_CURRENCY_CODE,
    Injector,
    LOCALE_ID,
    NgModule,
    NgZone,
    PLATFORM_ID,
} from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFirePerformanceModule, PerformanceMonitoringService } from '@angular/fire/performance';
import { MatDialogConfig, MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material/dialog';
import { MatFormFieldDefaultOptions, MAT_FORM_FIELD_DEFAULT_OPTIONS } from '@angular/material/form-field';
import { MAT_SNACK_BAR_DEFAULT_OPTIONS } from '@angular/material/snack-bar';
import { BrowserModule, HammerModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouteReuseStrategy } from '@angular/router';
import { ServiceWorkerModule } from '@angular/service-worker';
import { Capacitor } from '@capacitor/core';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { IonicModule } from '@ionic/angular';
import { EffectsModule } from '@ngrx/effects';
import { routerReducer, StoreRouterConnectingModule } from '@ngrx/router-store';
import { StoreModule } from '@ngrx/store';
import {
    MissingTranslationHandler,
    TranslateCompiler,
    TranslateLoader,
    TranslateModule,
    TranslateService,
} from '@ngx-translate/core';
import * as dayjs from 'dayjs';
import 'dayjs/locale/fr';
import * as calendar from 'dayjs/plugin/calendar';
import * as customParseFormat from 'dayjs/plugin/customParseFormat';
import * as relativeTime from 'dayjs/plugin/relativeTime';
import * as updateLocale from 'dayjs/plugin/updateLocale';
import 'firebase/messaging';
import { RecaptchaSettings, RECAPTCHA_SETTINGS } from 'ng-recaptcha';
import { MESSAGE_FORMAT_CONFIG, TranslateMessageFormatCompiler } from 'ngx-translate-messageformat-compiler';
import { VirtualScrollerModule } from 'ngx-virtual-scroller';
import { forkJoin } from 'rxjs';
import { first } from 'rxjs/operators';
import { environment, envModules } from '../environments/environment';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CustomLinkComponent } from './custom-link/custom-link.component';
import { CustomRouteReuseStrategy } from './custom-route-reuse-strategy';
import { AnalyticsModule } from './features/analytics/analytics.module';
import { AuthenticationModule } from './features/authentication/authentication.module';
import { CookieConsentModule } from './features/cookie-consent/cookie-consent.module';
import { ErrorNotificationModule } from './features/error-notification/error-notification.module';
import { NavigationDesktopModule } from './features/header-desktop/navigation-desktop.module';
import { InstallModule } from './features/install/install.module';
import { KiaLayoutModule } from './features/layout/layout.module';
import { NavigationModule } from './features/navigation/navigation.module';
import { NotificationsModule } from './features/notifications/notifications.module';
import { OfflineModule } from './features/offline/offline.module';
import { InternalLinkComponent } from './internal-link/internal-link.component';
import { CustomMissingTranslationHandler } from './missing-translation-handler';
import {
    DefaultAngularFireDatabase,
    defaultAngularFireDatabaseFactory,
    KiabiAngularFireDatabase,
    kiabiAngularFireDatabaseFactory,
    NotificationAngularFireDatabase,
    notificationAngularFireDatabaseFactory,
} from './multiple-database-factory';
import { MultipleTranslateHttpLoader } from './multiple-translate-http-loader';
import { AppCoreModule } from './shared/components/app-core/app-core.module';
import { ButtonModule } from './shared/components/button/button.module';
import { DirectivesModule } from './shared/directives/directives.module';
import { NativePushService } from './shared/services/native-push.service';
import { NativeShareService } from './shared/services/native-share.service';
import { NavigationHistoryService } from './shared/services/navigation-history.service';
import { PushService } from './shared/services/push.service';
import { ShareService } from './shared/services/share.service';
import { WebPushService } from './shared/services/web-push.service';
import { WebShareService } from './shared/services/web-share.service';
import { SharedModule } from './shared/shared.module';
import { AppStoreModule } from './store/app-store.module';
import { BankDetailsEffects } from './store/effects/bank-details.effects';
import { CategoriesEffects } from './store/effects/categories.effects';
import { ConnectionEffects } from './store/effects/connection.effects';
import { ContactEffects } from './store/effects/contact.effects';
import { CookiesEffects } from './store/effects/cookies.effects';
import { CreditEffects } from './store/effects/credit.effects';
import { DiscussionsEffects } from './store/effects/discussions.effects';
import { FileEffects } from './store/effects/file.effects';
import { FilterEffects } from './store/effects/filter.effects';
import { FollowEffects } from './store/effects/follow.effects';
import { GoogleAnalyticsEffects } from './store/effects/google-analytics.effects';
import { LegalContentEffects } from './store/effects/legal-content.effects';
import { LoggingEffects } from './store/effects/logging.effects';
import { LotEffects } from './store/effects/lot.effects';
import { NotificationsHistoryEffects } from './store/effects/notifications-history.effects';
import { OrdersEffects } from './store/effects/orders.effects';
import { ProductsEffects } from './store/effects/products.effects';
import { ProfileEffects } from './store/effects/profile.effects';
import { RatingsEffects } from './store/effects/ratings.effects';
import { RepositoriesEffects } from './store/effects/repositories.effects';
import { SearchEffects } from './store/effects/search.effects';
import { SeoEffects } from './store/effects/seo.effects';
import { SettingsEffects } from './store/effects/settings.effects';
import { SuggestionEffects } from './store/effects/suggestion.effects';
import { UiEffects } from './store/effects/ui.effects';
import { UserEffects } from './store/effects/user.effects';
import { WebNotificationsEffects } from './store/effects/web-notifications.effects';
import { WishlistEffects } from './store/effects/wishlist.effects';
import { CategoriesSelectors } from './store/services/categories-selectors.service';
import { RepositoriesSelectors } from './store/services/repositories-selectors.service';
import { WebComponentConstants } from './web-component.constants';

dayjs.extend(customParseFormat);
dayjs.extend(relativeTime);
dayjs.extend(updateLocale);
dayjs.extend(calendar);

dayjs.updateLocale('fr', {
    // used in chat (time since last message)
    relativeTime: {
        future: 'dans %s',
        past: (input: string) => {
            if (input.match(/^\d+ s$/)) {
                return '< 1 min';
            }

            return `il y a ${input}`;
        },
        s: '%d s',
        m: '1 min',
        mm: '%d min',
        h: '1 h',
        hh: '%d h',
        d: '1 jour',
        dd: '%d jours',
        M: '1 mois',
        MM: '%d mois',
        y: '1 an',
        yy: '%d ans',
    },
    // used in chat (messages grouped by date)
    calendar: {
        lastDay: '[Hier à] HH:mm',
        sameDay: 'HH:mm',
        lastWeek: 'dddd [à] HH:mm',
        sameElse: 'dddd D MMM YYYY [à] HH:mm',
    },
});

const appearance: MatFormFieldDefaultOptions = {
    appearance: 'outline',
};

export function createTranslateLoader(http: HttpClient) {
    const suffix = '.json?v=' + environment.appVersion;

    return new MultipleTranslateHttpLoader(http, [
        { prefix: './assets/i18n/', suffix },
        { prefix: './assets/i18n/product/', suffix },
        { prefix: './assets/i18n/text/', suffix },
    ]);
}

@NgModule({
    declarations: [AppComponent, CustomLinkComponent, InternalLinkComponent],
    imports: [
        BrowserModule,
        SharedModule,
        HttpClientModule,
        AppRoutingModule,
        AngularFireModule.initializeApp(environment.firebase),
        AngularFireDatabaseModule,
        AngularFirePerformanceModule,
        AuthenticationModule,
        ErrorNotificationModule,
        AnalyticsModule,
        ServiceWorkerModule.register('service-worker.js', { enabled: !Capacitor.isNativePlatform() }),
        NavigationModule,
        NavigationDesktopModule,
        OfflineModule,
        BrowserAnimationsModule,
        HammerModule,
        AppStoreModule,
        IonicModule.forRoot(),
        StoreModule.forRoot({
            router: routerReducer,
        }),
        EffectsModule.forRoot([
            UserEffects,
            DiscussionsEffects,
            CategoriesEffects,
            RepositoriesEffects,
            ConnectionEffects,
            SearchEffects,
            ProductsEffects,
            WebNotificationsEffects,
            BankDetailsEffects,
            UiEffects,
            LoggingEffects,
            WishlistEffects,
            LotEffects,
            SeoEffects,
            OrdersEffects,
            GoogleAnalyticsEffects,
            CookiesEffects,
            ProfileEffects,
            SettingsEffects,
            ContactEffects,
            FilterEffects,
            CreditEffects,
            FileEffects,
            LegalContentEffects,
            FollowEffects,
            SuggestionEffects,
            RatingsEffects,
            NotificationsHistoryEffects,
        ]),
        TranslateModule.forRoot({
            defaultLanguage: 'fr',
            missingTranslationHandler: {
                provide: MissingTranslationHandler,
                useClass: CustomMissingTranslationHandler,
            },
            compiler: {
                provide: TranslateCompiler,
                useClass: TranslateMessageFormatCompiler,
            },
            loader: {
                provide: TranslateLoader,
                useFactory: createTranslateLoader,
                deps: [HttpClient],
            },
        }),
        CookieConsentModule,
        StoreRouterConnectingModule.forRoot(),
        envModules,
        AppCoreModule,
        ButtonModule,
        KiaLayoutModule,
        VirtualScrollerModule, // 💡 : Imported here for "search modal" virtual scroll
        NotificationsModule,
        DirectivesModule,
        InstallModule,
    ],
    providers: [
        { provide: DEFAULT_CURRENCY_CODE, useValue: 'EUR' },
        { provide: LOCALE_ID, useValue: 'fr-FR' },
        { provide: MAT_FORM_FIELD_DEFAULT_OPTIONS, useValue: appearance },
        {
            provide: RECAPTCHA_SETTINGS,
            useValue: { siteKey: environment.reCaptchaKey } as RecaptchaSettings,
        },
        {
            provide: MAT_DIALOG_DEFAULT_OPTIONS,
            useValue: { ...new MatDialogConfig(), autoFocus: false } as MatDialogConfig,
        },
        {
            provide: MAT_SNACK_BAR_DEFAULT_OPTIONS,
            useValue: {
                horizontalPosition: 'end',
                verticalPosition: 'top',
                duration: 1500,
                panelClass: ['snackbar-light'],
            },
        },
        {
            provide: APP_INITIALIZER,
            useFactory: configPreload,
            deps: [RepositoriesSelectors, CategoriesSelectors],
            multi: true,
        },
        {
            provide: APP_INITIALIZER,
            useFactory: loadTranslations,
            deps: [TranslateService, Injector],
            multi: true,
        },
        {
            provide: APP_INITIALIZER,
            useFactory: (ns: NavigationHistoryService) => () => ns.load(),
            deps: [NavigationHistoryService],
            multi: true,
        },
        { provide: MESSAGE_FORMAT_CONFIG, useValue: { locales: ['fr'] } },
        {
            provide: RouteReuseStrategy,
            useClass: CustomRouteReuseStrategy,
        },
        {
            provide: DefaultAngularFireDatabase,
            useFactory: defaultAngularFireDatabaseFactory,
            deps: [PLATFORM_ID, NgZone],
        },
        {
            provide: KiabiAngularFireDatabase,
            useFactory: kiabiAngularFireDatabaseFactory,
            deps: [PLATFORM_ID, NgZone],
        },
        {
            provide: NotificationAngularFireDatabase,
            useFactory: notificationAngularFireDatabaseFactory,
            deps: [PLATFORM_ID, NgZone],
        },
        PerformanceMonitoringService,
        InAppBrowser,
        {
            provide: ShareService,
            useClass: Capacitor.isNativePlatform() ? NativeShareService : WebShareService,
        },
        {
            provide: PushService,
            useClass: Capacitor.isNativePlatform() ? NativePushService : WebPushService,
        },
        CurrencyPipe,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    bootstrap: [AppComponent],
})
export class AppModule {
    constructor(injector: Injector) {
        const el1 = createCustomElement(CustomLinkComponent, { injector });
        customElements.define(WebComponentConstants.customLink, el1);

        const el2 = createCustomElement(InternalLinkComponent, { injector });
        customElements.define(WebComponentConstants.internalLink, el2);
    }
}

export function configPreload(
    repositoriesSelectors: RepositoriesSelectors,
    categoriesSelectors: CategoriesSelectors,
): () => Promise<any> {
    return () =>
        forkJoin([
            categoriesSelectors.categoriesState$.pipe(first(categories => !categories.loading)),
            repositoriesSelectors.repositoriesState$.pipe(first(repositories => !repositories.loading)),
        ]).toPromise();
}

export function loadTranslations(translate: TranslateService, injector: Injector) {
    return () =>
        new Promise<any>((resolve: any) => {
            const locationInitialized = injector.get(LOCATION_INITIALIZED, Promise.resolve(null));
            locationInitialized.then(() => {
                const langToSet = 'fr';
                translate.setDefaultLang(langToSet);
                dayjs.locale(langToSet);
                translate.use(langToSet).subscribe(
                    () => console.log(`Successfully initialized '${langToSet}' language.`),
                    () => console.error(`Problem with '${langToSet}' language initialization.`),
                    () => resolve(),
                );
            });
        });
}
