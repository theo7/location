import { ViewportScroller } from '@angular/common';
import { AfterViewInit, ApplicationRef, ChangeDetectorRef, Component, NgZone, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { App } from '@capacitor/app';
import { Capacitor } from '@capacitor/core';
import { Keyboard } from '@capacitor/keyboard';
import { Platform } from '@ionic/angular';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { StorageMap } from '@ngx-pwa/local-storage';
import {
    fadeInOnEnterAnimation,
    fadeOutOnLeaveAnimation,
    slideInUpOnEnterAnimation,
    slideOutDownOnLeaveAnimation,
} from 'angular-animations';
import * as dayjs from 'dayjs';
import { combineLatest, Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, map, mergeMap, switchMap, take } from 'rxjs/operators';
import { environment } from '../environments/environment';
import { CookieConsentService } from './features/cookie-consent/services/cookie-consent.service';
import { authorizationDialogComponent$, cguModificationDialogComponent$ } from './imports.dynamic';
import { filterDefined } from './shared/functions/operators.functions';
import {
    CguType,
    LOCAL_STORAGE_INSTALL_PANEL,
    LOCAL_STORAGE_NOTIFICATIONS_SETTINGS,
    UserProfileStatus,
} from './shared/models/models';
import { DeviceService } from './shared/services/device.service';
import { NavigationHistoryService } from './shared/services/navigation-history.service';
import { UserService } from './shared/services/user.service';
import { WebPushService } from './shared/services/web-push.service';
import { RouterSelectors } from './store/services/router-selectors.service';
import { UserProfileSelectors } from './store/services/user-selectors.service';

@UntilDestroy()
@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
    animations: [
        fadeInOnEnterAnimation({ duration: 400 }),
        fadeOutOnLeaveAnimation({ duration: 400 }),
        slideInUpOnEnterAnimation(),
        slideOutDownOnLeaveAnimation(),
    ],
})
export class AppComponent implements OnInit, AfterViewInit {
    private APP_PASSWORD_KEY = 'SH_KIABI_PWD';
    private MAINTENANCE_PAGE = '/maintenance';

    isAuthorized = false;
    showBanner = environment.showBanner;
    isKeyboardDisplay = false;

    displayBottomNavigation$ = this.routerSelectors.displayBottomNavigation$.pipe(untilDestroyed(this));
    displayCookieConsentPanel$: Observable<boolean>;
    displayActivateNotifications$: Observable<boolean>;
    displayBlurredBackground$: Observable<boolean>;
    displayInstallPanel$: Observable<boolean>;
    hasFluidContainer$: Observable<boolean>;

    displayIsStableWarning = false;
    userProfileStatus = UserProfileStatus;

    private passwordTmp = '';

    constructor(
        private readonly routerSelectors: RouterSelectors,
        private readonly router: Router,
        private readonly dialog: MatDialog,
        private readonly userService: UserService,
        private readonly cd: ChangeDetectorRef,
        private readonly cookieConsentService: CookieConsentService,
        private readonly viewportScroller: ViewportScroller,
        private readonly webPushService: WebPushService,
        private readonly zone: NgZone,
        public readonly userProfileSelectors: UserProfileSelectors,
        public readonly platform: Platform,
        public readonly device: DeviceService,
        private readonly storage: StorageMap,
        private readonly appRef: ApplicationRef,
        private readonly navigationHistoryService: NavigationHistoryService,
    ) {
        App.addListener('appUrlOpen', (data: any) => {
            this.zone.run(() => {
                const slug = data.url.split(environment.domainExtension).pop();
                if (slug) {
                    this.router.navigateByUrl(slug);
                }
            });
        });

        this.checkCguValidation();

        // set offset position to use anchor
        // @_variables.scss
        const yOffset = device.mode === 'handset' ? 60 : environment.showBanner ? 160 : 120;
        this.viewportScroller.setOffset([0, yOffset]);

        const appPassword = localStorage.getItem(this.APP_PASSWORD_KEY);

        if (
            !environment.requireAuthorization ||
            Capacitor.isNativePlatform() ||
            this.router.url === this.MAINTENANCE_PAGE
        ) {
            this.isAuthorized = true;
        } else if (!!appPassword) {
            this.userService.appAuthorization(appPassword).subscribe(response => {
                if (response) {
                    this.isAuthorized = true;
                } else {
                    this.openAuthorizationDialog();
                }
            });
        } else {
            this.openAuthorizationDialog();
        }

        this.displayCookieConsentPanel$ = this.cookieConsentService.hasChosen$.pipe(
            map(hasChosen => environment.requireCookieConsent && !hasChosen),
            untilDestroyed(this),
        );

        this.displayBlurredBackground$ = combineLatest([
            this.displayCookieConsentPanel$,
            routerSelectors.isCookiesRouteActive$,
        ]).pipe(
            map(
                ([displayCookieConsentPanel, isCookiesRouteActive]) =>
                    displayCookieConsentPanel && !isCookiesRouteActive,
            ),
            untilDestroyed(this),
        );

        const installPanelAlreadyDisplayed$ = this.storage.watch<boolean>(LOCAL_STORAGE_INSTALL_PANEL, {
            type: 'boolean',
        });

        this.displayInstallPanel$ = combineLatest([
            this.displayCookieConsentPanel$,
            routerSelectors.isHomeRouteActive$,
            this.device.isHandset$,
            installPanelAlreadyDisplayed$,
        ]).pipe(
            map(([displayCookieConsentPanel, isHomeRouteActive, isHandset, installPanelAlreadyDisplayed]) => {
                const isAndroidOriOSBrowser = () => {
                    return this.device.isAndroidWebBrowser() || this.device.isiOSWebBrowser();
                };

                return (
                    installPanelAlreadyDisplayed === undefined &&
                    !displayCookieConsentPanel &&
                    isHomeRouteActive &&
                    isHandset &&
                    !Capacitor.isNativePlatform() &&
                    isAndroidOriOSBrowser()
                );
            }),
            untilDestroyed(this),
        );

        const pushActivated$ = this.storage.watch<boolean>(LOCAL_STORAGE_NOTIFICATIONS_SETTINGS, { type: 'boolean' });

        this.displayActivateNotifications$ = combineLatest([userProfileSelectors.isLogged$, pushActivated$]).pipe(
            map(([isLogged, pushActivated]) => {
                return (
                    environment.displayEnableNotificationsBanner &&
                    isLogged &&
                    pushActivated === undefined &&
                    !environment.isNative &&
                    this.webPushService.isSupported()
                );
            }),
            untilDestroyed(this),
        );

        this.hasFluidContainer$ = this.routerSelectors.isHomeRouteActive$.pipe(
            map(hasFluidContainer => !hasFluidContainer),
            untilDestroyed(this),
        );
    }

    ngOnInit(): void {
        // 💡 : Fix back button bug on Android
        App.addListener('backButton', () => {
            this.navigationHistoryService.back();
        });

        // users cannot use app if "ngsw-worker.js" is still registered in their browsers
        // this code is used to unsubscribe the old Service Worker: "ngsw-worker.js"
        // do not remove until mid-2021 :)
        if (navigator.serviceWorker) {
            navigator.serviceWorker.getRegistration().then(swRegistration => {
                if (swRegistration && swRegistration.active) {
                    if (/ngsw-worker.js$/.test(swRegistration.active.scriptURL)) {
                        swRegistration.unregister();
                    }
                }
            });
        }
    }

    ngAfterViewInit() {
        this.cd.detectChanges();

        if (Capacitor.isNativePlatform()) {
            Keyboard.addListener('keyboardWillShow', () => {
                this.isKeyboardDisplay = true;
                this.cd.detectChanges();
            });

            Keyboard.addListener('keyboardWillHide', () => {
                this.isKeyboardDisplay = false;
                this.cd.detectChanges();
            });

            Keyboard.setAccessoryBarVisible({ isVisible: true });
        }
    }

    private checkAppStability() {
        // 💡 : cannot use pipe async because it will not trigger change detection correctly
        this.appRef.isStable
            .pipe(
                distinctUntilChanged(),
                map(isStable => !environment.production && !isStable),
                debounceTime(500),
                untilDestroyed(this),
            )
            .subscribe(displayIsStableWarning => {
                this.displayIsStableWarning = displayIsStableWarning;
                this.cd.detectChanges();
            });
    }

    private openAuthorizationDialog() {
        authorizationDialogComponent$
            .pipe(
                switchMap(AuthorizationDialogComponent => {
                    return this.dialog
                        .open(AuthorizationDialogComponent, { disableClose: true })
                        .afterClosed()
                        .pipe(
                            mergeMap(password => {
                                this.passwordTmp = password;
                                return this.userService.appAuthorization(password);
                            }),
                        );
                }),
            )
            .subscribe(response => {
                if (response) {
                    localStorage.setItem(this.APP_PASSWORD_KEY, this.passwordTmp);
                    this.isAuthorized = true;
                    this.cd.markForCheck();
                    return;
                }

                return this.openAuthorizationDialog();
            });
    }

    private checkCguValidation() {
        const dialogParams = {
            width: '400px',
            disableClose: true,
        };

        // TODO : effect
        // check secondHand cgu
        this.userProfileSelectors.userProfile$
            .pipe(
                filterDefined(),
                take(1),
                map(user => {
                    return [
                        dayjs.unix(user.cguSecondHandValidationDate || 0),
                        dayjs.unix(environment.cguModificationDate.secondHand),
                    ];
                }),
                filter(([validationDate, modificationDate]) => validationDate.isBefore(modificationDate)),
                switchMap(() => cguModificationDialogComponent$),
                switchMap(CguModificationDialogComponent =>
                    this.dialog
                        .open(CguModificationDialogComponent, {
                            data: {
                                type: CguType.SECOND_HAND,
                            },
                            ...dialogParams,
                        })
                        .afterClosed(),
                ),
            )
            .subscribe();

        // TODO : effect
        // check lemonWay cgu
        this.userProfileSelectors.userProfile$
            .pipe(
                filterDefined(),
                take(1),
                map(user => {
                    return [
                        dayjs.unix(user.cguLemonWayValidationDate || 0),
                        dayjs.unix(environment.cguModificationDate.lemonWay),
                    ];
                }),
                filter(([validationDate, modificationDate]) => validationDate.isBefore(modificationDate)),
                switchMap(() => cguModificationDialogComponent$),
                switchMap(CguModificationDialogComponent =>
                    this.dialog
                        .open(CguModificationDialogComponent, {
                            data: {
                                type: CguType.LEMON_WAY,
                            },
                            ...dialogParams,
                        })
                        .afterClosed(),
                ),
            )
            .subscribe();
    }
}
