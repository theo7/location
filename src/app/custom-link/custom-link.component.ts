import { Component, Input } from '@angular/core';

@Component({
    selector: 'app-custom-link',
    templateUrl: './custom-link.component.html',
    styleUrls: ['./custom-link.component.scss'],
})
export class CustomLinkComponent {
    @Input() href?: string;
    @Input() ref?: string;
    @Input() title?: string;
}
