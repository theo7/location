import { Observable, of } from '@angular-devkit/core/node_modules/rxjs';
import { ChangeDetectionStrategy, Component, ViewChild } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { InfiniteScrollDirective } from 'ngx-infinite-scroll';
import { first, switchMap, withLatestFrom } from 'rxjs/operators';
import { UserFollowingProfile, UserProfile, UserProfileStatus, UserType } from 'src/app/shared/models/models';
import { DeviceService } from 'src/app/shared/services/device.service';
import { UserService } from 'src/app/shared/services/user.service';
import { FollowDispatchers } from 'src/app/store/services/follow-dispatchers.service';
import { FollowSelectors } from 'src/app/store/services/follow-selectors.service';
import { ProfileSelectors } from 'src/app/store/services/profile-selectors.service';
import { RouterSelectors } from 'src/app/store/services/router-selectors.service';
import { UserProfileSelectors } from 'src/app/store/services/user-selectors.service';

@UntilDestroy()
@Component({
    selector: 'app-user-followers',
    templateUrl: './user-followers.component.html',
    styleUrls: ['./user-followers.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserFollowersComponent {
    UserProfileStatus = UserProfileStatus;
    UserType = UserType;

    userFollowers$: Observable<UserFollowingProfile[]> = of([]);
    user$: Observable<UserProfile | undefined>;
    isCurrentUser$: Observable<boolean>;
    isFollowersLoading$: Observable<boolean>;

    private page?: number;
    private maxPage?: number;

    hasCallNextCpt = false;
    followersLoading: boolean = false;

    @ViewChild(InfiniteScrollDirective) infiniteScroll?: InfiniteScrollDirective;

    constructor(
        public readonly deviceService: DeviceService,
        private readonly followSelectors: FollowSelectors,
        private readonly userService: UserService,
        private readonly followDispatchers: FollowDispatchers,
        private readonly userProfileSelectors: UserProfileSelectors,
        private readonly routerSelectors: RouterSelectors,
        private readonly profileSelectors: ProfileSelectors,
    ) {
        const currentUser$ = this.userProfileSelectors.userProfile$;

        this.user$ = this.routerSelectors.userId$.pipe(
            withLatestFrom(currentUser$),
            switchMap(([userId, currentUser]) => {
                if (!userId || currentUser?.id === userId) {
                    return currentUser$;
                }

                return this.profileSelectors.current$;
            }),
            untilDestroyed(this),
        );

        this.isCurrentUser$ = this.user$.pipe(
            switchMap(user => {
                if (!user?.id) {
                    return of(false);
                }
                return this.userProfileSelectors.isCurrentUser$(user.id);
            }),
            untilDestroyed(this),
        );

        this.userFollowers$ = this.followSelectors.followers$.pipe(untilDestroyed(this));

        this.followSelectors.followersPage$.pipe(untilDestroyed(this)).subscribe(page => {
            this.page = page;
        });

        this.followSelectors.followersMaxPage$.pipe(untilDestroyed(this)).subscribe(maxPage => {
            if (maxPage !== undefined) {
                this.maxPage = maxPage;
            }
        });

        this.isFollowersLoading$ = this.followSelectors.isFollowersLoading$;

        this.isFollowersLoading$.pipe(untilDestroyed(this)).subscribe(loading => (this.followersLoading = loading));

        this.followDispatchers.resetFollowers();
        this.loadNextPage();
    }

    trackById = (index: number, user: UserFollowingProfile): number => user.id!;

    onScroll(): void {
        if (!this.hasCallNextCpt) {
            this.infiniteScroll?.ngOnDestroy();
            this.infiniteScroll?.setup();
            this.hasCallNextCpt = true;
        }

        this.loadNextPage();
    }

    loadNextPage() {
        const currentUser$ = this.userProfileSelectors.userProfile$;

        currentUser$.pipe(first(), withLatestFrom(this.routerSelectors.userId$)).subscribe(([currentUser, userId]) => {
            const followUserId = userId || currentUser?.id;

            if (
                this.maxPage === undefined ||
                this.page === undefined ||
                (this.maxPage && this.page < this.maxPage && !this.followersLoading)
            ) {
                if (followUserId && this.page !== undefined) {
                    this.followDispatchers.getUserFollower(followUserId, this.page);
                }
            }
        });
    }

    getAvatarUrl(user: UserFollowingProfile): string {
        return this.userService.getAvatarUrl(user);
    }
}
