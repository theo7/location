import { NgModule } from '@angular/core';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { IconsModule } from 'src/app/features/icons/icons.module';
import { HeaderModule } from 'src/app/features/layout/headers/header.module';
import { ChipModule } from 'src/app/shared/components/chip/chip.module';
import { UserInformationModule } from 'src/app/shared/components/user-information/user-information.module';
import { RatingModule } from 'src/app/shared/components/user-rating/rating.module';
import { DirectivesModule } from 'src/app/shared/directives/directives.module';
import { PipesModule } from 'src/app/shared/pipes/pipes.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { UserFollowersRoutingModule } from './user-followers-routing.module';
import { UserFollowersComponent } from './user-followers.component';

@NgModule({
    declarations: [UserFollowersComponent],
    imports: [
        SharedModule,
        IconsModule,
        UserFollowersRoutingModule,
        HeaderModule,
        UserInformationModule,
        ChipModule,
        RatingModule,
        DirectivesModule,
        InfiniteScrollModule,
        PipesModule,
    ],
})
export class UserFollowersModule {}
