import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { of, throwError } from 'rxjs';
import { catchError, first, switchMap, withLatestFrom } from 'rxjs/operators';
import { GoogleAnalyticsService } from 'src/app/features/analytics/google-analytics.service';
import { filterDefined } from 'src/app/shared/functions/operators.functions';
import { RouterSelectors } from 'src/app/store/services/router-selectors.service';
import { GoogleTagService } from '../../features/analytics/google-tag.service';
import { DiscussionStatus } from '../../shared/models/models';
import { LotService } from '../../shared/services/lot.service';

@UntilDestroy()
@Component({
    selector: 'app-payment-callback',
    templateUrl: './payment-callback.component.html',
    styleUrls: ['./payment-callback.component.scss'],
})
export class PaymentCallbackComponent implements OnInit {
    constructor(
        private readonly lotService: LotService,
        private readonly router: Router,
        private readonly routerSelectors: RouterSelectors,
        private readonly googleTagService: GoogleTagService,
        private readonly googleAnalyticsService: GoogleAnalyticsService,
    ) {}

    ngOnInit(): void {
        this.routerSelectors.responseWkToken$
            .pipe(
                withLatestFrom(this.routerSelectors.lotId$),
                switchMap(([responseWkToken, currentLotId]) => {
                    if (!currentLotId) {
                        return throwError('lotId required');
                    }

                    return this.lotService.buyCallback(currentLotId, responseWkToken);
                }),
                catchError(() => {
                    this.router.navigate([''], { replaceUrl: true });
                    return of(null);
                }),
                filterDefined(),
                first(),
                untilDestroyed(this),
            )
            .subscribe(discussion => {
                if (discussion.lot?.id) {
                    this.googleTagService.trackPayment(discussion.lot.id);
                    this.googleAnalyticsService.trackPurchase(discussion.lot);
                    this.googleTagService.trackPurchase(discussion.lot);
                }

                if (
                    discussion.status === DiscussionStatus.DISCUSSION_PENDING ||
                    discussion.status === DiscussionStatus.DISCUSSION_TEMP
                ) {
                    this.router.navigate(['chat', discussion.id], { replaceUrl: true });
                } else {
                    this.router.navigate([''], { replaceUrl: true });
                }
            });
    }
}
