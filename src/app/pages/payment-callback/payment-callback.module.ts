import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { PaymentCallbackRoutingModule } from './payment-callback-routing.module';
import { PaymentCallbackComponent } from './payment-callback.component';

@NgModule({
    declarations: [PaymentCallbackComponent],
    imports: [CommonModule, PaymentCallbackRoutingModule, SharedModule],
})
export class PaymentCallbackModule {}
