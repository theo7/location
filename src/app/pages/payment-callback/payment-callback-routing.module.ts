import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PaymentCallbackComponent } from './payment-callback.component';

const routes: Routes = [
    {
        path: ':lotId',
        component: PaymentCallbackComponent,
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class PaymentCallbackRoutingModule {}
