import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { HeaderModule } from 'src/app/features/layout/headers/header.module';
import { AuthenticationModule } from '../../features/authentication/authentication.module';
import { ProductFormModule } from '../../shared/components/product-form/product-form.module';
import { SimpleDialogModule } from '../../shared/components/simple-dialog/simple-dialog.module';
import { SharedModule } from '../../shared/shared.module';
import { AddProductRoutingModule } from './add-product-routing.module';
import { AddProductComponent } from './add-product.component';

@NgModule({
    declarations: [AddProductComponent],
    imports: [
        SharedModule,
        AddProductRoutingModule,
        AuthenticationModule,
        ProductFormModule,
        HeaderModule,
        MatButtonModule,
        SimpleDialogModule,
    ],
})
export class AddProductModule {}
