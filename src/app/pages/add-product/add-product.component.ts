import { Component } from '@angular/core';
import { UntilDestroy } from '@ngneat/until-destroy';
import { ILeavableFormComponent } from 'src/app/shared/guards/cannot-leave-form.guard';
import { DeviceService } from '../../shared/services/device.service';

@UntilDestroy()
@Component({
    selector: 'app-add-product',
    templateUrl: './add-product.component.html',
    styleUrls: ['./add-product.component.scss'],
})
export class AddProductComponent implements ILeavableFormComponent {
    hasUnsavedData = false;

    constructor(public deviceService: DeviceService) {}

    formDirty(dirty: boolean) {
        this.hasUnsavedData = dirty;
    }

    formSaved() {
        this.hasUnsavedData = false;
    }
}
