import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CannotLeaveFormGuard } from '../../shared/guards/cannot-leave-form.guard';
import { AddProductComponent } from './add-product.component';

const routes: Routes = [
    {
        path: '',
        component: AddProductComponent,
        canDeactivate: [CannotLeaveFormGuard],
        data: {
            confirmUnsaveChangesDialogTitle: 'form.confirmUnsaveChangesDialogTitle.addProduct',
        },
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class AddProductRoutingModule {}
