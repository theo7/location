import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ButtonModule } from '../../shared/components/button/button.module';
import { SharedModule } from '../../shared/shared.module';
import { MaintenanceRoutingModule } from './maintenance-routing.module';
import { MaintenanceComponent } from './maintenance.component';

@NgModule({
    declarations: [MaintenanceComponent],
    imports: [SharedModule, CommonModule, MaintenanceRoutingModule, ButtonModule],
})
export class MaintenanceModule {}
