import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { filter, finalize } from 'rxjs/operators';
import { Health } from '../../shared/models/models';
import { HealthService } from '../../shared/services/health.service';

@Component({
    selector: 'app-maintenance',
    templateUrl: './maintenance.component.html',
    styleUrls: ['./maintenance.component.scss'],
})
export class MaintenanceComponent implements OnInit {
    loading = false;

    constructor(private healthService: HealthService, private router: Router) {}

    ngOnInit(): void {
        this.healthService
            .healthCheck()
            .pipe(filter((health: Health) => health.status === 'UP'))
            .subscribe(() => {
                this.router.navigate([''], { replaceUrl: true });
            });
    }

    refresh() {
        this.loading = true;
        this.healthService
            .healthCheck()
            .pipe(
                filter((health: Health) => health.status === 'UP'),
                finalize(() => (this.loading = false)),
            )
            .subscribe(() => {
                this.router.navigate([''], { replaceUrl: true });
            });
    }
}
