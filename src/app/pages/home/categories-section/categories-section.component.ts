import { ChangeDetectionStrategy, Component } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Category, Indexable } from 'src/app/shared/models/models';
import { CategoriesSelectors } from 'src/app/store/services/categories-selectors.service';
import { SearchDispatchers } from 'src/app/store/services/search-dispatchers.service';

@UntilDestroy()
@Component({
    selector: 'app-categories-section',
    templateUrl: './categories-section.component.html',
    styleUrls: ['./categories-section.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CategoriesSectionComponent {
    categories$: Observable<Indexable<Category>[]>;
    canDisplay$: Observable<boolean>;

    displayBeautifulVersion = false;

    constructor(categoriesSelectors: CategoriesSelectors, private readonly searchDispatchers: SearchDispatchers) {
        this.categories$ = categoriesSelectors.indexedCategories$.pipe(untilDestroyed(this));

        this.canDisplay$ = this.categories$.pipe(
            map(categories => categories.length > 0),
            untilDestroyed(this),
        );
    }

    trackById = (index: number, category: Category) => category.id;

    onLinkClicked() {
        this.searchDispatchers.setShowFilter(false);
    }
}
