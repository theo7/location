import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Router } from '@angular/router';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { combineLatest, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Product } from 'src/app/shared/models/models';
import { DeviceService } from 'src/app/shared/services/device.service';
import { WishlistDispatchers } from 'src/app/store/services/wishlist-dispatchers.service';
import { WishlistSelectors } from 'src/app/store/services/wishlist-selectors.service';

const handsetTotalItems = 10;
const desktopTotalItems = 4;

@UntilDestroy()
@Component({
    selector: 'app-my-favorites-section',
    templateUrl: './my-favorites-section.component.html',
    styleUrls: ['./my-favorites-section.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MyFavoritesSectionComponent {
    displayedProducts$: Observable<Product[]>;
    canDisplay$: Observable<boolean>;

    constructor(
        wishlistDispatchers: WishlistDispatchers,
        wishlistSelectors: WishlistSelectors,
        device: DeviceService,
        private readonly router: Router,
    ) {
        const products$ = wishlistSelectors.wishlist$;

        this.displayedProducts$ = combineLatest([products$, device.isHandset$]).pipe(
            map(([products, isHandset]) => {
                if (isHandset) {
                    return products.slice(0, handsetTotalItems);
                }

                return products.slice(0, desktopTotalItems);
            }),
            untilDestroyed(this),
        );

        const hasEnoughProducts$ = combineLatest([products$, device.isHandset$]).pipe(
            map(([products, isHandset]) => {
                if (isHandset) {
                    return products.length >= handsetTotalItems / 2;
                }

                return products.length >= desktopTotalItems;
            }),
            untilDestroyed(this),
        );

        this.canDisplay$ = combineLatest([wishlistSelectors.isLoading$, hasEnoughProducts$]).pipe(
            map(([isLoading, hasEnoughProducts]) => !isLoading && hasEnoughProducts),
            untilDestroyed(this),
        );

        wishlistDispatchers.load();
    }

    trackById = (index: number, product: Product) => product.id;

    onProductClicked(product: Product) {
        this.router.navigate(['product', product.id]);
    }
}
