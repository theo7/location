import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Router } from '@angular/router';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { combineLatest, Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { Product } from 'src/app/shared/models/models';
import { DeviceService } from 'src/app/shared/services/device.service';
import { ProductsService } from 'src/app/shared/services/products.service';
import { SearchDispatchers } from 'src/app/store/services/search-dispatchers.service';

const hansetItemsPerRow = 2;
const handsetNumberOfRows = 3;
const handsetTotalItems = hansetItemsPerRow * handsetNumberOfRows;

const desktopItemsPerRow = 4;
const desktopNumberOfRows = 3;
const desktopTotalItems = desktopItemsPerRow * desktopNumberOfRows;

const totalItems = Math.max(handsetTotalItems, desktopTotalItems);

@UntilDestroy()
@Component({
    selector: 'app-last-articles-section',
    templateUrl: './last-articles-section.component.html',
    styleUrls: ['./last-articles-section.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LastArticlesSectionComponent {
    displayedProducts$: Observable<Product[]>;
    canDisplay$: Observable<boolean>;

    constructor(
        productsService: ProductsService,
        device: DeviceService,
        private readonly router: Router,
        private readonly searchDispatchers: SearchDispatchers,
    ) {
        const products$ = productsService.getProducts(0, totalItems).pipe(
            map(products => products.content),
            shareReplay(),
        );

        this.displayedProducts$ = combineLatest([products$, device.isHandset$]).pipe(
            map(([products, isHandset]) => {
                if (isHandset) {
                    return products.slice(0, handsetTotalItems);
                }

                return products.slice(0, desktopTotalItems);
            }),
            untilDestroyed(this),
        );

        this.canDisplay$ = this.displayedProducts$.pipe(
            map(displayedProducts => displayedProducts.length > 0),
            untilDestroyed(this),
        );
    }

    trackById = (index: number, product: Product) => product.id;

    onProductClicked(product: Product) {
        this.router.navigate(['product', product.id]);
    }

    onSeeAllButtonClicked() {
        this.searchDispatchers.setShowFilter(false);
    }
}
