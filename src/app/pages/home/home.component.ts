import { Component } from '@angular/core';
import { UserProfileSelectors } from 'src/app/store/services/user-selectors.service';
import { environment } from 'src/environments/environment';
import { DeviceService } from '../../shared/services/device.service';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss'],
})
export class HomeComponent {
    enableNewHomePage = environment.enableNewHomePage;

    constructor(
        public readonly deviceService: DeviceService,
        public readonly userProfileSelectors: UserProfileSelectors,
    ) {}
}
