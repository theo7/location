import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { concatAll, filter, map, toArray } from 'rxjs/operators';
import { Product } from '../../shared/models/models';
import { ProductsService } from '../../shared/services/products.service';

@Injectable({
    providedIn: 'root',
})
export class HomeService {
    private loadingSubject = new BehaviorSubject<boolean>(false);
    private resetLoadingSubject = new BehaviorSubject<boolean>(false);
    private productsSubject = new BehaviorSubject<Product[]>([]);
    private totalElementsSubject = new BehaviorSubject<number | null>(null);

    public loading$ = this.loadingSubject.asObservable();
    public resetLoading$ = this.resetLoadingSubject.asObservable();
    public products$ = this.productsSubject.asObservable();
    public totalElements$ = this.totalElementsSubject.asObservable();

    // fix async problem with ES products (product removed still displayed in product list)
    private removedProducts: number[] = [];
    private page = 0;

    constructor(private productsService: ProductsService) {}

    public addProducts(limit: number) {
        this.page += 1;
        this.loadingSubject.next(true);
        this.productsService.getProducts(this.page, limit).subscribe(products => {
            this.totalElementsSubject.next(products.totalElements);

            const productsWithDuplicates = this.productsSubject.getValue().concat(products.content);
            // removing duplicates
            const uniqProducts = [...new Set(productsWithDuplicates)];

            this.productsSubject.next(uniqProducts);
            this.loadingSubject.next(false);
        });
    }

    public setProducts(limit: number) {
        this.page = 0;
        this.loadingSubject.next(true);
        this.resetLoadingSubject.next(true);
        this.productsService
            .getProducts(0, limit)
            .pipe(
                map(products => products.content),
                concatAll(),
                filter(product => this.removedProducts.indexOf(product.id) === -1),
                toArray(),
            )
            .subscribe(products => {
                this.productsSubject.next(products);
                this.loadingSubject.next(false);
                this.resetLoadingSubject.next(false);
            });
    }

    public removeFromList(productId: number) {
        const products = this.productsSubject.getValue();
        this.removedProducts.push(productId);
        this.productsSubject.next(products.filter(p => p.id !== productId));
    }

    public addToList(product: Product) {
        const products = this.productsSubject.getValue();
        products.unshift(product);
        this.productsSubject.next(products);
    }

    public updateProduct(product: Product) {
        const products = this.productsSubject.getValue();
        products.splice(
            products.findIndex(p => p.id === product.id),
            1,
            product,
        );
        this.productsSubject.next(products);
    }
}
