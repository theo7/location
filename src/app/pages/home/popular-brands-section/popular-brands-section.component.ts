import { ChangeDetectionStrategy, Component } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Brand } from 'src/app/shared/models/models';
import { RepositoriesSelectors } from 'src/app/store/services/repositories-selectors.service';
import { SearchDispatchers } from 'src/app/store/services/search-dispatchers.service';

@UntilDestroy()
@Component({
    selector: 'app-popular-brands-section',
    templateUrl: './popular-brands-section.component.html',
    styleUrls: ['./popular-brands-section.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PopularBrandsSectionComponent {
    popularBrands$: Observable<Brand[]>;
    canDisplay$: Observable<boolean>;

    trackById = (index: number, brand: Brand) => brand.id;

    constructor(
        private readonly repositoriesSelectors: RepositoriesSelectors,
        private readonly searchDispatchers: SearchDispatchers,
    ) {
        this.popularBrands$ = this.repositoriesSelectors.getPopularBrands$;
        this.canDisplay$ = this.popularBrands$.pipe(
            map(brands => brands.length > 0),
            untilDestroyed(this),
        );
    }

    onLinkClicked() {
        this.searchDispatchers.setShowFilter(false);
    }
}
