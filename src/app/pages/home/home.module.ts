import { ScrollingModule } from '@angular/cdk/scrolling';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatChipsModule } from '@angular/material/chips';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { HammerModule } from '@angular/platform-browser';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { IconsModule } from 'src/app/features/icons/icons.module';
import { HeaderModule } from 'src/app/features/layout/headers/header.module';
import { ProductCardModule } from 'src/app/shared/components/product-card/product-card.module';
import { RefresherModule } from '../../features/refresher/refresher.module';
import { ButtonModule } from '../../shared/components/button/button.module';
import { ProductListModule } from '../../shared/components/product-list/product-list.module';
import { DirectivesModule } from '../../shared/directives/directives.module';
import { PipesModule } from '../../shared/pipes/pipes.module';
import { SharedModule } from '../../shared/shared.module';
import { AnonymousHomeComponent } from './anonymous-home/anonymous-home.component';
import { AuthorizedHomeComponent } from './authorized-home/authorized-home.component';
import { CategoriesSectionComponent } from './categories-section/categories-section.component';
import { FollowedDressingSectionComponent } from './followed-dressing-section/followed-dressing-section.component';
import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { LastArticlesSectionComponent } from './last-articles-section/last-articles-section.component';
import { LastSearchSectionComponent } from './last-search-section/last-search-section.component';
import { MyFavoritesSectionComponent } from './my-favorites-section/my-favorites-section.component';
import { PopularBrandsSectionComponent } from './popular-brands-section/popular-brands-section.component';
import { WelcomeSectionComponent } from './welcome-section/welcome-section.component';

@NgModule({
    declarations: [
        HomeComponent,
        AnonymousHomeComponent,
        LastArticlesSectionComponent,
        CategoriesSectionComponent,
        WelcomeSectionComponent,
        AuthorizedHomeComponent,
        MyFavoritesSectionComponent,
        LastSearchSectionComponent,
        PopularBrandsSectionComponent,
        FollowedDressingSectionComponent,
    ],
    imports: [
        SharedModule,
        HomeRoutingModule,
        HammerModule,
        ScrollingModule,
        InfiniteScrollModule,
        MatProgressSpinnerModule,
        ProductListModule,
        HeaderModule,
        ButtonModule,
        MatButtonModule,
        IconsModule,
        DirectivesModule,
        RefresherModule,
        PipesModule,
        ProductCardModule,
        MatChipsModule,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class HomeModule {}
