import { AfterViewChecked, ChangeDetectionStrategy, Component, ElementRef, Renderer2 } from '@angular/core';
import { DeviceService } from 'src/app/shared/services/device.service';

@Component({
    selector: 'app-authorized-home',
    templateUrl: './authorized-home.component.html',
    styleUrls: ['./authorized-home.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AuthorizedHomeComponent implements AfterViewChecked {
    constructor(
        public readonly deviceService: DeviceService,
        private readonly renderer: Renderer2,
        private readonly elementRef: ElementRef<HTMLElement>,
    ) {}

    ngAfterViewChecked(): void {
        const nativeElement = this.elementRef.nativeElement;

        // iterate on each child element, filter empty ones and apply background color
        // 💡 trust me, you cannot do it with :nth-child with CSS
        if (nativeElement) {
            const children = nativeElement.children;

            const notEmptyChildren: Element[] = [];

            for (let i = 0; i < children.length; i++) {
                const child = children.item(i);

                if (child && child.children.length > 0) {
                    notEmptyChildren.push(child);
                }
            }

            notEmptyChildren.forEach((element, index) => {
                if (index % 2 === 0) {
                    this.renderer.setStyle(element, 'background-color', '#f2f2f5');
                } else {
                    this.renderer.setStyle(element, 'background-color', 'white');
                }
            });
        }
    }
}
