import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Params, Router } from '@angular/router';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { sortBy } from 'lodash-es';
import { combineLatest, forkJoin, Observable, of } from 'rxjs';
import { first, map, shareReplay, switchMap, withLatestFrom } from 'rxjs/operators';
import { filterDefined } from 'src/app/shared/functions/operators.functions';
import { convertParamsToFilter, convertSearchToQueryParams } from 'src/app/shared/functions/product-filter.functions';
import { Criterion, Product, Search } from 'src/app/shared/models/models';
import { DeviceService } from 'src/app/shared/services/device.service';
import { ProductsService } from 'src/app/shared/services/products.service';
import { SearchService } from 'src/app/shared/services/search.service';
import { CategoriesSelectors } from 'src/app/store/services/categories-selectors.service';
import { FilterSelectors } from 'src/app/store/services/filter-selectors.service';
import { RepositoriesSelectors } from 'src/app/store/services/repositories-selectors.service';
import { SearchDispatchers } from 'src/app/store/services/search-dispatchers.service';

const hansetItemsPerRow = 4;
const handsetNumberOfRows = 3;
const handsetTotalItems = hansetItemsPerRow * handsetNumberOfRows;

const desktopItemsPerRow = 4;
const desktopNumberOfRows = 1;
const desktopTotalItems = desktopItemsPerRow * desktopNumberOfRows;

const totalItems = Math.max(handsetTotalItems, desktopTotalItems);

@UntilDestroy()
@Component({
    selector: 'app-last-search-section',
    templateUrl: './last-search-section.component.html',
    styleUrls: ['./last-search-section.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LastSearchSectionComponent {
    canDisplay$: Observable<boolean>;

    lastSearch$: Observable<Search | undefined>;
    criteria$: Observable<Observable<Criterion | undefined>[]>;
    displayedProducts$: Observable<Product[]>;
    queryParams$: Observable<Params>;

    constructor(
        productsService: ProductsService,
        device: DeviceService,
        filterSelectors: FilterSelectors,
        private readonly router: Router,
        private readonly categoriesSelectors: CategoriesSelectors,
        private readonly repositoriesSelectors: RepositoriesSelectors,
        searchService: SearchService,
        private readonly searchDispatchers: SearchDispatchers,
    ) {
        this.lastSearch$ = filterSelectors.searchLoading$.pipe(
            first(loading => !loading),
            withLatestFrom(filterSelectors.recentSearch$, filterSelectors.savedSearch$),
            map(([_, recentSearch, savedSearch]) => {
                if (savedSearch.length > 0) {
                    return sortBy(savedSearch, s => s.lastModifiedDate * -1)[0];
                }
                if (recentSearch.length > 0) {
                    return sortBy(recentSearch, s => s.lastModifiedDate * -1)[0];
                }

                return undefined;
            }),
            shareReplay(),
        );

        this.criteria$ = this.lastSearch$.pipe(
            map(search => {
                if (!search) {
                    return [];
                }

                return Object.keys(search)
                    .filter(
                        key => !['id', 'creationDate', 'lastModifiedDate', 'userId', 'saved', 'keyword'].includes(key),
                    )
                    .filter(key => !!search[key])
                    .map(key => searchService.getTranslationKey(search, key))
                    .flatMap(key => key);
            }),
        );

        this.queryParams$ = this.lastSearch$.pipe(
            filterDefined(),
            map(convertSearchToQueryParams),
            untilDestroyed(this),
        );

        const products$ = this.queryParams$.pipe(
            switchMap(queryParams => {
                return forkJoin({
                    params: of(queryParams || {}),
                    categories: this.categoriesSelectors.allCategories$.pipe(first()),
                    brands: this.repositoriesSelectors.brands$.pipe(first()),
                    colors: this.repositoriesSelectors.colors$.pipe(first()),
                    conditions: this.repositoriesSelectors.conditions$.pipe(first()),
                    sizes: this.categoriesSelectors.allSizes$.pipe(first()),
                });
            }),
            map(({ params, categories, brands, colors, conditions, sizes }) => {
                return convertParamsToFilter(params, categories, brands, colors, conditions, sizes);
            }),
            switchMap(filter => productsService.searchProducts(filter, 0, totalItems)),
            map(products => products.content),
            shareReplay(),
        );

        this.displayedProducts$ = combineLatest([products$, device.isHandset$]).pipe(
            map(([products, isHandset]) => {
                if (isHandset) {
                    return products.slice(0, handsetTotalItems);
                }

                return products.slice(0, desktopTotalItems);
            }),
            untilDestroyed(this),
        );

        this.canDisplay$ = combineLatest([this.lastSearch$, this.displayedProducts$]).pipe(
            map(([lastSearch, displayedProducts]) => !!lastSearch && displayedProducts.length >= 4),
            untilDestroyed(this),
        );
    }

    trackById = (index: number, product: Product) => product.id;

    onProductClicked(product: Product) {
        this.router.navigate(['product', product.id]);
    }

    onSeeAllButtonClicked() {
        this.searchDispatchers.setShowFilter(false);
    }
}
