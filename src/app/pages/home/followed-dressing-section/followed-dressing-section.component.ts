import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Router } from '@angular/router';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { combineLatest, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Product } from 'src/app/shared/models/models';
import { DeviceService } from 'src/app/shared/services/device.service';
import { SuggestionDispatchers } from 'src/app/store/services/suggestion-dispatchers.service';
import { SuggestionSelectors } from 'src/app/store/services/suggestion-selectors.service';

const handsetTotalItems = 10;
const desktopTotalItems = 4;

@UntilDestroy()
@Component({
    selector: 'app-followed-dressing-section',
    templateUrl: './followed-dressing-section.component.html',
    styleUrls: ['./followed-dressing-section.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FollowedDressingSectionComponent {
    displayedProducts$: Observable<Product[]>;
    canDisplay$: Observable<boolean>;

    constructor(
        suggestionDispatchers: SuggestionDispatchers,
        suggestionSelectors: SuggestionSelectors,
        device: DeviceService,
        private readonly router: Router,
    ) {
        const products$ = suggestionSelectors.suggestion$;

        this.displayedProducts$ = combineLatest([products$, device.isHandset$]).pipe(
            map(([products, isHandset]) => {
                if (isHandset) {
                    return products.slice(0, handsetTotalItems);
                }

                return products.slice(0, desktopTotalItems);
            }),
            untilDestroyed(this),
        );

        const hasEnoughSuggestion$ = combineLatest([products$, device.isHandset$]).pipe(
            map(([products, isHandset]) => {
                if (isHandset) {
                    return products.length >= handsetTotalItems / 2;
                }

                return products.length >= desktopTotalItems;
            }),
            untilDestroyed(this),
        );

        this.canDisplay$ = combineLatest([suggestionSelectors.isLoading$, hasEnoughSuggestion$]).pipe(
            map(([isLoading, hasEnoughSuggestion]) => !isLoading && hasEnoughSuggestion),
            untilDestroyed(this),
        );

        suggestionDispatchers.loadSuggestion();
    }

    trackById = (index: number, product: Product) => product.id;

    onProductClicked(product: Product) {
        this.router.navigate(['product', product.id]);
    }
}
