import { ChangeDetectionStrategy, Component } from '@angular/core';
import { DeviceService } from 'src/app/shared/services/device.service';

@Component({
    selector: 'app-welcome-section',
    templateUrl: './welcome-section.component.html',
    styleUrls: ['./welcome-section.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WelcomeSectionComponent {
    constructor(public readonly deviceService: DeviceService) {}
}
