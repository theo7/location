import { ChangeDetectionStrategy, Component, ViewEncapsulation } from '@angular/core';
import { DeviceService } from 'src/app/shared/services/device.service';

@Component({
    selector: 'app-guide-photo',
    templateUrl: './guide-photo.component.html',
    styleUrls: ['./guide-photo.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    encapsulation: ViewEncapsulation.None,
})
export class GuidePhotoComponent {
    constructor(public readonly deviceService: DeviceService) {}
}
