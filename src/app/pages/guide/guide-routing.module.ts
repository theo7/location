import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GuidePhotoComponent } from './guide-photo/guide-photo.component';
import { GuideComponent } from './guide.component';

const routes: Routes = [
    {
        path: '',
        component: GuideComponent,
    },
    {
        path: 'photo',
        component: GuidePhotoComponent,
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class GuideRoutingModule {}
