import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IconsModule } from 'src/app/features/icons/icons.module';
import { HeaderModule } from 'src/app/features/layout/headers/header.module';
import { AppCoreModule } from 'src/app/shared/components/app-core/app-core.module';
import { GuidePhotoComponent } from './guide-photo/guide-photo.component';
import { GuideRoutingModule } from './guide-routing.module';
import { GuideComponent } from './guide.component';

@NgModule({
    declarations: [GuideComponent, GuidePhotoComponent],
    imports: [CommonModule, GuideRoutingModule, AppCoreModule, TranslateModule, HeaderModule, IconsModule],
})
export class GuideModule {}
