import { ChangeDetectionStrategy, Component } from '@angular/core';
import { DeviceService } from 'src/app/shared/services/device.service';

@Component({
    selector: 'app-guide',
    templateUrl: './guide.component.html',
    styleUrls: ['./guide.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GuideComponent {
    constructor(public readonly deviceService: DeviceService) {}
}
