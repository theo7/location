import { ChangeDetectorRef, Component, Input, OnInit, ViewChild } from '@angular/core';
import { NavigationEnd, NavigationStart, Router } from '@angular/router';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Observable } from 'rxjs';
import { filter, startWith, withLatestFrom } from 'rxjs/operators';
import { ProductListComponent } from '../../../shared/components/product-list/product-list.component';
import { Product } from '../../../shared/models/models';
import { DeviceService } from '../../../shared/services/device.service';
import { SearchDispatchers } from '../../../store/services/search-dispatchers.service';
import { SearchSelectors } from '../../../store/services/search-selectors.service';

@UntilDestroy()
@Component({
    selector: 'app-search-product-list',
    templateUrl: './search-product-list.component.html',
    styleUrls: ['./search-product-list.component.scss'],
})
export class SearchProductListComponent implements OnInit {
    @ViewChild(ProductListComponent)
    productList?: ProductListComponent;

    @Input()
    parentScrollEl!: Element;

    products: Product[] = [];
    totalElements: number = 0;

    private activated = false;
    private enableScroll = true;

    loading$: Observable<boolean>;
    noProductFound$: Observable<boolean>;

    constructor(
        private readonly cd: ChangeDetectorRef,
        private readonly router: Router,
        private readonly deviceService: DeviceService,
        private readonly searchSelectors: SearchSelectors,
        private readonly searchDispatchers: SearchDispatchers,
    ) {
        this.loading$ = this.searchSelectors.loading$.pipe(untilDestroyed(this));
        this.noProductFound$ = this.searchSelectors.noProductFound$.pipe(untilDestroyed(this));
    }

    ngOnInit(): void {
        let init = true;

        this.searchSelectors.getFilterResults$
            .pipe(withLatestFrom(this.searchSelectors.isFirstPage$), untilDestroyed(this))
            .subscribe(([products, isFirstPage]) => {
                this.enableScroll = false;
                this.cd.detectChanges();
                this.products = products.content;
                this.totalElements = products.totalElements;

                init = false;
                this.enableScroll = true;
            });

        const enterOrRefreshPage$ = this.router.events.pipe(
            filter(e => e instanceof NavigationEnd),
            filter(e => (e as NavigationEnd).url.startsWith('/search')),
            startWith('init-page'),
            withLatestFrom(this.deviceService.isDesktop$),
        );

        enterOrRefreshPage$.pipe(untilDestroyed(this)).subscribe(([e, isDesktop]) => {
            if (this.products.length > 0) {
                this.setScrollState();
            }
            this.activated = true;
            this.cd.detectChanges();
        });

        this.router.events
            .pipe(
                filter(e => e instanceof NavigationStart),
                filter(e => !(e as NavigationStart).url.startsWith('/search')),
                untilDestroyed(this),
            )
            .subscribe(() => {
                this.activated = false;
                this.enableScroll = false;
                this.cd.detectChanges();
            });
    }

    onScroll(): void {
        this.searchDispatchers.executeSearchNext();
    }

    private setScrollState(): void {
        setTimeout(() => this.productList?.scrollToPreviousState());
    }
}
