import { Component, ElementRef, ViewChild, ViewEncapsulation } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { fadeInOnEnterAnimation, fadeOutOnLeaveAnimation } from 'angular-animations';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { DeviceService } from '../../shared/services/device.service';
import { SearchDispatchers } from '../../store/services/search-dispatchers.service';
import { SearchSelectors } from '../../store/services/search-selectors.service';

@UntilDestroy()
@Component({
    selector: 'app-search',
    templateUrl: './search.component.html',
    styleUrls: ['./search.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: [fadeInOnEnterAnimation({ duration: 150 }), fadeOutOnLeaveAnimation({ duration: 300 })],
})
export class SearchComponent {
    @ViewChild('searchListDesktop')
    searchListDesktopEl!: ElementRef<HTMLDivElement>;

    showBanner = environment.showBanner;

    showFilter$: Observable<boolean>;
    activeRefresher$: Observable<boolean>;
    noProductFound$: Observable<boolean>;

    constructor(
        private readonly searchSelectors: SearchSelectors,
        private readonly searchDispatchers: SearchDispatchers,
        readonly deviceService: DeviceService,
    ) {
        this.showFilter$ = this.searchSelectors.showFilter$.pipe(untilDestroyed(this));
        this.activeRefresher$ = this.searchSelectors.activeRefresher$.pipe(untilDestroyed(this));
        this.noProductFound$ = this.searchSelectors.noProductFound$.pipe(untilDestroyed(this));
    }

    refresh(): void {
        this.searchDispatchers.refreshSearch();
    }

    backToTop(): void {
        this.searchListDesktopEl.nativeElement.scrollTo(0, 0);
    }
}
