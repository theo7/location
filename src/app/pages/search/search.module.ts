import { OverlayModule } from '@angular/cdk/overlay';
import { NgModule } from '@angular/core';
import { MatTooltipModule } from '@angular/material/tooltip';
import { IconsModule } from '../../features/icons/icons.module';
import { RefresherModule } from '../../features/refresher/refresher.module';
import { AppCoreModule } from '../../shared/components/app-core/app-core.module';
import { ProductFilterModule } from '../../shared/components/product-filter/product-filter.module';
import { ProductListModule } from '../../shared/components/product-list/product-list.module';
import { DirectivesModule } from '../../shared/directives/directives.module';
import { SharedModule } from '../../shared/shared.module';
import { SearchDesktopFiltersComponent } from './search-desktop-filters/search-desktop-filters.component';
import { SearchHandsetHeaderComponent } from './search-handset-header/search-handset-header.component';
import { SearchIconComponent } from './search-icon/search-icon.component';
import { SearchProductListComponent } from './search-product-list/search-product-list.component';
import { SearchRoutingModule } from './search-routing.module';
import { SearchComponent } from './search.component';

@NgModule({
    declarations: [
        SearchComponent,
        SearchDesktopFiltersComponent,
        SearchHandsetHeaderComponent,
        SearchIconComponent,
        SearchProductListComponent,
    ],
    imports: [
        AppCoreModule,
        SharedModule,
        SearchRoutingModule,
        ProductListModule,
        OverlayModule,
        DirectivesModule,
        RefresherModule,
        ProductFilterModule,
        IconsModule,
        MatTooltipModule,
    ],
    providers: [],
    exports: [SearchComponent],
})
export class SearchModule {}
