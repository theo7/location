import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { FilterType } from '../../../shared/models/models';
import { FilterSelectors } from '../../../store/services/filter-selectors.service';
import { SearchSelectors } from '../../../store/services/search-selectors.service';

@UntilDestroy()
@Component({
    selector: 'app-search-desktop-filters',
    templateUrl: './search-desktop-filters.component.html',
    styleUrls: ['./search-desktop-filters.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SearchDesktopFiltersComponent implements OnInit {
    readonly filterKey: FilterType = 'search';

    hasFilter$?: Observable<boolean>;
    keyword$?: Observable<string | undefined>;
    totalElements$?: Observable<number>;
    loading$?: Observable<boolean>;

    constructor(private readonly filterSelectors: FilterSelectors, private readonly searchSelectors: SearchSelectors) {}

    ngOnInit(): void {
        this.hasFilter$ = this.filterSelectors.isEmpty$(this.filterKey).pipe(
            untilDestroyed(this),
            map(empty => !empty),
        );
        this.keyword$ = this.filterSelectors.keyword$(this.filterKey).pipe(untilDestroyed(this));
        this.totalElements$ = this.searchSelectors.getTotalElements$.pipe(untilDestroyed(this));
        this.loading$ = this.searchSelectors.loading$.pipe(untilDestroyed(this));
    }
}
