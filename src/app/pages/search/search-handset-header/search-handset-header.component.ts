import { Component } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Observable } from 'rxjs';
import { FilterType } from '../../../shared/models/models';
import { FilterDispatchers } from '../../../store/services/filter-dispatchers.service';
import { FilterSelectors } from '../../../store/services/filter-selectors.service';
import { SearchDispatchers } from '../../../store/services/search-dispatchers.service';
import { SearchSelectors } from '../../../store/services/search-selectors.service';

@UntilDestroy()
@Component({
    selector: 'app-search-handset-header',
    templateUrl: './search-handset-header.component.html',
    styleUrls: ['./search-handset-header.component.scss'],
})
export class SearchHandsetHeaderComponent {
    productsCount$: Observable<number>;
    hasFilter$: Observable<boolean>;

    private readonly filterKey: FilterType = 'search';

    constructor(
        private readonly filterDispatchers: FilterDispatchers,
        private readonly filterSelectors: FilterSelectors,
        private readonly searchSelectors: SearchSelectors,
        private readonly searchDispatchers: SearchDispatchers,
    ) {
        this.productsCount$ = this.searchSelectors.getTotalElements$.pipe(untilDestroyed(this));
        this.hasFilter$ = this.filterSelectors.hasFilter$(this.filterKey).pipe(untilDestroyed(this));
    }

    openFiltersDialog(): void {
        this.filterDispatchers.openDialog(this.filterKey);
    }

    openSearchHistoryDialog(): void {
        this.filterDispatchers.openSearchHistoryDialog();
    }

    goBackToFilters(): void {
        this.searchDispatchers.setShowFilter(true);
    }
}
