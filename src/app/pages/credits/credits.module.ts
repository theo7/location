import { ClipboardModule } from '@angular/cdk/clipboard';
import { CurrencyPipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTabsModule } from '@angular/material/tabs';
import { NgxBarcodeModule } from 'ngx-barcode';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { IconsModule } from 'src/app/features/icons/icons.module';
import { HeaderModule } from 'src/app/features/layout/headers/header.module';
import { AppCoreModule } from '../../shared/components/app-core/app-core.module';
import { ButtonModule } from '../../shared/components/button/button.module';
import { PipesModule } from '../../shared/pipes/pipes.module';
import { SharedModule } from '../../shared/shared.module';
import { CashCreditComponent } from './cash-credit/cash-credit.component';
import { CashHistoryComponent } from './cash-credit/cash-history/cash-history.component';
import { RedeemCashDialogComponent } from './cash-credit/redeem-cash-dialog/redeem-cash-dialog.component';
import { CreditsRoutingModule } from './credits-routing.module';
import { CreditsComponent } from './credits.component';
import { RedeemVoucherDialogComponent } from './store-credit/redeem-voucher-dialog/redeem-voucher-dialog.component';
import { StoreCreditComponent } from './store-credit/store-credit.component';
import { VoucherDialogComponent } from './store-credit/voucher-dialog/voucher-dialog.component';

@NgModule({
    declarations: [
        CreditsComponent,
        StoreCreditComponent,
        CashCreditComponent,
        RedeemCashDialogComponent,
        RedeemVoucherDialogComponent,
        VoucherDialogComponent,
        CashHistoryComponent,
    ],
    imports: [
        SharedModule,
        CreditsRoutingModule,
        MatTabsModule,
        MatButtonModule,
        ReactiveFormsModule,
        HeaderModule,
        AppCoreModule,
        ButtonModule,
        NgxBarcodeModule,
        ClipboardModule,
        MatSnackBarModule,
        InfiniteScrollModule,
        IconsModule,
        PipesModule,
    ],
    providers: [CurrencyPipe],
})
export class CreditsModule {}
