import { Component, HostBinding, OnInit, ViewEncapsulation } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core';
import { forkJoin } from 'rxjs';
import { filter, finalize } from 'rxjs/operators';
import { NotificationDialogComponent } from 'src/app/shared/components/notification-dialog/notification-dialog.component';
import { StoreCredit, StoreCreditProperties, Voucher } from '../../shared/models/models';
import { DeviceService } from '../../shared/services/device.service';
import { SettingService } from '../../shared/services/setting.service';
import { StoreCreditService } from '../../shared/services/store-credit.service';
import { RedeemCashDialogComponent } from './cash-credit/redeem-cash-dialog/redeem-cash-dialog.component';
import { RedeemVoucherDialogComponent } from './store-credit/redeem-voucher-dialog/redeem-voucher-dialog.component';

@Component({
    selector: 'app-credits',
    templateUrl: './credits.component.html',
    styleUrls: ['./credits.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class CreditsComponent implements OnInit {
    loading?: boolean;
    credit?: StoreCredit;
    properties?: StoreCreditProperties;
    generatingVoucher?: boolean;
    vouchers: Voucher[] = [];

    private page = 0;

    @HostBinding('class') get class() {
        return this.deviceService.mode;
    }

    constructor(
        private readonly storeCreditService: StoreCreditService,
        private readonly dialog: MatDialog,
        private readonly translateService: TranslateService,
        private readonly settingService: SettingService,
        public readonly deviceService: DeviceService,
    ) {}

    ngOnInit() {
        this.loading = true;
        forkJoin([this.storeCreditService.getStoreCredit(), this.settingService.getStoreCreditProperties()])
            .pipe(finalize(() => (this.loading = false)))
            .subscribe(([credit, properties]) => {
                this.credit = credit;
                this.properties = properties;
            });
        this.getVouchersHistory();
    }

    redeemCash() {
        this.dialog
            .open(RedeemCashDialogComponent, {
                autoFocus: false,
                width: '330px',
                panelClass: 'no-padding-dialog',
                data: {
                    minimumAmount: this.properties?.minimumMoneyOutAmount,
                    amount: this.credit?.cashAmount,
                },
            })
            .afterClosed()
            .pipe(filter(value => !!value))
            .subscribe(value => {
                if (this.credit) {
                    this.credit.cashAmount -= value;
                }

                this.dialog.open(NotificationDialogComponent, {
                    autoFocus: false,
                    width: '330px',
                    panelClass: 'no-padding-dialog',
                    data: {
                        type: 'success',
                        title: this.translateService.instant('profile.cashCredit.redeemCashSuccess'),
                        buttonContent: this.translateService.instant('common.ok'),
                    },
                });
            });
    }

    redeemVoucher() {
        this.dialog
            .open(RedeemVoucherDialogComponent, {
                autoFocus: false,
                width: '330px',
                panelClass: 'no-padding-dialog',
                data: {
                    amount: this.credit?.voucherAmount,
                },
            })
            .afterClosed()
            .pipe(filter(e => e))
            .subscribe(() => {
                if (this.credit) {
                    this.credit.voucherAmount = 0;
                }
                this.page = 0;
                this.getVouchersHistory();
            });
    }

    getNextVouchers() {
        this.page += 1;
        this.storeCreditService.getStoreCreditHistory(this.page).subscribe(vouchers => {
            this.vouchers = [...this.vouchers, ...vouchers.content];
        });
    }

    getVouchersHistory() {
        this.storeCreditService.getStoreCreditHistory(this.page).subscribe(vouchers => {
            this.vouchers = vouchers.content;
        });
    }
}
