import { Observable } from '@angular-devkit/core/node_modules/rxjs';
import { ChangeDetectionStrategy, Component, Input, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { TranslateService } from '@ngx-translate/core';
import { InfiniteScrollDirective } from 'ngx-infinite-scroll';
import { filter, first } from 'rxjs/operators';
import { OrderDetailDialogComponent } from 'src/app/pages/orders/order-detail-dialog/order-detail-dialog.component';
import { SimpleDialogComponent } from 'src/app/shared/components/simple-dialog/simple-dialog.component';
import { CashHistory, Lot, TransactionHistoryType } from 'src/app/shared/models/models';
import { DeviceService } from 'src/app/shared/services/device.service';
import { LotService } from 'src/app/shared/services/lot.service';
import { CreditSelectors } from 'src/app/store/services/credit-selectors.service';
import { DiscussionsSelectors } from 'src/app/store/services/discussions-selectors.service';

@UntilDestroy()
@Component({
    selector: 'app-cash-history',
    templateUrl: './cash-history.component.html',
    styleUrls: ['./cash-history.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CashHistoryComponent {
    @ViewChild(InfiniteScrollDirective) infiniteScroll?: InfiniteScrollDirective;

    loading$: Observable<boolean>;

    @Input()
    cashHistory: CashHistory[] | null = [];

    public TransactionHistoryType = TransactionHistoryType;

    constructor(
        public readonly device: DeviceService,
        private readonly dialog: MatDialog,
        private readonly router: Router,
        private readonly discussionsSelectors: DiscussionsSelectors,
        private readonly lotService: LotService,
        private readonly translateService: TranslateService,
        private readonly creditSelectors: CreditSelectors,
    ) {
        this.loading$ = this.creditSelectors.loading$.pipe(untilDestroyed(this));
    }

    trackById = (index: number, cashHistory: CashHistory): number => cashHistory.id;

    openPopup(ch: CashHistory) {
        if (ch.type === TransactionHistoryType.P2P) {
            this.getLotFromTransactionId(ch.id);
        } else {
            this.openMoneyOutDialog();
        }
    }

    getLotFromTransactionId(id: number) {
        this.lotService.getLotFromTransactionId(id).subscribe((lot: Lot) => {
            if (lot) {
                this.openDetailDialog(lot);
            } else {
                this.openEmptyLotDialog();
            }
        });
    }

    openMoneyOutDialog() {
        this.dialog.open(SimpleDialogComponent, {
            data: {
                title: this.translateService.instant('profile.cashCredit.history.money_out'),
                action: this.translateService.instant('common.ok'),
            },
        });
    }

    openEmptyLotDialog() {
        this.dialog.open(SimpleDialogComponent, {
            data: {
                title: this.translateService.instant('profile.cashCredit.history.empty'),
                action: this.translateService.instant('common.ok'),
            },
        });
    }

    openDetailDialog(lot: Lot): void {
        this.discussionsSelectors
            .discussionByLotId$(lot.id!)
            .pipe(first(), untilDestroyed(this))
            .subscribe(discussion => {
                const dialogDeviceParams = device =>
                    ({
                        handset: {
                            width: '100vw',
                            maxWidth: '100vw',
                            height: '100vh',
                            panelClass: 'full-screen-dialog',
                        },
                        desktop: {
                            height: '80vh',
                            width: '600px',
                            maxWidth: '600px',
                            panelClass: 'no-padding-dialog',
                        },
                    }[device]);

                this.dialog
                    .open(OrderDetailDialogComponent, {
                        autoFocus: false,
                        data: {
                            lot,
                            isBuyer: discussion?.currentUserBuyer,
                        },
                        ...dialogDeviceParams(this.device.mode),
                    })
                    .afterClosed()
                    .pipe(filter(e => e))
                    .subscribe(() => {
                        if (discussion) {
                            this.router.navigate(['chat', discussion.id]);
                        }
                    });
            });
    }
}
