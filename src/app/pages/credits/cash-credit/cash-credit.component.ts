import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { InfiniteScrollDirective } from 'ngx-infinite-scroll';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { CashHistory } from 'src/app/shared/models/models';
import { DeviceService } from 'src/app/shared/services/device.service';
import { BankDetailsDispatchers } from 'src/app/store/services/bank-details-dispatchers.service';
import { BankDetailsSelectors } from 'src/app/store/services/bank-details-selectors.service';
import { CreditDispatchersService } from 'src/app/store/services/credit-dispatchers.service';
import { CreditSelectors } from 'src/app/store/services/credit-selectors.service';

@UntilDestroy()
@Component({
    selector: 'app-cash-credit',
    templateUrl: './cash-credit.component.html',
    styleUrls: ['./cash-credit.component.scss'],
})
export class CashCreditComponent implements OnInit {
    @Input() credit?: number;
    @Input() holdingCredit?: number;
    @Input() minimumAmount?: number;

    @Output()
    redeemCashEvent = new EventEmitter<void>();

    loading$: Observable<boolean>;
    isBankAccountCompleted$: Observable<boolean>;
    isBankAccountIncomplete$: Observable<boolean>;
    isBankAccountPendingValidation$: Observable<boolean>;

    cashHistoryLoading = false;
    hasCallNextCpt = false;

    cashHistory$: Observable<CashHistory[]> = of([]);
    private page;
    private maxPage;
    @ViewChild(InfiniteScrollDirective) infiniteScroll?: InfiniteScrollDirective;

    infiniteScrollSelector: Observable<string>;

    constructor(
        private readonly bankDetailsSelectors: BankDetailsSelectors,
        private readonly bankDetailsDispatchers: BankDetailsDispatchers,
        private readonly creditDispatchersService: CreditDispatchersService,
        public readonly device: DeviceService,
        private readonly creditSelectors: CreditSelectors,
    ) {
        this.loading$ = this.bankDetailsSelectors.loading$.pipe(untilDestroyed(this));
        this.isBankAccountCompleted$ = this.bankDetailsSelectors.isBankAccountCompleted$.pipe(untilDestroyed(this));
        this.isBankAccountIncomplete$ = this.bankDetailsSelectors.isBankAccountIncomplete$.pipe(untilDestroyed(this));
        this.isBankAccountPendingValidation$ = this.bankDetailsSelectors.isBankAccountPendingValidation$.pipe(
            untilDestroyed(this),
        );
        this.infiniteScrollSelector = this.device.isDesktop$.pipe(
            map(isDesktop => (isDesktop ? '' : '.mat-tab-body-content')),
        );
        this.creditDispatchersService.resetCashHistory();
        this.creditSelectors.pageLoad$.pipe(untilDestroyed(this)).subscribe(page => {
            this.page = page;
        });
        this.creditSelectors.maxPages$.pipe(untilDestroyed(this)).subscribe(maxPage => {
            if (maxPage !== undefined) {
                this.maxPage = maxPage;
            }
        });
        this.cashHistory$ = this.creditSelectors.getHistory$.pipe(untilDestroyed(this));
        this.creditSelectors.loading$
            .pipe(untilDestroyed(this))
            .subscribe(loading => (this.cashHistoryLoading = loading));
        this.getCashHistory();
    }

    ngOnInit() {
        this.bankDetailsDispatchers.loadBankDetails();
    }

    openTransferCreditDialog() {
        this.redeemCashEvent.emit();
    }

    onScroll(): void {
        if (!this.hasCallNextCpt) {
            this.infiniteScroll?.ngOnDestroy();
            this.infiniteScroll?.setup();
            this.hasCallNextCpt = true;
        }
        this.getCashHistory();
    }

    getCashHistory() {
        if (this.maxPage === undefined || (this.maxPage && this.page < this.maxPage && !this.cashHistoryLoading)) {
            this.creditDispatchersService.getCashHistory(this.page);
        }
    }
}
