import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { finalize } from 'rxjs/operators';
import { currencyPatternValidator } from '../../../../shared/functions/custom-validator.functions';
import { StoreCreditService } from '../../../../shared/services/store-credit.service';

@Component({
    selector: 'app-redeem-cash-dialog',
    templateUrl: './redeem-cash-dialog.component.html',
    styleUrls: ['./redeem-cash-dialog.component.scss'],
})
export class RedeemCashDialogComponent implements OnInit {
    amountControl?: FormControl;
    minimumAmount = 0;
    amount = 0;
    loading?: boolean;

    constructor(
        private formBuilder: FormBuilder,
        @Inject(MAT_DIALOG_DATA) private data: any,
        private dialogRef: MatDialogRef<RedeemCashDialogComponent>,
        private storeCreditService: StoreCreditService,
    ) {}

    ngOnInit() {
        this.minimumAmount = this.data.minimumAmount;
        this.amount = this.data.amount;
        this.amountControl = this.formBuilder.control('', [
            Validators.required,
            Validators.min(this.minimumAmount),
            Validators.max(this.amount),
            currencyPatternValidator(),
        ]);
    }

    transferCredit() {
        if (this.amountControl) {
            this.loading = true;

            this.storeCreditService
                .redeemCash(this.amountControl.value)
                .pipe(finalize(() => (this.loading = false)))
                .subscribe(() => {
                    this.dialogRef.close(this.amountControl?.value);
                });
        }
    }
}
