import { Clipboard } from '@angular/cdk/clipboard';
import { Component, Inject, ViewEncapsulation } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TranslateService } from '@ngx-translate/core';
import { Voucher } from '../../../../shared/models/models';

@Component({
    selector: 'app-voucher-dialog',
    templateUrl: './voucher-dialog.component.html',
    styleUrls: ['./voucher-dialog.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class VoucherDialogComponent {
    voucher: Voucher;

    constructor(
        @Inject(MAT_DIALOG_DATA) private data: any,
        private clipboard: Clipboard,
        private snackBar: MatSnackBar,
        private translateService: TranslateService,
    ) {
        this.voucher = data.voucher;
    }

    copyIdCard() {
        this.clipboard.copy(this.voucher.idCard);
        this.snackBar.open(this.translateService.instant('snackbar.copy'), '', {
            duration: 500,
        });
    }
}
