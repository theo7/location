import {
    Component,
    EventEmitter,
    Input,
    OnChanges,
    Output,
    SimpleChanges,
    ViewChild,
    ViewEncapsulation,
} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import * as dayjs from 'dayjs';
import { InfiniteScrollDirective } from 'ngx-infinite-scroll';
import { environment } from '../../../../environments/environment';
import { StoreCredit, Voucher, VoucherStatus } from '../../../shared/models/models';
import { DeviceService } from '../../../shared/services/device.service';
import { VoucherDialogComponent } from './voucher-dialog/voucher-dialog.component';

@Component({
    selector: 'app-store-credit',
    templateUrl: './store-credit.component.html',
    styleUrls: ['./store-credit.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class StoreCreditComponent implements OnChanges {
    @ViewChild(InfiniteScrollDirective) infiniteScroll?: InfiniteScrollDirective;

    @Output() redeemVoucherEvent = new EventEmitter<any>();
    @Output() nextVouchers = new EventEmitter<any>();

    @Input() vouchers: Voucher[] = [];
    @Input() credit?: StoreCredit;
    @Input() generating?: boolean;

    loading = true;
    voucherStatus = VoucherStatus;
    hasCallNextCpt = false;

    minimumVoucherAmount = environment.storeCredit.minimumVoucherAmount;

    constructor(private readonly dialog: MatDialog, public readonly device: DeviceService) {}

    ngOnChanges(changes: SimpleChanges): void {
        this.loading = false;
    }

    get progressBarWidth() {
        return this.credit
            ? `${(this.credit.yearlyTotalContribution / this.credit.yearlyMaxContribution) * 100}%`
            : '0%';
    }

    getAvailabilityDate(voucher: Voucher): number {
        return dayjs(voucher.lastModifiedDate).add(1, 'year').unix() * 1000;
    }

    redeemVoucher(): void {
        this.redeemVoucherEvent.emit(true);
    }

    isAfterToday(voucher: Voucher): boolean {
        return (
            dayjs(this.getAvailabilityDate(voucher)).isAfter(dayjs()) &&
            !this.isInLessThanAMonth(voucher) &&
            voucher.status === VoucherStatus.SEND
        );
    }

    isInLessThanAMonth(voucher: Voucher): boolean {
        return (
            !this.isNotAvailable(voucher) &&
            dayjs(this.getAvailabilityDate(voucher)).isBefore(dayjs().add(1, 'month')) &&
            voucher.status === VoucherStatus.SEND
        );
    }

    isNotAvailable(voucher: Voucher): boolean {
        return dayjs(this.getAvailabilityDate(voucher)).isBefore(dayjs()) && voucher.status === VoucherStatus.SEND;
    }

    openVoucherDialog(voucher: Voucher): void {
        if (voucher.status === VoucherStatus.SEND) {
            const dialogDeviceParams = device =>
                ({
                    handset: {
                        width: '100vw',
                        maxWidth: '100vw',
                        height: '100vh',
                        panelClass: 'full-screen-dialog',
                    },
                    desktop: {
                        width: '600px',
                        height: '700px',
                        panelClass: 'full-screen-rounded-dialog',
                    },
                }[device]);
            this.dialog.open(VoucherDialogComponent, {
                data: {
                    voucher,
                },
                ...dialogDeviceParams(this.device.mode),
            });
        }
    }

    onScroll(): void {
        if (!this.hasCallNextCpt) {
            this.infiniteScroll?.ngOnDestroy();
            this.infiniteScroll?.setup();
            this.hasCallNextCpt = true;
        }
        this.loading = true;
        this.nextVouchers.emit();
    }
}
