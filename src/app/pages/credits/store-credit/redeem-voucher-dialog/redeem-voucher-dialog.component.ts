import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { StoreCreditService } from '../../../../shared/services/store-credit.service';
import { UserProfileSelectors } from '../../../../store/services/user-selectors.service';

@Component({
    selector: 'app-redeem-vouher-dialog-dialog',
    templateUrl: './redeem-voucher-dialog.component.html',
    styleUrls: ['./redeem-voucher-dialog.component.scss'],
})
export class RedeemVoucherDialogComponent implements OnInit {
    amount?: number;
    loading?: boolean;
    generatingSuccess?: boolean;
    generatingFailed?: boolean;
    mail$: Observable<string | undefined>;

    constructor(
        @Inject(MAT_DIALOG_DATA) private readonly data: any,
        private readonly dialogRef: MatDialogRef<RedeemVoucherDialogComponent>,
        private readonly storeCreditService: StoreCreditService,
        userProfileSelectors: UserProfileSelectors,
    ) {
        this.mail$ = userProfileSelectors.mail$;
    }

    ngOnInit() {
        this.amount = this.data.amount;
    }

    redeemVoucher() {
        this.loading = true;
        this.storeCreditService.redeemVoucher().subscribe(
            () => {
                this.loading = false;
                this.generatingSuccess = true;
            },
            () => {
                this.loading = false;
                this.generatingFailed = true;
            },
        );
    }

    close() {
        this.dialogRef.close(this.generatingSuccess);
    }
}
