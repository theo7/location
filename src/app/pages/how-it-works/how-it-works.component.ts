import { ViewportScroller } from '@angular/common';
import { Component, ViewEncapsulation } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { environment } from '../../../environments/environment';
import { DeviceService } from '../../shared/services/device.service';

@Component({
    selector: 'app-how-it-works',
    templateUrl: './how-it-works.component.html',
    styleUrls: ['./how-it-works.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class HowItWorksComponent {
    contactAddress = environment.contactAddress;
    iframeSrc = this.sanitizer.bypassSecurityTrustResourceUrl(
        `https://www.youtube.com/embed/${environment.howItWorksYoutubeId}`,
    );

    constructor(
        public deviceService: DeviceService,
        private scroller: ViewportScroller,
        private sanitizer: DomSanitizer,
    ) {}

    scrollToAnchor(anchorId: string) {
        if (this.deviceService.mode === 'desktop') {
            this.scroller.scrollToAnchor(anchorId);
        } else {
            const anchor = document.getElementById(anchorId);

            if (anchor) {
                anchor.scrollIntoView({ behavior: 'smooth' });
            }
        }
    }
}
