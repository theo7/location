import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { HeaderModule } from 'src/app/features/layout/headers/header.module';
import { AppCoreModule } from '../../shared/components/app-core/app-core.module';
import { ButtonModule } from '../../shared/components/button/button.module';
import { SharedModule } from '../../shared/shared.module';
import { HowItWorksRoutingModule } from './how-it-works-routing.module';
import { HowItWorksComponent } from './how-it-works.component';

@NgModule({
    declarations: [HowItWorksComponent],
    imports: [HowItWorksRoutingModule, SharedModule, HeaderModule, AppCoreModule, ButtonModule, MatButtonModule],
})
export class HowItWorksModule {}
