import { NgModule } from '@angular/core';
import { SearchModule } from '../search/search.module';
import { SearchBrandRoutingModule } from './search-brand-routing.module';

@NgModule({
    imports: [SearchModule, SearchBrandRoutingModule],
})
export class SearchBrandModule {}
