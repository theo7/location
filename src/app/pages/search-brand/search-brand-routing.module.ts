import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SearchComponent } from '../search/search.component';

const routes: Routes = [
    {
        path: ':brands',
        component: SearchComponent,
        data: {
            reuseRoute: true,
            name: 'search-brand',
        },
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class SearchBrandRoutingModule {}
