import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSelectModule } from '@angular/material/select';
import { IMaskModule } from 'angular-imask';
import { RecaptchaFormsModule, RecaptchaModule } from 'ng-recaptcha';
import { HeaderModule } from 'src/app/features/layout/headers/header.module';
import { LegalContentDialogModule } from '../../dialogs/legal-content-dialog/legal-content-dialog.module';
import { AuthenticationModule } from '../../features/authentication/authentication.module';
import { IconsModule } from '../../features/icons/icons.module';
import { AppCoreModule } from '../../shared/components/app-core/app-core.module';
import { ButtonModule } from '../../shared/components/button/button.module';
import { PasswordWithConfirmModule } from '../../shared/components/password-with-confirm/password-with-confirm.module';
import { SharedModule } from '../../shared/shared.module';
import { SignUpRoutingModule } from './sign-up-routing.module';
import { SignUpComponent } from './sign-up.component';

@NgModule({
    declarations: [SignUpComponent],
    imports: [
        SharedModule,
        SignUpRoutingModule,
        AuthenticationModule,
        FormsModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatInputModule,
        MatButtonModule,
        MatSelectModule,
        MatCheckboxModule,
        MatProgressSpinnerModule,
        RecaptchaModule,
        RecaptchaFormsModule,
        HeaderModule,
        AppCoreModule,
        ButtonModule,
        IMaskModule,
        IconsModule,
        PasswordWithConfirmModule,
        LegalContentDialogModule,
    ],
})
export class SignUpModule {}
