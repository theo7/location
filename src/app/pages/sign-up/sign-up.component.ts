import { Component, OnInit } from '@angular/core';
import { AbstractControlOptions, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Capacitor } from '@capacitor/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Actions, ofType } from '@ngrx/effects';
import * as dayjs from 'dayjs';
import { debounceTime, distinctUntilChanged, filter } from 'rxjs/operators';
import { CustomValidators } from '../../shared/helpers/custom-validators';
import { DeviceService } from '../../shared/services/device.service';
import { UserService } from '../../shared/services/user.service';
import * as UserActions from '../../store/actions/user.actions';
import { LegalContentDispatchers } from '../../store/services/legal-content-dispatchers.service';
import { UserProfileDispatchers } from '../../store/services/user-dispatchers.service';
import { UserProfileSelectors } from '../../store/services/user-selectors.service';

const NICKNAME_REGEX = new RegExp('^[A-Za-zÀ-ÖØ-öø-ÿ0-9]*$');

@UntilDestroy()
@Component({
    selector: 'app-sign-up',
    templateUrl: './sign-up.component.html',
    styleUrls: ['./sign-up.component.scss'],
})
export class SignUpComponent implements OnInit {
    isLoading$ = this.userProfileSelectors.isLoading$.pipe(untilDestroyed(this));

    signUpForm?: FormGroup;
    isNative = Capacitor.isNativePlatform();

    constructor(
        private readonly fb: FormBuilder,
        private readonly userDispatchers: UserProfileDispatchers,
        private readonly userProfileSelectors: UserProfileSelectors,
        private readonly userService: UserService,
        private readonly actions$: Actions,
        private readonly legalContentDispatchers: LegalContentDispatchers,
        public readonly deviceService: DeviceService,
    ) {}

    ngOnInit(): void {
        this.signUpForm = this.fb.group(
            {
                firstName: [
                    '',
                    [Validators.required, Validators.minLength(2), Validators.pattern(CustomValidators.NAME_PATTERN)],
                ],
                lastName: [
                    '',
                    [Validators.required, Validators.minLength(2), Validators.pattern(CustomValidators.NAME_PATTERN)],
                ],
                nickname: [
                    '',
                    {
                        validators: [
                            Validators.required,
                            Validators.pattern(NICKNAME_REGEX),
                            CustomValidators.unauthorizedName(),
                        ],
                        asyncValidators: [CustomValidators.uniqueNicknameAsyncValidator(this.userService)],
                        updateOn: 'blur',
                    },
                ],
                gender: ['', Validators.required],
                birthDate: [
                    '',
                    [
                        Validators.required,
                        CustomValidators.validDateFormat(),
                        CustomValidators.legalDate(),
                        CustomValidators.minimumDate('01/01/1900'),
                    ],
                ],
                email: [
                    '',
                    {
                        validators: [Validators.required, Validators.pattern(CustomValidators.MAIL_PATTERN)],
                        asyncValidators: [CustomValidators.uniqueEmailAsyncValidator(this.userService)],
                        updateOn: 'blur',
                    },
                ],
                emailConfirmation: ['', [Validators.required, Validators.pattern(CustomValidators.MAIL_PATTERN)]],
                password: this.fb.control({
                    value: '',
                    confirmValue: '',
                }),
                reCaptcha: [null, Validators.required],
                cgu: [false, Validators.requiredTrue],
                lemonwayCGU: [false, Validators.requiredTrue],
                commercialEmails: [false],
            },
            {
                validators: [CustomValidators.mustMatch('email', 'emailConfirmation', { caseInsensitive: true })],
            } as AbstractControlOptions,
        );

        this.listenForChanges();
        this.listenForEvents();
    }

    signUp() {
        if (this.signUpForm?.valid) {
            const formValue = this.signUpForm.value;
            const userData = {
                ...formValue,
                password: formValue.password.value,
                birthDate: dayjs(formValue.birthDate, 'DD/MM/YYYY').format('DD-MM-YYYY'),
            };

            this.userDispatchers.signUp(userData);
        }
    }

    private listenForChanges() {
        // reset errors on each fields when updating one of them
        // errors happening when LW rejects user sign up
        const firstNameControl = this.signUpForm?.get('firstName');
        const lastNameControl = this.signUpForm?.get('lastName');

        if (firstNameControl && lastNameControl) {
            firstNameControl.valueChanges.pipe(distinctUntilChanged(), untilDestroyed(this)).subscribe(() => {
                lastNameControl.setErrors({ invalidName: null });
                lastNameControl.updateValueAndValidity();
            });

            lastNameControl.valueChanges.pipe(distinctUntilChanged(), untilDestroyed(this)).subscribe(() => {
                firstNameControl.setErrors({ invalidName: null });
                firstNameControl.updateValueAndValidity();
            });
        }

        const nicknameControl = this.signUpForm?.get('nickname');

        if (nicknameControl) {
            nicknameControl.valueChanges
                .pipe(
                    filter(nickname => nickname.includes(' ')),
                    untilDestroyed(this),
                )
                .subscribe(nickname => {
                    nicknameControl.setValue(nickname.trim(), { emitEvent: false });
                });
        }
    }

    private listenForEvents() {
        // listen for error event
        this.actions$
            .pipe(
                ofType(UserActions.signUpError),
                filter(({ error }) => {
                    return error?.error?.message === 'PAYMENT_017';
                }),
                debounceTime(10),
                untilDestroyed(this),
            )
            .subscribe(() => {
                const lastNameControl = this.signUpForm?.get('lastName');
                if (lastNameControl) {
                    lastNameControl.setErrors({ invalidName: true });
                    lastNameControl.markAsTouched();
                }

                const firstNameControl = this.signUpForm?.get('firstName');
                if (firstNameControl) {
                    firstNameControl.setErrors({ invalidName: true });
                    firstNameControl.markAsTouched();
                }

                const reCaptchaControl = this.signUpForm?.get('reCaptcha');
                if (reCaptchaControl) {
                    reCaptchaControl.reset(false);
                }
            });

        this.actions$
            .pipe(
                ofType(UserActions.signUpError),
                filter(({ error }) => {
                    return error?.error?.message === 'USER_022';
                }),
                debounceTime(10),
                untilDestroyed(this),
            )
            .subscribe(() => {
                const emailControl = this.signUpForm?.get('email');
                if (emailControl) {
                    emailControl.setErrors({ invalid: true });
                    emailControl.markAsTouched();
                }

                const reCaptchaControl = this.signUpForm?.get('reCaptcha');
                if (reCaptchaControl) {
                    reCaptchaControl.reset(false);
                }
            });
    }

    openPrivacyDialog() {
        this.legalContentDispatchers.openDialog('privacy');
    }

    openCguDialog() {
        this.legalContentDispatchers.openDialog('secondHandCgu');
    }

    openLemonwayCguDialog() {
        this.legalContentDispatchers.openDialog('lemonWayCgu');
    }
}
