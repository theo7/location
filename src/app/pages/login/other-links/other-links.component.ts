import { Component } from '@angular/core';
import { UntilDestroy } from '@ngneat/until-destroy';
import { environment } from 'src/environments/environment';

@UntilDestroy()
@Component({
    selector: 'app-other-links',
    templateUrl: './other-links.component.html',
    styleUrls: ['./other-links.component.scss'],
})
export class OtherLinksComponent {
    otherLinks = [
        { labelKey: 'faq', path: '/other/qa' },
        { labelKey: 'secondHandCgu', path: '/other/cgu' },
        { labelKey: 'lemonWayCgu', path: '/other/lemonway-cgu' },
        { labelKey: 'cookies', path: '/other/cookies' },
        { labelKey: 'privacy', path: '/other/privacy' },
        { labelKey: 'listingConditions', path: '/other/listing-conditions' },
        { labelKey: 'legalNotice', path: '/other/legal-notice' },
    ];
    contactAddress = environment.contactAddress;
}
