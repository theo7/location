import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSelectModule } from '@angular/material/select';
import { HeaderModule } from 'src/app/features/layout/headers/header.module';
import { AuthenticationModule } from '../../features/authentication/authentication.module';
import { AppCoreModule } from '../../shared/components/app-core/app-core.module';
import { ButtonModule } from '../../shared/components/button/button.module';
import { PipesModule } from '../../shared/pipes/pipes.module';
import { SharedModule } from '../../shared/shared.module';
import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login.component';
import { OtherLinksComponent } from './other-links/other-links.component';

@NgModule({
    declarations: [LoginComponent, OtherLinksComponent],
    imports: [
        SharedModule,
        LoginRoutingModule,
        AuthenticationModule,
        FormsModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatInputModule,
        MatButtonModule,
        MatSelectModule,
        MatCheckboxModule,
        MatProgressSpinnerModule,
        MatListModule,
        HeaderModule,
        AppCoreModule,
        ButtonModule,
        PipesModule,
    ],
})
export class LoginModule {}
