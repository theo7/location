import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { filter, finalize, first, switchMap } from 'rxjs/operators';
import { RouterSelectors } from 'src/app/store/services/router-selectors.service';
import { environment } from 'src/environments/environment';
import { CustomValidators } from '../../shared/helpers/custom-validators';
import { DeviceService } from '../../shared/services/device.service';
import { UserService } from '../../shared/services/user.service';
import { UserProfileDispatchers } from '../../store/services/user-dispatchers.service';
import { UserProfileSelectors } from '../../store/services/user-selectors.service';

@UntilDestroy()
@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class LoginComponent implements OnInit {
    @ViewChild('passwordInputEl') passwordInputEl;

    loginForm: FormGroup;
    redirectUrl?: string;
    showPassword = false;
    showEmailConfirmationError = false;
    showLoginError = false;
    confirmationEmailSent = false;
    isLoading$ = this.userProfileSelectors.isLoading$.pipe(untilDestroyed(this));
    showEmailResentError = false;
    appVersion = environment.appVersion;

    constructor(
        private readonly routerSelectors: RouterSelectors,
        private readonly formBuilder: FormBuilder,
        private readonly router: Router,
        private readonly userDispatchers: UserProfileDispatchers,
        private readonly userProfileSelectors: UserProfileSelectors,
        private readonly userService: UserService,
        public readonly deviceService: DeviceService,
    ) {
        this.loginForm = this.formBuilder.group({
            email: ['', [Validators.required, Validators.pattern(CustomValidators.MAIL_PATTERN)]],
            password: ['', Validators.required],
        });
    }

    ngOnInit(): void {
        this.routerSelectors.redirectUrl$.pipe(untilDestroyed(this)).subscribe(redirectUrlTmp => {
            if (redirectUrlTmp) {
                this.redirectUrl = redirectUrlTmp.replace(/%2/g, '/');
            }
        });

        // 💩 refactor code to simplify the login feature
        this.userDispatchers.openLoginPage();
    }

    login() {
        if (this.loginForm?.valid) {
            this.showEmailConfirmationError = false;
            this.confirmationEmailSent = false;
            this.showLoginError = false;
            this.showPassword = false;

            const loginEmail = this.loginForm.controls.email.value as string;
            const loginPassword = this.loginForm.controls.password.value as string;

            this.userDispatchers.login(loginEmail.trim(), loginPassword);

            this.userProfileSelectors.isLogged$
                .pipe(
                    filter(e => !!e),
                    untilDestroyed(this),
                )
                .subscribe(() => this.router.navigate([`/${this.redirectUrl ?? ''}`], { replaceUrl: true }));

            this.userProfileSelectors.isError$
                .pipe(
                    filter(e => !!e),
                    switchMap(() => this.userProfileSelectors.error$),
                    first(),
                    untilDestroyed(this),
                )
                .subscribe(error => {
                    if (error.message === 'email-not-verified') {
                        this.showEmailConfirmationError = true;
                    } else {
                        this.showLoginError = true;
                    }
                });
        }
    }

    showOrHidePassword() {
        this.showPassword = !this.showPassword;

        if (this.passwordInputEl.nativeElement) {
            if (this.showPassword) {
                this.passwordInputEl.nativeElement.removeAttribute('type');
            } else {
                this.passwordInputEl.nativeElement.setAttribute('type', 'password');
            }
        }
    }

    onClickOnResend() {
        const formValue = this.loginForm?.value;
        const email = (formValue?.email || '').toLowerCase();

        this.userService
            .sendConfirmation(email)
            .pipe(
                untilDestroyed(this),
                finalize(() => (this.showEmailConfirmationError = false)),
            )
            .subscribe(
                () => {
                    this.confirmationEmailSent = true;
                },
                error => {
                    this.showEmailResentError = true;
                },
            );
    }
}
