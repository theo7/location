import { ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Photo as CameraPhoto } from '@capacitor/camera';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Actions, ofType } from '@ngrx/effects';
import { filter } from 'rxjs/operators';
import { environment } from '../../../../environments/environment';
import { FileType } from '../../../shared/models/models';
import { fileActions } from '../../../store/actions/file.actions';
import { FileDispatchers } from '../../../store/services/file-dispatchers.service';
import { FilterSelectors } from '../../../store/services/filter-selectors.service';

@UntilDestroy()
@Component({
    selector: 'app-upload-kyc-document',
    templateUrl: './upload-kyc-document.component.html',
    styleUrls: ['./upload-kyc-document.component.scss'],
})
export class UploadKycDocumentComponent implements OnInit {
    @Input()
    fileType!: FileType;

    @Input()
    file?: File;

    @Input()
    previews?: string[];

    @Output()
    documentUploaded = new EventEmitter<File>();

    acceptedTypes: string[] = environment.bankDetails.acceptedTypes;
    maxFileSize: number = environment.bankDetails.maxSize;
    loading: boolean = false;
    errorKey?: string;

    constructor(
        private readonly actions$: Actions,
        private readonly cd: ChangeDetectorRef,
        private readonly fileSelectors: FilterSelectors,
        private readonly fileDispatchers: FileDispatchers,
    ) {}

    ngOnInit(): void {
        // listen for file selection
        this.actions$
            .pipe(
                ofType(fileActions.fileSelected),
                filter(({ fileType }) => fileType === this.fileType),
                untilDestroyed(this),
            )
            .subscribe(({ file }) => this.onFileSelected(file));
        // listen for file upload
        this.actions$
            .pipe(
                ofType(fileActions.uploadKycFileSuccess),
                filter(({ fileType }) => fileType === this.fileType),
                untilDestroyed(this),
            )
            .subscribe(({ pdfFile }) => this.onFileUploaded(pdfFile));
        // listen for upload error
        this.actions$
            .pipe(
                ofType(fileActions.uploadKycFileError),
                filter(({ fileType }) => fileType === this.fileType),
                untilDestroyed(this),
            )
            .subscribe(() => {
                this.loading = false;
            });
    }

    selectFile(): void {
        this.fileDispatchers.selectFile(this.fileType);
    }

    private onFileSelected(file: CameraPhoto): void {
        if (!this.acceptedTypes.includes(file.format)) {
            this.errorKey = 'format';
            return;
        }

        this.loading = true;
        this.fileDispatchers.uploadKycFile(file, this.fileType);
    }

    private onFileUploaded(file: File): void {
        this.loading = false;
        this.cd.detectChanges();
        this.file = file;
        this.documentUploaded.emit(this.file);
    }
}
