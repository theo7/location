import { Component, Input } from '@angular/core';

@Component({
    selector: 'app-iban-icon',
    templateUrl: './iban-icon.component.html',
    styleUrls: ['./iban-icon.component.scss'],
})
export class IbanIconComponent {
    @Input() width = 40;
    @Input() height = 22.5;
}
