import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { IconsModule } from 'src/app/features/icons/icons.module';
import { HeaderModule } from 'src/app/features/layout/headers/header.module';
import { PdfModule } from 'src/app/features/pdf/pdf.module';
import { ButtonModule } from 'src/app/shared/components/button/button.module';
import { StepperModule } from 'src/app/shared/components/stepper/stepper.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { BankAccountCompleteIconComponent } from './bank-account-complete-icon/bank-account-complete-icon.component';
import { CreditCardIconComponent } from './credit-card-icon/credit-card-icon.component';
import { DocumentSentDialogComponent } from './document-sent-dialog/document-sent-dialog.component';
import { EditIbanDocumentDialogComponent } from './edit-iban-document-dialog/edit-iban-document-dialog.component';
import { EditIdentityDocumentDialogComponent } from './edit-identity-document-dialog/edit-identity-document-dialog.component';
import { EditIdentityDocumentRecapComponent } from './edit-identity-document-dialog/edit-identity-document-recap/edit-identity-document-recap.component';
import { EditIdentityDocumentCardComponent } from './edit-identity-document-dialog/edit-identity-document-type/edit-identity-document-card/edit-identity-document-card.component';
import { EditIdentityDocumentTypeComponent } from './edit-identity-document-dialog/edit-identity-document-type/edit-identity-document-type.component';
import { EditIdentityDocumentUploadComponent } from './edit-identity-document-dialog/edit-identity-document-upload/edit-identity-document-upload.component';
import { EditRibDocumentDialogComponent } from './edit-rib-document-dialog/edit-rib-document-dialog.component';
import { EditIbanFormComponent } from './iban-and-rib-document-dialog/edit-iban-form/edit-iban-form.component';
import { EditRibDocumentRecapComponent } from './iban-and-rib-document-dialog/edit-rib-document-recap/edit-rib-document-recap.component';
import { IbanAndRibDocumentDialogComponent } from './iban-and-rib-document-dialog/iban-and-rib-document-dialog.component';
import { IbanDocumentCardComponent } from './iban-document-card/iban-document-card.component';
import { IbanIconComponent } from './iban-icon/iban-icon.component';
import { IdentityDocumentCardComponent } from './identity-document-card/identity-document-card.component';
import { IdentityIconComponent } from './identity-icon/identity-icon.component';
import { InvalidDocumentDialogComponent } from './invalid-document-dialog/invalid-document-dialog.component';
import { PendingValidationDialogComponent } from './pending-validation-dialog/pending-validation-dialog.component';
import { RibDocumentCardComponent } from './rib-document-card/rib-document-card.component';
import { RibIconComponent } from './rib-icon/rib-icon.component';
import { StartIbanRibDocumentCardComponent } from './start-iban-rib-document-card/start-iban-rib-document-card.component';
import { UploadKycDocumentComponent } from './upload-kyc-document/upload-kyc-document.component';
import { UserBankDetailsRoutingModule } from './user-bank-details-routing.module';
import { UserBankDetailsComponent } from './user-bank-details.component';

@NgModule({
    declarations: [
        UserBankDetailsComponent,
        IbanAndRibDocumentDialogComponent,
        EditRibDocumentRecapComponent,
        EditIbanFormComponent,
        PendingValidationDialogComponent,
        DocumentSentDialogComponent,
        EditIdentityDocumentDialogComponent,
        EditIdentityDocumentTypeComponent,
        EditIdentityDocumentUploadComponent,
        EditIdentityDocumentRecapComponent,
        EditIdentityDocumentCardComponent,
        InvalidDocumentDialogComponent,
        BankAccountCompleteIconComponent,
        CreditCardIconComponent,
        IbanDocumentCardComponent,
        RibDocumentCardComponent,
        IdentityDocumentCardComponent,
        StartIbanRibDocumentCardComponent,
        EditIbanDocumentDialogComponent,
        EditRibDocumentDialogComponent,
        IbanIconComponent,
        RibIconComponent,
        IdentityIconComponent,
        UploadKycDocumentComponent,
    ],
    imports: [
        SharedModule,
        UserBankDetailsRoutingModule,
        MatButtonModule,
        StepperModule,
        ButtonModule,
        PdfModule,
        ReactiveFormsModule,
        HeaderModule,
        IconsModule,
    ],
    providers: [],
})
export class UserBankDetailsModule {}
