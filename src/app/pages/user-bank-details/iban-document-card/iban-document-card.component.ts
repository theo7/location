import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Observable } from 'rxjs';
import { filter, switchMap, take } from 'rxjs/operators';
import { DeviceService } from 'src/app/shared/services/device.service';
import { BankDetailsSelectors } from 'src/app/store/services/bank-details-selectors.service';
import { Iban, SimplifiedDocumentStatus } from '../../../shared/models/models';
import { EditIbanDocumentDialogComponent } from '../edit-iban-document-dialog/edit-iban-document-dialog.component';
import { InvalidDocumentDialogComponent } from '../invalid-document-dialog/invalid-document-dialog.component';
import { PendingValidationDialogComponent } from '../pending-validation-dialog/pending-validation-dialog.component';

@UntilDestroy()
@Component({
    selector: 'app-iban-document-card',
    templateUrl: './iban-document-card.component.html',
    styleUrls: ['./iban-document-card.component.scss'],
})
export class IbanDocumentCardComponent {
    ibanDocument$: Observable<Iban | undefined>;
    ibanDocumentSimplifiedStatus$: Observable<SimplifiedDocumentStatus>;

    SimplifiedDocumentStatus = SimplifiedDocumentStatus;

    dialogDeviceParams = device =>
        ({
            handset: {
                width: '100vw',
                maxWidth: '100vw',
                height: '100vh',
                panelClass: 'full-screen-dialog',
            },
            desktop: {
                width: '600px',
                height: '700px',
                panelClass: 'full-screen-rounded-dialog',
            },
        }[device]);

    constructor(
        private readonly dialog: MatDialog,
        private readonly device: DeviceService,
        bankDetailsSelectors: BankDetailsSelectors,
    ) {
        this.ibanDocument$ = bankDetailsSelectors.ibanDocument$.pipe(untilDestroyed(this));
        this.ibanDocumentSimplifiedStatus$ = bankDetailsSelectors.ibanDocumentSimplifiedStatus$.pipe(
            untilDestroyed(this),
        );
    }

    openPendingValidationDialog() {
        this.dialog.open(PendingValidationDialogComponent, {
            width: '500px',
            data: {
                type: 'iban',
            },
        });
    }

    openInvalidDocumentDialog() {
        this.ibanDocument$
            .pipe(
                take(1),
                switchMap(ibanDocument => {
                    return this.dialog
                        .open(InvalidDocumentDialogComponent, {
                            width: '500px',
                            data: {
                                type: 'iban',
                                status: ibanDocument?.status,
                            },
                        })
                        .afterClosed();
                }),
                filter(value => !!value),
            )
            .subscribe(() => this.edit());
    }

    edit() {
        this.dialog.open(EditIbanDocumentDialogComponent, {
            disableClose: true,
            ...this.dialogDeviceParams(this.device.mode),
        });
    }
}
