import { Component, Input } from '@angular/core';

@Component({
    selector: 'app-rib-icon',
    templateUrl: './rib-icon.component.html',
    styleUrls: ['./rib-icon.component.scss'],
})
export class RibIconComponent {
    @Input() width = 30;
    @Input() height = 22.5;
}
