import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Observable } from 'rxjs';
import { debounceTime, filter, finalize, map, startWith } from 'rxjs/operators';
import { CustomValidators } from 'src/app/shared/helpers/custom-validators';
import { UserService } from 'src/app/shared/services/user.service';
import { BankDetailsDispatchers } from 'src/app/store/services/bank-details-dispatchers.service';
import { BankDetailsSelectors } from 'src/app/store/services/bank-details-selectors.service';

@UntilDestroy()
@Component({
    selector: 'app-edit-iban-document-dialog',
    templateUrl: './edit-iban-document-dialog.component.html',
    styleUrls: ['./edit-iban-document-dialog.component.scss'],
})
export class EditIbanDocumentDialogComponent implements OnInit {
    loading?: boolean;
    ibanForm?: FormGroup;

    disabled$?: Observable<boolean>;

    constructor(
        private readonly formBuilder: FormBuilder,
        private readonly bankDetailsSelectors: BankDetailsSelectors,
        private readonly dialogRef: MatDialogRef<EditIbanDocumentDialogComponent>,
        private readonly userService: UserService,
        private readonly bankDetailsDispatchers: BankDetailsDispatchers,
    ) {}

    ngOnInit(): void {
        this.ibanForm = this.formBuilder.group({
            holder: ['', Validators.required],
            iban: ['', [Validators.required, CustomValidators.validIban()]],
        });

        // transform iban field to uppercase
        this.ibanForm.controls.iban.valueChanges.pipe(debounceTime(10), untilDestroyed(this)).subscribe(value => {
            const newValue = (value || '').replace(' ', '').replace('-', '').toUpperCase();
            this.ibanForm?.controls.iban.setValue(newValue);
        });

        this.bankDetailsSelectors.ibanDocument$
            .pipe(
                filter(ibanDocument => !!ibanDocument),
                untilDestroyed(this),
            )
            .subscribe(ibanDocument => {
                if (ibanDocument) {
                    this.ibanForm?.patchValue({
                        holder: ibanDocument.holder,
                        iban: ibanDocument.iban,
                    });
                }
            });

        this.disabled$ = this.ibanForm.valueChanges.pipe(
            map(() => !!this.ibanForm?.invalid),
            startWith(this.ibanForm.invalid),
            untilDestroyed(this),
        );
    }

    onBackButtonClicked() {
        this.dialogRef.close();
    }

    onSendButtonClicked() {
        if (this.ibanForm) {
            const { holder, iban } = this.ibanForm.getRawValue();

            this.loading = true;

            this.userService
                .updateIban(holder, iban)
                .pipe(finalize(() => (this.loading = false)))
                .subscribe(ibanResponse => {
                    if (ibanResponse.status) {
                        this.bankDetailsDispatchers.documentUploaded('iban', ibanResponse.status);
                    }
                    this.dialogRef.close(true);
                });
        }
    }
}
