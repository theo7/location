import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Observable } from 'rxjs';
import { filter, switchMap, take } from 'rxjs/operators';
import { DeviceService } from 'src/app/shared/services/device.service';
import { BankDetailsSelectors } from 'src/app/store/services/bank-details-selectors.service';
import { Document, SimplifiedDocumentStatus } from '../../../shared/models/models';
import { EditRibDocumentDialogComponent } from '../edit-rib-document-dialog/edit-rib-document-dialog.component';
import { InvalidDocumentDialogComponent } from '../invalid-document-dialog/invalid-document-dialog.component';
import { PendingValidationDialogComponent } from '../pending-validation-dialog/pending-validation-dialog.component';

@UntilDestroy()
@Component({
    selector: 'app-rib-document-card',
    templateUrl: './rib-document-card.component.html',
    styleUrls: ['./rib-document-card.component.scss'],
})
export class RibDocumentCardComponent {
    ribDocument$: Observable<Document | undefined>;
    ribDocumentSimplifiedStatus$: Observable<SimplifiedDocumentStatus>;

    SimplifiedDocumentStatus = SimplifiedDocumentStatus;

    dialogDeviceParams = device =>
        ({
            handset: {
                width: '100vw',
                maxWidth: '100vw',
                height: '100vh',
                panelClass: 'full-screen-dialog',
            },
            desktop: {
                width: '600px',
                height: '700px',
                panelClass: 'full-screen-rounded-dialog',
            },
        }[device]);

    constructor(
        private readonly dialog: MatDialog,
        private readonly device: DeviceService,
        bankDetailsSelectors: BankDetailsSelectors,
    ) {
        this.ribDocument$ = bankDetailsSelectors.ribDocument$.pipe(untilDestroyed(this));
        this.ribDocumentSimplifiedStatus$ = bankDetailsSelectors.ribDocumentSimplifiedStatus$.pipe(
            untilDestroyed(this),
        );
    }

    openPendingValidationDialog() {
        this.dialog.open(PendingValidationDialogComponent, {
            width: '500px',
            data: {
                type: 'rib',
            },
        });
    }

    openInvalidDocumentDialog() {
        this.ribDocument$
            .pipe(
                take(1),
                switchMap(ribDocument => {
                    return this.dialog
                        .open(InvalidDocumentDialogComponent, {
                            width: '500px',
                            data: {
                                type: 'rib',
                                status: ribDocument?.status,
                                reason: ribDocument?.comment,
                            },
                        })
                        .afterClosed();
                }),
                filter(value => !!value),
            )
            .subscribe(() => this.edit());
    }

    edit() {
        this.dialog.open(EditRibDocumentDialogComponent, {
            disableClose: true,
            ...this.dialogDeviceParams(this.device.mode),
        });
    }
}
