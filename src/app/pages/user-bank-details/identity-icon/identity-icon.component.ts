import { Component, Input } from '@angular/core';

@Component({
    selector: 'app-identity-icon',
    templateUrl: './identity-icon.component.html',
    styleUrls: ['./identity-icon.component.scss'],
})
export class IdentityIconComponent {
    @Input() width = 30;
    @Input() height = 22;
}
