import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { forkJoin } from 'rxjs';
import { debounceTime, filter, finalize } from 'rxjs/operators';
import { CustomValidators } from 'src/app/shared/helpers/custom-validators';
import { BankDetailsDispatchers } from 'src/app/store/services/bank-details-dispatchers.service';
import { BankDetailsSelectors } from 'src/app/store/services/bank-details-selectors.service';
import { FileType, Step } from '../../../shared/models/models';
import { UserService } from '../../../shared/services/user.service';

@UntilDestroy()
@Component({
    selector: 'app-iban-and-rib-document-dialog',
    templateUrl: './iban-and-rib-document-dialog.component.html',
    styleUrls: ['./iban-and-rib-document-dialog.component.scss'],
})
export class IbanAndRibDocumentDialogComponent implements OnInit {
    loading: boolean = false;
    ibanForm: FormGroup;
    steps: Step[] = [
        { label: 'profile.bankDetails.editRib.step.iban', icon: 'icon-iban.svg' },
        { label: 'profile.bankDetails.editRib.step.rib', icon: 'icon-rib-white.svg' },
        { label: 'profile.bankDetails.editRib.step.recap', icon: 'icon-recap.svg' },
    ];
    activeStep = 0;
    doneSteps = -1;
    fileType = FileType;

    constructor(
        private readonly cd: ChangeDetectorRef,
        private readonly formBuilder: FormBuilder,
        private readonly dialogRef: MatDialogRef<IbanAndRibDocumentDialogComponent>,
        private readonly userService: UserService,
        private readonly bankDetailsSelectors: BankDetailsSelectors,
        private readonly bankDetailsDispatchers: BankDetailsDispatchers,
    ) {
        this.ibanForm = this.formBuilder.group({
            holder: ['', Validators.required],
            iban: ['', [Validators.required, CustomValidators.validIban()]],
            rib: null,
        });
    }

    ngOnInit(): void {
        // transform iban field to uppercase
        this.ibanForm.controls.iban.valueChanges.pipe(debounceTime(10), untilDestroyed(this)).subscribe(value => {
            const newValue = (value || '').replace(' ', '').replace('-', '').toUpperCase();
            this.ibanForm.controls.iban.setValue(newValue);
        });

        this.bankDetailsSelectors.ibanDocument$
            .pipe(
                filter(ibanDocument => !!ibanDocument),
                untilDestroyed(this),
            )
            .subscribe(ibanDocument => {
                if (ibanDocument) {
                    this.ibanForm.patchValue({
                        holder: ibanDocument.holder,
                        iban: ibanDocument.iban,
                    });
                }
            });
    }

    previousStep() {
        if (this.activeStep !== 0) {
            this.activeStep--;
            this.doneSteps--;
        } else {
            this.dialogRef.close();
        }
    }

    nextStep() {
        if (this.activeStep < 2) {
            this.activeStep++;
            this.doneSteps++;
        } else {
            this.send();
        }
    }

    goToStep(index: number) {
        if (index < this.activeStep) {
            this.activeStep = index;
            this.doneSteps = this.activeStep - 1;
        }
    }

    onDocumentUploaded(event: File) {
        this.ibanForm.get('rib')?.setValue(event);
        this.cd.detectChanges();
    }

    get nextButtonDisabled(): boolean {
        switch (this.activeStep) {
            case 0:
                return this.ibanForm.invalid;
            case 1:
                return !this.ibanForm.get('rib')?.value;
            default:
                return false;
        }
    }

    private send() {
        this.loading = true;

        const { holder, iban, rib } = this.ibanForm.getRawValue();

        forkJoin([this.userService.updateIban(holder, iban), this.userService.sendRibDocument(rib)])
            .pipe(finalize(() => (this.loading = false)))
            .subscribe(([ibanResponse, ribResponse]) => {
                if (ibanResponse.status) {
                    this.bankDetailsDispatchers.documentUploaded('iban', ibanResponse.status);
                }
                this.bankDetailsDispatchers.documentUploaded('rib', ribResponse.status);
                this.bankDetailsDispatchers.accountUpdated(ribResponse.accountstatus);
                this.dialogRef.close(true);
            });
    }
}
