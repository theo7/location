import { Component, Input, ViewEncapsulation } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { environment } from '../../../../../environments/environment';

@Component({
    selector: 'app-edit-iban-form',
    templateUrl: './edit-iban-form.component.html',
    styleUrls: ['./edit-iban-form.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class EditIbanFormComponent {
    @Input() ibanForm?: FormGroup;

    contactAddress = environment.contactAddress;
}
