import { Component, Input } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { UploadUserIban } from '../../../../shared/models/models';

@Component({
    selector: 'app-edit-rib-document-recap',
    templateUrl: './edit-rib-document-recap.component.html',
    styleUrls: ['./edit-rib-document-recap.component.scss'],
})
export class EditRibDocumentRecapComponent {
    @Input() userIban?: UploadUserIban;

    constructor(private sanitizer: DomSanitizer) {}

    get rib(): File | undefined {
        return this.userIban?.rib;
    }

    get ribPreviewUrl(): SafeResourceUrl {
        return this.sanitizer.bypassSecurityTrustResourceUrl(URL.createObjectURL(this.rib));
    }
}
