import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DocumentStatus, IbanStatus } from 'src/app/shared/models/models';

@Component({
    selector: 'app-invalid-document-dialog',
    templateUrl: './invalid-document-dialog.component.html',
    styleUrls: ['./invalid-document-dialog.component.scss'],
})
export class InvalidDocumentDialogComponent implements OnInit {
    type?: 'rib' | 'iban' | 'identity';
    status?: DocumentStatus | IbanStatus;
    reason?: string;

    constructor(@Inject(MAT_DIALOG_DATA) private data: any) {}

    ngOnInit(): void {
        this.type = this.data.type;
        this.status = this.data.status;
        this.reason = this.data.reason;
    }
}
