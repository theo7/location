import { Component } from '@angular/core';

@Component({
    selector: 'app-bank-account-complete-icon',
    templateUrl: './bank-account-complete-icon.component.html',
    styleUrls: ['./bank-account-complete-icon.component.scss'],
})
export class BankAccountCompleteIconComponent {}
