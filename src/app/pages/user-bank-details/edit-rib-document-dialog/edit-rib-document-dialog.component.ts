import { ChangeDetectorRef, Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { UserService } from 'src/app/shared/services/user.service';
import { BankDetailsDispatchers } from 'src/app/store/services/bank-details-dispatchers.service';
import { FileType } from '../../../shared/models/models';

@Component({
    selector: 'app-edit-rib-document-dialog',
    templateUrl: './edit-rib-document-dialog.component.html',
    styleUrls: ['./edit-rib-document-dialog.component.scss'],
})
export class EditRibDocumentDialogComponent {
    loading: boolean = false;
    ribForm: FormGroup;

    disabled$?: Observable<boolean>;
    fileType = FileType;

    constructor(
        private readonly dialogRef: MatDialogRef<EditRibDocumentDialogComponent>,
        private readonly formBuilder: FormBuilder,
        private readonly bankDetailsDispatchers: BankDetailsDispatchers,
        private readonly userService: UserService,
        private readonly cd: ChangeDetectorRef,
    ) {
        this.ribForm = this.formBuilder.group({
            rib: null,
        });
    }

    onDocumentUploaded(event: File): void {
        this.ribForm.get('rib')?.setValue(event);
        this.cd.detectChanges();
    }

    onBackButtonClicked(): void {
        this.dialogRef.close();
    }

    onSendButtonClicked(): void {
        const { rib } = this.ribForm.getRawValue();

        this.loading = true;

        this.userService
            .sendRibDocument(rib)
            .pipe(finalize(() => (this.loading = false)))
            .subscribe(ribResponse => {
                this.bankDetailsDispatchers.documentUploaded('rib', ribResponse.status);
                this.bankDetailsDispatchers.accountUpdated(ribResponse.accountstatus);
                this.dialogRef.close(true);
            });
    }
}
