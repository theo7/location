import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { DocumentType } from '../../../../shared/models/models';

@Component({
    selector: 'app-edit-identity-document-type',
    templateUrl: './edit-identity-document-type.component.html',
    styleUrls: ['./edit-identity-document-type.component.scss'],
})
export class EditIdentityDocumentTypeComponent {
    @Input() identityForm?: FormGroup;

    @Output() documentTypeChanged = new EventEmitter<DocumentType>();

    documentType = DocumentType;

    driverLicenceEnabled = false;
}
