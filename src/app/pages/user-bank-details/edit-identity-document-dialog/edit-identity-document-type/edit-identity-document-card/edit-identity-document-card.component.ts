import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { DocumentType } from '../../../../../shared/models/models';

@Component({
    selector: 'app-edit-identity-document-card',
    templateUrl: './edit-identity-document-card.component.html',
    styleUrls: ['./edit-identity-document-card.component.scss'],
})
export class EditIdentityDocumentCardComponent {
    @Input() group?: FormGroup;
    @Input() type?: DocumentType;

    @Output() selected = new EventEmitter<DocumentType>();

    get isSelected() {
        return this.group?.get('documentType')?.value === this.type;
    }
}
