import { Component, EventEmitter, Input, Output } from '@angular/core';
import { isDefined } from 'src/app/shared/functions/filter.functions';
import { environment } from '../../../../../environments/environment';
import { PdfService } from '../../../../features/pdf/services/pdf.service';
import { DocumentType, FileType } from '../../../../shared/models/models';

@Component({
    selector: 'app-edit-identity-document-upload',
    templateUrl: './edit-identity-document-upload.component.html',
    styleUrls: ['./edit-identity-document-upload.component.scss'],
})
export class EditIdentityDocumentUploadComponent {
    @Input() type?: DocumentType;
    @Input() rectoFile?: File;
    @Input() versoFile?: File;

    @Output() fileUploaded = new EventEmitter<File>();
    @Output() rectoFileUploaded = new EventEmitter<File>();
    @Output() versoFileUploaded = new EventEmitter<File>();

    maxFileSize = environment.bankDetails.maxSize;
    fileType = FileType;

    constructor(private readonly pdfService: PdfService) {}

    get needToUploadRectoVerso(): boolean {
        switch (this.type) {
            case DocumentType.ID_CARD:
            case DocumentType.DRIVER_LICENCE:
            case DocumentType.RESIDENCE_PERMIT:
                return true;
            default:
                return false;
        }
    }

    onDoublePageFileUploaded(file: File) {
        this.fileUploaded.emit(file);
        this.rectoFileUploaded.emit(file);
    }

    onRectoFileUploaded(file: File) {
        this.rectoFile = file;
        this.rectoFileUploaded.emit(this.rectoFile);

        if (this.versoFile != null) {
            this.mergeFiles();
        }
    }

    onVersoFileUploaded(file: File) {
        this.versoFile = file;
        this.versoFileUploaded.emit(this.versoFile);

        if (this.rectoFile != null) {
            this.mergeFiles();
        }
    }

    get rectoImagePreviews(): string[] {
        switch (this.type) {
            case DocumentType.ID_CARD:
                return ['id_card_recto.png'];
            case DocumentType.PASSPORT_EU:
            case DocumentType.PASSPORT_OUTSIDE_EU:
                return ['passport.png'];
            case DocumentType.DRIVER_LICENCE:
                return ['driver_licence_recto.png', 'old_driver_licence_recto.png'];
            case DocumentType.RESIDENCE_PERMIT:
                return ['residence_permit_recto.png'];
            default:
                return [];
        }
    }

    get versoImagePreviews(): string[] {
        switch (this.type) {
            case DocumentType.ID_CARD:
                return ['id_card_verso.png'];
            case DocumentType.DRIVER_LICENCE:
                return ['driver_licence_verso.png', 'old_driver_licence_verso.png'];
            case DocumentType.RESIDENCE_PERMIT:
                return ['residence_permit_verso.png'];
            default:
                return [];
        }
    }

    private mergeFiles() {
        this.pdfService.mergePDFDocuments([this.rectoFile, this.versoFile].filter(isDefined)).then(file => {
            this.fileUploaded.emit(file);
        });
    }
}
