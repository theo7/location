import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { finalize } from 'rxjs/operators';
import { BankDetailsDispatchers } from 'src/app/store/services/bank-details-dispatchers.service';
import { DocumentType, Step } from '../../../shared/models/models';
import { UserService } from '../../../shared/services/user.service';

@Component({
    selector: 'app-edit-identity-document-dialog',
    templateUrl: './edit-identity-document-dialog.component.html',
    styleUrls: ['./edit-identity-document-dialog.component.scss'],
})
export class EditIdentityDocumentDialogComponent implements OnInit {
    identityForm?: FormGroup;
    loading?: boolean;
    rectoFile?: File;
    versoFile?: File;

    steps: Step[] = [
        { label: 'profile.bankDetails.editIdentity.step.type', icon: 'icon-document-type.svg' },
        { label: 'profile.bankDetails.editIdentity.step.select', icon: 'icon-document-picture.svg' },
        { label: 'profile.bankDetails.editIdentity.step.recap', icon: 'icon-recap.svg' },
    ];
    activeStep = 0;
    doneSteps = -1;

    constructor(
        private readonly cd: ChangeDetectorRef,
        private readonly formBuilder: FormBuilder,
        private readonly dialogRef: MatDialogRef<EditIdentityDocumentDialogComponent>,
        private readonly userService: UserService,
        private readonly bankDetailsDispatchers: BankDetailsDispatchers,
    ) {}

    ngOnInit() {
        this.identityForm = this.formBuilder.group({
            documentType: ['', Validators.required],
            multipartFile: null,
        });
    }

    previousStep() {
        if (this.activeStep > 0) {
            this.activeStep--;
            this.doneSteps--;
        } else {
            this.dialogRef.close();
        }
    }

    nextStep() {
        if (this.activeStep < 2) {
            this.activeStep++;
            this.doneSteps++;
        } else {
            this.send();
        }
    }

    goToStep(index: number) {
        if (index < this.activeStep) {
            this.activeStep = index;
            this.doneSteps = this.activeStep - 1;
        }
    }

    get nextButtonDisabled(): boolean {
        switch (this.activeStep) {
            case 0:
                return !!this.identityForm?.invalid;
            case 1:
                return !this.identityForm?.get('multipartFile')?.value;
            default:
                return false;
        }
    }

    onFileUploaded(file: File) {
        this.identityForm?.get('multipartFile')?.setValue(file);
        this.cd.detectChanges();
    }

    onDocumentTypeChanged(type: DocumentType) {
        this.identityForm?.patchValue({
            documentType: type,
            multipartFile: null,
        });
        this.rectoFile = undefined;
        this.versoFile = undefined;
    }

    onRectoFileUploaded(file: File) {
        this.rectoFile = file;
    }

    onVersoFileUploaded(file: File) {
        this.versoFile = file;
    }

    private send() {
        if (this.identityForm) {
            const { multipartFile } = this.identityForm.getRawValue();

            this.loading = true;

            this.userService
                .sendIdentityDocument(multipartFile)
                .pipe(finalize(() => (this.loading = false)))
                .subscribe(response => {
                    this.bankDetailsDispatchers.documentUploaded('identity', response.status);
                    this.bankDetailsDispatchers.accountUpdated(response.accountstatus);
                    this.dialogRef.close(this.identityForm?.value.documentType);
                });
        }
    }
}
