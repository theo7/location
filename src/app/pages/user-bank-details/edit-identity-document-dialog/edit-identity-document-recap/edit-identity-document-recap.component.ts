import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { DocumentType } from '../../../../shared/models/models';

@Component({
    selector: 'app-edit-identity-document-recap',
    templateUrl: './edit-identity-document-recap.component.html',
    styleUrls: ['./edit-identity-document-recap.component.scss'],
})
export class EditIdentityDocumentRecapComponent {
    @Input() identityForm?: FormGroup;

    get type(): DocumentType {
        return this.identityForm?.get('documentType')?.value;
    }

    get file(): File {
        return this.identityForm?.get('multipartFile')?.value;
    }
}
