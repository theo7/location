import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DeviceService } from '../../../shared/services/device.service';
import { IbanAndRibDocumentDialogComponent } from '../iban-and-rib-document-dialog/iban-and-rib-document-dialog.component';

@Component({
    selector: 'app-start-iban-rib-document-card',
    templateUrl: './start-iban-rib-document-card.component.html',
    styleUrls: ['./start-iban-rib-document-card.component.scss'],
})
export class StartIbanRibDocumentCardComponent {
    dialogDeviceParams = device =>
        ({
            handset: {
                width: '100vw',
                maxWidth: '100vw',
                height: '100vh',
                panelClass: 'full-screen-dialog',
            },
            desktop: {
                width: '600px',
                height: '700px',
                panelClass: 'full-screen-rounded-dialog',
            },
        }[device]);

    constructor(private readonly dialog: MatDialog, private readonly device: DeviceService) {}

    edit() {
        this.dialog.open(IbanAndRibDocumentDialogComponent, {
            disableClose: true,
            ...this.dialogDeviceParams(this.device.mode),
        });
    }
}
