import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
    selector: 'app-document-sent-dialog',
    templateUrl: './document-sent-dialog.component.html',
    styleUrls: ['./document-sent-dialog.component.scss'],
})
export class DocumentSentDialogComponent implements OnInit {
    type?: 'rib' | 'iban' | 'identity';

    constructor(@Inject(MAT_DIALOG_DATA) private data: any) {}

    ngOnInit() {
        this.type = this.data.documentType;
    }
}
