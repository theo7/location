import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserBankDetailsComponent } from './user-bank-details.component';

const routes: Routes = [
    {
        path: '',
        component: UserBankDetailsComponent,
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class UserBankDetailsRoutingModule {}
