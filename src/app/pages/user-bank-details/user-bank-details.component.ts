import { Component, OnInit } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Observable } from 'rxjs';
import { AccountStatus } from 'src/app/shared/models/models';
import { DeviceService } from 'src/app/shared/services/device.service';
import { BankDetailsDispatchers } from 'src/app/store/services/bank-details-dispatchers.service';
import { BankDetailsSelectors } from 'src/app/store/services/bank-details-selectors.service';

@UntilDestroy()
@Component({
    selector: 'app-user-bank-details',
    templateUrl: './user-bank-details.component.html',
    styleUrls: ['./user-bank-details.component.scss'],
})
export class UserBankDetailsComponent implements OnInit {
    loading$: Observable<boolean>;
    accountStatus$: Observable<AccountStatus | undefined>;
    isIbanAndRibProcessStarted$: Observable<boolean>;
    isBankAccountIncomplete$: Observable<boolean>;
    isBankAccountPendingValidation$: Observable<boolean>;
    isBankAccountCompleted$: Observable<boolean>;
    isBankAccountInError$: Observable<boolean>;

    dialogDeviceParams = device =>
        ({
            handset: {
                width: '100vw',
                maxWidth: '100vw',
                height: '100vh',
                panelClass: 'full-screen-dialog',
            },
            desktop: {
                width: '600px',
                height: '700px',
                panelClass: 'full-screen-rounded-dialog',
            },
        }[device]);

    constructor(
        public readonly deviceService: DeviceService,
        private readonly bankDetailsDispatchers: BankDetailsDispatchers,
        bankDetailsSelectors: BankDetailsSelectors,
    ) {
        this.loading$ = bankDetailsSelectors.loading$.pipe(untilDestroyed(this));
        this.accountStatus$ = bankDetailsSelectors.accountStatus$.pipe(untilDestroyed(this));
        this.isIbanAndRibProcessStarted$ = bankDetailsSelectors.isIbanAndRibProcessStarted$.pipe(untilDestroyed(this));
        this.isBankAccountIncomplete$ = bankDetailsSelectors.isBankAccountIncomplete$.pipe(untilDestroyed(this));
        this.isBankAccountPendingValidation$ = bankDetailsSelectors.isBankAccountPendingValidation$.pipe(
            untilDestroyed(this),
        );
        this.isBankAccountCompleted$ = bankDetailsSelectors.isBankAccountCompleted$.pipe(untilDestroyed(this));
        this.isBankAccountInError$ = bankDetailsSelectors.isBankAccountInError$.pipe(untilDestroyed(this));
    }

    ngOnInit() {
        this.bankDetailsDispatchers.loadBankDetails();
    }
}
