import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Observable } from 'rxjs';
import { filter, switchMap, take } from 'rxjs/operators';
import { BankDetailsSelectors } from 'src/app/store/services/bank-details-selectors.service';
import { Document, SimplifiedDocumentStatus } from '../../../shared/models/models';
import { DeviceService } from '../../../shared/services/device.service';
import { EditIdentityDocumentDialogComponent } from '../edit-identity-document-dialog/edit-identity-document-dialog.component';
import { InvalidDocumentDialogComponent } from '../invalid-document-dialog/invalid-document-dialog.component';
import { PendingValidationDialogComponent } from '../pending-validation-dialog/pending-validation-dialog.component';

@UntilDestroy()
@Component({
    selector: 'app-identity-document-card',
    templateUrl: './identity-document-card.component.html',
    styleUrls: ['./identity-document-card.component.scss'],
})
export class IdentityDocumentCardComponent {
    identityDocument$: Observable<Document | undefined>;
    identityDocumentSimplifiedStatus$: Observable<SimplifiedDocumentStatus>;

    SimplifiedDocumentStatus = SimplifiedDocumentStatus;

    dialogDeviceParams = device =>
        ({
            handset: {
                width: '100vw',
                maxWidth: '100vw',
                height: '100vh',
                panelClass: 'full-screen-dialog',
            },
            desktop: {
                width: '600px',
                height: '700px',
                panelClass: 'full-screen-rounded-dialog',
            },
        }[device]);

    constructor(
        private readonly dialog: MatDialog,
        private readonly device: DeviceService,
        bankDetailsSelectors: BankDetailsSelectors,
    ) {
        this.identityDocument$ = bankDetailsSelectors.identityDocument$.pipe(untilDestroyed(this));
        this.identityDocumentSimplifiedStatus$ = bankDetailsSelectors.identityDocumentSimplifiedStatus$.pipe(
            untilDestroyed(this),
        );
    }

    openPendingValidationDialog() {
        this.dialog.open(PendingValidationDialogComponent, {
            width: '500px',
            data: {
                type: 'identity',
            },
        });
    }

    openInvalidDocumentDialog() {
        this.identityDocument$
            .pipe(
                take(1),
                switchMap(identityDocument => {
                    return this.dialog
                        .open(InvalidDocumentDialogComponent, {
                            width: '500px',
                            data: {
                                type: 'identity',
                                status: identityDocument?.status,
                                reason: identityDocument?.comment,
                            },
                        })
                        .afterClosed();
                }),
                filter(value => !!value),
            )
            .subscribe(() => this.edit());
    }

    edit() {
        this.dialog.open(EditIdentityDocumentDialogComponent, {
            disableClose: true,
            ...this.dialogDeviceParams(this.device.mode),
        });
    }
}
