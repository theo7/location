import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
    selector: 'app-pending-validation-dialog',
    templateUrl: './pending-validation-dialog.component.html',
    styleUrls: ['./pending-validation-dialog.component.scss'],
})
export class PendingValidationDialogComponent implements OnInit {
    type?: 'rib' | 'iban' | 'identity';

    constructor(@Inject(MAT_DIALOG_DATA) private data: any) {}

    ngOnInit(): void {
        this.type = this.data.type;
    }
}
