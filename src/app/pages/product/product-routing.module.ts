import { NgModule } from '@angular/core';
import { AngularFireAuthGuard, redirectUnauthorizedTo } from '@angular/fire/auth-guard';
import { RouterModule, Routes } from '@angular/router';
import { CannotLeaveFormGuard } from 'src/app/shared/guards/cannot-leave-form.guard';
import { ProductComponent } from './product.component';
import { UpdateProductComponent } from './update-product/update-product.component';

const redirectUnauthorizedToLogin = () => redirectUnauthorizedTo(['login']);

const routes: Routes = [
    {
        path: ':productId/update',
        component: UpdateProductComponent,
        canActivate: [AngularFireAuthGuard],
        canDeactivate: [CannotLeaveFormGuard],
        data: {
            authGuardPipe: redirectUnauthorizedToLogin,
            confirmUnsaveChangesDialogTitle: 'form.confirmUnsaveChangesDialogTitle.editProduct',
        },
    },
    {
        path: ':productId',
        component: ProductComponent,
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class ProductRoutingModule {}
