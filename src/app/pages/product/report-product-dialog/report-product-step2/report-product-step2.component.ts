import { Component, EventEmitter, HostBinding, Output } from '@angular/core';
import { FormControl } from '@angular/forms';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Observable } from 'rxjs';
import { map, startWith, switchMap, take } from 'rxjs/operators';
import { filterDefined } from 'src/app/shared/functions/operators.functions';
import { Product, ProductReportAbuseReason } from 'src/app/shared/models/models';
import { DeviceService } from 'src/app/shared/services/device.service';
import { ImageService } from 'src/app/shared/services/image.service';
import { ProductsService } from 'src/app/shared/services/products.service';
import { ProductsDispatchers } from 'src/app/store/services/products-dispatchers.service';
import { RouterSelectors } from 'src/app/store/services/router-selectors.service';

type ProductReported = Product & { image: string };

@UntilDestroy()
@Component({
    selector: 'app-report-product-step2',
    templateUrl: './report-product-step2.component.html',
    styleUrls: ['./report-product-step2.component.scss'],
})
export class ReportProductStep2Component {
    content = new FormControl('');

    product$: Observable<ProductReported>;
    canValidate$: Observable<boolean>;

    @Output()
    goPreviousStep = new EventEmitter();

    @HostBinding('class') get class() {
        return this.device.mode;
    }

    constructor(
        private readonly routerSelectors: RouterSelectors,
        productsService: ProductsService,
        imageService: ImageService,
        private readonly productsDispatchers: ProductsDispatchers,
        private readonly device: DeviceService,
    ) {
        this.product$ = routerSelectors.productId$.pipe(
            filterDefined(),
            switchMap(productId => productsService.getProduct(productId)),
            map(product => ({
                ...product,
                image: imageService.getUrl(product.pictures[0]?.fileName),
            })),
            untilDestroyed(this),
        );

        this.canValidate$ = this.content.valueChanges.pipe(
            startWith(this.content.value),
            map(value => !!value),
            untilDestroyed(this),
        );
    }

    onBackButtonClicked() {
        this.goPreviousStep.emit();
    }

    onValidatedClicked() {
        this.routerSelectors.productId$.pipe(take(1)).subscribe(productId => {
            if (productId) {
                this.productsDispatchers.report(productId, ProductReportAbuseReason.OTHER, this.content.value);
            }
        });
    }
}
