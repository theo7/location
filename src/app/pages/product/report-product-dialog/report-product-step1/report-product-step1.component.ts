import { Component, EventEmitter, HostBinding, Output } from '@angular/core';
import { MatSelectionListChange } from '@angular/material/list';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, switchMap, take, withLatestFrom } from 'rxjs/operators';
import { filterDefined } from 'src/app/shared/functions/operators.functions';
import { Product, ProductReportAbuseReason } from 'src/app/shared/models/models';
import { DeviceService } from 'src/app/shared/services/device.service';
import { ImageService } from 'src/app/shared/services/image.service';
import { ProductsService } from 'src/app/shared/services/products.service';
import { ProductsDispatchers } from 'src/app/store/services/products-dispatchers.service';
import { RouterSelectors } from 'src/app/store/services/router-selectors.service';

type Reason = keyof typeof ProductReportAbuseReason;

const reasons: Reason[] = [
    ProductReportAbuseReason.INAPPROPRIATE,
    ProductReportAbuseReason.WRONG_DESCRIPTION,
    ProductReportAbuseReason.PROFESSIONAL,
    ProductReportAbuseReason.UNAUTHORIZED,
    ProductReportAbuseReason.UNAUTHORIZED_PICTURE,
    ProductReportAbuseReason.FAKE,
    ProductReportAbuseReason.OTHER,
];

type ProductReported = Product & { image: string };

@UntilDestroy()
@Component({
    selector: 'app-report-product-step1',
    templateUrl: './report-product-step1.component.html',
    styleUrls: ['./report-product-step1.component.scss'],
})
export class ReportProductStep1Component {
    reasons = reasons;
    ProductReportAbuseReason = ProductReportAbuseReason;

    product$: Observable<ProductReported>;
    selectedReason$ = new BehaviorSubject<Reason | undefined>(undefined);
    canValidate$: Observable<boolean>;

    @Output()
    goNextStep = new EventEmitter();

    @HostBinding('class') get class() {
        return this.device.mode;
    }

    constructor(
        private readonly routerSelectors: RouterSelectors,
        productsService: ProductsService,
        imageService: ImageService,
        private readonly productsDispatchers: ProductsDispatchers,
        private readonly device: DeviceService,
    ) {
        this.product$ = routerSelectors.productId$.pipe(
            filterDefined(),
            switchMap(productId => productsService.getProduct(productId)),
            map(product => ({
                ...product,
                image: imageService.getUrl(product.pictures[0]?.fileName),
            })),
            untilDestroyed(this),
        );

        this.canValidate$ = this.selectedReason$.pipe(
            map(selectedReason => !!selectedReason && selectedReason !== ProductReportAbuseReason.OTHER),
            untilDestroyed(this),
        );
    }

    onReasonSelected(e: MatSelectionListChange) {
        const selectedReason = e.option.value;

        if (selectedReason === ProductReportAbuseReason.OTHER) {
            this.goNextStep.emit();
            e.source.deselectAll();
            this.selectedReason$.next(undefined);
        } else {
            this.selectedReason$.next(e.option.value);
        }
    }

    onValidatedClicked() {
        this.selectedReason$
            .pipe(take(1), withLatestFrom(this.routerSelectors.productId$))
            .subscribe(([reason, productId]) => {
                if (productId && reason) {
                    this.productsDispatchers.report(productId, reason);
                }
            });
    }
}
