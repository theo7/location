import { Component, ViewChild } from '@angular/core';
import { MatStepper } from '@angular/material/stepper';

@Component({
    selector: 'app-report-product-dialog',
    templateUrl: './report-product-dialog.component.html',
    styleUrls: ['./report-product-dialog.component.scss'],
})
export class ReportProductDialogComponent {
    @ViewChild('stepper') stepper?: MatStepper;

    goPreviousStep() {
        if (this.stepper) {
            this.stepper.previous();
        }
    }

    goNextStep() {
        if (this.stepper) {
            this.stepper.next();
        }
    }
}
