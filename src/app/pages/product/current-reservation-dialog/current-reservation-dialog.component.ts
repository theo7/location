import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Reservation } from '../../../shared/models/models';

@Component({
    selector: 'app-current-reservation-dialog',
    templateUrl: './current-reservation-dialog.component.html',
    styleUrls: ['./current-reservation-dialog.component.scss'],
})
export class CurrentReservationDialogComponent {
    reservation: Reservation;

    constructor(
        private dialogRef: MatDialogRef<CurrentReservationDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
    ) {
        this.reservation = data.reservation;
    }
}
