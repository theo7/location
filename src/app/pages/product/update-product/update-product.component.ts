import { Component, OnInit } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { switchMap } from 'rxjs/operators';
import { filterDefined } from 'src/app/shared/functions/operators.functions';
import { ILeavableFormComponent } from 'src/app/shared/guards/cannot-leave-form.guard';
import { RouterSelectors } from 'src/app/store/services/router-selectors.service';
import { Product } from '../../../shared/models/models';
import { DeviceService } from '../../../shared/services/device.service';
import { ProductsService } from '../../../shared/services/products.service';

@UntilDestroy()
@Component({
    selector: 'app-update-product',
    templateUrl: './update-product.component.html',
    styleUrls: ['./update-product.component.scss'],
})
export class UpdateProductComponent implements OnInit, ILeavableFormComponent {
    product?: Product;
    isLoading = true;
    hasUnsavedData = false;

    constructor(
        private readonly productsService: ProductsService,
        public readonly deviceService: DeviceService,
        private readonly routerSelectors: RouterSelectors,
    ) {}

    ngOnInit(): void {
        this.routerSelectors.productId$
            .pipe(
                filterDefined(),
                switchMap(productId => this.productsService.getProduct(productId)),
                untilDestroyed(this),
            )
            .subscribe(product => {
                this.product = product;
                this.isLoading = false;
            });
    }

    formDirty(dirty: boolean) {
        this.hasUnsavedData = dirty;
    }

    formSaved() {
        this.hasUnsavedData = false;
    }
}
