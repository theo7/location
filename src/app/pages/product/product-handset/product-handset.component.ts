import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { EMPTY, Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { DiscussionsSelectors } from 'src/app/store/services/discussions-selectors.service';
import { LotDispatchers } from 'src/app/store/services/lot-dispatchers.service';
import {
    Discussion,
    Lot,
    Product,
    ProductStatusEnum,
    UserProfileStatus,
    UserType,
} from '../../../shared/models/models';
import { NavigationHistoryService } from '../../../shared/services/navigation-history.service';
import { ProductsDispatchers } from '../../../store/services/products-dispatchers.service';
import { ProductsSelectors } from '../../../store/services/products-selectors.service';
import { UserProfileSelectors } from '../../../store/services/user-selectors.service';

@UntilDestroy()
@Component({
    selector: 'app-product-handset',
    templateUrl: './product-handset.component.html',
    styleUrls: ['./product-handset.component.scss'],
})
export class ProductHandsetComponent implements OnInit, OnChanges {
    @Input() product?: Product;

    isLoading$?: Observable<boolean>;
    isCurrentUser$?: Observable<boolean>;
    isBlocked$?: Observable<boolean>;
    hasCartForSellerId$?: Observable<boolean>;
    lotOpenBySellerId$?: Observable<Lot | undefined>;
    discussionByLotId$?: Observable<Discussion | undefined>;
    hasMoreThanOneProductInDressing$?: Observable<boolean>;

    ProductStatusEnum = ProductStatusEnum;
    UserType = UserType;
    UserProfileStatus = UserProfileStatus;

    constructor(
        private readonly userProfileSelectors: UserProfileSelectors,
        private readonly navigationHistoryService: NavigationHistoryService,
        private readonly productsSelectors: ProductsSelectors,
        private readonly lotDispatchers: LotDispatchers,
        private readonly discussionsSelectors: DiscussionsSelectors,
        private readonly productsDispatchers: ProductsDispatchers,
    ) {}

    ngOnInit(): void {
        this.isLoading$ = this.productsSelectors.loading$.pipe(untilDestroyed(this));
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.product.currentValue) {
            this.isBlocked$ = this.userProfileSelectors.isBlocked$(this.product!.owner.id!).pipe(untilDestroyed(this));

            this.isCurrentUser$ = this.userProfileSelectors
                .isCurrentUser$(this.product!.owner.id!)
                .pipe(untilDestroyed(this));

            this.hasCartForSellerId$ = this.discussionsSelectors
                .hasCartForSellerId$(this.product!.owner.id!)
                .pipe(untilDestroyed(this));

            this.lotOpenBySellerId$ = this.discussionsSelectors
                .lotOpenBySellerId$(this.product!.owner.id!)
                .pipe(untilDestroyed(this));

            this.discussionByLotId$ = this.lotOpenBySellerId$.pipe(
                switchMap(lot => {
                    if (lot?.id) {
                        return this.discussionsSelectors.discussionByLotId$(lot.id);
                    }
                    return EMPTY;
                }),
                untilDestroyed(this),
            );
        }

        this.hasMoreThanOneProductInDressing$ = this.productsSelectors.hasMoreThanOneProductInDressing$.pipe(
            untilDestroyed(this),
        );
    }

    deleteProduct(): void {
        if (this.product) {
            this.productsDispatchers.deleteProduct(this.product.id);
        }
    }

    startDiscussion(): void {
        if (this.product) {
            this.lotDispatchers.startDiscussion(this.product.id);
        }
    }

    buy(): void {
        if (this.product) {
            this.lotDispatchers.startPaymentProcess(this.product.id);
        }
    }

    createLot(): void {
        if (this.product) {
            this.lotDispatchers.createLot(this.product);
        }
    }

    goBack(): void {
        this.navigationHistoryService.back();
    }
}
