import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSelectModule } from '@angular/material/select';
import { MatStepperModule } from '@angular/material/stepper';
import { HammerModule } from '@angular/platform-browser';
import { TranslateModule } from '@ngx-translate/core';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { IconsModule } from 'src/app/features/icons/icons.module';
import { HeaderModule } from 'src/app/features/layout/headers/header.module';
import { ImagesMiniModule } from 'src/app/shared/components/images-mini/images-mini.module';
import { UserBlockedWarningModule } from 'src/app/shared/components/user-blocked-warning/user-blocked-warning.module';
import { UserDressingHandsetModule } from '../../shared/components/app-user-dressing-handset/user-dressing-handset.module';
import { ButtonModule } from '../../shared/components/button/button.module';
import { C2cRegulationModule } from '../../shared/components/c2c-regulation/c2c-regulation.module';
import { PaymentProcessDialogModule } from '../../shared/components/payment-process-dialog/payment-process-dialog.module';
import { ProductDesktopDescriptionModule } from '../../shared/components/product-desktop-description/product-desktop-description.module';
import { ProductDesktopPhotoModule } from '../../shared/components/product-desktop-photo/product-desktop-photo.module';
import { ProductFilterModule } from '../../shared/components/product-filter/product-filter.module';
import { ProductFormModule } from '../../shared/components/product-form/product-form.module';
import { ProductListModule } from '../../shared/components/product-list/product-list.module';
import { ProductSheetModule } from '../../shared/components/product-sheet/product-sheet.module';
import { DirectivesModule } from '../../shared/directives/directives.module';
import { SharedModule } from '../../shared/shared.module';
import { CurrentReservationDialogComponent } from './current-reservation-dialog/current-reservation-dialog.component';
import { ProductDesktopUserComponent } from './product-desktop/product-desktop-user/product-desktop-user.component';
import { ProductDesktopComponent } from './product-desktop/product-desktop.component';
import { ProductHandsetComponent } from './product-handset/product-handset.component';
import { ProductRoutingModule } from './product-routing.module';
import { ProductComponent } from './product.component';
import { ReportProductDialogComponent } from './report-product-dialog/report-product-dialog.component';
import { ReportProductStep1Component } from './report-product-dialog/report-product-step1/report-product-step1.component';
import { ReportProductStep2Component } from './report-product-dialog/report-product-step2/report-product-step2.component';
import { UpdateProductComponent } from './update-product/update-product.component';

@NgModule({
    declarations: [
        ProductComponent,
        CurrentReservationDialogComponent,
        UpdateProductComponent,
        ProductHandsetComponent,
        ProductDesktopComponent,
        ProductDesktopUserComponent,
        ReportProductDialogComponent,
        ReportProductStep1Component,
        ReportProductStep2Component,
    ],
    imports: [
        SharedModule,
        MatButtonModule,
        MatDividerModule,
        ProductRoutingModule,
        HammerModule,
        MatProgressSpinnerModule,
        MatFormFieldModule,
        MatSelectModule,
        MatInputModule,
        MatListModule,
        MatStepperModule,
        ReactiveFormsModule,
        TranslateModule,
        ProductFormModule,
        PaymentProcessDialogModule,
        ProductListModule,
        ProductDesktopDescriptionModule,
        HeaderModule,
        ProductSheetModule,
        ButtonModule,
        ProductDesktopPhotoModule,
        UserDressingHandsetModule,
        InfiniteScrollModule,
        C2cRegulationModule,
        DirectivesModule,
        IconsModule,
        ImagesMiniModule,
        UserBlockedWarningModule,
        ProductFilterModule,
    ],
    exports: [ProductComponent, ProductDesktopComponent],
})
export class ProductModule {}
