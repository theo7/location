import { Component, OnInit } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { distinctUntilChanged } from 'rxjs/operators';
import { filterDefined } from 'src/app/shared/functions/operators.functions';
import { environment } from '../../../environments/environment';
import { Lot, Product } from '../../shared/models/models';
import { DeviceService } from '../../shared/services/device.service';
import { ProductsDispatchers } from '../../store/services/products-dispatchers.service';
import { ProductsSelectors } from '../../store/services/products-selectors.service';
import { RouterSelectors } from '../../store/services/router-selectors.service';

@UntilDestroy()
@Component({
    selector: 'app-product',
    templateUrl: './product.component.html',
    styleUrls: ['./product.component.scss'],
})
export class ProductComponent implements OnInit {
    product?: Product;
    lot?: Lot;

    showBanner = environment.showBanner;

    constructor(
        readonly deviceService: DeviceService,
        private readonly productsSelectors: ProductsSelectors,
        private readonly productsDispatchers: ProductsDispatchers,
        private readonly routerSelectors: RouterSelectors,
    ) {}

    ngOnInit(): void {
        this.routerSelectors.productId$
            .pipe(filterDefined(), distinctUntilChanged(), untilDestroyed(this))
            .subscribe(id => {
                this.productsDispatchers.loadProduct(id);
            });

        this.productsSelectors.product$.pipe(filterDefined(), untilDestroyed(this)).subscribe(product => {
            this.product = product;
        });
    }
}
