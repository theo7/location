import {
    Component,
    ElementRef,
    Input,
    OnChanges,
    OnInit,
    SimpleChanges,
    ViewChild,
    ViewEncapsulation,
} from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Actions, ofType } from '@ngrx/effects';
import { ROUTER_NAVIGATED } from '@ngrx/router-store';
import { Observable } from 'rxjs';
import { withLatestFrom } from 'rxjs/operators';
import { ShareService } from 'src/app/shared/services/share.service';
import { createLotSuccess } from 'src/app/store/actions/lot.actions';
import { DiscussionsSelectors } from 'src/app/store/services/discussions-selectors.service';
import { ProductListComponent } from '../../../../shared/components/product-list/product-list.component';
import { Product, UserProfile } from '../../../../shared/models/models';
import { ScrollStatesService } from '../../../../shared/services/scroll-states.service';
import { ProductsDispatchers } from '../../../../store/services/products-dispatchers.service';
import { ProductsSelectors } from '../../../../store/services/products-selectors.service';
import { RouterSelectors } from '../../../../store/services/router-selectors.service';
import { UserProfileSelectors } from '../../../../store/services/user-selectors.service';

@UntilDestroy()
@Component({
    selector: 'app-user-products-desktop',
    templateUrl: './product-desktop-user.component.html',
    styleUrls: ['./product-desktop-user.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class ProductDesktopUserComponent implements OnInit, OnChanges {
    canShareDressing = this.shareService.canShare();

    @ViewChild(ProductListComponent) productList?: ProductListComponent;
    @ViewChild('dressing') dressingElement!: ElementRef;

    @Input() owner?: UserProfile;
    @Input() hiddenProducts: Product[] = [];
    @Input() scrollEl?: HTMLElement;
    @Input() product?: Product;

    isCurrentUser$?: Observable<boolean>;
    canCreateLot$?: Observable<boolean>;
    products$?: Observable<Product[]>;
    hasMoreThanOneProducts$?: Observable<boolean>;
    hasCartForSellerId$?: Observable<boolean>;
    showDressingFilter$?: Observable<boolean>;
    productsCount$?: Observable<number>;

    creatingLot = false;

    constructor(
        private readonly userProfileSelectors: UserProfileSelectors,
        private readonly productsDispatchers: ProductsDispatchers,
        readonly productsSelectors: ProductsSelectors,
        private readonly scrollStateService: ScrollStatesService,
        private readonly routerSelectors: RouterSelectors,
        private readonly actions$: Actions,
        private readonly shareService: ShareService,
        private readonly discussionsSelectors: DiscussionsSelectors,
    ) {}

    ngOnInit(): void {
        this.actions$
            .pipe(ofType(ROUTER_NAVIGATED), withLatestFrom(this.routerSelectors.url$))
            .subscribe(([_, url]) => {
                if (this.scrollStateService.getScroll(url)) {
                    this.productList?.scrollToPreviousState();
                }
            });
        this.actions$.pipe(ofType(createLotSuccess), untilDestroyed(this)).subscribe(() => {
            this.dressingElement?.nativeElement.scrollIntoView({ behavior: 'smooth' });
        });
    }

    ngOnChanges(changes: SimpleChanges): void {
        this.isCurrentUser$ = this.userProfileSelectors.isCurrentUser$(this.owner!.id!).pipe(untilDestroyed(this));

        this.canCreateLot$ = this.userProfileSelectors.canCreateLot$(this.owner!.id!).pipe(untilDestroyed(this));

        this.products$ = this.productsSelectors
            .productsInDressingToDisplay$(this.owner!.id!, this.hiddenProducts)
            .pipe(untilDestroyed(this));

        this.hasCartForSellerId$ = this.discussionsSelectors
            .hasCartForSellerId$(this.owner!.id!)
            .pipe(untilDestroyed(this));

        this.hasMoreThanOneProducts$ = this.productsSelectors.hasMoreThanOneProductInDressing$.pipe(
            untilDestroyed(this),
        );
        this.showDressingFilter$ = this.productsSelectors.showDressingFilter$.pipe(untilDestroyed(this));
        this.productsCount$ = this.productsSelectors.dressingCount$.pipe(untilDestroyed(this));
    }

    onScroll(): void {
        if (this.owner?.id) {
            this.productsDispatchers.getNextDressing(this.owner.id);
        }
    }

    share(): void {
        if (this.owner) {
            this.productsDispatchers.shareDressing(this.owner);
        }
    }
}
