import { Location } from '@angular/common';
import { Component, ElementRef, Input, OnChanges, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { EMPTY, Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { filterDefined } from 'src/app/shared/functions/operators.functions';
import { RouterSelectors } from 'src/app/store/services/router-selectors.service';
import { UserProfileSelectors } from 'src/app/store/services/user-selectors.service';
import { Discussion, Lot, Product } from '../../../shared/models/models';
import { DiscussionsSelectors } from '../../../store/services/discussions-selectors.service';
import { ProductsSelectors } from '../../../store/services/products-selectors.service';

@UntilDestroy()
@Component({
    selector: 'app-product-desktop',
    templateUrl: './product-desktop.component.html',
    styleUrls: ['./product-desktop.component.scss'],
})
export class ProductDesktopComponent implements OnInit, OnChanges {
    @Input() product?: Product;

    @ViewChild('scrollProducts') scrollProductsElement?: ElementRef;

    isLoading$?: Observable<boolean>;
    isInLot$?: Observable<boolean | undefined>;
    isCurrentUser$?: Observable<boolean>;
    hasCartForSellerId$?: Observable<boolean>;
    lotOpenBySellerId$?: Observable<Lot | undefined>;
    discussionByLotId$?: Observable<Discussion | undefined>;

    constructor(
        private readonly discussionsSelectors: DiscussionsSelectors,
        private readonly productsSelectors: ProductsSelectors,
        private readonly routerSelectors: RouterSelectors,
        private readonly location: Location,
        private readonly userProfileSelectors: UserProfileSelectors,
    ) {}

    ngOnInit(): void {
        this.isLoading$ = this.productsSelectors.loading$.pipe(untilDestroyed(this));
        this.isInLot$ = this.routerSelectors.productId$.pipe(
            filterDefined(),
            switchMap(productId => this.discussionsSelectors.productIsInLot$(productId)),
            untilDestroyed(this),
        );
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.product.currentValue) {
            this.isCurrentUser$ = this.userProfileSelectors
                .isCurrentUser$(this.product!.owner.id!)
                .pipe(untilDestroyed(this));

            this.hasCartForSellerId$ = this.discussionsSelectors
                .hasCartForSellerId$(this.product!.owner.id!)
                .pipe(untilDestroyed(this));

            this.lotOpenBySellerId$ = this.discussionsSelectors
                .lotOpenBySellerId$(this.product!.owner.id!)
                .pipe(untilDestroyed(this));

            this.discussionByLotId$ = this.lotOpenBySellerId$.pipe(
                switchMap(lot => {
                    if (lot?.id) {
                        return this.discussionsSelectors.discussionByLotId$(lot.id);
                    }
                    return EMPTY;
                }),
                untilDestroyed(this),
            );
        }

        if (!!this.scrollProductsElement && !!changes.product) {
            this.scrollProductsElement.nativeElement.scrollTo(0, 0);
        }
    }

    close() {
        this.location.back();
    }
}
