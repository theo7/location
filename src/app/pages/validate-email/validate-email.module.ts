import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { TranslateModule } from '@ngx-translate/core';
import { HeaderModule } from 'src/app/features/layout/headers/header.module';
import { ValidateEmailRoutingModule } from './validate-email-routing.module';
import { ValidateEmailComponent } from './validate-email.component';

@NgModule({
    declarations: [ValidateEmailComponent],
    imports: [CommonModule, ValidateEmailRoutingModule, TranslateModule, HeaderModule, MatProgressSpinnerModule],
    providers: [],
})
export class ValidateEmailModule {}
