import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ValidateEmailComponent } from './validate-email.component';

const routes: Routes = [
    {
        path: '',
        component: ValidateEmailComponent,
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class ValidateEmailRoutingModule {}
