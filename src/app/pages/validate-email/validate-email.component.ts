import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { TranslateService } from '@ngx-translate/core';
import { switchMap, take } from 'rxjs/operators';
import { filterDefined } from 'src/app/shared/functions/operators.functions';
import { RouterSelectors } from 'src/app/store/services/router-selectors.service';
import { NotificationDialogComponent } from '../../shared/components/notification-dialog/notification-dialog.component';
import { DeviceService } from '../../shared/services/device.service';
import { UserService } from '../../shared/services/user.service';

@UntilDestroy()
@Component({
    selector: 'app-validate-email',
    templateUrl: './validate-email.component.html',
    styleUrls: ['./validate-email.component.scss'],
})
export class ValidateEmailComponent implements OnInit {
    haveFailed?: boolean;

    constructor(
        private readonly routerSelectors: RouterSelectors,
        private readonly router: Router,
        private readonly dialog: MatDialog,
        private readonly translateService: TranslateService,
        private readonly userService: UserService,
        public readonly deviceService: DeviceService,
    ) {}

    ngOnInit() {
        this.routerSelectors.token$
            .pipe(
                filterDefined(),
                take(1),
                switchMap(token => this.userService.validateEmail(token)),
                switchMap(() =>
                    this.dialog
                        .open(NotificationDialogComponent, {
                            panelClass: 'no-padding-dialog',
                            width: '280px',
                            data: {
                                type: 'success',
                                title: this.translateService.instant('profile.validateEmail.successDialog.title'),
                                content: this.translateService.instant('profile.validateEmail.successDialog.content'),
                                buttonContent: this.translateService.instant('common.continue'),
                            },
                        })
                        .afterClosed(),
                ),
                untilDestroyed(this),
            )
            .subscribe(
                () => {
                    this.router.navigate(['login']);
                },
                () => {
                    this.haveFailed = true;
                },
            );
    }
}
