import { Component, ViewEncapsulation } from '@angular/core';
import { DeviceService } from '../../../shared/services/device.service';

@Component({
    selector: 'app-lemonway-cgu',
    templateUrl: './lemonway-cgu.component.html',
    styleUrls: ['./lemonway-cgu.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class LemonwayCguComponent {
    constructor(public deviceService: DeviceService) {}
}
