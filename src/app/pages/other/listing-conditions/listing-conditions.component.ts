import { Component, ViewEncapsulation } from '@angular/core';
import { DeviceService } from 'src/app/shared/services/device.service';
import { environment } from 'src/environments/environment';

@Component({
    selector: 'app-listing-conditions',
    templateUrl: './listing-conditions.component.html',
    styleUrls: ['./listing-conditions.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class ListingConditionsComponent {
    contactEmail = environment.contactAddress;

    constructor(public readonly deviceService: DeviceService) {}
}
