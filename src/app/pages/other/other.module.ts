import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatExpansionModule } from '@angular/material/expansion';
import { RecaptchaFormsModule, RecaptchaModule } from 'ng-recaptcha';
import { HeaderModule } from 'src/app/features/layout/headers/header.module';
import { CustomQuillLinkModule } from '../../features/custom-quill-link/custom-quill-link.module';
import { AppCoreModule } from '../../shared/components/app-core/app-core.module';
import { PipesModule } from '../../shared/pipes/pipes.module';
import { SharedModule } from '../../shared/shared.module';
import { CguComponent } from './cgu/cgu.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { CookieComponent } from './cookies/cookie.component';
import { FaqComponent } from './faq/faq.component';
import { LegalNoticeComponent } from './legal-notice/legal-notice.component';
import { LemonwayCguComponent } from './lemonway-cgu/lemonway-cgu.component';
import { ListingConditionsComponent } from './listing-conditions/listing-conditions.component';
import { OtherRoutingModule } from './other-routing.module';
import { PrivacyComponent } from './privacy/privacy.component';

@NgModule({
    declarations: [
        FaqComponent,
        CguComponent,
        LemonwayCguComponent,
        CookieComponent,
        PrivacyComponent,
        LegalNoticeComponent,
        ListingConditionsComponent,
        ContactUsComponent,
    ],
    imports: [
        CommonModule,
        SharedModule,
        OtherRoutingModule,
        MatExpansionModule,
        MatButtonModule,
        HeaderModule,
        AppCoreModule,
        RecaptchaModule,
        RecaptchaFormsModule,
        PipesModule,
        CustomQuillLinkModule,
    ],
})
export class OtherModule {}
