import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { TranslateService } from '@ngx-translate/core';
import { groupBy } from 'lodash-es';
import { map } from 'rxjs/operators';
import { environment } from '../../../../environments/environment';
import { Faq } from '../../../shared/models/models';
import { DeviceService } from '../../../shared/services/device.service';
import { FaqService } from '../../../shared/services/faq.service';

interface GroupedFaqs {
    category: string;
    faqs: Faq[];
}

@UntilDestroy()
@Component({
    selector: 'app-faq',
    templateUrl: './faq.component.html',
    styleUrls: ['./faq.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class FaqComponent implements OnInit {
    contactEmail = environment.contactAddress;
    groupedFaqs?: GroupedFaqs[];

    constructor(
        public deviceService: DeviceService,
        private translateService: TranslateService,
        private readonly faqService: FaqService,
    ) {}

    ngOnInit() {
        this.faqService
            .listFaqs()
            .pipe(
                map(faqs => groupBy(faqs, f => f.category)),
                map(e => Object.keys(e).map(key => ({ category: key, faqs: e[key] }))),
                untilDestroyed(this),
            )
            .subscribe(groupedFaqs => {
                this.groupedFaqs = groupedFaqs;
            });
    }
}
