import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CguComponent } from './cgu/cgu.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { CookieComponent } from './cookies/cookie.component';
import { FaqComponent } from './faq/faq.component';
import { LegalNoticeComponent } from './legal-notice/legal-notice.component';
import { LemonwayCguComponent } from './lemonway-cgu/lemonway-cgu.component';
import { ListingConditionsComponent } from './listing-conditions/listing-conditions.component';
import { PrivacyComponent } from './privacy/privacy.component';

const routes: Routes = [
    { path: 'qa', component: FaqComponent },
    { path: 'cgu', component: CguComponent },
    { path: 'lemonway-cgu', component: LemonwayCguComponent },
    { path: 'cookies', component: CookieComponent },
    { path: 'privacy', component: PrivacyComponent },
    { path: 'listing-conditions', component: ListingConditionsComponent },
    { path: 'legal-notice', component: LegalNoticeComponent },
    { path: 'contact-us', component: ContactUsComponent },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class OtherRoutingModule {}
