import { Component, ViewEncapsulation } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { CookieConsentService } from 'src/app/features/cookie-consent/services/cookie-consent.service';
import { CookiesDispatchers } from 'src/app/store/services/cookies-dispatchers.service';
import { environment } from 'src/environments/environment';
import { DeviceService } from '../../../shared/services/device.service';

@UntilDestroy()
@Component({
    selector: 'app-cookie',
    templateUrl: './cookie.component.html',
    styleUrls: ['./cookie.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class CookieComponent {
    displayChangeConsentButton$: Observable<boolean>;

    constructor(
        public readonly deviceService: DeviceService,
        private readonly cookiesDispatchers: CookiesDispatchers,
        private readonly cookieConsentService: CookieConsentService,
    ) {
        this.displayChangeConsentButton$ = this.cookieConsentService.hasChosen$.pipe(
            map(hasChosen => environment.requireCookieConsent && hasChosen),
            untilDestroyed(this),
        );
    }

    onCookieConsentButtonClicked() {
        this.cookiesDispatchers.openConfigureModal();
    }
}
