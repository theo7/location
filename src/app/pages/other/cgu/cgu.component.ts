import { Component, ViewEncapsulation } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { DeviceService } from '../../../shared/services/device.service';

@Component({
    selector: 'app-cgu',
    templateUrl: './cgu.component.html',
    styleUrls: ['./cgu.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class CguComponent {
    contactEmail = environment.contactAddress;

    constructor(public readonly deviceService: DeviceService) {}
}
