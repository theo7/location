import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Actions, ofType } from '@ngrx/effects';
import { CustomValidators } from 'src/app/shared/helpers/custom-validators';
import { ContactSubject, ContactTheme, UserProfile } from 'src/app/shared/models/models';
import { ContactService } from 'src/app/shared/services/contact.service';
import { DeviceService } from 'src/app/shared/services/device.service';
import { ContactDispatchers } from 'src/app/store/services/contact-dispatchers.service';
import { UserProfileSelectors } from 'src/app/store/services/user-selectors.service';
import { environment } from 'src/environments/environment';
import * as ContactActions from '../../../store/actions/contact.actions';

@UntilDestroy()
@Component({
    selector: 'app-contact-us',
    templateUrl: './contact-us.component.html',
    styleUrls: ['./contact-us.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class ContactUsComponent implements OnInit {
    contactUsForm?: FormGroup;
    reCaptchaKey = environment.reCaptchaKey;
    user?: UserProfile;

    listTheme: ContactTheme[] = [];
    listSubject: ContactSubject[] = [];

    contactSend = false;

    loading$ = this.userProfileSelectors.isLoading$.pipe(untilDestroyed(this));

    constructor(
        public readonly deviceService: DeviceService,
        private readonly fb: FormBuilder,
        private readonly contactDispatchers: ContactDispatchers,
        private readonly userProfileSelectors: UserProfileSelectors,
        private readonly contactService: ContactService,
        private readonly actions$: Actions,
    ) {}

    ngOnInit(): void {
        this.userProfileSelectors.userProfile$.pipe(untilDestroyed(this)).subscribe(user => {
            this.user = user;
            this.initForm();
        });
        this.contactService.getContactSupportThemes().subscribe((list: ContactTheme[]) => {
            this.listTheme = list;
        });
        this.actions$
            .pipe(ofType(ContactActions.sendEmailContactSupportSuccess), untilDestroyed(this))
            .subscribe(() => {
                if (this.contactUsForm) {
                    this.initForm();
                    this.contactSend = true;
                }
            });
    }

    private initForm() {
        this.contactUsForm = this.fb.group({
            theme: ['', [Validators.required]],
            // change default subject when it will be used
            subject: [
                { value: { labelKey: 'Non renseigné' }, disabled: !this.theme },
                [Validators.required, Validators.maxLength(70)],
            ],
            email: [
                this.user?.email,
                {
                    validators: [Validators.required, Validators.pattern(CustomValidators.MAIL_PATTERN)],
                    updateOn: 'blur',
                },
            ],
            firstName: [
                { value: this.user?.firstName, disabled: !!this.user },
                [
                    Validators.required,
                    Validators.minLength(2),
                    Validators.maxLength(50),
                    Validators.pattern(CustomValidators.NAME_PATTERN),
                ],
            ],
            lastName: [
                { value: this.user?.lastName, disabled: !!this.user },
                [
                    Validators.required,
                    Validators.minLength(2),
                    Validators.maxLength(50),
                    Validators.pattern(CustomValidators.NAME_PATTERN),
                ],
            ],
            commandNumber: ['', [Validators.maxLength(50)]],
            message: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(2000)]],
            reCaptcha: [null, Validators.required],
            emailConnection: [this.user?.email],
            userAgent: [navigator.userAgent + ' / version : ' + environment.appVersion, Validators.required],
        });
    }

    sendContactUs() {
        this.contactDispatchers.sendEmailContactSupport(this.contactUsForm?.getRawValue());
    }

    loadSubjectsForTheme() {
        this.contactService.getContactSupportSubjects(this.theme?.id).subscribe((list: ContactSubject[]) => {
            this.listSubject = list;
        });
    }

    get theme() {
        return this.contactUsForm?.get('theme')?.value;
    }
}
