import { Component, ViewEncapsulation } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { DeviceService } from '../../../shared/services/device.service';

@Component({
    selector: 'app-privacy',
    templateUrl: './privacy.component.html',
    styleUrls: ['./privacy.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class PrivacyComponent {
    contactEmail = environment.contactAddress;

    constructor(public deviceService: DeviceService) {}
}
