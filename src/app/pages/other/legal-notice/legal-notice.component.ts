import { Component, ViewEncapsulation } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { DeviceService } from '../../../shared/services/device.service';

@Component({
    selector: 'app-legal-notice',
    templateUrl: './legal-notice.component.html',
    styleUrls: ['./legal-notice.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class LegalNoticeComponent {
    contactEmail = environment.contactAddress;

    constructor(public deviceService: DeviceService) {}
}
