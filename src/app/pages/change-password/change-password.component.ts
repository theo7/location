import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { TranslateService } from '@ngx-translate/core';
import { filter, first, map, switchMap } from 'rxjs/operators';
import { NotificationDialogComponent } from '../../shared/components/notification-dialog/notification-dialog.component';
import { DeviceService } from '../../shared/services/device.service';
import { UserProfileDispatchers } from '../../store/services/user-dispatchers.service';
import { UserProfileSelectors } from '../../store/services/user-selectors.service';

@UntilDestroy()
@Component({
    selector: 'app-change-password',
    templateUrl: './change-password.component.html',
    styleUrls: ['./change-password.component.scss'],
})
export class ChangePasswordComponent implements OnInit {
    form?: FormGroup;
    loading$ = this.userProfileSelectors.isLoading$.pipe(untilDestroyed(this));
    showCurrentPassword?: boolean;
    error?: string;

    @ViewChild('currentPasswordEl') currentPasswordEl?: ElementRef;

    constructor(
        private formBuilder: FormBuilder,
        private userProfileDispatchers: UserProfileDispatchers,
        private userProfileSelectors: UserProfileSelectors,
        private dialog: MatDialog,
        private router: Router,
        private translateService: TranslateService,
        public deviceService: DeviceService,
    ) {}

    ngOnInit() {
        this.form = this.formBuilder.group({
            current: '',
            password: this.formBuilder.control({
                value: '',
                confirmValue: '',
            }),
        });
    }

    showOrHideCurrentPassword() {
        this.showCurrentPassword = !this.showCurrentPassword;
        if (this.showCurrentPassword) {
            this.currentPasswordEl?.nativeElement?.removeAttribute('type');
        } else {
            this.currentPasswordEl?.nativeElement?.setAttribute('type', 'password');
        }
    }

    changePassword() {
        if (this.form?.valid) {
            const currentFormControl = this.form.get('current');
            const passwordFormControl = this.form.get('password');

            if (currentFormControl && passwordFormControl) {
                this.userProfileDispatchers.changePassword(currentFormControl.value, passwordFormControl.value.value);
            }

            // reset error
            this.error = undefined;
            this.showCurrentPassword = false;
            // listen for success event
            this.userProfileSelectors.userProfileState$
                .pipe(
                    filter(state => !state.loading),
                    first(),
                    filter(state => !state.error),
                    switchMap(() =>
                        this.dialog
                            .open(NotificationDialogComponent, {
                                panelClass: 'no-padding-dialog',
                                width: '280px',
                                data: {
                                    type: 'success',
                                    title: this.translateService.instant('changePassword.successDialog.title'),
                                    content: this.translateService.instant('changePassword.successDialog.content'),
                                    buttonContent: this.translateService.instant('common.continue'),
                                },
                            })
                            .afterClosed(),
                    ),
                    untilDestroyed(this),
                )
                .subscribe(() => this.router.navigate(['profile']));
            // listen for error event
            this.userProfileSelectors.userProfileState$
                .pipe(
                    filter(state => !state.loading),
                    first(),
                    filter(state => state.error),
                    map(state => state.errorReason),
                    untilDestroyed(this),
                )
                .subscribe(error => {
                    switch (error?.code) {
                        case 'auth/wrong-password':
                            setTimeout(() => this.form?.get('current')?.setErrors({ wrongPassword: true }));
                            break;
                        case 'auth/requires-recent-login':
                            this.error = this.translateService.instant('changePassword.error.needToLogin');
                            break;
                    }
                });
        }
    }
}
