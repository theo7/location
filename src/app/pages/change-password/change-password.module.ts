import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { HeaderModule } from 'src/app/features/layout/headers/header.module';
import { ButtonModule } from '../../shared/components/button/button.module';
import { PasswordWithConfirmModule } from '../../shared/components/password-with-confirm/password-with-confirm.module';
import { SharedModule } from '../../shared/shared.module';
import { ChangePasswordRoutingModule } from './change-password-routing.module';
import { ChangePasswordComponent } from './change-password.component';

@NgModule({
    declarations: [ChangePasswordComponent],
    imports: [
        SharedModule,
        ChangePasswordRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatInputModule,
        MatButtonModule,
        HeaderModule,
        ButtonModule,
        PasswordWithConfirmModule,
    ],
})
export class ChangePasswordModule {}
