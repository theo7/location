import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Actions, ofType } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { ConfirmActionDialogComponent } from '../../../shared/components/confirm-action-dialog/confirm-action-dialog.component';
import { Discussion, LotStatusEnum, RemunerationMode } from '../../../shared/models/models';
import * as DiscussionActions from '../../../store/actions/discussions.actions';
import * as LotActions from '../../../store/actions/lot.actions';
import { DiscussionsSelectors } from '../../../store/services/discussions-selectors.service';
import { LotDispatchers } from '../../../store/services/lot-dispatchers.service';
import { Lot } from './../../../shared/models/models';

@UntilDestroy()
@Component({
    selector: 'app-order-detail-dialog',
    templateUrl: './order-detail-dialog.component.html',
    styleUrls: ['./order-detail-dialog.component.scss'],
})
export class OrderDetailDialogComponent implements OnInit {
    isBuyer: boolean;
    loading?: boolean;

    LotStatusEnum = LotStatusEnum;
    RemunerationMode = RemunerationMode;

    discussion$?: Observable<Discussion | undefined>;
    canCancel$?: Observable<boolean>;

    lot: Lot;

    constructor(
        @Inject(MAT_DIALOG_DATA) private readonly data: { lot: Lot; isBuyer: boolean },
        private readonly actions$: Actions,
        private readonly dialog: MatDialog,
        private readonly discussionsSelectors: DiscussionsSelectors,
        private readonly lotDispatchers: LotDispatchers,
    ) {
        this.isBuyer = this.data.isBuyer;
        this.lot = this.data.lot;
    }

    ngOnInit() {
        this.canCancel$ = this.discussionsSelectors.isCancelSellEnabled$(this.lot.id!).pipe(
            map(canCancel => canCancel && this.data.isBuyer),
            untilDestroyed(this),
        );
        this.discussion$ = this.discussionsSelectors.discussionByLotId$(this.lot.id!).pipe(untilDestroyed(this));
        // loading event listener
        this.actions$
            .pipe(ofType(DiscussionActions.cancelSell, LotActions.changeRemunerationMode), untilDestroyed(this))
            .subscribe(() => (this.loading = true));
        this.actions$
            .pipe(
                ofType(
                    DiscussionActions.cancelSellSuccess,
                    DiscussionActions.cancelSellError,
                    LotActions.changeRemunerationModeSuccess,
                    LotActions.changeRemunerationModeError,
                ),
                untilDestroyed(this),
            )
            .subscribe(() => {
                this.loading = false;
            });
    }

    cancel(lotId: number | undefined) {
        if (lotId) {
            this.lotDispatchers.openConfirmCancelDialog(lotId);
        }
    }

    openChangeRemunerationModeDialog(): void {
        this.dialog
            .open(ConfirmActionDialogComponent, {
                data: {
                    titleKey: 'orders.detail.changeRemunerationMode.title',
                },
            })
            .afterClosed()
            .pipe(filter(e => e))
            .subscribe(() => {
                if (this.lot?.id) {
                    const remunerationMode =
                        this.lot.remunerationMode === RemunerationMode.CASH
                            ? RemunerationMode.VOUCHER
                            : RemunerationMode.CASH;
                    this.lotDispatchers.changeRemunerationMode(this.lot.id!, remunerationMode);
                }
            });
    }
}
