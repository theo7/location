import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Observable } from 'rxjs';
import { filter, first, map } from 'rxjs/operators';
import {
    getTotalPaidAfterRefund,
    getTransactionAmount,
    getTransactionRefund,
} from 'src/app/shared/functions/transaction.functions';
import { DiscussionsSelectors } from 'src/app/store/services/discussions-selectors.service';
import { Discussion, Lot, LotStatusEnum, RemunerationMode } from '../../../shared/models/models';
import { DeviceService } from '../../../shared/services/device.service';
import { OrderDetailDialogComponent } from '../order-detail-dialog/order-detail-dialog.component';

@UntilDestroy()
@Component({
    selector: 'app-order-card',
    templateUrl: './order-card.component.html',
    styleUrls: ['./order-card.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OrderCardComponent implements OnInit {
    @Input() lot!: Lot;
    @Input() isBuyer!: boolean;

    discussion$?: Observable<Discussion | undefined>;
    hasDiscussion$?: Observable<boolean>;

    LotStatusEnum = LotStatusEnum;
    RemunerationMode = RemunerationMode;

    constructor(
        private readonly dialog: MatDialog,
        private readonly deviceService: DeviceService,
        private readonly router: Router,
        private readonly discussionsSelectors: DiscussionsSelectors,
    ) {}

    ngOnInit(): void {
        this.discussion$ = this.discussionsSelectors.discussionByLotId$(this.lot.id!).pipe(untilDestroyed(this));
        this.hasDiscussion$ = this.discussion$.pipe(
            map(discussion => !!discussion),
            untilDestroyed(this),
        );
    }

    get refund(): number {
        return this.lot ? getTransactionRefund(this.lot) : 0;
    }

    get total(): number | undefined {
        return this.lot ? getTransactionAmount(this.lot, this.isBuyer) : 0;
    }

    get totalAfterRefund(): number | undefined {
        return this.lot ? getTotalPaidAfterRefund(this.lot, this.isBuyer) : 0;
    }

    openDetailDialog(): void {
        this.discussion$?.pipe(first()).subscribe(discussion => {
            const dialogDeviceParams = device =>
                ({
                    handset: {
                        width: '100vw',
                        maxWidth: '100vw',
                        height: '100vh',
                        panelClass: 'full-screen-dialog',
                    },
                    desktop: {
                        height: '80vh',
                        width: '600px',
                        maxWidth: '600px',
                        panelClass: 'no-padding-dialog',
                    },
                }[device]);

            this.dialog
                .open(OrderDetailDialogComponent, {
                    autoFocus: false,
                    data: {
                        lot: this.lot,
                        isBuyer: this.isBuyer,
                    },
                    ...dialogDeviceParams(this.deviceService.mode),
                })
                .afterClosed()
                .pipe(
                    filter(e => e),
                    untilDestroyed(this),
                )
                .subscribe(() => {
                    if (discussion) {
                        this.router.navigate(['chat', discussion.id]);
                    }
                });
        });
    }
}
