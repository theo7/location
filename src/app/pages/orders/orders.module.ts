import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatTabsModule } from '@angular/material/tabs';
import { TranslateModule } from '@ngx-translate/core';
import { HeaderModule } from 'src/app/features/layout/headers/header.module';
import { AppCoreModule } from '../../shared/components/app-core/app-core.module';
import { ImagesMiniModule } from '../../shared/components/images-mini/images-mini.module';
import { PaymentLinesModule } from '../../shared/components/payment-lines/payment-lines.module';
import { ProductLineModule } from '../../shared/components/product-line/product-line.module';
import { PipesModule } from '../../shared/pipes/pipes.module';
import { SharedModule } from '../../shared/shared.module';
import { OrderCardComponent } from './order-card/order-card.component';
import { OrderDetailDialogComponent } from './order-detail-dialog/order-detail-dialog.component';
import { OrdersRoutingModule } from './orders-routing.module';
import { OrdersComponent } from './orders.component';

@NgModule({
    declarations: [OrdersComponent, OrderCardComponent, OrderDetailDialogComponent],
    imports: [
        CommonModule,
        SharedModule,
        OrdersRoutingModule,
        MatProgressSpinnerModule,
        MatTabsModule,
        MatDividerModule,
        TranslateModule,
        ImagesMiniModule,
        ProductLineModule,
        PaymentLinesModule,
        MatButtonModule,
        PipesModule,
        AppCoreModule,
        HeaderModule,
    ],
})
export class OrdersModule {}
