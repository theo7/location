import { ChangeDetectionStrategy, Component, ViewEncapsulation } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { OrdersDispatchers } from 'src/app/store/services/orders-dispatchers.service';
import { OrdersSelectors } from 'src/app/store/services/orders-selectors.service';
import { DeviceService } from '../../shared/services/device.service';

@UntilDestroy()
@Component({
    selector: 'app-orders',
    templateUrl: './orders.component.html',
    styleUrls: ['./orders.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    encapsulation: ViewEncapsulation.None,
})
export class OrdersComponent {
    isLoading$ = this.ordersSelectors.loading$.pipe(untilDestroyed(this));
    purchases$ = this.ordersSelectors.purchases$.pipe(untilDestroyed(this));
    sales$ = this.ordersSelectors.sales$.pipe(untilDestroyed(this));

    constructor(
        private readonly ordersSelectors: OrdersSelectors,
        ordersDispatchers: OrdersDispatchers,
        public readonly deviceService: DeviceService,
    ) {
        ordersDispatchers.loadSales();
        ordersDispatchers.loadPurchases();
    }
}
