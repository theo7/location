import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserFollowingComponent } from './user-following.component';

const routes: Routes = [
    {
        path: '',
        component: UserFollowingComponent,
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class UserFollowingRoutingModule {}
