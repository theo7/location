import { Observable, of } from '@angular-devkit/core/node_modules/rxjs';
import { ChangeDetectionStrategy, Component, ViewChild } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { InfiniteScrollDirective } from 'ngx-infinite-scroll';
import { first, switchMap, withLatestFrom } from 'rxjs/operators';
import { UserFollowingProfile, UserProfile, UserProfileStatus, UserType } from 'src/app/shared/models/models';
import { DeviceService } from 'src/app/shared/services/device.service';
import { UserService } from 'src/app/shared/services/user.service';
import { FollowDispatchers } from 'src/app/store/services/follow-dispatchers.service';
import { FollowSelectors } from 'src/app/store/services/follow-selectors.service';
import { ProfileSelectors } from 'src/app/store/services/profile-selectors.service';
import { RouterSelectors } from 'src/app/store/services/router-selectors.service';
import { UserProfileSelectors } from 'src/app/store/services/user-selectors.service';

@UntilDestroy()
@Component({
    selector: 'app-user-following',
    templateUrl: './user-following.component.html',
    styleUrls: ['./user-following.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserFollowingComponent {
    UserProfileStatus = UserProfileStatus;
    UserType = UserType;

    userFollowing$: Observable<UserFollowingProfile[]> = of([]);
    user$: Observable<UserProfile | undefined>;
    isCurrentUser$: Observable<boolean>;
    isFollowingLoading$: Observable<boolean>;

    private page?: number;
    private maxPage?: number;

    hasCallNextCpt = false;
    followingLoading: boolean = false;

    @ViewChild(InfiniteScrollDirective) infiniteScroll?: InfiniteScrollDirective;

    constructor(
        public readonly deviceService: DeviceService,
        private readonly followSelectors: FollowSelectors,
        private readonly userService: UserService,
        private readonly followDispatchers: FollowDispatchers,
        private readonly userProfileSelectors: UserProfileSelectors,
        private readonly routerSelectors: RouterSelectors,
        private readonly profileSelectors: ProfileSelectors,
    ) {
        const currentUser$ = this.userProfileSelectors.userProfile$;

        this.user$ = this.routerSelectors.userId$.pipe(
            withLatestFrom(currentUser$),
            switchMap(([userId, currentUser]) => {
                if (!userId || currentUser?.id === userId) {
                    return currentUser$;
                }

                return this.profileSelectors.current$;
            }),
            untilDestroyed(this),
        );

        this.isCurrentUser$ = this.user$.pipe(
            switchMap(user => {
                if (!user?.id) {
                    return of(false);
                }
                return this.userProfileSelectors.isCurrentUser$(user.id);
            }),
            untilDestroyed(this),
        );

        this.userFollowing$ = this.followSelectors.following$.pipe(untilDestroyed(this));

        this.followSelectors.followingPage$.pipe(untilDestroyed(this)).subscribe(page => {
            this.page = page;
        });

        this.followSelectors.followingMaxPage$.pipe(untilDestroyed(this)).subscribe(maxPage => {
            if (maxPage !== undefined) {
                this.maxPage = maxPage;
            }
        });

        this.isFollowingLoading$ = this.followSelectors.isFollowingLoading$;

        this.isFollowingLoading$.pipe(untilDestroyed(this)).subscribe(loading => (this.followingLoading = loading));

        this.followDispatchers.resetFollowing();
        this.loadNextPage();
    }

    trackById = (index: number, user: UserFollowingProfile): number => user.id!;

    onScroll(): void {
        if (!this.hasCallNextCpt) {
            this.infiniteScroll?.ngOnDestroy();
            this.infiniteScroll?.setup();
            this.hasCallNextCpt = true;
        }

        this.loadNextPage();
    }

    loadNextPage() {
        const currentUser$ = this.userProfileSelectors.userProfile$;

        currentUser$.pipe(first(), withLatestFrom(this.routerSelectors.userId$)).subscribe(([currentUser, userId]) => {
            const followUserId = userId || currentUser?.id;

            if (
                this.maxPage === undefined ||
                this.page === undefined ||
                (this.maxPage && this.page < this.maxPage && !this.followingLoading)
            ) {
                if (followUserId && this.page !== undefined) {
                    this.followDispatchers.getUserFollowing(followUserId, this.page);
                }
            }
        });
    }

    getAvatarUrl(user: UserFollowingProfile): string {
        return this.userService.getAvatarUrl(user);
    }
}
