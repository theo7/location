import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSelectModule } from '@angular/material/select';
import { IMaskModule } from 'angular-imask';
import { HeaderModule } from 'src/app/features/layout/headers/header.module';
import { AppCoreModule } from '../../shared/components/app-core/app-core.module';
import { ButtonModule } from '../../shared/components/button/button.module';
import { DirectivesModule } from '../../shared/directives/directives.module';
import { SharedModule } from '../../shared/shared.module';
import { EditProfilRoutingModule } from './edit-profil-routing.module';
import { EditProfilComponent } from './edit-profil.component';

@NgModule({
    declarations: [EditProfilComponent],
    imports: [
        SharedModule,
        EditProfilRoutingModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatProgressSpinnerModule,
        MatButtonModule,
        HeaderModule,
        AppCoreModule,
        ButtonModule,
        IMaskModule,
        DirectivesModule,
    ],
    providers: [],
})
export class EditProfilModule {}
