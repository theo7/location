import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Actions, ofType } from '@ngrx/effects';
import { TranslateService } from '@ngx-translate/core';
import * as dayjs from 'dayjs';
import { EMPTY } from 'rxjs';
import {
    debounceTime,
    distinctUntilChanged,
    filter,
    finalize,
    first,
    map,
    mergeMap,
    skip,
    switchMap,
} from 'rxjs/operators';
import { ProfileDispatchers } from 'src/app/store/services/profile-dispatchers.service';
import { ErrorNotificationDialogComponent } from '../../shared/components/error-notification-dialog/error-notification-dialog.component';
import { SimpleDialogComponent } from '../../shared/components/simple-dialog/simple-dialog.component';
import { CustomValidators } from '../../shared/helpers/custom-validators';
import { AccountStatus, ModerationStatus, Picture, PictureType, UserProfile } from '../../shared/models/models';
import { DeviceService } from '../../shared/services/device.service';
import { ImageService } from '../../shared/services/image.service';
import { ProductsService } from '../../shared/services/products.service';
import { UserService } from '../../shared/services/user.service';
import * as NotificationsActions from '../../store/actions/notifications.actions';
import { UserProfileDispatchers } from '../../store/services/user-dispatchers.service';
import { UserProfileSelectors } from '../../store/services/user-selectors.service';

const PHONE_REGEXP = /^(0[1-9]{1}[0-9]{8})$|^((\+33)[1-9]{1}[0-9]{8})$|^(0033[1-9]{1}[0-9]{8})$/;
const NICKNAME_REGEX = '^[A-Za-zÀ-ÖØ-öø-ÿ0-9]*$';

@UntilDestroy()
@Component({
    selector: 'app-edit-profil',
    templateUrl: './edit-profil.component.html',
    styleUrls: ['./edit-profil.component.scss'],
})
export class EditProfilComponent implements OnInit {
    editProfilForm?: FormGroup;
    isLoading = true;
    user?: UserProfile;
    status?: AccountStatus;
    isUploadingPicture = false;
    hasNewPicture = false;

    constructor(
        private readonly actions$: Actions,
        private readonly fb: FormBuilder,
        private readonly compressImageService: ImageService,
        private readonly dialog: MatDialog,
        private readonly productsService: ProductsService,
        private readonly translateService: TranslateService,
        private readonly userDispatchers: UserProfileDispatchers,
        private readonly userProfileSelectors: UserProfileSelectors,
        private readonly userService: UserService,
        private readonly imageService: ImageService,
        public readonly deviceService: DeviceService,
        private readonly profileDispatchers: ProfileDispatchers,
    ) {}

    ngOnInit(): void {
        this.userProfileSelectors.userProfile$
            .pipe(
                first(),
                switchMap(user =>
                    this.userService
                        .getPaymentAccountStatus()
                        .pipe(map(status => [status, user] as [AccountStatus, UserProfile | undefined])),
                ),
                finalize(() => (this.isLoading = false)),
            )
            .subscribe(([status, user]) => {
                this.status = status;
                this.user = user;

                const birthDate = dayjs(user?.birthDate?.toString(), 'DD-MM-YYYY').format('DD/MM/YYYY');

                this.editProfilForm = this.fb.group({
                    picture: [this.user?.picture],
                    firstName: [
                        this.user?.firstName,
                        [
                            Validators.required,
                            Validators.minLength(2),
                            Validators.pattern(CustomValidators.NAME_PATTERN),
                        ],
                    ],
                    lastName: [
                        this.user?.lastName,
                        [
                            Validators.required,
                            Validators.minLength(2),
                            Validators.pattern(CustomValidators.NAME_PATTERN),
                        ],
                    ],
                    nickname: [
                        this.user?.nickname,
                        {
                            validators: [
                                Validators.required,
                                Validators.pattern(NICKNAME_REGEX),
                                CustomValidators.unauthorizedName(),
                            ],
                            asyncValidators: [
                                CustomValidators.uniqueNicknameAsyncValidator(this.userService, this.user?.nickname),
                            ],
                            updateOn: 'blur',
                        },
                    ],
                    gender: [this.user?.gender, Validators.required],
                    birthDate: [
                        birthDate,
                        [
                            Validators.required,
                            CustomValidators.validDateFormat(),
                            CustomValidators.legalDate(),
                            CustomValidators.minimumDate('01/01/1900'),
                        ],
                    ],
                    email: [
                        {
                            value: this.user?.email,
                            disabled: true,
                        },
                        Validators.required,
                    ],
                    phoneNumber: [this.user?.phoneNumber, [Validators.pattern(PHONE_REGEXP), Validators.maxLength(13)]],
                    presentation: [
                        this.user?.presentation,
                        [
                            CustomValidators.moderatedField(
                                this.user?.presentation,
                                this.user?.presentationStatus !== ModerationStatus.MODERATED,
                            ),
                        ],
                    ],
                    nationality: [this.user?.nationality],
                });

                if (this.user?.presentationStatus === ModerationStatus.MODERATED) {
                    this.editProfilForm.get('presentation')?.markAsTouched();
                }
                this.listenForFormChanges();
            });
        this.listenForEvents();
    }

    private listenForEvents(): void {
        this.actions$
            .pipe(ofType(NotificationsActions.requestPermissionFailed), debounceTime(10), untilDestroyed(this))
            .subscribe(() => {
                this.editProfilForm?.patchValue({
                    notificationsEnabled: false,
                });
            });

        this.actions$
            .pipe(ofType(NotificationsActions.revokePermissionFailed), debounceTime(10), untilDestroyed(this))
            .subscribe(() => {
                this.editProfilForm?.patchValue({
                    notificationsEnabled: true,
                });
            });
        // error events (invalid name or account status)
        this.userProfileSelectors.error$
            .pipe(
                skip(1),
                filter(error => !!error),
                map(error => error.error),
                untilDestroyed(this),
            )
            .subscribe(error => {
                this.isLoading = false;
                if (error.code === 'PAYMENT_017') {
                    this.editProfilForm?.get('lastName')?.setErrors({ invalidName: true });
                    this.editProfilForm?.get('firstName')?.setErrors({ invalidName: true });
                }
            });
    }

    editPicture(event: FileList): void {
        if (event.length === 0) {
            return;
        }

        if (event.length > 1) {
            return this.openDialog(this.translateService.instant('editProfile.tooManyFiles'));
        }

        const newFile = event.item(0);
        if (!newFile || !newFile.type.startsWith('image')) {
            return this.openDialog(this.translateService.instant('editProfile.onlyPicture'));
        }
        this.isUploadingPicture = true;
        this.compressImageService
            .compress(newFile)
            .pipe(
                mergeMap(file => this.productsService.uploadPicture(file, PictureType.AVATAR)),
                first(),
                finalize(() => {
                    this.isUploadingPicture = false;
                }),
            )
            .subscribe({
                next: (productPicture: Picture) => {
                    this.hasNewPicture = true;
                    this.editProfilForm?.get('picture')?.setValue(productPicture);
                },
                error: error => {
                    return this.openDialog(this.translateService.instant('editProfile.uploadError'));
                },
            });
    }

    private openDialog(message): void {
        this.dialog.open(SimpleDialogComponent, {
            width: '250px',
            data: { title: message, action: 'Ok' },
        });
    }

    private listenForFormChanges(): void {
        this.editProfilForm
            ?.get('nickname')
            ?.valueChanges.pipe(
                debounceTime(300),
                distinctUntilChanged(),
                filter(value => this.user?.nickname !== value && !!value),
                switchMap(value =>
                    this.editProfilForm?.controls?.nickname?.errors ? EMPTY : this.userService.checkNickname(value),
                ),
                filter(taken => !!taken),
            )
            .subscribe(() => this.editProfilForm?.get('nickname')?.setErrors({ notUnique: true }));
    }

    save(): void {
        if (!this.editProfilForm || this.editProfilForm.invalid) {
            return;
        }

        const updatedUser = { ...this.editProfilForm.value };
        updatedUser.birthDate = dayjs(updatedUser.birthDate, 'DD/MM/YYYY').format('DD-MM-YYYY');
        this.userDispatchers.updateUserProfile({
            ...this.user,
            ...updatedUser,
            presentationStatus: ModerationStatus.AWAIT_MODERATION,
        });
        this.isLoading = true;
    }

    onDeleteAccountButtonClicked(): void {
        this.profileDispatchers.openDeleteAccountModal();
    }

    getCompleteUrl(fileName): string {
        return this.imageService.getUrl(fileName);
    }

    openInvalidAccountStatusDialog(): void {
        this.dialog.open(ErrorNotificationDialogComponent, {
            width: '280px',
            data: {
                title: this.translateService.instant('userForm.invalidAccountStatus.title'),
                content: this.translateService.instant('userForm.invalidAccountStatus.content'),
                buttonContent: this.translateService.instant('common.ok'),
            },
        });
    }

    onRemovePictureButtonClicked() {
        this.hasNewPicture = true;
        this.editProfilForm?.get('picture')?.setValue(null);
    }
}
