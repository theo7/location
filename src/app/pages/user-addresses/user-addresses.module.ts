import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { TranslateModule } from '@ngx-translate/core';
import { HeaderModule } from 'src/app/features/layout/headers/header.module';
import { AppCoreModule } from '../../shared/components/app-core/app-core.module';
import { ButtonModule } from '../../shared/components/button/button.module';
import { UserAddressCardModule } from '../../shared/components/user-address-card/user-address-card.module';
import { SharedModule } from '../../shared/shared.module';
import { AddressesIconComponent } from './addresses-icon/addresses-icon.component';
import { UserAddressesRoutingModule } from './user-addresses-routing.module';
import { UserAddressesComponent } from './user-addresses.component';

@NgModule({
    declarations: [UserAddressesComponent, AddressesIconComponent],
    imports: [
        CommonModule,
        UserAddressesRoutingModule,
        TranslateModule,
        MatButtonModule,
        MatIconModule,
        SharedModule,
        ReactiveFormsModule,
        UserAddressCardModule,
        ButtonModule,
        HeaderModule,
        AppCoreModule,
    ],
})
export class UserAddressesModule {}
