import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';
import { filter } from 'rxjs/operators';
import { addAddressDialogComponent$ } from 'src/app/imports.dynamic';
import { ConfirmationDialogComponent } from '../../shared/components/confirmation-dialog/confirmation-dialog.component';
import { Address, UserProfile } from '../../shared/models/models';
import { DeviceService } from '../../shared/services/device.service';
import { UserProfileDispatchers } from '../../store/services/user-dispatchers.service';
import { UserProfileSelectors } from '../../store/services/user-selectors.service';

@UntilDestroy()
@Component({
    selector: 'app-user-addresses',
    templateUrl: './user-addresses.component.html',
    styleUrls: ['./user-addresses.component.scss'],
})
export class UserAddressesComponent implements OnInit {
    loading$?: Observable<boolean>;
    user?: UserProfile;
    addresses: Address[] = [];
    initialDefaultAddressId?: number;

    private dialogDeviceParams = device =>
        ({
            handset: {
                width: '100vw',
                maxWidth: '100vw',
                height: '100vh',
                panelClass: 'full-screen-dialog',
            },
            desktop: {
                height: '80vh',
                width: '600px',
                maxWidth: '600px',
                disableClose: true,
                panelClass: 'no-padding-dialog',
            },
        }[device]);

    constructor(
        private dialog: MatDialog,
        private userProfileSelectors: UserProfileSelectors,
        private userDispatchers: UserProfileDispatchers,
        private translateService: TranslateService,
        public deviceService: DeviceService,
    ) {}

    ngOnInit() {
        this.loading$ = this.userProfileSelectors.isLoading$;
        this.userProfileSelectors.userProfile$.pipe(untilDestroyed(this)).subscribe(user => {
            this.initialDefaultAddressId = this.initialDefaultAddressId || user?.addresses?.find(a => a.isDefault)?.id;
            this.user = user;
            this.addresses = [...(this.user?.addresses || [])].sort((addr1, addr2) => {
                if (addr1.id === this.initialDefaultAddressId) {
                    return -1;
                } else if (addr2.id === this.initialDefaultAddressId) {
                    return 1;
                }

                return (addr1.id || 0) - (addr2.id || 0);
            });
        });
    }

    openAddAddressDialog() {
        addAddressDialogComponent$.subscribe(AddAddressDialogComponent => {
            this.dialog.open(AddAddressDialogComponent, {
                autoFocus: false,
                data: {
                    user: this.user,
                },
                ...this.dialogDeviceParams(this.deviceService.mode),
            });
        });
    }

    deleteAddress(address: Address) {
        this.dialog
            .open(ConfirmationDialogComponent, {
                data: {
                    title: this.translateService.instant('address.validateDeletion'),
                    validateLabel: this.translateService.instant('common.validate'),
                    cancelLabel: this.translateService.instant('common.cancel'),
                },
            })
            .afterClosed()
            .pipe(filter(e => e && !address.isDefault))
            .subscribe(() => {
                this.userDispatchers.deleteUserAddress(address);
            });
    }

    editAddress(address: Address) {
        addAddressDialogComponent$.subscribe(AddAddressDialogComponent => {
            this.dialog.open(AddAddressDialogComponent, {
                autoFocus: false,
                data: {
                    address,
                    user: this.user,
                },
                ...this.dialogDeviceParams(this.deviceService.mode),
            });
        });
    }

    setDefaultAddress(address: Address) {
        this.userDispatchers.updateUserAddress({ ...address, isDefault: true });
    }
}
