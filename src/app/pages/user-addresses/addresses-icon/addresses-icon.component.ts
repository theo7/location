import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
    selector: 'app-addresses-icon',
    templateUrl: './addresses-icon.component.html',
    styleUrls: ['./addresses-icon.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AddressesIconComponent {}
