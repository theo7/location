import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { filter, first } from 'rxjs/operators';
import { DeviceService } from '../../shared/services/device.service';
import { UserProfileDispatchers } from '../../store/services/user-dispatchers.service';
import { UserProfileSelectors } from '../../store/services/user-selectors.service';

@UntilDestroy()
@Component({
    selector: 'app-reset-password',
    templateUrl: './reset-password.component.html',
    styleUrls: ['./reset-password.component.scss'],
})
export class ResetPasswordComponent implements OnInit {
    loading$ = this.userProfileSelectors.isLoading$.pipe(untilDestroyed(this));
    form?: FormGroup;
    resetPasswordEmail?: string;

    constructor(
        private formBuilder: FormBuilder,
        private userProfileDispatchers: UserProfileDispatchers,
        private userProfileSelectors: UserProfileSelectors,
        public deviceService: DeviceService,
    ) {}

    ngOnInit(): void {
        this.form = this.formBuilder.group({
            email: ['', [Validators.required, Validators.email]],
        });
    }

    resetPassword() {
        if (this.form?.valid) {
            const email = this.form.controls.email.value.trim().toLowerCase();
            this.userProfileDispatchers.resetPassword(email);
            this.userProfileSelectors.userProfileState$
                .pipe(
                    filter(state => !state.loading),
                    first(),
                    untilDestroyed(this),
                )
                .subscribe(() => {
                    this.resetPasswordEmail = email;
                });
        }
    }
}
