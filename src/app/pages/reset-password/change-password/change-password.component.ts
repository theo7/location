import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { TranslateService } from '@ngx-translate/core';
import { finalize, switchMap, take, tap } from 'rxjs/operators';
import { filterDefined } from 'src/app/shared/functions/operators.functions';
import { RouterSelectors } from 'src/app/store/services/router-selectors.service';
import { AuthenticationService } from '../../../features/authentication/service/authentication.service';
import { NotificationDialogComponent } from '../../../shared/components/notification-dialog/notification-dialog.component';
import { DeviceService } from '../../../shared/services/device.service';

@UntilDestroy()
@Component({
    selector: 'app-change-password',
    templateUrl: './change-password.component.html',
    styleUrls: ['./change-password.component.scss'],
})
export class ChangePasswordComponent implements OnInit {
    loading?: boolean;
    form?: FormGroup;
    error?: string;

    private token?: string;

    constructor(
        private readonly routerSelectors: RouterSelectors,
        private readonly router: Router,
        private readonly formBuilder: FormBuilder,
        private readonly dialog: MatDialog,
        private readonly translateService: TranslateService,
        private readonly authenticationService: AuthenticationService,
        public readonly deviceService: DeviceService,
    ) {}

    ngOnInit() {
        this.loading = true;
        this.form = this.formBuilder.group({
            password: this.formBuilder.control({
                value: '',
                confirmValue: '',
            }),
        });
        this.routerSelectors.token$
            .pipe(
                filterDefined(),
                take(1),
                tap(token => (this.token = token)),
                switchMap(token => this.authenticationService.checkTokenValidity(token)),
                finalize(() => (this.loading = false)),
                untilDestroyed(this),
            )
            .subscribe(
                validToken => {
                    if (!validToken) {
                        this.error = this.translateService.instant('resetPassword.invalidToken');
                    }
                },
                () => {
                    this.error = this.translateService.instant('resetPassword.invalidToken');
                },
            );
    }

    resetPassword() {
        if (!this.form || this.form.invalid) {
            return;
        }
        if (!this.token) {
            throw Error('Token is required');
        }

        const password = this.form.controls.password.value.value;

        this.loading = true;
        this.authenticationService
            .resetPassword(password, this.token)
            .pipe(
                switchMap(() =>
                    this.dialog
                        .open(NotificationDialogComponent, {
                            panelClass: 'no-padding-dialog',
                            width: '280px',
                            data: {
                                type: 'success',
                                title: this.translateService.instant('resetPassword.successDialog.title'),
                                content: this.translateService.instant('resetPassword.successDialog.content'),
                                buttonContent: this.translateService.instant('common.continue'),
                            },
                        })
                        .afterClosed(),
                ),
                finalize(() => (this.loading = false)),
            )
            .subscribe(
                () => {
                    this.router.navigate(['login']);
                },
                () => {
                    this.error = this.translateService.instant('resetPassword.invalidToken');
                },
            );
    }
}
