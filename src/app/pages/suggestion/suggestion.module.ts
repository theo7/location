import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IconsModule } from 'src/app/features/icons/icons.module';
import { HeaderModule } from 'src/app/features/layout/headers/header.module';
import { AppCoreModule } from 'src/app/shared/components/app-core/app-core.module';
import { ProductListModule } from 'src/app/shared/components/product-list/product-list.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { SuggestionRoutingModule } from './suggestion-routing.module';
import { SuggestionComponent } from './suggestion.component';

@NgModule({
    declarations: [SuggestionComponent],
    imports: [
        CommonModule,
        SuggestionRoutingModule,
        TranslateModule,
        HeaderModule,
        ProductListModule,
        AppCoreModule,
        SharedModule,
        IconsModule,
    ],
})
export class SuggestionModule {}
