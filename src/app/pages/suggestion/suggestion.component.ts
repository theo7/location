import { Observable } from '@angular-devkit/core/node_modules/rxjs';
import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Product } from 'src/app/shared/models/models';
import { DeviceService } from 'src/app/shared/services/device.service';
import { NavigationHistoryService } from 'src/app/shared/services/navigation-history.service';
import { SuggestionDispatchers } from 'src/app/store/services/suggestion-dispatchers.service';
import { SuggestionSelectors } from 'src/app/store/services/suggestion-selectors.service';

@UntilDestroy()
@Component({
    selector: 'app-suggestion',
    templateUrl: './suggestion.component.html',
    styleUrls: ['./suggestion.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuggestionComponent implements OnInit {
    isLoading$: Observable<boolean>;
    suggestions$: Observable<Product[]>;
    totalProducts$: Observable<number>;

    window = window;

    constructor(
        public readonly deviceService: DeviceService,
        private readonly suggestionSelectors: SuggestionSelectors,
        private readonly suggestionDispatchers: SuggestionDispatchers,
        private readonly router: Router,
        private readonly navigationHistoryService: NavigationHistoryService,
    ) {
        this.isLoading$ = this.suggestionSelectors.isLoading$.pipe(untilDestroyed(this));
        this.suggestions$ = this.suggestionSelectors.suggestion$.pipe(untilDestroyed(this));
        this.totalProducts$ = this.suggestionSelectors.totalProducts$.pipe(untilDestroyed(this));
    }

    back() {
        this.navigationHistoryService.back();
    }

    ngOnInit(): void {
        this.suggestionDispatchers.loadSuggestion();
    }

    onLoadMore() {
        this.suggestionDispatchers.loadMoreSuggestion();
    }
}
