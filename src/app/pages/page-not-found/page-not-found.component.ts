import { Component, OnDestroy, OnInit } from '@angular/core';
import { GoogleAnalyticsService } from 'src/app/features/analytics/google-analytics.service';
import { environment } from '../../../environments/environment';
import { DeviceService } from '../../shared/services/device.service';
import { NavigationHistoryService } from '../../shared/services/navigation-history.service';

@Component({
    selector: 'app-page-not-found',
    templateUrl: './page-not-found.component.html',
    styleUrls: ['./page-not-found.component.scss'],
})
export class PageNotFoundComponent implements OnInit, OnDestroy {
    contactAddress = environment.contactAddress;

    constructor(
        public readonly deviceService: DeviceService,
        private readonly navigationHistoryService: NavigationHistoryService,
        private readonly googleAnalyticsService: GoogleAnalyticsService,
    ) {}

    ngOnInit() {
        document.body.style.overflow = 'hidden';

        this.googleAnalyticsService.track404(document.location.href);
    }

    ngOnDestroy() {
        document.body.style.overflow = 'auto';
    }

    goBack() {
        this.navigationHistoryService.back();
    }
}
