import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { HeaderModule } from 'src/app/features/layout/headers/header.module';
import { AppCoreModule } from 'src/app/shared/components/app-core/app-core.module';
import { ProductListModule } from 'src/app/shared/components/product-list/product-list.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { IconsModule } from '../../features/icons/icons.module';
import { WishlistRoutingModule } from './wishlist-routing.module';
import { WishlistComponent } from './wishlist.component';

@NgModule({
    declarations: [WishlistComponent],
    imports: [
        CommonModule,
        WishlistRoutingModule,
        TranslateModule,
        IconsModule,
        HeaderModule,
        ProductListModule,
        AppCoreModule,
        SharedModule,
    ],
})
export class WishlistModule {}
