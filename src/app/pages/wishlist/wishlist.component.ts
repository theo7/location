import { Component, ElementRef, OnInit, Renderer2, ViewChild } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { BehaviorSubject, combineLatest, Observable } from 'rxjs';
import { throttleTime } from 'rxjs/operators';
import { DeviceService } from 'src/app/shared/services/device.service';
import { WishlistDispatchers } from 'src/app/store/services/wishlist-dispatchers.service';
import { WishlistSelectors } from 'src/app/store/services/wishlist-selectors.service';
import { Product } from '../../shared/models/models';

@UntilDestroy()
@Component({
    selector: 'app-wishlist',
    templateUrl: './wishlist.component.html',
    styleUrls: ['./wishlist.component.scss'],
})
export class WishlistComponent implements OnInit {
    @ViewChild('contentScrollEl') contentScroll?: ElementRef<HTMLDivElement>;

    isLoading$: Observable<boolean>;
    wishlist$: Observable<Product[]>;
    totalProducts$: Observable<number>;

    contentScrollHeight$ = new BehaviorSubject<number | undefined>(undefined);

    window = window;

    constructor(
        public readonly deviceService: DeviceService,
        private readonly wishlistSelectors: WishlistSelectors,
        private readonly wishlistDispatchers: WishlistDispatchers,
        private readonly renderer: Renderer2,
    ) {
        this.isLoading$ = this.wishlistSelectors.isLoading$.pipe(untilDestroyed(this));
        this.wishlist$ = this.wishlistSelectors.wishlist$.pipe(untilDestroyed(this));
        this.totalProducts$ = this.wishlistSelectors.totalProducts$.pipe(untilDestroyed(this));

        // 💡 hack to fix the real height of the component (to correctly handle sticky sidebar)
        combineLatest([this.deviceService.isHandset$, this.contentScrollHeight$])
            .pipe(throttleTime(50), untilDestroyed(this))
            .subscribe(([isHandset, contentScrollHeight]) => {
                if (this.contentScroll && contentScrollHeight !== undefined) {
                    const profileScrollNativeElement = this.contentScroll.nativeElement;

                    if (isHandset) {
                        this.renderer.setStyle(profileScrollNativeElement, 'height', '100%');
                    } else {
                        this.renderer.setStyle(profileScrollNativeElement, 'height', `${contentScrollHeight}px`);
                    }
                }
            });
    }

    ngOnInit(): void {
        this.wishlistDispatchers.load();
    }

    onFetchMore() {
        this.wishlistDispatchers.loadMore();
    }

    onWindowScroll() {
        if (this.contentScroll) {
            const contentScrollNativeElement = this.contentScroll.nativeElement;

            this.contentScrollHeight$.next(contentScrollNativeElement.scrollHeight);
        }
    }
}
