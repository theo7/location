import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
    selector: 'app-profil-desktop',
    templateUrl: './profil-desktop.component.html',
    styleUrls: ['./profil-desktop.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProfilDesktopComponent {}
