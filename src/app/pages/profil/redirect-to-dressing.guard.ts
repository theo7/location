import { Injectable, NgZone } from '@angular/core';
import { CanActivate, Router, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { first, map } from 'rxjs/operators';
import { RouterSelectors } from 'src/app/store/services/router-selectors.service';

@Injectable({
    providedIn: 'root',
})
export class RedirectToDressingGuard implements CanActivate {
    constructor(
        private readonly router: Router,
        private readonly routerSelectors: RouterSelectors,
        private readonly zone: NgZone,
    ) {}

    canActivate(): Observable<boolean | UrlTree> {
        return this.routerSelectors.userId$.pipe(
            first(),
            map(userId => {
                return this.zone.run(() => {
                    return this.router.createUrlTree(['profile', userId]);
                });
            }),
        );
    }
}
