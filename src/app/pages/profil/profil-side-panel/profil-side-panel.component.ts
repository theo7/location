import { AfterViewInit, ChangeDetectionStrategy, Component, ElementRef, Renderer2 } from '@angular/core';
import { FormControl } from '@angular/forms';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { combineLatest, fromEvent, Observable, of } from 'rxjs';
import { debounceTime, filter, first, map, startWith, switchMap, withLatestFrom } from 'rxjs/operators';
import { filterDefined } from 'src/app/shared/functions/operators.functions';
import { UserProfile, UserProfileStatus, UserType } from 'src/app/shared/models/models';
import { DeviceService } from 'src/app/shared/services/device.service';
import { ShareService } from 'src/app/shared/services/share.service';
import { UserService } from 'src/app/shared/services/user.service';
import { FollowDispatchers } from 'src/app/store/services/follow-dispatchers.service';
import { FollowSelectors } from 'src/app/store/services/follow-selectors.service';
import { ProductsDispatchers } from 'src/app/store/services/products-dispatchers.service';
import { ProfileSelectors } from 'src/app/store/services/profile-selectors.service';
import { RouterSelectors } from 'src/app/store/services/router-selectors.service';
import { UserProfileDispatchers } from 'src/app/store/services/user-dispatchers.service';
import { UserProfileSelectors } from 'src/app/store/services/user-selectors.service';
import { environment } from 'src/environments/environment';

@UntilDestroy()
@Component({
    selector: 'app-profil-side-panel',
    templateUrl: './profil-side-panel.component.html',
    styleUrls: ['./profil-side-panel.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProfilSidePanelComponent implements AfterViewInit {
    user$: Observable<UserProfile | undefined>;
    isCurrentUser$: Observable<boolean>;
    avatarURL$: Observable<string | undefined>;
    isProfileModerated$: Observable<boolean>;
    isDressingModerated$: Observable<boolean | undefined>;
    isFollowing$?: Observable<boolean>;
    followingLoading$?: Observable<boolean>;
    isLogged$?: Observable<boolean>;
    canFollow$: Observable<boolean>;

    UserProfileStatus = UserProfileStatus;
    UserType = UserType;

    canShareDressing = this.shareService.canShare();
    appVersion = environment.appVersion;
    vacationMode: FormControl;

    constructor(
        public readonly userProfileSelectors: UserProfileSelectors,
        routerSelectors: RouterSelectors,
        private readonly productsDispatchers: ProductsDispatchers,
        private readonly userDispatchers: UserProfileDispatchers,
        private readonly shareService: ShareService,
        public readonly deviceService: DeviceService,
        userService: UserService,
        profileSelectors: ProfileSelectors,
        private readonly followSelectors: FollowSelectors,
        private readonly followDispatchers: FollowDispatchers,
        private readonly elementRef: ElementRef<HTMLElement>,
        private readonly renderer: Renderer2,
    ) {
        this.vacationMode = new FormControl();

        const currentUser$ = userProfileSelectors.userProfile$;
        const isLogged$ = userProfileSelectors.isLogged$;

        routerSelectors.userId$
            .pipe(
                withLatestFrom(currentUser$, isLogged$),
                filter(([userId, currentUser, isLogged]) => {
                    if (!isLogged) {
                        return false;
                    }
                    if (!userId || currentUser?.id === userId) {
                        return true;
                    }

                    return false;
                }),
                untilDestroyed(this),
            )
            .subscribe(_ => {
                this.userDispatchers.getUserProfile();
            });

        this.user$ = combineLatest([routerSelectors.userId$, currentUser$]).pipe(
            switchMap(([userId, currentUser]) => {
                if (!userId || currentUser?.id === userId) {
                    return currentUser$;
                }

                return profileSelectors.current$;
            }),
            untilDestroyed(this),
        );

        this.avatarURL$ = this.user$.pipe(
            map(user => {
                return user && userService.getAvatarUrl(user, true);
            }),
            untilDestroyed(this),
        );

        this.isCurrentUser$ = this.user$.pipe(
            switchMap(user => (user?.id ? userProfileSelectors.isCurrentUser$(user.id) : of(false))),
            untilDestroyed(this),
        );

        this.isProfileModerated$ = this.isCurrentUser$.pipe(
            switchMap(isCurrentUser => {
                if (isCurrentUser) {
                    return userProfileSelectors.isProfileModerated$;
                }

                return of(false);
            }),
            untilDestroyed(this),
        );

        this.isDressingModerated$ = this.isCurrentUser$.pipe(
            switchMap(isCurrentUser => {
                if (isCurrentUser) {
                    return userProfileSelectors.isDressingModerated$;
                }

                return of(false);
            }),
            untilDestroyed(this),
        );

        this.isFollowing$ = profileSelectors.isFollowing$.pipe(untilDestroyed(this));

        this.followingLoading$ = this.followSelectors.isFollowingLoading$;
        this.isLogged$ = this.userProfileSelectors.isLogged$;

        this.canFollow$ = combineLatest([this.isCurrentUser$, this.isLogged$]).pipe(
            map(([isCurrentUser, isLogged]) => isCurrentUser === false && !!isLogged),
            untilDestroyed(this),
        );

        this.initVacationMode();
    }

    ngAfterViewInit(): void {
        // 💡 handle dynamic sticky header sidebar panel (if panel height < viewport height => panel always displayed at the bottom...)
        const selfElement = this.elementRef.nativeElement;

        const headerDesktopElements = document.getElementsByTagName('app-header-desktop');
        const headerDesktopElement = headerDesktopElements.item(0);

        fromEvent(window, 'resize')
            .pipe(startWith('start'), debounceTime(100), untilDestroyed(this))
            .subscribe(() => {
                const selfHeight = selfElement.clientHeight;
                const minHeight = window.innerHeight - (headerDesktopElement?.clientHeight ?? 0);

                const enableDynamicStickySidebar = selfHeight > minHeight;

                if (enableDynamicStickySidebar) {
                    // set dynamic sticky header
                    this.renderer.setStyle(selfElement, 'top', '-200vh');
                    this.renderer.setStyle(selfElement, 'bottom', '0');
                    this.renderer.setStyle(selfElement, 'align-self', 'flex-end');
                } else {
                    // set fixed header
                    this.renderer.setStyle(selfElement, 'top', '0');
                    this.renderer.setStyle(selfElement, 'align-self', 'flex-start');
                }
            });
    }

    private initVacationMode() {
        this.userProfileSelectors.isInVacationMode$.pipe(untilDestroyed(this)).subscribe(isInVacationMode => {
            this.vacationMode.setValue(isInVacationMode, { emitEvent: false });
        });

        this.vacationMode.valueChanges
            .pipe(untilDestroyed(this))
            .subscribe(() => this.userDispatchers.switchVacationMode());
    }

    onShareButtonClicked() {
        this.user$.pipe(first()).subscribe(user => {
            if (user) {
                this.productsDispatchers.shareDressing(user);
            }
        });
    }

    logout() {
        this.userDispatchers.logout();
    }

    onSubscriptionButtonClicked() {
        this.user$
            .pipe(filterDefined(), untilDestroyed(this))
            .subscribe(user => {
                this.followDispatchers.follow(user!.id!);
            })
            .unsubscribe();
    }

    onUnsubscriptionButtonClicked() {
        this.user$
            .pipe(filterDefined(), untilDestroyed(this))
            .subscribe(user => {
                this.followDispatchers.unfollow(user!.id!);
            })
            .unsubscribe();
    }
}
