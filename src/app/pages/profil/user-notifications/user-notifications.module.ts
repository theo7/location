import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDividerModule } from '@angular/material/divider';
import { MatListModule } from '@angular/material/list';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { TranslateModule } from '@ngx-translate/core';
import { HeaderModule } from 'src/app/features/layout/headers/header.module';
import { AppCoreModule } from 'src/app/shared/components/app-core/app-core.module';
import { UserNotificationsRoutingModule } from './user-notifications-routing.module';
import { UserNotificationsComponent } from './user-notifications.component';

@NgModule({
    declarations: [UserNotificationsComponent],
    imports: [
        CommonModule,
        UserNotificationsRoutingModule,
        TranslateModule,
        HeaderModule,
        FormsModule,
        ReactiveFormsModule,
        MatListModule,
        MatSlideToggleModule,
        MatDividerModule,
        AppCoreModule,
    ],
})
export class UserNotificationsModule {}
