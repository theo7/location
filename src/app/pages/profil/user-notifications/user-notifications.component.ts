import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Actions, ofType } from '@ngrx/effects';
import { isEqual } from 'lodash-es';
import { Observable } from 'rxjs';
import { distinctUntilChanged, filter, map } from 'rxjs/operators';
import { DeviceService } from 'src/app/shared/services/device.service';
import { UserSettings } from 'src/app/store/actions/settings.actions';
import { NotificationsDispatchers } from 'src/app/store/services/notifications-dispatchers.service';
import { SettingsDispatchers } from 'src/app/store/services/settings-dispatchers.service';
import { SettingsSelectors } from 'src/app/store/services/settings-selectors.service';
import { PushService } from '../../../shared/services/push.service';
import * as NotificationsActions from '../../../store/actions/notifications.actions';

@UntilDestroy()
@Component({
    selector: 'app-user-notifications',
    templateUrl: './user-notifications.component.html',
    styleUrls: ['./user-notifications.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserNotificationsComponent implements OnInit {
    form: FormGroup;

    pushSupported = this.pushService.isSupported();

    constructor(
        fb: FormBuilder,
        notificationsDispatchers: NotificationsDispatchers,
        actions$: Actions,
        settingsSelectors: SettingsSelectors,
        private readonly settingsDispatchers: SettingsDispatchers,
        private readonly pushService: PushService,
        public readonly deviceService: DeviceService,
    ) {
        this.form = fb.group({
            acceptCommercialContact: [false],
            acceptMailNotification: [false],
            acceptPushNotification: [false],
        });

        const pushControl = this.form.controls.acceptPushNotification;

        actions$.pipe(ofType(NotificationsActions.requestPermissionFailed), untilDestroyed(this)).subscribe(() => {
            // disable push consent
            pushControl.setValue(false, { emitEvent: false });
        });

        settingsSelectors.settings$.pipe(untilDestroyed(this)).subscribe(settings => {
            if (settings) {
                this.form.patchValue(settings, { emitEvent: false });
            }
        });

        const pushEnabled$: Observable<boolean> = pushControl.valueChanges;

        pushEnabled$
            .pipe(
                filter(pushEnabled => pushEnabled),
                untilDestroyed(this),
            )
            .subscribe(() => {
                // try register for push notif
                notificationsDispatchers.requestPermission();
            });

        pushEnabled$
            .pipe(
                filter(pushEnabled => !pushEnabled),
                untilDestroyed(this),
            )
            .subscribe(() => {
                // unregister for push notif
                notificationsDispatchers.revokePermission();
            });

        const valueChanges$: Observable<UserSettings> = this.form.valueChanges;

        valueChanges$
            .pipe(
                map(value => {
                    const { acceptPushNotification, ...settings } = value;
                    return settings;
                }),
                distinctUntilChanged((a, b) => isEqual(a, b)),
            )
            .subscribe(settings => {
                this.settingsDispatchers.setGlobalSettings(settings);
            });
    }

    ngOnInit(): void {
        this.settingsDispatchers.getSettings();
    }
}
