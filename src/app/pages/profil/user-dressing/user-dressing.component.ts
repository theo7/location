import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { ShareService } from 'src/app/shared/services/share.service';
import { UserProfileSelectors } from 'src/app/store/services/user-selectors.service';
import { environment } from '../../../../environments/environment';
import { Product } from '../../../shared/models/models';
import { ProductsDispatchers } from '../../../store/services/products-dispatchers.service';
import { ProductsSelectors } from '../../../store/services/products-selectors.service';

@Component({
    selector: 'app-user-dressing',
    templateUrl: './user-dressing.component.html',
    styleUrls: ['./user-dressing.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class UserDressingComponent implements OnInit {
    canShareDressing = this.shareService.canShare();
    showBanner = environment.showBanner;

    constructor(
        private readonly router: Router,
        private readonly productsDispatchers: ProductsDispatchers,
        private readonly userProfileSelectors: UserProfileSelectors,
        public readonly productsSelectors: ProductsSelectors,
        private readonly shareService: ShareService,
    ) {}

    ngOnInit() {
        this.productsDispatchers.getOwnDressing();
    }

    navigateToProductPage(product: Product) {
        this.router.navigate(['product', product.id]);
    }

    navigateToAddProduct() {
        this.router.navigate(['/add']);
    }

    onScroll() {
        this.productsDispatchers.getOwnNextDressing();
    }
}
