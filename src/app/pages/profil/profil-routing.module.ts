import { NgModule } from '@angular/core';
import { AngularFireAuthGuard, redirectUnauthorizedTo } from '@angular/fire/auth-guard';
import { RouterModule, Routes } from '@angular/router';
import { NotBlockedGuard } from 'src/app/shared/guards/not-blocked.guard';
import { SignedInGuard } from 'src/app/shared/guards/signed-in.guard';
import { ProfilChildComponent } from './profil-child/profil-child.component';
import { ProfilComponent } from './profil.component';
import { RedirectToDressingGuard } from './redirect-to-dressing.guard';
import { RedirectToMyDressingGuard } from './redirect-to-my-dressing.guard';

const redirectUnauthorizedToLogin = () => redirectUnauthorizedTo(['login']);

const routes: Routes = [
    {
        path: 'dressing',
        canActivate: [RedirectToMyDressingGuard],
    },
    {
        path: ':userId/dressing',
        canActivate: [RedirectToDressingGuard],
    },
    {
        path: '',
        component: ProfilComponent,
        children: [
            {
                path: '',
                component: ProfilChildComponent,
                canActivate: [SignedInGuard],
            },
            {
                path: 'edit',
                loadChildren: () => import('../../pages/edit-profil/edit-profil.module').then(m => m.EditProfilModule),
                canActivate: [SignedInGuard],
            },
            {
                path: 'credits',
                loadChildren: () => import('../../pages/credits/credits.module').then(m => m.CreditsModule),
                canActivate: [SignedInGuard, AngularFireAuthGuard],
                data: { authGuardPipe: redirectUnauthorizedToLogin },
            },
            {
                path: 'wishlist',
                loadChildren: () => import('../../pages/wishlist/wishlist.module').then(m => m.WishlistModule),
                canActivate: [SignedInGuard],
            },
            {
                path: 'orders',
                loadChildren: () => import('../../pages/orders/orders.module').then(m => m.OrdersModule),
                canActivate: [SignedInGuard],
            },
            {
                path: 'addresses',
                loadChildren: () =>
                    import('../../pages/user-addresses/user-addresses.module').then(m => m.UserAddressesModule),
                canActivate: [SignedInGuard, NotBlockedGuard],
            },
            {
                path: 'bank-details',
                loadChildren: () =>
                    import('../../pages/user-bank-details/user-bank-details.module').then(m => m.UserBankDetailsModule),
                canActivate: [SignedInGuard],
            },
            {
                path: 'change-password',
                loadChildren: () =>
                    import('../../pages/change-password/change-password.module').then(m => m.ChangePasswordModule),
                canActivate: [SignedInGuard],
            },
            {
                path: 'notifications',
                loadChildren: () =>
                    import('./user-notifications/user-notifications.module').then(m => m.UserNotificationsModule),
                canActivate: [SignedInGuard],
            },
            {
                path: 'infos',
                loadChildren: () =>
                    import('../../pages/profile-infos/profile-infos.module').then(m => m.ProfileInfosModule),
                canActivate: [SignedInGuard],
            },
            {
                path: ':userId/following',
                loadChildren: () => import('../user-following/user-following.module').then(m => m.UserFollowingModule),
            },
            {
                path: ':userId/followers',
                loadChildren: () => import('../user-followers/user-followers.module').then(m => m.UserFollowersModule),
            },
            {
                path: ':userId',
                loadChildren: () => import('./user-profile/user-profile.module').then(m => m.UserProfileModule),
            },
        ],
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class ProfilRoutingModule {}
