import { Component, ElementRef, Renderer2, ViewChild } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { BehaviorSubject, combineLatest, Observable } from 'rxjs';
import { map, skip, switchMap, throttleTime } from 'rxjs/operators';
import { filterDefined } from 'src/app/shared/functions/operators.functions';
import { RouterSelectors } from 'src/app/store/services/router-selectors.service';
import { environment } from '../../../../environments/environment';
import { UserProfile } from '../../../shared/models/models';
import { DeviceService } from '../../../shared/services/device.service';
import { ProductsSelectors } from '../../../store/services/products-selectors.service';
import { ProfileSelectors } from '../../../store/services/profile-selectors.service';
import { UserProfileSelectors } from '../../../store/services/user-selectors.service';

@UntilDestroy()
@Component({
    selector: 'app-user-profile',
    templateUrl: './user-profile.component.html',
    styleUrls: ['./user-profile.component.scss'],
})
export class UserProfileComponent {
    @ViewChild('profileScroll') profileScroll?: ElementRef<HTMLDivElement>;

    showBanner = environment.showBanner;

    isBlocked$: Observable<boolean>;
    loading$: Observable<boolean>;
    user$: Observable<UserProfile | undefined>;

    profileScrollHeight$ = new BehaviorSubject<number | undefined>(undefined);

    constructor(
        public readonly deviceService: DeviceService,
        userProfileSelectors: UserProfileSelectors,
        routerSelectors: RouterSelectors,
        profileSelectors: ProfileSelectors,
        productSelectors: ProductsSelectors,
        private readonly renderer: Renderer2,
    ) {
        this.loading$ = combineLatest([
            profileSelectors.loading$,
            productSelectors.dressingLoading$.pipe(skip(1)),
        ]).pipe(
            map(([profileLoading, dressingLoading]) => profileLoading || dressingLoading),
            untilDestroyed(this),
        );

        this.user$ = profileSelectors.current$.pipe(untilDestroyed(this));

        this.isBlocked$ = routerSelectors.userId$.pipe(
            filterDefined(),
            switchMap(userId => userProfileSelectors.isBlocked$(userId)),
            untilDestroyed(this),
        );

        // 💡 hack to fix the real height of the component (to correctly handle sticky sidebar)
        combineLatest([this.deviceService.isHandset$, this.profileScrollHeight$])
            .pipe(throttleTime(50), untilDestroyed(this))
            .subscribe(([isHandset, profileScrollHeight]) => {
                if (this.profileScroll && profileScrollHeight !== undefined) {
                    const profileScrollNativeElement = this.profileScroll.nativeElement;

                    if (isHandset) {
                        this.renderer.setStyle(profileScrollNativeElement, 'height', '100%');
                    } else {
                        this.renderer.setStyle(profileScrollNativeElement, 'height', `${profileScrollHeight}px`);
                    }
                }
            });
    }

    onWindowScroll() {
        if (this.profileScroll) {
            const profileScrollNativeElement = this.profileScroll.nativeElement;

            this.profileScrollHeight$.next(profileScrollNativeElement.scrollHeight);
        }
    }
}
