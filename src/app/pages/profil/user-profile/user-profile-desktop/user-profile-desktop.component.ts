import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { EMPTY, Observable } from 'rxjs';
import { first, map, switchMap, withLatestFrom } from 'rxjs/operators';
import { DiscussionsSelectors } from 'src/app/store/services/discussions-selectors.service';
import { UserProfileSelectors } from 'src/app/store/services/user-selectors.service';
import { Discussion, FilterType, Lot, Product, UserProfile } from '../../../../shared/models/models';
import { FilterSelectors } from '../../../../store/services/filter-selectors.service';
import { ProductsDispatchers } from '../../../../store/services/products-dispatchers.service';
import { ProductsSelectors } from '../../../../store/services/products-selectors.service';

@UntilDestroy()
@Component({
    selector: 'app-user-profile-desktop',
    templateUrl: './user-profile-desktop.component.html',
    styleUrls: ['./user-profile-desktop.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class UserProfileDesktopComponent implements OnInit {
    @Input() user!: UserProfile;
    @Input() scrollEl!: HTMLElement;

    isCurrentUser$?: Observable<boolean>;
    canCreateLot$?: Observable<boolean>;
    hasCartForSellerId$?: Observable<boolean>;
    lotOpenBySellerId$?: Observable<Lot | undefined>;
    discussionByLotId$?: Observable<Discussion | undefined>;
    products$?: Observable<Product[]>;
    totalProductsCount$?: Observable<number>;
    showDressingFilter$?: Observable<boolean>;

    window = window;

    readonly filterKey: FilterType = 'dressing';

    constructor(
        private readonly router: Router,
        private readonly userProfileSelectors: UserProfileSelectors,
        private readonly discussionsSelectors: DiscussionsSelectors,
        private readonly productsDispatchers: ProductsDispatchers,
        private readonly filterSelectors: FilterSelectors,
        readonly productsSelectors: ProductsSelectors,
    ) {}

    ngOnInit(): void {
        this.showDressingFilter$ = this.productsSelectors.showDressingFilter$.pipe(
            withLatestFrom(this.userProfileSelectors.isCurrentUser$(this.user.id!)),
            map(([showDressingFilter, isCurrentUser]) => showDressingFilter && !isCurrentUser),
            untilDestroyed(this),
        );

        this.isCurrentUser$ = this.userProfileSelectors.isCurrentUser$(this.user.id!).pipe(untilDestroyed(this));

        this.canCreateLot$ = this.userProfileSelectors.canCreateLot$(this.user.id!).pipe(untilDestroyed(this));

        this.hasCartForSellerId$ = this.discussionsSelectors
            .hasCartForSellerId$(this.user.id!)
            .pipe(untilDestroyed(this));

        this.lotOpenBySellerId$ = this.discussionsSelectors
            .lotOpenBySellerId$(this.user.id!)
            .pipe(untilDestroyed(this));

        this.discussionByLotId$ = this.lotOpenBySellerId$.pipe(
            switchMap(lot => {
                if (lot?.id) {
                    return this.discussionsSelectors.discussionByLotId$(lot.id);
                }
                return EMPTY;
            }),
            untilDestroyed(this),
        );

        this.products$ = this.productsSelectors.productsInDressing$.pipe(untilDestroyed(this));
        this.totalProductsCount$ = this.productsSelectors.dressingCount$.pipe(untilDestroyed(this));
    }

    navigateToProduct(product: Product): void {
        this.filterSelectors
            .currentRouterParams$(this.filterKey)
            .pipe(first())
            .subscribe(queryParams => {
                this.router.navigate(['product', product.id], { queryParams });
            });
    }

    onScroll(): void {
        this.userProfileSelectors.userProfile$.pipe(first()).subscribe(currentUser => {
            if (currentUser?.id === this.user.id) {
                this.productsDispatchers.getOwnNextDressing();
                return;
            }

            if (this.user.id) {
                this.productsDispatchers.getNextDressing(this.user.id);
                return;
            }
        });
    }
}
