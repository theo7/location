import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import * as dayjs from 'dayjs';
import { combineLatest, Observable, of, timer } from 'rxjs';
import { distinctUntilChanged, map, switchMap, take } from 'rxjs/operators';
import { RateUserDialogComponent } from 'src/app/pages/chat/chat/rate-user-dialog/rate-user-dialog.component';
import { LotService } from 'src/app/shared/services/lot.service';
import { ProfileSelectors } from 'src/app/store/services/profile-selectors.service';
import { RatingsDispatchers } from 'src/app/store/services/ratings-dispatchers.service';
import { UserProfileSelectors } from 'src/app/store/services/user-selectors.service';
import { environment } from '../../../../../../environments/environment';
import { ImageFullscreenDialogComponent } from '../../../../../shared/components/product-carousel/image-fullscreen-dialog/image-fullscreen-dialog.component';
import { Lot, Picture, UserProfile, UserRating } from '../../../../../shared/models/models';
import { DeviceService } from '../../../../../shared/services/device.service';

const SECOND = 1000;

@UntilDestroy()
@Component({
    selector: 'app-user-rating-card',
    templateUrl: './user-rating-card.component.html',
    styleUrls: ['./user-rating-card.component.scss'],
})
export class UserRatingCardComponent implements OnInit, OnChanges {
    @Input() rating!: UserRating;

    user!: UserProfile & { avatarUrl: string };
    pictures: (Picture & { pictureUrl?: string })[] = [];
    showFullComment = false;
    postDate$?: Observable<string>;
    canEdit$?: Observable<boolean>;
    ratedUser$?: Observable<UserProfile | undefined>;
    enableAnswer = false;
    canAnswer$?: Observable<boolean>;
    answerControl = new FormControl('', Validators.required);

    readonly KIABI_ID = environment.professional.kiabi.id;

    constructor(
        private readonly dialog: MatDialog,
        private readonly deviceService: DeviceService,
        private readonly userProfileSelectors: UserProfileSelectors,
        private readonly profileSelectors: ProfileSelectors,
        private readonly lotService: LotService,
        private readonly route: ActivatedRoute,
        private readonly ratingsDispatchers: RatingsDispatchers,
    ) {}

    ngOnInit() {
        this.handleUpdate();

        this.postDate$ = timer(0, SECOND).pipe(
            map(() => dayjs(this.rating?.date).fromNow()),
            distinctUntilChanged(),
            untilDestroyed(this),
        );

        this.canEdit$ = this.userProfileSelectors.userProfile$.pipe(
            map(userProfile => !!userProfile?.id && userProfile?.id === this.rating?.evaluatedBy.id),
            untilDestroyed(this),
        );

        this.ratedUser$ = this.profileSelectors.current$.pipe(untilDestroyed(this));
    }

    ngOnChanges(changes: SimpleChanges) {
        this.handleUpdate();
    }

    private handleUpdate() {
        this.user = this.rating && {
            ...this.rating.evaluatedBy,
            avatarUrl: this.rating.evaluatedBy.picture
                ? `${environment.s3url}${this.rating.evaluatedBy.picture.fileName}`
                : 'assets/images/defaut-avatar.svg',
        };

        this.pictures = (this.rating?.pictures || []).map(p => ({
            ...p,
            pictureUrl: `${environment.s3url}${p.fileName}`,
        }));

        this.canAnswer$ = combineLatest([this.userProfileSelectors.userProfile$, of(!!this.rating.answerRate)]).pipe(
            map(([user, hasAlreadyCommented]) => {
                return user?.id === this.rating.evaluated.id && !hasAlreadyCommented;
            }),
            untilDestroyed(this),
        );
    }

    displayFullComment(): void {
        this.showFullComment = true;
    }

    openPictureInFullScreen(picture: Picture): void {
        const dialogParams = device =>
            ({
                handset: {
                    height: '100vh',
                    width: '100vw',
                    maxWidth: '100vw',
                },
                desktop: {
                    width: '600px',
                    maxWidth: '600px',
                    height: '100vh',
                },
            }[device]);

        this.dialog.open(ImageFullscreenDialogComponent, {
            panelClass: 'transparent-dialog-container',
            backdropClass: 'darker-backdrop',
            data: {
                pictures: this.pictures,
                index: this.pictures.indexOf(picture),
            },
            ...dialogParams(this.deviceService.mode),
        });
    }

    onEditButtonClicked() {
        if (this.ratedUser$ && this.rating?.id) {
            this.ratedUser$
                ?.pipe(
                    switchMap(ratedUser => {
                        return this.lotService
                            .getLotFromRatingId(this.rating!.id!)
                            .pipe(map(lot => [ratedUser, lot] as [UserProfile | undefined, Lot]));
                    }),
                    take(1),
                )
                .subscribe(([ratedUser, lot]) => {
                    const dialogParams = device =>
                        ({
                            handset: {
                                height: '100vh',
                                width: '100vw',
                                maxWidth: '100vw',
                                panelClass: 'full-screen-dialog',
                            },
                            desktop: {
                                width: '90vw',
                                maxWidth: '700px',
                                height: '80vh',
                                panelClass: 'no-padding-dialog',
                            },
                        }[device]);

                    this.dialog.open(RateUserDialogComponent, {
                        data: {
                            edit: true,
                            user: ratedUser,
                            lot: lot,
                            rating: this.rating,
                        },
                        ...dialogParams(this.deviceService.mode),
                    });
                });
        }
    }

    answerRating() {
        this.enableAnswer = true;
    }

    sendReview() {
        this.enableAnswer = false;
        this.ratingsDispatchers.answerRating(this.user.id!, this.rating.id!, this.answerControl.value);
    }
}
