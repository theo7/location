import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatTooltipModule } from '@angular/material/tooltip';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { HeaderModule } from 'src/app/features/layout/headers/header.module';
import { AppCoreModule } from 'src/app/shared/components/app-core/app-core.module';
import { AvatarModule } from 'src/app/shared/components/avatar/avatar.module';
import { UserInformationModule } from 'src/app/shared/components/user-information/user-information.module';
import { RatingModule } from 'src/app/shared/components/user-rating/rating.module';
import { PipesModule } from 'src/app/shared/pipes/pipes.module';
import { ButtonModule } from '../../../../shared/components/button/button.module';
import { DirectivesModule } from '../../../../shared/directives/directives.module';
import { UserRatingCardComponent } from './user-rating-card/user-rating-card.component';
import { UserRatingProgressBarComponent } from './user-rating-progress-bar/user-rating-progress-bar.component';
import { UserRatingRoutingModule } from './user-rating-routing.module';
import { UserRatingComponent } from './user-rating.component';

@NgModule({
    declarations: [UserRatingComponent, UserRatingCardComponent, UserRatingProgressBarComponent],
    imports: [
        CommonModule,
        AppCoreModule,
        UserRatingRoutingModule,
        TranslateModule,
        RouterModule,
        PipesModule,
        HeaderModule,
        AvatarModule,
        RatingModule,
        UserInformationModule,
        InfiniteScrollModule,
        MatButtonModule,
        MatIconModule,
        MatTooltipModule,
        ButtonModule,
        MatFormFieldModule,
        DirectivesModule,
        ReactiveFormsModule,
    ],
})
export class UserRatingModule {}
