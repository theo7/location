import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserRatingComponent } from './user-rating.component';

const routes: Routes = [
    {
        path: '',
        component: UserRatingComponent,
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class UserRatingRoutingModule {}
