import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'app-user-rating-progress-bar',
    templateUrl: './user-rating-progress-bar.component.html',
    styleUrls: ['./user-rating-progress-bar.component.scss'],
})
export class UserRatingProgressBarComponent implements OnInit {
    @Input() total = 0;
    @Input() starNumber = 0;
    @Input() number = 0;

    progression = 0;

    ngOnInit() {
        this.progression = (this.number / this.total) * 100;
    }
}
