import { Component, OnInit } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ProfileSelectors } from 'src/app/store/services/profile-selectors.service';
import { RatingsDispatchers } from 'src/app/store/services/ratings-dispatchers.service';
import { RatingsSelectors } from 'src/app/store/services/ratings-selectors.service';
import { RouterSelectors } from 'src/app/store/services/router-selectors.service';
import { environment } from '../../../../../environments/environment';
import { UserProfile, UserRating, UserRatingSummary } from '../../../../shared/models/models';
import { DeviceService } from '../../../../shared/services/device.service';

@UntilDestroy()
@Component({
    selector: 'app-user-rating',
    templateUrl: './user-rating.component.html',
    styleUrls: ['./user-rating.component.scss'],
})
export class UserRatingComponent implements OnInit {
    ratingSummary$?: Observable<UserRatingSummary | undefined>;
    ratings$?: Observable<UserRating[]>;
    totalRatings$?: Observable<number>;
    user$?: Observable<UserProfile | undefined>;

    trackByRatingId = (index: number, item: UserRating): number => item.id || index;

    constructor(
        public readonly deviceService: DeviceService,
        private readonly profileSelectors: ProfileSelectors,
        private readonly ratingsDispatchers: RatingsDispatchers,
        private readonly ratingsSelectors: RatingsSelectors,
        private readonly routerSelectors: RouterSelectors,
    ) {}

    ngOnInit() {
        this.ratingsDispatchers.load();

        this.routerSelectors.userId$.pipe(untilDestroyed(this)).subscribe(() => {
            this.ratingsDispatchers.load();
        });

        this.ratingSummary$ = this.ratingsSelectors.summary$.pipe(untilDestroyed(this));
        this.ratings$ = this.ratingsSelectors.ratings$.pipe(untilDestroyed(this));
        this.totalRatings$ = this.ratingsSelectors.totalRatings$.pipe(untilDestroyed(this));

        this.user$ = this.profileSelectors.current$.pipe(
            map(user => {
                if (user) {
                    return {
                        ...user,
                        avatarUrl: user.picture
                            ? `${environment.s3url}${user.picture.fileName}`
                            : 'assets/images/defaut-avatar.svg',
                    };
                }

                return undefined;
            }),
            untilDestroyed(this),
        );
    }

    findNextRating() {
        this.ratingsDispatchers.loadMore();
    }
}
