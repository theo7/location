import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { IconsModule } from 'src/app/features/icons/icons.module';
import { HeaderModule } from 'src/app/features/layout/headers/header.module';
import { AppCoreModule } from 'src/app/shared/components/app-core/app-core.module';
import { UserDressingHandsetModule } from 'src/app/shared/components/app-user-dressing-handset/user-dressing-handset.module';
import { ButtonModule } from 'src/app/shared/components/button/button.module';
import { C2cRegulationModule } from 'src/app/shared/components/c2c-regulation/c2c-regulation.module';
import { EmptyDressingCardModule } from 'src/app/shared/components/empty-dressing-card/empty-dressing-card.module';
import { ImagesMiniModule } from 'src/app/shared/components/images-mini/images-mini.module';
import { ProductCardModule } from 'src/app/shared/components/product-card/product-card.module';
import { ProductListModule } from 'src/app/shared/components/product-list/product-list.module';
import { UserBlockedWarningModule } from 'src/app/shared/components/user-blocked-warning/user-blocked-warning.module';
import { RatingModule } from 'src/app/shared/components/user-rating/rating.module';
import { DirectivesModule } from 'src/app/shared/directives/directives.module';
import { PipesModule } from 'src/app/shared/pipes/pipes.module';
import { ChipModule } from '../../../shared/components/chip/chip.module';
import { ProductFilterModule } from '../../../shared/components/product-filter/product-filter.module';
import { UserInformationModule } from '../../../shared/components/user-information/user-information.module';
import { SharedModule } from '../../../shared/shared.module';
import { UserCardComponent } from './user-card/user-card.component';
import { UserProfileDesktopComponent } from './user-profile-desktop/user-profile-desktop.component';
import { UserProfileHandsetComponent } from './user-profile-handset/user-profile-handset.component';
import { UserProfileRoutingModule } from './user-profile-routing.module';
import { UserProfileComponent } from './user-profile.component';

@NgModule({
    declarations: [UserProfileComponent, UserProfileHandsetComponent, UserProfileDesktopComponent, UserCardComponent],
    imports: [
        CommonModule,
        TranslateModule,
        RouterModule,
        DirectivesModule,
        PipesModule,
        UserProfileRoutingModule,
        AppCoreModule,
        C2cRegulationModule,
        ImagesMiniModule,
        ProductCardModule,
        ProductListModule,
        HeaderModule,
        IconsModule,
        RatingModule,
        EmptyDressingCardModule,
        ButtonModule,
        UserBlockedWarningModule,
        UserDressingHandsetModule,
        UserInformationModule,
        MatProgressSpinnerModule,
        MatIconModule,
        MatButtonModule,
        ProductFilterModule,
        SharedModule,
        ChipModule,
    ],
    exports: [UserProfileComponent],
})
export class UserProfileModule {}
