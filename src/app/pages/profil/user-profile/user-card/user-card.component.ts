import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ShareService } from 'src/app/shared/services/share.service';
import { ProductsDispatchers } from 'src/app/store/services/products-dispatchers.service';
import { ProfileSelectors } from 'src/app/store/services/profile-selectors.service';
import { UserProfile, UserProfileStatus, UserType } from '../../../../shared/models/models';
import { DeviceService } from '../../../../shared/services/device.service';
import { UserService } from '../../../../shared/services/user.service';
import { FollowDispatchers } from '../../../../store/services/follow-dispatchers.service';
import { FollowSelectors } from '../../../../store/services/follow-selectors.service';
import { UserProfileSelectors } from '../../../../store/services/user-selectors.service';

@UntilDestroy()
@Component({
    selector: 'app-user-card',
    templateUrl: './user-card.component.html',
    styleUrls: ['./user-card.component.scss'],
})
export class UserCardComponent implements OnInit, OnChanges {
    @Input() user?: UserProfile;
    @Input() displayShare = true;

    canShareDressing = this.shareService.canShare();
    isCurrentUser$?: Observable<boolean>;
    avatarURL$?: Observable<string | undefined>;

    UserProfileStatus = UserProfileStatus;
    UserType = UserType;

    isFollowing$?: Observable<boolean>;
    followingLoading$?: Observable<boolean>;
    isLogged$?: Observable<boolean>;

    constructor(
        private readonly userService: UserService,
        private readonly userProfileSelectors: UserProfileSelectors,
        private readonly productsDispatchers: ProductsDispatchers,
        private readonly shareService: ShareService,
        public readonly deviceService: DeviceService,
        private readonly followSelectors: FollowSelectors,
        private readonly followDispatchers: FollowDispatchers,
        private readonly profileSelectors: ProfileSelectors,
    ) {}

    ngOnInit() {
        if (this.user?.id) {
            this.isCurrentUser$ = this.userProfileSelectors.isCurrentUser$(this.user.id).pipe(untilDestroyed(this));
            this.isFollowing$ = this.profileSelectors.isFollowing$.pipe(untilDestroyed(this));
        }
        this.followingLoading$ = this.followSelectors.isFollowingLoading$;
        this.isLogged$ = this.userProfileSelectors.isLogged$;
    }

    ngOnChanges(): void {
        this.avatarURL$ = this.userProfileSelectors.userProfile$.pipe(
            map(user => {
                const isCurrentUser = this.user?.id === user?.id;
                return this.user && this.userService.getAvatarUrl(this.user, isCurrentUser);
            }),
            untilDestroyed(this),
        );
    }

    onShareButtonClicked() {
        if (this.user) {
            this.productsDispatchers.shareDressing(this.user);
        }
    }

    onSubscriptionButtonClicked() {
        if (this.user) {
            this.followDispatchers.follow(this.user.id!);
        }
    }

    onUnsubscriptionButtonClicked() {
        if (this.user) {
            this.followDispatchers.unfollow(this.user.id!);
        }
    }
}
