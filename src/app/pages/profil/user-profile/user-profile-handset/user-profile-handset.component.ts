import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { EMPTY, Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { DiscussionsSelectors } from 'src/app/store/services/discussions-selectors.service';
import { UserProfileSelectors } from 'src/app/store/services/user-selectors.service';
import { Discussion, Lot, UserProfile } from '../../../../shared/models/models';

@UntilDestroy()
@Component({
    selector: 'app-user-profile-handset',
    templateUrl: './user-profile-handset.component.html',
    styleUrls: ['./user-profile-handset.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class UserProfileHandsetComponent implements OnInit {
    @Input() user?: UserProfile;
    @Input() scrollEl?: HTMLElement;

    isCurrentUser$?: Observable<boolean>;
    hasCartForSellerId$?: Observable<boolean>;
    lotOpenBySellerId$?: Observable<Lot | undefined>;
    discussionByLotId$?: Observable<Discussion | undefined>;

    constructor(
        private readonly userProfileSelectors: UserProfileSelectors,
        private readonly discussionsSelectors: DiscussionsSelectors,
    ) {}

    ngOnInit() {
        if (this.user?.id) {
            this.isCurrentUser$ = this.userProfileSelectors.isCurrentUser$(this.user.id).pipe(untilDestroyed(this));

            this.hasCartForSellerId$ = this.discussionsSelectors
                .hasCartForSellerId$(this.user.id)
                .pipe(untilDestroyed(this));

            this.lotOpenBySellerId$ = this.discussionsSelectors
                .lotOpenBySellerId$(this.user.id)
                .pipe(untilDestroyed(this));

            this.discussionByLotId$ = this.lotOpenBySellerId$.pipe(
                switchMap(lot => {
                    if (lot?.id) {
                        return this.discussionsSelectors.discussionByLotId$(lot.id);
                    }
                    return EMPTY;
                }),
                untilDestroyed(this),
            );
        }
    }
}
