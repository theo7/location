import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserProfileComponent } from './user-profile.component';

const routes: Routes = [
    {
        path: '',
        component: UserProfileComponent,
    },
    {
        path: 'rating',
        loadChildren: () => import('./user-rating/user-rating.module').then(m => m.UserRatingModule),
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class UserProfileRoutingModule {}
