import { ChangeDetectionStrategy, Component } from '@angular/core';
import { DeviceService } from 'src/app/shared/services/device.service';

@Component({
    selector: 'app-profil-child',
    templateUrl: './profil-child.component.html',
    styleUrls: ['./profil-child.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProfilChildComponent {
    constructor(public readonly deviceService: DeviceService) {}
}
