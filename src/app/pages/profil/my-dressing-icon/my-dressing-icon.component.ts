import { Component } from '@angular/core';

@Component({
    selector: 'app-my-dressing-icon',
    templateUrl: './my-dressing-icon.component.html',
    styleUrls: ['./my-dressing-icon.component.scss'],
})
export class MyDressingIconComponent {}
