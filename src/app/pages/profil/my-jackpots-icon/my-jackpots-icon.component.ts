import { Component } from '@angular/core';

@Component({
    selector: 'app-my-jackpots-icon',
    templateUrl: './my-jackpots-icon.component.html',
    styleUrls: ['./my-jackpots-icon.component.scss'],
})
export class MyJackpotsIconComponent {}
