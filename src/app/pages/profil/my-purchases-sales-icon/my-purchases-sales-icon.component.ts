import { Component } from '@angular/core';

@Component({
    selector: 'app-my-purchases-sales-icon',
    templateUrl: './my-purchases-sales-icon.component.html',
    styleUrls: ['./my-purchases-sales-icon.component.scss'],
})
export class MyPurchasesSalesIconComponent {}
