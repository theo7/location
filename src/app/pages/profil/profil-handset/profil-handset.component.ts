import { ChangeDetectionStrategy, Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormControl } from '@angular/forms';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { UserProfile } from '../../../shared/models/models';
import { UserProfileDispatchers } from '../../../store/services/user-dispatchers.service';
import { UserProfileSelectors } from '../../../store/services/user-selectors.service';

@UntilDestroy()
@Component({
    selector: 'app-profil-handset',
    templateUrl: './profil-handset.component.html',
    styleUrls: ['./profil-handset.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    encapsulation: ViewEncapsulation.None,
})
export class ProfilHandsetComponent implements OnInit {
    user$?: Observable<UserProfile | undefined>;
    isProfileModerated$?: Observable<boolean>;
    isDressingModerated$?: Observable<boolean | undefined>;

    feedbackFormUrl = environment.feedbackFormUrl;
    showBanner = environment.showBanner;
    appVersion = environment.appVersion;
    vacationMode: FormControl;

    constructor(
        private readonly userDispatchers: UserProfileDispatchers,
        public readonly userProfileSelectors: UserProfileSelectors,
    ) {
        this.vacationMode = new FormControl();
    }

    ngOnInit(): void {
        this.userDispatchers.getUserProfile();

        this.user$ = this.userProfileSelectors.userProfile$.pipe(untilDestroyed(this));
        this.isProfileModerated$ = this.userProfileSelectors.isProfileModerated$.pipe(untilDestroyed(this));
        this.isDressingModerated$ = this.userProfileSelectors.isDressingModerated$.pipe(untilDestroyed(this));

        this.initVacationMode();
    }

    private initVacationMode() {
        this.userProfileSelectors.isInVacationMode$.pipe(untilDestroyed(this)).subscribe(isInVacationMode => {
            this.vacationMode.setValue(isInVacationMode, { emitEvent: false });
        });

        this.vacationMode.valueChanges
            .pipe(untilDestroyed(this))
            .subscribe(() => this.userDispatchers.switchVacationMode());
    }

    logout() {
        this.userDispatchers.logout();
    }
}
