import { Component } from '@angular/core';

@Component({
    selector: 'app-addresses-icon',
    templateUrl: './addresses-icon.component.html',
    styleUrls: ['./addresses-icon.component.scss'],
})
export class AddressesIconComponent {}
