import { Component, ViewEncapsulation } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Observable } from 'rxjs';
import { UserProfileSelectors } from 'src/app/store/services/user-selectors.service';
import { UserProfile } from '../../shared/models/models';
import { DeviceService } from '../../shared/services/device.service';

@UntilDestroy()
@Component({
    selector: 'app-profil',
    templateUrl: './profil.component.html',
    styleUrls: ['./profil.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class ProfilComponent {
    user$?: Observable<UserProfile | undefined>;

    constructor(userProfileSelectors: UserProfileSelectors, public readonly deviceService: DeviceService) {
        this.user$ = userProfileSelectors.userProfile$.pipe(untilDestroyed(this));
    }
}
