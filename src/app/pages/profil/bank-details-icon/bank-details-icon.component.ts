import { Component } from '@angular/core';

@Component({
    selector: 'app-bank-details-icon',
    templateUrl: './bank-details-icon.component.html',
    styleUrls: ['./bank-details-icon.component.scss'],
})
export class BankDetailsIconComponent {}
