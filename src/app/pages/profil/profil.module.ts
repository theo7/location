import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatListModule } from '@angular/material/list';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatToolbarModule } from '@angular/material/toolbar';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { IconsModule } from 'src/app/features/icons/icons.module';
import { HeaderModule } from 'src/app/features/layout/headers/header.module';
import { KiaLayoutModule } from 'src/app/features/layout/layout.module';
import { ChipModule } from 'src/app/shared/components/chip/chip.module';
import { ImagesMiniModule } from 'src/app/shared/components/images-mini/images-mini.module';
import { UserBlockedWarningModule } from 'src/app/shared/components/user-blocked-warning/user-blocked-warning.module';
import { AppCoreModule } from '../../shared/components/app-core/app-core.module';
import { UserDressingHandsetModule } from '../../shared/components/app-user-dressing-handset/user-dressing-handset.module';
import { AvatarModule } from '../../shared/components/avatar/avatar.module';
import { ButtonModule } from '../../shared/components/button/button.module';
import { C2cRegulationModule } from '../../shared/components/c2c-regulation/c2c-regulation.module';
import { EmptyDressingCardModule } from '../../shared/components/empty-dressing-card/empty-dressing-card.module';
import { ProductCardModule } from '../../shared/components/product-card/product-card.module';
import { ProductListModule } from '../../shared/components/product-list/product-list.module';
import { UserInformationModule } from '../../shared/components/user-information/user-information.module';
import { RatingModule } from '../../shared/components/user-rating/rating.module';
import { DirectivesModule } from '../../shared/directives/directives.module';
import { PipesModule } from '../../shared/pipes/pipes.module';
import { SharedModule } from '../../shared/shared.module';
import { AddressesIconComponent } from './addresses-icon/addresses-icon.component';
import { BankDetailsIconComponent } from './bank-details-icon/bank-details-icon.component';
import { MyDressingIconComponent } from './my-dressing-icon/my-dressing-icon.component';
import { MyJackpotsIconComponent } from './my-jackpots-icon/my-jackpots-icon.component';
import { MyPurchasesSalesIconComponent } from './my-purchases-sales-icon/my-purchases-sales-icon.component';
import { ProfilChildComponent } from './profil-child/profil-child.component';
import { ProfilDesktopComponent } from './profil-desktop/profil-desktop.component';
import { ProfilHandsetComponent } from './profil-handset/profil-handset.component';
import { ProfilRoutingModule } from './profil-routing.module';
import { ProfilSidePanelComponent } from './profil-side-panel/profil-side-panel.component';
import { ProfilComponent } from './profil.component';
import { UserDressingComponent } from './user-dressing/user-dressing.component';
import { UserProfileModule } from './user-profile/user-profile.module';

@NgModule({
    declarations: [
        ProfilComponent,
        UserDressingComponent,
        AddressesIconComponent,
        BankDetailsIconComponent,
        MyDressingIconComponent,
        MyJackpotsIconComponent,
        MyPurchasesSalesIconComponent,
        ProfilSidePanelComponent,
        ProfilHandsetComponent,
        ProfilDesktopComponent,
        ProfilChildComponent,
    ],
    imports: [
        SharedModule,
        ProfilRoutingModule,
        MatProgressSpinnerModule,
        MatButtonModule,
        MatListModule,
        MatToolbarModule,
        ReactiveFormsModule,
        ProductCardModule,
        ButtonModule,
        AvatarModule,
        HeaderModule,
        AppCoreModule,
        PipesModule,
        InfiniteScrollModule,
        UserDressingHandsetModule,
        RatingModule,
        C2cRegulationModule,
        ProductListModule,
        IconsModule,
        DirectivesModule,
        ImagesMiniModule,
        EmptyDressingCardModule,
        UserBlockedWarningModule,
        UserInformationModule,
        MatSlideToggleModule,
        ChipModule,
        UserProfileModule,
        KiaLayoutModule,
    ],
    providers: [],
})
export class ProfilModule {}
