import { Injectable, NgZone } from '@angular/core';
import { CanActivate, Router, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { first, map } from 'rxjs/operators';
import { UserProfileSelectors } from 'src/app/store/services/user-selectors.service';

@Injectable({
    providedIn: 'root',
})
export class RedirectToMyDressingGuard implements CanActivate {
    constructor(
        private readonly router: Router,
        private readonly userProfileSelectors: UserProfileSelectors,
        private readonly zone: NgZone,
    ) {}

    canActivate(): Observable<boolean | UrlTree> {
        return this.userProfileSelectors.userProfile$.pipe(
            first(),
            map(userProfile => {
                return this.zone.run(() => {
                    if (userProfile) {
                        return this.router.createUrlTree(['profile', userProfile.id, 'dressing']);
                    }

                    return this.router.createUrlTree(['login']);
                });
            }),
        );
    }
}
