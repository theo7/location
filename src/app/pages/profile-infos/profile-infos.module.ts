import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { TranslateModule } from '@ngx-translate/core';
import { HeaderModule } from 'src/app/features/layout/headers/header.module';
import { AppCoreModule } from 'src/app/shared/components/app-core/app-core.module';
import { ProfileInfosRoutingModule } from './profile-infos-routing.module';
import { ProfileInfosComponent } from './profile-infos.component';

@NgModule({
    declarations: [ProfileInfosComponent],
    imports: [
        CommonModule,
        ProfileInfosRoutingModule,
        TranslateModule,
        HeaderModule,
        MatListModule,
        MatIconModule,
        AppCoreModule,
    ],
})
export class ProfileInfosModule {}
