import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProfileInfosComponent } from './profile-infos.component';

const routes: Routes = [
    {
        path: '',
        component: ProfileInfosComponent,
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class ProfileInfosRoutingModule {}
