import { ChangeDetectionStrategy, Component } from '@angular/core';
import { DeviceService } from 'src/app/shared/services/device.service';
import { environment } from 'src/environments/environment';

@Component({
    selector: 'app-profile-infos',
    templateUrl: './profile-infos.component.html',
    styleUrls: ['./profile-infos.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProfileInfosComponent {
    contactAddress = environment.contactAddress;

    constructor(public readonly deviceService: DeviceService) {}
}
