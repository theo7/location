import { MissingTranslationHandler, MissingTranslationHandlerParams } from '@ngx-translate/core';

export class CustomMissingTranslationHandler implements MissingTranslationHandler {
    handle(params: MissingTranslationHandlerParams): any {
        const additionalParams: any = params.interpolateParams;
        return additionalParams?.nullable ? null : params.key;
    }
}
