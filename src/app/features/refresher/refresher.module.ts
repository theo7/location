import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RefresherComponent } from './refresher.component';

@NgModule({
    declarations: [RefresherComponent],
    imports: [CommonModule],
    exports: [RefresherComponent],
})
export class RefresherModule {}
