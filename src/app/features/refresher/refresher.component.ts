import { AfterViewInit, Component, EventEmitter, Input, OnDestroy, Output } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import * as PullToRefresh from 'pulltorefreshjs';
import { SearchSelectors } from 'src/app/store/services/search-selectors.service';

@UntilDestroy()
@Component({
    selector: 'app-refresher',
    template: '',
})
export class RefresherComponent implements AfterViewInit, OnDestroy {
    @Input() mainElementId?: string;

    @Output() refresh = new EventEmitter<void>();

    activeRefresher = true;

    constructor(searchSelectors: SearchSelectors) {
        // TODO : this is NOT a generic solution!!
        // 💡 but using selector is the only way to disable PullToRefresh of search page...
        searchSelectors.activeRefresher$.pipe(untilDestroyed(this)).subscribe(activeRefresher => {
            this.activeRefresher = activeRefresher;
        });
    }

    ngAfterViewInit(): void {
        const self = this;

        PullToRefresh.init({
            mainElement: `#${this.mainElementId}`,
            iconRefreshing: '<img src="assets/svg/Rolling-1s-28px.svg" alt="spinner">',
            instructionsRefreshing: '<span></span>',
            instructionsPullToRefresh: '<span></span>',
            instructionsReleaseToRefresh: '<span></span>',
            shouldPullToRefresh() {
                if (!self.activeRefresher) {
                    return false;
                }

                const mainElement = this.mainElement as HTMLElement | undefined;
                return !mainElement?.scrollTop;
            },
            onRefresh: () => this.refresh.emit(),
        });
    }

    ngOnDestroy(): void {
        PullToRefresh.destroyAll();
    }
}
