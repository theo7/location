import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpStatus } from '../../shared/helpers/http-status.enum';

const TRACKING_ERROR_CODES_EXCLUDED = [HttpStatus.UNAUTHORIZED, HttpStatus.MISSING_RESPONSE];

@Injectable({
    providedIn: 'root',
})
export class ApiErrorsInterceptor implements HttpInterceptor {
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(req).pipe(
            catchError((err: any) => {
                if (err instanceof HttpErrorResponse && !TRACKING_ERROR_CODES_EXCLUDED.includes(err.status)) {
                    // TODO : backend log tracking ?
                }
                return throwError(err);
            }),
        );
    }
}
