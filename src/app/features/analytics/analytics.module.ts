import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { ApiErrorsInterceptor } from './api-errors.interceptor';

@NgModule({
    declarations: [],
    imports: [HttpClientModule],
    providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: ApiErrorsInterceptor,
            multi: true,
        },
    ],
})
export class AnalyticsModule {}
