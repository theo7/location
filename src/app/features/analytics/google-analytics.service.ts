import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import sha256 from 'crypto-js/sha256';
import { CookieService } from 'ngx-cookie-service';
import { EMPTY, Observable, of, throwError } from 'rxjs';
import { catchError, map, mergeMap } from 'rxjs/operators';
import {
    getHandlingFees,
    getShippingFees,
    getTransactionAmount,
    getTransactionId,
    getTransactionPaymentOptions,
} from 'src/app/shared/functions/transaction.functions';
import environment from 'src/environments/global.environment';
import { Lot, Product, ProductFilter } from '../../shared/models/models';
import { CookieConsentService } from '../cookie-consent/services/cookie-consent.service';

declare const ga: UniversalAnalytics.ga;
const trackerId = 'secondemain';

@Injectable({
    providedIn: 'root',
})
export class GoogleAnalyticsService {
    constructor(
        private readonly cookieService: CookieService,
        private readonly cookieConsentService: CookieConsentService,
        private readonly http: HttpClient,
        private readonly translate: TranslateService,
    ) {}

    private isEnabled() {
        return this.cookieConsentService.hasConsentedFor('performance');
    }

    private getIP(): Observable<string> {
        return this.http.get<{ ip: string }>(environment.ipService).pipe(
            mergeMap(response => {
                if (!response?.ip) {
                    return throwError('Cannot retrieve ip address...');
                }

                return of(response.ip);
            }),
        );
    }

    private getEncryptedIp(): Observable<string> {
        return this.getIP().pipe(
            map(ip => ip + environment.ipSalt),
            map(saltedIP => sha256(saltedIP).toString()),
        );
    }

    // Cookie utilisateur 13 mois non-glissant
    private getCookieExpires(): number {
        // TODO : unit tests
        let valueCookieGAExpire: number;
        const THIRTEEN_MONTHS = 60 * 60 * 24 * 30 * 13 * 1000;
        try {
            let timeDiff;
            const gaCookie = this.cookieService.get('_ga');
            if (gaCookie) {
                const gaCookieCreationTime = new Date(Number(gaCookie.split('.').pop()) * 1000).getTime();
                timeDiff = Math.round((gaCookieCreationTime + THIRTEEN_MONTHS - new Date().getTime()) / 1000);
            } else {
                timeDiff = THIRTEEN_MONTHS / 1000;
            }
            valueCookieGAExpire = timeDiff;
        } catch (e) {
            valueCookieGAExpire = THIRTEEN_MONTHS / 1000;
        }
        return valueCookieGAExpire;
    }

    public startTrackerWithId(trackingId: string): void {
        if (!this.isEnabled()) {
            return;
        }

        ga('create', {
            trackingId,
            cookieDomain: environment.domain,
            name: trackerId,
            cookieExpires: this.getCookieExpires(),
            siteSpeedSampleRate: 1,
        });
        ga(`${trackerId}.set`, 'anonymizeIp', true);
        ga(`${trackerId}.set`, 'dimension12', this.cookieService.get('_gid')); // Cookie session
        ga(`${trackerId}.set`, 'dimension13', this.cookieService.get('_ga')); // Cookie utilisateur

        this.getEncryptedIp()
            .pipe(catchError(() => EMPTY))
            .subscribe(ip => {
                ga(`${trackerId}.set`, 'dimension16', ip); // Adresse IP chiffrée
            });

        ga(`${trackerId}.set`, 'dimension50', navigator.userAgent);

        if (environment.useEnhancedEcommercePlugin) {
            ga(`${trackerId}.require`, 'ec');
            ga(`${trackerId}.set`, 'currencyCode', 'EUR');
        } else {
            ga(`${trackerId}.require`, 'ecommerce');
        }
    }

    public trackView(screenName: string): void {
        if (!this.isEnabled()) {
            return;
        }

        ga(`${trackerId}.set`, 'page', screenName);
        ga(`${trackerId}.send`, 'pageview');
    }

    private trackEvent(category: string, action: string, label: string, value?: number): void {
        if (!this.isEnabled()) {
            return;
        }

        ga(`${trackerId}.send`, 'event', {
            eventCategory: category,
            eventLabel: label,
            eventAction: action,
            eventValue: value,
        });
    }

    public track404(url: string): void {
        if (!this.isEnabled()) {
            return;
        }

        this.trackEvent('error', '404', url);
    }

    public trackSearchAndFilter(searchFilter: ProductFilter, results: number): void {
        if (!this.isEnabled()) {
            return;
        }

        if (searchFilter.searchText) {
            const searchAction = results ? 'search with results' : 'search without results';
            this.trackEvent('search', searchAction, searchFilter.searchText, results);
        }

        const action = results ? 'filter with results' : 'filter without results';
        const label = [
            searchFilter.category?.labelKey ?? '',
            searchFilter.subCategory?.labelKey ?? '',
            searchFilter.productsTypes?.map(productType => productType.labelKey).join(',') ?? '',
            searchFilter.brands?.map(brand => brand.label).join(',') ?? '',
            searchFilter.colors?.map(color => color.labelKey).join(',') ?? '',
            searchFilter.conditions?.map(condition => condition.labelKey).join(',') ?? '',
            searchFilter.sizes?.map(size => size.labelKey).join(',') ?? '',
            searchFilter.minPrice,
            searchFilter.maxPrice,
        ].join('|');

        const hasFilter = !!label.split('|').join('');

        if (hasFilter) {
            this.trackEvent('filter', action, label, results);
        }
    }

    public trackAddToCart(product: Product, sellerId: number): void {
        if (!this.isEnabled()) {
            return;
        }

        this.trackProduct('add_to_cart', product, sellerId);
    }

    public trackAddProduct(product: Product, sellerId: number): void {
        if (!this.isEnabled()) {
            return;
        }

        this.trackProduct('product_publication', product, sellerId);
    }

    public trackSendMessage(product: Product, sellerId: number): void {
        if (!this.isEnabled()) {
            return;
        }

        this.trackProduct('send_a_message', product, sellerId);
    }

    public trackPurchase(lot: Lot): void {
        if (!this.isEnabled()) {
            return;
        }

        const transactionId = getTransactionId(lot);
        const now = new Date();

        const id = sha256(transactionId ? transactionId.toString() : now.toISOString()).toString(); // anonymized id
        const revenue = getTransactionAmount(lot, true); // total amount paid
        const shipping = getShippingFees(lot); // shipping fees
        const tax = getHandlingFees(lot); //  tax = variable & fixed fees
        const option = getTransactionPaymentOptions(lot); // payment mode
        const affiliation = lot.seller?.nickname; // name of the store (seller name)

        if (environment.useEnhancedEcommercePlugin) {
            ga(`${trackerId}.ec:setAction`, 'purchase', {
                id,
                revenue,
                shipping,
                tax,
                option,
                affiliation,
            });

            ga(`${trackerId}.send`, 'pageview'); // required to trigger ecommerce (ec) event
        } else {
            ga(`${trackerId}.ecommerce:addTransaction`, {
                id,
                affiliation,
                revenue,
                shipping,
                tax,
                currency: 'EUR',
            });

            ga(`${trackerId}.ecommerce:send`);
        }
    }

    private trackProduct(event: string, product: Product, sellerId: number): void {
        if (!this.isEnabled()) {
            return;
        }

        const category = product.productType?.subCategory?.category?.labelKey
            ? this.translate.instant('product.category.' + product.productType?.subCategory?.category?.labelKey)
            : '';

        const subCategory = product.productType?.subCategory?.labelKey
            ? this.translate.instant('product.sub_category.' + product.productType?.subCategory?.labelKey)
            : '';

        const productType = product.productType?.labelKey
            ? this.translate.instant('product.type.' + product.productType?.labelKey)
            : '';

        const condition = this.translate.instant('condition.label.' + product.condition.labelKey);

        const action = [category, subCategory, productType, condition].join('|');

        const label = [sellerId, product.brand.label, product.label, product.id].join('|');
        const price = product.price;

        this.trackEvent(event, action, label, price);
    }
}
