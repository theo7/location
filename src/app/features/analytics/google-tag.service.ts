import sha256 from 'crypto-js/sha256';
import { getTransactionAmount, getTransactionId } from 'src/app/shared/functions/transaction.functions';
import { Lot } from 'src/app/shared/models/models';
import { CookieConsentService } from '../cookie-consent/services/cookie-consent.service';

declare let gtag: any;

@Injectable({
    providedIn: 'root',
})
export class GoogleTagService {
    constructor(private readonly cookieConsentService: CookieConsentService) {}

    private isEnabled() {
        return this.cookieConsentService.hasConsentedFor('performance');
    }

    public startTrackerWithIds(trackingIds: string[]): void {
        if (!this.isEnabled()) {
            return;
        }

        // 💡 Multiple GTAG : https://developers.google.com/gtagjs/devguide/add
        trackingIds.forEach(trackingId => gtag('config', trackingId));
    }

    public trackVisit(): void {
        if (!this.isEnabled()) {
            return;
        }

        gtag('event', 'conversion', { send_to: 'AW-443485052/gD4rCK610fUBEPyWvNMB' });
    }

    public trackAddToCart(): void {
        if (!this.isEnabled()) {
            return;
        }

        gtag('event', 'conversion', { send_to: 'AW-443485052/Mo2fCNXMsfUBEPyWvNMB' });
    }

    public trackPayment(lotId: number): void {
        if (!this.isEnabled()) {
            return;
        }

        gtag('event', 'conversion', { send_to: 'AW-443485052/_hF3CM7UsfUBEPyWvNMB', transaction_id: lotId });
    }

    public trackPurchase(lot: Lot): void {
        if (!this.isEnabled()) {
            return;
        }

        const transactionId = getTransactionId(lot);
        const now = new Date();

        const orderAmount = getTransactionAmount(lot, true);
        const currency = 'EUR'; // TODO : change when needed

        const id = sha256(transactionId ? transactionId.toString() : now.toISOString()).toString(); // anonymized id

        const getTrackerId = () => {
            switch (Capacitor.getPlatform()) {
                case 'android':
                    return 'AW-310926333/FswlCNCU1v4CEP23oZQB';
                case 'ios':
                    return 'AW-310926333/33lUCK-F1v4CEP23oZQB';
                default:
                    return 'AW-310926333/sQ_lCKu-1_ACEP23oZQB';
            }
        };

        const tracker = getTrackerId();

        gtag('event', 'conversion', {
            send_to: tracker,
            value: orderAmount,
            currency,
            transaction_id: id,
        });
    }
}
