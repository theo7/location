import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';

// 💡 : RGPD Facebook Technical doc : https://developers.facebook.com/docs/facebook-pixel/implementation/gdpr/

declare const fbq: any;

@Injectable({
    providedIn: 'root',
})
export class FacebookPixelService {
    constructor() {
        this.initTracker(environment.facebookPixelID);
    }

    public initTracker(trackingId: string | null): void {
        if (!trackingId) {
            return;
        }

        fbq('consent', 'revoke');
        fbq('set', 'autoConfig', false, trackingId); // 💡 : Prevent to send event not expected
        fbq('init', trackingId);
        fbq('track', 'PageView');
    }

    public activateTracker(): void {
        fbq('consent', 'grant');
    }

    public stopTracker(): void {
        fbq('consent', 'revoke');
    }
}
