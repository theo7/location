import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { TranslateModule } from '@ngx-translate/core';
import { AppCoreModule } from 'src/app/shared/components/app-core/app-core.module';
import { ActivateNotificationsComponent } from './activate-notifications/activate-notifications.component';

@NgModule({
    declarations: [ActivateNotificationsComponent],
    imports: [CommonModule, TranslateModule, AppCoreModule, MatButtonModule],
    exports: [ActivateNotificationsComponent],
})
export class NotificationsModule {}
