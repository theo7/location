import { ChangeDetectionStrategy, Component } from '@angular/core';
import { DeviceService } from 'src/app/shared/services/device.service';
import { NotificationsDispatchers } from 'src/app/store/services/notifications-dispatchers.service';
import { SettingsDispatchers } from 'src/app/store/services/settings-dispatchers.service';

@Component({
    selector: 'app-activate-notifications',
    templateUrl: './activate-notifications.component.html',
    styleUrls: ['./activate-notifications.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ActivateNotificationsComponent {
    constructor(
        private readonly notificationsDispatchers: NotificationsDispatchers,
        private readonly settingsDispatchers: SettingsDispatchers,
        public readonly device: DeviceService,
    ) {}

    onActivateNotificationsButtonClicked() {
        this.notificationsDispatchers.requestPermission();
    }

    onDontAskButtonClicked() {
        this.settingsDispatchers.setNotificationSetting(false);
    }
}
