import { ChangeDetectionStrategy, Component } from '@angular/core';
import { StorageMap } from '@ngx-pwa/local-storage';
import { LOCAL_STORAGE_INSTALL_PANEL } from 'src/app/shared/models/models';
import { DeviceService } from 'src/app/shared/services/device.service';
import { environment } from 'src/environments/environment';

@Component({
    selector: 'app-install-panel',
    templateUrl: './install-panel.component.html',
    styleUrls: ['./install-panel.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class InstallPanelComponent {
    isAndroidWebBrowser: boolean;
    isiOSWebBrowser: boolean;
    appStoreUrl?: string;

    constructor(private readonly device: DeviceService, private readonly storage: StorageMap) {
        this.isAndroidWebBrowser = this.device.isAndroidWebBrowser();
        this.isiOSWebBrowser = this.device.isiOSWebBrowser();

        if (this.isAndroidWebBrowser) {
            this.appStoreUrl = environment.appStoreUrls.android;
        }
        if (this.isiOSWebBrowser) {
            this.appStoreUrl = environment.appStoreUrls.iOS;
        }
    }

    onDontAskButtonClicked() {
        // sadly, subscribe is required...
        this.storage.set(LOCAL_STORAGE_INSTALL_PANEL, false).subscribe(() => {});
    }

    onInstallButtonClicked() {
        // sadly, subscribe is required...
        this.storage.set(LOCAL_STORAGE_INSTALL_PANEL, true).subscribe(() => {});
    }
}
