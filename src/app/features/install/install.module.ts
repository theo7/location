import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { ButtonModule } from 'src/app/shared/components/button/button.module';
import { InstallPanelComponent } from './install-panel/install-panel.component';

@NgModule({
    declarations: [InstallPanelComponent],
    imports: [CommonModule, TranslateModule, ButtonModule],
    exports: [InstallPanelComponent],
})
export class InstallModule {}
