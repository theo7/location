import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { Router } from '@angular/router';
import { Capacitor } from '@capacitor/core';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';
import { QuillModule } from 'ngx-quill';
import Quill from 'quill';
import { extractRelativePath, isExternalLink } from '../../shared/functions/links.functions';

@NgModule({
    declarations: [],
    imports: [CommonModule, QuillModule.forRoot()],
    exports: [QuillModule],
})
export class CustomQuillLinkModule {
    constructor(router: Router, iab: InAppBrowser) {
        const Link = Quill.import('formats/link');
        class CustomLink extends Link {
            static create(value: string) {
                const node: HTMLElement = super.create(value);

                if (isExternalLink(value, window.origin)) {
                    // external link with target="_blank"
                    if (Capacitor.isNativePlatform()) {
                        node.removeAttribute('href');
                        node.removeAttribute('target');
                        node.setAttribute('role', 'link');

                        node.addEventListener('click', () => {
                            const inAppBrowserOptions: InAppBrowserOptions = {
                                location: 'yes',
                                fullscreen: 'yes',
                                hidenavigationbuttons: 'no',
                                toolbarposition: 'top',
                            };
                            if (value) {
                                iab.create(value, '_blank', inAppBrowserOptions);
                            }
                        });
                    }

                    return node;
                }

                // internal link with routerLink
                const relativePath = extractRelativePath(value);

                node.removeAttribute('target');
                node.removeAttribute('rel');
                node.setAttribute('routerLink', relativePath);

                node.addEventListener('click', e => {
                    e.preventDefault();
                    e.stopPropagation();

                    router.navigateByUrl(relativePath);
                });

                return node;
            }
        }

        Quill.register(CustomLink, true);
    }
}
