import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { AuthorizationInterceptor } from './interceptor/authorization.interceptor';

@NgModule({
    imports: [SharedModule, HttpClientModule, ReactiveFormsModule],
    providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthorizationInterceptor,
            multi: true,
        },
    ],
})
export class AuthenticationModule {}
