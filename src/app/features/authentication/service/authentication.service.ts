import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { default as firebase } from 'firebase/app';
import { from, Observable, throwError } from 'rxjs';
import { first, switchMap } from 'rxjs/operators';
import { environment } from '../../../../environments/environment';

@Injectable({
    providedIn: 'root',
})
export class AuthenticationService {
    private readonly usersUrl = `${environment.apiUrl}/users`;
    public user?: firebase.User;

    constructor(private firebaseAuth: AngularFireAuth, private http: HttpClient) {
        firebaseAuth.user.subscribe(user => (this.user = user || undefined));
    }

    async login(email: string, password: string) {
        const result = await this.firebaseAuth.signInWithEmailAndPassword(email, password);
        if (!result.user?.emailVerified) throw new Error('email-not-verified');
        return result;
    }

    logout(): Promise<void> {
        return this.firebaseAuth.signOut();
    }

    createUser(user) {
        return this.http.post(this.usersUrl, user);
    }

    getToken(): Observable<string | null> {
        return this.firebaseAuth.idToken;
    }

    sendResetPasswordEmail(email: string): Observable<void> {
        return this.http.post<void>(`${this.usersUrl}/reset-password`, { mail: email });
    }

    resetPassword(password: string, token: string): Observable<void> {
        const headers = new HttpHeaders().append('without_auth', 'true');
        return this.http.post<void>(`${this.usersUrl}/reset-password/change`, { password, token }, { headers });
    }

    checkTokenValidity(token: string): Observable<boolean> {
        const headers = new HttpHeaders().append('without_auth', 'true');
        return this.http.post<boolean>(`${this.usersUrl}/reset-password/check`, { token }, { headers });
    }

    changePassword(currentPassword: string, newPassword: string): Observable<void> {
        return this.firebaseAuth.user.pipe(
            first(),
            switchMap(user => {
                if (!user?.email) {
                    return throwError('Email is required to change password...');
                }

                const credential = firebase.auth.EmailAuthProvider.credential(user.email, currentPassword);
                return from(user.reauthenticateWithCredential(credential));
            }),
            switchMap(userCredential => {
                if (!userCredential.user) {
                    return throwError('User not authenticated, it is required to change password...');
                }

                return from(userCredential.user.updatePassword(newPassword));
            }),
        );
    }
}
