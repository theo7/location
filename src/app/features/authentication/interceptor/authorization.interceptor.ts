import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { first, mergeMap } from 'rxjs/operators';
import { environment } from '../../../../environments/environment';
import { AuthenticationService } from '../service/authentication.service';

@Injectable()
export class AuthorizationInterceptor implements HttpInterceptor {
    constructor(private authenticationService: AuthenticationService) {}

    intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
        if (request.headers.get('without_auth')) {
            return next.handle(request.clone());
        } else {
            return this.authenticationService.getToken().pipe(
                first(),
                mergeMap(token => {
                    if (request.url.startsWith(environment.apiUrl)) {
                        const headers = request.headers.append('X-Auth-Token', token ?? '');
                        return next.handle(request.clone({ headers }));
                    } else {
                        return next.handle(request);
                    }
                }),
            );
        }
    }
}
