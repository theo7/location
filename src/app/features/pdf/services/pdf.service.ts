import { Injectable } from '@angular/core';
import { PDFDocument, PDFImage } from 'pdf-lib';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root',
})
export class PdfService {
    toPdf(file: File | Blob): Observable<File> {
        return new Observable<File>(observer => {
            const reader = new FileReader();
            reader.readAsArrayBuffer(file);
            reader.onload = async ev => {
                try {
                    let bytes: Uint8Array;

                    if (file.type === 'application/pdf' || file.type === 'pdf') {
                        bytes = new Uint8Array(ev.target?.result as ArrayBuffer);
                    } else {
                        const document = await PDFDocument.create();
                        const page = await document.addPage();
                        const image = await this.loadImageAsPDFImage(
                            document,
                            ev.target?.result as ArrayBuffer,
                            file.type,
                        );
                        let dimension = image.size();
                        if (image.width > page.getWidth()) {
                            dimension = image.scale(page.getWidth() / image.width);
                        }

                        page.drawImage(image, {
                            x: page.getWidth() / 2 - dimension.width / 2,
                            y: page.getHeight() - dimension.height,
                            width: dimension.width,
                            height: dimension.height,
                        });

                        bytes = await document.save();
                    }

                    observer.next(
                        new File([bytes], 'result.pdf', {
                            lastModified: new Date().getTime(),
                            type: 'application/pdf',
                        }),
                    );
                    observer.complete();
                } catch (e) {
                    observer.error(e);
                }
            };
            reader.onerror = ev => observer.error(ev);
        });
    }

    mergePDFDocuments(files: File[]): Promise<File> {
        return new Promise<File>(async resolve => {
            const document = await PDFDocument.create();
            const promises = files.map(async file => {
                const pdf = await this.loadFileAsPDFDocument(file);
                const pages = await document.copyPages(pdf, pdf.getPageIndices());
                pages.forEach(page => {
                    document.addPage(page);
                });
            });
            await Promise.all(promises);

            const bytes = await document.save();
            resolve(
                new File([bytes], 'result.pdf', {
                    type: 'application/pdf',
                }),
            );
        });
    }

    private loadFileAsPDFDocument(file: File): Promise<PDFDocument> {
        return new Promise<PDFDocument>((resolve, reject) => {
            const reader = new FileReader();
            reader.readAsArrayBuffer(file);
            reader.onload = ev => {
                resolve(PDFDocument.load(ev.target?.result as ArrayBuffer));
            };
            reader.onerror = ev => reject(ev);
        });
    }

    private loadImageAsPDFImage(doc: PDFDocument, buffer: ArrayBuffer, type: string): Promise<PDFImage> {
        switch (type) {
            case 'image/jpeg':
            case 'jpeg':
                return doc.embedJpg(buffer);
            case 'image/png':
            case 'png':
                return doc.embedPng(buffer);
            default:
                throw new Error('invalid image type');
        }
    }
}
