import { ChangeDetectorRef, Component, Input, OnChanges, SimpleChanges } from '@angular/core';

@Component({
    selector: 'app-file-preview',
    templateUrl: './file-preview.component.html',
    styleUrls: ['./file-preview.component.scss'],
})
export class FilePreviewComponent implements OnChanges {
    @Input() file?: File;

    loading?: boolean;
    fileData?: any;

    constructor(private cd: ChangeDetectorRef) {}

    ngOnChanges(changes: SimpleChanges) {
        if (this.file) {
            this.loading = true;
            const fileReader = new FileReader();
            fileReader.readAsDataURL(this.file);
            fileReader.onload = event => {
                this.fileData = event.target?.result;
                this.loading = false;
                this.cd.detectChanges();
            };
        }
    }
}
