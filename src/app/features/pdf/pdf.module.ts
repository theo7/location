import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { FilePreviewComponent } from './components/file-preview/file-preview.component';

@NgModule({
    declarations: [FilePreviewComponent],
    imports: [CommonModule, PdfViewerModule],
    exports: [FilePreviewComponent],
})
export class PdfModule {}
