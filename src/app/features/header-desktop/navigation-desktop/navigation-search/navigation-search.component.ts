import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { MatMenuTrigger } from '@angular/material/menu';
import { Router } from '@angular/router';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Actions } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, map, withLatestFrom } from 'rxjs/operators';
import { convertSearchToQueryParams } from 'src/app/shared/functions/product-filter.functions';
import { FilterType, Search } from '../../../../shared/models/models';
import { FilterDispatchers } from '../../../../store/services/filter-dispatchers.service';
import { FilterSelectors } from '../../../../store/services/filter-selectors.service';
import { UserProfileSelectors } from '../../../../store/services/user-selectors.service';

@UntilDestroy()
@Component({
    selector: 'app-navigation-search',
    templateUrl: './navigation-search.component.html',
    styleUrls: ['./navigation-search.component.scss'],
})
export class NavigationSearchComponent implements OnInit {
    @ViewChild(MatMenuTrigger)
    searchHistoryMenuTrigger?: MatMenuTrigger;

    searchInput: FormControl;

    isNotLogged$: Observable<boolean>;

    private readonly filterKey: FilterType = 'search';

    constructor(
        private readonly actions$: Actions,
        private readonly router: Router,
        private readonly formBuilder: FormBuilder,
        private readonly filterDispatchers: FilterDispatchers,
        private readonly filterSelectors: FilterSelectors,
        private readonly userProfileSelectors: UserProfileSelectors,
    ) {
        this.isNotLogged$ = this.userProfileSelectors.isNotLogged$.pipe(untilDestroyed(this));
        this.searchInput = this.formBuilder.control('');
    }

    ngOnInit(): void {
        this.searchInput.valueChanges
            .pipe(
                debounceTime(500),
                distinctUntilChanged((s1?: string, s2?: string) => s1?.trim() === s2?.trim()),
                withLatestFrom(this.filterSelectors.keyword$(this.filterKey)),
                filter(([keyword, currentKeyword]) => keyword?.trim() !== currentKeyword?.trim()),
                map(([keyword]) => keyword),
                filter(value => !value || value.length >= 3),
                withLatestFrom(this.filterSelectors.currentRouterParams$(this.filterKey)),
                untilDestroyed(this),
            )
            .subscribe(([searchText, queryParams]) => {
                this.router.navigate(['search'], {
                    queryParams: {
                        ...queryParams,
                        searchText,
                        sortField: !!searchText && searchText.length > 0 ? 'relevance' : 'creationDate',
                        sortOrder: 'desc',
                    },
                });
            });

        this.filterSelectors
            .keyword$(this.filterKey)
            .pipe(untilDestroyed(this))
            .subscribe(searchText => {
                this.searchInput.patchValue(searchText);
            });
    }

    resetFilter(): void {
        this.searchInput.patchValue(undefined);
    }

    onSearchSelected(search: Search) {
        this.searchHistoryMenuTrigger?.closeMenu();

        const queryParams = convertSearchToQueryParams(search);

        this.router.navigate(['search'], {
            queryParams,
        });
    }
}
