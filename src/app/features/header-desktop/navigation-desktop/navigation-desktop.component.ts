import { Component, OnInit } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ScrollStatesService } from 'src/app/shared/services/scroll-states.service';
import { NotificationsDispatchers } from 'src/app/store/services/notifications-dispatchers.service';
import { NotificationsSelectors } from 'src/app/store/services/notifications-selectors.service';
import { RouterSelectors } from 'src/app/store/services/router-selectors.service';
import { environment } from '../../../../environments/environment';
import { HomeService } from '../../../pages/home/home.service';
import { UserProfile } from '../../../shared/models/models';
import { UserService } from '../../../shared/services/user.service';
import { DiscussionsSelectors } from '../../../store/services/discussions-selectors.service';
import { UserProfileSelectors } from '../../../store/services/user-selectors.service';

@UntilDestroy()
@Component({
    selector: 'app-navigation-desktop',
    templateUrl: './navigation-desktop.component.html',
    styleUrls: ['./navigation-desktop.component.scss'],
})
export class NavigationDesktopComponent implements OnInit {
    userProfile$?: Observable<(UserProfile & { avatarURL: string }) | undefined>;
    actionsDisplayed = false;
    closeActionsOnClickOutside = false;
    showBanner = environment.showBanner;

    unReadDiscussions$?: Observable<boolean>;
    isChatRouteActive$: Observable<boolean>;
    unReadNotifications$?: Observable<boolean>;
    isUserModerated$: Observable<boolean | undefined>;
    isLogged$?: Observable<boolean>;

    notificationColor: 'kia-dark-blue' | 'neutral' = 'neutral';

    constructor(
        routerSelectors: RouterSelectors,
        private readonly homeService: HomeService,
        private readonly userProfileSelectors: UserProfileSelectors,
        private readonly userService: UserService,
        private readonly discussionsSelectors: DiscussionsSelectors,
        private readonly scrollStatesService: ScrollStatesService,
        private readonly notificationsSelectors: NotificationsSelectors,
        private readonly notificationsDispatchers: NotificationsDispatchers,
    ) {
        this.isChatRouteActive$ = routerSelectors.isChatRouteActive$.pipe(untilDestroyed(this));
        this.isUserModerated$ = userProfileSelectors.isModerated$.pipe(untilDestroyed(this));
        this.isLogged$ = this.userProfileSelectors.isLogged$.pipe(untilDestroyed(this));
    }

    ngOnInit(): void {
        this.unReadDiscussions$ = this.discussionsSelectors.discussions$.pipe(
            map(discussions => discussions.some(d => d.read === false)),
            untilDestroyed(this),
        );
        this.unReadNotifications$ = this.notificationsSelectors.isNotificationsRead$.pipe(
            map(read => read === false),
            untilDestroyed(this),
        );

        this.userProfile$ = this.userProfileSelectors.userProfile$.pipe(
            map(user => (user ? { ...user, avatarURL: this.userService.getAvatarUrl(user, true) } : undefined)),
            untilDestroyed(this),
        );
    }

    reload() {
        // reload homepage products
        this.homeService.setProducts(environment.homeDisplayedArticles.desktop);

        this.scrollStatesService.nextHome();
    }

    setNotificationRead() {
        this.notificationsDispatchers.setNotificationRead();
    }
}
