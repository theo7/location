import { Component } from '@angular/core';
import { UserProfileDispatchers } from '../../../store/services/user-dispatchers.service';

@Component({
    selector: 'app-vacation-mode-banner',
    templateUrl: './vacation-mode-banner.component.html',
    styleUrls: ['./vacation-mode-banner.component.scss'],
})
export class VacationModeBannerComponent {
    constructor(private readonly userProfileDispatchers: UserProfileDispatchers) {}

    unableVacationMode() {
        this.userProfileDispatchers.switchVacationMode();
    }
}
