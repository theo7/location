import { Component, HostListener } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { BehaviorSubject, Observable } from 'rxjs';
import { first } from 'rxjs/operators';
import { Category } from '../../../shared/models/models';
import { CategoriesSelectors } from '../../../store/services/categories-selectors.service';

@UntilDestroy()
@Component({
    selector: 'app-categories-desktop',
    templateUrl: './categories-desktop.component.html',
    styleUrls: ['./categories-desktop.component.scss'],
})
export class CategoriesDesktopComponent {
    state: 'expended' | 'collapsed' = 'expended';
    categories$: Observable<Category[]>;
    categoryDisplayed$ = new BehaviorSubject<Category | undefined>(undefined);

    @HostListener('window:scroll')
    scrollHandler() {
        this.state = window.scrollY ? 'collapsed' : 'expended';
    }

    constructor(private readonly categoriesSelectors: CategoriesSelectors) {
        this.categories$ = this.categoriesSelectors.indexedCategories$.pipe(untilDestroyed(this));
    }

    toggleCategory(category: Category) {
        this.categoryDisplayed$.pipe(first()).subscribe(currentCategory => {
            if (!currentCategory) {
                this.categoryDisplayed$.next(category);
            } else {
                this.closeCategory();
            }
        });
    }

    closeCategory() {
        this.categoryDisplayed$.next(undefined);
    }
}
