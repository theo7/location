import { Component } from '@angular/core';
import { environment } from '../../../environments/environment';
import { UserProfileSelectors } from '../../store/services/user-selectors.service';

@Component({
    selector: 'app-header-desktop',
    templateUrl: './header-desktop.component.html',
    styleUrls: ['./header-desktop.component.scss'],
})
export class HeaderDesktopComponent {
    showBanner = environment.showBanner;

    constructor(public readonly userProfileSelectors: UserProfileSelectors) {}
}
