import { Component } from '@angular/core';
import { environment } from '../../../../environments/environment';

@Component({
    selector: 'app-user-blocked-banner',
    templateUrl: './user-blocked-banner.component.html',
    styleUrls: ['./user-blocked-banner.component.scss'],
})
export class UserBlockedBannerComponent {
    contactEmail = environment.contactAddress;
    feedbackFormUrl = environment.feedbackFormUrl;
}
