import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { MatTooltipModule } from '@angular/material/tooltip';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { NotificationHistoryModule } from 'src/app/shared/components/notification-history/notification-history.module';
import { ProductFilterModule } from '../../shared/components/product-filter/product-filter.module';
import { DirectivesModule } from '../../shared/directives/directives.module';
import { PipesModule } from '../../shared/pipes/pipes.module';
import { SharedModule } from '../../shared/shared.module';
import { IconsModule } from '../icons/icons.module';
import { BetaBannerComponent } from './beta-banner/beta-banner.component';
import { CategoriesDesktopComponent } from './categories-desktop/categories-desktop.component';
import { HeaderDesktopComponent } from './header-desktop.component';
import { NavigationDesktopComponent } from './navigation-desktop/navigation-desktop.component';
import { NavigationSearchComponent } from './navigation-desktop/navigation-search/navigation-search.component';
import { UserBlockedBannerComponent } from './user-blocked-banner/user-blocked-banner.component';
import { VacationModeBannerComponent } from './vacation-mode-banner/vacation-mode-banner.component';

@NgModule({
    declarations: [
        HeaderDesktopComponent,
        CategoriesDesktopComponent,
        NavigationDesktopComponent,
        NavigationSearchComponent,
        BetaBannerComponent,
        UserBlockedBannerComponent,
        VacationModeBannerComponent,
    ],
    imports: [
        SharedModule,
        TranslateModule,
        RouterModule,
        ReactiveFormsModule,
        MatButtonModule,
        PipesModule,
        DirectivesModule,
        IconsModule,
        MatTooltipModule,
        MatMenuModule,
        ProductFilterModule,
        NotificationHistoryModule,
    ],
    exports: [HeaderDesktopComponent],
})
export class NavigationDesktopModule {}
