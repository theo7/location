export type ErrorLog = {
    key: string;
    error?: string;
    exception?: string;
    message?: string;
    status?: number;
    timestamp: number;
    url: string;
    stackTrace?: string;
    userAgent: string;
    userId?: number;
    firebaseId?: string;
    route?: string;
    version: string;
    language: string;
    windowSize: string;
};

export const getUniqueLogKey = (timestamp: number) => {
    return `location.logging.error.${timestamp}`;
};

export const sameError = (error1: any, error2: any) => {
    if (typeof error1 === typeof error2) {
        if (typeof error1 === 'object') {
            return (
                JSON.stringify(error1.name) === JSON.stringify(error2.name) &&
                JSON.stringify(error1.error) === JSON.stringify(error2.error) &&
                JSON.stringify(error1.message) === JSON.stringify(error2.message) &&
                JSON.stringify(error1.stack) === JSON.stringify(error2.stack)
            );
        }

        return error1 === error2;
    }

    return false;
};
