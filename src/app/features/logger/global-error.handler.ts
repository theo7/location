import { ErrorHandler, Injectable, Injector } from '@angular/core';
import { environment } from '../../../environments/environment';
import { LoggerService } from './logger.service';

const CHUNK_FAILED_MESSAGE = /Loading chunk .+ failed/i;

@Injectable()
export class GlobalErrorHandler implements ErrorHandler {
    constructor(private readonly injector: Injector) {}

    handleError(error: any): void {
        if (CHUNK_FAILED_MESSAGE.test(error.message)) {
            window.location.reload();
        }

        console.error(error);

        if (environment.logs.sendErrors) {
            const loggerService = this.injector.get(LoggerService);
            loggerService.logError(error);
        }
    }
}
