import { Injector } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { GoogleAnalyticsService } from '../analytics/google-analytics.service';
import { GlobalErrorHandler } from './global-error.handler';

jest.mock('src/environments/environment', () => ({
    get environment() {
        return {
            logs: {
                console: true,
                sendErrors: false,
            },
            production: true,
        };
    },
}));

describe('GlobalErrorHandler', () => {
    let globalErrorHandler: GlobalErrorHandler;

    const { location } = window;

    beforeEach(() => {
        Object.defineProperty(window, 'location', {
            value: {
                reload: jest.fn(),
            },
            writable: true,
        });
    });

    afterEach(() => {
        window.location = location;
    });

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [
                GlobalErrorHandler,
                {
                    provide: GoogleAnalyticsService,
                    useValue: {},
                },
                {
                    provide: Injector,
                    useValue: {},
                },
            ],
        });

        globalErrorHandler = TestBed.inject(GlobalErrorHandler);
    });

    const cases = [
        { message: 'Loading chunk 114 failed', shouldReload: true },
        { message: 'Loading chunk polyfills failed', shouldReload: true },
        { message: 'loading chunk polyfills failed', shouldReload: true },
        { message: 'loading chunk polyfills-web failed', shouldReload: true },
        { message: 'loading chunk 144', shouldReload: false },
        { message: 'chunk 144 failed', shouldReload: false },
        { message: 'polyfills failed', shouldReload: false },
        { message: 'loading failed', shouldReload: false },
    ];

    cases.forEach(({ message, shouldReload }) => {
        it(`should ${shouldReload ? '' : 'not'} reload if "${message}"`, () => {
            const error = {
                message,
            };

            globalErrorHandler.handleError(error);

            if (shouldReload) {
                expect(window.location.reload).toHaveBeenCalled();
            } else {
                expect(window.location.reload).not.toHaveBeenCalled();
            }
        });
    });
});
