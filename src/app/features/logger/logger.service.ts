import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { forkJoin } from 'rxjs';
import { filter, first } from 'rxjs/operators';
import { LoggingDispatchers } from 'src/app/store/services/logging-dispatchers.service';
import { RouterSelectors } from 'src/app/store/services/router-selectors.service';
import { UserProfileSelectors } from 'src/app/store/services/user-selectors.service';
import { environment } from 'src/environments/environment';
import { ErrorLog, getUniqueLogKey, sameError } from './logger.models';

const errorsApiUrl = `${environment.apiUrl}/errors`;

@Injectable({
    providedIn: 'root',
})
export class LoggerService {
    private lastError: any | undefined;

    constructor(
        private readonly http: HttpClient,
        private readonly userProfileSelectors: UserProfileSelectors,
        private readonly routerSelectors: RouterSelectors,
        private readonly loggingDispatchers: LoggingDispatchers,
    ) {}

    logError(error: any) {
        forkJoin([
            this.userProfileSelectors.userProfile$.pipe(first()),
            this.routerSelectors.currentRoute$.pipe(first()),
        ])
            .pipe(
                filter(() => !sameError(error, this.lastError)), // prevent duplicate error to be sent
            )
            .subscribe(([userProfile, currentRoute]) => {
                this.lastError = error;

                const timestamp = new Date().getTime();

                const key = getUniqueLogKey(timestamp);

                const errorLog: ErrorLog = {
                    key,
                    error: error.name,
                    exception: error.error && JSON.stringify(error.error),
                    message: error.message,
                    status: error.status,
                    timestamp,
                    url: window.location.href,
                    stackTrace: error.stack,
                    userAgent: navigator.userAgent,
                    userId: userProfile?.id,
                    firebaseId: userProfile?.firebaseId,
                    route: currentRoute && JSON.stringify(currentRoute),
                    version: environment.appVersion,
                    language: navigator.language,
                    windowSize: `${window.innerWidth}x${window.innerHeight}`,
                };

                // store error log in local storage
                this.loggingDispatchers.logError(errorLog);
            });
    }

    sendErrors(errors: ErrorLog[]) {
        return this.http.post(errorsApiUrl, { errors });
    }
}
