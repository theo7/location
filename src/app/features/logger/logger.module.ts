import { HttpClientModule } from '@angular/common/http';
import { ErrorHandler, NgModule } from '@angular/core';
import { AnalyticsModule } from '../analytics/analytics.module';
import { GlobalErrorHandler } from '../logger/global-error.handler';

@NgModule({
    declarations: [],
    imports: [HttpClientModule, AnalyticsModule],
    providers: [
        {
            provide: ErrorHandler,
            useClass: GlobalErrorHandler,
        },
    ],
})
export class LoggerModule {}
