import { getUniqueLogKey, sameError } from './logger.models';

describe('logger.models', () => {
    describe('getUniqueLogKey', () => {
        it('should be location.logging.error.2000 if current timestamp is 2000', () => {
            const result = getUniqueLogKey(2000);

            expect(result).toBe('location.logging.error.2000');
        });
    });

    describe('sameError', () => {
        it('should be false if the 2 errors do not have the same type', () => {
            const error1 = 'Error!';
            const error2 = {
                message: 'Another error...',
            };

            const result = sameError(error1, error2);

            expect(result).toBeFalsy();
        });

        it('should be true if the 2 errors are the same string', () => {
            const error1 = 'Error!';
            const error2 = 'Error!';

            const result = sameError(error1, error2);

            expect(result).toBeTruthy();
        });

        it('should be false if the 2 errors are different string', () => {
            const error1 = 'Error!';
            const error2 = 'Another error!';

            const result = sameError(error1, error2);

            expect(result).toBeFalsy();
        });

        it('should be true if the 2 errors have the same name, error, message and stack', () => {
            const error1 = {
                name: 'Error 404',
                error: 'Cannot find resource index.html',
                message: 'Cannot find resource index.html',
                stack: 'Failed to load resource. index.js:45:14',
            };
            const error2 = {
                name: 'Error 404',
                error: 'Cannot find resource index.html',
                message: 'Cannot find resource index.html',
                stack: 'Failed to load resource. index.js:45:14',
            };

            const result = sameError(error1, error2);

            expect(result).toBeTruthy();
        });

        it('should be false if the 2 errors does not have the same name', () => {
            const error1 = {
                name: 'Error 404',
                error: 'Cannot find resource index.html',
                message: 'Cannot find resource index.html',
                stack: 'Failed to load resource. index.js:45:14',
            };
            const error2 = {
                name: '',
                error: 'Cannot find resource index.html',
                message: 'Cannot find resource index.html',
                stack: 'Failed to load resource. index.js:45:14',
            };

            const result = sameError(error1, error2);

            expect(result).toBeFalsy();
        });

        it('should be false if the 2 errors does not have the same error', () => {
            const error1 = {
                name: 'Error 404',
                error: 'Cannot find resource index.html',
                message: 'Cannot find resource index.html',
                stack: 'Failed to load resource. index.js:45:14',
            };
            const error2 = {
                name: 'Error 404',
                error: '',
                message: 'Cannot find resource index.html',
                stack: 'Failed to load resource. index.js:45:14',
            };

            const result = sameError(error1, error2);

            expect(result).toBeFalsy();
        });

        it('should be false if the 2 errors does not have the same message', () => {
            const error1 = {
                name: 'Error 404',
                error: 'Cannot find resource index.html',
                message: 'Cannot find resource index.html',
                stack: 'Failed to load resource. index.js:45:14',
            };
            const error2 = {
                name: 'Error 404',
                error: 'Cannot find resource index.html',
                message: '',
                stack: 'Failed to load resource. index.js:45:14',
            };

            const result = sameError(error1, error2);

            expect(result).toBeFalsy();
        });

        it('should be false if the 2 errors does not have the same stack', () => {
            const error1 = {
                name: 'Error 404',
                error: 'Cannot find resource index.html',
                message: 'Cannot find resource index.html',
                stack: 'Failed to load resource. index.js:45:14',
            };
            const error2 = {
                name: 'Error 404',
                error: 'Cannot find resource index.html',
                message: 'Cannot find resource index.html',
                stack: '',
            };

            const result = sameError(error1, error2);

            expect(result).toBeFalsy();
        });
    });
});
