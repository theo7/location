import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { BreadcrumbComponent } from './breadcrumb/breadcrumb.component';
import { IsStableWarningComponent } from './is-stable-warning/is-stable-warning.component';

@NgModule({
    declarations: [IsStableWarningComponent, BreadcrumbComponent],
    imports: [CommonModule, RouterModule, TranslateModule],
    exports: [IsStableWarningComponent, BreadcrumbComponent],
})
export class KiaLayoutModule {}
