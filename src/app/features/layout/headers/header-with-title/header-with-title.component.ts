import { Component, EventEmitter, Input, Output } from '@angular/core';
import { NavigationHistoryService } from 'src/app/shared/services/navigation-history.service';

@Component({
    selector: 'app-header-with-title',
    templateUrl: './header-with-title.component.html',
    styleUrls: ['./header-with-title.component.scss'],
})
export class HeaderWithTitleComponent {
    @Input() title?: string;
    @Input() icon?: string;
    @Input() fakeBack? = false;
    @Input() previousRoute?: string;

    @Output() backClicked: EventEmitter<void> = new EventEmitter<void>();

    constructor(private readonly navigationHistoryService: NavigationHistoryService) {}

    back() {
        if (!this.fakeBack) {
            this.navigationHistoryService.back(this.previousRoute);
        } else {
            this.backClicked.emit();
        }
    }
}
