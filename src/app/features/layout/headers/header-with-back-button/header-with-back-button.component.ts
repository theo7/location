import { Component, EventEmitter, Input, Output } from '@angular/core';
import { NavigationHistoryService } from 'src/app/shared/services/navigation-history.service';

@Component({
    selector: 'app-header-with-back-button',
    templateUrl: './header-with-back-button.component.html',
    styleUrls: ['./header-with-back-button.component.scss'],
})
export class HeaderWithBackButtonComponent {
    @Output() backClicked = new EventEmitter<void>();

    @Input() useBack = true;

    constructor(private navigationHistoryService: NavigationHistoryService) {}

    back() {
        if (this.useBack) {
            this.navigationHistoryService.back();
        } else {
            this.backClicked.emit();
        }
    }
}
