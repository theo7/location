import { Component, Input, OnChanges, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Discussion, UserProfile, UserProfileStatus } from 'src/app/shared/models/models';
import { NavigationHistoryService } from 'src/app/shared/services/navigation-history.service';
import { UserService } from 'src/app/shared/services/user.service';
import { LotDispatchers } from 'src/app/store/services/lot-dispatchers.service';
import { RouterSelectors } from 'src/app/store/services/router-selectors.service';

@UntilDestroy()
@Component({
    selector: 'app-header-with-profil',
    templateUrl: './header-with-profil.component.html',
    styleUrls: ['./header-with-profil.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class HeaderWithProfilComponent implements OnChanges {
    @Input() user?: UserProfile;
    @Input() discussion?: Discussion | null;
    @Input() previousRoute?: string;

    avatarURL?: string;
    nickname?: string;

    displayMoreButton$: Observable<boolean>;

    constructor(
        private readonly router: Router,
        private readonly navigationHistoryService: NavigationHistoryService,
        private readonly userService: UserService,
        private readonly lotDispatchers: LotDispatchers,
        private readonly translateService: TranslateService,
        routerSelectors: RouterSelectors,
    ) {
        this.displayMoreButton$ = routerSelectors.isChatRoomActive$.pipe(
            map(isChatRoomActive => isChatRoomActive && !!this.user?.id),
            untilDestroyed(this),
        );
    }

    ngOnChanges() {
        if (this.user) {
            this.avatarURL = this.userService.getAvatarUrl(this.user);

            if (this.user.status === UserProfileStatus.BLOCKED) {
                this.nickname = this.translateService.instant('chat.blockedUser.title');
            } else {
                this.nickname = this.user.nickname;
            }
        }
    }

    back() {
        this.navigationHistoryService.back(this.previousRoute);
    }

    navigateToUserProfile() {
        if (!!this.user?.id) {
            this.router.navigate(['profile', this.user.id]);
        }
    }

    onMoreButtonClicked() {
        if (this.discussion && !!this.user?.id) {
            this.lotDispatchers.openMoreActionsModal(this.discussion, this.user.id);
        }
    }
}
