import { Component } from '@angular/core';
import { Reservation } from 'src/app/shared/models/models';
import { ScrollStatesService } from 'src/app/shared/services/scroll-states.service';
import { environment } from 'src/environments/environment';

@Component({
    selector: 'app-main-header',
    templateUrl: './main-header.component.html',
    styleUrls: ['./main-header.component.scss'],
})
export class MainHeaderComponent {
    reservation?: Reservation;
    showBanner = environment.showBanner;

    constructor(private readonly scrollStatesService: ScrollStatesService) {}

    onHomeClicked() {
        this.scrollStatesService.nextHome();
    }
}
