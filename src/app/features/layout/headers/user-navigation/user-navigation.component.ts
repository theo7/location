import { Component, HostListener } from '@angular/core';
import { NavigationHistoryService } from '../../../../shared/services/navigation-history.service';

@Component({
    selector: 'app-user-navigation',
    templateUrl: './user-navigation.component.html',
    styleUrls: ['./user-navigation.component.scss'],
})
export class UserNavigationComponent {
    state: 'expended' | 'collapsed' = 'expended';

    constructor(private navigationHistoryService: NavigationHistoryService) {}

    @HostListener('window:scroll')
    scrollHandler() {
        this.state = window.scrollY ? 'collapsed' : 'expended';
    }

    goBack() {
        this.navigationHistoryService.back();
    }
}
