import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { AppCoreModule } from 'src/app/shared/components/app-core/app-core.module';
import { RatingModule } from 'src/app/shared/components/user-rating/rating.module';
import { DirectivesModule } from 'src/app/shared/directives/directives.module';
import { PipesModule } from 'src/app/shared/pipes/pipes.module';
import { ChipModule } from '../../../shared/components/chip/chip.module';
import { HeaderWithBackButtonComponent } from './header-with-back-button/header-with-back-button.component';
import { HeaderWithProfilComponent } from './header-with-profil/header-with-profil.component';
import { HeaderWithTitleComponent } from './header-with-title/header-with-title.component';
import { LogoComponent } from './logo/logo.component';
import { MainHeaderComponent } from './main-header/main-header.component';
import { UserNavigationComponent } from './user-navigation/user-navigation.component';

@NgModule({
    declarations: [
        HeaderWithBackButtonComponent,
        HeaderWithProfilComponent,
        HeaderWithTitleComponent,
        MainHeaderComponent,
        UserNavigationComponent,
        LogoComponent,
    ],
    imports: [
        CommonModule,
        TranslateModule,
        MatIconModule,
        MatToolbarModule,
        MatButtonModule,
        RouterModule,
        RatingModule,
        DirectivesModule,
        AppCoreModule,
        PipesModule,
        ChipModule,
    ],
    exports: [
        HeaderWithBackButtonComponent,
        HeaderWithProfilComponent,
        HeaderWithTitleComponent,
        MainHeaderComponent,
        UserNavigationComponent,
    ],
})
export class HeaderModule {}
