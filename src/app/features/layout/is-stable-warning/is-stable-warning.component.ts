import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
    selector: 'app-is-stable-warning',
    templateUrl: './is-stable-warning.component.html',
    styleUrls: ['./is-stable-warning.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IsStableWarningComponent {}
