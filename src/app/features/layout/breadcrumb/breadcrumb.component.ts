import { ChangeDetectionStrategy, Component } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Observable } from 'rxjs';
import { BreadcrumbLink } from 'src/app/shared/models/models';
import { RouterSelectors } from 'src/app/store/services/router-selectors.service';

@UntilDestroy()
@Component({
    selector: 'app-breadcrumb',
    templateUrl: './breadcrumb.component.html',
    styleUrls: ['./breadcrumb.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BreadcrumbComponent {
    breadcrumbLinks$: Observable<BreadcrumbLink[]>;

    constructor(routerSelectors: RouterSelectors) {
        this.breadcrumbLinks$ = routerSelectors.breadcrumbLinks$.pipe(untilDestroyed(this));
    }
}
