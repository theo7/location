import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
    selector: 'app-notification-icon',
    templateUrl: './notification-icon.component.html',
    styleUrls: ['./notification-icon.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NotificationIconComponent {
    @Input()
    size: number = 24;

    @Input()
    color: 'kia-dark-blue' | 'neutral' = 'neutral';
}
