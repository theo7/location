import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
    selector: 'app-summer-icon',
    templateUrl: './summer-icon.component.html',
    styleUrls: ['./summer-icon.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SummerIconComponent {
    @Input() width = 50;
    @Input() height = 50;
    @Input() color = '#040037';
}
