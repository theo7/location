import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
    selector: 'app-p2p-icon',
    templateUrl: './p2p-icon.component.html',
    styleUrls: ['./p2p-icon.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class P2pIconComponent {}
