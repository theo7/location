import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
    selector: 'app-free-return-icon',
    templateUrl: './free-return-icon.component.html',
    styleUrls: ['./free-return-icon.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FreeReturnIconComponent {}
