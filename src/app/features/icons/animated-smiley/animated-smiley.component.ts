import { Component } from '@angular/core';

@Component({
    selector: 'app-animated-smiley',
    templateUrl: './animated-smiley.component.html',
    styleUrls: ['./animated-smiley.component.scss'],
})
export class AnimatedSmileyComponent {}
