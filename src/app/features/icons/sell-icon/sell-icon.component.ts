import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
    selector: 'app-sell-icon',
    templateUrl: './sell-icon.component.html',
    styleUrls: ['./sell-icon.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SellIconComponent {
    @Input() active = false;
}
