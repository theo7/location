import { Component, Input } from '@angular/core';

@Component({
    selector: 'app-warning-icon',
    templateUrl: './warning-icon.component.html',
    styleUrls: ['./warning-icon.component.scss'],
})
export class WarningIconComponent {
    @Input() width = 50;
    @Input() height = 50;
    @Input() color = '#FFFFFF';
}
