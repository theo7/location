import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
    selector: 'app-guide-icon',
    templateUrl: './guide-icon.component.html',
    styleUrls: ['./guide-icon.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GuideIconComponent {
    @Input() width = 83;
    @Input() height = 64;
}
