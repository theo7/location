import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
    selector: 'app-take-photo-icon',
    templateUrl: './take-photo-icon.component.html',
    styleUrls: ['./take-photo-icon.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TakePhotoIconComponent {
    @Input() width = 30;
    @Input() height = 25;
    @Input() color = '#fff';
}
