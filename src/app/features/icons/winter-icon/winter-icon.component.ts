import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
    selector: 'app-winter-icon',
    templateUrl: './winter-icon.component.html',
    styleUrls: ['./winter-icon.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WinterIconComponent {
    @Input() width = 50;
    @Input() height = 50;
    @Input() color = '#040037';
}
