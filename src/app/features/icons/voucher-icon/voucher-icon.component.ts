import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
    selector: 'app-voucher-icon',
    templateUrl: './voucher-icon.component.html',
    styleUrls: ['./voucher-icon.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class VoucherIconComponent {}
