import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
    selector: 'app-animated-hanger',
    templateUrl: './animated-hanger.component.html',
    styleUrls: ['./animated-hanger.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AnimatedHangerComponent {}
