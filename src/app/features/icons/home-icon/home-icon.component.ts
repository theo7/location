import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
    selector: 'app-home-icon',
    templateUrl: './home-icon.component.html',
    styleUrls: ['./home-icon.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomeIconComponent {
    @Input() active = false;
}
