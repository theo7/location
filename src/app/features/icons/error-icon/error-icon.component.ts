import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
    selector: 'app-error-icon',
    templateUrl: './error-icon.component.html',
    styleUrls: ['./error-icon.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ErrorIconComponent {
    @Input() width = 50;
    @Input() height = 50;
}
