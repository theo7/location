import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
    selector: 'app-money-out-icon',
    templateUrl: './money-out-icon.component.html',
    styleUrls: ['./money-out-icon.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MoneyOutIconComponent {}
