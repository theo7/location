import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
    selector: 'app-kiabi-icon',
    templateUrl: './kiabi-icon.component.html',
    styleUrls: ['./kiabi-icon.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class KiabiIconComponent {
    @Input() color = 'white';
    @Input() width = 40;
    @Input() height = 8;
}
