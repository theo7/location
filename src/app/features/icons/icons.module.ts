import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AllSeasonalityIconComponent } from './all-seasonality-icon/all-seasonality-icon.component';
import { AnimatedHangerComponent } from './animated-hanger/animated-hanger.component';
import { AnimatedSmileyComponent } from './animated-smiley/animated-smiley.component';
import { ArrowRightIconComponent } from './arrow-right-icon/arrow-right-icon.component';
import { ChatIconComponent } from './chat-icon/chat-icon.component';
import { DeliveryIconComponent } from './delivery-icon/delivery-icon.component';
import { ErrorIconComponent } from './error-icon/error-icon.component';
import { FranceFlagIconComponent } from './france-flag-icon/france-flag-icon.component';
import { FreeReturnIconComponent } from './free-return-icon/free-return-icon.component';
import { GuideIconComponent } from './guide-icon/guide-icon.component';
import { HeartIconComponent } from './heart-icon/heart-icon.component';
import { HomeIconComponent } from './home-icon/home-icon.component';
import { HowItWorksIconComponent } from './how-it-works-icon/how-it-works-icon.component';
import { KiabiIconComponent } from './kiabi-icon/kiabi-icon.component';
import { MoneyOutIconComponent } from './money-out-icon/money-out-icon.component';
import { NotificationIconComponent } from './notification-icon/notification-icon.component';
import { P2pIconComponent } from './p2p-icon/p2p-icon.component';
import { QaIconComponent } from './qa-icon/qa-icon.component';
import { QualityIconComponent } from './quality-icon/quality-icon.component';
import { SaleValidateIconComponent } from './sale-valildate-icon/sale-validate.component';
import { SearchIconComponent } from './search-icon/search-icon.component';
import { SellIconComponent } from './sell-icon/sell-icon.component';
import { SellShirtIconComponent } from './sell-shirt-icon/sell-shirt-icon.component';
import { SuccessIconComponent } from './success-icon/success-icon.component';
import { SummerIconComponent } from './summer-icon/summer-icon.component';
import { TakePhotoIconComponent } from './take-photo-icon/take-photo-icon.component';
import { VoucherIconComponent } from './voucher-icon/voucher-icon.component';
import { WarningIconComponent } from './warning-icon/warning-icon.component';
import { WinterIconComponent } from './winter-icon/winter-icon.component';

@NgModule({
    declarations: [
        ChatIconComponent,
        SellIconComponent,
        HeartIconComponent,
        GuideIconComponent,
        ArrowRightIconComponent,
        SearchIconComponent,
        HomeIconComponent,
        AnimatedHangerComponent,
        SellShirtIconComponent,
        FranceFlagIconComponent,
        ErrorIconComponent,
        VoucherIconComponent,
        KiabiIconComponent,
        SuccessIconComponent,
        WarningIconComponent,
        SummerIconComponent,
        WinterIconComponent,
        AllSeasonalityIconComponent,
        SaleValidateIconComponent,
        QualityIconComponent,
        DeliveryIconComponent,
        FreeReturnIconComponent,
        AnimatedSmileyComponent,
        MoneyOutIconComponent,
        P2pIconComponent,
        HowItWorksIconComponent,
        QaIconComponent,
        TakePhotoIconComponent,
        NotificationIconComponent,
    ],
    imports: [CommonModule],
    exports: [
        ChatIconComponent,
        SellIconComponent,
        HeartIconComponent,
        GuideIconComponent,
        ArrowRightIconComponent,
        SearchIconComponent,
        HomeIconComponent,
        AnimatedHangerComponent,
        SellShirtIconComponent,
        FranceFlagIconComponent,
        ErrorIconComponent,
        VoucherIconComponent,
        KiabiIconComponent,
        SuccessIconComponent,
        WarningIconComponent,
        SummerIconComponent,
        WinterIconComponent,
        AllSeasonalityIconComponent,
        SaleValidateIconComponent,
        QualityIconComponent,
        DeliveryIconComponent,
        FreeReturnIconComponent,
        MoneyOutIconComponent,
        P2pIconComponent,
        AnimatedSmileyComponent,
        HowItWorksIconComponent,
        QaIconComponent,
        TakePhotoIconComponent,
        NotificationIconComponent,
    ],
})
export class IconsModule {}
