import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
    selector: 'app-all-seasonality-icon',
    templateUrl: './all-seasonality-icon.component.html',
    styleUrls: ['./all-seasonality-icon.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AllSeasonalityIconComponent {
    @Input() width = 50;
    @Input() height = 50;
    @Input() color = '#040037';
}
