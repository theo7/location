import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
    selector: 'app-qa-icon',
    templateUrl: './qa-icon.component.html',
    styleUrls: ['./qa-icon.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class QaIconComponent {
    @Input() width = 35;
    @Input() height = 35;
}
