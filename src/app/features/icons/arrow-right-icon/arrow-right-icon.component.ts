import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
    selector: 'app-arrow-right-icon',
    templateUrl: './arrow-right-icon.component.html',
    styleUrls: ['./arrow-right-icon.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ArrowRightIconComponent {}
