import { Component, Input } from '@angular/core';

@Component({
    selector: 'app-success-icon',
    templateUrl: './success-icon.component.html',
    styleUrls: ['./success-icon.component.scss'],
})
export class SuccessIconComponent {
    @Input() width = 50;
    @Input() height = 50;
}
