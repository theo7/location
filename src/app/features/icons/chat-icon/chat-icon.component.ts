import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
    selector: 'app-chat-icon',
    templateUrl: './chat-icon.component.html',
    styleUrls: ['./chat-icon.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ChatIconComponent {
    @Input() active = false;
}
