import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
    selector: 'app-heart-icon',
    templateUrl: './heart-icon.component.html',
    styleUrls: ['./heart-icon.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeartIconComponent {
    @Input() active: boolean | null = false;
    @Input() color: 'primary' | 'secondary' = 'primary';
}
