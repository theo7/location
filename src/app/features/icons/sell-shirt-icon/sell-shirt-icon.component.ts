import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
    selector: 'app-sell-shirt-icon',
    templateUrl: './sell-shirt-icon.component.html',
    styleUrls: ['./sell-shirt-icon.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SellShirtIconComponent {}
