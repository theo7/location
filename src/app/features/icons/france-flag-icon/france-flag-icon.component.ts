import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
    selector: 'app-france-flag-icon',
    templateUrl: './france-flag-icon.component.svg',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FranceFlagIconComponent {}
