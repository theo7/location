import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
    selector: 'app-how-it-works-icon',
    templateUrl: './how-it-works-icon.component.html',
    styleUrls: ['./how-it-works-icon.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HowItWorksIconComponent {
    @Input() width = 83;
    @Input() height = 64;
}
