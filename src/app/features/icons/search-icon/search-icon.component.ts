import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
    selector: 'app-search-icon',
    templateUrl: './search-icon.component.html',
    styleUrls: ['./search-icon.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SearchIconComponent {
    @Input() active = false;
}
