import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
    selector: 'app-delivery-icon',
    templateUrl: './delivery-icon.component.html',
    styleUrls: ['./delivery-icon.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DeliveryIconComponent {}
