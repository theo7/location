import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
    selector: 'app-sale-validate-icon',
    templateUrl: './sale-validate.component.html',
    styleUrls: ['./sale-validate.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SaleValidateIconComponent {}
