import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable, Injector } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Observable, throwError } from 'rxjs';
import { catchError, tap, withLatestFrom } from 'rxjs/operators';
import { SimpleDialogComponent } from 'src/app/shared/components/simple-dialog/simple-dialog.component';
import { JsonError } from 'src/app/shared/models/models';
import { environment } from 'src/environments/environment';
import { ErrorNotificationDialogComponent } from '../../shared/components/error-notification-dialog/error-notification-dialog.component';
import { isUpdateRequired } from '../../shared/functions/versions.functions';
import { HttpStatus } from '../../shared/helpers/http-status.enum';
import { HealthService } from '../../shared/services/health.service';
import { RequirementsService } from '../../shared/services/requirements.service';
import { UserProfileDispatchers } from '../../store/services/user-dispatchers.service';
import { ErrorNotificationComponent } from './dialog/error-notification.component';
import { ErrorToastrComponent } from './toastr/error-toastr/error-toastr.component';

const UNHANDLED_MESSAGES = [
    'PAYMENT_017',
    'USER_019',
    'USER_021',
    'FIREBASE_007',
    'PRODUCT_025',
    'USER_022',
    'PICTURE_003',
    'PICTURE_004',
    'DISCUSSION_037',
    'VOUCHER_002',
    'DISCUSSION_038',
];
const UNHANDLED_ORIGINS = ['https://api64.ipify.org'];
const TRANSPORT_MESSAGES = ['TRANSPORT_002', 'TRANSPORT_003', 'TRANSPORT_004'];
const UNHANDLED_ROUTES = ['actuator', '/users/store-credit/redeem-voucher', '/products/price-average'];

const shouldInterceptError = (err: any): boolean => {
    return (
        err instanceof HttpErrorResponse &&
        UNHANDLED_ROUTES.every(route => !err.url?.includes(route)) &&
        UNHANDLED_ORIGINS.every(origin => !err.url?.startsWith(origin))
    );
};

@Injectable()
export class ErrorNotificationInterceptor implements HttpInterceptor {
    constructor(
        private readonly injector: Injector,
        private readonly router: Router,
        private readonly dialog: MatDialog,
        private readonly userProfileDispatchers: UserProfileDispatchers,
        private readonly healthService: HealthService,
        private readonly requirementsService: RequirementsService,
    ) {}

    intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
        const translateService = this.injector.get(TranslateService);

        return next.handle(request).pipe(
            catchError(err => {
                if (shouldInterceptError(err)) {
                    const message = (err.error as JsonError)?.message;

                    this.healthService
                        .healthCheck()
                        .pipe(
                            catchError(error => {
                                this.router.navigate(['maintenance']);
                                return throwError(error);
                            }),
                            withLatestFrom(this.requirementsService.frontVersion()),
                        )
                        .subscribe(
                            ([health, requirements]) => {
                                if (isUpdateRequired(requirements.frontVersion, environment.appVersion)) {
                                    return this.dialog
                                        .open(SimpleDialogComponent, {
                                            data: {
                                                title: translateService.instant('update.newUpdate'),
                                                action: translateService.instant('update.newUpdateAction'),
                                            },
                                            disableClose: true,
                                        })
                                        .afterClosed()
                                        .pipe(tap(() => document.location.reload()));
                                }

                                if (health.status !== 'DOWN') {
                                    if (err.status === HttpStatus.NOT_FOUND) {
                                        return throwError(err);
                                    }

                                    if (err.status === HttpStatus.UNAUTHORIZED) {
                                        this.userProfileDispatchers.logout();
                                        this.router.navigate(['login']);
                                        return throwError(err);
                                    }

                                    if (UNHANDLED_MESSAGES.includes(message)) {
                                        return throwError(err);
                                    }

                                    if (TRANSPORT_MESSAGES.includes(message)) {
                                        this.dialog.open(ErrorNotificationDialogComponent, {
                                            data: {
                                                title: translateService.instant('error-notification.title'),
                                                content: translateService.instant(`error.${message}`),
                                                buttonContent: translateService.instant('common.ok'),
                                            },
                                        });
                                        return throwError(err);
                                    }

                                    if (environment.errorNotificationLevel === 'DETAILED') {
                                        const translation = translateService.instant(`error.${message}`, {
                                            nullable: true,
                                        });

                                        if (!!translation) {
                                            this.injector.get(MatSnackBar).openFromComponent(ErrorToastrComponent, {
                                                verticalPosition: 'top',
                                                horizontalPosition: 'end',
                                                duration: 6000,
                                                data: {
                                                    message: translation,
                                                },
                                            });
                                        } else {
                                            this.dialog.open(ErrorNotificationComponent, {
                                                data: {
                                                    error: err,
                                                },
                                            });
                                        }
                                    }

                                    if (environment.errorNotificationLevel === 'SIMPLIFIED') {
                                        const notificationContent =
                                            message && translateService.instant(`error.${message}`, { nullable: true });
                                        const defaultContent = translateService.instant(`error.DEFAULT`);

                                        this.dialog.open(ErrorNotificationDialogComponent, {
                                            data: {
                                                title: translateService.instant('error-notification.title'),
                                                content: notificationContent || defaultContent,
                                                buttonContent: translateService.instant('common.ok'),
                                            },
                                        });
                                    }
                                }
                            },
                            () => this.router.navigate(['maintenance']),
                        );
                }
                return throwError(err);
            }),
        );
    }
}
