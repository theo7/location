import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
    templateUrl: './error-notification.component.html',
    styleUrls: ['./error-notification.component.scss'],
})
export class ErrorNotificationComponent {
    constructor(@Inject(MAT_DIALOG_DATA) public data: { error: any }) {}
}
