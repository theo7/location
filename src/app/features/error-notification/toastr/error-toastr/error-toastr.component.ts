import { Component, Inject } from '@angular/core';
import { MAT_SNACK_BAR_DATA } from '@angular/material/snack-bar';

@Component({
    selector: 'app-error-toastr',
    templateUrl: './error-toastr.component.html',
    styleUrls: ['./error-toastr.component.scss'],
})
export class ErrorToastrComponent {
    constructor(@Inject(MAT_SNACK_BAR_DATA) public data: any) {}
}
