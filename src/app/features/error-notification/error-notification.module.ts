import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { ButtonModule } from '../../shared/components/button/button.module';
import { SharedModule } from '../../shared/shared.module';
import { BlobErrorInterceptor } from './blob-error.interceptor';
import { ErrorNotificationComponent } from './dialog/error-notification.component';
import { ErrorNotificationInterceptor } from './error-notification.interceptor';
import { ErrorToastrComponent } from './toastr/error-toastr/error-toastr.component';

@NgModule({
    declarations: [ErrorNotificationComponent, ErrorToastrComponent],
    imports: [SharedModule, HttpClientModule, MatSnackBarModule, ButtonModule],
    providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: ErrorNotificationInterceptor,
            multi: true,
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: BlobErrorInterceptor,
            multi: true,
        },
    ],
})
export class ErrorNotificationModule {}
