import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable()
export class BlobErrorInterceptor implements HttpInterceptor {
    // 📝 used for convert response type blob request error to JSON
    // angular known issue see : https://github.com/angular/angular/issues/19888
    intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
        return next.handle(request).pipe(
            catchError(err => {
                if (
                    err instanceof HttpErrorResponse &&
                    err.error instanceof Blob &&
                    err.error.type === 'application/json'
                ) {
                    return new Observable<any>(observer => {
                        const reader = new FileReader();
                        reader.onload = e => {
                            try {
                                const message = JSON.parse(e.target?.result as string);
                                observer.error(
                                    new HttpErrorResponse({
                                        ...err,
                                        error: message,
                                    } as any),
                                );
                            } catch (exception) {
                                observer.error(err);
                            }
                        };
                        reader.onerror = e => observer.error(err);
                        reader.readAsText(err.error);

                        return () => reader.abort();
                    });
                }

                return throwError(err);
            }),
        );
    }
}
