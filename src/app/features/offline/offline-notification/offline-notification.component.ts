import { Component } from '@angular/core';

@Component({
    selector: 'app-offline-notification',
    templateUrl: './offline-notification.component.html',
    styleUrls: ['./offline-notification.component.scss'],
})
export class OfflineNotificationComponent {}
