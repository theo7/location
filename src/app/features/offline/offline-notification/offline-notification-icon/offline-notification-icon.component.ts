import { Component } from '@angular/core';

@Component({
    selector: 'app-offline-notification-icon',
    templateUrl: './offline-notification-icon.component.html',
})
export class OfflineNotificationIconComponent {}
