import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { OfflineNotificationComponent } from './offline-notification/offline-notification.component';

@Injectable({
    providedIn: 'root',
})
export class OfflineNotificationService {
    private notificationOpened = false;

    constructor(private dialog: MatDialog) {}

    public displayNotification() {
        if (!this.notificationOpened) {
            this.notificationOpened = true;
            this.dialog
                .open(OfflineNotificationComponent)
                .afterClosed()
                .subscribe(() => (this.notificationOpened = false));
        }
    }
}
