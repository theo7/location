import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, first } from 'rxjs/operators';
import { ConnectionSelectors } from '../../store/services/connection-selectors.service';
import { OfflineNotificationService } from './offline-notification.service';

@Injectable()
export class OfflineInterceptor implements HttpInterceptor {
    constructor(
        private offlineNotificationService: OfflineNotificationService,
        private connectionSelectors: ConnectionSelectors,
    ) {}

    intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
        return next.handle(request).pipe(
            catchError(err => {
                this.connectionSelectors.isOffline$.pipe(first()).subscribe(isOffline => {
                    if (isOffline) {
                        this.offlineNotificationService.displayNotification();
                    }
                });

                return throwError(err);
            }),
        );
    }
}
