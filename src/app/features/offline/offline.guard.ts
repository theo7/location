import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild, CanLoad } from '@angular/router';
import { Observable } from 'rxjs';
import { first, tap } from 'rxjs/operators';
import { ConnectionSelectors } from '../../store/services/connection-selectors.service';
import { OfflineNotificationService } from './offline-notification.service';

@Injectable({
    providedIn: 'root',
})
export class OfflineGuard implements CanLoad, CanActivate, CanActivateChild {
    constructor(
        private readonly offlineNotificationService: OfflineNotificationService,
        private readonly connectionSelectors: ConnectionSelectors,
    ) {}

    canLoad() {
        return this.process();
    }

    canActivate(): Observable<boolean> {
        return this.process();
    }

    canActivateChild(): Observable<boolean> {
        return this.process();
    }

    private process(): Observable<boolean> {
        return this.connectionSelectors.isOnline$.pipe(
            first(),
            tap(isOnline => {
                if (!isOnline) {
                    this.offlineNotificationService.displayNotification();
                }
            }),
        );
    }
}
