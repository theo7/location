import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { MatDialogModule } from '@angular/material/dialog';
import { TranslateModule } from '@ngx-translate/core';
import { ButtonModule } from '../../shared/components/button/button.module';
import { OfflineNotificationIconComponent } from './offline-notification/offline-notification-icon/offline-notification-icon.component';
import { OfflineNotificationComponent } from './offline-notification/offline-notification.component';
import { OfflineInterceptor } from './offline.interceptor';

@NgModule({
    declarations: [OfflineNotificationComponent, OfflineNotificationIconComponent],
    imports: [HttpClientModule, ButtonModule, TranslateModule, MatDialogModule],
    exports: [],
    providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: OfflineInterceptor,
            multi: true,
        },
    ],
})
export class OfflineModule {}
