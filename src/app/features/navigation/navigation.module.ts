import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AvatarModule } from '../../shared/components/avatar/avatar.module';
import { DirectivesModule } from '../../shared/directives/directives.module';
import { PipesModule } from '../../shared/pipes/pipes.module';
import { SharedModule } from '../../shared/shared.module';
import { IconsModule } from '../icons/icons.module';
import { NavigationComponent } from './navigation.component';

@NgModule({
    declarations: [NavigationComponent],
    imports: [SharedModule, RouterModule, PipesModule, AvatarModule, IconsModule, DirectivesModule],
    exports: [NavigationComponent],
})
export class NavigationModule {}
