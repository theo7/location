import { Component, OnInit } from '@angular/core';
import { Params } from '@angular/router';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { UserProfile, UserProfileStatus } from 'src/app/shared/models/models';
import { ScrollStatesService } from 'src/app/shared/services/scroll-states.service';
import { DiscussionsSelectors } from 'src/app/store/services/discussions-selectors.service';
import { NotificationsSelectors } from 'src/app/store/services/notifications-selectors.service';
import { UserProfileSelectors } from 'src/app/store/services/user-selectors.service';
import { FilterSelectors } from '../../store/services/filter-selectors.service';
import { SearchDispatchers } from '../../store/services/search-dispatchers.service';
import { UserProfileDispatchers } from '../../store/services/user-dispatchers.service';

@UntilDestroy()
@Component({
    selector: 'app-navigation',
    templateUrl: './navigation.component.html',
    styleUrls: ['./navigation.component.scss'],
})
export class NavigationComponent implements OnInit {
    user?: UserProfile;
    unReadDiscussions$?: Observable<boolean>;
    unReadNotifications$?: Observable<boolean>;
    filter$: Observable<Params>;
    isLogged$: Observable<boolean>;
    isUserModerated$: Observable<boolean | undefined>;

    userProfileStatus = UserProfileStatus;

    constructor(
        private readonly userProfileSelectors: UserProfileSelectors,
        private readonly discussionsSelectors: DiscussionsSelectors,
        private readonly scrollStatesService: ScrollStatesService,
        private readonly userProfileDispatchers: UserProfileDispatchers,
        private readonly notificationsSelectors: NotificationsSelectors,
        private readonly searchDispatchers: SearchDispatchers,
        private readonly filterSelectors: FilterSelectors,
    ) {
        this.isLogged$ = userProfileSelectors.isLogged$.pipe(untilDestroyed(this));
        this.isUserModerated$ = userProfileSelectors.isModerated$.pipe(untilDestroyed(this));
        this.userProfileSelectors.userProfile$.pipe(untilDestroyed(this)).subscribe(user => {
            this.user = user;
        });
        this.filter$ = this.filterSelectors.currentRouterParams$('search').pipe(untilDestroyed(this));
    }

    ngOnInit(): void {
        this.unReadDiscussions$ = this.discussionsSelectors.discussions$.pipe(
            map(discussions => discussions.some(d => d.read === false)),
            untilDestroyed(this),
        );
        this.unReadNotifications$ = this.notificationsSelectors.isNotificationsRead$.pipe(
            map(read => read === false),
            untilDestroyed(this),
        );
    }

    onHomeClicked(): void {
        this.scrollStatesService.nextHome();
    }

    onSearchClicked(): void {
        this.searchDispatchers.setShowFilter(true);
    }

    unableVacationMode(): void {
        this.userProfileDispatchers.switchVacationMode();
    }
}
