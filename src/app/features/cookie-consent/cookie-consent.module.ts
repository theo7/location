import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { AppCoreModule } from 'src/app/shared/components/app-core/app-core.module';
import { ButtonModule } from 'src/app/shared/components/button/button.module';
import { CookieConsentPanelComponent } from './components/cookie-consent-panel/cookie-consent-panel.component';

@NgModule({
    declarations: [CookieConsentPanelComponent],
    imports: [CommonModule, RouterModule, TranslateModule, AppCoreModule, ButtonModule],
    exports: [CookieConsentPanelComponent],
})
export class CookieConsentModule {}
