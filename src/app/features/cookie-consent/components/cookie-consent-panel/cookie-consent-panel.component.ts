import { ChangeDetectionStrategy, Component } from '@angular/core';
import { CookiesDispatchers } from 'src/app/store/services/cookies-dispatchers.service';

@Component({
    selector: 'app-cookie-consent-panel',
    templateUrl: './cookie-consent-panel.component.html',
    styleUrls: ['./cookie-consent-panel.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CookieConsentPanelComponent {
    constructor(private readonly cookiesDispatchers: CookiesDispatchers) {}

    onAcceptButtonClicked() {
        this.cookiesDispatchers.acceptAll();
    }

    onConfigureButtonClicked() {
        this.cookiesDispatchers.openConfigureModal();
    }

    onContinueWithoutAcceptButtonClicked() {
        this.cookiesDispatchers.denyAll();
    }
}
