import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { BehaviorSubject, combineLatest, Observable, of, Subject } from 'rxjs';
import { distinctUntilChanged, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

const COOKIE_CONSENT_STATUS_KEY = 'cookieconsent_status';
const COOKIE_CONSENT_SELECTION_KEY = 'cookieconsent_selection';

export type CookieConsentStatus = 'allow' | 'dismiss' | 'deny' | undefined;
export type CookieConsentFeature = 'ads' | 'service' | 'performance' | 'technical';
export type CookieConsentSelection = Record<CookieConsentFeature, boolean>;

const adsCookies = ['_gat_secondemain']; // Google Adwords + Havas + Facebook
const servicesCookies = []; // Rien pour le moment
const performanceCookies = ['_gid', '_ga']; // Google Analytics

const cookiePath = '/';

@Injectable({
    providedIn: 'root',
})
export class CookieConsentService {
    private statusSubject$: Subject<CookieConsentStatus>;
    private selectionSubject$: Subject<CookieConsentSelection | undefined>;

    status$: Observable<CookieConsentStatus>;
    selection$: Observable<CookieConsentSelection | undefined>;
    hasChosen$: Observable<boolean>;

    constructor(private readonly cookieService: CookieService) {
        this.statusSubject$ = new BehaviorSubject<CookieConsentStatus>(this.getStatus());
        this.selectionSubject$ = new BehaviorSubject<CookieConsentSelection | undefined>(this.getSelection());

        this.status$ = this.statusSubject$.asObservable();
        this.selection$ = this.selectionSubject$.asObservable();
        this.hasChosen$ = combineLatest([this.statusSubject$, this.selectionSubject$]).pipe(
            map(([status, selection]) => status !== undefined && selection !== undefined),
            distinctUntilChanged(),
        );
    }

    accept(selection: CookieConsentSelection, status: CookieConsentStatus = 'allow') {
        const expireInDays = 365;

        this.cookieService.set(COOKIE_CONSENT_STATUS_KEY, status, expireInDays, cookiePath, environment.domain);
        this.cookieService.set(
            COOKIE_CONSENT_SELECTION_KEY,
            JSON.stringify(selection),
            expireInDays,
            cookiePath,
            environment.domain,
        );

        this.statusSubject$.next(status);
        this.selectionSubject$.next(selection);
    }

    acceptAll() {
        const selection: CookieConsentSelection = {
            ads: true,
            service: true,
            performance: true,
            technical: true,
        };

        this.accept(selection);
    }

    denyAll() {
        const selection: CookieConsentSelection = {
            ads: false,
            service: false,
            performance: false,
            technical: true,
        };

        this.accept(selection, 'dismiss');
    }

    deleteAdsCookies() {
        adsCookies.forEach(name => {
            this.cookieService.delete(name, cookiePath, environment.domain);
        });
    }

    deleteServicesCookies() {
        servicesCookies.forEach(name => {
            this.cookieService.delete(name, cookiePath, environment.domain);
        });
    }

    deletePerformanceCookies() {
        performanceCookies.forEach(name => {
            this.cookieService.delete(name, cookiePath, environment.domain);
        });
    }

    hasConsentedFor(feature: CookieConsentFeature) {
        if (environment.requireCookieConsent) {
            const selection = this.getSelection();
            return selection && selection[feature];
        }

        return true;
    }

    hasConsentedFor$(feature: CookieConsentFeature) {
        if (environment.requireCookieConsent) {
            return this.selection$.pipe(
                map(selection => selection && selection[feature]),
                distinctUntilChanged(),
            );
        }

        return of(true);
    }

    private getStatus() {
        if (this.cookieService.check(COOKIE_CONSENT_STATUS_KEY)) {
            return this.cookieService.get(COOKIE_CONSENT_STATUS_KEY) as CookieConsentStatus;
        }

        return undefined;
    }

    private getSelection() {
        if (this.cookieService.check(COOKIE_CONSENT_SELECTION_KEY)) {
            return JSON.parse(this.cookieService.get(COOKIE_CONSENT_SELECTION_KEY)) as CookieConsentSelection;
        }

        return undefined;
    }
}
