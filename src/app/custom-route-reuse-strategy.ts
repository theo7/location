import { ActivatedRouteSnapshot, DetachedRouteHandle, RouteReuseStrategy } from '@angular/router';

export class CustomRouteReuseStrategy implements RouteReuseStrategy {
    private storedRoutes = new Map<string, DetachedRouteHandle>();

    // first method trigger
    // future: route we are leaving
    // curr: route we are landing on
    // if return true: routing will not happened
    shouldReuseRoute(future: ActivatedRouteSnapshot, curr: ActivatedRouteSnapshot): boolean {
        return future.routeConfig === curr.routeConfig;
    }

    // when landing
    // route: route we are landing on
    // true: go to retreive method
    // false: next component (route component) will be created
    shouldAttach(route: ActivatedRouteSnapshot): boolean {
        return (
            route.routeConfig &&
            this.storedRoutes.has(route.routeConfig.data?.name) &&
            route.routeConfig.data?.reuseRoute
        );
    }

    // when landing
    // route: route we are landing on
    // null: nothing happened
    // DetachedRouteHandle: set the route with the one from cache
    retrieve(route: ActivatedRouteSnapshot): DetachedRouteHandle | null {
        if (!route.routeConfig || !this.storedRoutes.has(route.routeConfig.data?.name)) {
            return null;
        }
        return this.storedRoutes.get(route.routeConfig.data?.name) || null;
    }

    // invoke when leaving route
    // route: route we are leaving
    // true: go to store method
    // false: do nothing
    shouldDetach(route: ActivatedRouteSnapshot): boolean {
        if (!route.routeConfig) {
            return false;
        }
        return route.routeConfig.data?.reuseRoute;
    }

    // route: route we are leaving
    // detached: route we want to cache
    store(route: ActivatedRouteSnapshot, detachedTree: DetachedRouteHandle): void {
        if (!route.routeConfig) {
            return;
        }
        this.storedRoutes.set(route.routeConfig.data?.name, detachedTree);
    }
}
