import { Component, Inject, ViewEncapsulation } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { environment } from '../../../environments/environment';

@Component({
    selector: 'app-legal-content-dialog',
    templateUrl: './legal-content-dialog.component.html',
    styleUrls: ['./legal-content-dialog.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class LegalContentDialogComponent {
    legalContentProp: string;
    contactEmail = environment.contactAddress;

    constructor(
        private readonly dialogRef: MatDialogRef<LegalContentDialogComponent, boolean>,
        @Inject(MAT_DIALOG_DATA) private data: any,
    ) {
        this.legalContentProp = this.data.legalContentProp;
    }

    close(): void {
        this.dialogRef.close();
    }
}
