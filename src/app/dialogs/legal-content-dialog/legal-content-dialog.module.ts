import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { TranslateModule } from '@ngx-translate/core';
import { HeaderModule } from '../../features/layout/headers/header.module';
import { PipesModule } from '../../shared/pipes/pipes.module';
import { LegalContentDialogComponent } from './legal-content-dialog.component';

@NgModule({
    declarations: [LegalContentDialogComponent],
    imports: [
        CommonModule,
        MatIconModule,
        TranslateModule,
        MatDialogModule,
        MatButtonModule,
        HeaderModule,
        PipesModule,
    ],
})
export class LegalContentDialogModule {}
