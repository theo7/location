import { Component } from '@angular/core';

@Component({
    selector: 'app-cancel-sell-icon',
    templateUrl: './cancel-sell-icon.component.html',
    styleUrls: ['./cancel-sell-icon.component.scss'],
})
export class CancelSellIconComponent {}
