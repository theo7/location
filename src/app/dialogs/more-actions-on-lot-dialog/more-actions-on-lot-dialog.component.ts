import { Component, HostBinding, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';
import { DiscussionsSelectors } from 'src/app/store/services/discussions-selectors.service';
import { Discussion, LotStatusEnum, MoreActionsOnLotModalResult, UserProfile } from '../../shared/models/models';
import { DeviceService } from '../../shared/services/device.service';
import { UserProfileSelectors } from '../../store/services/user-selectors.service';

type MoreActionsOnLotDialogComponentData = {
    discussion: Discussion;
    userId: number;
};

@UntilDestroy()
@Component({
    selector: 'app-more-actions-on-lot-dialog',
    templateUrl: './more-actions-on-lot-dialog.component.html',
    styleUrls: ['./more-actions-on-lot-dialog.component.scss'],
})
export class MoreActionsOnLotDialogComponent implements OnInit {
    isCancelSellEnabled$?: Observable<boolean>;
    isUserBlock$?: Observable<boolean>;
    isSaleInProgress$?: Observable<boolean>;
    isSaleInProgressWith$?: Observable<boolean>;
    userProfile$?: Observable<UserProfile | undefined>;
    discussion?: Discussion;

    @HostBinding('class') get class() {
        return this.device.mode;
    }

    constructor(
        private readonly device: DeviceService,
        private readonly discussionsSelectors: DiscussionsSelectors,
        private readonly userProfileSelectors: UserProfileSelectors,
        private readonly dialog: MatDialogRef<MoreActionsOnLotDialogComponent, MoreActionsOnLotModalResult>,
        @Inject(MAT_DIALOG_DATA) private readonly data: MoreActionsOnLotDialogComponentData,
    ) {}

    ngOnInit() {
        this.isCancelSellEnabled$ = this.discussionsSelectors
            .isCancelSellEnabled$(this.data.discussion.lot!.id!)
            .pipe(untilDestroyed(this));
        this.isUserBlock$ = this.userProfileSelectors.isBlocked$(this.data.userId).pipe(untilDestroyed(this));
        this.isSaleInProgress$ = this.discussionsSelectors
            .isSaleInProgress$(this.data.discussion.lot!.id!)
            .pipe(untilDestroyed(this));
        this.isSaleInProgressWith$ = this.discussionsSelectors
            .isSaleInProgressWith$(this.data.userId)
            .pipe(untilDestroyed(this));
        this.userProfile$ = this.userProfileSelectors.userProfile$.pipe(untilDestroyed(this));
        this.discussion = this.data.discussion;
    }

    onCancelSellButtonClicked() {
        this.userProfile$!.pipe(first()).subscribe(userProfile => {
            const lot = this.discussion?.lot;

            if (lot && lot.id) {
                const isBuyer = lot.buyer?.id === userProfile?.id;
                const isSeller = lot.seller?.id === userProfile?.id;

                if (isBuyer && lot.status === LotStatusEnum.PREPAID) {
                    this.dialog.close({
                        type: 'CANCEL_SELL',
                        lotId: lot.id,
                    });
                }
                if (isSeller && lot.status === LotStatusEnum.PREPAID) {
                    this.dialog.close({
                        type: 'DECLINE_SELL',
                        lotId: lot.id,
                    });
                }
                if (isSeller && lot.status === LotStatusEnum.PAID) {
                    this.dialog.close({
                        type: 'CANCEL_PACKAGE',
                        lotId: lot.id,
                    });
                }
            }
        });
    }

    onBlockUserButtonClicked() {
        this.dialog.close({
            type: 'BLOCK_USER',
            userId: this.data.userId,
        });
    }

    onUnblockUserButtonClicked() {
        this.dialog.close({
            type: 'UNBLOCK_USER',
            userId: this.data.userId,
        });
    }

    onArchiveDiscussionButtonClicked() {
        if (this.discussion?.id) {
            this.dialog.close({
                type: 'ARCHIVE_DISCUSSION',
                discussionId: this.discussion?.id,
            });
        }
    }

    onReportUserButtonClicked() {
        if (this.discussion) {
            this.dialog.close({
                type: 'REPORT_USER',
                discussion: this.discussion,
            });
        }
    }
}
