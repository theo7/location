import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatTooltipModule } from '@angular/material/tooltip';
import { TranslateModule } from '@ngx-translate/core';
import { AppCoreModule } from 'src/app/shared/components/app-core/app-core.module';
import { ButtonModule } from 'src/app/shared/components/button/button.module';
import { AppStoreModule } from 'src/app/store/app-store.module';
import { CancelSellIconComponent } from './cancel-sell-icon/cancel-sell-icon.component';
import { MoreActionsOnLotDialogComponent } from './more-actions-on-lot-dialog.component';

@NgModule({
    declarations: [MoreActionsOnLotDialogComponent, CancelSellIconComponent],
    imports: [
        CommonModule,
        AppCoreModule,
        TranslateModule,
        AppStoreModule,
        MatDialogModule,
        MatIconModule,
        MatTooltipModule,
        MatButtonModule,
        ButtonModule,
    ],
    exports: [MoreActionsOnLotDialogComponent],
})
export class MoreActionsOnLotDialogModule {}

export { MoreActionsOnLotDialogComponent };
