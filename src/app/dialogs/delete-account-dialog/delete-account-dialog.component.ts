import { ChangeDetectionStrategy, Component } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';
import { CustomValidators } from 'src/app/shared/helpers/custom-validators';
import { UserProfileSelectors } from 'src/app/store/services/user-selectors.service';

@UntilDestroy()
@Component({
    selector: 'app-delete-account-dialog',
    templateUrl: './delete-account-dialog.component.html',
    styleUrls: ['./delete-account-dialog.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DeleteAccountDialogComponent {
    mail$: Observable<string | undefined>;

    email?: FormControl;

    constructor(
        userProfileSelectors: UserProfileSelectors,
        private readonly dialogRef: MatDialogRef<DeleteAccountDialogComponent, boolean>,
    ) {
        this.mail$ = userProfileSelectors.mail$.pipe(untilDestroyed(this));

        this.mail$.pipe(first()).subscribe(mail => {
            this.email = new FormControl('', [CustomValidators.strictEqual(mail)]);
        });
    }

    onConfirmButtonClicked() {
        this.dialogRef.close(true);
    }
}
