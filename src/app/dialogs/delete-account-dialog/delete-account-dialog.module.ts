import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { TranslateModule } from '@ngx-translate/core';
import { ButtonModule } from 'src/app/shared/components/button/button.module';
import { DeleteAccountDialogComponent } from './delete-account-dialog.component';

@NgModule({
    declarations: [DeleteAccountDialogComponent],
    imports: [
        CommonModule,
        TranslateModule,
        ButtonModule,
        FormsModule,
        ReactiveFormsModule,
        MatDialogModule,
        MatFormFieldModule,
        MatInputModule,
        MatIconModule,
        MatButtonModule,
    ],
    exports: [DeleteAccountDialogComponent],
})
export class DeleteAccountDialogModule {}

export { DeleteAccountDialogComponent };
