import { Observable } from '@angular-devkit/core/node_modules/rxjs';
import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { get as applyLevenshtein } from 'fast-levenshtein';
import { debounceTime, finalize, map, withLatestFrom } from 'rxjs/operators';
import { CustomValidators } from 'src/app/shared/helpers/custom-validators';
import { Brand } from 'src/app/shared/models/models';
import { ProductsService } from 'src/app/shared/services/products.service';
import { RepositoriesSelectors } from 'src/app/store/services/repositories-selectors.service';

type SuggestBrandDialogComponentData = {
    productId?: number;
};

@UntilDestroy()
@Component({
    selector: 'app-suggest-brand-dialog',
    templateUrl: './suggest-brand-dialog.component.html',
    styleUrls: ['./suggest-brand-dialog.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuggestBrandDialogComponent {
    loading = false;
    brand?: FormControl;

    approximativeBrands$?: Observable<Brand[]>;

    constructor(
        @Inject(MAT_DIALOG_DATA) private readonly data: SuggestBrandDialogComponentData,
        repositoriesSelectors: RepositoriesSelectors,
        private readonly productsService: ProductsService,
        private readonly dialogRef: MatDialogRef<SuggestBrandDialogComponent, boolean>,
    ) {
        this.brand = new FormControl('', {
            validators: [Validators.required],
            asyncValidators: [CustomValidators.uniqueBrandAsyncValidators(repositoriesSelectors)],
        });

        const valueChanges$ = this.brand?.valueChanges as Observable<string>;

        valueChanges$.pipe(untilDestroyed(this)).subscribe(() => {
            if (this.brand) {
                this.brand.markAsTouched();
            }
        });

        // approximate check if suggested brand is not one of the existing
        this.approximativeBrands$ = valueChanges$.pipe(
            debounceTime(200),
            withLatestFrom(repositoriesSelectors.brands$),
            map(([value, brands]) => {
                if (this.brand?.valid !== true) {
                    return [];
                }
                if (!value) {
                    return [];
                }

                const lowercaseValue = value.trim().toLowerCase();

                if (lowercaseValue.length <= 2) {
                    return [];
                }

                const valueLength = lowercaseValue.length;
                const distanceMinimum = valueLength > 6 ? 2 : 1;

                return brands.filter(b => {
                    const distance = applyLevenshtein(lowercaseValue, b.label.toLowerCase());
                    return distance <= distanceMinimum;
                });
            }),
            untilDestroyed(this),
        );
    }

    onConfirmButtonClicked() {
        this.loading = true;

        const value = this.brand?.value?.trim();

        this.productsService
            .suggestBrand(value, this.data?.productId)
            .pipe(
                finalize(() => {
                    this.loading = false;
                }),
            )
            .subscribe(() => {
                this.dialogRef.close(true);
            });
    }

    close() {
        this.dialogRef.close(false);
    }
}
