import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { filter, first } from 'rxjs/operators';
import { CguType } from '../../shared/models/models';
import { DeviceService } from '../../shared/services/device.service';
import { UserProfileDispatchers } from '../../store/services/user-dispatchers.service';
import { UserProfileSelectors } from '../../store/services/user-selectors.service';
import { CguDialogComponent } from './cgu-dialog/cgu-dialog.component';

@UntilDestroy()
@Component({
    selector: 'app-cgu-modification-dialog',
    templateUrl: './cgu-modification-dialog.component.html',
    styleUrls: ['./cgu-modification-dialog.component.scss'],
})
export class CguModificationDialogComponent implements OnInit {
    type?: CguType;
    loading?: boolean;

    constructor(
        @Inject(MAT_DIALOG_DATA) private data: any,
        private dialog: MatDialog,
        private dialogRef: MatDialogRef<CguModificationDialogComponent>,
        private userProfileSelectors: UserProfileSelectors,
        private userProfileDispatchers: UserProfileDispatchers,
        private deviceService: DeviceService,
    ) {}

    ngOnInit() {
        this.type = this.data.type;
    }

    openCgu() {
        const dialogParams = device =>
            ({
                handset: {
                    height: '100vh',
                    width: '100vw',
                    maxWidth: '100vw',
                    panelClass: 'full-screen-dialog',
                },
                desktop: {
                    width: '90vw',
                    maxWidth: '700px',
                    height: '80vh',
                },
            }[device]);

        this.dialog.open(CguDialogComponent, {
            data: {
                type: this.type,
            },
            ...dialogParams(this.deviceService.mode),
        });
    }

    accept() {
        this.loading = true;
        if (this.type) {
            this.userProfileDispatchers.updateCguModificationType(this.type);
        }
        this.userProfileSelectors.userProfileState$
            .pipe(
                filter(state => !state.loading),
                first(),
                filter(state => !state.error),
                untilDestroyed(this),
            )
            .subscribe(() => {
                this.dialogRef.close(true);
            });
    }
}
