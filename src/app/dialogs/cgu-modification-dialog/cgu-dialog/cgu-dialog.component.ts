import { Component, Inject, ViewEncapsulation } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { environment } from '../../../../environments/environment';

@Component({
    selector: 'app-cgu-dialog',
    templateUrl: './cgu-dialog.component.html',
    styleUrls: ['./cgu-dialog.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class CguDialogComponent {
    contactAddress = environment.contactAddress;

    constructor(@Inject(MAT_DIALOG_DATA) public data: any) {}
}
