import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { TranslateModule } from '@ngx-translate/core';
import { ButtonModule } from '../../shared/components/button/button.module';
import { PipesModule } from '../../shared/pipes/pipes.module';
import { CguDialogComponent } from './cgu-dialog/cgu-dialog.component';
import { CguModificationDialogComponent } from './cgu-modification-dialog.component';

@NgModule({
    declarations: [CguModificationDialogComponent, CguDialogComponent],
    imports: [
        CommonModule,
        TranslateModule,
        ButtonModule,
        MatDialogModule,
        MatProgressSpinnerModule,
        MatButtonModule,
        MatIconModule,
        PipesModule,
    ],
    exports: [CguModificationDialogComponent],
})
export class CguModificationDialogModule {}

export { CguModificationDialogComponent };
