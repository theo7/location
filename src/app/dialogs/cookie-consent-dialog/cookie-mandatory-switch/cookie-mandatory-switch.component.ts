import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
    selector: 'app-cookie-mandatory-switch',
    templateUrl: './cookie-mandatory-switch.component.html',
    styleUrls: ['./cookie-mandatory-switch.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CookieMandatorySwitchComponent {}
