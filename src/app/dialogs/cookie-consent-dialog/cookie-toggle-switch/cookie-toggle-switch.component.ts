import { ChangeDetectionStrategy, Component, forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
    selector: 'app-cookie-toggle-switch',
    templateUrl: './cookie-toggle-switch.component.html',
    styleUrls: ['./cookie-toggle-switch.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => CookieToggleSwitchComponent),
            multi: true,
        },
    ],
})
export class CookieToggleSwitchComponent implements ControlValueAccessor {
    checked = false;
    isDisabled = false;

    onChange: (value: boolean) => void = () => {};
    onTouched: () => void = () => {};

    writeValue(value: boolean): void {
        this.checked = value;
    }

    registerOnChange(fn: any): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }

    setDisabledState?(disabled: boolean): void {
        this.isDisabled = disabled;
    }

    onContainerButtonClicked() {
        this.checked = !this.checked;
        this.onChange(this.checked);
        this.onTouched();
    }
}
