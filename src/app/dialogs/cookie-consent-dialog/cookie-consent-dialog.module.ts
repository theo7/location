import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { CookieConsentModule } from 'src/app/features/cookie-consent/cookie-consent.module';
import { ButtonModule } from 'src/app/shared/components/button/button.module';
import { CookieMandatorySwitchComponent } from '../../dialogs/cookie-consent-dialog/cookie-mandatory-switch/cookie-mandatory-switch.component';
import { CookieToggleSwitchComponent } from '../../dialogs/cookie-consent-dialog/cookie-toggle-switch/cookie-toggle-switch.component';
import { CookieConsentDialogComponent } from './cookie-consent-dialog/cookie-consent-dialog.component';

@NgModule({
    declarations: [CookieConsentDialogComponent, CookieMandatorySwitchComponent, CookieToggleSwitchComponent],
    imports: [
        CommonModule,
        RouterModule,
        TranslateModule,
        ButtonModule,
        FormsModule,
        ReactiveFormsModule,
        CookieConsentModule,
    ],
    exports: [CookieConsentDialogComponent],
})
export class CookieConsentDialogModule {}

export { CookieConsentDialogComponent };
