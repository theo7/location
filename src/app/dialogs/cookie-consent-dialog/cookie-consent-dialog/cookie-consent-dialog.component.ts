import { ChangeDetectionStrategy, Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { first } from 'rxjs/operators';
import { CookieConsentService } from '../../../features/cookie-consent/services/cookie-consent.service';

@Component({
    selector: 'app-cookie-consent-dialog',
    templateUrl: './cookie-consent-dialog.component.html',
    styleUrls: ['./cookie-consent-dialog.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CookieConsentDialogComponent {
    form: FormGroup;

    constructor(
        fb: FormBuilder,
        cookieConsentService: CookieConsentService,
        private readonly dialogRef: MatDialogRef<CookieConsentDialogComponent>,
    ) {
        this.form = fb.group({
            ads: [false],
            service: [false],
            performance: [false],
            technical: [{ value: true, disabled: true }, Validators.requiredTrue],
        });

        cookieConsentService.selection$.pipe(first()).subscribe(selection => {
            if (selection) {
                this.form.patchValue({
                    ...selection,
                    technical: true,
                });
            }
        });
    }

    onValidateButtonClicked() {
        const value = this.form.getRawValue();
        this.dialogRef.close(value);
    }
}
