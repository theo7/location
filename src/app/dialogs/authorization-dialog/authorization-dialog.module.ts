import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { TranslateModule } from '@ngx-translate/core';
import { AuthorizationDialogComponent } from './authorization-dialog.component';

@NgModule({
    declarations: [AuthorizationDialogComponent],
    imports: [
        CommonModule,
        TranslateModule,
        MatButtonModule,
        MatDialogModule,
        MatFormFieldModule,
        MatInputModule,
        FormsModule,
        ReactiveFormsModule,
    ],
    exports: [AuthorizationDialogComponent],
})
export class AuthorizationDialogModule {}

export { AuthorizationDialogComponent };
