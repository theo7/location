import { Component } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
    selector: 'app-authorization-dialog',
    templateUrl: './authorization-dialog.component.html',
    styleUrls: ['./authorization-dialog.component.scss'],
})
export class AuthorizationDialogComponent {
    password = new FormControl('', Validators.required);

    constructor(public dialogRef: MatDialogRef<AuthorizationDialogComponent>) {}

    closeDialog() {
        if (this.password.valid) {
            this.dialogRef.close(this.password.value);
        }
    }
}
