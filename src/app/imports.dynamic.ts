import { from } from 'rxjs';
import { map } from 'rxjs/operators';

export const paymentProcessDialogComponent$ = from(
    import('./shared/components/payment-process-dialog/payment-process-dialog.module'),
).pipe(map(({ PaymentProcessDialogComponent }) => PaymentProcessDialogComponent));

export const addAddressDialogComponent$ = from(
    import('./shared/components/add-address-dialog/add-address-dialog.module'),
).pipe(map(({ AddAddressDialogComponent }) => AddAddressDialogComponent));

export const cookieConsentDialogComponent$ = from(
    import('./dialogs/cookie-consent-dialog/cookie-consent-dialog.module'),
).pipe(map(({ CookieConsentDialogComponent }) => CookieConsentDialogComponent));

export const authorizationDialogComponent$ = from(
    import('./dialogs/authorization-dialog/authorization-dialog.module'),
).pipe(map(({ AuthorizationDialogComponent }) => AuthorizationDialogComponent));

export const moreActionsOnLotDialogComponent$ = from(
    import('./dialogs/more-actions-on-lot-dialog/more-actions-on-lot-dialog.module'),
).pipe(map(({ MoreActionsOnLotDialogComponent }) => MoreActionsOnLotDialogComponent));

export const cguModificationDialogComponent$ = from(
    import('./dialogs/cgu-modification-dialog/cgu-modification-dialog.module'),
).pipe(map(({ CguModificationDialogComponent }) => CguModificationDialogComponent));

export const deleteAccountDialogComponent$ = from(
    import('./dialogs/delete-account-dialog/delete-account-dialog.module'),
).pipe(map(({ DeleteAccountDialogComponent }) => DeleteAccountDialogComponent));

export const suggestBranchDialogComponent$ = from(
    import('./dialogs/suggest-brand-dialog/suggest-brand-dialog.module'),
).pipe(map(({ SuggestBrandDialogComponent }) => SuggestBrandDialogComponent));

export const pdfService$ = from(import('./features/pdf/services/pdf.service')).pipe(
    map(({ PdfService }) => PdfService),
);
