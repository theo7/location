import { Injectable, NgZone } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { environment } from '../environments/environment';

@Injectable()
export class DefaultAngularFireDatabase extends AngularFireDatabase {}

@Injectable()
export class KiabiAngularFireDatabase extends AngularFireDatabase {}

@Injectable()
export class NotificationAngularFireDatabase extends AngularFireDatabase {}

export function defaultAngularFireDatabaseFactory(platformId: object, zone: NgZone): DefaultAngularFireDatabase {
    return new DefaultAngularFireDatabase(
        environment.firebase,
        undefined,
        environment.firebase.databaseURL,
        platformId,
        zone,
        null,
        null,
    );
}

export function kiabiAngularFireDatabaseFactory(platformId: object, zone: NgZone): DefaultAngularFireDatabase {
    return new KiabiAngularFireDatabase(
        environment.firebase,
        undefined,
        environment.firebase.kiabiDatabaseUrl,
        platformId,
        zone,
        null,
        null,
    );
}

export function notificationAngularFireDatabaseFactory(platformId: object, zone: NgZone): DefaultAngularFireDatabase {
    return new NotificationAngularFireDatabase(
        environment.firebase,
        undefined,
        environment.firebase.notificationDatabaseUrl,
        platformId,
        zone,
        null,
        null,
    );
}
