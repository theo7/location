import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'app-internal-link',
    templateUrl: './internal-link.component.html',
    styleUrls: ['./internal-link.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class InternalLinkComponent implements OnInit {
    @Input() link?: string;

    routerLink?: string;
    queryParams: any;

    ngOnInit() {
        const splittedLink = this.link?.split('?') ?? [];

        this.routerLink = splittedLink[0];

        const queryString = splittedLink[1] ?? '';
        const params = new URLSearchParams(queryString);

        this.queryParams = {};

        params.forEach((value, key) => {
            this.queryParams[key] = value;
        });
    }
}
