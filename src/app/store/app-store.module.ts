import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { metaReducers, reducers } from './reducers';
import { BankDetailsDispatchers } from './services/bank-details-dispatchers.service';
import { BankDetailsSelectors } from './services/bank-details-selectors.service';
import { CategoriesDispatchers } from './services/categories-dispatchers.service';
import { CategoriesSelectors } from './services/categories-selectors.service';
import { ConnectionDispatchers } from './services/connection-dispatchers.service';
import { ConnectionSelectors } from './services/connection-selectors.service';
import { CookiesDispatchers } from './services/cookies-dispatchers.service';
import { CreditDispatchersService } from './services/credit-dispatchers.service';
import { CreditSelectors } from './services/credit-selectors.service';
import { DiscussionsDispatchers } from './services/discussions-dispatchers.service';
import { DiscussionsSelectors } from './services/discussions-selectors.service';
import { FileDispatchers } from './services/file-dispatchers.service';
import { FilterDispatchers } from './services/filter-dispatchers.service';
import { FilterSelectors } from './services/filter-selectors.service';
import { FollowDispatchers } from './services/follow-dispatchers.service';
import { FollowSelectors } from './services/follow-selectors.service';
import { LegalContentDispatchers } from './services/legal-content-dispatchers.service';
import { LoggingDispatchers } from './services/logging-dispatchers.service';
import { LotDispatchers } from './services/lot-dispatchers.service';
import { NotificationsDispatchers } from './services/notifications-dispatchers.service';
import { NotificationsSelectors } from './services/notifications-selectors.service';
import { OrdersDispatchers } from './services/orders-dispatchers.service';
import { OrdersSelectors } from './services/orders-selectors.service';
import { ProductsDispatchers } from './services/products-dispatchers.service';
import { ProductsSelectors } from './services/products-selectors.service';
import { ProfileDispatchers } from './services/profile-dispatchers.service';
import { ProfileSelectors } from './services/profile-selectors.service';
import { RatingsDispatchers } from './services/ratings-dispatchers.service';
import { RatingsSelectors } from './services/ratings-selectors.service';
import { RepositoriesDispatchers } from './services/repositories-dispatchers.service';
import { RepositoriesSelectors } from './services/repositories-selectors.service';
import { RouterSelectors } from './services/router-selectors.service';
import { SearchDispatchers } from './services/search-dispatchers.service';
import { SearchSelectors } from './services/search-selectors.service';
import { SettingsDispatchers } from './services/settings-dispatchers.service';
import { SettingsSelectors } from './services/settings-selectors.service';
import { SuggestionDispatchers } from './services/suggestion-dispatchers.service';
import { SuggestionSelectors } from './services/suggestion-selectors.service';
import { UserProfileDispatchers } from './services/user-dispatchers.service';
import { UserProfileSelectors } from './services/user-selectors.service';
import { WishlistDispatchers } from './services/wishlist-dispatchers.service';
import { WishlistSelectors } from './services/wishlist-selectors.service';

@NgModule({
    imports: [StoreModule.forFeature('entityCache', reducers, { metaReducers }), EffectsModule.forFeature([])],
    providers: [
        UserProfileDispatchers,
        UserProfileSelectors,
        DiscussionsDispatchers,
        DiscussionsSelectors,
        CategoriesDispatchers,
        CategoriesSelectors,
        SearchDispatchers,
        RepositoriesDispatchers,
        RepositoriesSelectors,
        ConnectionDispatchers,
        ConnectionSelectors,
        RouterSelectors,
        SearchSelectors,
        ProductsDispatchers,
        NotificationsDispatchers,
        ProductsSelectors,
        BankDetailsSelectors,
        BankDetailsDispatchers,
        WishlistSelectors,
        WishlistDispatchers,
        LotDispatchers,
        OrdersDispatchers,
        OrdersSelectors,
        LoggingDispatchers,
        CookiesDispatchers,
        ProfileDispatchers,
        ProfileSelectors,
        SettingsSelectors,
        SettingsDispatchers,
        FilterSelectors,
        FilterDispatchers,
        FileDispatchers,
        LegalContentDispatchers,
        CreditSelectors,
        CreditDispatchersService,
        NotificationsSelectors,
        FollowDispatchers,
        FollowSelectors,
        SuggestionDispatchers,
        SuggestionSelectors,
        RatingsDispatchers,
        RatingsSelectors,
    ],
    exports: [StoreModule, EffectsModule],
})
export class AppStoreModule {}
