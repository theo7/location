import { createAction, props } from '@ngrx/store';
import { ProductFilter } from '../../shared/models/models';

export const trackSearchAndFilter = createAction(
    '[Google Analytics] Track Search and Filter',
    props<{ searchFilter: ProductFilter; results: number }>(),
);
