import { createAction, props } from '@ngrx/store';
import { ErrorLog } from 'src/app/features/logger/logger.models';

export const logError = createAction('[Logging] LOG_ERROR', props<{ errorLog: ErrorLog }>());
