import { createAction, props } from '@ngrx/store';
import { PriceAverage, Product, ProductFilter, ProductId, UserProfile } from '../../shared/models/models';

export const loadProduct = createAction('[Product] LOAD_PRODUCT', props<{ productId: number }>());
export const loadProductSuccess = createAction('[Product] LOAD_PRODUCT_SUCCESS', props<{ product: Product }>());
export const loadProductError = createAction('[Product] LOAD_PRODUCT_ERROR');

export const deleteProduct = createAction('[Product] DELETE_PRODUCT', props<{ productId: number }>());
export const deleteProductSuccess = createAction('[Product] DELETE_PRODUCT_SUCCESS', props<{ productId: number }>());
export const deleteProductError = createAction('[Product] DELETE_PRODUCT_ERROR', props<{ error: any }>());

export const reportProduct = createAction(
    '[Product] REPORT',
    props<{ productId: number; reason: string; content?: string }>(),
);
export const reportProductSuccess = createAction('[Product] REPORT_SUCCESS');
export const reportProductError = createAction('[Product] REPORT_ERROR', props<{ error: any }>());

export const getDressing = createAction('[Product] GET_DRESSING', props<{ userId: number }>());
export const getDressingSuccess = createAction(
    '[Product] GET_DRESSING_SUCCESS',
    props<{ products: Product[]; count: number; total: number }>(),
);
export const getDressingError = createAction('[Product] GET_DRESSING_ERROR', props<{ error: any }>());

export const getNextDressing = createAction('[Product] GET_NEXT_DRESSING', props<{ userId: number }>());
export const getNextDressingSuccess = createAction(
    '[Product] GET_NEXT_DRESSING_SUCCESS',
    props<{ products: Product[] }>(),
);
export const getNextDressingError = createAction('[Product] GET_NEXT_DRESSING_ERROR', props<{ error: any }>());

export const getOwnDressing = createAction('[Product] GET_OWN_DRESSING');
export const getOwnDressingSuccess = createAction(
    '[Product] GET_OWN_DRESSING_SUCCESS',
    props<{ products: Product[]; count: number; total: number }>(),
);
export const getOwnDressingError = createAction('[Product] GET_OWN_DRESSING_ERROR', props<{ error: any }>());

export const getOwnNextDressing = createAction('[Product] GET_OWN_NEXT_DRESSING');
export const getOwnNextDressingSuccess = createAction(
    '[Product] GET_OWN_NEXT_DRESSING_SUCCESS',
    props<{ products: Product[] }>(),
);
export const getOwnNextDressingError = createAction('[Product] GET_OWN_NEXT_DRESSING_ERROR', props<{ error: any }>());

export const like = createAction('[Product] LIKE', props<{ productId: ProductId }>());
export const likeSuccess = createAction('[Product] LIKE_SUCCESS', props<{ productId: ProductId }>());
export const likeError = createAction('[Product] LIKE_ERROR', props<{ error: any }>());

export const deleteLike = createAction('[Product] DELETE_LIKE', props<{ productId: ProductId }>());
export const deleteLikeSuccess = createAction('[Product] DELETE_LIKE_SUCCESS', props<{ productId: ProductId }>());
export const deleteLikeError = createAction('[Product] DELETE_LIKE_ERROR', props<{ error: any }>());

export const shareDressing = createAction('[Product] SHARE_DRESSING', props<{ user: UserProfile }>());
export const shareProduct = createAction('[Product] SHARE_PRODUCT', props<{ product: Product }>());

export const getProductViews = createAction('[Product] GET_PRODUCT_VIEWS', props<{ productId: number }>());
export const getProductViewsSuccess = createAction('[Product] GET_PRODUCT_VIEWS_SUCCESS', props<{ views: number }>());
export const getProductViewsError = createAction('[Product] GET_PRODUCT_VIEWS_ERROR', props<{ error: any }>());

export const getPriceAverage = createAction('[Product] GET_PRICE_AVERAGE', props<{ filter: ProductFilter }>());
export const getPriceAverageSuccess = createAction(
    '[Product] GET_PRICE_AVERAGE_SUCCESS',
    props<{ priceAverage: PriceAverage }>(),
);
export const getPriceAverageError = createAction('[Product] GET_PRICE_AVERAGE_ERROR', props<{ error: any }>());
