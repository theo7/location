import { createAction, props } from '@ngrx/store';
import { Lot } from '../../shared/models/models';

export const loadSales = createAction('[Orders] LOAD_SALES');
export const loadSalesSuccess = createAction('[Orders] LOAD_SALES_SUCCESS', props<{ lots: Lot[] }>());
export const loadSalesError = createAction('[Orders] LOAD_SALES_ERROR', props<{ error: any }>());

export const loadPurchases = createAction('[Orders] LOAD_PURCHASES');
export const loadPurchasesSuccess = createAction('[Orders] LOAD_PURCHASES_SUCCESS', props<{ lots: Lot[] }>());
export const loadPurchasesError = createAction('[Orders] LOAD_PURCHASES_ERROR', props<{ error: any }>());
