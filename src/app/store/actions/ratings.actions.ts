import { createAction, props } from '@ngrx/store';
import { Page, UserRating, UserRatingSummary } from '../../shared/models/models';

export const load = createAction('[Rating] LOAD');
export const loadSuccess = createAction(
    '[Rating] LOAD_SUCCESS',
    props<{ ratingsPages: Page<UserRating>; summary: UserRatingSummary }>(),
);
export const loadError = createAction('[Rating] LOAD_ERROR', props<{ error: any }>());

export const loadMore = createAction('[Rating] LOAD_MORE');
export const loadMoreSuccess = createAction('[Rating] LOAD_MORE_SUCCESS', props<{ ratingsPages: Page<UserRating> }>());
export const loadMoreError = createAction('[Rating] LOAD_MORE_ERROR', props<{ error: any }>());

export const update = createAction('[Rating] UPDATE', props<{ userRating: UserRating }>());

// answer
export const answerRating = createAction(
    '[Rating] ANSWER_RATING',
    props<{ userId: number; rateId: number; answer: string }>(),
);
export const answerRatingSuccess = createAction(
    '[Rating] ANSWER_RATING_SUCCESS',
    props<{ rateId: number; rating: UserRating }>(),
);
export const answerRatingError = createAction('[Rating] ANSWER_RATING_ERROR', props<{ error: any }>());
