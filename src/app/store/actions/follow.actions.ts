import { createAction, props } from '@ngrx/store';
import { Page, UserFollowingProfile } from '../../shared/models/models';

export const userFollow = createAction('[Follow] USER_FOLLOW', props<{ userId: number }>());
export const userFollowSuccess = createAction(
    '[Follow] USER_FOLLOW_SUCCESS',
    props<{ following: UserFollowingProfile }>(),
);
export const userFollowError = createAction('[Follow] USER_FOLLOW_ERROR', props<{ error: any }>());

export const userUnfollow = createAction('[Follow] USER_UNFOLLOW', props<{ userId: number }>());
export const userUnfollowSuccess = createAction('[Follow] USER_UNFOLLOW_SUCCESS', props<{ userId: number }>());
export const userUnfollowError = createAction('[Follow] USER_UNFOLLOW_ERROR', props<{ error: any }>());

export const getUserFollowing = createAction('[Follow] GET_USER_FOLLOWING', props<{ userId: number; page: number }>());
export const getUserFollowingSuccess = createAction(
    '[Follow] GET_USER_FOLLOWING_SUCCESS',
    props<{ followings: Page<UserFollowingProfile> & { totalPages: number } }>(),
);
export const getUserFollowingError = createAction('[Follow] GET_USER_FOLLOWING_ERROR', props<{ error: any }>());

export const getUserFollower = createAction('[Follow] GET_USER_FOLLOWER', props<{ userId: number; page: number }>());
export const getUserFollowerSuccess = createAction(
    '[Follow] GET_USER_FOLLOWER_SUCCESS',
    props<{ followers: Page<UserFollowingProfile> & { totalPages: number } }>(),
);
export const getUserFollowerError = createAction('[Follow] GET_USER_FOLLOWER_ERROR', props<{ error: any }>());

export const resetFollowing = createAction('[Follow] RESET_FOLLOWING');

export const resetFollowers = createAction('[Follow] RESET_FOLLOWERS');
