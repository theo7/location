import { createAction, props } from '@ngrx/store';

export const openDialog = createAction('[LEGAL_CONTENT] OPEN_DIALOG', props<{ legalContentProp: string }>());
