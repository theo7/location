import { createAction, props } from '@ngrx/store';
import { Category, Indexable } from '../../shared/models/models';

// getCategories
export const getCategories = createAction('[Categories] GET_CATEGORIES');
export const getCategoriesSuccess = createAction(
    '[Categories] GET_CATEGORIES_SUCCESS',
    props<{ categories: Indexable<Category>[] }>(),
);
export const getCategoriesError = createAction('[Categories] GET_CATEGORIES_ERROR');
