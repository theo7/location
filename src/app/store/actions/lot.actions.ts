import { createAction, props } from '@ngrx/store';
import { Discussion, Lot, NewDiscussionReport, Product, RemunerationMode } from '../../shared/models/models';

export const openMoreActionsModal = createAction(
    '[Lot] OPEN_MORE_ACTIONS_MODAL',
    props<{ discussion: Discussion; userId: number }>(),
);

export const openConfirmCancelSellModal = createAction(
    '[Lot] OPEN_CONFIRM_CANCEL_SELL_MODAL',
    props<{ lotId: number }>(),
);

export const confirmCancelSell = createAction('[Lot] CONFIRM_CANCEL_SELL', props<{ lotId: number }>());
export const confirmCancelSellSuccess = createAction('[Lot] CONFIRM_CANCEL_SELL_SUCCESS', props<{ lotId: number }>());

export const openConfirmDeclineSellModal = createAction(
    '[Lot] OPEN_CONFIRM_DECLINE_SELL_MODAL',
    props<{ lotId: number }>(),
);

export const confirmDeclineSell = createAction('[Lot] CONFIRM_DECLINE_SELL', props<{ lotId: number }>());
export const confirmDeclineSellSuccess = createAction('[Lot] CONFIRM_DECLINE_SELL_SUCCESS', props<{ lotId: number }>());

export const openConfirmCancelPackageModal = createAction(
    '[Lot] OPEN_CONFIRM_CANCEL_PACKAGE_MODAL',
    props<{ lotId: number }>(),
);

export const confirmCancelPackage = createAction('[Lot] CONFIRM_CANCEL_PACKAGE', props<{ lotId: number }>());
export const confirmCancelPackageSuccess = createAction(
    '[Lot] CONFIRM_CANCEL_PACKAGE_SUCCESS',
    props<{ lotId: number }>(),
);

// blockUser
export const openConfirmBlockUserModal = createAction(
    '[User] OPEN_CONFIRM_BLOCK_USER_MODAL',
    props<{ userId: number }>(),
);

export const confirmBlockUserSuccess = createAction('[Lot] CONFIRM_BLOCK_USER_SUCCESS', props<{ userId: number }>());

export const openConfirmUnblockUserModal = createAction(
    '[User] OPEN_CONFIRM_UNBLOCK_USER_MODAL',
    props<{ userId: number }>(),
);

export const confirmUnblockUserSuccess = createAction(
    '[Lot] CONFIRM_UNBLOCK_USER_SUCCESS',
    props<{ userId: number }>(),
);

export const openConfirmArchiveDiscussionModal = createAction(
    '[User] OPEN_CONFIRM_ARCHIVE_DISCUSSION_MODAL',
    props<{ discussionId: string | number }>(),
);

export const confirmArchiveDiscussionSuccess = createAction(
    '[Lot] CONFIRM_ARCHIVE_DISCUSSION_SUCCESS',
    props<{ discussionId: number }>(),
);

export const openReportUserModal = createAction('[User] OPEN_REPORT_USER_MODAL', props<{ discussion: Discussion }>());

export const confirmReportUserSuccess = createAction(
    '[User] CONFIRM_REPORT_USER_SUCCESSS',
    props<{ discussionId: number | string; newDiscussionReport: NewDiscussionReport }>(),
);

export const startDiscussion = createAction('[Lot] START_DISCUSSION', props<{ productId: number }>());

export const startPaymentProcess = createAction('[Lot] START_PAYMENT_PROCESS', props<{ productId: number }>());

export const createLot = createAction('[Lot] CREATE_LOT', props<{ product: Product }>());
export const createLotSuccess = createAction('[Lot] CREATE_LOT_SUCCESS');

export const changeRemunerationMode = createAction(
    '[Lot] CHANGE_REMUNERATION_MODE',
    props<{ lotId: number; remunerationMode: RemunerationMode }>(),
);
export const changeRemunerationModeSuccess = createAction(
    '[Lot] CHANGE_REMUNERATION_MODE_SUCCESS',
    props<{ lot: Lot }>(),
);
export const changeRemunerationModeError = createAction(
    '[Lot] CHANGE_REMUNERATION_MODE_ERROR',
    props<{ error: any }>(),
);
