import { createAction, props } from '@ngrx/store';
import { Page, Product } from '../../shared/models/models';

export const loadSuggestion = createAction('[Suggestion] LOAD_SUGGESTION');
export const loadSuggestionSuccess = createAction(
    '[Suggestion] LOAD_SUGGESTION_SUCCESS',
    props<{ page: Page<Product> }>(),
);
export const loadSuggestionError = createAction('[Suggestion] LOAD_SUGGESTION_ERROR', props<{ error: any }>());

export const loadMoreSuggestion = createAction('[Suggestion] LOAD_MORE_SUGGESTION');
export const loadMoreSuggestionSuccess = createAction(
    '[Suggestion] LOAD_MORE_SUGGESTION_SUCCESS',
    props<{ page: Page<Product> }>(),
);
export const loadMoreSuggestionError = createAction('[Suggestion] LOAD_MORE_SUGGESTION_ERROR', props<{ error: any }>());
