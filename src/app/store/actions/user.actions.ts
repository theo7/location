import { createAction, props } from '@ngrx/store';
import { Address, CguType, UserProfile } from '../../shared/models/models';

export const openLoginPage = createAction('[User] OPEN_LOGIN_PAGE');

// login
export const login = createAction('[User] LOGIN', props<{ email: string; password: string }>());
export const loginSuccess = createAction('[User] LOGIN_SUCCESS');
export const loginError = createAction('[User] LOGIN_ERROR', props<{ error: any }>());

// getUserProfile
export const getUserProfile = createAction('[User] GET_USER_PROFILE');
export const getUserProfileSuccess = createAction(
    '[User] GET_USER_PROFILE_SUCCESS',
    props<{ userProfile: UserProfile }>(),
);
export const getUserProfileError = createAction('[User] GET_USER_PROFILE_ERROR');

// logout
export const logout = createAction('[User] LOGOUT');
export const logoutSuccess = createAction('[User] LOGOUT_SUCCESS');
export const logoutError = createAction('[User] LOGOUT_ERROR');

// updateUserProfile
export const updateUserProfile = createAction('[User] UPDATE_USER_PROFILE', props<{ userProfile: UserProfile }>());
export const updateUserProfileSuccess = createAction(
    '[User] UPDATE_USER_PROFILE_SUCCESS',
    props<{ userProfile: UserProfile }>(),
);
export const updateUserProfileError = createAction('[User] UPDATE_USER_PROFILE_ERROR', props<{ error: any }>());

// postUserAddress
export const postUserAddress = createAction('[User] POST_USER_ADDRESS', props<{ address: Address }>());
export const postUserAddressSuccess = createAction('[User] POST_USER_ADDRESS_SUCCESS', props<{ address: Address }>());
export const postUserAddressError = createAction('[User] POST_USER_ADDRESS_ERROR');

// deleteUserAddress
export const deleteUserAddress = createAction('[User] DELETE_USER_ADDRESS', props<{ address: Address }>());
export const deleteUserAddressSuccess = createAction(
    '[User] DELETE_USER_ADDRESS_SUCCESS',
    props<{ address: Address }>(),
);
export const deleteUserAddressError = createAction('[User] DELETE_USER_ADDRESS_ERROR');

// updateUserAddress
export const updateUserAddress = createAction('[User] UPDATE_USER_ADDRESS', props<{ address: Address }>());
export const updateUserAddressSuccess = createAction(
    '[User] UPDATE_USER_ADDRESS_SUCCESS',
    props<{ address: Address }>(),
);
export const updateUserAddressError = createAction('[User] UPDATE_USER_ADDRESS_ERROR');

// update cgu modification date
export const updateCgu = createAction('[User] UPDATE_CGU_MODIFICATION_DATE', props<{ cguType: CguType }>());
export const updateCguSuccess = createAction(
    '[User] UPDATE_CGU_MODIFICATION_DATE_SUCCESS',
    props<{ userProfile: UserProfile }>(),
);
export const updateCguError = createAction('[User] UPDATE_CGU_MODIFICATION_DATE_ERROR', props<{ error: any }>());

// signUp
export const signUp = createAction('[User] SIGN_UP', props<{ signUpData: any }>());
export const signUpSuccess = createAction('[User] SIGN_UP_SUCCESS');
export const signUpError = createAction('[User] SIGN_UP_ERROR', props<{ error: any }>());

// resetPassword
export const resetPassword = createAction('[User] RESET_PASSWORD', props<{ email: string }>());
export const resetPasswordSuccess = createAction('[User] RESET_PASSWORD_SUCCESS');
export const resetPasswordError = createAction('[User] RESET_PASSWORD_ERROR', props<{ error: any }>());

// changePassword
export const changePassword = createAction(
    '[User] CHANGE_PASSWORD',
    props<{ currentPassword: string; newPassword: string }>(),
);
export const changePasswordSuccess = createAction('[User] CHANGE_PASSWORD_SUCCESS');
export const changePasswordError = createAction('[User] CHANGE_PASSWORD_ERROR', props<{ error: any }>());

export const blockUser = createAction('[User] BLOCK_USER', props<{ userId: number }>());
export const blockUserSuccess = createAction('[User] BLOCK_USER_SUCCESS', props<{ userId: number }>());
export const blockUserError = createAction('[User] BLOCK_USER_ERROR', props<{ error: any }>());

export const unblockUser = createAction('[User] UNBLOCK_USER', props<{ userId: number }>());
export const unblockUserSuccess = createAction('[User] UNBLOCK_USER_SUCCESS', props<{ userId: number }>());
export const unblockUserError = createAction('[User] UNBLOCK_USER_ERROR', props<{ error: any }>());

export const switchVacationMode = createAction('[User] SWITCH_VACATION_MODE');
export const switchVacationModeSuccess = createAction('[User] SWITCH_VACATION_MODE_SUCCESS');
export const switchVacationModeError = createAction('[User] SWITCH_VACATION_MODE_ERROR', props<{ error: any }>());
