import { Photo as CameraPhoto } from '@capacitor/camera';
import { createAction, props } from '@ngrx/store';
import { FileType, Picture, PictureType } from '../../shared/models/models';

const selectFile = createAction('[FILE] SELECT_FILE', props<{ fileType: FileType }>());
const fileSelected = createAction('[FILE] FILE_SELECTED', props<{ file: CameraPhoto; fileType: FileType }>());

const uploadProductPicture = createAction(
    '[FILE] UPLOAD_PRODUCT_PICTURE',
    props<{ picture: CameraPhoto; index: number; pictureType: PictureType }>(),
);
const uploadProductPictureSuccess = createAction(
    '[FILE] UPLOAD_PRODUCT_PICTURE_SUCCESS',
    props<{ picture: Picture; index: number }>(),
);
const uploadProductPictureError = createAction(
    '[FILE] UPLOAD_PRODUCT_PICTURE_ERROR',
    props<{ error: any; index: number }>(),
);

const uploadRawPicture = createAction(
    '[FILE] UPLOAD_RAW_PICTURE',
    props<{ file: Blob; index: number; pictureType: PictureType }>(),
);

const uploadChatPicture = createAction(
    '[FILE] UPLOAD_CHAT_PICTURE',
    props<{ picture: CameraPhoto; discussionId: number; index: number }>(),
);
const uploadChatPictureSuccess = createAction(
    '[FILE] UPLOAD_CHAT_PICTURE_SUCCESS',
    props<{ picture: Picture; index: number }>(),
);
const uploadChatPictureError = createAction('[FILE] UPLOAD_CHAT_PICTURE_ERROR', props<{ error: any; index: number }>());

const uploadKycFile = createAction('[FILE] UPLOAD_KYC_FILE', props<{ file: CameraPhoto; fileType: FileType }>());
const uploadKycFileSuccess = createAction(
    '[FILE] UPLOAD_KYC_FILE_SUCCESS',
    props<{ pdfFile: File; fileType: FileType }>(),
);
const uploadKycFileError = createAction('[FILE] UPLOAD_KYC_FILE_ERROR', props<{ error: any; fileType: FileType }>());

export const fileActions = {
    selectFile,
    fileSelected,
    uploadProductPicture,
    uploadProductPictureSuccess,
    uploadProductPictureError,
    uploadRawPicture,
    uploadChatPicture,
    uploadChatPictureSuccess,
    uploadChatPictureError,
    uploadKycFile,
    uploadKycFileSuccess,
    uploadKycFileError,
};
