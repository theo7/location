import { createAction, props } from '@ngrx/store';
import { TransportFee } from 'src/app/shared/models/models';

export type GlobalSettings = {
    acceptCommercialContact: boolean;
    acceptMailNotification: boolean;
};

export type UserSettings = GlobalSettings & {
    acceptPushNotification: boolean;
};

export const getSettings = createAction('[Settings] GET_SETTINGS');
export const getSettingsSuccess = createAction('[Settings] GET_SETTINGS_SUCCESS', props<{ settings: UserSettings }>());
export const getSettingsFailed = createAction('[Settings] GET_SETTINGS_FAILED', props<{ error: any }>());

export const setGlobalSettings = createAction('[Settings] SET_GLOBAL_SETTINGS', props<{ settings: GlobalSettings }>());
export const setGlobalSettingsSuccess = createAction(
    '[Notification] SET_GLOBAL_SETTINGS_SUCCESS',
    props<{ settings: GlobalSettings }>(),
);
export const setGlobalSettingsFailed = createAction(
    '[Notification] SET_GLOBAL_SETTINGS_FAILED',
    props<{ error: any }>(),
);

export const setNotificationSetting = createAction('[Settings] SET_NOTIFICATION_SETTING', props<{ value: boolean }>());
export const setNotificationSettingSuccess = createAction(
    '[Notification] SET_NOTIFICATION_SETTING_SUCCESS',
    props<{ value: boolean }>(),
);
export const setNotificationSettingFailed = createAction(
    '[Notification] SET_NOTIFICATION_SETTING_FAILED',
    props<{ error: any }>(),
);

export const getShippingsFeesSettings = createAction('[Settings] GET_SHIPPING_FEES_SETTINGS');
export const getShippingsFeesSettingsSuccess = createAction(
    '[Settings] GET_SHIPPING_FEES_SETTINGS_SUCCESS',
    props<{ fee: TransportFee }>(),
);
export const getShippingsFeesSettingsError = createAction(
    '[Settings] GET_SHIPPING_FEES_SETTINGS_FAILED',
    props<{ error: any }>(),
);
