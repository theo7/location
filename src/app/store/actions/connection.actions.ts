import { createAction, props } from '@ngrx/store';
import { ConnectionState } from '../reducers/connection.reducer';

export const setConnection = createAction('[Connection] SET_CONNECTION', props<{ connectionState: ConnectionState }>());
