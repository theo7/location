import { createAction, props } from '@ngrx/store';
import { PushNotificationList } from 'src/app/shared/models/models';

export const requestPermission = createAction('[Notification] REQUEST_PERMISSION');
export const requestPermissionSuccess = createAction(
    '[Notification] REQUEST_PERMISSION_SUCCESS',
    props<{ token: string }>(),
);
export const requestPermissionFailed = createAction(
    '[Notification] REQUEST_PERMISSION_FAILED',
    props<{ error: any }>(),
);

export const revokePermission = createAction('[Notification] REVOKE_PERMISSION');
export const revokePermissionSuccess = createAction(
    '[Notification] REVOKE_PERMISSION_SUCCESS',
    props<{ token: string }>(),
);
export const revokePermissionFailed = createAction('[Notification] REVOKE_PERMISSION_FAILED', props<{ error: any }>());

export const loadNotificationHistory = createAction('[Notification] LOAD_NOTIFICATION_HISTORY');
export const loadNotificationHistorySuccess = createAction(
    '[Notification] LOAD_NOTIFICATION_HISTORY_SUCCESS',
    props<{ pushNotificationList: PushNotificationList | null }>(),
);
export const loadNotificationHistoryError = createAction(
    '[Notification] LOAD_NOTIFICATION_HISTORY_ERROR',
    props<{ error: any }>(),
);

export const setNotificationRead = createAction('[Notification] SET_NOTIFICATION_READ');
export const setNotificationReadSuccess = createAction('[Notification] SET_NOTIFICATION_READ_SUCCESS');
export const setNotificationReadError = createAction(
    '[Notification] SET_NOTIFICATION_READ_ERROR',
    props<{ error: any }>(),
);
