import { createAction, props } from '@ngrx/store';
import { Page, Product } from '../../shared/models/models';

export const load = createAction('[Wishlist] LOAD');
export const loadSuccess = createAction('[Wishlist] LOAD_SUCCESS', props<{ productsPages: Page<Product> }>());
export const loadError = createAction('[Wishlist] LOAD_ERROR', props<{ error: any }>());

export const loadMore = createAction('[Wishlist] LOAD_MORE');
export const loadMoreSuccess = createAction('[Wishlist] LOAD_MORE_SUCCESS', props<{ productsPages: Page<Product> }>());
export const loadMoreError = createAction('[Wishlist] LOAD_MORE_ERROR', props<{ error: any }>());
