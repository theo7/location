import { createAction, props } from '@ngrx/store';
import { Page, Product } from '../../shared/models/models';

export const startSearchFromUrl = createAction('[Search] START_SEARCH_FROM_URL');

export const executeSearch = createAction('[Search] EXECUTE_SEARCH');
export const executeSearchSuccess = createAction(
    '[Search] EXECUTE_SEARCH_SUCCESS',
    props<{ products: Page<Product> }>(),
);
export const executeSearchError = createAction('[Search] EXECUTE_SEARCH_ERROR', props<{ error: any }>());

export const executeSearchNext = createAction('[Search] EXECUTE_SEARCH_NEXT');
export const executeSearchNextSuccess = createAction(
    '[Search] EXECUTE_SEARCH_NEXT_SUCCESS',
    props<{ products: Page<Product> }>(),
);
export const executeSearchNextError = createAction('[Search] EXECUTE_SEARCH_NEXT_ERROR', props<{ error: any }>());

export const executeSearchCountSuccess = createAction(
    '[Search] EXECUTE_SEARCH_COUNT_SUCCESS',
    props<{ total: number }>(),
);
export const executeSearchCountError = createAction('[Search] EXECUTE_SEARCH_COUNT_ERROR', props<{ error: any }>());

export const setShowFilter = createAction('[SEARCH] SET_SHOW_FILTER', props<{ showFilter: boolean }>());
export const setActivateRefresher = createAction(
    '[SEARCH] SET_ACTIVATE_REFRESHER',
    props<{ activateRefresher: boolean }>(),
);
