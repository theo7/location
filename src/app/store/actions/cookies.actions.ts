import { createAction, props } from '@ngrx/store';
import { CookieConsentSelection } from '../../features/cookie-consent/services/cookie-consent.service';

export const openConfigureModal = createAction('[Cookies] OPEN_CONFIGURE_MODAL');

export const acceptAll = createAction('[Cookies] ACCEPT_ALL');
export const accept = createAction('[Cookies] ACCEPT', props<{ selection: CookieConsentSelection }>());
export const acceptSuccess = createAction('[Cookies] ACCEPT_SUCCESS');
export const acceptError = createAction('[Cookies] ACCEPT_ERROR', props<{ error: any }>());

export const denyAll = createAction('[Cookies] DENY_ALL');
