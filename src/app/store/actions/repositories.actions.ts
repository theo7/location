import { createAction, props } from '@ngrx/store';
import { Brand, Color, Condition } from '../../shared/models/models';

// getRepositories
export const getRepositories = createAction('[Repositories] GET_REPOSITORIES');
export const getRepositoriesSuccess = createAction(
    '[Repositories] GET_REPOSITORIES_SUCCESS',
    props<{ brands: Brand[]; colors: Color[]; conditions: Condition[]; defaultBrand: Brand; popularBrands: Brand[] }>(),
);
export const getRepositoriesError = createAction('[Repositories] GET_REPOSITORIES_ERROR');
