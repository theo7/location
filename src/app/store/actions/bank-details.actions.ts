import { createAction, props } from '@ngrx/store';
import { AccountStatus, DocumentStatus, IbanStatus, UserDocuments } from 'src/app/shared/models/models';

export const loadBankDetails = createAction('[Bank Details] LOAD');
export const loadBankDetailsSuccess = createAction('[Bank Details] LOAD_SUCCESS', props<{ response: UserDocuments }>());
export const loadBankDetailsError = createAction('[Bank Details] LOAD_ERROR', props<{ error: any }>());

export type DocumentType = 'iban' | 'rib' | 'identity';

export const documentUploaded = createAction(
    '[Bank Details] Document uploaded',
    props<{ documentType: DocumentType; status: DocumentStatus | IbanStatus }>(),
);

export const accountUpdated = createAction('[Bank Details] Account updated', props<{ status: AccountStatus }>());
