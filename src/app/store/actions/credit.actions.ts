import { createAction, props } from '@ngrx/store';
import { CashHistory, PageLw } from '../../shared/models/models';

export const getCashHistory = createAction('[User] GET_CASH_HISTORY', props<{ page: number }>());
export const getCashHistorySuccess = createAction(
    '[User] GET_CASH_HISTORY_SUCCESS',
    props<{ history: PageLw<CashHistory> }>(),
);
export const getCashHistoryError = createAction('[User] GET_CASH_HISTORY_ERROR', props<{ error: any }>());
export const resetCashHistory = createAction('[User] RESET_CASH_HISTORY');
