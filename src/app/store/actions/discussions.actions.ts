import { createAction, props } from '@ngrx/store';
import { Discussion, Lot, NewChatMessage, Product, RemunerationMode } from '../../shared/models/models';

// getDiscussion
export const getDiscussion = createAction('[Discussion] GET_DISCUSSION', props<{ id: number }>());
export const getDiscussionSuccess = createAction(
    '[Discussion] GET_DISCUSSION_SUCCESS',
    props<{ discussion: Discussion }>(),
);
export const getDiscussionError = createAction('[Discussion] GET_DISCUSSION_ERROR');

// getUserDiscussions
export const getUserDiscussions = createAction('[Discussion] GET_USER_DISCUSSIONS');
export const getUserDiscussionsSuccess = createAction(
    '[Discussion] GET_USER_DISCUSSIONS_SUCCESS',
    props<{ discussions: Discussion[] }>(),
);
export const getUserDiscussionsError = createAction('[Discussion] GET_USER_DISCUSSIONS_ERROR', props<{ error: any }>());

// startDiscussion
export const startDiscussion = createAction('[Discussion] START_DISCUSSION', props<{ productId: number }>());
export const startListDiscussion = createAction(
    '[Discussion] START_LIST_DISCUSSION',
    props<{ productIds: number[] }>(),
);
export const startDiscussionSuccess = createAction(
    '[Discussion] START_DISCUSSION_SUCCESS',
    props<{ discussion: Discussion }>(),
);
export const startDiscussionError = createAction('[Discussion] START_DISCUSSION_ERROR');

// removeProductFromLot
export const removeProductFromLot = createAction(
    '[Discussion] REMOVE_PRODUCT_FROM_LOT',
    props<{ lotId: number; productId: number }>(),
);
export const removeProductFromLotSuccess = createAction(
    '[Discussion] REMOVE_PRODUCT_FROM_LOT_SUCCESS',
    props<{ lot: Lot }>(),
);
export const removeProductFromLotError = createAction('[Discussion] REMOVE_PRODUCT_FROM_LOT_ERROR');

// addProductToLot
export const addProductToLot = createAction(
    '[Discussion] ADD_PRODUCT_TO_LOT',
    props<{ lotId: number; productId: number }>(),
);
export const addProductToLotSuccess = createAction('[Discussion] ADD_PRODUCT_TO_LOT_SUCCESS', props<{ lot: Lot }>());
export const addProductToLotError = createAction('[Discussion] ADD_PRODUCT_TO_LOT_ERROR');

// setLotProducts
export const setLotProducts = createAction(
    '[Discussion] SET_LOT_PRODUCTS',
    props<{ lotId: number; products: Product[] }>(),
);
export const setLotProductsSuccess = createAction('[Discussion] SET_LOT_PRODUCTS_SUCCESS', props<{ lot: Lot }>());
export const setLotProductsError = createAction('[Discussion] SET_LOT_PRODUCTS_ERROR');

// confirmSell
export const confirmSell = createAction(
    '[Discussion] CONFIRM_SELL',
    props<{ lotId: number; remunerationMode: RemunerationMode }>(),
);
export const confirmSellSuccess = createAction('[Discussion] CONFIRM_SELL_SUCCESS', props<{ lot: Lot }>());
export const confirmSellError = createAction('[Discussion] CONFIRM_SELL_ERROR', props<{ lotId: number }>());

// confirmProfessionalSell
export const confirmProfessionalSell = createAction(
    '[Discussion] CONFIRM_PROFESSIONAL_SELL',
    props<{ lotId: number; trackingNumber: number }>(),
);
export const confirmProfessionalSellSuccess = createAction(
    '[Discussion] CONFIRM_PROFESSIONAL_SELL_SUCCESS',
    props<{ lot: Lot }>(),
);
export const confirmProfessionalSellError = createAction(
    '[Discussion] CONFIRM_PROFESSIONAL_SELL_ERROR',
    props<{ lotId: number }>(),
);

// declineProfessionalSell
export const declineProfessionalSell = createAction(
    '[Discussion] DECLINE_PROFESSIONAL_SELL',
    props<{ lotId: number }>(),
);
export const declineProfessionalSellSuccess = createAction(
    '[Discussion] DECLINE_PROFESSIONAL_SELL_SUCCESS',
    props<{ lot: Lot }>(),
);
export const declineProfessionalSellError = createAction(
    '[Discussion] DECLINE_PROFESSIONAL_SELL_ERROR',
    props<{ lotId: number }>(),
);

// cancelSell
export const cancelSell = createAction('[Discussion] CANCEL_SELL', props<{ lotId: number }>());
export const cancelSellSuccess = createAction('[Discussion] CANCEL_SELL_SUCCESS', props<{ lot: Lot }>());
export const cancelSellError = createAction('[Discussion] CANCEL_SELL_ERROR', props<{ lotId: number }>());

// cancelPackage
export const cancelPackage = createAction('[Discussion] CANCEL_PACKAGE', props<{ lotId: number }>());
export const cancelPackageSuccess = createAction('[Discussion] CANCEL_PACKAGE_SUCCESS', props<{ lot: Lot }>());
export const cancelPackageError = createAction('[Discussion] CANCEL_PACKAGE_ERROR', props<{ lotId: number }>());

// declineSell
export const declineSell = createAction('[Discussion] DECLINE_SELL', props<{ lotId: number }>());
export const declineSellSuccess = createAction('[Discussion] DECLINE_SELL_SUCCESS', props<{ lot: Lot }>());
export const declineSellError = createAction('[Discussion] DECLINE_SELL_ERROR', props<{ lotId: number }>());

// confirmPackageCompliant
export const confirmPackageCompliant = createAction(
    '[Discussion] CONFIRM_PACKAGE_COMPLIANT',
    props<{ lotId: number }>(),
);
export const confirmPackageCompliantSuccess = createAction(
    '[Discussion] CONFIRM_PACKAGE_COMPLIANT_SUCCESS',
    props<{ lot: Lot }>(),
);
export const confirmPackageCompliantError = createAction(
    '[Discussion] CONFIRM_PACKAGE_COMPLIANT_ERROR',
    props<{ lotId: number }>(),
);

// confirmPackageNonCompliant
export const confirmPackageNonCompliant = createAction(
    '[Discussion] CONFIRM_PACKAGE_NON_COMPLIANT',
    props<{ lotId: number }>(),
);
export const confirmPackageNonCompliantSuccess = createAction(
    '[Discussion] CONFIRM_PACKAGE_NON_COMPLIANT_SUCCESS',
    props<{ lot: Lot }>(),
);
export const confirmPackageNonCompliantError = createAction(
    '[Discussion] CONFIRM_PACKAGE_NON_COMPLIANT_ERROR',
    props<{ lotId: number }>(),
);

// sendMessage
export const sendMessage = createAction('[Discussion] SEND_MESSAGE', props<{ message: NewChatMessage }>());
export const sendMessageSuccess = createAction(
    '[Discussion] SEND_MESSAGE_SUCCESS',
    props<{ discussion: Discussion }>(),
);
export const sendMessageError = createAction('[Discussion] SEND_MESSAGE_ERROR');

// sendMessage
export const updateDiscussion = createAction('[Discussion] UPDATE_DISCUSSION', props<{ discussion: Discussion }>());

export const archiveDiscussion = createAction('[Discussion] ARCHIVE_DISCUSSION', props<{ discussionId: number }>());
export const archiveDiscussionSuccess = createAction(
    '[Discussion] ARCHIVE_DISCUSSION_SUCCESS',
    props<{ discussionId: number }>(),
);
export const archiveDiscussionError = createAction('[Discussion] ARCHIVE_DISCUSSION_ERROR', props<{ error: any }>());

// set discussion read
export const readDiscussion = createAction(
    '[Discussion] READ_DISCUSSION',
    props<{ roomId: number | string; isCurrentBuyer: boolean }>(),
);
export const readDiscussionSuccess = createAction(
    '[Discussion] READ_DISCUSSION_SUCCESS',
    props<{ roomId: number | string }>(),
);
export const readDiscussionError = createAction('[Discussion] READ_DISCUSSION_ERROR', props<{ error: any }>());

export const reportUser = createAction(
    '[Discussion] REPORT_USER',
    props<{ discussionId: any; newDiscussionReport: any }>(),
);

export const reportUserSuccess = createAction('[Discussion] REPORT_USER_SUCCESS');
export const reportUserError = createAction('[Discussion] REPORT_USER_ERROR', props<{ error: any }>());

export const setInstantBuy = createAction('[Discussion] SET_INSTANT_BUY', props<{ lotId: number | null }>());

export const archiveTemporaryDiscussion = createAction(
    '[Discussion] ARCHIVE_TEMPORARY_DISCUSSION',
    props<{ discussionId: number }>(),
);

export const archiveTemporaryDiscussionSuccess = createAction(
    '[Discussion] ARCHIVE_TEMPORARY_DISCUSSION_SUCCESS',
    props<{ discussionId: number }>(),
);

export const archiveTemporaryDiscussionError = createAction(
    '[Discussion] ARCHIVE_TEMPORARY_DISCUSSION_ERROR',
    props<{ error: any }>(),
);
