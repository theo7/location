import { createAction, props } from '@ngrx/store';
import {
    Brand,
    Category,
    Color,
    Condition,
    CreateSearch,
    FilterType,
    ProductFilter,
    ProductFilterSort,
    ProductType,
    Search,
    Seasonality,
    Size,
    SubCategory,
} from '../../shared/models/models';

const FEATURE = '[FILTERS]';
type baseProps = { key: FilterType };

// GENERAL
const openDialog = createAction(`${FEATURE} OPEN_DIALOG`, props<baseProps>());
const set = createAction(`${FEATURE} SET`, props<baseProps & { filter: ProductFilter }>());
const reset = createAction(`${FEATURE} RESET`, props<baseProps>());
const setKeyword = createAction(`${FEATURE} SET_KEYWORD`, props<baseProps & { keyword: string }>());

// CATEGORY
const setCategory = createAction(`${FEATURE} SET_CATEGORY`, props<baseProps & { category: Category }>());
const resetCategory = createAction(`${FEATURE} RESET_CATEGORY`, props<baseProps>());

// SUB CATEGORY
const setSubCategory = createAction(
    `${FEATURE} SET_SUB_CATEGORY`,
    props<baseProps & { subCategory: SubCategory; category: Category }>(),
);
const resetSubCategory = createAction(`${FEATURE} RESET_SUB_CATEGORY`, props<baseProps>());

// PRODUCT TYPE
const addProductType = createAction(
    `${FEATURE} ADD_PRODUCT_TYPE`,
    props<baseProps & { productType: ProductType; category: Category; subCategory: SubCategory }>(),
);
const removeProductType = createAction(
    `${FEATURE} REMOVE_PRODUCT_TYPE`,
    props<baseProps & { productType: ProductType }>(),
);

// SIZE
const addSize = createAction(`${FEATURE} ADD_SIZE`, props<baseProps & { size: Size }>());
const removeSize = createAction(`${FEATURE} REMOVE_SIZE`, props<baseProps & { size: Size }>());

// BRAND
const addBrand = createAction(`${FEATURE} ADD_BRAND`, props<baseProps & { brand: Brand }>());
const removeBrand = createAction(`${FEATURE} REMOVE_BRAND`, props<baseProps & { brand: Brand }>());

// COLOR
const addColor = createAction(`${FEATURE} ADD_COLOR`, props<baseProps & { color: Color }>());
const removeColor = createAction(`${FEATURE} REMOVE_COLOR`, props<baseProps & { color: Color }>());

// CONDITION
const addCondition = createAction(`${FEATURE} ADD_CONDITION`, props<baseProps & { condition: Condition }>());
const removeCondition = createAction(`${FEATURE} REMOVE_CONDITION`, props<baseProps & { condition: Condition }>());

// PRICE
const setMinPrice = createAction(`${FEATURE} SET_MIN_PRICE`, props<baseProps & { price: number }>());
const setMaxPrice = createAction(`${FEATURE} SET_MAX_PRICE`, props<baseProps & { price: number }>());
const resetMinPrice = createAction(`${FEATURE} RESET_MIN_PRICE`, props<baseProps>());
const resetMaxPrice = createAction(`${FEATURE} RESET_MAX_PRICE`, props<baseProps>());

// SEASONALITY
const setSeasonality = createAction(`${FEATURE} SET_SEASONALITY`, props<baseProps & { seasonality: Seasonality }>());
const resetSeasonality = createAction(`${FEATURE} RESET_SEASONALITY`, props<baseProps>());

// SORT
const setSort = createAction(`${FEATURE} SET_SORT`, props<baseProps & { sort: ProductFilterSort }>());
const resetSort = createAction(`${FEATURE} RESET_SORT`, props<baseProps>());

const search = createAction(`${FEATURE} SEARCH`, props<baseProps>());

// SEARCH HISTORY
const loadSearchHistory = createAction('[FILTERS] LOAD_SEARCH_HISTORY');
const loadSearchHistorySuccess = createAction(
    '[FILTERS] LOAD_SEARCH_HISTORY_SUCCESS',
    props<{ searchList: Search[] }>(),
);
const loadSearchHistoryError = createAction('[FILTERS] LOAD_SEARCH_HISTORY_ERROR', props<{ error: any }>());

const addCurrentSearch = createAction('[FILTERS] ADD_CURRENT_SEARCH');
const saveCurrentSearch = createAction('[FILTERS] SAVE_CURRENT_SEARCH');

const addSearch = createAction('[FILTERS] ADD_SEARCH', props<{ search: CreateSearch }>());
const addSearchSuccess = createAction('[FILTERS] ADD_SEARCH_SUCCESS', props<{ search: Search }>());
const addSearchError = createAction('[FILTERS] ADD_SEARCH_SUCCESS', props<{ error: any }>());

const saveSearch = createAction('[FILTERS] SAVE_SEARCH', props<{ id: number }>());
const saveSearchSuccess = createAction('[FILTERS] SAVE_SEARCH_SUCCESS', props<{ search: Search }>());
const saveSearchError = createAction('[FILTERS] SAVE_SEARCH_ERROR', props<{ error: any }>());

const unSaveSearch = createAction('[FILTERS] UNSAVE_SEARCH', props<{ id: number }>());
const unSaveSearchSuccess = createAction('[FILTERS] UNSAVE_SEARCH_SUCCESS', props<{ search: Search }>());
const unSaveSearchError = createAction('[FILTERS] UNSAVE_SEARCH_ERROR', props<{ error: any }>());

const applySearchFilter = createAction('[FILTERS] APPLY_SEARCH_FILTER', props<{ search: Search }>());

const openSearchHistoryDialog = createAction('[FILTERS] OPEN_SEARCH_HISTORY_DIALOG');
const resetSearchHistory = createAction('[FILTERS] RESET_SEARCH_HISTORY');

export const filtersActions = {
    search,
    openDialog,
    set,
    reset,
    setKeyword,
    setCategory,
    resetCategory,
    setSubCategory,
    resetSubCategory,
    addProductType,
    removeProductType,
    addSize,
    removeSize,
    addBrand,
    removeBrand,
    addColor,
    removeColor,
    addCondition,
    removeCondition,
    setMinPrice,
    setMaxPrice,
    resetMinPrice,
    resetMaxPrice,
    setSeasonality,
    resetSeasonality,
    setSort,
    resetSort,
    loadSearchHistory,
    loadSearchHistorySuccess,
    loadSearchHistoryError,
    addCurrentSearch,
    saveCurrentSearch,
    addSearch,
    addSearchSuccess,
    addSearchError,
    saveSearch,
    saveSearchSuccess,
    saveSearchError,
    unSaveSearch,
    unSaveSearchSuccess,
    unSaveSearchError,
    applySearchFilter,
    openSearchHistoryDialog,
    resetSearchHistory,
};
