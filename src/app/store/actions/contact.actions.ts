import { createAction, props } from '@ngrx/store';
import { ContactSupportInfo } from 'src/app/shared/models/models';

// contact second hand support
export const sendEmailContactSupport = createAction(
    '[Contact] CONTACT_SUPPORT',
    props<{ contactSupportInfo: ContactSupportInfo }>(),
);
export const sendEmailContactSupportSuccess = createAction('[Contact] CONTACT_SUPPORT_SUCCESS');
export const sendEmailContactSupportError = createAction('[Contact] CONTACT_SUPPORT_ERROR', props<{ error: any }>());
