import { createAction, props } from '@ngrx/store';
import { UserProfile } from '../../shared/models/models';

const openDeleteAccountModal = createAction('[Profile] OPEN_DELETE_ACCOUNT_MODAL');

const deleteAccount = createAction('[Profile] DELETE_ACCOUNT');
const deleteAccountSuccess = createAction('[Profile] DELETE_ACCOUNT_SUCCESS');
const deleteAccountError = createAction('[Profile] DELETE_ACCOUNT_ERROR', props<{ error: any }>());

const load = createAction('[Profile] LOAD', props<{ userId: number }>());
const loadSuccess = createAction('[Profile] LOAD_SUCCESS', props<{ profile: UserProfile }>());
const loadError = createAction('[Profile] LOAD_ERROR', props<{ error: any }>());

export const profileActions = {
    openDeleteAccountModal,
    deleteAccount,
    deleteAccountSuccess,
    deleteAccountError,
    load,
    loadSuccess,
    loadError,
};
