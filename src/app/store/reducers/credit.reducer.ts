import { createReducer, on } from '@ngrx/store';
import { CashHistory } from 'src/app/shared/models/models';
import * as CreditActions from '../actions/credit.actions';

export type CashState = {
    history: CashHistory[];
    maxPages: number | undefined;
    pageLoad: number;
    loading: boolean;
};

const initialCashState: CashState = { history: [], maxPages: undefined, pageLoad: 0, loading: false };

export const creditReducer = createReducer(
    initialCashState,
    on(CreditActions.getCashHistorySuccess, (state, payload) => ({
        ...state,
        history: [...state.history, ...payload.history.content],
        maxPages: state.maxPages ? state.maxPages : payload.history.totalPages,
        pageLoad: state.pageLoad + 1,
        loading: false,
    })),
    on(CreditActions.getCashHistory, (state, _) => ({
        ...state,
        loading: true,
    })),
    on(CreditActions.getCashHistoryError, (state, _) => ({
        ...state,
        loading: false,
    })),
    on(CreditActions.resetCashHistory, (state, _) => initialCashState),
);
