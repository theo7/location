import { createReducer, on } from '@ngrx/store';
import { Lot } from '../../shared/models/models';
import * as LotActions from '../actions/lot.actions';
import * as OrdersActions from '../actions/orders.actions';

export interface OrdersState {
    sales: {
        loading: boolean;
        values: Lot[];
    };
    purchases: {
        loading: boolean;
        values: Lot[];
    };
}

export const initialOrdersState: OrdersState = {
    sales: {
        loading: false,
        values: [],
    },
    purchases: {
        loading: false,
        values: [],
    },
};

export const ordersReducer = createReducer(
    initialOrdersState,

    on(OrdersActions.loadSales, state => {
        return {
            ...state,
            sales: { ...state.sales, loading: true },
        };
    }),
    on(OrdersActions.loadSalesSuccess, (state, action) => {
        return {
            ...state,
            sales: { ...state.sales, values: action.lots, loading: false },
        };
    }),
    on(OrdersActions.loadSalesError, state => {
        return {
            ...state,
            sales: { ...state.sales, loading: false },
        };
    }),

    on(OrdersActions.loadPurchases, state => {
        return {
            ...state,
            purchases: { ...state.purchases, loading: true },
        };
    }),
    on(OrdersActions.loadPurchasesSuccess, (state, action) => {
        return {
            ...state,
            purchases: { ...state.purchases, values: action.lots, loading: false },
        };
    }),
    on(OrdersActions.loadPurchasesError, state => {
        return {
            ...state,
            purchases: { ...state.purchases, loading: false },
        };
    }),
    on(LotActions.changeRemunerationModeSuccess, (state, { lot }) => {
        const values = state.sales.values.map(l => {
            return l.id === lot.id ? lot : l;
        });
        return {
            ...state,
            sales: { ...state.sales, values },
        };
    }),
);
