import { createReducer, on } from '@ngrx/store';
import { TransportFee } from 'src/app/shared/models/models';
import * as SettingsActions from '../actions/settings.actions';
import { UserSettings } from '../actions/settings.actions';

export interface SettingsState {
    loading: boolean;
    settings: UserSettings | undefined;
    shippingsFees: TransportFee | undefined;
}

export const initialSettingsState: SettingsState = {
    loading: false,
    settings: undefined,
    shippingsFees: undefined,
};

export const settingsReducer = createReducer(
    initialSettingsState,

    on(SettingsActions.getSettings, state => ({ ...state, loading: true })),
    on(SettingsActions.getSettingsSuccess, (state, action) => ({
        ...state,
        loading: false,
        settings: action.settings,
    })),
    on(SettingsActions.getSettingsFailed, state => ({ ...state, loading: false })),

    on(SettingsActions.setGlobalSettingsSuccess, (state, action) => ({
        ...state,
        settings: { ...state.settings, ...action.settings } as UserSettings,
    })),
    on(SettingsActions.setNotificationSettingSuccess, (state, action) => ({
        ...state,
        settings: { ...state.settings, acceptPushNotification: action.value } as UserSettings,
    })),
    on(SettingsActions.getShippingsFeesSettings, state => ({ ...state, loading: false })),
    on(SettingsActions.getShippingsFeesSettingsSuccess, (state, action) => ({
        ...state,
        shippingsFees: action.fee,
    })),
);
