import { createReducer, on } from '@ngrx/store';
import { Product } from '../../shared/models/models';
import * as WishlistActions from '../actions/wishlist.actions';

export interface WishlistState {
    products: Product[];
    total: number;
    page: number;
    loading: boolean;
    error: boolean;
}

export const initialState: WishlistState = {
    products: [],
    total: 0,
    page: 0,
    loading: false,
    error: false,
};

export const wishlistReducer = createReducer<WishlistState>(
    initialState,

    on(WishlistActions.load, state => ({ ...state, loading: true, error: false })),
    on(WishlistActions.loadSuccess, (state, { productsPages }) => ({
        ...state,
        products: productsPages.content,
        total: productsPages.totalElements,
        page: 0,
        loading: false,
        error: false,
    })),
    on(WishlistActions.loadError, state => ({ ...state, loading: false, error: true })),

    on(WishlistActions.loadMore, state => ({ ...state, loading: true, error: false })),
    on(WishlistActions.loadMoreSuccess, (state, { productsPages }) => ({
        ...state,
        products: [...state.products, ...productsPages.content],
        page: state.page + 1,
        loading: false,
        error: false,
    })),
    on(WishlistActions.loadMoreError, state => ({ ...state, loading: false, error: true })),
);
