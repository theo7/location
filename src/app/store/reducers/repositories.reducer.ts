import { createReducer, on } from '@ngrx/store';
import { Brand, Color, Condition } from '../../shared/models/models';
import * as RepositoriesActions from '../actions/repositories.actions';

export interface RepositoriesState {
    brands: Brand[];
    colors: Color[];
    conditions: Condition[];
    defaultBrand: Brand | undefined;
    popularBrands: Brand[];
    loading: boolean;
    error: boolean;
}

const initialRepositoriesState: RepositoriesState = {
    brands: [],
    colors: [],
    conditions: [],
    defaultBrand: undefined,
    popularBrands: [],
    loading: false,
    error: false,
};

export const repositoriesReducer = createReducer(
    initialRepositoriesState,

    on(RepositoriesActions.getRepositories, state => ({ ...state, loading: true, error: false })),
    on(
        RepositoriesActions.getRepositoriesSuccess,
        (state, { brands, colors, conditions, defaultBrand, popularBrands }) => ({
            brands,
            colors,
            conditions,
            defaultBrand,
            popularBrands,
            loading: false,
            error: false,
        }),
    ),
    on(RepositoriesActions.getRepositoriesError, state => ({ ...state, loading: false, error: true })),
);
