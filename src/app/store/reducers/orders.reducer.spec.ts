import { Lot } from 'src/app/shared/models/models';
import * as OrdersActions from '../actions/orders.actions';
import { initialOrdersState, ordersReducer, OrdersState } from './orders.reducer';

describe('orders.reducer', () => {
    it('should have initial state', () => {
        const action = { type: 'NOOP' };
        const state = ordersReducer(initialOrdersState, action);

        expect(state.sales.values).toEqual([]);
        expect(state.sales.loading).toBeFalsy();
        expect(state.purchases.values).toEqual([]);
        expect(state.purchases.loading).toBeFalsy();
    });

    describe('loadSales', () => {
        it('should be loading', () => {
            const action = OrdersActions.loadSales();
            const state = ordersReducer(initialOrdersState, action);

            expect(state.sales.loading).toBeTruthy();
        });
    });

    describe('loadSalesSuccess', () => {
        let afterLoadingState: OrdersState;

        beforeEach(() => {
            const action = OrdersActions.loadSales();
            afterLoadingState = ordersReducer(initialOrdersState, action);
        });

        it('should not be loading', () => {
            const lots = [
                {
                    id: 1,
                },
            ] as Lot[];
            const action = OrdersActions.loadSalesSuccess({ lots });
            const state = ordersReducer(afterLoadingState, action);

            expect(state.sales.loading).toBeFalsy();
        });

        it('should have sales', () => {
            const lots = [
                {
                    id: 1,
                },
            ] as Lot[];
            const action = OrdersActions.loadSalesSuccess({ lots });
            const state = ordersReducer(afterLoadingState, action);

            expect(state.sales.values).toBe(lots);
        });
    });

    describe('loadSalesError', () => {
        let afterLoadingState: OrdersState;

        beforeEach(() => {
            const action = OrdersActions.loadSales();
            afterLoadingState = ordersReducer(initialOrdersState, action);
        });

        it('should not be loading', () => {
            const action = OrdersActions.loadSalesError({ error: 'Error' });
            const state = ordersReducer(afterLoadingState, action);

            expect(state.sales.loading).toBeFalsy();
        });
    });

    describe('loadPurchases', () => {
        it('should be loading', () => {
            const action = OrdersActions.loadPurchases();
            const state = ordersReducer(initialOrdersState, action);

            expect(state.purchases.loading).toBeTruthy();
        });
    });

    describe('loadPurchasesSuccess', () => {
        let afterLoadingState: OrdersState;

        beforeEach(() => {
            const action = OrdersActions.loadPurchases();
            afterLoadingState = ordersReducer(initialOrdersState, action);
        });

        it('should not be loading', () => {
            const lots = [
                {
                    id: 1,
                },
            ] as Lot[];
            const action = OrdersActions.loadPurchasesSuccess({ lots });
            const state = ordersReducer(afterLoadingState, action);

            expect(state.purchases.loading).toBeFalsy();
        });

        it('should have purchases', () => {
            const lots = [
                {
                    id: 1,
                },
            ] as Lot[];
            const action = OrdersActions.loadPurchasesSuccess({ lots });
            const state = ordersReducer(afterLoadingState, action);

            expect(state.purchases.values).toBe(lots);
        });
    });

    describe('loadPurchasesError', () => {
        let afterLoadingState: OrdersState;

        beforeEach(() => {
            const action = OrdersActions.loadPurchases();
            afterLoadingState = ordersReducer(initialOrdersState, action);
        });

        it('should not be loading', () => {
            const action = OrdersActions.loadPurchasesError({ error: 'Error' });
            const state = ordersReducer(afterLoadingState, action);

            expect(state.purchases.loading).toBeFalsy();
        });
    });
});
