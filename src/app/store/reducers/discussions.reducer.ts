import { createReducer, on } from '@ngrx/store';
import { Discussion, DiscussionStatus, Lot } from '../../shared/models/models';
import * as DiscussionsActions from '../actions/discussions.actions';
import * as LotActions from '../actions/lot.actions';
import * as UserActions from '../actions/user.actions';

export interface DiscussionsState {
    discussions: Discussion[];
    instantBuyLotId: number | null;
    loading: boolean;
    error: boolean | undefined;
}

const initialDiscussionsState: DiscussionsState = {
    discussions: [],
    instantBuyLotId: null,
    loading: false,
    error: false,
};

export const discussionsReducer = createReducer(
    initialDiscussionsState,

    on(DiscussionsActions.getUserDiscussions, state => ({ ...state, loading: true, error: false })),
    on(DiscussionsActions.getUserDiscussionsSuccess, (state, { discussions }) => ({
        ...state,
        discussions,
        loading: false,
        error: false,
    })),
    on(DiscussionsActions.getUserDiscussionsError, state => ({ ...state, loading: false, error: true })),

    on(DiscussionsActions.startDiscussion, state => ({ ...state, loading: true, error: false })),
    on(DiscussionsActions.startListDiscussion, state => ({ ...state, loading: true, error: false })),
    on(DiscussionsActions.startDiscussionSuccess, (state, { discussion }) => {
        const discussions = [discussion, ...state.discussions];
        return {
            ...state,
            discussions,
            loading: false,
            error: false,
        };
    }),
    on(DiscussionsActions.startDiscussionError, state => ({ ...state, loading: false, error: true })),

    on(DiscussionsActions.removeProductFromLot, state => ({ ...state, loading: true, error: false })),
    on(DiscussionsActions.removeProductFromLotSuccess, (state, { lot }) => {
        const discussionToUpdate: Discussion = {
            ...state.discussions.find(d => d.lot?.id === lot.id),
            status: DiscussionStatus.DISCUSSION_CANCEL,
            lot,
        };
        let discussions = state.discussions.map(d => (d.id === discussionToUpdate.id ? discussionToUpdate : d));

        return {
            ...state,
            discussions,
            loading: false,
            error: false,
        };
    }),
    on(DiscussionsActions.removeProductFromLotError, state => ({ ...state, loading: false, error: true })),

    on(DiscussionsActions.addProductToLot, state => ({ ...state, loading: true, error: false })),
    on(DiscussionsActions.addProductToLotSuccess, (state, { lot }) => {
        return lotSuccessHandler(state, lot);
    }),
    on(DiscussionsActions.addProductToLotError, state => ({ ...state, loading: false, error: true })),

    on(DiscussionsActions.setLotProducts, state => ({ ...state, loading: true, error: false })),
    on(DiscussionsActions.setLotProductsSuccess, (state, { lot }) => {
        return lotSuccessHandler(state, lot);
    }),
    on(DiscussionsActions.setLotProductsError, state => ({ ...state, loading: false, error: true })),

    on(DiscussionsActions.confirmSell, state => ({ ...state, loading: true, error: false })),
    on(DiscussionsActions.confirmSellSuccess, (state, { lot }) => {
        return lotSuccessHandler(state, lot);
    }),
    on(DiscussionsActions.confirmSellError, state => ({
        ...state,
        loading: false,
        error: true,
    })),
    // confirmProfessionalSell
    on(DiscussionsActions.confirmProfessionalSell, state => ({ ...state, loading: true, error: false })),
    on(DiscussionsActions.confirmProfessionalSellSuccess, (state, { lot }) => {
        return lotSuccessHandler(state, lot);
    }),
    on(DiscussionsActions.confirmProfessionalSellError, state => ({ ...state, loading: false, error: true })),
    // declineProfessionalSell
    on(DiscussionsActions.declineProfessionalSell, state => ({ ...state, loading: true, error: false })),
    on(DiscussionsActions.declineProfessionalSellSuccess, (state, { lot }) => {
        return lotSuccessHandler(state, lot);
    }),
    on(DiscussionsActions.declineProfessionalSellError, state => ({ ...state, loading: false, error: true })),
    // cancelSell
    on(DiscussionsActions.cancelSell, state => ({ ...state, loading: true, error: false })),
    on(DiscussionsActions.cancelSellSuccess, (state, { lot }) => {
        return lotSuccessHandler(state, lot);
    }),
    on(DiscussionsActions.cancelSellError, state => ({ ...state, loading: false, error: true })),

    on(DiscussionsActions.declineSell, state => ({ ...state, loading: true, error: false })),
    on(DiscussionsActions.declineSellSuccess, (state, { lot }) => {
        return lotSuccessHandler(state, lot);
    }),
    on(DiscussionsActions.declineSellError, state => ({ ...state, loading: false, error: true })),

    on(UserActions.logoutSuccess, state => ({ ...state, discussions: [], loading: false, error: false })),

    on(DiscussionsActions.confirmPackageCompliant, state => ({ ...state, loading: true, error: false })),
    on(DiscussionsActions.confirmPackageCompliantSuccess, (state, { lot }) => {
        return lotSuccessHandler(state, lot);
    }),
    on(DiscussionsActions.confirmPackageCompliantError, state => ({ ...state, loading: false, error: true })),

    on(DiscussionsActions.confirmPackageNonCompliant, state => ({ ...state, loading: true, error: false })),
    on(DiscussionsActions.confirmPackageNonCompliantSuccess, (state, { lot }) => {
        return lotSuccessHandler(state, lot);
    }),
    on(DiscussionsActions.confirmPackageNonCompliantError, state => ({ ...state, loading: false, error: true })),

    on(DiscussionsActions.sendMessage, state => ({ ...state, loading: true, error: false })),
    on(DiscussionsActions.sendMessageSuccess, (state, { discussion }) => {
        const discussions = updateDiscussion(state, discussion);
        return {
            ...state,
            discussions,
            loading: false,
            error: false,
        };
    }),
    on(DiscussionsActions.sendMessageError, state => ({ ...state, loading: false, error: true })),

    on(DiscussionsActions.updateDiscussion, (state, { discussion }) => {
        const discussions = updateDiscussion(state, discussion);
        return {
            ...state,
            discussions,
            loading: false,
            error: false,
        };
    }),

    on(DiscussionsActions.archiveDiscussionSuccess, (state, { discussionId }) => {
        let discussions = [...state.discussions];
        discussions = [...discussions.filter(discussion => discussion.id !== discussionId)];
        return {
            ...state,
            discussions,
            loading: false,
            error: false,
        };
    }),

    // discussion read
    on(DiscussionsActions.readDiscussion, state => ({ ...state, loading: true, error: undefined })),
    on(DiscussionsActions.readDiscussionSuccess, (state, { roomId }) => {
        return {
            ...state,
            discussions: state.discussions.map(d => (d.id === roomId && roomId === 'kiabi' ? { ...d, read: true } : d)),
            loading: false,
        };
    }),
    on(DiscussionsActions.readDiscussionError, (state, { error }) => ({ ...state, loading: false, error })),
    // instant buy
    on(DiscussionsActions.setInstantBuy, (state, { lotId }) => ({
        ...state,
        instantBuyLotId: lotId,
    })),
    // archive discussion
    on(DiscussionsActions.archiveTemporaryDiscussionSuccess, (state, { discussionId }) => {
        const discussions = state.discussions.filter(discussion => discussion.id !== discussionId);
        return {
            ...state,
            discussions,
            loading: false,
            error: false,
        };
    }),
    // change remuneration mode
    on(LotActions.changeRemunerationModeSuccess, (state, { lot }) => {
        const index = state.discussions.findIndex(d => d.lot?.id === lot.id);
        const discussion = { ...state.discussions[index] };
        discussion.lot = lot;

        const discussions = state.discussions.map(d => {
            return d.id === discussion.id ? discussion : d;
        });

        return {
            ...state,
            discussions,
        };
    }),
);

const lotSuccessHandler = (state: DiscussionsState, lot: Lot) => {
    const discussionToUpdate = { ...state.discussions.find(d => d.lot?.id === lot.id) };
    discussionToUpdate.lot = lot;
    let discussions = [...state.discussions.filter(d => d.id !== discussionToUpdate.id)];
    discussions = [discussionToUpdate, ...discussions];

    return {
        ...state,
        discussions,
        loading: false,
        error: false,
    };
};

const updateDiscussion = (state: DiscussionsState, discussion: Discussion) => {
    return state.discussions.map(d => {
        if (d.id === discussion.id) {
            return {
                ...d,
                ...discussion,
            };
        }
        return d;
    });
};
