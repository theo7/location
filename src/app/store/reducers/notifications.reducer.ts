import { createReducer, on } from '@ngrx/store';
import {
    loadNotificationHistory,
    loadNotificationHistoryError,
    loadNotificationHistorySuccess,
} from '../actions/notifications.actions';
import * as UserActions from '../actions/user.actions';
import { PushNotificationMap } from './../../shared/models/models';

export interface NotificationState {
    read: boolean | null | undefined;
    notifications: PushNotificationMap | null | undefined;
    lastModifiedDate: number | null | undefined;
    loading: boolean;
}

export const initialNotificationState: NotificationState = {
    read: null,
    notifications: {},
    lastModifiedDate: null,
    loading: false,
};

export const notificationReducer = createReducer(
    initialNotificationState,
    // load
    on(loadNotificationHistory, state => ({
        ...state,
        loading: true,
    })),
    on(loadNotificationHistorySuccess, (state, { pushNotificationList }) => ({
        ...state,
        loading: false,
        notifications: pushNotificationList?.notifications,
        read: pushNotificationList?.read,
        lastModifiedDate: pushNotificationList?.lastModifiedDate,
    })),
    on(loadNotificationHistoryError, state => ({
        ...state,
        loading: false,
    })),
    on(UserActions.logoutSuccess, state => initialNotificationState),
);
