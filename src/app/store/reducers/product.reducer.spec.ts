import * as ProductsActions from '../actions/products.actions';
import { initialDressingState, productReducer, ProductState } from './product.reducer';

describe('dressing.reducer', () => {
    it('should have initial state', () => {
        const action = { type: 'NOOP' };
        const state = productReducer(initialDressingState, action);

        expect(state.dressing.products).toEqual([]);
        expect(state.dressing.page).toBe(0);
        expect(state.loading).toBeFalsy();
        expect(state.error).toBeFalsy();
    });

    describe('getProductViews', () => {
        it('should be loading views', () => {
            const productId = 4;

            const action = ProductsActions.getProductViews({ productId });
            const state = productReducer(initialDressingState, action);

            expect(state.views.loading).toBeTruthy();
        });

        it('should reset value to undefined', () => {
            const productId = 4;

            const action = ProductsActions.getProductViews({ productId });
            const state = productReducer(initialDressingState, action);

            expect(state.views.value).toBeUndefined();
        });
    });

    describe('getProductViewsSuccess', () => {
        const productId = 4;

        let afterLoadingState: ProductState;

        beforeEach(() => {
            const action = ProductsActions.getProductViews({ productId });
            afterLoadingState = productReducer(initialDressingState, action);
        });

        it('should not be loading views', () => {
            const views = 233;

            const action = ProductsActions.getProductViewsSuccess({ views });
            const state = productReducer(afterLoadingState, action);

            expect(state.views.loading).toBeFalsy();
        });

        it('should have 233 views', () => {
            const views = 233;

            const action = ProductsActions.getProductViewsSuccess({ views });
            const state = productReducer(afterLoadingState, action);

            expect(state.views.value).toBe(233);
        });
    });

    describe('getProductViewsError', () => {
        const productId = 4;

        let afterLoadingState: ProductState;

        beforeEach(() => {
            const action = ProductsActions.getProductViews({ productId });
            afterLoadingState = productReducer(initialDressingState, action);
        });

        it('should not be loading views', () => {
            const error = 'Cannot load product views...';

            const action = ProductsActions.getProductViewsError({ error });
            const state = productReducer(afterLoadingState, action);

            expect(state.views.loading).toBeFalsy();
        });

        it('should have undefined views', () => {
            const error = 'Cannot load product views...';

            const action = ProductsActions.getProductViewsError({ error });
            const state = productReducer(afterLoadingState, action);

            expect(state.views.value).toBeUndefined();
        });
    });
});
