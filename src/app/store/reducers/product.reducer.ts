import { createReducer, on } from '@ngrx/store';
import { Product } from '../../shared/models/models';
import * as ProductsActions from '../actions/products.actions';

interface DressingState {
    products: Product[];
    count: number;
    total: number;
    page: number;
    loading: boolean;
}

export interface ProductState {
    product: Product | undefined;
    dressing: DressingState;
    views: {
        loading: boolean;
        value?: number;
    };
    loading: boolean;
    error: boolean;
}

export const initialDressingState: ProductState = {
    product: undefined,
    views: {
        loading: false,
    },
    dressing: {
        products: [],
        count: 0,
        total: 0,
        page: 0,
        loading: false,
    },
    loading: false,
    error: false,
};

export const productReducer = createReducer<ProductState>(
    initialDressingState,
    on(ProductsActions.loadProduct, state => ({
        ...state,
        productsPages: null,
        products: [],
        product: undefined,
        page: 0,
        loading: true,
        error: false,
    })),
    on(ProductsActions.loadProductSuccess, (state, { product }) => ({
        ...state,
        loading: false,
        product,
    })),
    on(ProductsActions.loadProductError, state => ({
        ...state,
        loading: false,
        error: true,
    })),
    on(ProductsActions.getDressing, state => ({
        ...state,
        dressing: {
            ...state.dressing,
            loading: true,
        },
        error: false,
    })),
    on(ProductsActions.getDressingSuccess, (state, { products, count, total }) => ({
        ...state,
        dressing: {
            ...state.dressing,
            products,
            count,
            total,
            page: 0,
            loading: false,
        },
    })),
    on(ProductsActions.getDressingError, state => ({
        ...state,
        dressing: {
            ...state.dressing,
            loading: false,
        },
        error: true,
    })),

    on(ProductsActions.getNextDressing, state => ({
        ...state,
        dressing: {
            ...state.dressing,
            loading: true,
        },
        error: false,
    })),
    on(ProductsActions.getNextDressingSuccess, (state, { products }) => ({
        ...state,
        dressing: {
            ...state.dressing,
            products: [...state.dressing.products, ...products],
            page: state.dressing.page + 1,
            loading: false,
        },
    })),
    on(ProductsActions.getNextDressingError, state => ({
        ...state,
        dressing: {
            ...state.dressing,
            loading: false,
        },
        error: true,
    })),
    on(ProductsActions.getOwnDressing, state => ({
        ...state,
        dressing: {
            ...state.dressing,
            loading: true,
        },
        error: false,
    })),
    on(ProductsActions.getOwnDressingSuccess, (state, { products, count, total }) => ({
        ...state,
        dressing: {
            ...state.dressing,
            page: 0,
            loading: false,
            products,
            count,
            total,
        },
        error: false,
    })),
    on(ProductsActions.getOwnDressingError, state => ({
        ...state,
        dressing: {
            ...state.dressing,
            loading: false,
        },
        error: true,
    })),
    on(ProductsActions.getOwnNextDressing, state => ({
        ...state,
        dressing: {
            ...state.dressing,
            loading: true,
        },
        error: false,
    })),
    on(ProductsActions.getOwnNextDressingSuccess, (state, { products }) => ({
        ...state,
        dressing: {
            ...state.dressing,
            products: [...state.dressing.products, ...products],
            page: state.dressing.page + 1,
            loading: false,
        },
        error: false,
    })),
    on(ProductsActions.getOwnNextDressingError, state => ({
        ...state,
        dressing: {
            ...state.dressing,
            loading: false,
        },
        error: true,
    })),

    on(ProductsActions.getProductViews, state => ({
        ...state,
        views: {
            ...state.views,
            loading: true,
            value: undefined,
        },
    })),
    on(ProductsActions.getProductViewsSuccess, (state, action) => ({
        ...state,
        views: {
            ...state.views,
            loading: false,
            value: action.views,
        },
    })),
    on(ProductsActions.getProductViewsError, state => ({
        ...state,
        views: {
            ...state.views,
            loading: false,
            value: undefined,
        },
    })),
);
