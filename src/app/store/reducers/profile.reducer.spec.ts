import { profileActions } from '../actions/profile.actions';
import { initialProfileState, profileReducer, ProfileState } from './profile.reducer';

describe('profile.reducer', () => {
    describe('load', () => {
        let afterActionState: ProfileState;

        beforeEach(() => {
            const action = profileActions.load({ userId: 1 });
            afterActionState = profileReducer(initialProfileState, action);
        });

        it('should start loading', () => {
            expect(afterActionState.loading).toBeTruthy();
        });

        it('should reset current profile', () => {
            expect(afterActionState.current).toBeUndefined();
        });
    });

    describe('loadSuccess', () => {
        let afterActionState: ProfileState;

        beforeEach(() => {
            const action = profileActions.loadSuccess({
                profile: {
                    id: 1,
                    firebaseId: '1',
                    nickname: 'test',
                    creationDate: 0,
                    lastModifiedDate: 0,
                },
            });
            afterActionState = profileReducer(initialProfileState, action);
        });

        it('should stop loading', () => {
            expect(afterActionState.loading).toBeFalsy();
        });

        it('should set current profile', () => {
            expect(afterActionState.current?.id).toBe(1);
        });
    });
});
