import { createReducer, on } from '@ngrx/store';
import { Category, Indexable } from '../../shared/models/models';
import * as CategoriesActions from '../actions/categories.actions';

export interface CategoriesState {
    categories: Indexable<Category>[];
    loading: boolean;
    error: boolean;
}

const initialCategoriesState: CategoriesState = {
    categories: [],
    loading: false,
    error: false,
};

export const categoriesReducer = createReducer(
    initialCategoriesState,

    on(CategoriesActions.getCategories, state => ({ ...state, loading: true, error: false })),
    on(CategoriesActions.getCategoriesSuccess, (state, { categories }) => ({
        categories,
        loading: false,
        error: false,
    })),
    on(CategoriesActions.getCategoriesError, state => ({ ...state, loading: false, error: true })),
);
