import * as UserActions from '../actions/user.actions';
import { UserProfileState, userReducer } from './user.reducer';

describe('user.reducer', () => {
    describe('blockUserSuccess', () => {
        it('should block user if blockedUserIds is undefined', () => {
            const userId = 14;

            const stateToUse = {
                userProfile: {
                    blockedUserIds: undefined,
                },
                loading: false,
                error: false,
                errorReason: null,
            } as UserProfileState;

            const action = UserActions.blockUserSuccess({ userId });
            const state = userReducer(stateToUse, action);

            expect(state.userProfile?.blockedUserIds).toEqual([14]);
        });

        it('should block user if empty array of blockedUserIds', () => {
            const userId = 14;

            const stateToUse = {
                userProfile: {
                    blockedUserIds: [] as number[],
                },
                loading: false,
                error: false,
                errorReason: null,
            } as UserProfileState;

            const action = UserActions.blockUserSuccess({ userId });
            const state = userReducer(stateToUse, action);

            expect(state.userProfile?.blockedUserIds).toEqual([14]);
        });

        it('should block user if existing array of blockedUserIds', () => {
            const userId = 14;

            const stateToUse = {
                userProfile: {
                    blockedUserIds: [9, 12],
                },
                loading: false,
                error: false,
                errorReason: null,
            } as UserProfileState;

            const action = UserActions.blockUserSuccess({ userId });
            const state = userReducer(stateToUse, action);

            expect(state.userProfile?.blockedUserIds).toEqual([9, 12, 14]);
        });
    });

    describe('unblockUserSuccess', () => {
        it('should do nothing if blockedUserIds is undefined', () => {
            const userId = 14;

            const stateToUse = {
                userProfile: {
                    blockedUserIds: undefined,
                },
                loading: false,
                error: false,
                errorReason: null,
            } as UserProfileState;

            const action = UserActions.unblockUserSuccess({ userId });
            const state = userReducer(stateToUse, action);

            expect(state.userProfile?.blockedUserIds).toEqual([]);
        });

        it('should do nothing if empty array of blockedUserIds', () => {
            const userId = 14;

            const stateToUse = {
                userProfile: {
                    blockedUserIds: [] as number[],
                },
                loading: false,
                error: false,
                errorReason: null,
            } as UserProfileState;

            const action = UserActions.unblockUserSuccess({ userId });
            const state = userReducer(stateToUse, action);

            expect(state.userProfile?.blockedUserIds).toEqual([]);
        });

        it('should do nothing block user if not present in array of blockedUserIds', () => {
            const userId = 14;

            const stateToUse = {
                userProfile: {
                    blockedUserIds: [9, 12],
                },
                loading: false,
                error: false,
                errorReason: null,
            } as UserProfileState;

            const action = UserActions.unblockUserSuccess({ userId });
            const state = userReducer(stateToUse, action);

            expect(state.userProfile?.blockedUserIds).toEqual([9, 12]);
        });

        it('should unblock user if present in array of blockedUserIds', () => {
            const userId = 12;

            const stateToUse = {
                userProfile: {
                    blockedUserIds: [9, 12],
                },
                loading: false,
                error: false,
                errorReason: null,
            } as UserProfileState;

            const action = UserActions.unblockUserSuccess({ userId });
            const state = userReducer(stateToUse, action);

            expect(state.userProfile?.blockedUserIds).toEqual([9]);
        });
    });
});
