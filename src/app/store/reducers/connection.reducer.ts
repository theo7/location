import { createReducer, on } from '@ngrx/store';
import * as ConnectionActions from '../actions/connection.actions';

export type ConnectionState = 'online' | 'offline';

const initialConnectionState: ConnectionState = navigator.onLine ? 'online' : 'offline';

export const connectionReducer = createReducer(
    initialConnectionState,
    on(ConnectionActions.setConnection, (state, connection) => connection.connectionState),
);
