import { Page, UserRating, UserRatingSummary } from '../../shared/models/models';
import * as RatingsActions from '../actions/ratings.actions';
import { initialState, ratingsReducer } from './ratings.reducer';

const ratingsPagesForPage0 = {
    content: [
        {
            id: 1,
            value: 4,
            comment: 'so far so good',
        } as UserRating,
        {
            id: 2,
            value: 5,
        } as UserRating,
    ],
    totalElements: 36,
} as Page<UserRating>;

const ratingsPagesForPage1 = {
    content: [
        {
            id: 3,
            value: 4,
            comment: 'so far so good',
        } as UserRating,
        {
            id: 4,
            value: 2,
        } as UserRating,
    ],
    totalElements: 36,
} as Page<UserRating>;

const summary = {
    totalRating: 36,
    averageRating: 3.6,
    detailedRating: new Map([
        [1, 0],
        [2, 0],
        [3, 1],
        [4, 1],
        [5, 2],
    ]),
} as UserRatingSummary;

describe('ratings.reducer', () => {
    describe('load', () => {
        it('should start loading', () => {
            const action = RatingsActions.load();
            const state = ratingsReducer(initialState, action);

            expect(state.loading).toBeTruthy();
        });

        it('should reset error', () => {
            const action = RatingsActions.load();
            const state = ratingsReducer(initialState, action);

            expect(state.error).toBeFalsy();
        });

        it('should reset page', () => {
            const action = RatingsActions.load();
            const state = ratingsReducer(initialState, action);

            expect(state.page).toBe(0);
        });

        it('should reset summary', () => {
            const action = RatingsActions.load();
            const state = ratingsReducer(initialState, action);

            expect(state.summary).toBeUndefined();
        });

        it('should reset ratings', () => {
            const action = RatingsActions.load();
            const state = ratingsReducer(initialState, action);

            expect(state.ratings).toEqual([]);
        });
    });

    describe('loadSuccess', () => {
        it('should be on page 0', () => {
            const action = RatingsActions.loadSuccess({ summary, ratingsPages: ratingsPagesForPage0 });
            const state = ratingsReducer(initialState, action);

            expect(state.page).toBe(0);
        });

        it('should have 2 products', () => {
            const action = RatingsActions.loadSuccess({ summary, ratingsPages: ratingsPagesForPage0 });
            const state = ratingsReducer(initialState, action);

            expect(state.ratings.length).toBe(2);
        });

        it('should have 36 total elements', () => {
            const action = RatingsActions.loadSuccess({ summary, ratingsPages: ratingsPagesForPage0 });
            const state = ratingsReducer(initialState, action);

            expect(state.total).toBe(36);
        });
    });

    describe('loadError', () => {
        it('should have error in state', () => {
            const action = RatingsActions.loadError({ error: 'Cannot load...' });
            const state = ratingsReducer(initialState, action);

            expect(state.error).toBeDefined();
        });
    });

    describe('loadMore', () => {
        it('should start loading', () => {
            const action = RatingsActions.loadMore();
            const state = ratingsReducer(initialState, action);

            expect(state.loading).toBeTruthy();
        });

        it('should reset error', () => {
            const action = RatingsActions.loadMore();
            const state = ratingsReducer(initialState, action);

            expect(state.error).toBeFalsy();
        });
    });

    describe('loadMoreSuccess', () => {
        it('should be on page 1', () => {
            const action1 = RatingsActions.loadSuccess({ summary, ratingsPages: ratingsPagesForPage0 });
            const state1 = ratingsReducer(initialState, action1);

            const action = RatingsActions.loadMoreSuccess({ ratingsPages: ratingsPagesForPage1 });
            const state = ratingsReducer(state1, action);

            expect(state.page).toBe(1);
        });

        it('should have 4 products', () => {
            const action1 = RatingsActions.loadSuccess({ summary, ratingsPages: ratingsPagesForPage0 });
            const state1 = ratingsReducer(initialState, action1);

            const action = RatingsActions.loadMoreSuccess({ ratingsPages: ratingsPagesForPage1 });
            const state = ratingsReducer(state1, action);

            expect(state.ratings.length).toBe(4);
        });

        it('should have 36 total elements', () => {
            const action1 = RatingsActions.loadSuccess({ summary, ratingsPages: ratingsPagesForPage0 });
            const state1 = ratingsReducer(initialState, action1);

            const action = RatingsActions.loadMoreSuccess({ ratingsPages: ratingsPagesForPage1 });
            const state = ratingsReducer(state1, action);

            expect(state.total).toBe(36);
        });
    });

    describe('loadMoreError', () => {
        it('should have error in state', () => {
            const action = RatingsActions.loadMoreError({ error: 'Cannot load...' });
            const state = ratingsReducer(initialState, action);

            expect(state.error).toBeDefined();
        });
    });

    describe('update', () => {
        it('should update an existing rating', () => {
            const action1 = RatingsActions.loadSuccess({ summary, ratingsPages: ratingsPagesForPage0 });
            const state1 = ratingsReducer(initialState, action1);

            const updatedRating = {
                id: 1,
                value: 5,
                comment: "it's really great now",
            } as UserRating;

            const action = RatingsActions.update({ userRating: updatedRating });
            const state = ratingsReducer(state1, action);

            expect(state.ratings[0].value).toBe(5);
            expect(state.ratings[0].comment).toBe("it's really great now");
        });

        it('should not update ratings if none existing', () => {
            const action1 = RatingsActions.loadSuccess({ summary, ratingsPages: ratingsPagesForPage0 });
            const state1 = ratingsReducer(initialState, action1);

            const updatedRating = {
                id: 14,
                value: 5,
                comment: "it's really great now",
            } as UserRating;

            const action = RatingsActions.update({ userRating: updatedRating });
            const state = ratingsReducer(state1, action);

            expect(state.ratings).toEqual(ratingsPagesForPage0.content);
        });
    });
});
