import { Page, Product } from 'src/app/shared/models/models';
import * as SuggestionActions from '../actions/suggestion.actions';
import { initialState, suggestionReducer } from './suggestion.reducer';

const productsPagesForPage0 = {
    content: [
        {
            id: 1,
            label: 'Product status 1',
        },
        {
            id: 2,
            label: 'Product status 2',
        },
        {
            id: 3,
            label: 'Product status 3',
        },
    ],
    totalElements: 20,
} as Page<Product>;

const productsPagesForPage1 = {
    content: [
        {
            id: 4,
            label: 'Product status 4',
        },
        {
            id: 5,
            label: 'Product status 5',
        },
        {
            id: 6,
            label: 'Product status 6',
        },
    ],
    totalElements: 20,
} as Page<Product>;

describe('suggestion.reducer', () => {
    describe('loadSuggestion', () => {
        it('should start loading', () => {
            const action = SuggestionActions.loadSuggestion();
            const state = suggestionReducer(initialState, action);

            expect(state.loading).toBeTruthy();
        });

        it('should reset error', () => {
            const action = SuggestionActions.loadSuggestion();
            const state = suggestionReducer(initialState, action);

            expect(state.error).toBeFalsy();
        });
    });

    describe('loadSuggestionSuccess', () => {
        it('should be on page 0', () => {
            const action = SuggestionActions.loadSuggestionSuccess({ page: productsPagesForPage0 });
            const state = suggestionReducer(initialState, action);

            expect(state.page).toBe(0);
        });

        it('should have 3 products', () => {
            const action = SuggestionActions.loadSuggestionSuccess({ page: productsPagesForPage0 });
            const state = suggestionReducer(initialState, action);

            expect(state.products.length).toBe(3);
        });

        it('should have 20 total elements', () => {
            const action = SuggestionActions.loadSuggestionSuccess({ page: productsPagesForPage0 });
            const state = suggestionReducer(initialState, action);

            expect(state.total).toBe(20);
        });
    });

    describe('loadSuggestionError', () => {
        it('should have error in state', () => {
            const action = SuggestionActions.loadSuggestionError({ error: 'Cannot load...' });
            const state = suggestionReducer(initialState, action);

            expect(state.error).toBeDefined();
        });
    });

    describe('loadMoreSuggestion', () => {
        it('should start loading', () => {
            const action = SuggestionActions.loadMoreSuggestion();
            const state = suggestionReducer(initialState, action);

            expect(state.loading).toBeTruthy();
        });

        it('should reset error', () => {
            const action = SuggestionActions.loadMoreSuggestion();
            const state = suggestionReducer(initialState, action);

            expect(state.error).toBeFalsy();
        });
    });

    describe('loadMoreSuggestionSuccess', () => {
        it('should be on page 2', () => {
            const action1 = SuggestionActions.loadMoreSuggestionSuccess({ page: productsPagesForPage0 });
            const state1 = suggestionReducer(initialState, action1);

            const action = SuggestionActions.loadMoreSuggestionSuccess({ page: productsPagesForPage1 });
            const state = suggestionReducer(state1, action);

            expect(state.page).toBe(2);
        });

        it('should have 6 products', () => {
            const action1 = SuggestionActions.loadMoreSuggestionSuccess({ page: productsPagesForPage0 });
            const state1 = suggestionReducer(initialState, action1);

            const action = SuggestionActions.loadMoreSuggestionSuccess({ page: productsPagesForPage1 });
            const state = suggestionReducer(state1, action);

            expect(state.products.length).toBe(6);
        });

        it('should have 20 total elements', () => {
            const action1 = SuggestionActions.loadMoreSuggestionSuccess({ page: productsPagesForPage0 });
            const state1 = suggestionReducer(initialState, action1);

            const action = SuggestionActions.loadMoreSuggestionSuccess({ page: productsPagesForPage1 });
            const state = suggestionReducer(state1, action);

            expect(state.total).toBe(20);
        });
    });

    describe('loadMoreSuggestionError', () => {
        it('should have error in state', () => {
            const action = SuggestionActions.loadMoreSuggestionError({ error: 'Cannot load...' });
            const state = suggestionReducer(initialState, action);

            expect(state.error).toBeDefined();
        });
    });
});
