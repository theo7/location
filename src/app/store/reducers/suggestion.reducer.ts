import { createReducer, on } from '@ngrx/store';
import { Product } from '../../shared/models/models';
import * as SuggestionActions from '../actions/suggestion.actions';

export interface SuggestionState {
    products: Product[];
    total: number;
    page: number;
    loading: boolean;
    error: boolean;
}

export const initialState: SuggestionState = {
    products: [],
    total: 0,
    page: 0,
    loading: false,
    error: false,
};

export const suggestionReducer = createReducer<SuggestionState>(
    initialState,

    on(SuggestionActions.loadSuggestion, state => ({ ...state, loading: true, error: false })),
    on(SuggestionActions.loadSuggestionSuccess, (state, { page }) => ({
        ...state,
        products: page.content,
        total: page.totalElements,
        page: 0,
        loading: false,
        error: false,
    })),
    on(SuggestionActions.loadSuggestionError, state => ({ ...state, loading: false, error: true })),

    on(SuggestionActions.loadMoreSuggestion, state => ({ ...state, loading: true, error: false })),
    on(SuggestionActions.loadMoreSuggestionSuccess, (state, { page }) => ({
        ...state,
        products: [...state.products, ...page.content],
        page: state.page + 1,
        total: page.totalElements,
        loading: false,
        error: false,
    })),
    on(SuggestionActions.loadMoreSuggestionError, state => ({ ...state, loading: false, error: true })),
);
