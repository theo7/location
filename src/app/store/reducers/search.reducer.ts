import { createReducer, on } from '@ngrx/store';
import { uniqBy } from 'lodash-es';
import { Page, Product } from '../../shared/models/models';
import { filtersActions } from '../actions/filters.actions';
import * as SearchActions from '../actions/search.actions';

export interface SearchState {
    loading: boolean;
    results: Page<Product>;
    page: number;
    limit: number;
    preSearchTotalElements: number;
    showFilter: boolean;
    activateRefresher: boolean;
}

export const initialSearchState: SearchState = {
    loading: false,
    results: { content: [], totalElements: 0, numberOfElements: 0 },
    page: 0,
    limit: 24,
    preSearchTotalElements: 10000,
    showFilter: true,
    activateRefresher: true,
};

export const searchReducer = createReducer(
    initialSearchState,
    on(SearchActions.startSearchFromUrl, (state, _) => ({
        ...state,
        loading: initialSearchState.loading,
        results: initialSearchState.results,
        page: initialSearchState.page,
        limit: initialSearchState.limit,
        preSearchTotalElements: state.preSearchTotalElements,
    })),
    on(SearchActions.executeSearch, state => ({
        ...state,
        loading: true,
        page: initialSearchState.page,
    })),
    on(SearchActions.executeSearchSuccess, (state, { products }) => ({
        ...state,
        loading: false,
        results: products,
    })),
    on(SearchActions.executeSearchError, state => ({
        ...state,
        loading: false,
    })),
    on(SearchActions.executeSearchNext, state => ({
        ...state,
        loading: true,
        page: state.page + 1,
    })),
    on(SearchActions.executeSearchNextSuccess, (state, { products }) => ({
        ...state,
        loading: false,
        results: concatContentWithoutDuplicates(state.results, products),
    })),
    on(SearchActions.executeSearchError, state => ({
        ...state,
        loading: false,
    })),
    on(SearchActions.executeSearchCountSuccess, (state, { total }) => ({
        ...state,
        preSearchTotalElements: total,
    })),
    on(SearchActions.setShowFilter, (state, { showFilter }) => ({
        ...state,
        showFilter,
    })),
    on(SearchActions.setActivateRefresher, (state, { activateRefresher }) => ({
        ...state,
        activateRefresher,
    })),
    // close filter on specific actions
    on(filtersActions.search, state => ({
        ...state,
        showFilter: false,
    })),
    on(filtersActions.applySearchFilter, state => ({
        ...state,
        showFilter: false,
    })),
);

function concatContentWithoutDuplicates(results: Page<Product>, products: Page<Product>) {
    const newResults = { ...results };
    const concatResult = newResults.content.concat(products.content);
    newResults.content = uniqBy(concatResult, 'id');
    return newResults;
}
