import { ActionReducer, ActionReducerMap, MetaReducer } from '@ngrx/store';
import { localStorageSync } from 'ngrx-store-localstorage';
import { environment } from '../../../environments/environment';
import { EntityState } from '../models';
import { bankDetailsReducer } from './bank-details.reducer';
import { categoriesReducer } from './categories.reducer';
import { connectionReducer } from './connection.reducer';
import { creditReducer } from './credit.reducer';
import { discussionsReducer } from './discussions.reducer';
import { filtersReducer } from './filters.reducer';
import { followReducer } from './follow.reducer';
import { notificationReducer } from './notifications.reducer';
import { ordersReducer } from './orders.reducer';
import { productReducer } from './product.reducer';
import { profileReducer } from './profile.reducer';
import { ratingsReducer } from './ratings.reducer';
import { repositoriesReducer } from './repositories.reducer';
import { searchReducer } from './search.reducer';
import { settingsReducer } from './settings.reducer';
import { suggestionReducer } from './suggestion.reducer';
import { userReducer } from './user.reducer';
import { wishlistReducer } from './wishlist.reducer';

export const reducers: ActionReducerMap<EntityState> = {
    userProfile: userReducer,
    discussions: discussionsReducer,
    categories: categoriesReducer,
    search: searchReducer,
    repositories: repositoriesReducer,
    connection: connectionReducer,
    product: productReducer,
    bankDetails: bankDetailsReducer,
    wishlist: wishlistReducer,
    orders: ordersReducer,
    settings: settingsReducer,
    filters: filtersReducer,
    cashState: creditReducer,
    profile: profileReducer,
    notifications: notificationReducer,
    follow: followReducer,
    suggestion: suggestionReducer,
    ratings: ratingsReducer,
    // here is where i put other reducers, when i have them
};

function localStorageSyncReducer(reducer: ActionReducer<any>): ActionReducer<any> {
    return localStorageSync({
        keys: ['userProfile', 'discussions'],
        rehydrate: true, // Pull initial state from local storage on startup
    })(reducer);
}

function logger(reducer: ActionReducer<any>): ActionReducer<any> {
    return (state, action) => {
        console.log('action', action);
        return reducer(state, action);
    };
}

export const metaReducers: MetaReducer<EntityState>[] = !environment.production
    ? [localStorageSyncReducer, logger]
    : [localStorageSyncReducer];
