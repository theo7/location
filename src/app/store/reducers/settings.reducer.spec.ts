import * as SettingsActions from '../actions/settings.actions';
import { initialSettingsState, settingsReducer, SettingsState } from './settings.reducer';

describe('settings.reducer', () => {
    it('should have initial state', () => {
        const action = { type: 'NOOP' };
        const state = settingsReducer(initialSettingsState, action);

        expect(state.loading).toBeFalsy();
        expect(state.settings).toBeUndefined();
    });

    describe('getSettings', () => {
        it('should be loading', () => {
            const action = SettingsActions.getSettings();
            const state = settingsReducer(initialSettingsState, action);

            expect(state.loading).toBeTruthy();
        });
    });

    describe('loadSalesSuccess', () => {
        let afterLoadingState: SettingsState;

        beforeEach(() => {
            const action = SettingsActions.getSettings();
            afterLoadingState = settingsReducer(initialSettingsState, action);
        });

        it('should not be loading', () => {
            const settings: SettingsActions.UserSettings = {
                acceptCommercialContact: true,
                acceptMailNotification: false,
                acceptPushNotification: true,
            };
            const action = SettingsActions.getSettingsSuccess({ settings });
            const state = settingsReducer(afterLoadingState, action);

            expect(state.loading).toBeFalsy();
        });

        it('should have settings', () => {
            const settings: SettingsActions.UserSettings = {
                acceptCommercialContact: true,
                acceptMailNotification: false,
                acceptPushNotification: true,
            };
            const action = SettingsActions.getSettingsSuccess({ settings });
            const state = settingsReducer(afterLoadingState, action);

            expect(state.settings).toBe(settings);
        });
    });

    describe('getSettingsFailed', () => {
        let afterLoadingState: SettingsState;

        beforeEach(() => {
            const action = SettingsActions.getSettings();
            afterLoadingState = settingsReducer(initialSettingsState, action);
        });

        it('should not be loading', () => {
            const action = SettingsActions.getSettingsFailed({ error: 'Error' });
            const state = settingsReducer(afterLoadingState, action);

            expect(state.loading).toBeFalsy();
        });
    });

    describe('setGlobalSettingsSuccess', () => {
        it('should set global settings', () => {
            const settings: SettingsActions.GlobalSettings = {
                acceptCommercialContact: true,
                acceptMailNotification: false,
            };
            const action = SettingsActions.setGlobalSettingsSuccess({ settings });
            const state = settingsReducer(initialSettingsState, action);

            expect(state.settings).toBeDefined();
            expect(state.settings?.acceptCommercialContact).toBe(true);
            expect(state.settings?.acceptMailNotification).toBe(false);
            expect(state.settings?.acceptPushNotification).toBeUndefined();
        });
    });

    describe('setNotificationSettingSuccess', () => {
        it('should set push settings', () => {
            const action = SettingsActions.setNotificationSettingSuccess({ value: true });
            const state = settingsReducer(initialSettingsState, action);

            expect(state.settings).toBeDefined();
            expect(state.settings?.acceptCommercialContact).toBeUndefined();
            expect(state.settings?.acceptMailNotification).toBeUndefined();
            expect(state.settings?.acceptPushNotification).toBe(true);
        });
    });
});
