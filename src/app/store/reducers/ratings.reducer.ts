import { createReducer, on } from '@ngrx/store';
import { UserRating, UserRatingSummary } from '../../shared/models/models';
import * as RatingsActions from '../actions/ratings.actions';

export interface RatingsState {
    summary: UserRatingSummary | undefined;
    ratings: UserRating[];
    total: number;
    page: number;
    loading: boolean;
    error: boolean;
}

export const initialState: RatingsState = {
    summary: undefined,
    ratings: [],
    total: 0,
    page: 0,
    loading: false,
    error: false,
};

export const ratingsReducer = createReducer<RatingsState>(
    initialState,

    on(RatingsActions.load, state => ({
        ...state,
        summary: undefined,
        page: 0,
        ratings: [],
        loading: true,
        error: false,
    })),
    on(RatingsActions.loadSuccess, (state, { ratingsPages, summary }) => ({
        ...state,
        summary,
        ratings: ratingsPages.content,
        total: ratingsPages.totalElements,
        page: 0,
        loading: false,
        error: false,
    })),
    on(RatingsActions.loadError, state => ({ ...state, loading: false, error: true })),

    on(RatingsActions.loadMore, state => ({ ...state, loading: true, error: false })),
    on(RatingsActions.loadMoreSuccess, (state, { ratingsPages }) => ({
        ...state,
        ratings: [...state.ratings, ...ratingsPages.content],
        page: state.page + 1,
        loading: false,
        error: false,
    })),
    on(RatingsActions.loadMoreError, state => ({ ...state, loading: false, error: true })),

    on(RatingsActions.update, (state, { userRating }) => ({
        ...state,
        ratings: state.ratings.map(r => {
            if (r.id === userRating.id) {
                return userRating;
            }

            return r;
        }),
    })),
    on(RatingsActions.answerRatingSuccess, (state, { rateId, rating }) => ({
        ...state,
        ratings: state.ratings.map(r => {
            if (r.id === rateId) {
                return rating;
            }
            return r;
        }),
    })),
);
