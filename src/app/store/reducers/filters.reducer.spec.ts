import { Category, FilterType, ProductFilterSort, Search, Seasonality } from '../../shared/models/models';
import { filtersActions } from '../actions/filters.actions';
import { filtersReducer, FiltersState, initialFiltersState, updateSearchList } from './filters.reducer';

describe('filters.reducer', () => {
    it('should have initial state', () => {
        const action = { type: 'NOOP' };
        const state = filtersReducer(initialFiltersState, action);

        expect(state).toBeDefined();
        expect(state.dressing).toBeDefined();
        expect(state.search).toBeDefined();
    });

    describe('setKeyword action', () => {
        const currentFilterType: FilterType = 'search';
        const keyword = 'test';
        const action = filtersActions.setKeyword({ key: currentFilterType, keyword });
        const state = filtersReducer(initialFiltersState, action);

        it('should set keyword for current filter type', () => {
            expect(state[currentFilterType].searchText).toBeDefined();
            expect(state[currentFilterType].searchText).toEqual(keyword);
        });
        it('should not modify other filters', () => {
            for (const filter of Object.keys(state[currentFilterType])) {
                if (filter !== 'searchText') {
                    expect(state[currentFilterType][filter]).toEqual(initialFiltersState[currentFilterType][filter]);
                }
            }
        });
        it('should not modify other filter type keyword', () => {
            for (const filterType of Object.keys(state)) {
                if (filterType !== currentFilterType) {
                    expect(state[filterType].searchText).not.toBe(keyword);
                }
            }
        });
    });

    describe('setSort action', () => {
        const currentFilterType: FilterType = 'search';
        const sort: ProductFilterSort = {
            field: 'price',
            order: 'asc',
        };
        const action = filtersActions.setSort({ key: currentFilterType, sort });
        const state = filtersReducer(initialFiltersState, action);

        it('should set sort for current filter type', () => {
            expect(state[currentFilterType].sort).toBeDefined();
            expect(state[currentFilterType].sort).toEqual(sort);
        });
        it('should not modify other filters', () => {
            for (const filter of Object.keys(state[currentFilterType])) {
                if (filter !== 'sort') {
                    expect(state[currentFilterType][filter]).toEqual(initialFiltersState[currentFilterType][filter]);
                }
            }
        });
        it('should not modify other filter type sort', () => {
            for (const filterType of Object.keys(state)) {
                if (filterType !== currentFilterType) {
                    expect(state[filterType].sort).not.toBe(sort);
                }
            }
        });
    });

    describe('setSeasonality action', () => {
        const currentFilterType: FilterType = 'search';
        const seasonality: Seasonality = Seasonality.WINTER;
        const action = filtersActions.setSeasonality({ key: currentFilterType, seasonality });
        const state = filtersReducer(initialFiltersState, action);

        it('should set seasonality for current filter type', () => {
            expect(state[currentFilterType].seasonality).toBeDefined();
            expect(state[currentFilterType].seasonality).toEqual(seasonality);
        });
        it('should not modify other filters', () => {
            for (const filter of Object.keys(state[currentFilterType])) {
                if (filter !== 'seasonality') {
                    expect(state[currentFilterType][filter]).toEqual(initialFiltersState[currentFilterType][filter]);
                }
            }
        });
        it('should not modify other filter type sort', () => {
            for (const filterType of Object.keys(state)) {
                if (filterType !== currentFilterType) {
                    expect(state[filterType].seasonality).not.toBe(seasonality);
                }
            }
        });
    });

    describe('setCategory action', () => {
        const currentFilterType: FilterType = 'search';
        const category: Category = {
            id: 1,
            labelKey: 'fake_category',
            subCategories: [],
            orderNumber: 1,
        };
        const action = filtersActions.setCategory({ key: currentFilterType, category });
        const state = filtersReducer(initialFiltersState, action);

        it('should set category for current filter type', () => {
            const currentFilter = state[currentFilterType];

            expect(currentFilter.category).toBeDefined();
            expect(currentFilter.category).toEqual(category);
            expect(currentFilter.subCategory).toBeNull();
            expect(currentFilter.productsTypes).toBeDefined();
            expect(currentFilter.productsTypes?.length).toEqual(0);
            expect(currentFilter.sizes).toBeDefined();
            expect(currentFilter.sizes?.length).toEqual(0);
        });
        it('should not modify other filter type sort', () => {
            for (const filterType of Object.keys(state)) {
                if (filterType !== currentFilterType) {
                    expect(state[filterType].category).not.toBe(category);
                }
            }
        });
    });

    describe('resetCategory action', () => {
        const currentFilterType: FilterType = 'search';
        const action = filtersActions.resetCategory({ key: currentFilterType });
        const initialState = {
            ...initialFiltersState,
            dressing: {
                ...initialFiltersState.dressing,
                category: {
                    id: 1,
                    labelKey: 'fake_category',
                    orderNumber: 1,
                },
            },
        };
        const state = filtersReducer(initialState, action);

        it('should reset category for current filter type', () => {
            const currentFilter = state[currentFilterType];

            expect(currentFilter.category).toBeNull();
        });
        it('should not modify other filter type sort', () => {
            expect(state.dressing.category).toEqual(initialState.dressing.category);
        });
    });

    describe('load search history', () => {
        describe('load', () => {
            it('should start loading', () => {
                const action = filtersActions.loadSearchHistory();
                const { searchHistory } = filtersReducer(initialFiltersState, action);

                expect(searchHistory.loading).toBeTruthy();
            });
        });

        describe('success', () => {
            it('should stop loading and set list', () => {
                const initialState = {
                    ...initialFiltersState,
                    searchHistory: {
                        ...initialFiltersState.searchHistory,
                        loading: true,
                    },
                };
                const action = filtersActions.loadSearchHistorySuccess({ searchList: [] });
                const { searchHistory } = filtersReducer(initialState, action);

                expect(searchHistory.loading).toBeFalsy();
            });
        });

        describe('error', () => {
            it('should stop loading', () => {
                const initialState = {
                    ...initialFiltersState,
                    searchHistory: {
                        ...initialFiltersState.searchHistory,
                        loading: true,
                    },
                };
                const action = filtersActions.loadSearchHistoryError({ error: 'oops' });
                const { searchHistory } = filtersReducer(initialState, action);

                expect(searchHistory.loading).toBeFalsy();
            });
        });
    });

    describe('add search history', () => {
        describe('add', () => {
            it('should start loading', () => {
                // given
                const initialState = {
                    ...initialFiltersState,
                    searchHistory: {
                        ...initialFiltersState.searchHistory,
                        recent: [createFakeSearch(1)],
                        loading: false,
                    },
                };
                const action = filtersActions.loadSearchHistory();

                // call
                const { searchHistory } = filtersReducer(initialFiltersState, action);

                // expect
                expect(searchHistory.loading).toBeTruthy();
            });
        });

        describe('success', () => {
            it('should stop loading and add new search in recent list', () => {
                // given
                const initialState = {
                    ...initialFiltersState,
                    searchHistory: {
                        ...initialFiltersState.searchHistory,
                        recent: [createFakeSearch(1)],
                        loading: true,
                    },
                };
                const action = filtersActions.addSearchSuccess({ search: createFakeSearch(2) });

                // call
                const { searchHistory } = filtersReducer(initialState, action);

                // expect
                expect(searchHistory.loading).toBeFalsy();
                expect(searchHistory.recent.length).toBe(2);
                expect(searchHistory.recent.find(s => s.id === 2)).toBeTruthy();
            });

            it('should stop loading & replace oldest search in recent list', () => {
                // given
                let initialState = {
                    ...initialFiltersState,
                    searchHistory: {
                        ...initialFiltersState.searchHistory,
                        recent: [],
                        loading: true,
                    },
                } as FiltersState;
                for (let i = 10; i > 0; i--) {
                    initialState.searchHistory.recent.push(createFakeSearch(i));
                }
                const action = filtersActions.addSearchSuccess({ search: createFakeSearch(11) });

                // call
                const { searchHistory } = filtersReducer(initialState, action);

                // expect
                expect(searchHistory.loading).toBeFalsy();
                expect(searchHistory.recent.length).toBe(10);
                expect(searchHistory.recent.find(s => s.id === 1)).toBeFalsy();
                expect(searchHistory.recent.find(s => s.id === 11)).toBeTruthy();
            });
        });

        describe('error', () => {
            it('should stop loading', () => {
                // given
                const initialState = {
                    ...initialFiltersState,
                    searchHistory: {
                        ...initialFiltersState.searchHistory,
                        loading: true,
                    },
                };
                const action = filtersActions.addSearchError({ error: 'oops' });

                // call
                const { searchHistory } = filtersReducer(initialState, action);

                // expect
                expect(searchHistory.loading).toBeFalsy();
            });
        });
    });

    describe('save search action', () => {
        describe('save', () => {
            it('should start loading', () => {
                // given
                let initialState = {
                    ...initialFiltersState,
                    searchHistory: {
                        ...initialFiltersState.searchHistory,
                        recent: [createFakeSearch(1)],
                        saved: [],
                        loading: false,
                    },
                } as FiltersState;
                const action = filtersActions.saveSearch({ id: 1 });

                // call
                const { searchHistory } = filtersReducer(initialState, action);

                // expect
                expect(searchHistory.loading).toBeTruthy();
                expect(searchHistory.recent.length).toBe(1);
                expect(searchHistory.saved.length).toBe(0);
            });
        });

        describe('success', () => {
            it('should stop loading, update object and add it to saved list', () => {
                // given
                let initialState = {
                    ...initialFiltersState,
                    searchHistory: {
                        ...initialFiltersState.searchHistory,
                        recent: [createFakeSearch(1)],
                        saved: [],
                        loading: true,
                    },
                } as FiltersState;
                const action = filtersActions.saveSearchSuccess({ search: createFakeSearch(1) });

                // call
                const { searchHistory } = filtersReducer(initialState, action);

                // expect
                expect(searchHistory.recent.length).toBe(1);
                expect(searchHistory.saved.length).toBe(1);
                expect(searchHistory.saved.find(s => s.id === 1)).toBeTruthy();
            });
        });

        describe('error', () => {
            it('should stop loading', () => {
                // given
                let initialState = {
                    ...initialFiltersState,
                    searchHistory: {
                        ...initialFiltersState.searchHistory,
                        recent: [createFakeSearch(1)],
                        saved: [],
                        loading: true,
                    },
                } as FiltersState;
                const action = filtersActions.saveSearchError({ error: 'oops' });

                // call
                const { searchHistory } = filtersReducer(initialState, action);

                // expect
                expect(searchHistory.loading).toBeFalsy();
                expect(searchHistory.recent.length).toBe(1);
                expect(searchHistory.saved.length).toBe(0);
            });
        });
    });

    describe('unsave search action', () => {
        describe('unsave', () => {
            it('should start loading', () => {
                // given
                const initialState = {
                    ...initialFiltersState,
                    searchHistory: {
                        ...initialFiltersState.searchHistory,
                        recent: [createFakeSearch(2), createFakeSearch(1)],
                        saved: [createFakeSearch(1)],
                        loading: false,
                    },
                };
                const action = filtersActions.unSaveSearch({ id: 1 });

                // call
                const { searchHistory } = filtersReducer(initialState, action);

                // expect
                expect(searchHistory.loading).toBeTruthy();
            });
        });

        describe('success', () => {
            it('should stop loading, update object and remove it from saved list', () => {
                // given
                const initialState = {
                    ...initialFiltersState,
                    searchHistory: {
                        ...initialFiltersState.searchHistory,
                        recent: [createFakeSearch(2), createFakeSearch(1)],
                        saved: [createFakeSearch(1)],
                        loading: true,
                    },
                };
                const action = filtersActions.unSaveSearchSuccess({ search: createFakeSearch(1) });

                // call
                const { searchHistory } = filtersReducer(initialState, action);

                // expect
                expect(searchHistory.loading).toBeFalsy();
                expect(searchHistory.recent.length).toBe(2);
                expect(searchHistory.saved.length).toBe(0);
            });
        });

        describe('error', () => {
            it('should stop loading', () => {
                // given
                const initialState = {
                    ...initialFiltersState,
                    searchHistory: {
                        ...initialFiltersState.searchHistory,
                        recent: [createFakeSearch(2), createFakeSearch(1)],
                        saved: [createFakeSearch(1)],
                        loading: true,
                    },
                };
                const action = filtersActions.unSaveSearchError({ error: 'oops' });

                // call
                const { searchHistory } = filtersReducer(initialState, action);

                // expect
                expect(searchHistory.loading).toBeFalsy();
            });
        });
    });

    describe('reset search history action', () => {
        it('should reset search history state', () => {
            // given
            const initialState = {
                ...initialFiltersState,
                searchHistory: {
                    ...initialFiltersState.searchHistory,
                    recent: [createFakeSearch(2), createFakeSearch(1)],
                    saved: [createFakeSearch(1)],
                    loading: false,
                },
            };
            const action = filtersActions.resetSearchHistory();

            // call
            const { searchHistory } = filtersReducer(initialState, action);

            // expect
            expect(searchHistory.loading).toBeFalsy();
            expect(searchHistory.recent.length).toBe(0);
            expect(searchHistory.saved.length).toBe(0);
        });
    });

    describe('updateSearchList', () => {
        it('should add new search to list if not found', () => {
            // given
            const list = [createFakeSearch(1, 2, undefined), createFakeSearch(2, 1, Seasonality.ALL)];

            // call
            const result = updateSearchList(list, createFakeSearch(3, undefined, Seasonality.WINTER));

            // expect
            expect(result.length).toBe(3);
        });

        it('should edit search in list if found', () => {
            // given
            const list = [createFakeSearch(1, 2, undefined), createFakeSearch(2, 1, Seasonality.ALL)];

            // call
            const result = updateSearchList(list, createFakeSearch(1, 2, undefined));

            // expect
            expect(result.length).toBe(2);
        });
    });
});

function createFakeSearch(id: number, categoryId?: number, seasonality?: Seasonality): Search {
    return {
        id,
        categoryId,
        seasonality,
        userId: 1,
        keyword: 'test',
        saved: false,
    } as Search;
}
