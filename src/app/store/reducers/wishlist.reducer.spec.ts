import { Page, Product } from 'src/app/shared/models/models';
import * as WishlistActions from '../actions/wishlist.actions';
import { initialState, wishlistReducer } from './wishlist.reducer';

const productsPagesForPage0 = {
    content: [
        {
            id: 1,
            label: 'Product status',
        },
        {
            id: 2,
            label: 'Product status',
        },
    ],
    totalElements: 36,
} as Page<Product>;

const productsPagesForPage1 = {
    content: [
        {
            id: 3,
            label: 'Product status',
        },
        {
            id: 4,
            label: 'Product status',
        },
    ],
    totalElements: 36,
} as Page<Product>;

describe('wishlist.reducer', () => {
    describe('load', () => {
        it('should start loading', () => {
            const action = WishlistActions.load();
            const state = wishlistReducer(initialState, action);

            expect(state.loading).toBeTruthy();
        });

        it('should reset error', () => {
            const action = WishlistActions.load();
            const state = wishlistReducer(initialState, action);

            expect(state.error).toBeFalsy();
        });
    });

    describe('loadSuccess', () => {
        it('should be on page 0', () => {
            const action = WishlistActions.loadSuccess({ productsPages: productsPagesForPage0 });
            const state = wishlistReducer(initialState, action);

            expect(state.page).toBe(0);
        });

        it('should have 2 products', () => {
            const action = WishlistActions.loadSuccess({ productsPages: productsPagesForPage0 });
            const state = wishlistReducer(initialState, action);

            expect(state.products.length).toBe(2);
        });

        it('should have 36 total elements', () => {
            const action = WishlistActions.loadSuccess({ productsPages: productsPagesForPage0 });
            const state = wishlistReducer(initialState, action);

            expect(state.total).toBe(36);
        });
    });

    describe('loadError', () => {
        it('should have error in state', () => {
            const action = WishlistActions.loadError({ error: 'Cannot load...' });
            const state = wishlistReducer(initialState, action);

            expect(state.error).toBeDefined();
        });
    });

    describe('loadMore', () => {
        it('should start loading', () => {
            const action = WishlistActions.loadMore();
            const state = wishlistReducer(initialState, action);

            expect(state.loading).toBeTruthy();
        });

        it('should reset error', () => {
            const action = WishlistActions.loadMore();
            const state = wishlistReducer(initialState, action);

            expect(state.error).toBeFalsy();
        });
    });

    describe('loadMoreSuccess', () => {
        it('should be on page 1', () => {
            const action1 = WishlistActions.loadSuccess({ productsPages: productsPagesForPage0 });
            const state1 = wishlistReducer(initialState, action1);

            const action = WishlistActions.loadMoreSuccess({ productsPages: productsPagesForPage1 });
            const state = wishlistReducer(state1, action);

            expect(state.page).toBe(1);
        });

        it('should have 4 products', () => {
            const action1 = WishlistActions.loadSuccess({ productsPages: productsPagesForPage0 });
            const state1 = wishlistReducer(initialState, action1);

            const action = WishlistActions.loadMoreSuccess({ productsPages: productsPagesForPage1 });
            const state = wishlistReducer(state1, action);

            expect(state.products.length).toBe(4);
        });

        it('should have 36 total elements', () => {
            const action1 = WishlistActions.loadSuccess({ productsPages: productsPagesForPage0 });
            const state1 = wishlistReducer(initialState, action1);

            const action = WishlistActions.loadMoreSuccess({ productsPages: productsPagesForPage1 });
            const state = wishlistReducer(state1, action);

            expect(state.total).toBe(36);
        });
    });

    describe('loadMoreError', () => {
        it('should have error in state', () => {
            const action = WishlistActions.loadMoreError({ error: 'Cannot load...' });
            const state = wishlistReducer(initialState, action);

            expect(state.error).toBeDefined();
        });
    });
});
