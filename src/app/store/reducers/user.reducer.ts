import { createReducer, on } from '@ngrx/store';
import { UserProfile, UserProfileStatus } from '../../shared/models/models';
import * as ContactActions from '../actions/contact.actions';
import * as ProductsActions from '../actions/products.actions';
import * as UserActions from '../actions/user.actions';

export interface UserProfileState {
    userProfile: UserProfile | undefined;
    loading: boolean;
    error: boolean;
    errorReason?: any | null;
}

export const initialState: UserProfileState = {
    userProfile: undefined,
    loading: false,
    error: false,
    errorReason: null,
};

export const userReducer = createReducer<UserProfileState>(
    initialState,

    on(UserActions.signUp, state => ({ ...state, loading: true, error: false, errorReason: null })),
    on(UserActions.signUpSuccess, state => ({ ...state, loading: false, error: false })),
    on(UserActions.signUpError, (state, { error }) => ({ ...state, loading: false, error: true, errorReason: error })),

    on(UserActions.openLoginPage, state => ({ ...state, loading: false, error: false })),

    on(UserActions.login, state => ({ ...state, loading: true, error: false, errorReason: null })),
    on(UserActions.loginSuccess, state => ({ ...state, loading: false, error: false })),
    on(UserActions.loginError, (state, { error }) => ({ ...state, loading: false, error: true, errorReason: error })),

    on(UserActions.getUserProfile, state => ({ ...state, loading: true, error: false })),
    on(UserActions.getUserProfileSuccess, (state, { userProfile }) => ({
        ...state,
        userProfile,
        loading: false,
        error: false,
    })),
    on(UserActions.getUserProfileError, state => ({ ...state, loading: false, error: true })),

    on(UserActions.updateUserProfile, state => ({ ...state, loading: true, error: false })),
    on(UserActions.updateUserProfileSuccess, (state, { userProfile }) => ({
        ...state,
        userProfile,
        loading: false,
        error: false,
    })),
    on(UserActions.updateUserProfileError, (state, { error }) => ({
        ...state,
        loading: false,
        error: true,
        errorReason: error,
    })),

    on(UserActions.postUserAddress, state => ({ ...state, loading: true, error: false })),
    on(UserActions.postUserAddressSuccess, (state, { address }) => {
        const addresses = state.userProfile?.addresses?.map(a => ({ ...a })) || [];
        if (address.isDefault) {
            addresses.forEach(a => (a.isDefault = false));
        }
        addresses.push(address);

        const userProfile = {
            ...state.userProfile,
            addresses,
        } as UserProfile;

        return {
            ...state,
            userProfile,
            loading: false,
            error: false,
        };
    }),
    on(UserActions.postUserAddressError, state => ({ ...state, loading: false, error: true })),

    on(UserActions.deleteUserAddress, state => ({ ...state, loading: true, error: false })),
    on(UserActions.deleteUserAddressSuccess, (state, { address }) => {
        const userProfile = {
            ...state.userProfile,
            addresses: state.userProfile?.addresses?.filter(a => a.id !== address.id) || [],
        } as UserProfile;

        return {
            ...state,
            userProfile,
            loading: false,
            error: false,
        };
    }),
    on(UserActions.deleteUserAddressError, state => ({ ...state, loading: false, error: true })),

    on(UserActions.updateUserAddress, state => ({ ...state, loading: true, error: false })),
    on(UserActions.updateUserAddressSuccess, (state, { address }) => {
        const addresses = state.userProfile?.addresses?.map(a => ({ ...a })) || [];
        if (address.isDefault) {
            addresses.forEach(a => (a.isDefault = false));
        }

        const userProfile = {
            ...state.userProfile,
            addresses: addresses.map(a => {
                return a.id === address.id ? address : a;
            }),
        } as UserProfile;

        return {
            ...state,
            userProfile,
            loading: false,
            error: false,
        };
    }),
    on(UserActions.updateUserAddressError, state => ({ ...state, loading: false, error: true })),

    on(UserActions.logout, state => ({ ...state, loading: true, error: false })),
    on(UserActions.logoutSuccess, state => ({
        ...state,
        userProfile: undefined,
        loading: false,
        error: false,
    })),
    on(UserActions.logoutError, state => ({ ...state, loading: false, error: true })),

    on(UserActions.resetPassword, state => ({ ...state, loading: true, error: false })),
    on(UserActions.resetPasswordSuccess, state => ({ ...state, loading: false, error: false })),
    on(UserActions.resetPasswordError, (state, { error }) => ({
        ...state,
        loading: false,
        error: true,
        errorReason: error,
    })),

    on(UserActions.changePassword, state => ({ ...state, loading: true, error: false })),
    on(UserActions.changePasswordSuccess, state => ({ ...state, loading: false, error: false })),
    on(UserActions.changePasswordError, (state, { error }) => ({
        ...state,
        loading: false,
        error: true,
        errorReason: error,
    })),

    on(UserActions.updateCgu, state => ({ ...state, loading: true, error: false })),
    on(UserActions.updateCguSuccess, (state, { userProfile }) => ({
        ...state,
        userProfile,
        loading: false,
        error: false,
    })),
    on(UserActions.updateCguError, (state, { error }) => ({
        ...state,
        loading: false,
        error: true,
        errorReason: error,
    })),

    on(ProductsActions.likeSuccess, (state, { productId }) => ({
        ...state,
        userProfile: {
            ...state.userProfile,
            favoriteProductsIds: (state.userProfile?.favoriteProductsIds || []).concat(productId),
        } as UserProfile,
    })),
    on(ProductsActions.deleteLikeSuccess, (state, { productId }) => ({
        ...state,
        userProfile: {
            ...state.userProfile,
            favoriteProductsIds: (state.userProfile?.favoriteProductsIds || []).filter(id => id !== productId),
        } as UserProfile,
    })),

    on(UserActions.blockUserSuccess, (state, { userId }) => {
        const userProfile = {
            ...state.userProfile,
            blockedUserIds: (state.userProfile?.blockedUserIds || []).concat(userId),
        } as UserProfile;

        return {
            ...state,
            userProfile,
            loading: false,
            error: false,
        };
    }),

    on(UserActions.unblockUserSuccess, (state, { userId }) => {
        const userProfile = {
            ...state.userProfile,
            blockedUserIds: (state.userProfile?.blockedUserIds || []).filter(id => id !== userId),
        } as UserProfile;

        return {
            ...state,
            userProfile,
            loading: false,
            error: false,
        };
    }),

    on(UserActions.switchVacationModeSuccess, state => {
        const userProfile = {
            ...state.userProfile,
            status:
                state.userProfile?.status !== UserProfileStatus.VACATION
                    ? UserProfileStatus.VACATION
                    : UserProfileStatus.ACTIVE,
        } as UserProfile;

        return {
            ...state,
            userProfile,
            loading: false,
            error: false,
        };
    }),
    on(ContactActions.sendEmailContactSupport, state => ({ ...state, loading: true, error: false })),
    on(ContactActions.sendEmailContactSupportSuccess, state => ({ ...state, loading: false, error: false })),
    on(ContactActions.sendEmailContactSupportError, state => ({ ...state, loading: false, error: true })),
);
