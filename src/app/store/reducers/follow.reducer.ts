import { createReducer, on } from '@ngrx/store';
import { UserFollowingProfile } from 'src/app/shared/models/models';
import * as FollowAction from '../actions/follow.actions';

export interface FollowState {
    following: {
        users: UserFollowingProfile[];
        maxPage: number | undefined;
        page: number;
        loading: boolean;
        error: boolean;
    };
    followers: {
        users: UserFollowingProfile[];
        maxPage: number | undefined;
        page: number;
        loading: boolean;
        error: boolean;
    };
}

export const initialState: FollowState = {
    following: {
        users: [],
        maxPage: undefined,
        page: 0,
        loading: false,
        error: false,
    },
    followers: {
        users: [],
        maxPage: undefined,
        page: 0,
        loading: false,
        error: false,
    },
};

export const followReducer = createReducer<FollowState>(
    initialState,

    on(FollowAction.getUserFollowing, state => ({
        ...state,
        following: {
            ...state.following,
            loading: true,
            error: false,
        },
    })),
    on(FollowAction.getUserFollowingSuccess, (state, { followings }) => ({
        ...state,
        following: {
            ...state.following,
            users: [...state.following.users, ...followings.content],
            maxPage: state.following.maxPage ? state.following.maxPage : followings.totalPages,
            page: state.following.page + 1,
            loading: false,
            error: false,
        },
    })),
    on(FollowAction.getUserFollowingError, state => ({
        ...state,
        following: {
            ...state.following,
            loading: false,
            error: true,
        },
    })),
    on(FollowAction.resetFollowing, state => ({
        ...state,
        following: initialState.following,
    })),
    on(FollowAction.getUserFollower, state => ({
        ...state,
        followers: {
            ...state.followers,
            loading: true,
            error: false,
        },
    })),
    on(FollowAction.getUserFollowerSuccess, (state, { followers }) => ({
        ...state,
        followers: {
            ...state.followers,
            users: [...state.followers.users, ...followers.content],
            maxPage: state.followers.maxPage ? state.followers.maxPage : followers.totalPages,
            page: state.followers.page + 1,
            loading: false,
            error: false,
        },
    })),
    on(FollowAction.getUserFollowerError, state => ({
        ...state,
        followers: {
            ...state.followers,
            loading: false,
            error: true,
        },
    })),

    on(FollowAction.resetFollowers, state => ({
        ...state,
        followers: initialState.followers,
    })),
);
