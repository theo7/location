import { createReducer, on } from '@ngrx/store';
import { BankDetailsAccount, Document, DocumentStatus, Iban, IbanStatus } from 'src/app/shared/models/models';
import * as BankDetailsActions from '../actions/bank-details.actions';

export interface BankDetailsState {
    loading: boolean;
    account: BankDetailsAccount | undefined;
    ibanDocument: Iban | undefined;
    ribDocument: Document | undefined;
    identityDocument: Document | undefined;
}

const initialBankDetailsState: BankDetailsState = {
    loading: false,
    account: undefined,
    ibanDocument: undefined,
    ribDocument: undefined,
    identityDocument: undefined,
};

export const bankDetailsReducer = createReducer(
    initialBankDetailsState,

    on(BankDetailsActions.loadBankDetails, state => ({ ...state, loading: true })),
    on(BankDetailsActions.loadBankDetailsSuccess, (_, { response }) => ({
        account: response.account,
        ibanDocument: response.iban,
        ribDocument: response.rib,
        identityDocument: response.identity,
        loading: false,
    })),
    on(BankDetailsActions.documentUploaded, (state, { documentType, status }) => {
        if (documentType === 'iban') {
            return {
                ...state,
                ibanDocument: {
                    ...state.ibanDocument,
                    status: status as IbanStatus,
                },
            };
        }
        if (documentType === 'rib') {
            return {
                ...state,
                ribDocument: {
                    ...state.ribDocument,
                    status: status as DocumentStatus,
                },
            };
        }

        return {
            ...state,
            identityDocument: {
                ...state.identityDocument,
                status: status as DocumentStatus,
            },
        };
    }),
    on(BankDetailsActions.accountUpdated, (state, { status }) => {
        return {
            ...state,
            account: {
                ...state.account,
                status,
            },
        };
    }),
);
