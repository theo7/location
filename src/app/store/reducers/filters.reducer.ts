import { createReducer, on } from '@ngrx/store';
import { FilterType, LoadingState, ProductFilter, Search, Seasonality } from '../../shared/models/models';
import { filtersActions } from '../actions/filters.actions';

export type Filters = {
    [key in FilterType]: ProductFilter;
};

export type SearchHistoryState = {
    recent: Search[];
    saved: Search[];
};

export type FiltersState = Filters & { searchHistory: LoadingState<SearchHistoryState> };

export const initialProductFilterState: ProductFilter = {
    searchText: '',
    category: undefined,
    subCategory: undefined,
    productsTypes: [],
    brands: [],
    colors: [],
    conditions: [],
    sizes: [],
    minPrice: undefined,
    maxPrice: undefined,
    seasonality: Seasonality.ALL,
    sort: {
        field: 'creationDate',
        order: 'desc',
    },
};

export const initialFiltersState: FiltersState = {
    search: initialProductFilterState,
    dressing: initialProductFilterState,
    searchHistory: {
        recent: [],
        saved: [],
        loading: false,
    },
};

export const filtersReducer = createReducer(
    initialFiltersState,
    on(filtersActions.set, (state, { key, filter }) => ({
        ...state,
        [key]: {
            searchText: filter.searchText ?? initialProductFilterState.searchText,
            category: filter.category ?? initialProductFilterState.category,
            subCategory: filter.subCategory ?? initialProductFilterState.subCategory,
            productsTypes: filter.productsTypes ?? initialProductFilterState.productsTypes,
            brands: filter.brands ?? initialProductFilterState.brands,
            colors: filter.colors ?? initialProductFilterState.colors,
            conditions: filter.conditions ?? initialProductFilterState.conditions,
            sizes: filter.sizes ?? initialProductFilterState.sizes,
            minPrice: filter.minPrice ?? initialProductFilterState.minPrice,
            maxPrice: filter.maxPrice ?? initialProductFilterState.maxPrice,
            seasonality: filter.seasonality ?? initialProductFilterState.seasonality,
            sort: filter.sort?.field && filter.sort?.order ? filter.sort : initialProductFilterState.sort,
        },
    })),
    on(filtersActions.reset, (state, { key }) => ({
        ...state,
        [key]: initialProductFilterState,
    })),
    on(filtersActions.setKeyword, (state, { key, keyword }) => ({
        ...state,
        [key]: {
            ...state[key],
            searchText: keyword,
            sort: !keyword
                ? {
                      field: 'creationDate',
                      order: 'desc',
                  }
                : state[key].sort,
        },
    })),
    // SORT
    on(filtersActions.setSort, (state, { key, sort }) => ({
        ...state,
        [key]: {
            ...state[key],
            sort,
        },
    })),
    on(filtersActions.resetSort, (state, { key }) => ({
        ...state,
        [key]: {
            ...state[key],
            sort: initialProductFilterState.sort,
        },
    })),
    // SEASONALITY
    on(filtersActions.setSeasonality, (state, { key, seasonality }) => ({
        ...state,
        [key]: {
            ...state[key],
            seasonality,
        },
    })),
    on(filtersActions.resetSeasonality, (state, { key }) => ({
        ...state,
        [key]: {
            ...state[key],
            seasonality: initialProductFilterState.seasonality,
        },
    })),
    // CATEGORY
    on(filtersActions.setCategory, (state, { key, category }) => {
        return {
            ...state,
            [key]: {
                ...state[key],
                category,
                subCategory: null,
                productsTypes: [],
                sizes: [],
            },
        };
    }),
    on(filtersActions.resetCategory, (state, { key }) => ({
        ...state,
        [key]: {
            ...state[key],
            category: null,
            subCategory: null,
            productsTypes: [],
            sizes: [],
        },
    })),
    // SUB CATEGORY
    on(filtersActions.setSubCategory, (state, { key, subCategory, category }) => {
        return {
            ...state,
            [key]: {
                ...state[key],
                category,
                subCategory,
                productsTypes: [],
                sizes: [],
            },
        };
    }),
    on(filtersActions.resetSubCategory, (state, { key }) => ({
        ...state,
        [key]: {
            ...state[key],
            subCategory: null,
            productsTypes: [],
            sizes: [],
        },
    })),
    // PRODUCT TYPE
    on(filtersActions.addProductType, (state, { key, productType, subCategory, category }) => {
        let productsTypes = state[key].productsTypes || [];
        if (state[key].subCategory?.id !== subCategory.id) {
            productsTypes = [];
        }

        return {
            ...state,
            [key]: {
                ...state[key],
                subCategory,
                category,
                productsTypes: productsTypes.concat(productType),
            },
        };
    }),
    on(filtersActions.removeProductType, (state, { key, productType }) => ({
        ...state,
        [key]: {
            ...state[key],
            productsTypes: state[key].productsTypes?.filter(p => p.id !== productType.id) || [],
            sizes: [],
        },
    })),
    // SIZE
    on(filtersActions.addSize, (state, { key, size }) => ({
        ...state,
        [key]: {
            ...state[key],
            sizes: state[key].sizes?.concat(size) || [],
        },
    })),
    on(filtersActions.removeSize, (state, { key, size }) => ({
        ...state,
        [key]: {
            ...state[key],
            sizes: state[key].sizes?.filter(s => s.id !== size.id) || [],
        },
    })),
    // BRAND
    on(filtersActions.addBrand, (state, { key, brand }) => ({
        ...state,
        [key]: {
            ...state[key],
            brands: [brand].concat(state[key].brands!),
        },
    })),
    on(filtersActions.removeBrand, (state, { key, brand }) => ({
        ...state,
        [key]: {
            ...state[key],
            brands: state[key].brands!.filter(b => b.id !== brand.id) || [],
        },
    })),
    // COLOR
    on(filtersActions.addColor, (state, { key, color }) => ({
        ...state,
        [key]: {
            ...state[key],
            colors: state[key].colors?.concat(color) || [],
        },
    })),
    on(filtersActions.removeColor, (state, { key, color }) => ({
        ...state,
        [key]: {
            ...state[key],
            colors: state[key].colors?.filter(c => c.id !== color.id) || [],
        },
    })),
    // CONDITION
    on(filtersActions.addCondition, (state, { key, condition }) => ({
        ...state,
        [key]: {
            ...state[key],
            conditions: state[key].conditions?.concat(condition) || [],
        },
    })),
    on(filtersActions.removeCondition, (state, { key, condition }) => ({
        ...state,
        [key]: {
            ...state[key],
            conditions: state[key].conditions?.filter(c => c.id !== condition.id) || [],
        },
    })),
    // PRICE
    on(filtersActions.setMinPrice, (state, { key, price }) => ({
        ...state,
        [key]: {
            ...state[key],
            minPrice: price,
        },
    })),
    on(filtersActions.setMaxPrice, (state, { key, price }) => ({
        ...state,
        [key]: {
            ...state[key],
            maxPrice: price,
        },
    })),
    on(filtersActions.resetMinPrice, (state, { key }) => ({
        ...state,
        [key]: {
            ...state[key],
            minPrice: initialProductFilterState.minPrice,
        },
    })),
    on(filtersActions.resetMaxPrice, (state, { key }) => ({
        ...state,
        [key]: {
            ...state[key],
            maxPrice: initialProductFilterState.maxPrice,
        },
    })),
    // search-history
    on(filtersActions.loadSearchHistory, state => ({
        ...state,
        searchHistory: {
            ...state.searchHistory,
            loading: true,
        },
    })),
    on(filtersActions.loadSearchHistorySuccess, (state, { searchList }) => {
        return {
            ...state,
            searchHistory: {
                recent: searchList.slice(0, 10),
                saved: searchList.filter(s => s.saved),
                loading: false,
            },
        };
    }),
    on(filtersActions.loadSearchHistoryError, state => ({
        ...state,
        searchHistory: {
            ...state.searchHistory,
            loading: false,
        },
    })),
    on(filtersActions.addSearch, state => ({
        ...state,
        searchHistory: {
            ...state.searchHistory,
            loading: true,
        },
    })),
    on(filtersActions.addSearchSuccess, (state, { search }) => {
        const recentSearch = [...state.searchHistory.recent];
        if (recentSearch.length === 10) {
            recentSearch.pop();
        }

        return {
            ...state,
            searchHistory: {
                ...state.searchHistory,
                recent: [search].concat(recentSearch),
                loading: false,
            },
        };
    }),
    on(filtersActions.addSearchError, state => ({
        ...state,
        searchHistory: {
            ...state.searchHistory,
            loading: false,
        },
    })),
    on(filtersActions.saveSearch, state => ({
        ...state,
        searchHistory: {
            ...state.searchHistory,
            loading: true,
        },
    })),
    on(filtersActions.saveSearchSuccess, (state, { search }) => {
        return {
            ...state,
            searchHistory: {
                ...state.searchHistory,
                recent: updateSearchList(state.searchHistory.recent, search),
                saved: [search].concat(state.searchHistory.saved),
                loading: false,
            },
        };
    }),
    on(filtersActions.saveSearchError, state => ({
        ...state,
        searchHistory: {
            ...state.searchHistory,
            loading: false,
        },
    })),
    on(filtersActions.unSaveSearch, state => ({
        ...state,
        searchHistory: {
            ...state.searchHistory,
            loading: true,
        },
    })),
    on(filtersActions.unSaveSearchSuccess, (state, { search }) => {
        return {
            ...state,
            searchHistory: {
                ...state.searchHistory,
                recent: updateSearchList(state.searchHistory.recent, search),
                saved: state.searchHistory.saved.filter(s => s.id !== search.id),
                loading: false,
            },
        };
    }),
    on(filtersActions.unSaveSearchError, state => ({
        ...state,
        searchHistory: {
            ...state.searchHistory,
            loading: false,
        },
    })),
    on(filtersActions.resetSearchHistory, state => ({
        ...state,
        searchHistory: {
            ...state.searchHistory,
            recent: [],
            saved: [],
            loading: false,
        },
    })),
);

export const updateSearchList = (list: Search[], search: Search): Search[] => {
    let found = false;
    const newList = list.map(s => {
        if (s.id === search.id) {
            found = true;
            return { ...s, ...search };
        }

        return s;
    });

    return !found ? [search].concat(newList) : newList;
};
