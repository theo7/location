import { createReducer, on } from '@ngrx/store';
import { UserProfile } from '../../shared/models/models';
import * as FollowActions from '../actions/follow.actions';
import { profileActions } from '../actions/profile.actions';

export interface ProfileState {
    current: UserProfile | undefined;
    loading: boolean;
}

export const initialProfileState: ProfileState = {
    current: undefined,
    loading: false,
};

export const profileReducer = createReducer(
    initialProfileState,
    // load
    on(profileActions.load, state => ({
        ...state,
        current: undefined,
        loading: true,
    })),
    on(profileActions.loadSuccess, (state, { profile }) => ({
        ...state,
        current: profile,
        loading: false,
    })),
    on(profileActions.loadError, state => ({
        ...state,
        loading: false,
    })),
    on(FollowActions.userFollowSuccess, state => ({
        ...state,
        current: {
            ...state.current,
            isFollowing: true,
            totalFollowers: state.current!.totalFollowers! + 1,
        } as UserProfile,
    })),
    on(FollowActions.userUnfollowSuccess, state => ({
        ...state,
        current: {
            ...state.current,
            isFollowing: false,
            totalFollowers: state.current!.totalFollowers! - 1,
        } as UserProfile,
    })),
);
