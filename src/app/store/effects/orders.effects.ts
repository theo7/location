import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import { LotService } from 'src/app/shared/services/lot.service';
import * as OrdersAction from '../actions/orders.actions';

@Injectable()
export class OrdersEffects {
    constructor(private readonly actions$: Actions, private readonly lotService: LotService) {}

    loadSales$ = createEffect(() =>
        this.actions$.pipe(
            ofType(OrdersAction.loadSales),
            switchMap(() =>
                this.lotService.getUserSales().pipe(
                    map(lots => OrdersAction.loadSalesSuccess({ lots })),
                    catchError(error => of(OrdersAction.loadSalesError({ error }))),
                ),
            ),
        ),
    );

    loadPurchases$ = createEffect(() =>
        this.actions$.pipe(
            ofType(OrdersAction.loadPurchases),
            switchMap(() =>
                this.lotService.getUserPurchases().pipe(
                    map(lots => OrdersAction.loadPurchasesSuccess({ lots })),
                    catchError(error => of(OrdersAction.loadPurchasesError({ error }))),
                ),
            ),
        ),
    );
}
