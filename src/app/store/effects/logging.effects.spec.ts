import { ApplicationRef } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { Actions, EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { StorageMap } from '@ngx-pwa/local-storage';
import { cold, hot, Scheduler, time } from 'jest-marbles';
import { interval, Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { ErrorLog } from 'src/app/features/logger/logger.models';
import { LoggerService } from 'src/app/features/logger/logger.service';
import * as LoggingActions from '../actions/logging.actions';
import { LoggingEffects } from './logging.effects';
import { LOCAL_STORAGE_LOGGING } from './logging.effects.constants';

const loggingInterval = jest.fn();

jest.mock('./logging.effects.constants', () => ({
    get LOCAL_STORAGE_LOGGING() {
        return 'location.logging';
    },
    get SEND_ERRORS_INTERVAL() {
        return time('-----|');
    },
    get loggingInterval() {
        return loggingInterval;
    },
}));

describe('logging.effects', () => {
    let actions$: Observable<Actions>;
    let loggerService: LoggerService;
    let appRef: ApplicationRef;
    let storage: StorageMap;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [
                LoggingEffects,
                provideMockActions(() => actions$),
                {
                    provide: LoggerService,
                    useValue: {
                        sendErrors: jest.fn(),
                    },
                },
                {
                    provide: ApplicationRef,
                    useValue: {},
                },
                {
                    provide: StorageMap,
                    useValue: {
                        get: jest.fn(),
                        set: jest.fn(),
                    },
                },
            ],
        });

        loggerService = TestBed.inject(LoggerService);
        appRef = TestBed.inject(ApplicationRef);
        storage = TestBed.inject(StorageMap);
    });

    const when = {
        app: {
            isStable: (...params: Parameters<typeof cold>) => ((appRef as any).isStable = cold(...params)),
        },
    };

    describe('saveError$', () => {
        beforeEach(() => {
            when.app.isStable('--');
        });

        it('should not dispatch actions', () => {
            const effects = TestBed.inject(LoggingEffects);
            const metadata: EffectsMetadata<LoggingEffects> = getEffectsMetadata(effects);

            expect(metadata.saveError$!.dispatch).toBeFalsy();
        });

        it('should add new log if no one saved before', () => {
            Scheduler.instance!.run(({ flush }) => {
                const storageContext = {
                    [LOCAL_STORAGE_LOGGING]: undefined,
                };

                jest.spyOn(storage, 'get').mockImplementation(key => {
                    return cold('-(a|)', { a: storageContext[key] });
                });
                jest.spyOn(storage, 'set').mockImplementation((key, data) => {
                    storageContext[key] = data;
                    return cold('-(a|)', { a: undefined });
                });

                const newLog = {
                    key: 'log-1',
                } as ErrorLog;
                const action = LoggingActions.logError({ errorLog: newLog });

                actions$ = hot('--a-', { a: action });
                const expected = cold('----u', { u: undefined });

                const effects = TestBed.inject(LoggingEffects);
                expect(effects.saveError$).toBeObservable(expected);

                flush();
                expect(storage.set).toHaveBeenCalledTimes(1);
                expect(storage.set).toHaveBeenCalledWith(LOCAL_STORAGE_LOGGING, [newLog]);
            });
        });

        it('should append a new log to the existing logs', () => {
            Scheduler.instance!.run(({ flush }) => {
                const existingLogs = [{ key: 'log-1' }, { key: 'log-2' }] as ErrorLog[];

                const storageContext = {
                    [LOCAL_STORAGE_LOGGING]: existingLogs,
                };

                jest.spyOn(storage, 'get').mockImplementation(key => {
                    return cold('-(a|)', { a: storageContext[key] });
                });
                jest.spyOn(storage, 'set').mockImplementation((key, data) => {
                    storageContext[key] = data;
                    return cold('-(a|)', { a: undefined });
                });

                const newLog = {
                    key: 'log-3',
                } as ErrorLog;
                const action = LoggingActions.logError({ errorLog: newLog });

                actions$ = hot('--a-', { a: action });
                const expected = cold('----u', { u: undefined });

                const effects = TestBed.inject(LoggingEffects);
                expect(effects.saveError$).toBeObservable(expected);

                flush();
                expect(storage.set).toHaveBeenCalledTimes(1);
                expect(storage.set).toHaveBeenCalledWith(LOCAL_STORAGE_LOGGING, existingLogs.concat(newLog));
            });
        });

        it('should add new log even if we cannot get existing logs', () => {
            Scheduler.instance!.run(({ flush }) => {
                const storageContext = {
                    [LOCAL_STORAGE_LOGGING]: undefined,
                };

                jest.spyOn(storage, 'get').mockImplementation(() => {
                    return cold('-#');
                });
                jest.spyOn(storage, 'set').mockImplementation((key, data) => {
                    storageContext[key] = data;
                    return cold('-(a|)', { a: undefined });
                });

                const newLog = {
                    key: 'log-1',
                } as ErrorLog;
                const action = LoggingActions.logError({ errorLog: newLog });

                actions$ = hot('--a-', { a: action });
                const expected = cold('----u', { u: undefined });

                const effects = TestBed.inject(LoggingEffects);
                expect(effects.saveError$).toBeObservable(expected);

                flush();
                expect(storage.set).toHaveBeenCalledTimes(1);
                expect(storage.set).toHaveBeenCalledWith(LOCAL_STORAGE_LOGGING, [newLog]);
            });
        });
    });

    describe('batchLogging$', () => {
        it('should not dispatch actions', () => {
            when.app.isStable('-t', { t: true });

            const effects = TestBed.inject(LoggingEffects);
            const metadata: EffectsMetadata<LoggingEffects> = getEffectsMetadata(effects);

            expect(metadata.batchLogging$!.dispatch).toBeFalsy();
        });

        it('should never send errors if no logs', () => {
            Scheduler.instance!.run(({ flush }) => {
                when.app.isStable('-t', { t: true });

                const logs = [] as ErrorLog[];

                const storageContext = {
                    [LOCAL_STORAGE_LOGGING]: logs,
                };

                jest.spyOn(storage, 'get').mockImplementation(key => {
                    return cold('-(a|)', { a: storageContext[key] });
                });
                jest.spyOn(storage, 'set').mockImplementation((key, data) => {
                    storageContext[key] = data;
                    return cold('-(a|)', { a: undefined });
                });

                loggingInterval.mockReturnValue(interval(time('-----|')).pipe(take(3)));

                const effects = TestBed.inject(LoggingEffects);

                const expected = cold('- ----- ----- -----|');
                expect(effects.batchLogging$).toBeObservable(expected);

                flush();
            });
        });

        it('should send errors if 3 logs', () => {
            Scheduler.instance!.run(({ flush }) => {
                when.app.isStable('-t', { t: true });

                const logs = [
                    {
                        key: 'log-1',
                    },
                    {
                        key: 'log-2',
                    },
                    {
                        key: 'log-3',
                    },
                ] as ErrorLog[];

                const storageContext = {
                    [LOCAL_STORAGE_LOGGING]: logs,
                };

                jest.spyOn(storage, 'get').mockImplementation(key => {
                    return cold('-(a|)', { a: storageContext[key] });
                });
                jest.spyOn(storage, 'set').mockImplementation((key, data) => {
                    storageContext[key] = data;
                    return cold('-(a|)', { a: undefined });
                });

                jest.spyOn(loggerService, 'sendErrors').mockReturnValue(cold('-(a|)', { a: {} }));
                loggingInterval.mockReturnValue(interval(time('-----|')).pipe(take(3)));

                const effects = TestBed.inject(LoggingEffects);

                const expected = cold('- ----- ----u ----- -|', { u: undefined });
                expect(effects.batchLogging$).toBeObservable(expected);

                flush();
                expect(loggerService.sendErrors).toHaveBeenCalledTimes(1);
            });
        });

        it('should retry if sending errors failed', () => {
            Scheduler.instance!.run(({ flush }) => {
                when.app.isStable('-t', { t: true });

                const logs = [
                    {
                        key: 'log-1',
                    },
                    {
                        key: 'log-2',
                    },
                    {
                        key: 'log-3',
                    },
                ] as ErrorLog[];

                const storageContext = {
                    [LOCAL_STORAGE_LOGGING]: logs,
                };

                jest.spyOn(storage, 'get').mockImplementation(key => {
                    return cold('-(a|)', { a: storageContext[key] });
                });
                jest.spyOn(storage, 'set').mockImplementation((key, data) => {
                    storageContext[key] = data;
                    return cold('-(a|)', { a: undefined });
                });

                jest.spyOn(loggerService, 'sendErrors').mockReturnValue(cold('-#'));
                loggingInterval.mockReturnValue(interval(time('-----|')).pipe(take(3)));

                const effects = TestBed.inject(LoggingEffects);

                const expected = cold('- ----- ----- -----|');
                expect(effects.batchLogging$).toBeObservable(expected);

                flush();
                expect(loggerService.sendErrors).toHaveBeenCalledTimes(3);
            });
        });

        it('should split if more than 50 errors to send', () => {
            Scheduler.instance!.run(({ flush }) => {
                when.app.isStable('-t', { t: true });

                const logs = [...Array(200).keys()].map(index => {
                    return {
                        key: `logs-${index + 1}`,
                    };
                });

                const storageContext = {
                    [LOCAL_STORAGE_LOGGING]: logs,
                };

                jest.spyOn(storage, 'get').mockImplementation(key => {
                    return cold('-(a|)', { a: storageContext[key] });
                });
                jest.spyOn(storage, 'set').mockImplementation((key, data) => {
                    storageContext[key] = data;
                    return cold('-(a|)', { a: undefined });
                });

                jest.spyOn(loggerService, 'sendErrors').mockReturnValue(cold('-(a|)', { a: {} }));
                loggingInterval.mockReturnValue(interval(time('-----|')).pipe(take(3)));

                const effects = TestBed.inject(LoggingEffects);

                const expected = cold('- ----- ----u ----u ----(u|)', { u: undefined });
                expect(effects.batchLogging$).toBeObservable(expected);

                flush();
                expect(loggerService.sendErrors).toHaveBeenCalledTimes(3);
            });
        });
    });
});
