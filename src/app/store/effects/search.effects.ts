import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { isEqual } from 'lodash-es';
import { combineLatest, EMPTY, forkJoin, interval, Observable, of, timer } from 'rxjs';
import {
    buffer,
    catchError,
    distinctUntilChanged,
    filter,
    first,
    map,
    mapTo,
    shareReplay,
    skip,
    skipUntil,
    startWith,
    switchMap,
    take,
    tap,
    withLatestFrom,
} from 'rxjs/operators';
import { randomNumber } from '../../shared/functions/number.functions';
import { convertParamsToFilter, countFilter } from '../../shared/functions/product-filter.functions';
import { FilterType, SearchRouterParams } from '../../shared/models/models';
import { DeviceService } from '../../shared/services/device.service';
import { ProductsService } from '../../shared/services/products.service';
import { filtersActions } from '../actions/filters.actions';
import * as GoogleAnalyticsActions from '../actions/google-analytics.actions';
import * as SearchActions from '../actions/search.actions';
import { CategoriesSelectors } from '../services/categories-selectors.service';
import { FilterSelectors } from '../services/filter-selectors.service';
import { RepositoriesSelectors } from '../services/repositories-selectors.service';
import { RouterSelectors } from '../services/router-selectors.service';
import { SearchSelectors } from '../services/search-selectors.service';

@Injectable()
export class SearchEffects {
    private readonly filterKey: FilterType = 'search';
    shouldExecuteSearchChangeNavigation$: Observable<boolean>;

    constructor(
        private readonly actions$: Actions,
        private readonly routerSelectors: RouterSelectors,
        private readonly repositoriesSelectors: RepositoriesSelectors,
        private readonly categoriesSelectors: CategoriesSelectors,
        private readonly filterSelectors: FilterSelectors,
        private readonly searchSelectors: SearchSelectors,
        private readonly productsService: ProductsService,
        private readonly router: Router,
        private readonly device: DeviceService,
    ) {
        this.shouldExecuteSearchChangeNavigation$ = this.routerSelectors.url$.pipe(
            buffer(
                this.routerSelectors.url$.pipe(
                    skip(1),
                    withLatestFrom(this.routerSelectors.isSearchEnabled$),
                    filter(([, isSearchEnabled]) => isSearchEnabled),
                ),
            ),
            withLatestFrom(this.routerSelectors.url$),
            map(([previousUrls, currentUrl]) => {
                const urls = previousUrls.concat(currentUrl);
                const firstUrl = urls[0];
                const lastUrl = urls[urls.length - 1];

                return urls.length > 2 && firstUrl === lastUrl;
            }),
            startWith(false),
            shareReplay(),
        );
    }

    listenSearchUrlChange$ = createEffect(() =>
        this.routerSelectors.url$.pipe(
            withLatestFrom(this.routerSelectors.isSearchEnabled$),
            filter(([_, isSearchEnabled]) => isSearchEnabled),
            map(([url]) => url),
            distinctUntilChanged(),
            mapTo(SearchActions.startSearchFromUrl()),
        ),
    );

    startSearchFromUrl$ = createEffect(() =>
        this.actions$.pipe(
            ofType(SearchActions.startSearchFromUrl),
            switchMap(() =>
                forkJoin({
                    params: this.routerSelectors.pageParams$.pipe<SearchRouterParams>(first()),
                    categories: this.categoriesSelectors.allCategories$.pipe(first()),
                    brands: this.repositoriesSelectors.brands$.pipe(first()),
                    colors: this.repositoriesSelectors.colors$.pipe(first()),
                    conditions: this.repositoriesSelectors.conditions$.pipe(first()),
                    sizes: this.categoriesSelectors.allSizes$.pipe(first()),
                }),
            ),
            switchMap(({ params, categories, brands, colors, conditions, sizes }) => {
                const newFilter = convertParamsToFilter(params, categories, brands, colors, conditions, sizes);
                return of(
                    filtersActions.set({ key: this.filterKey, filter: newFilter }),
                    SearchActions.executeSearch(),
                );
            }),
        ),
    );

    executeSearch$ = createEffect(() =>
        this.actions$.pipe(
            ofType(SearchActions.executeSearch),
            switchMap(() =>
                forkJoin([
                    this.searchSelectors.getSearchState$.pipe(first()),
                    this.filterSelectors.current$(this.filterKey).pipe(first()),
                ]),
            ),
            switchMap(([searchState, searchFilter]) => {
                return this.productsService.searchProducts(searchFilter, searchState.page, searchState.limit).pipe(
                    switchMap(products => [
                        SearchActions.executeSearchSuccess({ products }),
                        GoogleAnalyticsActions.trackSearchAndFilter({
                            results: products.totalElements,
                            searchFilter,
                        }),
                    ]),
                    catchError(error => of(SearchActions.executeSearchError({ error }))),
                );
            }),
        ),
    );

    executeSearchCount$ = createEffect(() =>
        this.filterSelectors.current$(this.filterKey).pipe(
            distinctUntilChanged((a, b) => isEqual(a, b)),
            switchMap(searchFilter => {
                return this.productsService.searchProductsCount(searchFilter).pipe(
                    map(total => SearchActions.executeSearchCountSuccess({ total })),
                    catchError(error => of(SearchActions.executeSearchCountError({ error }))),
                );
            }),
        ),
    );

    executeSearchNext$ = createEffect(() =>
        this.actions$.pipe(
            ofType(SearchActions.executeSearchNext),
            switchMap(() =>
                forkJoin([
                    this.searchSelectors.getSearchState$.pipe(first()),
                    this.filterSelectors.current$(this.filterKey).pipe(first()),
                ]),
            ),
            switchMap(([searchState, searchFilter]) =>
                forkJoin([
                    this.productsService.searchProducts(searchFilter, searchState.page, searchState.limit),
                    // ⚡️ : delay to avoid scrolling to fast
                    timer(randomNumber(1000, 2000)),
                ]).pipe(
                    map(([products]) => SearchActions.executeSearchNextSuccess({ products })),
                    catchError(error => of(SearchActions.executeSearchNextError({ error }))),
                ),
            ),
        ),
    );

    navigateWhenFilterChangedOnDesktopAndSearchEnabled$ = createEffect(
        () =>
            combineLatest([this.routerSelectors.isSearchEnabled$, this.routerSelectors.url$]).pipe(
                switchMap(([isSearchEnabled]) => {
                    if (isSearchEnabled) {
                        return this.filterSelectors.currentRouterParams$(this.filterKey).pipe(
                            distinctUntilChanged((a, b) => isEqual(a, b)),
                            withLatestFrom(this.device.isDesktop$),
                            filter(([_, isDesktop]) => isDesktop),
                            map(([queryParams]) => queryParams),
                            skipUntil(
                                this.shouldExecuteSearchChangeNavigation$.pipe(
                                    switchMap(isSamePageWithOtherPageBetween => {
                                        if (isSamePageWithOtherPageBetween) {
                                            return interval(100);
                                        }
                                        return this.actions$.pipe(ofType(SearchActions.executeSearch));
                                    }),
                                ),
                            ), // 💡 : prevent infinite loop
                            take(1),
                        );
                    }
                    return EMPTY;
                }),
                tap(queryParams => this.router.navigate(['search'], { queryParams, replaceUrl: true })),
            ),
        { dispatch: false },
    );

    // when filter
    onFilterChangedOnMobileAndActive$ = createEffect(() =>
        this.actions$.pipe(
            ofType(filtersActions.set),
            withLatestFrom(this.device.isHandset$, this.routerSelectors.isSearchRouteActive$),
            filter(
                ([{ filter, key }, isHandset, isSearchRouteActive]) =>
                    isHandset && isSearchRouteActive && key === this.filterKey && countFilter(filter) > 0,
            ),
            mapTo(SearchActions.setShowFilter({ showFilter: false })),
        ),
    );
}
