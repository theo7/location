import { TestBed } from '@angular/core/testing';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Actions } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { TranslateService } from '@ngx-translate/core';
import { cold, hot } from 'jest-marbles';
import { Observable } from 'rxjs';
import { Lot } from 'src/app/shared/models/models';
import { ChatService } from 'src/app/shared/services/chat.service';
import { LotService } from 'src/app/shared/services/lot.service';
import { ProductsService } from 'src/app/shared/services/products.service';
import * as DiscussionsActions from '../actions/discussions.actions';
import { DiscussionsEffects } from './discussions.effects';

describe('discussions.effects', () => {
    let actions$: Observable<Actions>;
    let effects: DiscussionsEffects;
    let lotService: LotService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [
                DiscussionsEffects,
                provideMockActions(() => actions$),
                {
                    provide: ChatService,
                    useValue: {},
                },
                {
                    provide: ProductsService,
                    useValue: {},
                },
                {
                    provide: LotService,
                    useValue: {
                        cancelPackage: jest.fn(),
                    },
                },
                {
                    provide: Router,
                    useValue: {},
                },
                {
                    provide: MatDialog,
                    useValue: {},
                },
                {
                    provide: TranslateService,
                    useValue: {},
                },
                {
                    provide: MatSnackBar,
                    useValue: {},
                },
            ],
        });

        effects = TestBed.inject(DiscussionsEffects);
        lotService = TestBed.inject(LotService);
    });

    describe('cancelPackage$', () => {
        const lotId = 14;
        const action = DiscussionsActions.cancelPackage({ lotId });

        it('should dispatch cancelPackageSuccess action', () => {
            const lot = { id: 14 } as Lot;

            jest.spyOn(lotService, 'cancelPackage').mockReturnValue(cold('-(l|)', { l: lot }));

            const completion = DiscussionsActions.cancelPackageSuccess({ lot });

            actions$ = hot('-a-', { a: action });
            const expected = cold('--b', { b: completion });
            expect(effects.cancelPackage$).toBeObservable(expected);
        });

        it('should dispatch cancelPackageError action', () => {
            const error = 'Cannot cancel this package';
            jest.spyOn(lotService, 'cancelPackage').mockReturnValue(cold('-#', undefined, error));

            const completion = DiscussionsActions.cancelPackageError({ lotId });

            actions$ = hot('-a-', { a: action });
            const expected = cold('--b', { b: completion });
            expect(effects.cancelPackage$).toBeObservable(expected);
        });
    });
});
