import { of } from '@angular-devkit/core/node_modules/rxjs';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { catchError, map, switchMap } from 'rxjs/operators';
import { FollowService } from 'src/app/shared/services/follow.service';
import { UserService } from 'src/app/shared/services/user.service';
import * as FollowActions from '../actions/follow.actions';
import { RootState } from '../models';

@Injectable()
export class FollowEffects {
    constructor(
        private readonly followService: FollowService,
        private readonly userService: UserService,
        private readonly actions$: Actions,
        private readonly store$: Store<RootState>,
    ) {}

    follow$ = createEffect(() =>
        this.actions$.pipe(
            ofType(FollowActions.userFollow),
            switchMap(payload => {
                return this.followService.follow(payload.userId).pipe(
                    map(following => FollowActions.userFollowSuccess({ following })),
                    catchError(error => of(FollowActions.userFollowError({ error }))),
                );
            }),
        ),
    );

    unfollow$ = createEffect(() =>
        this.actions$.pipe(
            ofType(FollowActions.userUnfollow),
            switchMap(payload => {
                return this.followService.unfollow(payload.userId).pipe(
                    map(() => FollowActions.userUnfollowSuccess({ userId: payload.userId })),
                    catchError(error => of(FollowActions.userUnfollowError({ error }))),
                );
            }),
        ),
    );

    getUserFollowing$ = createEffect(() =>
        this.actions$.pipe(
            ofType(FollowActions.getUserFollowing),
            switchMap(payload => {
                return this.followService.getUserFollowing(payload.userId, payload.page).pipe(
                    map(followings => FollowActions.getUserFollowingSuccess({ followings })),
                    catchError(error => of(FollowActions.getUserFollowingError({ error }))),
                );
            }),
        ),
    );

    getUserFollower$ = createEffect(() =>
        this.actions$.pipe(
            ofType(FollowActions.getUserFollower),
            switchMap(payload => {
                return this.followService.getUserFollower(payload.userId, payload.page).pipe(
                    map(followers => FollowActions.getUserFollowerSuccess({ followers })),
                    catchError(error => of(FollowActions.getUserFollowerError({ error }))),
                );
            }),
        ),
    );
}
