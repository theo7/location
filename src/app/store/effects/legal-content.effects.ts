import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { tap } from 'rxjs/operators';
import { LegalContentDialogComponent } from '../../dialogs/legal-content-dialog/legal-content-dialog.component';
import * as LegalContentActions from '../actions/legal-content.actions';

@Injectable()
export class LegalContentEffects {
    constructor(private readonly actions$: Actions, private readonly dialog: MatDialog) {}

    openDialog$ = createEffect(
        () => () =>
            this.actions$.pipe(
                ofType(LegalContentActions.openDialog),
                tap(payload =>
                    this.dialog.open(LegalContentDialogComponent, {
                        width: '100vw',
                        maxWidth: '100vw',
                        height: '100vh',
                        panelClass: 'full-screen-dialog',
                        data: {
                            legalContentProp: payload.legalContentProp,
                        },
                    }),
                ),
            ),
        { dispatch: false },
    );
}
