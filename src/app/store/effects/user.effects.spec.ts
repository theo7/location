import { TestBed } from '@angular/core/testing';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Actions, EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { TranslateService } from '@ngx-translate/core';
import { cold, hot } from 'jest-marbles';
import { EMPTY, Observable, of } from 'rxjs';
import { AuthenticationService } from 'src/app/features/authentication/service/authentication.service';
import { UserService } from 'src/app/shared/services/user.service';
import { NotificationDialogComponent } from '../../shared/components/notification-dialog/notification-dialog.component';
import * as UserActions from '../actions/user.actions';
import { UserProfileSelectors } from '../services/user-selectors.service';
import { UserEffects } from './user.effects';

describe('user.effects', () => {
    let actions$: Observable<Actions>;
    let effects: UserEffects;
    let authenticationService: AuthenticationService;
    let userService: UserService;
    let userProfileSelectors: UserProfileSelectors;
    let dialog: MatDialog;
    let translateService: TranslateService;
    let router: Router;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [
                UserEffects,
                provideMockActions(() => actions$),
                {
                    provide: AuthenticationService,
                    useValue: {
                        createUser: jest.fn(),
                    },
                },
                {
                    provide: UserService,
                    useValue: {},
                },
                {
                    provide: UserProfileSelectors,
                    useValue: {},
                },
                {
                    provide: MatDialog,
                    useValue: {
                        open: jest.fn(),
                    },
                },
                {
                    provide: TranslateService,
                    useValue: {
                        instant: jest.fn(),
                    },
                },
                {
                    provide: Router,
                    useValue: {
                        navigate: jest.fn(),
                    },
                },
            ],
        });

        effects = TestBed.inject(UserEffects);
        authenticationService = TestBed.inject(AuthenticationService);
        userService = TestBed.inject(UserService);
        userProfileSelectors = TestBed.inject(UserProfileSelectors);
        dialog = TestBed.inject(MatDialog);
        translateService = TestBed.inject(TranslateService);
        router = TestBed.inject(Router);
    });

    describe('redirectOnLogout$', () => {
        it('should not dispatch actions', () => {
            const metadata: EffectsMetadata<UserEffects> = getEffectsMetadata(effects);

            expect(metadata.redirectOnLogout$!.dispatch).toBeFalsy();
        });

        it('should navigate to home page', () => {
            const action = UserActions.logout();

            jest.spyOn(router, 'navigate').mockReturnValue(cold('-t', { t: true }) as any as Promise<boolean>);

            actions$ = hot('-a-', { a: action });

            expect(effects.redirectOnLogout$).toSatisfyOnFlush(() => {
                expect(router.navigate).toHaveBeenCalledWith(['/']);
            });
        });
    });

    describe('signUp$', () => {
        it('should be signUpSuccess if create user succeed', () => {
            const signUpData = {
                username: 'Alpha',
            };

            const action = UserActions.signUp({ signUpData });
            actions$ = hot('-a-', { a: action });

            jest.spyOn(authenticationService, 'createUser').mockReturnValue(cold('-(u|)', { u: {} }));

            const signUpSuccess = UserActions.signUpSuccess();

            const expected = cold('--s', { s: signUpSuccess });
            expect(effects.signUp$).toBeObservable(expected);
        });

        it('should be signUpError if cannot create user', () => {
            const signUpData = {
                username: 'Alpha',
            };

            const error = 'Cannot create user...';

            const action = UserActions.signUp({ signUpData });
            actions$ = hot('-a-', { a: action });

            jest.spyOn(authenticationService, 'createUser').mockReturnValue(cold('-#', undefined, error));

            const signUpError = UserActions.signUpError({ error });

            const expected = cold('--e', { e: signUpError });
            expect(effects.signUp$).toBeObservable(expected);
        });
    });

    describe('signUpSuccess$', () => {
        beforeEach(() => {
            jest.spyOn(translateService, 'instant').mockReturnValue('');
        });

        it('should not dispatch actions', () => {
            const metadata: EffectsMetadata<UserEffects> = getEffectsMetadata(effects);

            expect(metadata.signUpSuccess$!.dispatch).toBeFalsy();
        });

        it('should open dialog of SuccessNotificationDialogComponent', () => {
            const action = UserActions.signUpSuccess();
            actions$ = hot('-a-', { a: action });

            jest.spyOn(dialog, 'open').mockImplementation(component => {
                return {
                    afterClosed: () => EMPTY as Observable<boolean>,
                } as MatDialogRef<typeof component>;
            });

            expect(effects.signUpSuccess$).toSatisfyOnFlush(() => {
                expect(dialog.open).toHaveBeenCalledWith(NotificationDialogComponent, expect.any(Object));
                expect(router.navigate).not.toHaveBeenCalled();
            });
        });

        it('should navigate to login when SuccessNotificationDialogComponent closed', () => {
            const action = UserActions.signUpSuccess();
            actions$ = hot('-a-', { a: action });

            jest.spyOn(dialog, 'open').mockImplementation(component => {
                return {
                    afterClosed: () => of(true),
                } as MatDialogRef<typeof component>;
            });

            expect(effects.signUpSuccess$).toSatisfyOnFlush(() => {
                expect(dialog.open).toHaveBeenCalledWith(NotificationDialogComponent, expect.any(Object));
                expect(router.navigate).toHaveBeenCalledWith(['login'], { queryParamsHandling: 'preserve' });
            });
        });
    });
});
