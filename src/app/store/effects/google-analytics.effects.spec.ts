import { TestBed } from '@angular/core/testing';
import { NavigationEnd, NavigationStart, Router } from '@angular/router';
import { Actions, EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { cold, hot, Scheduler, time } from 'jest-marbles';
import { Observable } from 'rxjs';
import { GoogleAnalyticsService } from 'src/app/features/analytics/google-analytics.service';
import { ProductFilter } from 'src/app/shared/models/models';
import { CookieConsentService } from '../../features/cookie-consent/services/cookie-consent.service';
import * as GoogleAnalyticsActions from '../actions/google-analytics.actions';
import { GoogleAnalyticsEffects } from './google-analytics.effects';

jest.mock('./google-analytics.effects.constants', () => ({
    get SEARCH_TRACKING_DEBOUNCE_TIME() {
        return time('-----|');
    },
}));

describe('google-analytics.effects', () => {
    let actions$: Observable<Actions>;
    let router: Router;
    let googleAnalyticsService: GoogleAnalyticsService;
    let cookieConsentService: CookieConsentService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [
                GoogleAnalyticsEffects,
                provideMockActions(() => actions$),
                {
                    provide: Router,
                    useValue: {},
                },
                {
                    provide: GoogleAnalyticsService,
                    useValue: {
                        trackSearchAndFilter: jest.fn(),
                        trackView: jest.fn(),
                    },
                },
                {
                    provide: CookieConsentService,
                    useValue: {
                        hasConsentedFor$: jest.fn(),
                    },
                },
            ],
        });

        router = TestBed.inject(Router);
        googleAnalyticsService = TestBed.inject(GoogleAnalyticsService);
        cookieConsentService = TestBed.inject(CookieConsentService);
    });

    beforeEach(() => {
        (router as any).events = hot('--');
    });

    describe('trackSearchAndFilter$', () => {
        it('should not dispatch actions', () => {
            const effects = TestBed.inject(GoogleAnalyticsEffects);

            const metadata: EffectsMetadata<GoogleAnalyticsEffects> = getEffectsMetadata(effects);

            expect(metadata.trackSearchAndFilter$!.dispatch).toBeFalsy();
        });

        it('should call trackSearchAndFilter after 3s', () => {
            Scheduler.instance!.run(({ flush }) => {
                const searchFilter = {
                    brands: [],
                    sizes: [],
                } as ProductFilter;

                const results = 12;

                const action = GoogleAnalyticsActions.trackSearchAndFilter({ searchFilter, results });

                actions$ = hot('-a--', { a: action });

                const expected = cold('------a', { a: action });

                const effects = TestBed.inject(GoogleAnalyticsEffects);

                expect(effects.trackSearchAndFilter$).toBeObservable(expected);

                flush();
                expect(googleAnalyticsService.trackSearchAndFilter).toHaveBeenCalledWith(searchFilter, results);
            });
        });
    });

    describe('trackView$', () => {
        it('should not dispatch actions', () => {
            jest.spyOn(cookieConsentService, 'hasConsentedFor$').mockReturnValue(hot('-t', { t: true }));

            const effects = TestBed.inject(GoogleAnalyticsEffects);

            const metadata: EffectsMetadata<GoogleAnalyticsEffects> = getEffectsMetadata(effects);

            expect(metadata.trackView$!.dispatch).toBeFalsy();
        });

        it('should track view if NavigationEnd events detected', () => {
            const event = new NavigationEnd(1, '/', '/redirect');

            jest.spyOn(cookieConsentService, 'hasConsentedFor$').mockReturnValue(hot('-t', { t: true }));
            (router as any).events = hot('---e', { e: event });

            const effects = TestBed.inject(GoogleAnalyticsEffects);

            expect(effects.trackView$).toSatisfyOnFlush(() => {
                expect(googleAnalyticsService.trackView).toHaveBeenCalledWith('/redirect');
            });
        });

        it('should not track view if NavigationStart events detected', () => {
            const event = new NavigationStart(1, '/');

            jest.spyOn(cookieConsentService, 'hasConsentedFor$').mockReturnValue(hot('-t', { t: true }));
            (router as any).events = hot('---e', { e: event });

            const effects = TestBed.inject(GoogleAnalyticsEffects);

            expect(effects.trackView$).toSatisfyOnFlush(() => {
                expect(googleAnalyticsService.trackView).not.toHaveBeenCalled();
            });
        });

        it('should track view if user consented after NavigationEnd', () => {
            const event = new NavigationEnd(1, '/', '/redirect');

            (router as any).events = hot('-e', { e: event });
            jest.spyOn(cookieConsentService, 'hasConsentedFor$').mockReturnValue(hot('f--t', { t: true, f: false }));

            const effects = TestBed.inject(GoogleAnalyticsEffects);

            expect(effects.trackView$).toSatisfyOnFlush(() => {
                expect(googleAnalyticsService.trackView).toHaveBeenCalledWith('/redirect');
            });
        });

        it('should stop to emit track view if user removed consent', () => {
            const event = new NavigationEnd(1, '/', '/redirect');

            (router as any).events = hot('--e--e-', { e: event });
            jest.spyOn(cookieConsentService, 'hasConsentedFor$').mockReturnValue(hot('-t--f--', { t: true, f: false }));

            const effects = TestBed.inject(GoogleAnalyticsEffects);

            expect(effects.trackView$).toSatisfyOnFlush(() => {
                expect(googleAnalyticsService.trackView).toHaveBeenCalledTimes(1);
            });
        });

        it('should not track the same view twice', () => {
            const event = new NavigationEnd(1, '/', '/redirect');

            (router as any).events = hot('--e-', { e: event });
            jest.spyOn(cookieConsentService, 'hasConsentedFor$').mockReturnValue(
                hot('-t--f--t', { t: true, f: false }),
            );

            const effects = TestBed.inject(GoogleAnalyticsEffects);

            expect(effects.trackView$).toSatisfyOnFlush(() => {
                expect(googleAnalyticsService.trackView).toHaveBeenCalledTimes(1);
            });
        });

        it('should track view changed', () => {
            const event1 = new NavigationEnd(1, '/', '/redirect');
            const event2 = new NavigationEnd(2, '/redirect', '/home');

            (router as any).events = hot('--e--f-', { e: event1, f: event2 });
            jest.spyOn(cookieConsentService, 'hasConsentedFor$').mockReturnValue(
                hot('-t--f--t', { t: true, f: false }),
            );

            const effects = TestBed.inject(GoogleAnalyticsEffects);

            expect(effects.trackView$).toSatisfyOnFlush(() => {
                expect(googleAnalyticsService.trackView).toHaveBeenCalledTimes(2);
            });
        });
    });
});
