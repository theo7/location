import { TestBed } from '@angular/core/testing';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Actions, getEffectsMetadata } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { cold, hot } from 'jest-marbles';
import { Observable, of } from 'rxjs';
import { DeleteAccountDialogComponent } from 'src/app/dialogs/delete-account-dialog/delete-account-dialog.component';
import { FollowService } from 'src/app/shared/services/follow.service';
import { UserService } from 'src/app/shared/services/user.service';
import {
    ProductFilter,
    SearchRouterParams,
    UserFollowingProfile,
    UserFollowingSummary,
    UserProfile,
    UserRatingSummary,
} from '../../shared/models/models';
import { DeviceService } from '../../shared/services/device.service';
import { SeoService } from '../../shared/services/seo.service';
import { filtersActions } from '../actions/filters.actions';
import { getDressing, getOwnDressing } from '../actions/products.actions';
import { profileActions } from '../actions/profile.actions';
import * as UserActions from '../actions/user.actions';
import { CategoriesSelectors } from '../services/categories-selectors.service';
import { FilterSelectors } from '../services/filter-selectors.service';
import { ProfileSelectors } from '../services/profile-selectors.service';
import { RepositoriesSelectors } from '../services/repositories-selectors.service';
import { RouterSelectors } from '../services/router-selectors.service';
import { UserProfileSelectors } from '../services/user-selectors.service';
import { ProfileEffects } from './profile.effects';

jest.mock('src/app/dialogs/delete-account-dialog/delete-account-dialog.component', () => ({
    get DeleteAccountDialogComponent() {
        return {};
    },
}));

jest.mock('../../imports.dynamic', () => ({
    get deleteAccountDialogComponent$() {
        return of(DeleteAccountDialogComponent);
    },
}));

describe('profile.effects', () => {
    let actions$: Observable<Actions>;
    let dialog: MatDialog;
    let userService: UserService;
    let followService: FollowService;
    let filterSelectors: FilterSelectors;
    let profileSelectors: ProfileSelectors;
    let routerSelectors: RouterSelectors;
    let deviceService: DeviceService;
    let router: Router;
    let categoriesSelectors: CategoriesSelectors;
    let repositoriesSelectors: RepositoriesSelectors;
    let userProfileSelectors: UserProfileSelectors;
    let seoService: SeoService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [
                ProfileEffects,
                provideMockActions(() => actions$),
                {
                    provide: MatDialog,
                    useValue: {
                        open: jest.fn(),
                    },
                },
                {
                    provide: UserService,
                    useValue: {
                        deleteOwnProfile: jest.fn(),
                        getProfile: jest.fn(),
                        findRatingSummary: jest.fn(),
                        getFollowingSummary: jest.fn(),
                    },
                },
                {
                    provide: FollowService,
                    useValue: {
                        isFollowing: jest.fn(),
                    },
                },
                {
                    provide: FilterSelectors,
                    useValue: {
                        current$: jest.fn(),
                        currentRouterParams$: jest.fn(),
                    },
                },
                {
                    provide: ProfileSelectors,
                    useValue: {},
                },
                {
                    provide: RouterSelectors,
                    useValue: {},
                },
                {
                    provide: CategoriesSelectors,
                    useValue: {},
                },
                {
                    provide: RepositoriesSelectors,
                    useValue: {},
                },
                {
                    provide: DeviceService,
                    useValue: {},
                },
                {
                    provide: UserProfileSelectors,
                    useValue: {},
                },
                {
                    provide: SeoService,
                    useValue: {
                        configureDressingPage: jest.fn(),
                    },
                },
                {
                    provide: Router,
                    useValue: {
                        navigate: jest.fn(),
                    },
                },
            ],
        });

        dialog = TestBed.inject(MatDialog);
        userService = TestBed.inject(UserService);
        followService = TestBed.inject(FollowService);
        filterSelectors = TestBed.inject(FilterSelectors);
        profileSelectors = TestBed.inject(ProfileSelectors);
        routerSelectors = TestBed.inject(RouterSelectors);
        deviceService = TestBed.inject(DeviceService);
        router = TestBed.inject(Router);
        categoriesSelectors = TestBed.inject(CategoriesSelectors);
        repositoriesSelectors = TestBed.inject(RepositoriesSelectors);
        seoService = TestBed.inject(SeoService);
        userProfileSelectors = TestBed.inject(UserProfileSelectors);

        routerSelectors.url$ = cold('---');
    });

    describe('listenForFirstUrlChange$', () => {
        it('should dispatch set filter action', () => {
            // mock
            routerSelectors.url$ = cold('-u', { u: '/profile/1' });
            routerSelectors.isDressingPageVisible$ = cold('-t', { t: true });

            const urlParams: SearchRouterParams = {
                maxPrice: 10,
                searchText: 'test',
            };
            routerSelectors.pageParams$ = cold('-p', { p: urlParams });
            categoriesSelectors.allCategories$ = cold('-c', { c: [] });
            repositoriesSelectors.brands$ = cold('-b', { b: [] });
            repositoriesSelectors.colors$ = cold('-c', { c: [] });
            repositoriesSelectors.conditions$ = cold('-c', { c: [] });
            categoriesSelectors.allSizes$ = cold('-s', { s: [] });

            // call
            const effects = TestBed.inject(ProfileEffects);

            // asserts
            expect(effects.listenForFirstUrlChange$).toBeObservable(
                cold('--a', {
                    a: filtersActions.set({
                        key: 'dressing',
                        filter: {
                            maxPrice: 10,
                            searchText: 'test',
                        },
                    }),
                }),
            );
        });

        it('should do nothing when profile route is not active', () => {
            // mock
            routerSelectors.url$ = cold('-u', { u: '/search' });
            routerSelectors.isDressingPageVisible$ = cold('-f', { f: false });

            // call
            const effects = TestBed.inject(ProfileEffects);

            // asserts
            expect(effects.listenForFirstUrlChange$).toBeObservable(cold('---'));
        });
    });

    describe('changeDressingWhenFilterChangedAndProfileActive$', () => {
        const filter: ProductFilter = {};

        it('should dispatch load action on reload', () => {
            // mock
            actions$ = hot('-a-', { a: filtersActions.set({ key: 'dressing', filter }) });
            routerSelectors.isDressingPageVisible$ = cold('t-', { t: true });
            routerSelectors.userId$ = cold('t-', { t: 1 });
            profileSelectors.current$ = cold('u-', { u: null });
            userProfileSelectors.userProfile$ = cold('u-', { u: { id: 1 } });

            // call
            const effects = TestBed.inject(ProfileEffects);

            // asserts
            expect(effects.changeDressingWhenFilterChangedAndProfileActive$).toBeObservable(
                cold('-(ab)', {
                    a: profileActions.load({ userId: 1 }),
                    b: getOwnDressing(),
                }),
            );
        });

        it('should dispatch get dressing action when user is not logged', () => {
            // mock
            actions$ = hot('-a-', { a: filtersActions.set({ key: 'dressing', filter }) });
            routerSelectors.isDressingPageVisible$ = cold('t-', { t: true });
            routerSelectors.userId$ = cold('t-', { t: 1 });
            profileSelectors.current$ = cold('u-', { u: { id: 1 } });
            userProfileSelectors.userProfile$ = cold('u-', { u: null });

            // call
            const effects = TestBed.inject(ProfileEffects);

            // asserts
            expect(effects.changeDressingWhenFilterChangedAndProfileActive$).toBeObservable(
                cold('-a', {
                    a: getDressing({ userId: 1 }),
                }),
            );
        });

        it('should dispatch get dressing action when current user view another profile', () => {
            // mock
            actions$ = hot('-a-', { a: filtersActions.set({ key: 'dressing', filter }) });
            routerSelectors.isDressingPageVisible$ = cold('t-', { t: true });
            routerSelectors.userId$ = cold('t-', { t: 1 });
            profileSelectors.current$ = cold('u-', { u: { id: 1 } });
            userProfileSelectors.userProfile$ = cold('u-', { u: { id: 2 } });

            // call
            const effects = TestBed.inject(ProfileEffects);

            // asserts
            expect(effects.changeDressingWhenFilterChangedAndProfileActive$).toBeObservable(
                cold('-a', {
                    a: getDressing({ userId: 1 }),
                }),
            );
        });

        it('should dispatch get own dressing action when current user view his profile', () => {
            // mock
            actions$ = hot('-a-', { a: filtersActions.set({ key: 'dressing', filter }) });
            routerSelectors.isDressingPageVisible$ = cold('t-', { t: true });
            routerSelectors.userId$ = cold('t-', { t: 1 });
            profileSelectors.current$ = cold('u-', { u: { id: 1 } });
            userProfileSelectors.userProfile$ = cold('u-', { u: { id: 1 } });

            // call
            const effects = TestBed.inject(ProfileEffects);

            // asserts
            expect(effects.changeDressingWhenFilterChangedAndProfileActive$).toBeObservable(
                cold('-a', {
                    a: getOwnDressing(),
                }),
            );
        });

        it('should do nothing when profile route is not active', () => {
            // mock
            actions$ = hot('-a-', { a: filtersActions.set({ key: 'dressing', filter }) });
            routerSelectors.isDressingPageVisible$ = cold('f-', { f: false });
            routerSelectors.userId$ = cold('t-', { t: 1 });
            profileSelectors.current$ = cold('u-', { u: { id: 1 } });
            userProfileSelectors.userProfile$ = cold('u-', { u: { id: 1 } });

            // call
            const effects = TestBed.inject(ProfileEffects);

            // asserts
            expect(effects.changeDressingWhenFilterChangedAndProfileActive$).toBeObservable(cold('---'));
        });

        it('should use own dressing if no user id in url', () => {
            // mock
            actions$ = hot('-a-', { a: filtersActions.set({ key: 'dressing', filter }) });
            routerSelectors.isDressingPageVisible$ = cold('t-', { t: true });
            routerSelectors.userId$ = cold('t-', { t: undefined });
            profileSelectors.current$ = cold('u-', { u: { id: 1 } });
            userProfileSelectors.userProfile$ = cold('u-', { u: { id: 1 } });

            // call
            const effects = TestBed.inject(ProfileEffects);

            // asserts
            expect(effects.changeDressingWhenFilterChangedAndProfileActive$).toBeObservable(
                cold('-a', {
                    a: getOwnDressing(),
                }),
            );
        });

        it('should do nothing when other filter changed', () => {
            // mock
            actions$ = hot('-a-', { a: filtersActions.set({ key: 'search', filter }) });
            routerSelectors.isDressingPageVisible$ = cold('t-', { t: true });
            routerSelectors.userId$ = cold('t-', { t: 1 });
            profileSelectors.current$ = cold('u-', { u: { id: 1 } });
            userProfileSelectors.userProfile$ = cold('u-', { u: { id: 1 } });

            // call
            const effects = TestBed.inject(ProfileEffects);

            // asserts
            expect(effects.changeDressingWhenFilterChangedAndProfileActive$).toBeObservable(cold('---'));
        });
    });

    describe('openDeleteAccountModal$', () => {
        const action = profileActions.openDeleteAccountModal();

        it('should dispatch deleteAccount if DeleteAccountDialogComponent result is true', () => {
            actions$ = hot('-a-', { a: action });

            jest.spyOn(dialog, 'open').mockImplementation(component => {
                return {
                    afterClosed: () => of(true),
                } as MatDialogRef<typeof component>;
            });

            const effects = TestBed.inject(ProfileEffects);

            const deleteAccount = profileActions.deleteAccount();

            const expected = cold('-d-', { d: deleteAccount });
            expect(effects.openDeleteAccountModal$).toBeObservable(expected);
        });

        it('should not dispatch deleteAccount if DeleteAccountDialogComponent result is false', () => {
            actions$ = hot('-a-', { a: action });

            jest.spyOn(dialog, 'open').mockImplementation(component => {
                return {
                    afterClosed: () => of(false),
                } as MatDialogRef<typeof component>;
            });

            const effects = TestBed.inject(ProfileEffects);

            const expected = cold('---');
            expect(effects.openDeleteAccountModal$).toBeObservable(expected);
        });

        it('should not dispatch deleteAccount if DeleteAccountDialogComponent result is undefined', () => {
            actions$ = hot('-a-', { a: action });

            jest.spyOn(dialog, 'open').mockImplementation(component => {
                return {
                    afterClosed: () => of(undefined),
                } as MatDialogRef<typeof component>;
            });

            const effects = TestBed.inject(ProfileEffects);

            const expected = cold('---');
            expect(effects.openDeleteAccountModal$).toBeObservable(expected);
        });

        it('should dispatch deleteAccount once', () => {
            actions$ = hot('-aa-', { a: action });

            jest.spyOn(dialog, 'open').mockImplementation(component => {
                return {
                    afterClosed: () => cold('--t', { t: true }) as Observable<boolean>,
                } as MatDialogRef<typeof component>;
            });

            const effects = TestBed.inject(ProfileEffects);

            const deleteAccount = profileActions.deleteAccount();

            const expected = cold('---d-', { d: deleteAccount });
            expect(effects.openDeleteAccountModal$).toBeObservable(expected);
        });
    });

    describe('deleteAccount$', () => {
        const action = profileActions.deleteAccount();

        it('should dispatch deleteAccountSuccess action', () => {
            const effects = TestBed.inject(ProfileEffects);

            jest.spyOn(userService, 'deleteOwnProfile').mockReturnValue(cold('-(u|)', { u: undefined }));

            const completion = profileActions.deleteAccountSuccess();

            actions$ = hot('-a-', { a: action });
            const expected = cold('--b', { b: completion });
            expect(effects.deleteAccount$).toBeObservable(expected);
        });

        it('should dispatch deleteAccountError action', () => {
            const effects = TestBed.inject(ProfileEffects);

            const error = 'Cannot delete account...';

            jest.spyOn(userService, 'deleteOwnProfile').mockReturnValue(cold('-#', undefined, error));

            const completion = profileActions.deleteAccountError({ error });

            actions$ = hot('-a-', { a: action });
            const expected = cold('--b', { b: completion });
            expect(effects.deleteAccount$).toBeObservable(expected);
        });
    });

    describe('deleteAccountSuccess$', () => {
        const action = profileActions.deleteAccountSuccess();

        it('should dispatch logout action', () => {
            const effects = TestBed.inject(ProfileEffects);

            const completion = UserActions.logout();

            actions$ = hot('-a-', { a: action });
            const expected = cold('-b-', { b: completion });
            expect(effects.deleteAccountSuccess$).toBeObservable(expected);
        });
    });

    describe('load$', () => {
        it('should dispatch loadSuccess action', () => {
            // given
            const user: UserProfile = {
                id: 1,
                firebaseId: '1',
                nickname: 'test',
                creationDate: 0,
                lastModifiedDate: 0,
            };
            const rating: UserRatingSummary = {
                totalRating: 3,
                averageRating: 2.5,
                detailedRating: new Map<number, number>(),
            };
            const following: UserFollowingSummary = {
                totalFollowing: 4,
                totalFollower: 3,
            };

            const followingProfile: UserFollowingProfile = {
                id: 0,
                nickname: '',
                totalRating: 0,
                averageRating: 0,
            };

            // mock
            actions$ = hot('-a-', { a: profileActions.load({ userId: 1 }) });
            jest.spyOn(userService, 'getProfile').mockReturnValue(cold('-(u|)', { u: user }));
            jest.spyOn(userService, 'findRatingSummary').mockReturnValue(cold('-(r|)', { r: rating }));
            jest.spyOn(userService, 'getFollowingSummary').mockReturnValue(cold('-(f|)', { f: following }));
            jest.spyOn(followService, 'isFollowing').mockReturnValue(cold('-(i|)', { i: followingProfile }));
            jest.spyOn(seoService, 'configureDressingPage').mockReturnValue();
            userProfileSelectors.isLogged$ = cold('t', { t: true });

            // call
            const effects = TestBed.inject(ProfileEffects);

            // asserts
            expect(effects.load$).toBeObservable(
                cold('---a', {
                    a: profileActions.loadSuccess({
                        profile: {
                            ...user,
                            rate: rating.averageRating,
                            totalRate: rating.totalRating,
                            totalFollowing: following.totalFollowing,
                            totalFollowers: following.totalFollower,
                            isFollowing: true,
                        },
                    }),
                }),
            );
        });
    });

    describe('loadSuccess$', () => {
        it('should dispatch getDressing action', () => {
            // given
            const user: UserProfile = {
                id: 1,
                firebaseId: '1',
                nickname: 'test',
                creationDate: 0,
                lastModifiedDate: 0,
            };

            // mock
            actions$ = hot('-a-', { a: profileActions.loadSuccess({ profile: user }) });
            userProfileSelectors.userProfile$ = cold('u-', { u: { ...user, id: 2 } });

            // call
            const effects = TestBed.inject(ProfileEffects);

            // asserts
            expect(effects.loadSuccess$).toBeObservable(
                cold('-a', {
                    a: getDressing({ userId: 1 }),
                }),
            );
        });
    });

    describe('navigateWhenFilterChangedOnDesktop$', () => {
        it('should not dispatch action', () => {
            // call
            const effects = TestBed.inject(ProfileEffects);
            const metadata = getEffectsMetadata(effects);

            // asserts
            expect(metadata.navigateWhenFilterChangedOnDesktop$!.dispatch).toBeFalsy();
        });

        it('should do nothing when filter changed on handset', () => {
            // mock
            routerSelectors.isUserProfileRouteActive$ = cold('-t', { t: true });
            routerSelectors.url$ = cold('-u', { u: '/profile/1' });
            routerSelectors.userId$ = cold('-u', { u: 1 });
            jest.spyOn(filterSelectors, 'currentRouterParams$').mockReturnValue(
                cold('---f', {
                    f: {},
                }),
            );
            deviceService.isDesktop$ = cold('-f', { f: false });

            // call
            const effects = TestBed.inject(ProfileEffects);

            // asserts
            expect(effects.navigateWhenFilterChangedOnDesktop$).toSatisfyOnFlush(() => {
                expect(router.navigate).not.toHaveBeenCalled();
            });
        });
    });
});
