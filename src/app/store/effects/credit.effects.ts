import { of } from '@angular-devkit/core/node_modules/rxjs';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, map, switchMap } from 'rxjs/operators';
import { StoreCreditService } from 'src/app/shared/services/store-credit.service';
import * as CreditAction from '../actions/credit.actions';

@Injectable()
export class CreditEffects {
    constructor(private readonly actions$: Actions, private readonly storeCreditService: StoreCreditService) {}

    getCashHistory$ = createEffect(() =>
        this.actions$.pipe(
            ofType(CreditAction.getCashHistory),
            switchMap(payload =>
                this.storeCreditService.getCashCreditHistory(payload.page).pipe(
                    map(history => CreditAction.getCashHistorySuccess({ history })),
                    catchError(error => of(CreditAction.getCashHistoryError({ error }))),
                ),
            ),
        ),
    );
}
