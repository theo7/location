import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap, withLatestFrom } from 'rxjs/operators';
import { ProductsService } from 'src/app/shared/services/products.service';
import * as SuggestionActions from '../actions/suggestion.actions';
import { SuggestionSelectors } from './../services/suggestion-selectors.service';

@Injectable()
export class SuggestionEffects {
    constructor(
        private readonly actions$: Actions,
        private readonly productsService: ProductsService,
        private readonly suggestionSelectors: SuggestionSelectors,
    ) {}

    load$ = createEffect(() =>
        this.actions$.pipe(
            ofType(SuggestionActions.loadSuggestion),
            switchMap(() =>
                this.productsService.getSuggestions().pipe(
                    map(page => SuggestionActions.loadSuggestionSuccess({ page })),
                    catchError(error => of(SuggestionActions.loadSuggestionError({ error }))),
                ),
            ),
        ),
    );

    loadMore$ = createEffect(() =>
        this.actions$.pipe(
            ofType(SuggestionActions.loadMoreSuggestion),
            withLatestFrom(this.suggestionSelectors.page$),
            switchMap(([, pageIndex]) =>
                this.productsService.getSuggestions(pageIndex + 1).pipe(
                    map(page => SuggestionActions.loadMoreSuggestionSuccess({ page })),
                    catchError(error => of(SuggestionActions.loadMoreSuggestionError({ error }))),
                ),
            ),
        ),
    );
}
