import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType, ROOT_EFFECTS_INIT } from '@ngrx/effects';
import { forkJoin, of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import { ProductsService } from '../../shared/services/products.service';
import * as RepositoriesActions from '../actions/repositories.actions';

@Injectable()
export class RepositoriesEffects {
    constructor(private actions$: Actions, private productsService: ProductsService) {}

    init$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ROOT_EFFECTS_INIT),
            switchMap(() => [RepositoriesActions.getRepositories()]),
        ),
    );

    getCategories$ = createEffect(() =>
        this.actions$.pipe(
            ofType(RepositoriesActions.getRepositories),
            switchMap(() => {
                return forkJoin([
                    this.productsService.getBrands(),
                    this.productsService.getColors(),
                    this.productsService.getConditions(),
                    this.productsService.getDefaultBrand(),
                    this.productsService.getPopularBrands(),
                ]).pipe(
                    map(([brands, colors, conditions, defaultBrand, popularBrands]) => ({
                        brands,
                        colors,
                        conditions,
                        defaultBrand,
                        popularBrands,
                    })),
                    map(repositories => RepositoriesActions.getRepositoriesSuccess({ ...repositories })),
                    catchError(() => of(RepositoriesActions.getRepositoriesError())),
                );
            }),
        ),
    );
}
