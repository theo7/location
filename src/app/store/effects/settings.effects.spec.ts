import { TestBed } from '@angular/core/testing';
import { Actions } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { StorageMap } from '@ngx-pwa/local-storage';
import { cold, hot } from 'jest-marbles';
import { Observable } from 'rxjs';
import { TransportFee } from 'src/app/shared/models/models';
import { TransportService } from 'src/app/shared/services/transport.service';
import { UserService } from 'src/app/shared/services/user.service';
import * as SettingsActions from '../actions/settings.actions';
import { SettingsEffects } from './settings.effects';

describe('settings.effects', () => {
    let actions$: Observable<Actions>;
    let userService: UserService;
    let storage: StorageMap;
    let transportService: TransportService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [
                SettingsEffects,
                provideMockActions(() => actions$),
                {
                    provide: UserService,
                    useValue: {
                        getSettings: jest.fn(),
                        updateSettings: jest.fn(),
                    },
                },
                {
                    provide: TransportService,
                    useValue: {
                        getShippingFees: jest.fn(),
                    },
                },
                {
                    provide: StorageMap,
                    useValue: {
                        get: jest.fn(),
                        set: jest.fn(),
                    },
                },
            ],
        });

        userService = TestBed.inject(UserService);
        transportService = TestBed.inject(TransportService);
        storage = TestBed.inject(StorageMap);
    });

    describe('getSettings$', () => {
        const action = SettingsActions.getSettings();

        it('should dispatch getSettingsSuccess', () => {
            const globalSettings: SettingsActions.GlobalSettings = {
                acceptCommercialContact: false,
                acceptMailNotification: false,
            };
            const settings: SettingsActions.UserSettings = {
                ...globalSettings,
                acceptPushNotification: true,
            };

            const effects = TestBed.inject(SettingsEffects);

            jest.spyOn(userService, 'getSettings').mockReturnValue(cold('-(s|)', { s: globalSettings }));
            jest.spyOn(storage, 'get').mockReturnValue(cold('-(t|)', { t: true }));

            actions$ = hot('-a--', { a: action });
            const expected = SettingsActions.getSettingsSuccess({ settings });

            expect(effects.getSettings$).toBeObservable(cold('--e', { e: expected }));
        });

        it('should dispatch getSettingsFailed if cannot fetch global settings', () => {
            const error = 'Cannot fetch global settings...';

            const effects = TestBed.inject(SettingsEffects);

            jest.spyOn(userService, 'getSettings').mockReturnValue(cold('-#', undefined, error));
            jest.spyOn(storage, 'get').mockReturnValue(cold('-(t|)', { t: true }));

            actions$ = hot('-a--', { a: action });
            const expected = SettingsActions.getSettingsFailed({ error });

            expect(effects.getSettings$).toBeObservable(cold('--e', { e: expected }));
        });

        it('should dispatch getSettingsFailed if cannot get push info', () => {
            const error = 'Cannot retireve data from local storage';

            const globalSettings: SettingsActions.GlobalSettings = {
                acceptCommercialContact: false,
                acceptMailNotification: false,
            };

            const effects = TestBed.inject(SettingsEffects);

            jest.spyOn(userService, 'getSettings').mockReturnValue(cold('-(s|)', { s: globalSettings }));
            jest.spyOn(storage, 'get').mockReturnValue(cold('-#', undefined, error));

            actions$ = hot('-a--', { a: action });
            const expected = SettingsActions.getSettingsFailed({ error });

            expect(effects.getSettings$).toBeObservable(cold('--e', { e: expected }));
        });
    });

    describe('getShippingFees$', () => {
        const fee = {} as TransportFee;

        const action = SettingsActions.getShippingsFeesSettings();

        it('should dispatch getShippingsFeesSettingsSuccess', () => {
            const effects = TestBed.inject(SettingsEffects);

            jest.spyOn(transportService, 'getShippingFees').mockReturnValue(cold('-(u|)', { u: fee }));

            actions$ = hot('-a--', { a: action });
            const expected = SettingsActions.getShippingsFeesSettingsSuccess({ fee });

            expect(effects.getShippingFees$).toBeObservable(cold('--e', { e: expected }));
        });

        it('should dispatch getShippingsFeesSettingsError on error', () => {
            const error = 'Cannot get fees...';

            const effects = TestBed.inject(SettingsEffects);

            jest.spyOn(transportService, 'getShippingFees').mockReturnValue(cold('-#', undefined, error));

            actions$ = hot('-a--', { a: action });
            const expected = SettingsActions.getShippingsFeesSettingsError({ error });

            expect(effects.getShippingFees$).toBeObservable(cold('--e', { e: expected }));
        });
    });

    describe('setGlobalSettings$', () => {
        const settings: SettingsActions.GlobalSettings = {
            acceptCommercialContact: true,
            acceptMailNotification: true,
        };

        const action = SettingsActions.setGlobalSettings({
            settings,
        });

        it('should dispatch setGlobalSettingsSuccess', () => {
            const effects = TestBed.inject(SettingsEffects);

            jest.spyOn(userService, 'updateSettings').mockReturnValue(cold('-(u|)', { u: undefined }));

            actions$ = hot('-a--', { a: action });
            const expected = SettingsActions.setGlobalSettingsSuccess({ settings });

            expect(effects.setGlobalSettings$).toBeObservable(cold('--e', { e: expected }));
        });

        it('should dispatch setGlobalSettingsFailed on error', () => {
            const error = 'Cannot update settings...';

            const effects = TestBed.inject(SettingsEffects);

            jest.spyOn(userService, 'updateSettings').mockReturnValue(cold('-#', undefined, error));

            actions$ = hot('-a--', { a: action });
            const expected = SettingsActions.setGlobalSettingsFailed({ error });

            expect(effects.setGlobalSettings$).toBeObservable(cold('--e', { e: expected }));
        });
    });

    describe('setNotificationSetting$', () => {
        const value = true;

        const action = SettingsActions.setNotificationSetting({
            value,
        });

        it('should dispatch setNotificationSettingSuccess', () => {
            const effects = TestBed.inject(SettingsEffects);

            jest.spyOn(storage, 'set').mockReturnValue(cold('-(u|)', { u: undefined }));

            actions$ = hot('-a--', { a: action });
            const expected = SettingsActions.setNotificationSettingSuccess({ value });

            expect(effects.setNotificationSetting$).toBeObservable(cold('--e', { e: expected }));
        });

        it('should dispatch setNotificationSettingFailed on error', () => {
            const error = 'Cannot update local storage...';

            const effects = TestBed.inject(SettingsEffects);

            jest.spyOn(storage, 'set').mockReturnValue(cold('-#', undefined, error));

            actions$ = hot('-a--', { a: action });
            const expected = SettingsActions.setNotificationSettingFailed({ error });

            expect(effects.setNotificationSetting$).toBeObservable(cold('--e', { e: expected }));
        });
    });
});
