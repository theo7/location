import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { TranslateService } from '@ngx-translate/core';
import { Observable, of } from 'rxjs';
import { catchError, exhaustMap, map, switchMap, tap } from 'rxjs/operators';
import { ErrorNotificationDialogComponent } from '../../shared/components/error-notification-dialog/error-notification-dialog.component';
import { ChatService } from '../../shared/services/chat.service';
import { LotService } from '../../shared/services/lot.service';
import { ProductsService } from '../../shared/services/products.service';
import * as DiscussionsActions from '../actions/discussions.actions';

const DISCUSSION_ALREADY_REPORTED = 'DISCUSSION_037';

@Injectable()
export class DiscussionsEffects {
    constructor(
        private actions$: Actions,
        private chatService: ChatService,
        private productsService: ProductsService,
        private lotService: LotService,
        private router: Router,
        private dialog: MatDialog,
        private translateService: TranslateService,
        private readonly snackBar: MatSnackBar,
    ) {}

    getDiscussion$ = createEffect(() =>
        this.actions$.pipe(
            ofType(DiscussionsActions.getDiscussion),
            switchMap(action =>
                this.chatService.getDiscussion(action.id).pipe(
                    map(discussion => DiscussionsActions.getDiscussionSuccess({ discussion })),
                    catchError(() => of(DiscussionsActions.getDiscussionError())),
                ),
            ),
        ),
    );

    watchDiscussions$ = createEffect(() =>
        this.actions$.pipe(
            ofType(DiscussionsActions.getUserDiscussions),
            switchMap(() =>
                this.chatService.watchDiscussions().pipe(
                    map(discussions => DiscussionsActions.getUserDiscussionsSuccess({ discussions })),
                    catchError(error => of(DiscussionsActions.getUserDiscussionsError({ error }))),
                ),
            ),
        ),
    );

    startDiscussion$ = createEffect(() =>
        this.actions$.pipe(
            ofType(DiscussionsActions.startDiscussion),
            switchMap(action =>
                this.productsService.startDiscussion(action.productId).pipe(
                    map(discussion => DiscussionsActions.startDiscussionSuccess({ discussion })),
                    catchError(() => of(DiscussionsActions.startDiscussionError())),
                ),
            ),
        ),
    );

    startListDiscussion$ = createEffect(() =>
        this.actions$.pipe(
            ofType(DiscussionsActions.startListDiscussion),
            switchMap(action => this.productsService.startListDiscussion(action.productIds)),
            map(discussion => DiscussionsActions.startDiscussionSuccess({ discussion })),
            catchError(() => of(DiscussionsActions.startDiscussionError())),
        ),
    );

    removeProductFromLot$ = createEffect(() =>
        this.actions$.pipe(
            ofType(DiscussionsActions.removeProductFromLot),
            switchMap(action =>
                this.lotService.removeProductFromLot(action.lotId, action.productId).pipe(
                    map(lot => DiscussionsActions.removeProductFromLotSuccess({ lot })),
                    catchError(() => of(DiscussionsActions.removeProductFromLotError())),
                ),
            ),
        ),
    );

    addProductToLot$ = createEffect(() =>
        this.actions$.pipe(
            ofType(DiscussionsActions.addProductToLot),
            switchMap(action =>
                this.lotService.addProductToLot(action.lotId, action.productId).pipe(
                    map(lot => DiscussionsActions.addProductToLotSuccess({ lot })),
                    catchError(() => of(DiscussionsActions.addProductToLotError())),
                ),
            ),
        ),
    );

    setLotProducts = createEffect(() =>
        this.actions$.pipe(
            ofType(DiscussionsActions.setLotProducts),
            switchMap(action =>
                this.lotService.setProductList(action.lotId, action.products).pipe(
                    map(lot => DiscussionsActions.setLotProductsSuccess({ lot })),
                    catchError(() => of(DiscussionsActions.setLotProductsError())),
                ),
            ),
        ),
    );

    confirmSell$ = createEffect(() =>
        this.actions$.pipe(
            ofType(DiscussionsActions.confirmSell),
            switchMap(action =>
                this.lotService.approveSell(action.lotId, action.remunerationMode).pipe(
                    map(lot => DiscussionsActions.confirmSellSuccess({ lot })),
                    catchError(() => of(DiscussionsActions.confirmSellError({ lotId: action.lotId }))),
                ),
            ),
        ),
    );

    confirmProfessionalSell$ = createEffect(() =>
        this.actions$.pipe(
            ofType(DiscussionsActions.confirmProfessionalSell),
            switchMap(({ lotId, trackingNumber }) => {
                return this.lotService.approveProfessionalSell(lotId, trackingNumber).pipe(
                    map(lot => DiscussionsActions.confirmProfessionalSellSuccess({ lot })),
                    catchError(() => of(DiscussionsActions.confirmProfessionalSellError({ lotId }))),
                );
            }),
        ),
    );

    declineProfessionalSell$ = createEffect(() =>
        this.actions$.pipe(
            ofType(DiscussionsActions.declineProfessionalSell),
            switchMap(({ lotId }) => {
                return this.lotService.declineProfessionalSell(lotId).pipe(
                    map(lot => DiscussionsActions.declineProfessionalSellSuccess({ lot })),
                    catchError(() => of(DiscussionsActions.declineProfessionalSellError({ lotId }))),
                );
            }),
        ),
    );

    cancelSell$ = createEffect(() =>
        this.actions$.pipe(
            ofType(DiscussionsActions.cancelSell),
            switchMap(action =>
                this.lotService.cancelSell(action.lotId).pipe(
                    map(lot => DiscussionsActions.cancelSellSuccess({ lot })),
                    catchError(() => of(DiscussionsActions.cancelSellError({ lotId: action.lotId }))),
                ),
            ),
        ),
    );

    declineSell$ = createEffect(() =>
        this.actions$.pipe(
            ofType(DiscussionsActions.declineSell),
            switchMap(action =>
                this.lotService.declineSell(action.lotId).pipe(
                    map(lot => DiscussionsActions.declineSellSuccess({ lot })),
                    catchError(() => of(DiscussionsActions.declineSellError({ lotId: action.lotId }))),
                ),
            ),
        ),
    );

    cancelPackage$ = createEffect(() =>
        this.actions$.pipe(
            ofType(DiscussionsActions.cancelPackage),
            switchMap(action =>
                this.lotService.cancelPackage(action.lotId).pipe(
                    map(lot => DiscussionsActions.cancelPackageSuccess({ lot })),
                    catchError(() => of(DiscussionsActions.cancelPackageError({ lotId: action.lotId }))),
                ),
            ),
        ),
    );

    confirmPackageCompliant$ = createEffect(() =>
        this.actions$.pipe(
            ofType(DiscussionsActions.confirmPackageCompliant),
            exhaustMap(action =>
                this.lotService.confirmPackageCompliant(action.lotId).pipe(
                    map(lot => DiscussionsActions.confirmPackageCompliantSuccess({ lot })),
                    catchError(() => of(DiscussionsActions.confirmPackageCompliantError({ lotId: action.lotId }))),
                ),
            ),
        ),
    );

    confirmPackageNonCompliant$ = createEffect(() =>
        this.actions$.pipe(
            ofType(DiscussionsActions.confirmPackageNonCompliant),
            exhaustMap(action =>
                this.lotService.confirmPackageNonCompliant(action.lotId).pipe(
                    map(lot => DiscussionsActions.confirmPackageNonCompliantSuccess({ lot })),
                    catchError(() => of(DiscussionsActions.confirmPackageNonCompliantError({ lotId: action.lotId }))),
                ),
            ),
        ),
    );

    sendMessage$ = createEffect(() =>
        this.actions$.pipe(
            ofType(DiscussionsActions.sendMessage),
            switchMap(action =>
                this.chatService.sendMessage(action.message).pipe(
                    map(discussion => DiscussionsActions.sendMessageSuccess({ discussion })),
                    catchError(() => of(DiscussionsActions.sendMessageError())),
                ),
            ),
        ),
    );

    archiveDiscussion$ = createEffect(() =>
        this.actions$.pipe(
            ofType(DiscussionsActions.archiveDiscussion),
            switchMap(action =>
                this.chatService.archiveDiscussion(action.discussionId).pipe(
                    map(() => DiscussionsActions.archiveDiscussionSuccess({ discussionId: action.discussionId })),
                    catchError(error => of(DiscussionsActions.archiveDiscussionError({ error }))),
                ),
            ),
        ),
    );

    archiveDiscussionSuccess$ = createEffect(
        () =>
            this.actions$.pipe(
                ofType(DiscussionsActions.archiveDiscussionSuccess),
                switchMap(() => this.router.navigate(['chat'])),
            ),
        { dispatch: false },
    );

    readDiscussion$ = createEffect(() =>
        this.actions$.pipe(
            ofType(DiscussionsActions.readDiscussion),
            switchMap(payload => {
                const observable$: Observable<unknown> =
                    typeof payload.roomId === 'number'
                        ? this.chatService.setDiscussionRead(payload.roomId, payload.isCurrentBuyer)
                        : this.chatService.setKiabiDiscussionRead();
                return observable$.pipe(
                    map(() => DiscussionsActions.readDiscussionSuccess({ roomId: payload.roomId })),
                    catchError(error => of(DiscussionsActions.readDiscussionError({ error }))),
                );
            }),
        ),
    );

    archiveTemporaryDiscussion$ = createEffect(() =>
        this.actions$.pipe(
            ofType(DiscussionsActions.archiveTemporaryDiscussion),
            switchMap(action =>
                this.chatService.archiveDiscussion(action.discussionId).pipe(
                    map(() =>
                        DiscussionsActions.archiveTemporaryDiscussionSuccess({ discussionId: action.discussionId }),
                    ),
                    catchError(error => of(DiscussionsActions.archiveTemporaryDiscussionError({ error }))),
                ),
            ),
        ),
    );

    archiveTemporaryDiscussionSuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(DiscussionsActions.archiveTemporaryDiscussionSuccess),
            map(() => DiscussionsActions.setInstantBuy({ lotId: null })),
        ),
    );

    reportUser$ = createEffect(() =>
        this.actions$.pipe(
            ofType(DiscussionsActions.reportUser),
            switchMap(action =>
                this.chatService.reportUser(action.discussionId, action.newDiscussionReport).pipe(
                    map(() => DiscussionsActions.reportUserSuccess()),
                    catchError(error => of(DiscussionsActions.reportUserError({ error }))),
                ),
            ),
        ),
    );

    reportUserSuccess$ = createEffect(
        () =>
            this.actions$.pipe(
                ofType(DiscussionsActions.reportUserSuccess),
                switchMap(() => this.translateService.get('dialog.actions.types.reportUser.success')),
                tap(message => {
                    this.snackBar.open(message);
                }),
            ),
        { dispatch: false },
    );

    reportUserError$ = createEffect(
        () =>
            this.actions$.pipe(
                ofType(DiscussionsActions.reportUserError),
                tap(({ error }) => {
                    const message = error.error?.message;

                    const isDiscussionAlreadyReported = message === DISCUSSION_ALREADY_REPORTED;

                    const dialogTitleProp = isDiscussionAlreadyReported
                        ? 'dialog.actions.types.errorReportUser.alreadyReported.title'
                        : 'dialog.actions.types.errorReportUser.title';
                    const dialogContentProp = isDiscussionAlreadyReported
                        ? 'dialog.actions.types.errorReportUser.alreadyReported.content'
                        : 'dialog.actions.types.errorReportUser.content';

                    this.dialog.open(ErrorNotificationDialogComponent, {
                        panelClass: 'no-padding-dialog',
                        width: '280px',
                        data: {
                            title: this.translateService.instant(dialogTitleProp),
                            content: this.translateService.instant(dialogContentProp),
                            buttonContent: this.translateService.instant('common.ok'),
                        },
                    });
                }),
            ),
        { dispatch: false },
    );
}
