import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { TranslateService } from '@ngx-translate/core';
import { EMPTY, of, throwError } from 'rxjs';
import { catchError, exhaustMap, filter, first, map, mergeMap, switchMap, tap } from 'rxjs/operators';
import { GoogleAnalyticsService } from 'src/app/features/analytics/google-analytics.service';
import { OfflineNotificationService } from 'src/app/features/offline/offline-notification.service';
import { ConfirmCancelPackageDialogComponent } from 'src/app/shared/components/confirm-cancel-package-dialog/confirm-cancel-package-dialog.component';
import { ConfirmCancelSellDialogComponent } from 'src/app/shared/components/confirm-cancel-sell-dialog/confirm-cancel-sell-dialog.component';
import { ConfirmDeclineSellDialogComponent } from 'src/app/shared/components/confirm-decline-sell-dialog/confirm-decline-sell-dialog.component';
import { GoToLoginDialogComponent } from 'src/app/shared/components/go-to-login-dialog/go-to-login-dialog.component';
import {
    ConfirmArchiveDiscussionDialogResult,
    ConfirmBlockUserDialogResult,
    ConfirmCancelPackageDialogResult,
    ConfirmCancelSellDialogResult,
    ConfirmDeclineSellDialogResult,
    LotStatusEnum,
    MoreActionsOnLotModalResult,
    ReportUserDialogResult,
} from 'src/app/shared/models/models';
import { DeviceService } from 'src/app/shared/services/device.service';
import { LotService } from 'src/app/shared/services/lot.service';
import { ProductsService } from 'src/app/shared/services/products.service';
import { moreActionsOnLotDialogComponent$, paymentProcessDialogComponent$ } from '../../imports.dynamic';
import { ConfirmArchiveDiscussionDialogComponent } from '../../shared/components/confirm-archive-discussion-dialog/confirm-archive-discussion-dialog.component';
import { ConfirmBlockUserDialogComponent } from '../../shared/components/confirm-block-user-dialog/confirm-block-user-dialog.component';
import { ConfirmUnblockUserDialogComponent } from '../../shared/components/confirm-unblock-user-dialog/confirm-unblock-user-dialog.component';
import { ReportUserDialogComponent } from '../../shared/components/report-user-dialog/report-user-dialog.component';
import * as DiscussionsActions from '../actions/discussions.actions';
import * as LotActions from '../actions/lot.actions';
import * as UserActions from '../actions/user.actions';
import { ConnectionSelectors } from '../services/connection-selectors.service';
import { DiscussionsDispatchers } from '../services/discussions-dispatchers.service';
import { DiscussionsSelectors } from '../services/discussions-selectors.service';
import { UserProfileSelectors } from '../services/user-selectors.service';

@Injectable()
export class LotEffects {
    constructor(
        private readonly deviceService: DeviceService,
        private readonly actions$: Actions,
        private readonly dialog: MatDialog,
        private readonly snackBar: MatSnackBar,
        private readonly connectionSelectors: ConnectionSelectors,
        private readonly offlineNotificationService: OfflineNotificationService,
        private readonly productsService: ProductsService,
        private readonly lotService: LotService,
        private readonly device: DeviceService,
        private readonly userProfileSelectors: UserProfileSelectors,
        private readonly router: Router,
        private readonly discussionsSelectors: DiscussionsSelectors,
        private readonly discussionsDispatchers: DiscussionsDispatchers,
        private readonly gaService: GoogleAnalyticsService,
        private readonly translate: TranslateService,
    ) {}

    openMoreActionsModal$ = createEffect(() =>
        this.actions$.pipe(
            ofType(LotActions.openMoreActionsModal),
            exhaustMap(({ discussion, userId }) => {
                const dialogDeviceParams = device =>
                    ({
                        handset: {
                            width: '100vw',
                            maxWidth: '100vw',
                            height: '100vh',
                            panelClass: 'full-screen-dialog',
                        },
                        desktop: {
                            height: '60vh',
                            width: '600px',
                            maxWidth: '600px',
                            autoFocus: false,
                            disableClose: true,
                            panelClass: 'no-padding-dialog',
                        },
                    }[device]);

                return moreActionsOnLotDialogComponent$.pipe(
                    switchMap(MoreActionsOnLotDialogComponent => {
                        return this.dialog
                            .open(MoreActionsOnLotDialogComponent, {
                                data: {
                                    discussion,
                                    userId,
                                },
                                ...dialogDeviceParams(this.deviceService.mode),
                            })
                            .afterClosed();
                    }),
                );
            }),
            switchMap((result: MoreActionsOnLotModalResult | undefined) => {
                if (result?.type === 'CANCEL_SELL') {
                    return of(LotActions.openConfirmCancelSellModal({ lotId: result.lotId }));
                }
                if (result?.type === 'DECLINE_SELL') {
                    return of(LotActions.openConfirmDeclineSellModal({ lotId: result.lotId }));
                }
                if (result?.type === 'CANCEL_PACKAGE') {
                    return of(LotActions.openConfirmCancelPackageModal({ lotId: result.lotId }));
                }
                if (result?.type === 'BLOCK_USER') {
                    return of(LotActions.openConfirmBlockUserModal({ userId: result.userId }));
                }
                if (result?.type === 'UNBLOCK_USER') {
                    return of(LotActions.openConfirmUnblockUserModal({ userId: result.userId }));
                }
                if (result?.type === 'ARCHIVE_DISCUSSION') {
                    return of(LotActions.openConfirmArchiveDiscussionModal({ discussionId: result.discussionId }));
                }
                if (result?.type === 'REPORT_USER') {
                    return of(LotActions.openReportUserModal({ discussion: result.discussion }));
                }
                return EMPTY;
            }),
        ),
    );

    openConfirmCancelSellModal$ = createEffect(() =>
        this.actions$.pipe(
            ofType(LotActions.openConfirmCancelSellModal),
            exhaustMap(({ lotId }) => {
                const dialogRef = this.dialog.open(ConfirmCancelSellDialogComponent, {
                    width: '380px',
                    data: {
                        lotId,
                    },
                });
                return dialogRef.afterClosed();
            }),
            switchMap((result: ConfirmCancelSellDialogResult | undefined) => {
                if (result) {
                    return of(LotActions.confirmCancelSellSuccess({ lotId: result.lotId }));
                }

                return EMPTY;
            }),
        ),
    );

    confirmCancelSellSuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(LotActions.confirmCancelSellSuccess),
            map(({ lotId }) => DiscussionsActions.cancelSell({ lotId })),
        ),
    );

    openConfirmDeclineSellModal$ = createEffect(() =>
        this.actions$.pipe(
            ofType(LotActions.openConfirmDeclineSellModal),
            exhaustMap(({ lotId }) => {
                const dialogRef = this.dialog.open(ConfirmDeclineSellDialogComponent, {
                    width: '380px',
                    data: {
                        lotId,
                    },
                });
                return dialogRef.afterClosed();
            }),
            switchMap((result: ConfirmDeclineSellDialogResult | undefined) => {
                if (result) {
                    return of(LotActions.confirmDeclineSellSuccess({ lotId: result.lotId }));
                }

                return EMPTY;
            }),
        ),
    );

    confirmDeclineSellSuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(LotActions.confirmDeclineSellSuccess),
            map(({ lotId }) => DiscussionsActions.declineSell({ lotId })),
        ),
    );

    openConfirmCancelPackageModal$ = createEffect(() =>
        this.actions$.pipe(
            ofType(LotActions.openConfirmCancelPackageModal),
            exhaustMap(({ lotId }) => {
                const dialogRef = this.dialog.open(ConfirmCancelPackageDialogComponent, {
                    width: '380px',
                    data: {
                        lotId,
                    },
                });
                return dialogRef.afterClosed();
            }),
            switchMap((result: ConfirmCancelPackageDialogResult | undefined) => {
                if (result) {
                    return of(LotActions.confirmCancelPackageSuccess({ lotId: result.lotId }));
                }

                return EMPTY;
            }),
        ),
    );

    confirmCancelPackageSuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(LotActions.confirmCancelPackageSuccess),
            map(({ lotId }) => DiscussionsActions.cancelPackage({ lotId })),
        ),
    );

    openConfirmBlockUserModal$ = createEffect(() =>
        this.actions$.pipe(
            ofType(LotActions.openConfirmBlockUserModal),
            exhaustMap(({ userId }) => {
                const dialogRef = this.dialog.open(ConfirmBlockUserDialogComponent, {
                    width: '380px',
                    data: {
                        userId,
                    },
                });
                return dialogRef.afterClosed();
            }),
            switchMap((result: ConfirmBlockUserDialogResult | undefined) => {
                if (result) {
                    return of(LotActions.confirmBlockUserSuccess({ userId: result.userId }));
                }

                return EMPTY;
            }),
        ),
    );

    confirmBlockUserSuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(LotActions.confirmBlockUserSuccess),
            map(({ userId }) => {
                return UserActions.blockUser({ userId });
            }),
        ),
    );

    openConfirmUnblockUserModal$ = createEffect(() =>
        this.actions$.pipe(
            ofType(LotActions.openConfirmUnblockUserModal),
            exhaustMap(({ userId }) => {
                const dialogRef = this.dialog.open(ConfirmUnblockUserDialogComponent, {
                    width: '380px',
                    data: {
                        userId,
                    },
                });
                return dialogRef.afterClosed();
            }),
            switchMap((result: ConfirmBlockUserDialogResult | undefined) => {
                if (result) {
                    return of(LotActions.confirmUnblockUserSuccess({ userId: result.userId }));
                }

                return EMPTY;
            }),
        ),
    );

    confirmUnblockUserSuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(LotActions.confirmUnblockUserSuccess),
            map(({ userId }) => UserActions.unblockUser({ userId })),
        ),
    );

    openConfirmArchiveDiscussionModal$ = createEffect(() =>
        this.actions$.pipe(
            ofType(LotActions.openConfirmArchiveDiscussionModal),
            exhaustMap(({ discussionId }) => {
                const dialogRef = this.dialog.open(ConfirmArchiveDiscussionDialogComponent, {
                    width: '380px',
                    data: {
                        discussionId,
                    },
                });
                return dialogRef.afterClosed();
            }),
            switchMap((result: ConfirmArchiveDiscussionDialogResult | undefined) => {
                if (result) {
                    return of(LotActions.confirmArchiveDiscussionSuccess({ discussionId: result.discussionId }));
                }

                return EMPTY;
            }),
        ),
    );

    confirmArchiveDiscussionSuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(LotActions.confirmArchiveDiscussionSuccess),
            map(({ discussionId }) => DiscussionsActions.archiveDiscussion({ discussionId })),
        ),
    );

    startDiscussion$ = createEffect(
        () =>
            this.actions$.pipe(
                ofType(LotActions.startDiscussion),
                exhaustMap(({ productId }) =>
                    this.connectionSelectors.isOffline$.pipe(
                        first(),
                        switchMap(isOffline => {
                            if (isOffline) {
                                this.offlineNotificationService.displayNotification();
                                return EMPTY;
                            }

                            return of(isOffline);
                        }),
                        switchMap(() => this.userProfileSelectors.isLogged$.pipe(first())),
                        switchMap(isLogged => {
                            if (!isLogged) {
                                this.dialog.open(GoToLoginDialogComponent, { data: { redirectUrl: this.router.url } });
                                return EMPTY;
                            }

                            return of(isLogged);
                        }),
                        switchMap(() => this.discussionsSelectors.discussionByProductId$(productId)),
                        tap(
                            discussion =>
                                (!discussion || discussion.lot?.status === LotStatusEnum.CANCELLED) &&
                                this.discussionsDispatchers.startDiscussion(productId),
                        ),
                        first(discussion => !!discussion && discussion.lot?.status !== LotStatusEnum.CANCELLED),
                    ),
                ),
                switchMap(discussion => {
                    if (!discussion) {
                        return throwError('Discussion required');
                    }

                    if (discussion.lot?.products && discussion.seller?.id) {
                        this.gaService.trackSendMessage(discussion.lot.products[0], discussion.seller.id);
                    }

                    return this.router.navigate(['chat', discussion.id]);
                }),
            ),
        { dispatch: false },
    );

    openReportUserModal$ = createEffect(() =>
        this.actions$.pipe(
            ofType(LotActions.openReportUserModal),
            exhaustMap(({ discussion }) => {
                const dialogDeviceParams = device =>
                    ({
                        handset: {
                            width: '100vw',
                            maxWidth: '100vw',
                            height: '100vh',
                            panelClass: 'full-screen-dialog',
                        },
                        desktop: {
                            height: '80vh',
                            width: '600px',
                            maxWidth: '600px',
                            disableClose: true,
                            panelClass: 'no-padding-dialog',
                        },
                    }[device]);

                const dialogRef = this.dialog.open(ReportUserDialogComponent, {
                    ...dialogDeviceParams(this.device.mode),
                    data: {
                        discussion,
                    },
                });
                return dialogRef.afterClosed();
            }),
            switchMap((result: ReportUserDialogResult | undefined) => {
                if (result) {
                    return of(
                        LotActions.confirmReportUserSuccess({
                            discussionId: result.discussionId,
                            newDiscussionReport: result.newDiscussionReport,
                        }),
                    );
                }

                return EMPTY;
            }),
        ),
    );

    confirmReportUserSuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(LotActions.confirmReportUserSuccess),
            map(({ discussionId, newDiscussionReport }) =>
                DiscussionsActions.reportUser({ discussionId, newDiscussionReport }),
            ),
        ),
    );

    startPaymentProcess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(LotActions.startPaymentProcess),
            exhaustMap(({ productId }) =>
                this.connectionSelectors.isOffline$.pipe(
                    first(),
                    switchMap(isOffline => {
                        if (isOffline) {
                            this.offlineNotificationService.displayNotification();
                            return EMPTY;
                        }

                        return of(isOffline);
                    }),
                    switchMap(() => this.userProfileSelectors.isLogged$.pipe(first())),
                    switchMap(isLogged => {
                        if (!isLogged) {
                            this.dialog.open(GoToLoginDialogComponent, { data: { redirectUrl: this.router.url } });
                            return EMPTY;
                        }

                        return of(isLogged);
                    }),
                    switchMap(() => this.productsService.startListDiscussion([productId])),
                    tap(lot => {
                        this.discussionsDispatchers.setInstantBuy(lot.id as number);
                    }),
                    switchMap(discussion => {
                        if (discussion.lot?.id) {
                            return this.lotService.getCart(discussion.lot.id).pipe(map(lot => [lot, discussion]));
                        }

                        return EMPTY;
                    }),
                    switchMap(([lot, discussion]) => {
                        const dialogDeviceParamsCart = device =>
                            ({
                                handset: {
                                    width: '100vw',
                                    maxWidth: '100vw',
                                    height: '100vh',
                                    panelClass: 'full-screen-dialog',
                                },
                                desktop: {
                                    width: '90vw',
                                    maxWidth: '700px',
                                    panelClass: 'no-padding-dialog',
                                    disableClose: true,
                                    minHeight: '50vh',
                                },
                            }[device]);

                        return paymentProcessDialogComponent$.pipe(
                            mergeMap(PaymentProcessDialogComponent => {
                                return this.dialog
                                    .open(PaymentProcessDialogComponent, {
                                        ...dialogDeviceParamsCart(this.device.mode),
                                        data: {
                                            lot,
                                        },
                                    })
                                    .afterClosed()
                                    .pipe(
                                        filter(e => !e),
                                        map(() =>
                                            DiscussionsActions.archiveTemporaryDiscussion({
                                                discussionId: discussion.id as number,
                                            }),
                                        ),
                                    );
                            }),
                        );
                    }),
                ),
            ),
        ),
    );

    createLot$ = createEffect(() =>
        this.actions$.pipe(
            ofType(LotActions.createLot),
            exhaustMap(({ product }) =>
                this.connectionSelectors.isOffline$.pipe(
                    first(),
                    switchMap(isOffline => {
                        if (isOffline) {
                            this.offlineNotificationService.displayNotification();
                            return EMPTY;
                        }

                        return of(isOffline);
                    }),
                    switchMap(() => this.userProfileSelectors.isLogged$.pipe(first())),
                    switchMap(isLogged => {
                        if (!isLogged) {
                            this.dialog.open(GoToLoginDialogComponent, { data: { redirectUrl: this.router.url } });
                            return EMPTY;
                        }

                        return of(isLogged);
                    }),
                    switchMap(() => {
                        return this.productsService.startListDiscussion([product.id]);
                    }),
                ),
            ),
            switchMap(() => of(LotActions.createLotSuccess())),
        ),
    );

    changeRemunerationMode$ = createEffect(() =>
        this.actions$.pipe(
            ofType(LotActions.changeRemunerationMode),
            switchMap(payload => {
                return this.lotService.changeRemunerationMode(payload.lotId, payload.remunerationMode).pipe(
                    map(lot => LotActions.changeRemunerationModeSuccess({ lot })),
                    catchError(error => of(LotActions.changeRemunerationModeError({ error }))),
                );
            }),
        ),
    );

    changeRemunerationModeSuccess$ = createEffect(
        () =>
            this.actions$.pipe(
                ofType(LotActions.changeRemunerationModeSuccess),
                switchMap(() => this.translate.get('orders.detail.changeRemunerationMode.success')),
                tap(message => {
                    this.snackBar.open(message, undefined, {
                        horizontalPosition: 'end',
                        verticalPosition: 'top',
                        duration: 1500,
                    });
                }),
            ),
        { dispatch: false },
    );
}
