import { TestBed } from '@angular/core/testing';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Actions, EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { TranslateService } from '@ngx-translate/core';
import { cold, hot } from 'jest-marbles';
import { Observable } from 'rxjs';
import { ContactService } from 'src/app/shared/services/contact.service';
import * as ContactActions from '../actions/contact.actions';
import { ContactSupportInfo } from './../../shared/models/models';
import { ContactEffects } from './contact.effects';

describe('contact.effects', () => {
    let actions$: Observable<Actions>;
    let contactService: ContactService;
    let contactEffects: ContactEffects;
    let translateService: TranslateService;
    let snackBar: MatSnackBar;
    let dialog: MatDialog;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [
                ContactEffects,
                provideMockActions(() => actions$),
                {
                    provide: ContactService,
                    useValue: {
                        sendEmailContactSupport: jest.fn(),
                        getContactSupportThemes: jest.fn(),
                        getContactSupportSubjects: jest.fn(),
                    },
                },
                {
                    provide: TranslateService,
                    useValue: {
                        get: jest.fn(),
                        instant: jest.fn(),
                    },
                },
                {
                    provide: MatSnackBar,
                    useValue: {
                        open: jest.fn(),
                    },
                },
                {
                    provide: MatDialog,
                    useValue: {
                        open: jest.fn(),
                    },
                },
            ],
        });
        contactEffects = TestBed.inject(ContactEffects);
        contactService = TestBed.inject(ContactService);
        translateService = TestBed.inject(TranslateService);
        snackBar = TestBed.inject(MatSnackBar);
        dialog = TestBed.inject(MatDialog);
    });

    beforeEach(() => {
        jest.spyOn(translateService, 'get').mockReturnValue(cold('-t-', { t: '' }));
    });

    describe('sendEmailContactSupport$', () => {
        const contactSupportInfo = {} as ContactSupportInfo;
        const action = ContactActions.sendEmailContactSupport({ contactSupportInfo });

        it('should dispatch sendEmailContactSupport action', () => {
            const effects = TestBed.inject(ContactEffects);
            jest.spyOn(contactService, 'sendEmailContactSupport').mockReturnValue(cold('-(u|)', { u: undefined }));
            const completion = ContactActions.sendEmailContactSupportSuccess();

            actions$ = hot('-a-', { a: action });
            const expected = cold('--b', { b: completion });
            expect(effects.sendEmailContactSupport$).toBeObservable(expected);
        });

        it('should dispatch sendEmailContactError action', () => {
            const effects = TestBed.inject(ContactEffects);
            const error = 'Cannot send email to support';
            jest.spyOn(contactService, 'sendEmailContactSupport').mockReturnValue(cold('-#', undefined, error));
            const completion = ContactActions.sendEmailContactSupportError({ error });

            actions$ = hot('-a-', { a: action });
            const expected = cold('--b', { b: completion });
            expect(effects.sendEmailContactSupport$).toBeObservable(expected);
        });
    });

    describe('sendEmailContactSupportError$', () => {
        const error = 'Error...';
        const action = ContactActions.sendEmailContactSupportError({ error });

        it('should not dispatch actions', () => {
            const metadata: EffectsMetadata<ContactEffects> = getEffectsMetadata(contactEffects);
            expect(metadata.sendEmailContactSupportError$!.dispatch).toBeFalsy();
        });

        it('should open snackbar', () => {
            const effects = TestBed.inject(ContactEffects);
            actions$ = hot('--a', { a: action });

            expect(effects.sendEmailContactSupportError$).toSatisfyOnFlush(() => {
                expect(snackBar.open).toHaveBeenCalled();
            });
        });
    });
});
