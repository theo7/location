import { TestBed } from '@angular/core/testing';
import { Actions } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { cold, hot } from 'jest-marbles';
import { Observable } from 'rxjs';
import { Page, UserRating, UserRatingSummary } from 'src/app/shared/models/models';
import { UserService } from 'src/app/shared/services/user.service';
import * as RatingsActions from '../actions/ratings.actions';
import { RatingsSelectors } from '../services/ratings-selectors.service';
import { RouterSelectors } from '../services/router-selectors.service';
import { RatingsEffects } from './ratings.effects';

const ratingsPages = {
    content: [] as UserRating[],
    totalElements: 14,
} as Page<UserRating>;

const summary = {
    totalRating: 36,
    averageRating: 3.6,
    detailedRating: new Map([
        [1, 0],
        [2, 0],
        [3, 1],
        [4, 1],
        [5, 2],
    ]),
} as UserRatingSummary;

describe('ratings.effects', () => {
    let actions$: Observable<Actions>;
    let userService: UserService;
    let ratingsSelectors: RatingsSelectors;
    let routerSelectors: RouterSelectors;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [
                RatingsEffects,
                provideMockActions(() => actions$),
                {
                    provide: UserService,
                    useValue: {
                        findRatingSummary: jest.fn(),
                        findRating: jest.fn(),
                    },
                },
                {
                    provide: RatingsSelectors,
                    useValue: {},
                },
                {
                    provide: RouterSelectors,
                    useValue: {},
                },
            ],
        });

        userService = TestBed.inject(UserService);
        ratingsSelectors = TestBed.inject(RatingsSelectors);
        routerSelectors = TestBed.inject(RouterSelectors);
    });

    describe('load$', () => {
        const action = RatingsActions.load();

        it('should load ratings successfully', () => {
            actions$ = hot('-a-', { a: action });

            routerSelectors.userId$ = cold('u-', { u: 14 });

            jest.spyOn(userService, 'findRatingSummary').mockReturnValue(cold('-(s|)', { s: summary }));
            jest.spyOn(userService, 'findRating').mockReturnValue(cold('-(r|)', { r: ratingsPages }));

            const effects = TestBed.inject(RatingsEffects);

            const loadSuccess = RatingsActions.loadSuccess({ summary, ratingsPages });

            const expected = cold('--s', { s: loadSuccess });
            expect(effects.load$).toBeObservable(expected);
        });

        it('should failed to load ratings', () => {
            const error = 'Cannot load ratings...';

            actions$ = hot('-a-', { a: action });

            routerSelectors.userId$ = cold('u-', { u: 14 });

            jest.spyOn(userService, 'findRatingSummary').mockReturnValue(cold('-(s|)', { s: summary }));
            jest.spyOn(userService, 'findRating').mockReturnValue(cold('-#', undefined, error));

            const effects = TestBed.inject(RatingsEffects);

            const loadError = RatingsActions.loadError({ error });

            const expected = cold('--e', { e: loadError });
            expect(effects.load$).toBeObservable(expected);
        });
    });

    describe('loadMore$', () => {
        const action = RatingsActions.loadMore();

        it('should load more of ratings successfully', () => {
            actions$ = hot('-a--', { a: action });

            routerSelectors.userId$ = cold('u-', { u: 14 });
            ratingsSelectors.page$ = cold('p-', { p: 0 });

            jest.spyOn(userService, 'findRating').mockReturnValue(cold('-(r|)', { r: ratingsPages }));

            const effects = TestBed.inject(RatingsEffects);

            const loadMoreSuccess = RatingsActions.loadMoreSuccess({ ratingsPages });

            const expected = cold('--s', { s: loadMoreSuccess });
            expect(effects.loadMore$).toBeObservable(expected);
        });

        it('should failed to load more of ratings', () => {
            const error = 'Cannot load more of ratings...';

            actions$ = hot('-a--', { a: action });

            routerSelectors.userId$ = cold('u-', { u: 14 });
            ratingsSelectors.page$ = cold('p-', { p: 0 });

            jest.spyOn(userService, 'findRating').mockReturnValue(cold('-#', undefined, error));

            const effects = TestBed.inject(RatingsEffects);

            const loadMoreError = RatingsActions.loadMoreError({ error });

            const expected = cold('--e', { e: loadMoreError });
            expect(effects.loadMore$).toBeObservable(expected);
        });
    });
});
