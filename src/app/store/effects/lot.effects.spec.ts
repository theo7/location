import { TestBed } from '@angular/core/testing';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Actions, EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { TranslateService } from '@ngx-translate/core';
import { cold, hot } from 'jest-marbles';
import { Observable, of } from 'rxjs';
import { MoreActionsOnLotDialogComponent } from 'src/app/dialogs/more-actions-on-lot-dialog/more-actions-on-lot-dialog.component';
import { GoogleAnalyticsService } from 'src/app/features/analytics/google-analytics.service';
import { OfflineNotificationService } from 'src/app/features/offline/offline-notification.service';
import { ConfirmCancelPackageDialogComponent } from 'src/app/shared/components/confirm-cancel-package-dialog/confirm-cancel-package-dialog.component';
import { ConfirmCancelSellDialogComponent } from 'src/app/shared/components/confirm-cancel-sell-dialog/confirm-cancel-sell-dialog.component';
import { ConfirmDeclineSellDialogComponent } from 'src/app/shared/components/confirm-decline-sell-dialog/confirm-decline-sell-dialog.component';
import { GoToLoginDialogComponent } from 'src/app/shared/components/go-to-login-dialog/go-to-login-dialog.component';
import { PaymentProcessDialogComponent } from 'src/app/shared/components/payment-process-dialog/payment-process-dialog.component';
import {
    ConfirmCancelPackageDialogResult,
    ConfirmCancelSellDialogResult,
    ConfirmDeclineSellDialogResult,
    Discussion,
    LotStatusEnum,
    MoreActionsOnLotModalResult,
    Product,
} from 'src/app/shared/models/models';
import { DeviceService } from 'src/app/shared/services/device.service';
import { LotService } from 'src/app/shared/services/lot.service';
import { ProductsService } from 'src/app/shared/services/products.service';
import * as DiscussionsActions from '../actions/discussions.actions';
import * as LotActions from '../actions/lot.actions';
import * as UserActions from '../actions/user.actions';
import { ConnectionSelectors } from '../services/connection-selectors.service';
import { DiscussionsDispatchers } from '../services/discussions-dispatchers.service';
import { DiscussionsSelectors } from '../services/discussions-selectors.service';
import { UserProfileSelectors } from '../services/user-selectors.service';
import { LotEffects } from './lot.effects';

jest.mock('src/app/shared/components/payment-process-dialog/payment-process-dialog.component', () => ({
    get PaymentProcessDialogComponent() {
        return {};
    },
}));

jest.mock('src/app/dialogs/more-actions-on-lot-dialog/more-actions-on-lot-dialog.component', () => ({
    get MoreActionsOnLotDialogComponent() {
        return {};
    },
}));

jest.mock('../../imports.dynamic', () => ({
    get paymentProcessDialogComponent$() {
        return of(PaymentProcessDialogComponent);
    },
    get moreActionsOnLotDialogComponent$() {
        return of(MoreActionsOnLotDialogComponent);
    },
}));

describe('lot.effects', () => {
    let actions$: Observable<Actions>;
    let effects: LotEffects;
    let snackBarService: MatSnackBar;
    let dialog: MatDialog;
    let connectionSelectors: ConnectionSelectors;
    let offlineNotificationService: OfflineNotificationService;
    let productsService: ProductsService;
    let lotService: LotService;
    let userProfileSelectors: UserProfileSelectors;
    let router: Router;
    let discussionsSelectors: DiscussionsSelectors;
    let gaService: GoogleAnalyticsService;
    let translateService: TranslateService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [
                LotEffects,
                provideMockActions(() => actions$),
                {
                    provide: MatDialog,
                    useValue: {
                        open: jest.fn(),
                    },
                },
                {
                    provide: MatSnackBar,
                    useValue: {
                        open: jest.fn(),
                    },
                },
                {
                    provide: ConnectionSelectors,
                    useValue: {},
                },
                {
                    provide: OfflineNotificationService,
                    useValue: {
                        displayNotification: jest.fn(),
                    },
                },
                {
                    provide: ProductsService,
                    useValue: {
                        startListDiscussion: jest.fn(),
                    },
                },
                {
                    provide: LotService,
                    useValue: {
                        getCart: jest.fn(),
                    },
                },
                {
                    provide: DeviceService,
                    useValue: {
                        mode: 'handset',
                    },
                },
                {
                    provide: UserProfileSelectors,
                    useValue: {},
                },
                {
                    provide: Router,
                    useValue: {
                        navigate: jest.fn(),
                    },
                },
                {
                    provide: DiscussionsSelectors,
                    useValue: {
                        discussionByProductId$: jest.fn(),
                    },
                },
                {
                    provide: DiscussionsDispatchers,
                    useValue: {
                        setInstantBuy: jest.fn(),
                    },
                },
                {
                    provide: GoogleAnalyticsService,
                    useValue: {
                        trackSendMessage: jest.fn(),
                    },
                },
                {
                    provide: TranslateService,
                    useValue: {
                        get: jest.fn(),
                    },
                },
            ],
        });

        effects = TestBed.inject(LotEffects);
        dialog = TestBed.inject(MatDialog);
        snackBarService = TestBed.inject(MatSnackBar);
        connectionSelectors = TestBed.inject(ConnectionSelectors);
        offlineNotificationService = TestBed.inject(OfflineNotificationService);
        productsService = TestBed.inject(ProductsService);
        lotService = TestBed.inject(LotService);
        userProfileSelectors = TestBed.inject(UserProfileSelectors);
        router = TestBed.inject(Router);
        discussionsSelectors = TestBed.inject(DiscussionsSelectors);
        gaService = TestBed.inject(GoogleAnalyticsService);
        translateService = TestBed.inject(TranslateService);
    });

    describe('openMoreActionsModal$', () => {
        type DialogRef = MatDialogRef<MoreActionsOnLotDialogComponent, MoreActionsOnLotModalResult>;

        it('should not dispatch action if modal closed without result', () => {
            const discussion: Discussion = { id: 1, lot: { id: 1, products: [] } };
            const userId = 1;

            const action = LotActions.openMoreActionsModal({ discussion, userId });

            const openModalWithoutResult = () => {
                return {
                    afterClosed: () => of(undefined),
                } as DialogRef;
            };
            jest.spyOn(dialog, 'open').mockImplementation(openModalWithoutResult);

            actions$ = hot('--a-', { a: action });
            const expected = cold('---');
            expect(effects.openMoreActionsModal$).toBeObservable(expected);
        });

        it('should dispatch confirmCancelSellSuccess if modal closed with result', () => {
            const discussion: Discussion = { id: 1, lot: { id: 1, products: [] } };
            const userId = 1;

            const action = LotActions.openMoreActionsModal({ discussion, userId });

            const openModalWithResult = () => {
                return {
                    afterClosed: () =>
                        of({ type: 'CANCEL_SELL', lotId: discussion.lot!.id } as MoreActionsOnLotModalResult),
                } as DialogRef;
            };
            jest.spyOn(dialog, 'open').mockImplementation(openModalWithResult);

            actions$ = hot('--a-', { a: action });
            const completion = LotActions.openConfirmCancelSellModal({ lotId: discussion.lot!.id! });
            const expected = cold('--b', { b: completion });
            expect(effects.openMoreActionsModal$).toBeObservable(expected);
        });
    });

    describe('openConfirmCancelSellModal$', () => {
        type DialogRef = MatDialogRef<ConfirmCancelSellDialogComponent, ConfirmCancelSellDialogResult>;

        it('should not dispatch action if modal closed without result', () => {
            const lotId = 14;

            const action = LotActions.openConfirmCancelSellModal({ lotId });

            const openModalWithoutResult = () => {
                return {
                    afterClosed: () => of(undefined),
                } as DialogRef;
            };
            jest.spyOn(dialog, 'open').mockImplementation(openModalWithoutResult);

            actions$ = hot('--a-', { a: action });
            const expected = cold('---');
            expect(effects.openConfirmCancelSellModal$).toBeObservable(expected);
        });

        it('should dispatch confirmCancelSellSuccess if modal closed with result', () => {
            const lotId = 14;

            const action = LotActions.openConfirmCancelSellModal({ lotId });

            const openModalWithoutResult = () => {
                return {
                    afterClosed: () => of({ lotId } as ConfirmCancelSellDialogResult),
                } as DialogRef;
            };
            jest.spyOn(dialog, 'open').mockImplementation(openModalWithoutResult);

            actions$ = hot('--a-', { a: action });
            const completion = LotActions.confirmCancelSellSuccess({ lotId });
            const expected = cold('--b', { b: completion });
            expect(effects.openConfirmCancelSellModal$).toBeObservable(expected);
        });
    });

    describe('confirmCancelSellSuccess$', () => {
        it('should dispatch cancelSell action', () => {
            const lotId = 14;

            const action = LotActions.confirmCancelSellSuccess({ lotId });
            const completion = DiscussionsActions.cancelSell({ lotId });

            actions$ = hot('--a-', { a: action });
            const expected = cold('--b', { b: completion });
            expect(effects.confirmCancelSellSuccess$).toBeObservable(expected);
        });
    });

    describe('openConfirmDeclineSellModal$', () => {
        type DialogRef = MatDialogRef<ConfirmDeclineSellDialogComponent, ConfirmDeclineSellDialogResult>;

        it('should not dispatch action if modal closed without result', () => {
            const lotId = 14;

            const action = LotActions.openConfirmDeclineSellModal({ lotId });

            const openModalWithoutResult = () => {
                return {
                    afterClosed: () => of(undefined),
                } as DialogRef;
            };
            jest.spyOn(dialog, 'open').mockImplementation(openModalWithoutResult);

            actions$ = hot('--a-', { a: action });
            const expected = cold('---');
            expect(effects.openConfirmDeclineSellModal$).toBeObservable(expected);
        });

        it('should dispatch confirmDeclineSellSuccess if modal closed with result', () => {
            const lotId = 14;

            const action = LotActions.openConfirmDeclineSellModal({ lotId });

            const openModalWithoutResult = () => {
                return {
                    afterClosed: () => of({ lotId } as ConfirmDeclineSellDialogResult),
                } as DialogRef;
            };
            jest.spyOn(dialog, 'open').mockImplementation(openModalWithoutResult);

            actions$ = hot('--a-', { a: action });
            const completion = LotActions.confirmDeclineSellSuccess({ lotId });
            const expected = cold('--b', { b: completion });
            expect(effects.openConfirmDeclineSellModal$).toBeObservable(expected);
        });
    });

    describe('confirmDeclineSellSuccess$', () => {
        it('should dispatch declineSell action', () => {
            const lotId = 14;

            const action = LotActions.confirmDeclineSellSuccess({ lotId });
            const completion = DiscussionsActions.declineSell({ lotId });

            actions$ = hot('--a-', { a: action });
            const expected = cold('--b', { b: completion });
            expect(effects.confirmDeclineSellSuccess$).toBeObservable(expected);
        });
    });

    describe('openConfirmCancelPackageModal$', () => {
        type DialogRef = MatDialogRef<ConfirmCancelPackageDialogComponent, ConfirmCancelPackageDialogResult>;

        it('should not dispatch action if modal closed without result', () => {
            const lotId = 14;

            const action = LotActions.openConfirmCancelPackageModal({ lotId });

            const openModalWithoutResult = () => {
                return {
                    afterClosed: () => of(undefined),
                } as DialogRef;
            };
            jest.spyOn(dialog, 'open').mockImplementation(openModalWithoutResult);

            actions$ = hot('--a-', { a: action });
            const expected = cold('---');
            expect(effects.openConfirmCancelPackageModal$).toBeObservable(expected);
        });

        it('should dispatch confirmCancelPackageSuccess if modal closed with result', () => {
            const lotId = 14;

            const action = LotActions.openConfirmCancelPackageModal({ lotId });

            const openModalWithoutResult = () => {
                return {
                    afterClosed: () => of({ lotId } as ConfirmCancelPackageDialogResult),
                } as DialogRef;
            };
            jest.spyOn(dialog, 'open').mockImplementation(openModalWithoutResult);

            actions$ = hot('--a-', { a: action });
            const completion = LotActions.confirmCancelPackageSuccess({ lotId });
            const expected = cold('--b', { b: completion });
            expect(effects.openConfirmCancelPackageModal$).toBeObservable(expected);
        });
    });

    describe('confirmCancelPackageSuccess$', () => {
        it('should dispatch cancelPackage action', () => {
            const lotId = 14;

            const action = LotActions.confirmCancelPackageSuccess({ lotId });
            const completion = DiscussionsActions.cancelPackage({ lotId });

            actions$ = hot('--a-', { a: action });
            const expected = cold('--b', { b: completion });
            expect(effects.confirmCancelPackageSuccess$).toBeObservable(expected);
        });
    });

    describe('confirmBlockUserSuccess$', () => {
        it('should dispatch blockUser action', () => {
            const userId = 14;

            const action = LotActions.confirmBlockUserSuccess({ userId });
            const completion = UserActions.blockUser({ userId });

            actions$ = hot('--a-', { a: action });
            const expected = cold('--b', { b: completion });
            expect(effects.confirmBlockUserSuccess$).toBeObservable(expected);
        });
    });

    describe('confirmUnblockUserSuccess$', () => {
        it('should dispatch unblockUser action', () => {
            const userId = 14;

            const action = LotActions.confirmUnblockUserSuccess({ userId });
            const completion = UserActions.unblockUser({ userId });

            actions$ = hot('--a-', { a: action });
            const expected = cold('--b', { b: completion });
            expect(effects.confirmUnblockUserSuccess$).toBeObservable(expected);
        });
    });

    describe('startDiscussion$', () => {
        it('should not dispatch actions', () => {
            const metadata: EffectsMetadata<LotEffects> = getEffectsMetadata(effects);

            expect(metadata.startDiscussion$!.dispatch).toBeFalsy();
        });

        it('should displayNotification if offline', () => {
            connectionSelectors.isOffline$ = cold('-t', { t: true });

            const productId = 1;
            const action = LotActions.startDiscussion({ productId });

            actions$ = hot('-a--', { a: action });

            expect(effects.startDiscussion$).toSatisfyOnFlush(() => {
                expect(offlineNotificationService.displayNotification).toHaveBeenCalled();
            });
        });

        it('should open GoToLoginDialogComponent if not logged', () => {
            const currentUrl = '/product/1';

            (router as any).url = currentUrl;
            connectionSelectors.isOffline$ = cold('-f', { f: false });
            userProfileSelectors.isLogged$ = cold('-f', { f: false });

            const productId = 1;
            const action = LotActions.startDiscussion({ productId });

            actions$ = hot('-a--', { a: action });

            expect(effects.startDiscussion$).toSatisfyOnFlush(() => {
                expect(dialog.open).toHaveBeenCalledWith(GoToLoginDialogComponent, {
                    data: { redirectUrl: currentUrl },
                });
            });
        });

        it('should navigate to chat of the product discussion', () => {
            const currentUrl = '/product/1';
            const discussionId = 2;

            const discussion = {
                id: discussionId,
                lot: {
                    id: 1,
                    status: LotStatusEnum.OPEN,
                },
            } as Discussion;

            (router as any).url = currentUrl;
            connectionSelectors.isOffline$ = cold('-f', { f: false });
            userProfileSelectors.isLogged$ = cold('-t', { t: true });

            jest.spyOn(discussionsSelectors, 'discussionByProductId$').mockImplementation(() => {
                return cold('-d', { d: discussion });
            });
            jest.spyOn(router, 'navigate').mockReturnValue(cold('-(u|)', { u: undefined }) as any);

            const productId = 1;
            const action = LotActions.startDiscussion({ productId });

            actions$ = hot('-a--', { a: action });

            expect(effects.startDiscussion$).toSatisfyOnFlush(() => {
                expect(gaService.trackSendMessage).not.toHaveBeenCalled();
                expect(router.navigate).toHaveBeenCalledWith(['chat', discussionId]);
            });
        });

        it('track send message when lot has products', () => {
            const currentUrl = '/product/1';
            const discussionId = 2;

            const discussion = {
                id: discussionId,
                seller: {
                    id: 13,
                },
                lot: {
                    id: 1,
                    status: LotStatusEnum.OPEN,
                    products: [{ id: 1 }, { id: 2 }],
                },
            } as Discussion;

            (router as any).url = currentUrl;
            connectionSelectors.isOffline$ = cold('-f', { f: false });
            userProfileSelectors.isLogged$ = cold('-t', { t: true });

            jest.spyOn(discussionsSelectors, 'discussionByProductId$').mockImplementation(() => {
                return cold('-d', { d: discussion });
            });
            jest.spyOn(router, 'navigate').mockReturnValue(cold('-(u|)', { u: undefined }) as any);

            const productId = 1;
            const action = LotActions.startDiscussion({ productId });

            actions$ = hot('-a--', { a: action });

            expect(effects.startDiscussion$).toSatisfyOnFlush(() => {
                expect(gaService.trackSendMessage).toHaveBeenCalled();
                expect(router.navigate).toHaveBeenCalledWith(['chat', discussionId]);
            });
        });
    });

    describe('startPaymentProcess$', () => {
        type DialogRef = MatDialogRef<PaymentProcessDialogComponent>;

        it('should dispatch actions', () => {
            const metadata: EffectsMetadata<LotEffects> = getEffectsMetadata(effects);

            expect(metadata.startPaymentProcess$!.dispatch).toBeTruthy();
        });

        it('should displayNotification if offline', () => {
            connectionSelectors.isOffline$ = cold('-t', { t: true });

            const productId = 1;
            const action = LotActions.startPaymentProcess({ productId });

            actions$ = hot('-a--', { a: action });

            expect(effects.startPaymentProcess$).toSatisfyOnFlush(() => {
                expect(offlineNotificationService.displayNotification).toHaveBeenCalled();
            });
        });

        it('should open GoToLoginDialogComponent if not logged', () => {
            const currentUrl = '/product/1';

            (router as any).url = currentUrl;
            connectionSelectors.isOffline$ = cold('-f', { f: false });
            userProfileSelectors.isLogged$ = cold('-f', { f: false });

            const productId = 1;
            const action = LotActions.startPaymentProcess({ productId });

            actions$ = hot('-a--', { a: action });

            expect(effects.startPaymentProcess$).toSatisfyOnFlush(() => {
                expect(dialog.open).toHaveBeenCalledWith(GoToLoginDialogComponent, {
                    data: { redirectUrl: currentUrl },
                });
            });
        });

        it('should open PaymentProcessDialogComponent', () => {
            connectionSelectors.isOffline$ = cold('-f', { f: false });
            userProfileSelectors.isLogged$ = cold('-t', { t: true });

            const lot = {
                id: 1,
            };

            const discussion = {
                id: 1,
                lot,
            };

            jest.spyOn(productsService, 'startListDiscussion').mockReturnValue(cold('-(d|)', { d: discussion }));
            jest.spyOn(lotService, 'getCart').mockReturnValue(cold('-(c|)', { c: lot }));

            const productId = 1;
            const action = LotActions.startPaymentProcess({ productId });

            actions$ = hot('-a--', { a: action });

            expect(effects.startPaymentProcess$).toSatisfyOnFlush(() => {
                expect(offlineNotificationService.displayNotification).not.toHaveBeenCalled();
                expect(dialog.open).toHaveBeenCalledWith(PaymentProcessDialogComponent, expect.anything());
            });
        });

        it('should not open PaymentProcessDialogComponent twice if buying too soon', () => {
            connectionSelectors.isOffline$ = cold('-f', { f: false });
            userProfileSelectors.isLogged$ = cold('-t', { t: true });

            const lot = {
                id: 1,
            };

            const discussion = {
                id: 1,
                lot,
            };

            jest.spyOn(productsService, 'startListDiscussion').mockReturnValue(cold('-(d|)', { d: discussion }));
            jest.spyOn(lotService, 'getCart').mockReturnValue(cold('-(c|)', { c: lot }));

            const openModalWithoutResult = () => {
                return {
                    afterClosed: () => of(undefined),
                } as DialogRef;
            };
            jest.spyOn(dialog, 'open').mockImplementation(openModalWithoutResult);

            const productId = 1;
            const action = LotActions.startPaymentProcess({ productId });

            actions$ = hot('-a-a', { a: action });

            expect(effects.startPaymentProcess$).toSatisfyOnFlush(() => {
                expect(offlineNotificationService.displayNotification).not.toHaveBeenCalled();
                expect(dialog.open).toHaveBeenCalledTimes(1);
                expect(dialog.open).toHaveBeenCalledWith(PaymentProcessDialogComponent, expect.anything());
            });
        });
    });

    describe('createLot$', () => {
        it('should dispatch actions', () => {
            const metadata: EffectsMetadata<LotEffects> = getEffectsMetadata(effects);

            expect(metadata.createLot$!.dispatch).toBeTruthy();
        });

        it('should displayNotification if offline', () => {
            connectionSelectors.isOffline$ = cold('-t', { t: true });

            const product = {
                id: 1,
            } as Product;
            const action = LotActions.createLot({ product });

            actions$ = hot('-a--', { a: action });

            expect(effects.createLot$).toSatisfyOnFlush(() => {
                expect(offlineNotificationService.displayNotification).toHaveBeenCalled();
            });
        });

        it('should open GoToLoginDialogComponent if not logged', () => {
            const currentUrl = '/product/1';

            (router as any).url = currentUrl;
            connectionSelectors.isOffline$ = cold('-f', { f: false });
            userProfileSelectors.isLogged$ = cold('-f', { f: false });

            const product = {
                id: 1,
            } as Product;
            const action = LotActions.createLot({ product });

            actions$ = hot('-a--', { a: action });

            expect(effects.createLot$).toSatisfyOnFlush(() => {
                expect(dialog.open).toHaveBeenCalledWith(GoToLoginDialogComponent, {
                    data: { redirectUrl: currentUrl },
                });
            });
        });

        it('should create new discussion', () => {
            connectionSelectors.isOffline$ = cold('-f', { f: false });
            userProfileSelectors.isLogged$ = cold('-t', { t: true });

            const product = {
                id: 1,
            } as Product;
            const action = LotActions.createLot({ product });

            actions$ = hot('-a--', { a: action });

            expect(effects.createLot$).toSatisfyOnFlush(() => {
                expect(productsService.startListDiscussion).toHaveBeenCalledWith([1]);
            });
        });
    });
});
