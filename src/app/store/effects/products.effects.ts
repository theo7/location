import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { ROUTER_NAVIGATED } from '@ngrx/router-store';
import { Action } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { isEqual } from 'lodash-es';
import { combineLatest, EMPTY, forkJoin, of, timer } from 'rxjs';
import {
    catchError,
    distinctUntilChanged,
    exhaustMap,
    filter,
    first,
    map,
    mergeMap,
    skipUntil,
    switchMap,
    take,
    tap,
    withLatestFrom,
} from 'rxjs/operators';
import { ErrorNotificationDialogComponent } from 'src/app/shared/components/error-notification-dialog/error-notification-dialog.component';
import { NotificationDialogComponent } from 'src/app/shared/components/notification-dialog/notification-dialog.component';
import { filterDefined } from 'src/app/shared/functions/operators.functions';
import { ImageService } from 'src/app/shared/services/image.service';
import { ShareService } from 'src/app/shared/services/share.service';
import { environment } from 'src/environments/environment';
import { HomeService } from '../../pages/home/home.service';
import { ConfirmationDialogComponent } from '../../shared/components/confirmation-dialog/confirmation-dialog.component';
import { randomNumber } from '../../shared/functions/number.functions';
import { convertParamsToFilter } from '../../shared/functions/product-filter.functions';
import {
    FilterType,
    ProductFilter,
    ProductStatusEnum,
    SearchRouterParams,
    UserRatingSummary,
} from '../../shared/models/models';
import { DeviceService } from '../../shared/services/device.service';
import { ProductsService } from '../../shared/services/products.service';
import { UserService } from '../../shared/services/user.service';
import { filtersActions } from '../actions/filters.actions';
import * as ProductsActions from '../actions/products.actions';
import { CategoriesSelectors } from '../services/categories-selectors.service';
import { FilterSelectors } from '../services/filter-selectors.service';
import { ProductsSelectors } from '../services/products-selectors.service';
import { RepositoriesSelectors } from '../services/repositories-selectors.service';
import { RouterSelectors } from '../services/router-selectors.service';
import { UserProfileSelectors } from '../services/user-selectors.service';

const PRODUCT_ALREADY_REPORTED = 'PRODUCT_025';
const PRODUCTS_PER_PAGE = 24;

@Injectable()
export class ProductsEffects {
    private readonly filterKey: FilterType = 'dressing';

    constructor(
        private readonly actions$: Actions,
        private readonly productsService: ProductsService,
        private readonly dialog: MatDialog,
        private readonly translateService: TranslateService,
        private readonly imageService: ImageService,
        private readonly shareService: ShareService,
        private readonly productsSelectors: ProductsSelectors,
        private readonly filterSelectors: FilterSelectors,
        private readonly routerSelectors: RouterSelectors,
        private readonly deviceService: DeviceService,
        private readonly router: Router,
        private readonly homeService: HomeService,
        private readonly repositoriesSelectors: RepositoriesSelectors,
        private readonly categoriesSelectors: CategoriesSelectors,
        private readonly userService: UserService,
        private readonly userProfileSelectors: UserProfileSelectors,
    ) {}

    listenForFirstUrlChange$ = createEffect(() =>
        this.routerSelectors.url$.pipe(
            withLatestFrom(this.routerSelectors.isProductRouteActive$),
            filter(([_, active]) => active),
            map(([url]) => url),
            switchMap(() =>
                forkJoin({
                    params: this.routerSelectors.pageParams$.pipe<SearchRouterParams>(first()),
                    categories: this.categoriesSelectors.allCategories$.pipe(first()),
                    brands: this.repositoriesSelectors.brands$.pipe(first()),
                    colors: this.repositoriesSelectors.colors$.pipe(first()),
                    conditions: this.repositoriesSelectors.conditions$.pipe(first()),
                    sizes: this.categoriesSelectors.allSizes$.pipe(first()),
                }),
            ),
            switchMap(({ params, categories, brands, colors, conditions, sizes }) => {
                const newFilter = convertParamsToFilter(params, categories, brands, colors, conditions, sizes);
                return of(filtersActions.set({ key: this.filterKey, filter: newFilter }));
            }),
        ),
    );

    changeDressingWhenFilterChangedAndProductActive$ = createEffect(() =>
        this.actions$.pipe(
            ofType(filtersActions.set),
            filter(payload => payload.key === this.filterKey),
            withLatestFrom(this.routerSelectors.isProductRouteActive$, this.productsSelectors.product$),
            filter(([_, routeActive]) => routeActive),
            map(([_, __, product]) => product?.owner?.id),
            filterDefined(),
            map(userId => ProductsActions.getDressing({ userId })),
        ),
    );

    loadProduct$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ProductsActions.loadProduct),
            switchMap(payload => {
                return this.productsService.getProduct(payload.productId).pipe(
                    switchMap(product =>
                        forkJoin([
                            of(product),
                            product.owner.id
                                ? this.userService.findRatingSummary(product.owner.id)
                                : of({} as UserRatingSummary),
                            this.userProfileSelectors.userProfile$.pipe(first()),
                            this.userProfileSelectors.isLogged$.pipe(
                                first(),
                                switchMap(isLogged => {
                                    if (isLogged && product.owner.id) {
                                        return this.userService.hasBlockedMe(product.owner.id);
                                    }
                                    return of(false);
                                }),
                            ),
                        ]),
                    ),
                    tap(([product, _, currentUser, __]) => {
                        if (
                            product.productStatus === ProductStatusEnum.MODERATED &&
                            !(currentUser && product.owner.id === currentUser.id)
                        ) {
                            this.router.navigate(['404'], { replaceUrl: true });
                        }
                    }),
                    map(([product, rating, _, hasBlockedMe]) => ({
                        ...product,
                        owner: {
                            ...product.owner,
                            rate: rating.averageRating,
                            totalRate: rating.totalRating,
                            hasBlockedMe,
                        },
                    })),
                    map(product => ProductsActions.loadProductSuccess({ product })),
                    catchError(() => of(ProductsActions.loadProductError())),
                );
            }),
        ),
    );

    loadProductSuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ProductsActions.loadProductSuccess),
            switchMap(({ product }) => {
                const actions: Action[] = [];

                if (product.owner.id) {
                    actions.push(ProductsActions.getDressing({ userId: product.owner.id }));
                }

                actions.push(ProductsActions.getProductViews({ productId: product.id }));

                return actions;
            }),
        ),
    );

    loadProductError$ = createEffect(
        () =>
            this.actions$.pipe(
                ofType(ProductsActions.loadProductError),
                tap(() => {
                    this.router.navigate(['search']);
                }),
            ),
        { dispatch: false },
    );

    report$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ProductsActions.reportProduct),
            tap(() => this.dialog.closeAll()),
            mergeMap(action =>
                this.productsService.reportProduct(action.productId, action.reason, action.content).pipe(
                    map(() => ProductsActions.reportProductSuccess()),
                    catchError(error => of(ProductsActions.reportProductError({ error }))),
                ),
            ),
        ),
    );

    openReportSuccessModal$ = createEffect(
        () =>
            this.actions$.pipe(
                ofType(ProductsActions.reportProductSuccess),
                tap(() => {
                    this.dialog.open(NotificationDialogComponent, {
                        panelClass: 'no-padding-dialog',
                        width: '280px',
                        data: {
                            type: 'success',
                            title: this.translateService.instant('product.report.successDialog.title'),
                            content: this.translateService.instant('product.report.successDialog.content'),
                            buttonContent: this.translateService.instant('common.ok'),
                        },
                    });
                }),
            ),
        { dispatch: false },
    );

    openReportFailedModal$ = createEffect(
        () =>
            this.actions$.pipe(
                ofType(ProductsActions.reportProductError),
                tap(({ error }) => {
                    const message = error.error?.message;

                    const isProductAlreadyReported = message === PRODUCT_ALREADY_REPORTED;

                    const dialogTitleProp = isProductAlreadyReported
                        ? 'product.report.errorDialog.alreadyReported.title'
                        : 'product.report.errorDialog.title';
                    const dialogContentProp = isProductAlreadyReported
                        ? 'product.report.errorDialog.alreadyReported.content'
                        : 'product.report.errorDialog.content';

                    this.dialog.open(ErrorNotificationDialogComponent, {
                        width: '280px',
                        data: {
                            title: this.translateService.instant(dialogTitleProp),
                            content: this.translateService.instant(dialogContentProp),
                            buttonContent: this.translateService.instant('common.ok'),
                        },
                    });
                }),
            ),
        { dispatch: false },
    );

    getDressing$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ProductsActions.getDressing),
            withLatestFrom(this.routerSelectors.productId$, this.filterSelectors.current$(this.filterKey)),
            switchMap(([payload, productId, currentFilter]) => {
                const filter: ProductFilter = {
                    ...currentFilter,
                };

                if (!!productId) {
                    filter.excludedIds = [productId];
                }

                return this.productsService.getUserProducts(payload.userId, filter, 0, PRODUCTS_PER_PAGE).pipe(
                    map(productsPage =>
                        ProductsActions.getDressingSuccess({
                            count: productsPage.totalElements,
                            products: productsPage.content,
                            total: productsPage.numberOfElements,
                        }),
                    ),
                    catchError(error => of(ProductsActions.getDressingError({ error }))),
                );
            }),
        ),
    );

    getNextDressing$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ProductsActions.getNextDressing),
            withLatestFrom(
                this.productsSelectors.page$,
                this.routerSelectors.productId$,
                this.filterSelectors.current$(this.filterKey),
            ),
            switchMap(([{ userId }, pageIndex, productId, currentFilter]) => {
                const filter: ProductFilter = {
                    ...currentFilter,
                };

                if (!!productId) {
                    filter.excludedIds = [productId];
                }

                return forkJoin([
                    this.productsService.getUserProducts(userId, filter, pageIndex + 1, PRODUCTS_PER_PAGE),
                    // ⚡️ : delay to avoid scrolling to fast
                    timer(randomNumber(1000, 2000)),
                ]).pipe(
                    map(([productPages]) => ProductsActions.getNextDressingSuccess({ products: productPages.content })),
                    catchError(error => of(ProductsActions.getNextDressingError({ error }))),
                );
            }),
        ),
    );

    getOwnDressing$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ProductsActions.getOwnDressing),
            withLatestFrom(this.userProfileSelectors.userProfile$),
            switchMap(([, user]) => {
                return this.productsService.getOwnProducts(0, PRODUCTS_PER_PAGE).pipe(
                    map(productsPages =>
                        ProductsActions.getOwnDressingSuccess({
                            count: productsPages.totalElements,
                            products: productsPages.content,
                            total: productsPages.totalElements,
                        }),
                    ),
                    catchError(error => of(ProductsActions.getOwnDressingError({ error }))),
                );
            }),
        ),
    );

    getOwnNextDressing$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ProductsActions.getOwnNextDressing),
            withLatestFrom(this.productsSelectors.page$),
            switchMap(([, pageIndex]) => {
                return forkJoin([
                    this.productsService.getOwnProducts(pageIndex + 1, PRODUCTS_PER_PAGE),
                    // ⚡️ : delay to avoid scrolling to fast
                    timer(randomNumber(1000, 2000)),
                ]).pipe(
                    map(([productsPages]) =>
                        ProductsActions.getNextDressingSuccess({ products: productsPages.content }),
                    ),
                    catchError(error => of(ProductsActions.getNextDressingError({ error }))),
                );
            }),
        ),
    );

    like$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ProductsActions.like),
            exhaustMap(({ productId }) => {
                return this.productsService.like(productId).pipe(
                    map(() => ProductsActions.likeSuccess({ productId })),
                    catchError(error => of(ProductsActions.likeError({ error }))),
                );
            }),
        ),
    );

    deleteLike$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ProductsActions.deleteLike),
            exhaustMap(({ productId }) => {
                return this.productsService.deleteLike(productId).pipe(
                    map(() => ProductsActions.deleteLikeSuccess({ productId })),
                    catchError(error => of(ProductsActions.deleteLikeError({ error }))),
                );
            }),
        ),
    );

    shareDressing$ = createEffect(
        () =>
            this.actions$.pipe(
                ofType(ProductsActions.shareDressing),
                filter(() => this.shareService.canShare()),
                tap(({ user }) => {
                    this.shareService.share({
                        title: this.translateService.instant('profile.view.dressing.share.title', {
                            nickname: user.nickname,
                        }),
                        text: this.translateService.instant('profile.view.dressing.share.text', {
                            nickname: user.nickname,
                        }),
                        url: `${environment.baseUrl}/profile/${user.id}`,
                    });
                }),
            ),
        { dispatch: false },
    );

    shareProduct$ = createEffect(
        () =>
            this.actions$.pipe(
                ofType(ProductsActions.shareProduct),
                filter(() => this.shareService.canShare()),
                exhaustMap(({ product }) => {
                    const files$ = environment.share.enableFileShare
                        ? forkJoin(
                              product.pictures.map(picture => {
                                  return this.imageService.fetchImageAsFile(this.imageService.getUrl(picture.fileName));
                              }),
                          )
                        : of([]);

                    return files$.pipe(catchError(() => of([]))).pipe(
                        tap(files => {
                            this.shareService.share({
                                title: this.translateService.instant('product.detail.share.title', {
                                    productName: product.label,
                                }),
                                text: this.translateService.instant('product.detail.share.text', {
                                    productName: product.label,
                                }),
                                url: `${environment.baseUrl}/product/${product.id}`,
                                files,
                            });
                        }),
                    );
                }),
            ),
        { dispatch: false },
    );

    getProductViews$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ProductsActions.getProductViews),
            filter(() => environment.displayProductViews),
            switchMap(({ productId }) => {
                return this.productsService.getProductViews(productId).pipe(
                    map(views => ProductsActions.getProductViewsSuccess({ views })),
                    catchError(error => of(ProductsActions.getProductViewsError({ error }))),
                );
            }),
        ),
    );

    deleteProduct$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ProductsActions.deleteProduct),
            switchMap(payload => {
                return this.dialog
                    .open(ConfirmationDialogComponent, {
                        data: {
                            title: this.translateService.instant('product.removeDialog.title'),
                            cancelLabel: this.translateService.instant('product.removeDialog.cancelLabel'),
                            validateLabel: this.translateService.instant('product.removeDialog.validateLabel'),
                        },
                    })
                    .afterClosed()
                    .pipe(
                        filter(e => !!e),
                        switchMap(() => {
                            return this.productsService.deactivateProduct(payload.productId).pipe(
                                map(() => ProductsActions.deleteProductSuccess({ productId: payload.productId })),
                                catchError(error => of(ProductsActions.deleteProductError({ error }))),
                            );
                        }),
                    );
            }),
        ),
    );

    deleteProductSuccess$ = createEffect(
        () =>
            this.actions$.pipe(
                ofType(ProductsActions.deleteProductSuccess),
                tap(payload => {
                    this.homeService.removeFromList(payload.productId);
                    this.router.navigateByUrl('/');
                }),
            ),
        { dispatch: false },
    );

    navigateWhenFilterChangedOnDesktop$ = createEffect(
        () =>
            combineLatest([this.routerSelectors.isProductRouteActive$, this.routerSelectors.url$]).pipe(
                switchMap(([isSearchEnabled]) => {
                    if (isSearchEnabled) {
                        return this.filterSelectors.currentRouterParams$(this.filterKey).pipe(
                            distinctUntilChanged((a, b) => isEqual(a, b)),
                            withLatestFrom(this.deviceService.isDesktop$),
                            filter(([_, isDesktop]) => isDesktop),
                            map(([queryParams]) => queryParams),
                            skipUntil(this.actions$.pipe(ofType(ROUTER_NAVIGATED))), // 💡 : prevent infinite loop
                            take(1),
                        );
                    }
                    return EMPTY;
                }),
                withLatestFrom(this.routerSelectors.productId$),
                tap(([queryParams, productId]) => {
                    this.router.navigate(['product', productId], { queryParams, replaceUrl: true }).then(() => {
                        const anchor = document.getElementById('filters-scroll-anchor');
                        anchor?.scrollIntoView(!!'ALIGN_TO_TOP');
                    });
                }),
            ),
        { dispatch: false },
    );

    getPriceAverage$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ProductsActions.getPriceAverage),
            switchMap(payload =>
                this.productsService.getPriceAverage(payload.filter).pipe(
                    map(priceAverage => ProductsActions.getPriceAverageSuccess({ priceAverage })),
                    catchError(error => of(ProductsActions.getPriceAverageError({ error }))),
                ),
            ),
        ),
    );
}
