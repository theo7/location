import { TestBed } from '@angular/core/testing';
import { Actions, EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { ROUTER_NAVIGATED, ROUTER_NAVIGATION } from '@ngrx/router-store';
import { cold, hot } from 'jest-marbles';
import { Observable } from 'rxjs';
import { Brand } from 'src/app/shared/models/models';
import { SeoService } from 'src/app/shared/services/seo.service';
import { RepositoriesSelectors } from '../services/repositories-selectors.service';
import { RouterSelectors } from '../services/router-selectors.service';
import { SeoEffects } from './seo.effects';

describe('seo.effects', () => {
    let actions$: Observable<Actions>;
    let effects: SeoEffects;
    let seoService: SeoService;
    let routerSelectors: RouterSelectors;
    let repositoriesSelectors: RepositoriesSelectors;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [
                SeoEffects,
                provideMockActions(() => actions$),
                {
                    provide: SeoService,
                    useValue: {
                        reset: jest.fn(),
                        configureBrandPage: jest.fn(),
                    },
                },
                {
                    provide: RouterSelectors,
                    useValue: {},
                },
                {
                    provide: RepositoriesSelectors,
                    useValue: {
                        brandBySlug$: jest.fn(),
                    },
                },
            ],
        });

        effects = TestBed.inject(SeoEffects);
        seoService = TestBed.inject(SeoService);
        routerSelectors = TestBed.inject(RouterSelectors);
        repositoriesSelectors = TestBed.inject(RepositoriesSelectors);
    });

    describe('resetSeoTags$', () => {
        it('should not dispatch actions', () => {
            const metadata: EffectsMetadata<SeoEffects> = getEffectsMetadata(effects);

            expect(metadata.resetSeoTags$!.dispatch).toBeFalsy();
        });

        it('should reset SEO when ROUTER_NAVIGATION action is dispatched', () => {
            const action = { type: ROUTER_NAVIGATION };
            actions$ = hot('-a-', { a: action });

            expect(effects.resetSeoTags$).toSatisfyOnFlush(() => {
                expect(seoService.reset).toHaveBeenCalled();
            });
        });
    });

    describe('setBrandPageSeoTags$', () => {
        it('should not dispatch actions', () => {
            const metadata: EffectsMetadata<SeoEffects> = getEffectsMetadata(effects);

            expect(metadata.setBrandPageSeoTags$!.dispatch).toBeFalsy();
        });

        it('should not set brand page SEO when ROUTER_NAVIGATED action is dispatched but no brand slug route param', () => {
            const action = { type: ROUTER_NAVIGATED };
            actions$ = hot('-a-', { a: action });

            routerSelectors.brandSlug$ = cold('s', { s: undefined });

            expect(effects.setBrandPageSeoTags$).toSatisfyOnFlush(() => {
                expect(seoService.configureBrandPage).not.toHaveBeenCalled();
            });
        });

        it('should not set brand page SEO when ROUTER_NAVIGATED action is dispatched but not brand for slug "kiabi"', () => {
            const action = { type: ROUTER_NAVIGATED };
            actions$ = hot('-a-', { a: action });

            routerSelectors.brandSlug$ = cold('s', { s: 'kiabi' });

            jest.spyOn(repositoriesSelectors, 'brandBySlug$').mockImplementation(() => {
                return cold('b', { b: undefined });
            });

            expect(effects.setBrandPageSeoTags$).toSatisfyOnFlush(() => {
                expect(seoService.configureBrandPage).not.toHaveBeenCalled();
            });
        });

        it('should set brand page SEO when ROUTER_NAVIGATED action is dispatched', () => {
            const action = { type: ROUTER_NAVIGATED };
            actions$ = hot('-a-', { a: action });

            routerSelectors.brandSlug$ = cold('s', { s: 'kiabi' });

            jest.spyOn(repositoriesSelectors, 'brandBySlug$').mockImplementation(slug => {
                return cold('b', {
                    b: {
                        id: 1,
                        slug,
                    } as Brand,
                });
            });

            expect(effects.setBrandPageSeoTags$).toSatisfyOnFlush(() => {
                expect(seoService.configureBrandPage).toHaveBeenCalledWith({
                    id: 1,
                    slug: 'kiabi',
                } as Brand);
            });
        });
    });
});
