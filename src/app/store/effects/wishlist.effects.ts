import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap, withLatestFrom } from 'rxjs/operators';
import { ProductsService } from 'src/app/shared/services/products.service';
import * as WishlistActions from '../actions/wishlist.actions';
import { WishlistSelectors } from '../services/wishlist-selectors.service';

@Injectable()
export class WishlistEffects {
    constructor(
        private readonly actions$: Actions,
        private readonly productsService: ProductsService,
        private readonly wishlistSelectors: WishlistSelectors,
    ) {}

    load$ = createEffect(() =>
        this.actions$.pipe(
            ofType(WishlistActions.load),
            switchMap(() =>
                this.productsService.getWishlist().pipe(
                    map(productsPages => WishlistActions.loadSuccess({ productsPages })),
                    catchError(error => of(WishlistActions.loadError({ error }))),
                ),
            ),
        ),
    );

    loadMore$ = createEffect(() =>
        this.actions$.pipe(
            ofType(WishlistActions.loadMore),
            withLatestFrom(this.wishlistSelectors.page$),
            switchMap(([, pageIndex]) =>
                this.productsService.getWishlist(pageIndex + 1).pipe(
                    map(productsPages => WishlistActions.loadMoreSuccess({ productsPages })),
                    catchError(error => of(WishlistActions.loadMoreError({ error }))),
                ),
            ),
        ),
    );
}
