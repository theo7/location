import { ApplicationRef, Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { StorageMap } from '@ngx-pwa/local-storage';
import { EMPTY, of } from 'rxjs';
import { catchError, exhaustMap, first, map, mergeMap, switchMap } from 'rxjs/operators';
import { ErrorLog } from '../../features/logger/logger.models';
import { LoggerService } from '../../features/logger/logger.service';
import * as LoggingActions from '../actions/logging.actions';
import { LOCAL_STORAGE_LOGGING, loggingInterval, SEND_ERRORS_INTERVAL } from './logging.effects.constants';

@Injectable()
export class LoggingEffects {
    constructor(
        private readonly actions$: Actions,
        private readonly loggerService: LoggerService,
        private readonly storage: StorageMap,
        private readonly appRef: ApplicationRef,
    ) {}

    saveError$ = createEffect(
        () =>
            this.actions$.pipe(
                ofType(LoggingActions.logError),
                mergeMap(({ errorLog }) =>
                    this.storage.get(LOCAL_STORAGE_LOGGING).pipe(
                        map(data => (data || []) as ErrorLog[]),
                        catchError(() => of([] as ErrorLog[])),
                        switchMap(existingLogs => {
                            const allLogs = existingLogs.concat(errorLog);
                            return this.storage.set(LOCAL_STORAGE_LOGGING, allLogs);
                        }),
                    ),
                ),
            ),
        { dispatch: false },
    );

    batchLogging$ = createEffect(
        () =>
            this.appRef.isStable.pipe(
                first(isStable => isStable === true),
                switchMap(() =>
                    loggingInterval(SEND_ERRORS_INTERVAL).pipe(
                        exhaustMap(() => {
                            const getErrorLogs$ = this.storage.get(LOCAL_STORAGE_LOGGING).pipe(
                                map(data => (data || []) as ErrorLog[]),
                                switchMap(logs => {
                                    if (logs.length <= 0) {
                                        return EMPTY;
                                    }

                                    const batch = logs.slice(0, 50);
                                    return of(batch);
                                }),
                                catchError(() => EMPTY),
                            );

                            return getErrorLogs$.pipe(
                                switchMap(errorLogs =>
                                    this.loggerService.sendErrors(errorLogs).pipe(
                                        map(() => errorLogs),
                                        catchError(() => EMPTY),
                                    ),
                                ),
                                switchMap(logsSent => {
                                    return this.storage.get(LOCAL_STORAGE_LOGGING).pipe(
                                        map(data => (data || []) as ErrorLog[]),
                                        map(logs => {
                                            const remainingErrorLogs = logs.filter(l1 =>
                                                logsSent.every(l2 => l1.key !== l2.key),
                                            );
                                            return remainingErrorLogs;
                                        }),
                                        switchMap(remainingErrorLogs => {
                                            return this.storage.set(LOCAL_STORAGE_LOGGING, remainingErrorLogs);
                                        }),
                                    );
                                }),
                            );
                        }),
                    ),
                ),
            ),
        { dispatch: false },
    );
}
