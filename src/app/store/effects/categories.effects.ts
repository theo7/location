import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType, ROOT_EFFECTS_INIT } from '@ngrx/effects';
import { forkJoin, of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import { ProductsService } from '../../shared/services/products.service';
import * as CategoriesActions from '../actions/categories.actions';

@Injectable()
export class CategoriesEffects {
    constructor(private actions$: Actions, private productsService: ProductsService) {}

    init$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ROOT_EFFECTS_INIT),
            switchMap(() => [CategoriesActions.getCategories()]),
        ),
    );

    getCategories$ = createEffect(() =>
        this.actions$.pipe(
            ofType(CategoriesActions.getCategories),
            switchMap(action => {
                return forkJoin([
                    this.productsService.getCategories(true),
                    this.productsService.getCategories(false),
                ]).pipe(
                    map(([indexedCategories, allCategories]) =>
                        allCategories.map(category => ({
                            ...category,
                            indexed: indexedCategories.some(indexedCategory => indexedCategory.id === category.id),
                        })),
                    ),
                    map(categories => CategoriesActions.getCategoriesSuccess({ categories })),
                    catchError(() => of(CategoriesActions.getCategoriesError())),
                );
            }),
        ),
    );
}
