import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, debounceTime, map, switchMap, tap } from 'rxjs/operators';
import { DocumentSentDialogComponent } from 'src/app/pages/user-bank-details/document-sent-dialog/document-sent-dialog.component';
import { UserService } from '../../shared/services/user.service';
import * as BankDetailsActions from '../actions/bank-details.actions';

@Injectable()
export class BankDetailsEffects {
    constructor(
        private readonly actions$: Actions,
        private readonly dialog: MatDialog,
        private readonly userService: UserService,
    ) {}

    loadBankDetails$ = createEffect(() =>
        this.actions$.pipe(
            ofType(BankDetailsActions.loadBankDetails),
            switchMap(() => {
                return this.userService.loadBankDetails().pipe(
                    map(response => BankDetailsActions.loadBankDetailsSuccess({ response })),
                    catchError(error => of(BankDetailsActions.loadBankDetailsError({ error }))),
                );
            }),
        ),
    );

    openSentDocumentDialog$ = createEffect(
        () =>
            this.actions$.pipe(
                ofType(BankDetailsActions.documentUploaded),
                debounceTime(100),
                tap(({ documentType }) => {
                    this.dialog.open(DocumentSentDialogComponent, {
                        data: {
                            documentType,
                        },
                    });
                }),
            ),
        { dispatch: false },
    );
}
