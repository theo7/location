import { TestBed } from '@angular/core/testing';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Actions, EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { TranslateService } from '@ngx-translate/core';
import { cold, hot } from 'jest-marbles';
import { Observable } from 'rxjs';
import { NotificationsService } from 'src/app/shared/services/notifications.service';
import { ServiceWorkerService } from 'src/app/shared/services/service-worker.service';
import { WebPushService } from 'src/app/shared/services/web-push.service';
import * as NotificationsActions from '../actions/notifications.actions';
import * as SettingsActions from '../actions/settings.actions';
import { UserProfileSelectors } from '../services/user-selectors.service';
import { WebNotificationsEffects } from './web-notifications.effects';

const token = '123-456';

describe('web-notifications.effects', () => {
    let actions$: Observable<Actions>;
    let router: Router;
    let notificationsService: NotificationsService;
    let matSnackBar: MatSnackBar;
    let translateService: TranslateService;
    let webPushService: WebPushService;
    let serviceWorkerService: ServiceWorkerService;
    let userProfileSelectors: UserProfileSelectors;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [
                WebNotificationsEffects,
                provideMockActions(() => actions$),
                {
                    provide: Router,
                    useValue: {
                        navigate: jest.fn(),
                    },
                },
                {
                    provide: NotificationsService,
                    useValue: {
                        register: jest.fn(),
                        unregister: jest.fn(),
                        isRegistered: jest.fn(),
                        watchNotifications: jest.fn(),
                        setNotificationRead: jest.fn(),
                    },
                },
                {
                    provide: MatSnackBar,
                    useValue: {
                        open: jest.fn(),
                    },
                },
                {
                    provide: TranslateService,
                    useValue: {
                        instant: jest.fn(),
                    },
                },
                {
                    provide: WebPushService,
                    useValue: {
                        isActivated: jest.fn(),
                        useServiceWorker: jest.fn(),
                        getNotificationToken: jest.fn(),
                        deleteToken: jest.fn(),
                    },
                },
                {
                    provide: ServiceWorkerService,
                    useValue: {
                        isSupported: jest.fn(),
                        getCurrent: jest.fn(),
                        message$: jest.fn(),
                    },
                },
                {
                    provide: UserProfileSelectors,
                    useValue: {
                        isLogged$: jest.fn(),
                    },
                },
            ],
        });

        router = TestBed.inject(Router);
        notificationsService = TestBed.inject(NotificationsService);
        matSnackBar = TestBed.inject(MatSnackBar);
        translateService = TestBed.inject(TranslateService);
        webPushService = TestBed.inject(WebPushService);
        serviceWorkerService = TestBed.inject(ServiceWorkerService);
        userProfileSelectors = TestBed.inject(UserProfileSelectors);
    });

    beforeEach(() => {
        userProfileSelectors.isLogged$ = cold('----');
        jest.spyOn(translateService, 'instant').mockReturnValue('');
        jest.spyOn(serviceWorkerService, 'message$').mockReturnValue(cold('---'));
    });

    describe('listenForRegister$', () => {
        it('should not dispatch actions', () => {
            userProfileSelectors.isLogged$ = cold('-t', { t: true });

            const effects = TestBed.inject(WebNotificationsEffects);
            const metadata: EffectsMetadata<WebNotificationsEffects> = getEffectsMetadata(effects);

            expect(metadata.listenForRegister$!.dispatch).toBeFalsy();
        });

        it('should register to notifications', () => {
            userProfileSelectors.isLogged$ = cold('-t', { t: true });
            jest.spyOn(serviceWorkerService, 'isSupported').mockReturnValue(true);
            jest.spyOn(serviceWorkerService, 'getCurrent').mockReturnValue(cold('-r', { r: {} }));
            jest.spyOn(webPushService, 'isActivated').mockReturnValue(true);
            jest.spyOn(webPushService, 'useServiceWorker').mockReturnValue(cold('-u', { u: undefined }));
            jest.spyOn(webPushService, 'getNotificationToken').mockReturnValue(cold('-t', { t: token }));
            jest.spyOn(notificationsService, 'register').mockReturnValue(cold('-u', { u: undefined }));
            jest.spyOn(notificationsService, 'isRegistered').mockReturnValue(cold('-f', { f: false }));

            const effects = TestBed.inject(WebNotificationsEffects);

            expect(effects.listenForRegister$).toSatisfyOnFlush(() => {
                expect(notificationsService.register).toHaveBeenCalledWith(token);
            });
        });
    });

    describe('redirectOnBackgroundMode$', () => {
        it('should not dispatch actions', () => {
            const effects = TestBed.inject(WebNotificationsEffects);

            const metadata: EffectsMetadata<WebNotificationsEffects> = getEffectsMetadata(effects);

            expect(metadata.redirectOnBackgroundMode$!.dispatch).toBeFalsy();
        });

        it('should navigate to /dressing', () => {
            const event = {
                data: {
                    uri: '/dressing',
                    type: 'redirect',
                },
            };

            jest.spyOn(serviceWorkerService, 'message$').mockReturnValue(cold('-e', { e: event }));

            const effects = TestBed.inject(WebNotificationsEffects);

            expect(effects.redirectOnBackgroundMode$).toSatisfyOnFlush(() => {
                expect(router.navigate).toHaveBeenCalledWith(['', 'dressing']);
            });
        });

        it('should not navigate if message type is not "redirect"', () => {
            const event = {
                data: {
                    uri: '/dressing',
                    type: 'navigate',
                },
            };

            jest.spyOn(serviceWorkerService, 'message$').mockReturnValue(cold('-e', { e: event }));

            const effects = TestBed.inject(WebNotificationsEffects);

            expect(effects.redirectOnBackgroundMode$).toSatisfyOnFlush(() => {
                expect(router.navigate).not.toHaveBeenCalled();
            });
        });

        it('should not navigate if no uri', () => {
            const event = {
                data: {
                    type: 'redirect',
                },
            };

            jest.spyOn(serviceWorkerService, 'message$').mockReturnValue(cold('-e', { e: event }));

            const effects = TestBed.inject(WebNotificationsEffects);

            expect(effects.redirectOnBackgroundMode$).toSatisfyOnFlush(() => {
                expect(router.navigate).not.toHaveBeenCalled();
            });
        });

        it('should not navigate if no event data', () => {
            const event = {};

            jest.spyOn(serviceWorkerService, 'message$').mockReturnValue(cold('-e', { e: event }));

            const effects = TestBed.inject(WebNotificationsEffects);

            expect(effects.redirectOnBackgroundMode$).toSatisfyOnFlush(() => {
                expect(router.navigate).not.toHaveBeenCalled();
            });
        });
    });

    describe('requestPermission$', () => {
        const action = NotificationsActions.requestPermission();

        it('should get token and dispatch requestPermissionSuccess', () => {
            jest.spyOn(webPushService, 'getNotificationToken').mockReturnValue(cold('-(t|)', { t: token }));

            const effects = TestBed.inject(WebNotificationsEffects);

            actions$ = hot('-a--', { a: action });
            const expected = NotificationsActions.requestPermissionSuccess({ token });

            expect(effects.requestPermission$).toBeObservable(cold('--e', { e: expected }));
        });

        it('should dispatch requestPermissionFailed when no token found', () => {
            const error = 'Cannot find token...';

            jest.spyOn(webPushService, 'getNotificationToken').mockReturnValue(cold('-#', undefined, error));

            const effects = TestBed.inject(WebNotificationsEffects);

            actions$ = hot('-a--', { a: action });
            const expected = NotificationsActions.requestPermissionFailed({ error });

            expect(effects.requestPermission$).toBeObservable(cold('--e', { e: expected }));
        });
    });

    describe('revokePermission$', () => {
        const action = NotificationsActions.revokePermission();

        it('should delete token and dispatch revokePermissionSuccess', () => {
            jest.spyOn(webPushService, 'getNotificationToken').mockReturnValue(cold('-(t|)', { t: token }));
            jest.spyOn(webPushService, 'deleteToken').mockReturnValue(cold('-(t|)', { t: true }));

            const effects = TestBed.inject(WebNotificationsEffects);

            actions$ = hot('-a--', { a: action });
            const expected = NotificationsActions.revokePermissionSuccess({ token });

            expect(effects.revokePermission$).toBeObservable(cold('---e', { e: expected }));
        });

        it('should dispatch revokePermissionFailed if we cannot delete token', () => {
            jest.spyOn(webPushService, 'getNotificationToken').mockReturnValue(cold('-(t|)', { t: token }));
            jest.spyOn(webPushService, 'deleteToken').mockReturnValue(cold('-(f|)', { f: false }));

            const effects = TestBed.inject(WebNotificationsEffects);

            actions$ = hot('-a--', { a: action });
            const expected = NotificationsActions.revokePermissionFailed({ error: 'Failed to revoke permission...' });

            expect(effects.revokePermission$).toBeObservable(cold('---e', { e: expected }));
        });

        it('should dispatch revokePermissionFailed if getNotificationToken fails', () => {
            const error = 'Cannot find token...';

            jest.spyOn(webPushService, 'getNotificationToken').mockReturnValue(cold('-#', undefined, error));
            jest.spyOn(webPushService, 'deleteToken').mockReturnValue(cold('-(f|)', { f: false }));

            const effects = TestBed.inject(WebNotificationsEffects);

            actions$ = hot('-a--', { a: action });
            const expected = NotificationsActions.revokePermissionFailed({ error });

            expect(effects.revokePermission$).toBeObservable(cold('--e', { e: expected }));
        });

        it('should dispatch revokePermissionFailed if we cannot delete token', () => {
            const error = 'Cannot delete token...';

            jest.spyOn(webPushService, 'getNotificationToken').mockReturnValue(cold('-(t|)', { t: token }));
            jest.spyOn(webPushService, 'deleteToken').mockReturnValue(cold('-#', undefined, error));

            const effects = TestBed.inject(WebNotificationsEffects);

            actions$ = hot('-a--', { a: action });
            const expected = NotificationsActions.revokePermissionFailed({ error });

            expect(effects.revokePermission$).toBeObservable(cold('---e', { e: expected }));
        });
    });

    describe('register$', () => {
        const action = NotificationsActions.requestPermissionSuccess({ token });

        it('should not dispatch actions', () => {
            const effects = TestBed.inject(WebNotificationsEffects);

            const metadata: EffectsMetadata<WebNotificationsEffects> = getEffectsMetadata(effects);

            expect(metadata.register$!.dispatch).toBeFalsy();
        });

        it('should register to notifications', () => {
            jest.spyOn(notificationsService, 'register').mockReturnValue(cold('-u', { u: undefined }));

            const effects = TestBed.inject(WebNotificationsEffects);

            actions$ = hot('-a--', { a: action });

            expect(effects.register$).toSatisfyOnFlush(() => {
                expect(notificationsService.register).toHaveBeenCalledWith(token);
            });
        });

        it('should not stop effect even if register failed', () => {
            const error = 'Cannot register this device...';

            jest.spyOn(notificationsService, 'register').mockReturnValue(cold('-#', undefined, error));

            const effects = TestBed.inject(WebNotificationsEffects);

            actions$ = hot('-a--a--a', { a: action });

            expect(effects.register$).toSatisfyOnFlush(() => {
                expect(notificationsService.register).toHaveBeenCalledTimes(3);
            });
        });
    });

    describe('unregister$', () => {
        const action = NotificationsActions.revokePermissionSuccess({ token });

        it('should not dispatch actions', () => {
            const effects = TestBed.inject(WebNotificationsEffects);

            const metadata: EffectsMetadata<WebNotificationsEffects> = getEffectsMetadata(effects);

            expect(metadata.unregister$!.dispatch).toBeFalsy();
        });

        it('should register to notifications', () => {
            jest.spyOn(notificationsService, 'unregister').mockReturnValue(cold('-u', { u: undefined }));

            const effects = TestBed.inject(WebNotificationsEffects);

            actions$ = hot('-a--', { a: action });

            expect(effects.unregister$).toSatisfyOnFlush(() => {
                expect(notificationsService.unregister).toHaveBeenCalledWith(token);
            });
        });

        it('should not stop effect even if unregister failed', () => {
            const error = 'Cannot unregister this device...';

            jest.spyOn(notificationsService, 'unregister').mockReturnValue(cold('-#', undefined, error));

            const effects = TestBed.inject(WebNotificationsEffects);

            actions$ = hot('-a--a--a', { a: action });

            expect(effects.unregister$).toSatisfyOnFlush(() => {
                expect(notificationsService.unregister).toHaveBeenCalledTimes(3);
            });
        });
    });

    describe('displayMessageWhenRequestFailed$', () => {
        const error = 'Cannot activate notifications...';
        const action = NotificationsActions.requestPermissionFailed({ error });

        it('should not dispatch actions', () => {
            const effects = TestBed.inject(WebNotificationsEffects);

            const metadata: EffectsMetadata<WebNotificationsEffects> = getEffectsMetadata(effects);

            expect(metadata.displayMessageWhenRequestFailed$!.dispatch).toBeFalsy();
        });

        it('should open snackbar', () => {
            const effects = TestBed.inject(WebNotificationsEffects);

            actions$ = hot('-a--', { a: action });

            expect(effects.displayMessageWhenRequestFailed$).toSatisfyOnFlush(() => {
                expect(matSnackBar.open).toHaveBeenCalled();
            });
        });
    });

    describe('displayMessageWhenRevokeFailed$', () => {
        const error = 'Cannot deactivate notifications...';
        const action = NotificationsActions.revokePermissionFailed({ error });

        it('should not dispatch actions', () => {
            const effects = TestBed.inject(WebNotificationsEffects);

            const metadata: EffectsMetadata<WebNotificationsEffects> = getEffectsMetadata(effects);

            expect(metadata.displayMessageWhenRevokeFailed$!.dispatch).toBeFalsy();
        });

        it('should open snackbar', () => {
            const effects = TestBed.inject(WebNotificationsEffects);

            actions$ = hot('-a--', { a: action });

            expect(effects.displayMessageWhenRevokeFailed$).toSatisfyOnFlush(() => {
                expect(matSnackBar.open).toHaveBeenCalled();
            });
        });
    });

    describe('setNotificationSettingToTrue$', () => {
        const action = NotificationsActions.requestPermissionSuccess({ token });

        it('should dispatch setNotificationSetting with setting to true', () => {
            const effects = TestBed.inject(WebNotificationsEffects);

            actions$ = hot('-a--', { a: action });
            const expected = SettingsActions.setNotificationSetting({ value: true });

            expect(effects.setNotificationSettingToTrue$).toBeObservable(cold('-e', { e: expected }));
        });
    });

    describe('setNotificationSettingToFalse$', () => {
        const action = NotificationsActions.revokePermissionSuccess({ token });

        it('should dispatch setNotificationSetting with setting to false', () => {
            const effects = TestBed.inject(WebNotificationsEffects);

            actions$ = hot('-a--', { a: action });
            const expected = SettingsActions.setNotificationSetting({ value: false });

            expect(effects.setNotificationSettingToFalse$).toBeObservable(cold('-e', { e: expected }));
        });
    });
});
