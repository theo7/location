import { TestBed } from '@angular/core/testing';
import { Actions } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { cold, hot } from 'jest-marbles';
import { Observable } from 'rxjs';
import { StoreCreditService } from 'src/app/shared/services/store-credit.service';
import * as CreditActions from '../actions/credit.actions';
import { CashHistory, PageLw } from './../../shared/models/models';
import { CreditEffects } from './credit.effects';

describe('credit.effects', () => {
    let actions$: Observable<Actions>;
    let creditEffects: CreditEffects;
    let storeCreditService: StoreCreditService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [
                CreditEffects,
                provideMockActions(() => actions$),
                {
                    provide: StoreCreditService,
                    useValue: {
                        getCashCreditHistory: jest.fn(),
                    },
                },
            ],
        });

        creditEffects = TestBed.inject(CreditEffects);
        storeCreditService = TestBed.inject(StoreCreditService);
    });

    describe('getCashHistory$', () => {
        const page = 2;
        const action = CreditActions.getCashHistory({ page });

        it('should dispatch getCashHistorySuccess action', () => {
            const history: CashHistory[] = [
                {
                    id: 1,
                    amount: 10.99,
                } as CashHistory,
                {
                    id: 2,
                    amount: 3.2,
                } as CashHistory,
            ];

            const pageLwHistory: PageLw<CashHistory> = {
                totalPages: 4,
                content: history,
            };

            jest.spyOn(storeCreditService, 'getCashCreditHistory').mockReturnValue(cold('-(h|)', { h: pageLwHistory }));

            const completion = CreditActions.getCashHistorySuccess({ history: pageLwHistory });

            actions$ = hot('-a-', { a: action });
            const expected = cold('--b', { b: completion });
            expect(creditEffects.getCashHistory$).toBeObservable(expected);
        });

        it('should dispatch getCashHistoryError action', () => {
            const error = 'Cannot get the cash history';
            jest.spyOn(storeCreditService, 'getCashCreditHistory').mockReturnValue(cold('-#', undefined, error));

            const completion = CreditActions.getCashHistoryError({ error });

            actions$ = hot('-a-', { a: action });
            const expected = cold('--b', { b: completion });
            expect(creditEffects.getCashHistory$).toBeObservable(expected);
        });
    });
});
