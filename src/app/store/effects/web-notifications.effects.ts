import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { TranslateService } from '@ngx-translate/core';
import { EMPTY, of } from 'rxjs';
import { catchError, filter, map, switchMap, tap } from 'rxjs/operators';
import { ServiceWorkerService } from 'src/app/shared/services/service-worker.service';
import { WebPushService } from 'src/app/shared/services/web-push.service';
import { NotificationsService } from '../../shared/services/notifications.service';
import * as NotificationsActions from '../actions/notifications.actions';
import * as SettingsActions from '../actions/settings.actions';
import { UserProfileSelectors } from '../services/user-selectors.service';

@Injectable()
export class WebNotificationsEffects {
    constructor(
        private readonly actions$: Actions,
        private readonly router: Router,
        private readonly notificationsService: NotificationsService,
        private readonly snackBar: MatSnackBar,
        private readonly translateService: TranslateService,
        private readonly webPushService: WebPushService,
        private readonly serviceWorkerService: ServiceWorkerService,
        private readonly userProfileSelectors: UserProfileSelectors,
    ) {}

    listenForRegister$ = createEffect(
        () =>
            this.userProfileSelectors.isLogged$.pipe(
                filter(isLogged => isLogged),
                filter(() => this.serviceWorkerService.isSupported()),
                filter(() => this.webPushService.isActivated()),
                switchMap(() => this.webPushService.getNotificationToken()),
                switchMap(token =>
                    this.notificationsService.isRegistered(token).pipe(
                        filter(isRegistered => !isRegistered),
                        map(_ => token),
                    ),
                ),
                switchMap(token => this.notificationsService.register(token)),
            ),
        { dispatch: false },
    );

    redirectOnBackgroundMode$ = createEffect(
        () =>
            this.serviceWorkerService.message$().pipe(
                filter((event: any) => {
                    return !!event.data && !!event.data.uri && event.data.type === 'redirect';
                }),
                tap((event: any) => {
                    this.router.navigate(event.data.uri.split('/'));
                }),
            ),
        { dispatch: false },
    );

    requestPermission$ = createEffect(() =>
        this.actions$.pipe(
            ofType(NotificationsActions.requestPermission),
            switchMap(() =>
                this.webPushService.getNotificationToken().pipe(
                    map(token => NotificationsActions.requestPermissionSuccess({ token })),
                    catchError(error => of(NotificationsActions.requestPermissionFailed({ error }))),
                ),
            ),
        ),
    );

    revokePermission$ = createEffect(() =>
        this.actions$.pipe(
            ofType(NotificationsActions.revokePermission),
            switchMap(() =>
                this.webPushService.getNotificationToken().pipe(
                    switchMap(token => {
                        return this.webPushService
                            .deleteToken(token)
                            .pipe(map<boolean, [boolean, string]>(success => [success, token]));
                    }),
                    map(([success, token]) => {
                        if (success) {
                            return NotificationsActions.revokePermissionSuccess({ token });
                        }
                        return NotificationsActions.revokePermissionFailed({
                            error: 'Failed to revoke permission...',
                        });
                    }),
                    catchError(error => of(NotificationsActions.revokePermissionFailed({ error }))),
                ),
            ),
        ),
    );

    register$ = createEffect(
        () =>
            this.actions$.pipe(
                ofType(NotificationsActions.requestPermissionSuccess),
                switchMap(({ token }) => this.notificationsService.register(token).pipe(catchError(() => EMPTY))),
            ),
        { dispatch: false },
    );

    unregister$ = createEffect(
        () =>
            this.actions$.pipe(
                ofType(NotificationsActions.revokePermissionSuccess),
                switchMap(({ token }) => this.notificationsService.unregister(token).pipe(catchError(() => EMPTY))),
            ),
        { dispatch: false },
    );

    displayMessageWhenRequestFailed$ = createEffect(
        () =>
            this.actions$.pipe(
                ofType(NotificationsActions.requestPermissionFailed),
                tap(() => {
                    this.snackBar.open(
                        this.translateService.instant('notifications.permissions.request.failed'),
                        undefined,
                        {
                            duration: 2500,
                        },
                    );
                }),
            ),
        { dispatch: false },
    );

    displayMessageWhenRevokeFailed$ = createEffect(
        () =>
            this.actions$.pipe(
                ofType(NotificationsActions.revokePermissionFailed),
                tap(() => {
                    this.snackBar.open(
                        this.translateService.instant('notifications.permissions.revoke.failed'),
                        undefined,
                        {
                            horizontalPosition: 'end',
                            verticalPosition: 'top',
                            duration: 2500,
                        },
                    );
                }),
            ),
        { dispatch: false },
    );

    setNotificationSettingToTrue$ = createEffect(() =>
        this.actions$.pipe(
            ofType(NotificationsActions.requestPermissionSuccess),
            map(() => SettingsActions.setNotificationSetting({ value: true })),
        ),
    );

    setNotificationSettingToFalse$ = createEffect(() =>
        this.actions$.pipe(
            ofType(NotificationsActions.revokePermissionSuccess),
            map(() => SettingsActions.setNotificationSetting({ value: false })),
        ),
    );
}
