import { Injectable, Injector } from '@angular/core';
import { Camera, CameraResultType, CameraSource, Photo as CameraPhoto } from '@capacitor/camera';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { TranslateService } from '@ngx-translate/core';
import { Observable, of } from 'rxjs';
import { fromPromise } from 'rxjs/internal-compatibility';
import { catchError, filter, map, switchMap, withLatestFrom } from 'rxjs/operators';
import { pdfService$ } from 'src/app/imports.dynamic';
import { environment } from '../../../environments/environment';
import { ChatService } from '../../shared/services/chat.service';
import { ImageService } from '../../shared/services/image.service';
import { ProductsService } from '../../shared/services/products.service';
import { fileActions } from '../actions/file.actions';

@Injectable()
export class FileEffects {
    constructor(
        private readonly actions$: Actions,
        private readonly translateService: TranslateService,
        private readonly productsService: ProductsService,
        private readonly imageService: ImageService,
        private readonly chatService: ChatService,
        private readonly injector: Injector,
    ) {}

    selectNativeFile$ = createEffect(() =>
        this.actions$.pipe(
            ofType(fileActions.selectFile),
            switchMap(({ fileType }) => {
                return fromPromise(
                    Camera.getPhoto({
                        quality: 50,
                        allowEditing: false,
                        webUseInput: true,
                        resultType: CameraResultType.Uri,
                        source: CameraSource.Prompt,
                        promptLabelHeader: this.translateService.instant('product.takePhoto.promptLabelHeader'),
                        promptLabelCancel: this.translateService.instant('product.takePhoto.promptLabelCancel'),
                        promptLabelPhoto: this.translateService.instant('product.takePhoto.promptLabelPhoto'),
                        promptLabelPicture: this.translateService.instant('product.takePhoto.promptLabelPicture'),
                    }),
                ).pipe(
                    filter(file => !!file),
                    map(file => fileActions.fileSelected({ file, fileType })),
                );
            }),
        ),
    );

    uploadProductPicture$ = createEffect(() =>
        this.actions$.pipe(
            ofType(fileActions.uploadProductPicture),
            switchMap(({ picture, index, pictureType }) => {
                return this.compressPicture(picture).pipe(
                    switchMap(file => {
                        return this.productsService.uploadPicture(file, pictureType).pipe(
                            map(picture => fileActions.uploadProductPictureSuccess({ picture, index })),
                            catchError(error => of(fileActions.uploadProductPictureError({ error, index }))),
                        );
                    }),
                );
            }),
        ),
    );

    uploadRawPicture$ = createEffect(() =>
        this.actions$.pipe(
            ofType(fileActions.uploadRawPicture),
            switchMap(({ file, index, pictureType }) => {
                return this.productsService.uploadPicture(file, pictureType).pipe(
                    map(picture => fileActions.uploadProductPictureSuccess({ picture, index })),
                    catchError(error => of(fileActions.uploadProductPictureError({ error, index }))),
                );
            }),
        ),
    );

    uploadChatPicture$ = createEffect(() =>
        this.actions$.pipe(
            ofType(fileActions.uploadChatPicture),
            switchMap(({ picture, discussionId, index }) => {
                return this.compressPicture(picture).pipe(
                    switchMap(file => {
                        return this.chatService.uploadPicture(file, discussionId).pipe(
                            map(picture => fileActions.uploadChatPictureSuccess({ picture, index })),
                            catchError(error => of(fileActions.uploadChatPictureError({ error, index }))),
                        );
                    }),
                );
            }),
        ),
    );

    uploadKycFile$ = createEffect(() =>
        this.actions$.pipe(
            ofType(fileActions.uploadKycFile),
            switchMap(({ file, fileType }) => {
                return this.compressPicture(file).pipe(
                    withLatestFrom(pdfService$),
                    switchMap(([compressedFile, PdfService]) => {
                        const pdfService = this.injector.get(PdfService);

                        return pdfService.toPdf(compressedFile).pipe(
                            map(pdfFile => fileActions.uploadKycFileSuccess({ pdfFile, fileType })),
                            catchError(error => of(fileActions.uploadKycFileError({ error, fileType }))),
                        );
                    }),
                );
            }),
        ),
    );

    private compressPicture(picture: CameraPhoto): Observable<Blob> {
        return fromPromise(fetch(picture.webPath!)).pipe(
            switchMap(data => fromPromise(data.blob())),
            switchMap(picture => {
                if (picture.size > environment.picture.maxSize) {
                    return this.imageService.compress(picture);
                }

                return of(picture);
            }),
        );
    }
}
