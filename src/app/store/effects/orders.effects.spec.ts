import { TestBed } from '@angular/core/testing';
import { Actions } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { cold, hot } from 'jest-marbles';
import { Observable } from 'rxjs';
import { Lot } from 'src/app/shared/models/models';
import { LotService } from 'src/app/shared/services/lot.service';
import * as OrdersActions from '../actions/orders.actions';
import { OrdersEffects } from './orders.effects';

describe('orders.effects', () => {
    let actions$: Observable<Actions>;
    let lotService: LotService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [
                OrdersEffects,
                provideMockActions(() => actions$),
                {
                    provide: LotService,
                    useValue: {
                        getUserSales: jest.fn(),
                        getUserPurchases: jest.fn(),
                    },
                },
            ],
        });

        lotService = TestBed.inject(LotService);
    });

    describe('loadSales$', () => {
        it('should load sales', () => {
            const lots = [
                {
                    id: 1,
                },
            ] as Lot[];

            const effects = TestBed.inject(OrdersEffects);

            const action = OrdersActions.loadSales();
            const completion = OrdersActions.loadSalesSuccess({ lots });

            jest.spyOn(lotService, 'getUserSales').mockReturnValue(cold('-l', { l: lots }));

            actions$ = hot('-a-', { a: action });
            const expected = cold('--b', { b: completion });
            expect(effects.loadSales$).toBeObservable(expected);
        });

        it('should failed to load sales', () => {
            const error = 'Cannot load user sales...';

            const effects = TestBed.inject(OrdersEffects);

            const action = OrdersActions.loadSales();
            const completion = OrdersActions.loadSalesError({ error });

            jest.spyOn(lotService, 'getUserSales').mockReturnValue(cold('-#', undefined, error));

            actions$ = hot('-a-', { a: action });
            const expected = cold('--b', { b: completion });
            expect(effects.loadSales$).toBeObservable(expected);
        });
    });

    describe('loadPurchases$', () => {
        it('should load sales', () => {
            const lots = [
                {
                    id: 1,
                },
            ] as Lot[];

            const effects = TestBed.inject(OrdersEffects);

            const action = OrdersActions.loadPurchases();
            const completion = OrdersActions.loadPurchasesSuccess({ lots });

            jest.spyOn(lotService, 'getUserPurchases').mockReturnValue(cold('-l', { l: lots }));

            actions$ = hot('-a-', { a: action });
            const expected = cold('--b', { b: completion });
            expect(effects.loadPurchases$).toBeObservable(expected);
        });

        it('should failed to load sales', () => {
            const error = 'Cannot load user purchases...';

            const effects = TestBed.inject(OrdersEffects);

            const action = OrdersActions.loadPurchases();
            const completion = OrdersActions.loadPurchasesError({ error });

            jest.spyOn(lotService, 'getUserPurchases').mockReturnValue(cold('-#', undefined, error));

            actions$ = hot('-a-', { a: action });
            const expected = cold('--b', { b: completion });
            expect(effects.loadPurchases$).toBeObservable(expected);
        });
    });
});
