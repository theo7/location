import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType, ROOT_EFFECTS_INIT } from '@ngrx/effects';
import { StorageMap } from '@ngx-pwa/local-storage';
import { forkJoin, of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import { LOCAL_STORAGE_NOTIFICATIONS_SETTINGS } from 'src/app/shared/models/models';
import { TransportService } from 'src/app/shared/services/transport.service';
import { UserService } from 'src/app/shared/services/user.service';
import * as SettingsActions from '../actions/settings.actions';

@Injectable()
export class SettingsEffects {
    constructor(
        private readonly actions$: Actions,
        private readonly userService: UserService,
        private readonly storage: StorageMap,
        private transportService: TransportService,
    ) {}

    init$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ROOT_EFFECTS_INIT),
            switchMap(() => [SettingsActions.getShippingsFeesSettings()]),
        ),
    );

    getSettings$ = createEffect(() =>
        this.actions$.pipe(
            ofType(SettingsActions.getSettings),
            switchMap(() =>
                forkJoin([
                    this.userService.getSettings(),
                    this.storage.get<boolean>(LOCAL_STORAGE_NOTIFICATIONS_SETTINGS, { type: 'boolean' }),
                ]).pipe(
                    map(([globalSettings, acceptPushNotification]) => {
                        return {
                            ...globalSettings,
                            acceptPushNotification: acceptPushNotification || false,
                        };
                    }),
                    map(settings => SettingsActions.getSettingsSuccess({ settings })),
                    catchError(error => of(SettingsActions.getSettingsFailed({ error }))),
                ),
            ),
        ),
    );

    getShippingFees$ = createEffect(() =>
        this.actions$.pipe(
            ofType(SettingsActions.getShippingsFeesSettings),
            switchMap(() => {
                return this.transportService.getShippingFees().pipe(
                    map(fee => SettingsActions.getShippingsFeesSettingsSuccess({ fee })),
                    catchError(error => of(SettingsActions.getShippingsFeesSettingsError({ error }))),
                );
            }),
        ),
    );

    setGlobalSettings$ = createEffect(() =>
        this.actions$.pipe(
            ofType(SettingsActions.setGlobalSettings),
            switchMap(({ settings }) =>
                this.userService.updateSettings(settings).pipe(
                    map(() => SettingsActions.setGlobalSettingsSuccess({ settings })),
                    catchError(error => of(SettingsActions.setGlobalSettingsFailed({ error }))),
                ),
            ),
        ),
    );

    setNotificationSetting$ = createEffect(() =>
        this.actions$.pipe(
            ofType(SettingsActions.setNotificationSetting),
            switchMap(({ value }) =>
                this.storage.set(LOCAL_STORAGE_NOTIFICATIONS_SETTINGS, value).pipe(
                    map(() => SettingsActions.setNotificationSettingSuccess({ value })),
                    catchError(error => of(SettingsActions.setNotificationSettingFailed({ error }))),
                ),
            ),
        ),
    );
}
