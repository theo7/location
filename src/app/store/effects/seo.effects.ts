import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { ROUTER_NAVIGATED, ROUTER_NAVIGATION } from '@ngrx/router-store';
import { switchMap, tap } from 'rxjs/operators';
import { filterDefined } from 'src/app/shared/functions/operators.functions';
import { SeoService } from '../../shared/services/seo.service';
import { RepositoriesSelectors } from '../services/repositories-selectors.service';
import { RouterSelectors } from '../services/router-selectors.service';

@Injectable()
export class SeoEffects {
    constructor(
        private readonly actions$: Actions,
        private readonly routerSelectors: RouterSelectors,
        private readonly repositoriesSelectors: RepositoriesSelectors,
        private readonly seo: SeoService,
    ) {}

    resetSeoTags$ = createEffect(
        () =>
            this.actions$.pipe(
                ofType(ROUTER_NAVIGATION),
                tap(() => this.seo.reset()),
            ),
        { dispatch: false },
    );

    setBrandPageSeoTags$ = createEffect(
        () =>
            this.actions$.pipe(
                ofType(ROUTER_NAVIGATED),
                switchMap(() => this.routerSelectors.brandSlug$),
                filterDefined(),
                switchMap(slug => this.repositoriesSelectors.brandBySlug$(slug)),
                filterDefined(),
                tap(brand => this.seo.configureBrandPage(brand)),
            ),
        { dispatch: false },
    );
}
