import { TestBed } from '@angular/core/testing';
import { Actions } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { cold, hot } from 'jest-marbles';
import { Observable } from 'rxjs';
import { Page, Product } from 'src/app/shared/models/models';
import { ProductsService } from 'src/app/shared/services/products.service';
import * as WishlistActions from '../actions/wishlist.actions';
import { WishlistSelectors } from '../services/wishlist-selectors.service';
import { WishlistEffects } from './wishlist.effects';

describe('wishlist.effects', () => {
    let actions$: Observable<Actions>;
    let productsService: ProductsService;
    let wishlistSelectors: WishlistSelectors;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [
                WishlistEffects,
                provideMockActions(() => actions$),
                {
                    provide: ProductsService,
                    useValue: {
                        getWishlist: jest.fn(),
                    },
                },
                {
                    provide: WishlistSelectors,
                    useValue: {},
                },
            ],
        });

        productsService = TestBed.inject(ProductsService);
        wishlistSelectors = TestBed.inject(WishlistSelectors);
    });

    describe('load$', () => {
        const action = WishlistActions.load();

        it('should load wishlist successfully', () => {
            const productsPages = {
                content: [] as Product[],
                totalElements: 14,
            } as Page<Product>;

            actions$ = hot('-a-', { a: action });

            jest.spyOn(productsService, 'getWishlist').mockReturnValue(cold('-(p|)', { p: productsPages }));

            const effects = TestBed.inject(WishlistEffects);

            const loadSuccess = WishlistActions.loadSuccess({ productsPages });

            const expected = cold('--s', { s: loadSuccess });
            expect(effects.load$).toBeObservable(expected);
        });

        it('should failed to load wishlist', () => {
            const error = 'Cannot load wishlist...';

            actions$ = hot('-a-', { a: action });

            jest.spyOn(productsService, 'getWishlist').mockReturnValue(cold('-#', undefined, error));

            const effects = TestBed.inject(WishlistEffects);

            const loadError = WishlistActions.loadError({ error });

            const expected = cold('--e', { e: loadError });
            expect(effects.load$).toBeObservable(expected);
        });
    });

    describe('loadMore$', () => {
        const action = WishlistActions.loadMore();

        it('should load more of wishlist successfully', () => {
            const productsPages = {
                content: [] as Product[],
                totalElements: 14,
            } as Page<Product>;

            actions$ = hot('-a--', { a: action });

            wishlistSelectors.page$ = cold('p-', { p: 0 });

            jest.spyOn(productsService, 'getWishlist').mockReturnValue(cold('-(p|)', { p: productsPages }));

            const effects = TestBed.inject(WishlistEffects);

            const loadMoreSuccess = WishlistActions.loadMoreSuccess({ productsPages });

            const expected = cold('--s', { s: loadMoreSuccess });
            expect(effects.loadMore$).toBeObservable(expected);
        });

        it('should failed to load more of wishlist', () => {
            const error = 'Cannot load more of wishlist...';

            actions$ = hot('-a--', { a: action });

            wishlistSelectors.page$ = cold('p-', { p: 0 });

            jest.spyOn(productsService, 'getWishlist').mockReturnValue(cold('-#', undefined, error));

            const effects = TestBed.inject(WishlistEffects);

            const loadMoreError = WishlistActions.loadMoreError({ error });

            const expected = cold('--e', { e: loadMoreError });
            expect(effects.loadMore$).toBeObservable(expected);
        });
    });
});
