import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { TranslateService } from '@ngx-translate/core';
import { of } from 'rxjs';
import { catchError, filter, map, switchMap, tap } from 'rxjs/operators';
import { GoogleAnalyticsService } from 'src/app/features/analytics/google-analytics.service';
import { GoogleTagService } from 'src/app/features/analytics/google-tag.service';
import {
    CookieConsentSelection,
    CookieConsentService,
} from 'src/app/features/cookie-consent/services/cookie-consent.service';
import { cookieConsentDialogComponent$ } from 'src/app/imports.dynamic';
import { filterDefined } from 'src/app/shared/functions/operators.functions';
import { DeviceService } from 'src/app/shared/services/device.service';
import { environment } from 'src/environments/environment';
import { FacebookPixelService } from '../../features/analytics/facebook-pixel.service';
import * as CookiesActions from '../actions/cookies.actions';

@Injectable()
export class CookiesEffects {
    constructor(
        private readonly actions$: Actions,
        private readonly dialog: MatDialog,
        private readonly device: DeviceService,
        private readonly cookieConsentService: CookieConsentService,
        private readonly googleAnalyticsService: GoogleAnalyticsService,
        private readonly googleTagService: GoogleTagService,
        private readonly facebookPixelService: FacebookPixelService,
        private readonly snackBar: MatSnackBar,
        private readonly translate: TranslateService,
    ) {}

    openConfigureModal$ = createEffect(() =>
        this.actions$.pipe(
            ofType(CookiesActions.openConfigureModal),
            switchMap(() => {
                const dialogDeviceParams = device =>
                    ({
                        handset: {
                            width: '100vw',
                            maxWidth: '100vw',
                            height: '100vh',
                            panelClass: 'full-screen-dialog',
                        },
                        desktop: {
                            width: '600px',
                            height: '700px',
                            panelClass: 'full-screen-rounded-dialog',
                        },
                    }[device]);

                return cookieConsentDialogComponent$.pipe(
                    switchMap(CookieConsentDialogComponent => {
                        return this.dialog
                            .open<any, any, CookieConsentSelection | undefined>(CookieConsentDialogComponent, {
                                ...dialogDeviceParams(this.device.mode),
                            })
                            .afterClosed();
                    }),
                );
            }),
            filterDefined(),
            map(selection => CookiesActions.accept({ selection })),
        ),
    );

    acceptAll$ = createEffect(
        () =>
            this.actions$.pipe(
                ofType(CookiesActions.acceptAll),
                tap(() => {
                    this.cookieConsentService.acceptAll();
                }),
            ),
        { dispatch: false },
    );

    accept$ = createEffect(() =>
        this.actions$.pipe(
            ofType(CookiesActions.accept),
            tap(({ selection }) => {
                this.cookieConsentService.accept(selection);
            }),
            map(() => CookiesActions.acceptSuccess()),
            catchError(error => of(CookiesActions.acceptError({ error }))),
        ),
    );

    acceptSuccess$ = createEffect(
        () =>
            this.actions$.pipe(
                ofType(CookiesActions.acceptSuccess),
                switchMap(() => this.translate.get('profile.cookies.success')),
                tap(message => {
                    this.snackBar.open(message);
                }),
            ),
        { dispatch: false },
    );
    acceptError$ = createEffect(
        () =>
            this.actions$.pipe(
                ofType(CookiesActions.acceptError),
                switchMap(() => this.translate.get('profile.cookies.error')),
                tap(message => {
                    this.snackBar.open(message);
                }),
            ),
        { dispatch: false },
    );

    denyAll$ = createEffect(
        () =>
            this.actions$.pipe(
                ofType(CookiesActions.denyAll),
                tap(() => {
                    this.cookieConsentService.denyAll();
                }),
            ),
        { dispatch: false },
    );

    consentForPerformance$ = createEffect(
        () =>
            this.cookieConsentService.hasConsentedFor$('performance').pipe(
                filter(hasConsented => !!hasConsented),
                tap(() => {
                    // initialize a tracker for google analytics
                    this.googleAnalyticsService.startTrackerWithId(environment.googleAnalyticsUID);
                    this.facebookPixelService.activateTracker();
                    this.googleTagService.startTrackerWithIds(environment.gtags);
                    this.googleTagService.trackVisit();
                }),
            ),
        { dispatch: false },
    );

    deleteAdsCookies$ = createEffect(
        () =>
            this.cookieConsentService.hasConsentedFor$('ads').pipe(
                filter(hasConsented => !hasConsented),
                tap(() => {
                    this.cookieConsentService.deleteAdsCookies();
                    this.facebookPixelService.stopTracker();
                }),
            ),
        { dispatch: false },
    );

    deleteServicesCookies$ = createEffect(
        () =>
            this.cookieConsentService.hasConsentedFor$('service').pipe(
                filter(hasConsented => !hasConsented),
                tap(() => {
                    this.cookieConsentService.deleteServicesCookies();
                }),
            ),
        { dispatch: false },
    );

    deletePerformanceCookies$ = createEffect(
        () =>
            this.cookieConsentService.hasConsentedFor$('performance').pipe(
                filter(hasConsented => !hasConsented),
                tap(() => {
                    this.cookieConsentService.deletePerformanceCookies();
                }),
            ),
        { dispatch: false },
    );
}
