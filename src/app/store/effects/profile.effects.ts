import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { ROUTER_NAVIGATED } from '@ngrx/router-store';
import { Action } from '@ngrx/store';
import { isEqual } from 'lodash-es';
import { combineLatest, EMPTY, forkJoin, of } from 'rxjs';
import {
    catchError,
    distinctUntilChanged,
    exhaustMap,
    filter,
    first,
    map,
    skipUntil,
    switchMap,
    take,
    tap,
    withLatestFrom,
} from 'rxjs/operators';
import { FollowService } from 'src/app/shared/services/follow.service';
import { UserService } from 'src/app/shared/services/user.service';
import { deleteAccountDialogComponent$ } from '../../imports.dynamic';
import { convertParamsToFilter } from '../../shared/functions/product-filter.functions';
import { FilterType, SearchRouterParams } from '../../shared/models/models';
import { DeviceService } from '../../shared/services/device.service';
import { SeoService } from '../../shared/services/seo.service';
import { filtersActions } from '../actions/filters.actions';
import * as ProductsActions from '../actions/products.actions';
import { profileActions } from '../actions/profile.actions';
import * as UserActions from '../actions/user.actions';
import { CategoriesSelectors } from '../services/categories-selectors.service';
import { FilterSelectors } from '../services/filter-selectors.service';
import { ProfileSelectors } from '../services/profile-selectors.service';
import { RepositoriesSelectors } from '../services/repositories-selectors.service';
import { RouterSelectors } from '../services/router-selectors.service';
import { UserProfileSelectors } from '../services/user-selectors.service';

@Injectable()
export class ProfileEffects {
    private readonly filterKey: FilterType = 'dressing';

    constructor(
        private readonly actions$: Actions,
        private readonly router: Router,
        private readonly dialog: MatDialog,
        private readonly userService: UserService,
        private readonly filterSelectors: FilterSelectors,
        private readonly profileSelectors: ProfileSelectors,
        private readonly routerSelectors: RouterSelectors,
        private readonly deviceService: DeviceService,
        private readonly categoriesSelectors: CategoriesSelectors,
        private readonly repositoriesSelectors: RepositoriesSelectors,
        private readonly userProfileSelectors: UserProfileSelectors,
        private readonly seoService: SeoService,
        private readonly followService: FollowService,
    ) {}

    listenForFirstUrlChange$ = createEffect(() =>
        this.routerSelectors.url$.pipe(
            withLatestFrom(this.routerSelectors.isDressingPageVisible$),
            filter(([_, isDressingPageVisible]) => isDressingPageVisible),
            map(([url]) => url),
            switchMap(() =>
                forkJoin(
                    this.routerSelectors.pageParams$.pipe<SearchRouterParams>(first()),
                    this.categoriesSelectors.allCategories$.pipe(first()),
                    this.repositoriesSelectors.brands$.pipe(first()),
                    this.repositoriesSelectors.colors$.pipe(first()),
                    this.repositoriesSelectors.conditions$.pipe(first()),
                    this.categoriesSelectors.allSizes$.pipe(first()),
                ),
            ),
            switchMap(([params, categories, brands, colors, conditions, sizes]) => {
                const newFilter = convertParamsToFilter(params, categories, brands, colors, conditions, sizes);
                return of(filtersActions.set({ key: this.filterKey, filter: newFilter }));
            }),
        ),
    );

    changeDressingWhenFilterChangedAndProfileActive$ = createEffect(() =>
        this.actions$.pipe(
            ofType(filtersActions.set),
            filter(payload => payload.key === this.filterKey),
            withLatestFrom(this.routerSelectors.isDressingPageVisible$, this.routerSelectors.userId$),
            filter(([_, isDressingPageVisible]) => isDressingPageVisible),
            map(([_, __, id]) => id),
            withLatestFrom(this.profileSelectors.current$, this.userProfileSelectors.userProfile$),
            switchMap(([userId, current, currentUser]) => {
                const actions: Action[] = [];

                const expectedUserId = userId || currentUser?.id;

                if (!expectedUserId) {
                    return actions; // how should it happen?
                }

                const isOwnDressing = currentUser?.id === expectedUserId;

                if (current?.id !== expectedUserId) {
                    actions.push(profileActions.load({ userId: expectedUserId }));
                }

                if (isOwnDressing) {
                    actions.push(ProductsActions.getOwnDressing());
                    return actions;
                }

                actions.push(ProductsActions.getDressing({ userId: expectedUserId }));
                return actions;
            }),
        ),
    );

    openDeleteAccountModal$ = createEffect(() =>
        this.actions$.pipe(
            ofType(profileActions.openDeleteAccountModal),
            exhaustMap(() => {
                return deleteAccountDialogComponent$.pipe(
                    switchMap(DeleteAccountDialogComponent => {
                        return this.dialog
                            .open(DeleteAccountDialogComponent, {
                                width: '400px',
                            })
                            .afterClosed();
                    }),
                    filter(e => !!e),
                    map(() => profileActions.deleteAccount()),
                );
            }),
        ),
    );

    deleteAccount$ = createEffect(() =>
        this.actions$.pipe(
            ofType(profileActions.deleteAccount),
            switchMap(() =>
                this.userService.deleteOwnProfile().pipe(
                    map(() => profileActions.deleteAccountSuccess()),
                    catchError(error => of(profileActions.deleteAccountError({ error }))),
                ),
            ),
        ),
    );

    deleteAccountSuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(profileActions.deleteAccountSuccess),
            map(() => UserActions.logout()),
        ),
    );

    load$ = createEffect(() =>
        this.actions$.pipe(
            ofType(profileActions.load),
            withLatestFrom(this.userProfileSelectors.isLogged$),
            switchMap(([payload, isLogged]) => {
                return this.userService.getProfile(payload.userId).pipe(
                    switchMap(user => {
                        let followingSource = isLogged ? this.followService.isFollowing(user.id!) : of(null);
                        return forkJoin([
                            of(user),
                            this.userService.findRatingSummary(user.id!),
                            this.userService.getFollowingSummary(user.id!),
                            followingSource,
                        ]);
                    }),
                    map(([user, ratingSummary, followingSummary, isFollowing]) => ({
                        ...user,
                        rate: ratingSummary.averageRating,
                        totalRate: ratingSummary.totalRating,
                        totalFollowing: followingSummary.totalFollowing,
                        totalFollowers: followingSummary.totalFollower,
                        isFollowing: !!isFollowing,
                    })),
                    tap(profile => {
                        this.seoService.configureDressingPage(profile);
                    }),
                    map(profile => profileActions.loadSuccess({ profile })),
                    catchError(error => of(profileActions.loadError({ error }))),
                );
            }),
        ),
    );

    loadSuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(profileActions.loadSuccess),
            withLatestFrom(this.userProfileSelectors.userProfile$),
            map(([payload, currentUser]) => {
                if (currentUser?.id === payload.profile.id) {
                    return ProductsActions.getOwnDressing();
                }

                return ProductsActions.getDressing({ userId: payload.profile.id! });
            }),
        ),
    );

    navigateWhenFilterChangedOnDesktop$ = createEffect(
        () =>
            combineLatest([this.routerSelectors.isUserProfileRouteActive$, this.routerSelectors.url$]).pipe(
                switchMap(([isProfileEnabled]) => {
                    if (isProfileEnabled) {
                        return this.filterSelectors.currentRouterParams$(this.filterKey).pipe(
                            distinctUntilChanged((a, b) => isEqual(a, b)),
                            withLatestFrom(this.deviceService.isDesktop$),
                            filter(([_, isDesktop]) => isDesktop),
                            map(([queryParams]) => queryParams),
                            skipUntil(this.actions$.pipe(ofType(ROUTER_NAVIGATED))), // 💡 : prevent infinite loop
                            take(1),
                        );
                    }
                    return EMPTY;
                }),
                withLatestFrom(this.routerSelectors.userId$),
                tap(([queryParams, userId]) => {
                    this.router.navigate(['profile', userId], { queryParams, replaceUrl: true });
                }),
            ),
        { dispatch: false },
    );
}
