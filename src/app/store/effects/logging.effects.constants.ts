import { interval } from 'rxjs';

export const LOCAL_STORAGE_LOGGING = 'location.logging';
export const SEND_ERRORS_INTERVAL = 30 * 1000;

export const loggingInterval = interval;
