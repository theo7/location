import { TestBed } from '@angular/core/testing';
import { Actions } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { cold, hot } from 'jest-marbles';
import { Observable } from 'rxjs';
import { Page, Product } from 'src/app/shared/models/models';
import { ProductsService } from 'src/app/shared/services/products.service';
import { SuggestionSelectors } from 'src/app/store/services/suggestion-selectors.service';
import * as SuggestionActions from '../actions/suggestion.actions';
import { SuggestionEffects } from './suggestion.effects';

describe('suggestions.effects', () => {
    let actions$: Observable<Actions>;
    let productsService: ProductsService;
    let suggestionSelectors: SuggestionSelectors;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [
                SuggestionEffects,
                provideMockActions(() => actions$),
                {
                    provide: ProductsService,
                    useValue: {
                        getSuggestions: jest.fn(),
                    },
                },
                {
                    provide: SuggestionSelectors,
                    useValue: {},
                },
            ],
        });

        productsService = TestBed.inject(ProductsService);
        suggestionSelectors = TestBed.inject(SuggestionSelectors);
    });

    describe('load$', () => {
        const action = SuggestionActions.loadSuggestion();

        it('should load suggestion list successfully', () => {
            const page = {
                content: [] as Product[],
                totalElements: 5,
            } as Page<Product>;

            actions$ = hot('-a-', { a: action });

            jest.spyOn(productsService, 'getSuggestions').mockReturnValue(cold('-(p|)', { p: page }));

            const effects = TestBed.inject(SuggestionEffects);

            const loadSuggestionSuccess = SuggestionActions.loadSuggestionSuccess({ page });

            const expected = cold('--s', { s: loadSuggestionSuccess });
            expect(effects.load$).toBeObservable(expected);
        });

        it('should failed to load suggestion list', () => {
            const error = 'Cannot load suggestion list...';

            actions$ = hot('-a-', { a: action });

            jest.spyOn(productsService, 'getSuggestions').mockReturnValue(cold('-#', undefined, error));

            const effects = TestBed.inject(SuggestionEffects);

            const loadError = SuggestionActions.loadSuggestionError({ error });

            const expected = cold('--e', { e: loadError });
            expect(effects.load$).toBeObservable(expected);
        });
    });

    describe('loadMore$', () => {
        const action = SuggestionActions.loadMoreSuggestion();

        it('should load more suggestion successfully', () => {
            const page = {
                content: [] as Product[],
                totalElements: 10,
            } as Page<Product>;

            actions$ = hot('-a--', { a: action });

            suggestionSelectors.page$ = cold('p-', { p: 0 });

            jest.spyOn(productsService, 'getSuggestions').mockReturnValue(cold('-(p|)', { p: page }));

            const effects = TestBed.inject(SuggestionEffects);

            const loadMoreSuccess = SuggestionActions.loadMoreSuggestionSuccess({ page });

            const expected = cold('--s', { s: loadMoreSuccess });
            expect(effects.loadMore$).toBeObservable(expected);
        });

        it('should failed to load more suggestion', () => {
            const error = 'Cannot load more suggestion...';

            actions$ = hot('-a--', { a: action });

            suggestionSelectors.page$ = cold('p-', { p: 0 });

            jest.spyOn(productsService, 'getSuggestions').mockReturnValue(cold('-#', undefined, error));

            const effects = TestBed.inject(SuggestionEffects);

            const loadMoreError = SuggestionActions.loadMoreSuggestionError({ error });

            const expected = cold('--e', { e: loadMoreError });
            expect(effects.loadMore$).toBeObservable(expected);
        });
    });
});
