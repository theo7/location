import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { createEffect } from '@ngrx/effects';
import { fromEvent } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, map, startWith, tap } from 'rxjs/operators';
import { DeviceService } from 'src/app/shared/services/device.service';

@Injectable()
export class UiEffects {
    constructor(private readonly dialog: MatDialog, private readonly device: DeviceService) {}

    // 🍎 : set "vh" css variable to add ios_safari workaround to full-height management
    // https://css-tricks.com/the-trick-to-viewport-units-on-mobile/
    setVh$ = createEffect(
        () =>
            fromEvent(window, 'resize').pipe(
                debounceTime(100),
                startWith('start'),
                map(() => window.innerHeight * 0.01),
                distinctUntilChanged(),
                tap(vh => {
                    document.documentElement.style.setProperty('--vh', `${vh}px`);
                }),
            ),
        { dispatch: false },
    );

    // 🍎 : remove scroll on document page when a modal is displayed
    // https://css-tricks.com/prevent-page-scrolling-when-a-modal-is-open/
    removePageScrollWhenModalOpened$ = createEffect(
        () =>
            this.dialog.afterOpened.pipe(
                filter(() => this.device.mode === 'handset'),
                tap(() => {
                    document.body.style.position = 'fixed';
                    document.body.style.top = `-${window.scrollY}px`;
                }),
            ),
        { dispatch: false },
    );

    // 🍎 : reset scroll on document page when a modal is closed
    // https://css-tricks.com/prevent-page-scrolling-when-a-modal-is-open/
    resetPageScrollWhenModalClosed$ = createEffect(
        () =>
            this.dialog.afterAllClosed.pipe(
                tap(() => {
                    const scrollY = document.body.style.top;
                    document.body.style.position = '';
                    document.body.style.top = '';
                    window.scrollTo(0, parseInt(scrollY || '0', 0) * -1);
                }),
            ),
        { dispatch: false },
    );
}
