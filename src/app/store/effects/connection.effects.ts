import { Injectable } from '@angular/core';
import { createEffect } from '@ngrx/effects';
import { merge } from 'rxjs';
import { map, mapTo } from 'rxjs/operators';
import { ConnectionService } from 'src/app/shared/services/connection.service';
import * as ConnectionActions from '../actions/connection.actions';

@Injectable()
export class ConnectionEffects {
    constructor(private readonly connectionService: ConnectionService) {}

    init$ = createEffect(() =>
        merge(
            this.connectionService.online$().pipe(mapTo(true)),
            this.connectionService.offline$().pipe(mapTo(false)),
        ).pipe(map(isOnline => ConnectionActions.setConnection({ connectionState: isOnline ? 'online' : 'offline' }))),
    );
}
