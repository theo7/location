import { TestBed } from '@angular/core/testing';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { Actions, ROOT_EFFECTS_INIT } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { createAction } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { cold, hot } from 'jest-marbles';
import { Observable, of } from 'rxjs';
import { SearchHistoryDialogComponent } from '../../shared/components/product-filter/search-history-dialog/search-history-dialog.component';
import { CreateSearch, FilterType, ProductFilter, Search } from '../../shared/models/models';
import { DeviceService } from '../../shared/services/device.service';
import { UserService } from '../../shared/services/user.service';
import { filtersActions } from '../actions/filters.actions';
import * as SearchActions from '../actions/search.actions';
import * as UserActions from '../actions/user.actions';
import { FilterSelectors } from '../services/filter-selectors.service';
import { RouterSelectors } from '../services/router-selectors.service';
import { SearchDispatchers } from '../services/search-dispatchers.service';
import { UserProfileSelectors } from '../services/user-selectors.service';
import { FilterEffects, FILTER_DIALOG_ID } from './filter.effects';

describe('filter.effects', () => {
    let effects: FilterEffects;
    let actions$: Observable<Actions>;
    let router: Router;
    let route: ActivatedRoute;
    let dialog: MatDialog;
    let filterSelectors: FilterSelectors;
    let deviceService: DeviceService;
    let userProfileSelectors: UserProfileSelectors;
    let userService: UserService;
    let translateService: TranslateService;
    let snackBar: MatSnackBar;
    let routerSelectors: RouterSelectors;
    let searchDispatchers: SearchDispatchers;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                FilterEffects,
                provideMockActions(() => actions$),
                {
                    provide: UserProfileSelectors,
                    useValue: {},
                },
                {
                    provide: MatDialog,
                    useValue: {
                        open: jest.fn(),
                        getDialogById: jest.fn(),
                    },
                },
                {
                    provide: FilterSelectors,
                    useValue: {
                        isSearchAlreadyInHistory$: jest.fn(),
                        current$: jest.fn(),
                        currentRouterParams$: jest.fn(),
                    },
                },
                {
                    provide: DeviceService,
                    useValue: {
                        mode: 'handset',
                    },
                },
                {
                    provide: UserService,
                    useValue: {
                        getSearchHistory: jest.fn(),
                        addSearchHistory: jest.fn(),
                        updateSearch: jest.fn(),
                    },
                },
                {
                    provide: TranslateService,
                    useValue: {
                        get: jest.fn(),
                    },
                },
                {
                    provide: MatSnackBar,
                    useValue: {
                        openFromComponent: jest.fn(),
                    },
                },
                {
                    provide: RouterSelectors,
                    useValue: {},
                },
                {
                    provide: Router,
                    useValue: {
                        navigate: jest.fn(),
                    },
                },
                {
                    provide: ActivatedRoute,
                    useValue: {},
                },
                {
                    provide: SearchDispatchers,
                    useValue: {},
                },
            ],
        });

        userProfileSelectors = TestBed.inject(UserProfileSelectors);
        deviceService = TestBed.inject(DeviceService);
        filterSelectors = TestBed.inject(FilterSelectors);
        dialog = TestBed.inject(MatDialog);
        userService = TestBed.inject(UserService);
        translateService = TestBed.inject(TranslateService);
        snackBar = TestBed.inject(MatSnackBar);
        routerSelectors = TestBed.inject(RouterSelectors);
        router = TestBed.inject(Router);
        route = TestBed.inject(ActivatedRoute);
        searchDispatchers = TestBed.inject(SearchDispatchers);

        routerSelectors.url$ = cold('-');
        routerSelectors.isSearchEnabled$ = cold('-');
    });

    describe('init$', () => {
        it('should dispatch load search history action if user is logged', () => {
            // mock
            userProfileSelectors.isLogged$ = cold('t', { t: true });

            // call
            actions$ = hot('-a', { a: createAction(ROOT_EFFECTS_INIT) });

            // create effect instance
            effects = TestBed.inject(FilterEffects);

            // expect
            expect(effects.init$).toBeObservable(
                cold('-a', {
                    a: filtersActions.loadSearchHistory(),
                }),
            );
        });

        it('should do nothing if user is not logged', () => {
            // mock
            userProfileSelectors.isLogged$ = cold('f', { f: false });

            // call
            actions$ = hot('-a', { a: createAction(ROOT_EFFECTS_INIT) });

            // create effect instance
            effects = TestBed.inject(FilterEffects);

            // expect
            expect(effects.init$).toBeObservable(cold('--'));
        });
    });

    describe('onLoginSuccess$', () => {
        it('should dispatch load search history action', () => {
            // call
            actions$ = hot('-a', { a: UserActions.loginSuccess() });

            // create effect instance
            effects = TestBed.inject(FilterEffects);

            // expect
            expect(effects.onLoginSuccess$).toBeObservable(
                cold('-a', {
                    a: filtersActions.loadSearchHistory(),
                }),
            );
        });
    });

    describe('onLogoutSuccess$', () => {
        it('should dispatch reset search history action', () => {
            // call
            actions$ = hot('-a', { a: UserActions.logoutSuccess() });

            // create effect instance
            effects = TestBed.inject(FilterEffects);

            // expect
            expect(effects.onLogoutSuccess$).toBeObservable(
                cold('-a', {
                    a: filtersActions.resetSearchHistory(),
                }),
            );
        });
    });

    describe('search$', () => {
        it('should navigate to current route with new params', () => {
            // given
            const key: FilterType = 'search';
            const queryParams = { searchText: 'test', category: 1 };

            // mock
            jest.spyOn(filterSelectors, 'currentRouterParams$').mockReturnValue(cold('-q', { q: queryParams }));
            jest.spyOn(router, 'navigate').mockReturnValue(cold('-t', { t: true }) as any as Promise<boolean>);
            jest.spyOn(dialog, 'getDialogById').mockImplementation(id => {
                return {
                    close: () => {},
                } as MatDialogRef<any>;
            });

            // call
            actions$ = hot('-a', { a: filtersActions.search({ key }) });

            // create effect instance
            effects = TestBed.inject(FilterEffects);

            // expect
            expect(effects.search$).toSatisfyOnFlush(() => {
                expect(router.navigate).toHaveBeenNthCalledWith(1, [], {
                    relativeTo: expect.anything(),
                    queryParamsHandling: 'merge',
                    queryParams,
                });
                expect(dialog.getDialogById).toHaveBeenNthCalledWith(1, FILTER_DIALOG_ID);
            });
        });
    });

    describe('disableRefresherOutsideSearch$', () => {
        it('should activate refresher when search enabled', () => {
            // given
            const action = SearchActions.setActivateRefresher({ activateRefresher: true });

            // mock
            routerSelectors.isSearchEnabled$ = hot('-t', { t: true });

            // create effect instance
            effects = TestBed.inject(FilterEffects);

            // expect
            expect(effects.disableRefresherOutsideSearch$).toBeObservable(cold('-a', { a: action }));
        });

        it('should deactivate refresher when search enabled', () => {
            // given
            const action = SearchActions.setActivateRefresher({ activateRefresher: false });

            // mock
            routerSelectors.isSearchEnabled$ = hot('-f', { f: false });

            // create effect instance
            effects = TestBed.inject(FilterEffects);

            // expect
            expect(effects.disableRefresherOutsideSearch$).toBeObservable(cold('-a', { a: action }));
        });
    });

    describe('openSearchHistoryDialog$', () => {
        it('should open SearchHistoryDialogComponent', () => {
            // mock
            jest.spyOn(dialog, 'open').mockImplementation(component => {
                return {
                    afterClosed: () => of(undefined),
                } as MatDialogRef<typeof component>;
            });

            // call
            actions$ = hot('-a', { a: filtersActions.openSearchHistoryDialog() });

            // create effect instance
            effects = TestBed.inject(FilterEffects);

            // expect
            expect(effects.openSearchHistoryDialog$).toSatisfyOnFlush(() => {
                expect(dialog.open).toHaveBeenCalledTimes(1);
                expect(dialog.open).toHaveBeenCalledWith(SearchHistoryDialogComponent, expect.anything());
            });
        });
    });

    describe('filterChangedOnMobile$', () => {
        // given
        const key: FilterType = 'search';
        let filter: ProductFilter;

        beforeEach(() => {
            filter = { searchText: 'test', category: { id: 1 }, colors: [{ id: 1 }, { id: 2 }] } as ProductFilter;
        });

        it('should do nothing on desktop', () => {
            // mock
            deviceService.isHandset$ = cold('f', { f: false });
            userProfileSelectors.isLogged$ = cold('t', { t: true });

            // call
            actions$ = hot('-a', { a: filtersActions.set({ key, filter }) });

            // create effect instance
            effects = TestBed.inject(FilterEffects);

            // expect
            expect(effects.filterChangedOnMobile$).toBeObservable(cold('--'));
        });

        it('should do nothing when user is not logged', () => {
            // mock
            deviceService.isHandset$ = cold('t', { t: true });
            userProfileSelectors.isLogged$ = cold('f', { f: false });

            // call
            actions$ = hot('-a', { a: filtersActions.set({ key, filter }) });

            // create effect instance
            effects = TestBed.inject(FilterEffects);

            // expect
            expect(effects.filterChangedOnMobile$).toBeObservable(cold('--'));
        });

        it('should dispatch add current search action', () => {
            // mock
            deviceService.isHandset$ = cold('t', { t: true });
            userProfileSelectors.isLogged$ = cold('t', { t: true });

            // call
            actions$ = hot('-a', { a: filtersActions.set({ key, filter }) });

            // create effect instance
            effects = TestBed.inject(FilterEffects);

            // expect
            expect(effects.filterChangedOnMobile$).toBeObservable(
                cold('-a', {
                    a: filtersActions.addCurrentSearch(),
                }),
            );
        });
    });

    describe('filterChangedOnDesktop$', () => {
        // given
        const key: FilterType = 'search';
        let filter: ProductFilter;

        beforeEach(() => {
            // given
            filter = { searchText: 'test', category: { id: 1 }, colors: [{ id: 1 }, { id: 2 }] } as ProductFilter;
        });

        it('should do nothing when navigate from other route to search route', () => {
            // mock
            deviceService.isDesktop$ = cold('t', { t: true });
            userProfileSelectors.isLogged$ = cold('t', { t: true });

            // call
            routerSelectors.url$ = cold('u-v-', { u: '/product/123', v: '/search' });

            // create effect instance
            effects = TestBed.inject(FilterEffects);

            // expect
            expect(effects.filterChangedOnDesktop$).toBeObservable(cold('--'));
        });

        it('should do nothing on mobile', () => {
            // mock
            deviceService.isDesktop$ = cold('f', { f: false });
            userProfileSelectors.isLogged$ = cold('t', { t: true });

            // call
            routerSelectors.url$ = cold('u-v-', { u: '/search', v: '/product/123' });

            // create effect instance
            effects = TestBed.inject(FilterEffects);

            // expect
            expect(effects.filterChangedOnDesktop$).toBeObservable(cold('--'));
        });

        it('should do nothing when user is not logged', () => {
            // mock
            deviceService.isDesktop$ = cold('t', { t: true });
            userProfileSelectors.isLogged$ = cold('f', { f: false });

            // call
            routerSelectors.url$ = cold('u-v-', { u: '/search', v: '/product/123' });

            // create effect instance
            effects = TestBed.inject(FilterEffects);

            // expect
            expect(effects.filterChangedOnDesktop$).toBeObservable(cold('--'));
        });

        it('should dispatch add current search action', () => {
            // mock
            deviceService.isDesktop$ = cold('t', { t: true });
            userProfileSelectors.isLogged$ = cold('t', { t: true });

            // call
            routerSelectors.url$ = cold('uv', { u: '/search', v: '/product/123' });

            // create effect instance
            effects = TestBed.inject(FilterEffects);

            // expect
            expect(effects.filterChangedOnDesktop$).toBeObservable(
                cold('-a', {
                    a: filtersActions.addCurrentSearch(),
                }),
            );
        });
    });

    describe('addCurrentSearch$', () => {
        // given
        const key: FilterType = 'search';
        let filter: ProductFilter;

        beforeEach(() => {
            // given
            filter = { searchText: 'test', category: { id: 1 }, colors: [{ id: 1 }, { id: 2 }] } as ProductFilter;

            // mock
            jest.spyOn(filterSelectors, 'current$').mockReturnValue(cold('f', { f: filter }));
        });

        it('should do nothing when search is only with a category', () => {
            // given
            filter = { category: { id: 1 } } as ProductFilter;

            // mock
            jest.spyOn(filterSelectors, 'current$').mockReturnValue(cold('f', { f: filter }));

            // call
            actions$ = hot('-a', { a: filtersActions.addCurrentSearch() });

            // create effect instance
            effects = TestBed.inject(FilterEffects);

            // expect
            expect(effects.addCurrentSearch$).toBeObservable(cold('--'));
        });

        it('should do nothing when search is already saved', () => {
            // mock
            jest.spyOn(filterSelectors, 'isSearchAlreadyInHistory$').mockReturnValue(cold('-t', { t: true }));

            // call
            actions$ = hot('-a', { a: filtersActions.addCurrentSearch() });

            // create effect instance
            effects = TestBed.inject(FilterEffects);

            // expect
            expect(effects.addCurrentSearch$).toBeObservable(cold('--'));
        });

        it('should dispatch add search action', () => {
            // given
            const search: CreateSearch = {
                keyword: 'test',
                categoryId: 1,
                colorIds: '1,2',
            };

            // mock
            jest.spyOn(filterSelectors, 'isSearchAlreadyInHistory$').mockReturnValue(cold('-f', { f: false }));

            // call
            actions$ = hot('-a', { a: filtersActions.addCurrentSearch() });

            // create effect instance
            effects = TestBed.inject(FilterEffects);

            // expect
            expect(effects.addCurrentSearch$).toBeObservable(
                cold('--a', {
                    a: filtersActions.addSearch({ search }),
                }),
            );
        });
    });

    describe('loadSearchHistory$', () => {
        it('should dispatch load search history success action', () => {
            // mock
            jest.spyOn(userService, 'getSearchHistory').mockReturnValue(cold('-l', { l: [] }));

            // call
            actions$ = hot('-a', { a: filtersActions.loadSearchHistory() });

            // create effect instance
            effects = TestBed.inject(FilterEffects);

            // expect
            expect(effects.loadSearchHistory$).toBeObservable(
                cold('--a', {
                    a: filtersActions.loadSearchHistorySuccess({ searchList: [] }),
                }),
            );
        });

        it('should dispatch load search history error action on error', () => {
            // mock
            jest.spyOn(userService, 'getSearchHistory').mockReturnValue(cold('-#', undefined, 'oops'));

            // call
            actions$ = hot('-a', { a: filtersActions.loadSearchHistory() });

            // create effect instance
            effects = TestBed.inject(FilterEffects);

            // expect
            expect(effects.loadSearchHistory$).toBeObservable(
                cold('--a', {
                    a: filtersActions.loadSearchHistoryError({ error: 'oops' }),
                }),
            );
        });
    });

    describe('addSearch$', () => {
        it('should dispatch add search success action', () => {
            // given
            const dto = { keyword: 'test' } as CreateSearch;
            const search = { id: 1, userId: 1, keyword: 'test' } as Search;

            // mock
            jest.spyOn(userService, 'addSearchHistory').mockReturnValue(cold('-l', { l: search }));

            // call
            actions$ = hot('-a', { a: filtersActions.addSearch({ search: dto }) });

            // create effect instance
            effects = TestBed.inject(FilterEffects);

            // expect
            expect(effects.addSearch$).toBeObservable(
                cold('--a', {
                    a: filtersActions.addSearchSuccess({ search }),
                }),
            );
        });

        it('should dispatch add search error action on error', () => {
            // given
            const dto = { keyword: 'test' } as CreateSearch;

            // mock
            jest.spyOn(userService, 'addSearchHistory').mockReturnValue(cold('-#', undefined, 'oops'));

            // call
            actions$ = hot('-a', { a: filtersActions.addSearch({ search: dto }) });

            // create effect instance
            effects = TestBed.inject(FilterEffects);

            // expect
            expect(effects.addSearch$).toBeObservable(
                cold('--a', {
                    a: filtersActions.addSearchError({ error: 'oops' }),
                }),
            );
        });
    });

    describe('saveSearch$', () => {
        it('should dispatch save search success action', () => {
            // given
            const search = { id: 1, userId: 1, keyword: 'test', saved: true } as Search;

            // mock
            jest.spyOn(userService, 'updateSearch').mockReturnValue(cold('-l', { l: search }));

            // call
            actions$ = hot('-a', { a: filtersActions.saveSearch({ id: 1 }) });

            // create effect instance
            effects = TestBed.inject(FilterEffects);

            // expect
            expect(effects.saveSearch$).toBeObservable(
                cold('--a', {
                    a: filtersActions.saveSearchSuccess({ search }),
                }),
            );
        });

        it('should dispatch save search error action on error', () => {
            // mock
            jest.spyOn(userService, 'updateSearch').mockReturnValue(cold('-#', undefined, 'oops'));

            // call
            actions$ = hot('-a', { a: filtersActions.saveSearch({ id: 1 }) });

            // create effect instance
            effects = TestBed.inject(FilterEffects);

            // expect
            expect(effects.saveSearch$).toBeObservable(
                cold('--a', {
                    a: filtersActions.saveSearchError({ error: 'oops' }),
                }),
            );
        });
    });

    describe('saveSearchSuccess$', () => {
        it('should open success notification component', () => {
            // given
            const search = { id: 1, userId: 1, keyword: 'test', saved: true } as Search;

            // mock
            jest.spyOn(translateService, 'get').mockReturnValue(cold('-t', { t: 'success' }));
            jest.spyOn(snackBar, 'openFromComponent').mockReturnValue({} as any);

            // call
            actions$ = hot('-a', { a: filtersActions.saveSearchSuccess({ search }) });

            // create effect instance
            effects = TestBed.inject(FilterEffects);

            // expect
            expect(effects.saveSearchSuccess$).toSatisfyOnFlush(() => {
                expect(snackBar.openFromComponent).toHaveBeenCalledTimes(1);
            });
        });
    });

    describe('applySearchFilter$', () => {
        // given
        const search = { keyword: 'test', categoryId: 1 } as Search;

        it('should navigate to search page when filter routes is not active', () => {
            // mock
            routerSelectors.isOneOfFilterRoutes$ = cold('f', { f: false });
            jest.spyOn(router, 'navigate').mockReturnValue(cold('-t', { t: true }) as any as Promise<boolean>);
            jest.spyOn(dialog, 'getDialogById').mockImplementation(id => {
                return {
                    close: () => {},
                } as MatDialogRef<any>;
            });

            // call
            actions$ = hot('-a', { a: filtersActions.applySearchFilter({ search }) });

            // create effect instance
            effects = TestBed.inject(FilterEffects);

            // expect
            expect(effects.applySearchFilter$).toSatisfyOnFlush(() => {
                expect(router.navigate).toHaveBeenCalledTimes(1);
                expect(router.navigate).toHaveBeenCalledWith(['search'], expect.anything());
                expect(dialog.getDialogById).toHaveBeenCalledTimes(2);
            });
        });

        it('should navigate to current route when filter routes is active', () => {
            // mock
            routerSelectors.isOneOfFilterRoutes$ = cold('t', { t: true });
            jest.spyOn(router, 'navigate').mockReturnValue(cold('-t', { t: true }) as any as Promise<boolean>);
            jest.spyOn(dialog, 'getDialogById').mockImplementation(id => {
                return {
                    close: () => {},
                } as MatDialogRef<any>;
            });

            // call
            actions$ = hot('-a', { a: filtersActions.applySearchFilter({ search }) });

            // create effect instance
            effects = TestBed.inject(FilterEffects);

            // expect
            expect(effects.applySearchFilter$).toSatisfyOnFlush(() => {
                expect(router.navigate).toHaveBeenCalledTimes(1);
                expect(router.navigate).toHaveBeenCalledWith([], expect.anything());
                expect(dialog.getDialogById).toHaveBeenCalledTimes(2);
            });
        });
    });
});
