import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType, ROOT_EFFECTS_INIT } from '@ngrx/effects';
import { TranslateService } from '@ngx-translate/core';
import { forkJoin, from, of } from 'rxjs';
import { catchError, filter, map, switchMap, tap } from 'rxjs/operators';
import { AuthenticationService } from '../../features/authentication/service/authentication.service';
import { NotificationDialogComponent } from '../../shared/components/notification-dialog/notification-dialog.component';
import { UserService } from '../../shared/services/user.service';
import * as DiscussionsActions from '../actions/discussions.actions';
import * as UserActions from '../actions/user.actions';
import { UserProfileSelectors } from '../services/user-selectors.service';

@Injectable()
export class UserEffects {
    constructor(
        private readonly actions$: Actions,
        private readonly router: Router,
        private readonly authenticationService: AuthenticationService,
        private readonly userService: UserService,
        private readonly userProfileSelectors: UserProfileSelectors,
        private readonly dialog: MatDialog,
        private readonly translateService: TranslateService,
    ) {}

    init$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ROOT_EFFECTS_INIT),
            switchMap(() => this.userProfileSelectors.isLogged$),
            filter(e => e),
            switchMap(() => [UserActions.getUserProfile(), DiscussionsActions.getUserDiscussions()]),
        ),
    );

    login$ = createEffect(() =>
        this.actions$.pipe(
            ofType(UserActions.login),
            switchMap(action =>
                from(this.authenticationService.login(action.email, action.password)).pipe(
                    map(() => UserActions.loginSuccess()),
                    catchError(error => of(UserActions.loginError({ error }))),
                ),
            ),
        ),
    );

    loginSuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(UserActions.loginSuccess),
            switchMap(() => {
                return [UserActions.getUserProfile(), DiscussionsActions.getUserDiscussions()];
            }),
        ),
    );

    logout$ = createEffect(() =>
        this.actions$.pipe(
            ofType(UserActions.logout),
            switchMap(action =>
                from(this.authenticationService.logout()).pipe(
                    map(() => UserActions.logoutSuccess()),
                    catchError(() => of(UserActions.logoutError())),
                ),
            ),
        ),
    );

    redirectOnLogout$ = createEffect(
        () =>
            this.actions$.pipe(
                ofType(UserActions.logout),
                switchMap(() => {
                    return this.router.navigate(['/']);
                }),
            ),
        { dispatch: false },
    );

    getUserProfile$ = createEffect(() =>
        this.actions$.pipe(
            ofType(UserActions.getUserProfile),
            switchMap(action =>
                this.userService.getCurrentUser().pipe(
                    switchMap(user =>
                        forkJoin([
                            of(user),
                            this.userService.findRatingSummary(user.id as number),
                            this.userService.getFollowingSummary(user.id!),
                        ]),
                    ),
                    map(([userProfile, ratingSummary, followingSummary]) => ({
                        ...userProfile,
                        rate: ratingSummary.averageRating,
                        totalRate: ratingSummary.totalRating,
                        totalFollowing: followingSummary.totalFollowing,
                        totalFollowers: followingSummary.totalFollower,
                    })),
                    map(userProfile => UserActions.getUserProfileSuccess({ userProfile })),
                    catchError(() => of(UserActions.getUserProfileError())),
                ),
            ),
        ),
    );

    updateUserProfile$ = createEffect(() =>
        this.actions$.pipe(
            ofType(UserActions.updateUserProfile),
            switchMap(action =>
                this.userService.updateProfil(action.userProfile).pipe(
                    map(() => UserActions.updateUserProfileSuccess({ userProfile: action.userProfile })),
                    catchError(error => of(UserActions.updateUserProfileError({ error }))),
                ),
            ),
        ),
    );

    updateUserProfileSuccess$ = createEffect(
        () =>
            this.actions$.pipe(
                ofType(UserActions.updateUserProfileSuccess),
                tap(() => {
                    this.router.navigate(['profile']);
                }),
            ),
        { dispatch: false },
    );

    postUserAddress$ = createEffect(() =>
        this.actions$.pipe(
            ofType(UserActions.postUserAddress),
            switchMap(action =>
                this.userService.postUserAddress(action.address).pipe(
                    map(address => UserActions.postUserAddressSuccess({ address })),
                    catchError(() => of(UserActions.postUserAddressError())),
                ),
            ),
        ),
    );

    deleteUserAddress$ = createEffect(() =>
        this.actions$.pipe(
            ofType(UserActions.deleteUserAddress),
            switchMap(action => {
                if (action.address.id) {
                    return this.userService.deleteUserAddress(action.address.id).pipe(
                        map(() => UserActions.deleteUserAddressSuccess({ address: action.address })),
                        catchError(() => of(UserActions.deleteUserAddressError())),
                    );
                }

                return of(UserActions.deleteUserAddressError());
            }),
        ),
    );

    updateUserAddress$ = createEffect(() =>
        this.actions$.pipe(
            ofType(UserActions.updateUserAddress),
            switchMap(action =>
                this.userService.editUserAddress(action.address).pipe(
                    map(address => UserActions.updateUserAddressSuccess({ address })),
                    catchError(() => of(UserActions.updateUserAddressError())),
                ),
            ),
        ),
    );

    signUp$ = createEffect(() =>
        this.actions$.pipe(
            ofType(UserActions.signUp),
            switchMap(action =>
                this.authenticationService.createUser(action.signUpData).pipe(
                    map(() => UserActions.signUpSuccess()),
                    catchError(error => of(UserActions.signUpError({ error }))),
                ),
            ),
        ),
    );

    signUpSuccess$ = createEffect(
        () =>
            this.actions$.pipe(
                ofType(UserActions.signUpSuccess),
                switchMap(() =>
                    this.dialog
                        .open(NotificationDialogComponent, {
                            panelClass: 'no-padding-dialog',
                            width: '280px',
                            data: {
                                type: 'success',
                                title: this.translateService.instant('userForm.successDialog.title'),
                                content: this.translateService.instant('userForm.successDialog.content'),
                                buttonContent: this.translateService.instant('common.continue'),
                            },
                        })
                        .afterClosed(),
                ),
                switchMap(() => {
                    return this.router.navigate(['login'], { queryParamsHandling: 'preserve' });
                }),
            ),
        { dispatch: false },
    );

    resetPassword$ = createEffect(() =>
        this.actions$.pipe(
            ofType(UserActions.resetPassword),
            switchMap(action =>
                this.authenticationService.sendResetPasswordEmail(action.email).pipe(
                    map(() => UserActions.resetPasswordSuccess()),
                    catchError(error => of(UserActions.resetPasswordError({ error }))),
                ),
            ),
        ),
    );

    changePassword$ = createEffect(() =>
        this.actions$.pipe(
            ofType(UserActions.changePassword),
            switchMap(action =>
                this.authenticationService.changePassword(action.currentPassword, action.newPassword).pipe(
                    map(() => UserActions.changePasswordSuccess()),
                    catchError(error => of(UserActions.changePasswordError({ error }))),
                ),
            ),
        ),
    );

    updateCgu$ = createEffect(() =>
        this.actions$.pipe(
            ofType(UserActions.updateCgu),
            switchMap(action =>
                this.userService.updateCguModificationDate(action.cguType).pipe(
                    map(userProfile => UserActions.updateCguSuccess({ userProfile })),
                    catchError(error => of(UserActions.updateCguError({ error }))),
                ),
            ),
        ),
    );

    blockUser$ = createEffect(() =>
        this.actions$.pipe(
            ofType(UserActions.blockUser),
            switchMap(action => {
                return this.userService.blockUser(action.userId).pipe(
                    map(() => UserActions.blockUserSuccess({ userId: action.userId })),
                    catchError(error => of(UserActions.blockUserError({ error }))),
                );
            }),
        ),
    );

    unblockUser$ = createEffect(() =>
        this.actions$.pipe(
            ofType(UserActions.unblockUser),
            switchMap(action =>
                this.userService.unblockUser(action.userId).pipe(
                    map(() => UserActions.unblockUserSuccess({ userId: action.userId })),
                    catchError(error => of(UserActions.unblockUserError({ error }))),
                ),
            ),
        ),
    );

    switchVacationMode$ = createEffect(() =>
        this.actions$.pipe(
            ofType(UserActions.switchVacationMode),
            switchMap(() =>
                this.userService.switchVacationMode().pipe(
                    map(() => UserActions.switchVacationModeSuccess()),
                    catchError(error => of(UserActions.switchVacationModeError({ error }))),
                ),
            ),
        ),
    );
}
