import { of } from '@angular-devkit/core/node_modules/rxjs';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType, ROOT_EFFECTS_INIT } from '@ngrx/effects';
import { catchError, filter, map, switchMap } from 'rxjs/operators';
import { NotificationsService } from 'src/app/shared/services/notifications.service';
import * as NotificationsActions from '../actions/notifications.actions';
import { UserProfileSelectors } from '../services/user-selectors.service';

@Injectable()
export class NotificationsHistoryEffects {
    constructor(
        private readonly actions$: Actions,
        private readonly notificationsService: NotificationsService,
        private readonly userProfileSelectors: UserProfileSelectors,
    ) {}

    init$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ROOT_EFFECTS_INIT),
            switchMap(() => this.userProfileSelectors.isLogged$),
            filter(e => e),
            switchMap(() => [NotificationsActions.loadNotificationHistory()]),
        ),
    );

    loadNotificationHistory$ = createEffect(() =>
        this.actions$.pipe(
            ofType(NotificationsActions.loadNotificationHistory),
            switchMap(() =>
                this.notificationsService.watchNotifications().pipe(
                    map(pushNotificationList =>
                        NotificationsActions.loadNotificationHistorySuccess({ pushNotificationList }),
                    ),
                    catchError(error => of(NotificationsActions.loadNotificationHistoryError({ error }))),
                ),
            ),
        ),
    );

    setNotificationRead$ = createEffect(() =>
        this.actions$.pipe(
            ofType(NotificationsActions.setNotificationRead),
            switchMap(() =>
                this.notificationsService.setNotificationRead().pipe(
                    map(() => NotificationsActions.setNotificationReadSuccess()),
                    catchError(error => of(NotificationsActions.setNotificationReadError({ error }))),
                ),
            ),
        ),
    );
}
