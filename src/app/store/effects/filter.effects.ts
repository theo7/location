import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { Actions, createEffect, ofType, ROOT_EFFECTS_INIT } from '@ngrx/effects';
import { TranslateService } from '@ngx-translate/core';
import { forkJoin, of } from 'rxjs';
import {
    catchError,
    exhaustMap,
    filter,
    first,
    map,
    mapTo,
    pairwise,
    switchMap,
    tap,
    withLatestFrom,
} from 'rxjs/operators';
import { ProductFilterDialogComponent } from '../../shared/components/product-filter/product-filter-dialog/product-filter-dialog.component';
import { SearchHistoryDialogComponent } from '../../shared/components/product-filter/search-history-dialog/search-history-dialog.component';
import { SuccessNotificationComponent } from '../../shared/components/success-notification/success-notification.component';
import {
    convertSearchToQueryParams,
    createSearchWithFilter,
    shouldSaveFilter,
} from '../../shared/functions/product-filter.functions';
import { DeviceService } from '../../shared/services/device.service';
import { UserService } from '../../shared/services/user.service';
import { filtersActions } from '../actions/filters.actions';
import * as SearchActions from '../actions/search.actions';
import * as UserActions from '../actions/user.actions';
import { FilterSelectors } from '../services/filter-selectors.service';
import { RouterSelectors } from '../services/router-selectors.service';
import { SearchDispatchers } from '../services/search-dispatchers.service';
import { UserProfileSelectors } from '../services/user-selectors.service';

export const FILTER_DIALOG_ID = 'product-filter-dialog';
export const SEARCH_HISTORY_DIALOG_ID = 'search-history-dialog';

@Injectable()
export class FilterEffects {
    constructor(
        private readonly actions$: Actions,
        private readonly router: Router,
        private readonly route: ActivatedRoute,
        private readonly dialog: MatDialog,
        private readonly filterSelectors: FilterSelectors,
        private readonly deviceService: DeviceService,
        private readonly userProfileSelectors: UserProfileSelectors,
        private readonly userService: UserService,
        private readonly translateService: TranslateService,
        private readonly snackBar: MatSnackBar,
        private readonly routerSelectors: RouterSelectors,
        private readonly searchDispatchers: SearchDispatchers,
    ) {}

    // 💡 : load search history on startup if user is logged
    init$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ROOT_EFFECTS_INIT),
            withLatestFrom(this.userProfileSelectors.isLogged$),
            filter(([_, isLogged]) => isLogged),
            mapTo(filtersActions.loadSearchHistory()),
        ),
    );

    // 💡: load search history when logging in
    onLoginSuccess$ = createEffect(() =>
        this.actions$.pipe(ofType(UserActions.loginSuccess), mapTo(filtersActions.loadSearchHistory())),
    );

    // 💡 : reset search history state when logging out
    onLogoutSuccess$ = createEffect(() =>
        this.actions$.pipe(ofType(UserActions.logoutSuccess), mapTo(filtersActions.resetSearchHistory())),
    );

    search$ = createEffect(
        () =>
            this.actions$.pipe(
                ofType(filtersActions.search),
                switchMap(({ key }) => this.filterSelectors.currentRouterParams$(key).pipe(first())),
                tap(queryParams => {
                    // navigate to current route with new query params
                    this.router.navigate([], {
                        relativeTo: this.route,
                        queryParamsHandling: 'merge',
                        queryParams,
                    });

                    // close filter dialog
                    this.dialog.getDialogById(FILTER_DIALOG_ID)?.close();
                }),
            ),
        { dispatch: false },
    );

    openFilterDialog$ = createEffect(
        () =>
            this.actions$.pipe(
                ofType(filtersActions.openDialog),
                tap(() => {
                    // deactivate search list refresher
                    this.searchDispatchers.setActivateRefresher(false);
                }),
                exhaustMap(({ key }) => {
                    return this.dialog
                        .open(ProductFilterDialogComponent, {
                            id: FILTER_DIALOG_ID,
                            width: '100vw',
                            maxWidth: '100vw',
                            height: '100vh',
                            panelClass: ['full-screen-dialog'],
                            data: {
                                key,
                            },
                        })
                        .afterClosed();
                }),
                tap(() => {
                    // reactivate search list refresher
                    this.searchDispatchers.setActivateRefresher(true);
                }),
            ),
        { dispatch: false },
    );

    disableRefresherOutsideSearch$ = createEffect(() =>
        this.routerSelectors.isSearchEnabled$.pipe(
            map(isSearchEnabled => {
                return SearchActions.setActivateRefresher({ activateRefresher: isSearchEnabled });
            }),
        ),
    );

    openSearchHistoryDialog$ = createEffect(
        () =>
            this.actions$.pipe(
                ofType(filtersActions.openSearchHistoryDialog),
                switchMap(() => {
                    const dialogDeviceParams = device =>
                        ({
                            handset: {
                                width: '100vw',
                                maxWidth: '100vw',
                                height: '100vh',
                                panelClass: 'full-screen-dialog',
                            },
                            desktop: {
                                height: '60vh',
                                width: '460px',
                                maxWidth: '460px',
                                panelClass: 'no-padding-dialog',
                            },
                        }[device]);

                    return this.dialog
                        .open(SearchHistoryDialogComponent, {
                            ...dialogDeviceParams(this.deviceService.mode),
                            id: SEARCH_HISTORY_DIALOG_ID,
                        })
                        .afterClosed();
                }),
            ),
        { dispatch: false },
    );

    // 💡 : save search on mobile when closing dialog, so when filter set action is dispatched
    filterChangedOnMobile$ = createEffect(() =>
        this.actions$.pipe(
            ofType(filtersActions.set),
            withLatestFrom(this.userProfileSelectors.isLogged$, this.deviceService.isHandset$),
            filter(([{ key }, isLogged, isHandset]) => key === 'search' && isLogged && isHandset),
            map(() => filtersActions.addCurrentSearch()),
        ),
    );

    // 💡 : save search on desktop when navigating from search route to a different one
    filterChangedOnDesktop$ = createEffect(() =>
        this.routerSelectors.url$.pipe(
            pairwise(),
            filter(([prev, curr]) => {
                return prev?.startsWith('/search') && !curr?.startsWith('/search');
            }),
            withLatestFrom(this.deviceService.isDesktop$, this.userProfileSelectors.isLogged$),
            filter(([_, isDesktop, isLogged]) => isDesktop && isLogged),
            map(() => filtersActions.addCurrentSearch()),
        ),
    );

    loadSearchHistory$ = createEffect(() =>
        this.actions$.pipe(
            ofType(filtersActions.loadSearchHistory),
            switchMap(() => {
                return this.userService.getSearchHistory().pipe(
                    map(searchList => filtersActions.loadSearchHistorySuccess({ searchList })),
                    catchError(error => of(filtersActions.loadSearchHistoryError({ error }))),
                );
            }),
        ),
    );

    // 💡 : add current search to recent
    addCurrentSearch$ = createEffect(() =>
        this.actions$.pipe(
            ofType(filtersActions.addCurrentSearch),
            switchMap(() => {
                return this.filterSelectors.current$('search').pipe(
                    first(),
                    filter(filter => shouldSaveFilter(filter)),
                    map(filter => createSearchWithFilter(filter)),
                    switchMap(search =>
                        forkJoin([of(search), this.filterSelectors.isSearchAlreadyInHistory$(search).pipe(first())]),
                    ),
                    filter(([_, isSearchAlreadyInHistory]) => !isSearchAlreadyInHistory),
                    map(([search]) => filtersActions.addSearch({ search })),
                );
            }),
        ),
    );

    // 💡 : save current search to saved
    saveCurrentSearch$ = createEffect(() =>
        this.actions$.pipe(
            ofType(filtersActions.saveCurrentSearch),
            switchMap(() => {
                return this.filterSelectors.current$('search').pipe(
                    first(),
                    map(filter => createSearchWithFilter(filter)),
                    switchMap(search =>
                        forkJoin([of(search), this.filterSelectors.getSearchInHistory$(search).pipe(first())]),
                    ),
                    switchMap(([dto, existingSearch]) => {
                        if (!existingSearch) {
                            return this.userService.addSearchHistory(dto);
                        }

                        return of(existingSearch);
                    }),
                    map(search => filtersActions.saveSearch({ id: search.id })),
                );
            }),
        ),
    );

    addSearch$ = createEffect(() =>
        this.actions$.pipe(
            ofType(filtersActions.addSearch),
            switchMap(({ search }) => {
                return this.userService.addSearchHistory(search).pipe(
                    map(search => filtersActions.addSearchSuccess({ search })),
                    catchError(error => of(filtersActions.addSearchError({ error }))),
                );
            }),
        ),
    );

    saveSearch$ = createEffect(() =>
        this.actions$.pipe(
            ofType(filtersActions.saveSearch),
            switchMap(({ id }) => {
                return this.userService.updateSearch(id, { saved: true }).pipe(
                    map(search => filtersActions.saveSearchSuccess({ search })),
                    catchError(error => of(filtersActions.saveSearchError({ error }))),
                );
            }),
        ),
    );

    saveSearchSuccess$ = createEffect(
        () =>
            this.actions$.pipe(
                ofType(filtersActions.saveSearchSuccess),
                switchMap(() => this.translateService.get('filter.searchHistory.save.success')),
                tap(message => {
                    this.snackBar.openFromComponent(SuccessNotificationComponent, {
                        verticalPosition: 'top',
                        horizontalPosition: 'end',
                        duration: 2000,
                        data: message,
                    });
                }),
            ),
        { dispatch: false },
    );

    unSaveSearch$ = createEffect(() =>
        this.actions$.pipe(
            ofType(filtersActions.unSaveSearch),
            switchMap(({ id }) => {
                return this.userService.updateSearch(id, { saved: false }).pipe(
                    map(search => filtersActions.unSaveSearchSuccess({ search })),
                    catchError(error => of(filtersActions.unSaveSearchError({ error }))),
                );
            }),
        ),
    );

    unSaveSearchSuccess$ = createEffect(
        () =>
            this.actions$.pipe(
                ofType(filtersActions.unSaveSearchSuccess),
                switchMap(() => this.translateService.get('filter.searchHistory.unSave.success')),
                tap(message => {
                    this.snackBar.openFromComponent(SuccessNotificationComponent, {
                        verticalPosition: 'top',
                        horizontalPosition: 'end',
                        duration: 2000,
                        data: message,
                    });
                }),
            ),
        { dispatch: false },
    );

    applySearchFilter$ = createEffect(
        () =>
            this.actions$.pipe(
                ofType(filtersActions.applySearchFilter),
                map(({ search }) => {
                    return convertSearchToQueryParams(search);
                }),
                withLatestFrom(this.routerSelectors.isOneOfFilterRoutes$),
                tap(([queryParams, isOneOfFilterRoutes]) => {
                    if (isOneOfFilterRoutes) {
                        // navigate to current route with new query params
                        this.router.navigate([], {
                            relativeTo: this.route,
                            queryParamsHandling: 'merge',
                            queryParams,
                        });
                    } else {
                        // navigate to search route with new query params
                        this.router.navigate(['search'], {
                            queryParamsHandling: 'merge',
                            queryParams,
                        });
                    }

                    // close filter or search history dialog
                    this.dialog.getDialogById(FILTER_DIALOG_ID)?.close();
                    this.dialog.getDialogById(SEARCH_HISTORY_DIALOG_ID)?.close();
                }),
            ),
        { dispatch: false },
    );
}
