import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { TranslateService } from '@ngx-translate/core';
import { of } from 'rxjs';
import { catchError, map, switchMap, tap } from 'rxjs/operators';
import { ContactService } from 'src/app/shared/services/contact.service';
import * as ContactActions from '../actions/contact.actions';

@Injectable()
export class ContactEffects {
    constructor(
        private readonly actions$: Actions,
        private readonly contactService: ContactService,
        private readonly translate: TranslateService,
        private readonly snackBar: MatSnackBar,
        private readonly dialog: MatDialog,
    ) {}

    sendEmailContactSupport$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ContactActions.sendEmailContactSupport),
            switchMap(payload =>
                this.contactService.sendEmailContactSupport(payload.contactSupportInfo).pipe(
                    map(() => ContactActions.sendEmailContactSupportSuccess()),
                    catchError(error => of(ContactActions.sendEmailContactSupportError({ error }))),
                ),
            ),
        ),
    );

    sendEmailContactSupportError$ = createEffect(
        () =>
            this.actions$.pipe(
                ofType(ContactActions.sendEmailContactSupportError),
                switchMap(() => this.translate.get('contact.error')),
                tap(message => {
                    this.snackBar.open(message, undefined, {
                        horizontalPosition: 'end',
                        verticalPosition: 'top',
                        duration: 1500,
                    });
                }),
            ),
        { dispatch: false },
    );
}
