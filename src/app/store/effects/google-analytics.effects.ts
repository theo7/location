import { Injectable } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { combineLatest, Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, tap } from 'rxjs/operators';
import { GoogleAnalyticsService } from '../../features/analytics/google-analytics.service';
import { CookieConsentService } from '../../features/cookie-consent/services/cookie-consent.service';
import * as GoogleAnalyticsActions from '../actions/google-analytics.actions';
import { SEARCH_TRACKING_DEBOUNCE_TIME } from './google-analytics.effects.constants';

@Injectable()
export class GoogleAnalyticsEffects {
    constructor(
        private readonly actions$: Actions,
        private readonly router: Router,
        private readonly googleAnalyticsService: GoogleAnalyticsService,
        private readonly cookieConsentService: CookieConsentService,
    ) {}

    trackSearchAndFilter$ = createEffect(
        () =>
            this.actions$.pipe(
                ofType(GoogleAnalyticsActions.trackSearchAndFilter),
                debounceTime(SEARCH_TRACKING_DEBOUNCE_TIME),
                tap(action => this.googleAnalyticsService.trackSearchAndFilter(action.searchFilter, action.results)),
            ),
        { dispatch: false },
    );

    trackView$ = createEffect(
        () =>
            combineLatest([
                this.router.events.pipe(filter(event => event instanceof NavigationEnd)) as Observable<NavigationEnd>,
                this.cookieConsentService.hasConsentedFor$('performance'),
            ]).pipe(
                filter(([_, hasConsented]) => !!hasConsented),
                distinctUntilChanged(([event1], [event2]) => event1.id === event2.id),
                tap(([event]) => this.googleAnalyticsService.trackView(event.urlAfterRedirects)),
            ),
        { dispatch: false },
    );
}
