import { TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { Actions, ROOT_EFFECTS_INIT } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { createAction } from '@ngrx/store';
import { cold, hot } from 'jest-marbles';
import { Observable } from 'rxjs';
import { NotificationsService } from 'src/app/shared/services/notifications.service';
import * as NotificationsActions from '../actions/notifications.actions';
import { UserProfileSelectors } from '../services/user-selectors.service';
import { PushNotificationList } from './../../shared/models/models';
import { NotificationsHistoryEffects } from './notifications-history.effects';

describe('notifications-history.effects', () => {
    let actions$: Observable<Actions>;
    let notificationsService: NotificationsService;
    let userProfileSelectors: UserProfileSelectors;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [
                NotificationsHistoryEffects,
                provideMockActions(() => actions$),
                {
                    provide: Router,
                    useValue: {
                        navigate: jest.fn(),
                    },
                },
                {
                    provide: NotificationsService,
                    useValue: {
                        register: jest.fn(),
                        unregister: jest.fn(),
                        isRegistered: jest.fn(),
                        watchNotifications: jest.fn(),
                        setNotificationRead: jest.fn(),
                    },
                },
                {
                    provide: UserProfileSelectors,
                    useValue: {
                        isLogged$: jest.fn(),
                    },
                },
            ],
        });

        notificationsService = TestBed.inject(NotificationsService);
        userProfileSelectors = TestBed.inject(UserProfileSelectors);
    });

    describe('loadNotificationHistory$', () => {
        const action = NotificationsActions.loadNotificationHistory();
        const pushNotificationList: PushNotificationList = {
            lastModifiedDate: 123456789,
            read: false,
            notifications: {},
        };

        it('should dispatch loadNotificationHistorySuccess', () => {
            jest.spyOn(notificationsService, 'watchNotifications').mockReturnValue(
                cold('-(a|)', { a: pushNotificationList }),
            );

            const effects = TestBed.inject(NotificationsHistoryEffects);

            actions$ = hot('-a--', { a: action });
            const expected = NotificationsActions.loadNotificationHistorySuccess({ pushNotificationList });

            expect(effects.loadNotificationHistory$).toBeObservable(cold('--e', { e: expected }));
        });

        it('should dispatch loadNotificationHistoryError', () => {
            const error = 'Cannot find notifications list...';

            jest.spyOn(notificationsService, 'watchNotifications').mockReturnValue(cold('-#', undefined, error));

            const effects = TestBed.inject(NotificationsHistoryEffects);

            actions$ = hot('-a--', { a: action });
            const expected = NotificationsActions.loadNotificationHistoryError({ error });

            expect(effects.loadNotificationHistory$).toBeObservable(cold('--e', { e: expected }));
        });
    });

    describe('init$', () => {
        const action = ROOT_EFFECTS_INIT;

        it('should dispatch loadNotificationHistory when user is logged', () => {
            // mock
            userProfileSelectors.isLogged$ = cold('f', { f: true });

            // call
            actions$ = hot('-a', { a: createAction(ROOT_EFFECTS_INIT) });

            // create effect instance
            const effects = TestBed.inject(NotificationsHistoryEffects);

            // expect
            const expected = NotificationsActions.loadNotificationHistory();
            expect(effects.init$).toBeObservable(cold('-e', { e: expected }));
        });

        it('should not dispatch any action', () => {
            userProfileSelectors.isLogged$ = cold('f', { f: false });

            const effects = TestBed.inject(NotificationsHistoryEffects);

            actions$ = hot('-a--', { a: action });

            expect(effects.init$).toBeObservable(cold('---'));
        });
    });

    describe('setNotificationRead$', () => {
        const action = NotificationsActions.setNotificationRead();

        it('should dispatch setNotificationReadSuccess', () => {
            jest.spyOn(notificationsService, 'setNotificationRead').mockReturnValue(cold('-(a|)', { a: null }));

            const effects = TestBed.inject(NotificationsHistoryEffects);

            actions$ = hot('-a--', { a: action });
            const expected = NotificationsActions.setNotificationReadSuccess();

            expect(effects.setNotificationRead$).toBeObservable(cold('--e', { e: expected }));
        });

        it('should dispatch setNotificationReadError', () => {
            const error = 'Cannot set notifications as read...';

            jest.spyOn(notificationsService, 'setNotificationRead').mockReturnValue(cold('-#', undefined, error));

            const effects = TestBed.inject(NotificationsHistoryEffects);

            actions$ = hot('-a--', { a: action });
            const expected = NotificationsActions.setNotificationReadError({ error });

            expect(effects.setNotificationRead$).toBeObservable(cold('--e', { e: expected }));
        });
    });
});
