import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { forkJoin, of } from 'rxjs';
import { catchError, map, switchMap, withLatestFrom } from 'rxjs/operators';
import { UserService } from 'src/app/shared/services/user.service';
import * as RatingsActions from '../actions/ratings.actions';
import { RatingsSelectors } from '../services/ratings-selectors.service';
import { RouterSelectors } from '../services/router-selectors.service';

@Injectable()
export class RatingsEffects {
    constructor(
        private readonly actions$: Actions,
        private readonly userService: UserService,
        private readonly ratingsSelectors: RatingsSelectors,
        private readonly routerSelectors: RouterSelectors,
    ) {}

    load$ = createEffect(() =>
        this.actions$.pipe(
            ofType(RatingsActions.load),
            withLatestFrom(this.routerSelectors.userId$),
            switchMap(([_, userId]) => {
                if (userId) {
                    return forkJoin([
                        this.userService.findRatingSummary(userId),
                        this.userService.findRating(userId, 0),
                    ]).pipe(
                        map(([summary, ratingsPages]) => RatingsActions.loadSuccess({ summary, ratingsPages })),
                        catchError(error => of(RatingsActions.loadError({ error }))),
                    );
                }

                return of(RatingsActions.loadError({ error: 'No user selected...' }));
            }),
        ),
    );

    loadMore$ = createEffect(() =>
        this.actions$.pipe(
            ofType(RatingsActions.loadMore),
            withLatestFrom(this.routerSelectors.userId$, this.ratingsSelectors.page$),
            switchMap(([, userId, pageIndex]) => {
                if (userId) {
                    return this.userService.findRating(userId, pageIndex + 1).pipe(
                        map(ratingsPages => RatingsActions.loadMoreSuccess({ ratingsPages })),
                        catchError(error => of(RatingsActions.loadMoreError({ error }))),
                    );
                }

                return of(RatingsActions.loadMoreError({ error: 'No user selected...' }));
            }),
        ),
    );

    answerRating$ = createEffect(() =>
        this.actions$.pipe(
            ofType(RatingsActions.answerRating),
            switchMap(payload => {
                return this.userService.answerRating(payload.userId, payload.rateId, payload.answer).pipe(
                    map(rating => RatingsActions.answerRatingSuccess({ rateId: payload.rateId, rating })),
                    catchError(error => of(RatingsActions.answerRatingError({ error }))),
                );
            }),
        ),
    );
}
