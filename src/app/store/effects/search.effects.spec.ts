import { TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { Actions, EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { cold, hot } from 'jest-marbles';
import { Observable } from 'rxjs';
import { Brand, ProductFilter, SearchRouterParams } from 'src/app/shared/models/models';
import { DeviceService } from '../../shared/services/device.service';
import { ProductsService } from '../../shared/services/products.service';
import { filtersActions } from '../actions/filters.actions';
import * as SearchActions from '../actions/search.actions';
import { CategoriesSelectors } from '../services/categories-selectors.service';
import { FilterSelectors } from '../services/filter-selectors.service';
import { RepositoriesSelectors } from '../services/repositories-selectors.service';
import { RouterSelectors } from '../services/router-selectors.service';
import { SearchSelectors } from '../services/search-selectors.service';
import { SearchEffects } from './search.effects';

describe('search.effects', () => {
    let actions$: Observable<Actions>;
    let routerSelectors: RouterSelectors;
    let repositoriesSelectors: RepositoriesSelectors;
    let categoriesSelectors: CategoriesSelectors;
    let searchSelectors: SearchSelectors;
    let filterSelectors: FilterSelectors;
    let router: Router;
    let device: DeviceService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [
                SearchEffects,
                provideMockActions(() => actions$),
                {
                    provide: RouterSelectors,
                    useValue: {},
                },
                {
                    provide: RepositoriesSelectors,
                    useValue: {},
                },
                {
                    provide: CategoriesSelectors,
                    useValue: {},
                },
                {
                    provide: SearchSelectors,
                    useValue: {},
                },
                {
                    provide: ProductsService,
                    useValue: {},
                },
                {
                    provide: Router,
                    useValue: { navigate: jest.fn() },
                },
                {
                    provide: DeviceService,
                    useValue: {},
                },
                {
                    provide: FilterSelectors,
                    useValue: {
                        current$: jest.fn(),
                        currentRouterParams$: jest.fn(),
                        hasFilter$: jest.fn(),
                    },
                },
            ],
        });

        routerSelectors = TestBed.inject(RouterSelectors);
        repositoriesSelectors = TestBed.inject(RepositoriesSelectors);
        categoriesSelectors = TestBed.inject(CategoriesSelectors);
        searchSelectors = TestBed.inject(SearchSelectors);
        router = TestBed.inject(Router);
        device = TestBed.inject(DeviceService);
        filterSelectors = TestBed.inject(FilterSelectors);
    });

    describe('shouldExecuteSearchChangeNavigation$', () => {
        it("['search/+', 'search/+'] => false", () => {
            routerSelectors.url$ = cold('-v--v-', { v: '/search?maxPrice=30' });
            routerSelectors.isSearchEnabled$ = cold('-t--t-', { t: true });
            jest.spyOn(filterSelectors, 'current$').mockReturnValue(cold('-f', { f: null }));

            const effects = TestBed.inject(SearchEffects);

            expect(effects.shouldExecuteSearchChangeNavigation$).toBeObservable(
                hot('f---f-', {
                    f: false,
                }),
            );
        });

        it("['search/+', '/product/4', 'search/+'] => true", () => {
            routerSelectors.url$ = cold('-v-p-v-', { v: '/search?maxPrice=30', p: '/product/4' });
            routerSelectors.isSearchEnabled$ = cold('-t-f-t-', { t: true, f: false });
            jest.spyOn(filterSelectors, 'current$').mockReturnValue(cold('-f', { f: null }));

            const effects = TestBed.inject(SearchEffects);

            expect(effects.shouldExecuteSearchChangeNavigation$).toBeObservable(
                hot('f----t-', {
                    f: false,
                    t: true,
                }),
            );
        });

        it("['search/+', '/product/4', '/product/41', '/product/4', 'search/+'] => true", () => {
            routerSelectors.url$ = cold('-v-p-p-p-v-', { v: '/search?maxPrice=30', p: '/product/4' });
            routerSelectors.isSearchEnabled$ = cold('-t-f-f-f-t-', { t: true, f: false });
            jest.spyOn(filterSelectors, 'current$').mockReturnValue(cold('-f', { f: null }));

            const effects = TestBed.inject(SearchEffects);

            expect(effects.shouldExecuteSearchChangeNavigation$).toBeObservable(
                hot('f--------t-', {
                    f: false,
                    t: true,
                }),
            );
        });

        it("['search/+', 'product/4'] => false", () => {
            routerSelectors.url$ = cold('-v-p-', { v: '/search?maxPrice=30', p: '/product/4' });
            routerSelectors.isSearchEnabled$ = cold('-t-f-', { t: true, f: false });
            jest.spyOn(filterSelectors, 'current$').mockReturnValue(cold('-f', { f: null }));

            const effects = TestBed.inject(SearchEffects);

            expect(effects.shouldExecuteSearchChangeNavigation$).toBeObservable(
                hot('f----', {
                    f: false,
                }),
            );
        });
    });

    describe('startSearchFromUrl$', () => {
        beforeEach(() => {
            routerSelectors.url$ = cold('----');
        });

        it('should filter brands using slug', () => {
            const action = SearchActions.startSearchFromUrl();
            actions$ = hot('-a-', { a: action });

            const brands = [
                {
                    id: 1,
                    slug: 'kiabi',
                },
                {
                    id: 2,
                    slug: 'mac-douglas',
                },
            ] as Brand[];

            const urlParams = {
                brands: 'kiabi',
            };

            routerSelectors.pageParams$ = cold('-p', { p: urlParams });
            categoriesSelectors.allCategories$ = cold('-c', { c: [] });
            repositoriesSelectors.brands$ = cold('-b', { b: brands });
            repositoriesSelectors.colors$ = cold('-c', { c: [] });
            repositoriesSelectors.conditions$ = cold('-c', { c: [] });
            categoriesSelectors.allSizes$ = cold('-s', { s: [] });
            jest.spyOn(filterSelectors, 'current$').mockReturnValue(cold('-f', { f: null }));

            const effects = TestBed.inject(SearchEffects);

            const filterParam = {
                brands: [
                    {
                        id: 1,
                        slug: 'kiabi',
                    },
                ],
            } as ProductFilter;
            const filterSearch = filtersActions.set({ key: 'search', filter: filterParam });

            const executeSearch = SearchActions.executeSearch();

            const expected = cold('--(bc)', { b: filterSearch, c: executeSearch });
            expect(effects.startSearchFromUrl$).toBeObservable(expected);
        });
    });

    describe('navigateWhenFilterChangedOnDesktopAndSearchEnabled$', () => {
        const executeSearch = SearchActions.executeSearch();
        const queryParams = { maxPrice: 20 } as SearchRouterParams;

        // Nominal success case
        beforeEach(() => {
            device.isDesktop$ = cold('-t', { t: true });
            routerSelectors.isSearchEnabled$ = cold('-t', { t: true });
            routerSelectors.url$ = cold('-u', '/search');
            actions$ = hot('--a--', { a: executeSearch });
            jest.spyOn(filterSelectors, 'currentRouterParams$').mockReturnValue(cold('---p', { p: queryParams }));
            jest.spyOn(filterSelectors, 'current$').mockReturnValue(cold('---f', { f: null }));
        });

        it('should not dispatch actions', () => {
            const effects = TestBed.inject(SearchEffects);
            const metadata: EffectsMetadata<SearchEffects> = getEffectsMetadata(effects);

            expect(metadata.navigateWhenFilterChangedOnDesktopAndSearchEnabled$!.dispatch).toBeFalsy();
        });

        it('should navigate to search when on desktop and filter changed', () => {
            const effects = TestBed.inject(SearchEffects);

            expect(effects.navigateWhenFilterChangedOnDesktopAndSearchEnabled$).toSatisfyOnFlush(() => {
                expect(router.navigate).toHaveBeenCalledWith(['search'], { queryParams, replaceUrl: true });
            });
        });

        it('should not navigate to search when on handset and filter changed', () => {
            device.isDesktop$ = cold('-f', { f: false });

            const effects = TestBed.inject(SearchEffects);

            expect(effects.navigateWhenFilterChangedOnDesktopAndSearchEnabled$).toSatisfyOnFlush(() => {
                expect(router.navigate).not.toHaveBeenCalled();
            });
        });

        it('should not navigate to search when on desktop but not on search enabled page and filter changed', () => {
            routerSelectors.isSearchEnabled$ = cold('-f', { f: false });

            const effects = TestBed.inject(SearchEffects);

            expect(effects.navigateWhenFilterChangedOnDesktopAndSearchEnabled$).toSatisfyOnFlush(() => {
                expect(router.navigate).not.toHaveBeenCalled();
            });
        });

        it('should not navigate to search when on desktop and filter changed but before executeSearch', () => {
            jest.spyOn(filterSelectors, 'currentRouterParams$').mockReturnValue(cold('p---', { p: queryParams }));

            const effects = TestBed.inject(SearchEffects);

            expect(effects.navigateWhenFilterChangedOnDesktopAndSearchEnabled$).toSatisfyOnFlush(() => {
                expect(router.navigate).not.toHaveBeenCalled();
            });
        });

        it('should repeat search navigate when url changed', () => {
            const queryParams1 = { maxPrice: 20 } as SearchRouterParams;
            const queryParams2 = { maxPrice: 30 } as SearchRouterParams;

            routerSelectors.url$ = cold('-u--v-', { u: '/search?maxPrice=20', v: '/search?maxPrice=30' });
            jest.spyOn(filterSelectors, 'currentRouterParams$').mockReturnValue(
                cold('--p--q-', {
                    p: queryParams1,
                    q: queryParams2,
                }),
            );
            actions$ = hot('---a--a-', { a: executeSearch });

            const effects = TestBed.inject(SearchEffects);

            expect(effects.navigateWhenFilterChangedOnDesktopAndSearchEnabled$).toSatisfyOnFlush(() => {
                expect(router.navigate).toHaveBeenCalledTimes(2);
            });
        });

        it('should not repeat search navigate on same queryParams', () => {
            jest.spyOn(filterSelectors, 'currentRouterParams$').mockReturnValue(cold('---p---p-', { p: queryParams }));

            const effects = TestBed.inject(SearchEffects);

            expect(effects.navigateWhenFilterChangedOnDesktopAndSearchEnabled$).toSatisfyOnFlush(() => {
                expect(router.navigate).toHaveBeenCalledTimes(1);
            });
        });

        it('should navigate to search when on handset then on desktop and filter changed', () => {
            device.isDesktop$ = cold('-f---t', { f: false, t: true });
            jest.spyOn(filterSelectors, 'currentRouterParams$').mockReturnValue(cold('---------p', { p: queryParams }));

            const effects = TestBed.inject(SearchEffects);

            expect(effects.navigateWhenFilterChangedOnDesktopAndSearchEnabled$).toSatisfyOnFlush(() => {
                expect(router.navigate).toHaveBeenCalledTimes(1);
            });
        });
    });
});
