import { TestBed } from '@angular/core/testing';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Actions, EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { TranslateService } from '@ngx-translate/core';
import { cold, hot } from 'jest-marbles';
import { Observable, of } from 'rxjs';
import { CookieConsentDialogComponent } from 'src/app/dialogs/cookie-consent-dialog/cookie-consent-dialog/cookie-consent-dialog.component';
import { GoogleAnalyticsService } from 'src/app/features/analytics/google-analytics.service';
import { GoogleTagService } from 'src/app/features/analytics/google-tag.service';
import {
    CookieConsentSelection,
    CookieConsentService,
} from 'src/app/features/cookie-consent/services/cookie-consent.service';
import { DeviceService } from 'src/app/shared/services/device.service';
import { FacebookPixelService } from '../../features/analytics/facebook-pixel.service';
import * as CookiesActions from '../actions/cookies.actions';
import { CookiesEffects } from './cookies.effects';

jest.mock('src/app/dialogs/cookie-consent-dialog/cookie-consent-dialog/cookie-consent-dialog.component', () => ({
    get CookieConsentDialogComponent() {
        return {};
    },
}));

jest.mock('../../imports.dynamic', () => ({
    get cookieConsentDialogComponent$() {
        return of(CookieConsentDialogComponent);
    },
}));

describe('cookies.effects', () => {
    let actions$: Observable<Actions>;
    let dialog: MatDialog;
    let cookieConsentService: CookieConsentService;
    let googleAnalyticsService: GoogleAnalyticsService;
    let googleTagService: GoogleTagService;
    let facebookPixelService: FacebookPixelService;
    let snackBar: MatSnackBar;
    let translate: TranslateService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [
                CookiesEffects,
                provideMockActions(() => actions$),
                {
                    provide: MatDialog,
                    useValue: {
                        open: jest.fn(),
                    },
                },
                {
                    provide: DeviceService,
                    useValue: {
                        mode: 'handset',
                    },
                },
                {
                    provide: CookieConsentService,
                    useValue: {
                        acceptAll: jest.fn(),
                        accept: jest.fn(),
                        denyAll: jest.fn(),
                        deleteAdsCookies: jest.fn(),
                        deleteServicesCookies: jest.fn(),
                        deletePerformanceCookies: jest.fn(),
                        hasConsentedFor$: jest.fn(),
                    },
                },
                {
                    provide: GoogleAnalyticsService,
                    useValue: {
                        startTrackerWithId: jest.fn(),
                    },
                },
                {
                    provide: GoogleTagService,
                    useValue: {
                        startTrackerWithIds: jest.fn(),
                        trackVisit: jest.fn(),
                    },
                },
                {
                    provide: FacebookPixelService,
                    useValue: {
                        initTracker: jest.fn(),
                        activateTracker: jest.fn(),
                        stopTracker: jest.fn(),
                    },
                },
                {
                    provide: MatSnackBar,
                    useValue: {
                        open: jest.fn(),
                    },
                },
                {
                    provide: TranslateService,
                    useValue: {
                        get: jest.fn(),
                    },
                },
            ],
        });

        dialog = TestBed.inject(MatDialog);
        cookieConsentService = TestBed.inject(CookieConsentService);
        googleAnalyticsService = TestBed.inject(GoogleAnalyticsService);
        googleTagService = TestBed.inject(GoogleTagService);
        facebookPixelService = TestBed.inject(FacebookPixelService);
        snackBar = TestBed.inject(MatSnackBar);
        translate = TestBed.inject(TranslateService);
    });

    beforeEach(() => {
        jest.spyOn(cookieConsentService, 'hasConsentedFor$').mockReturnValue(cold('--'));
    });

    describe('openConfigureModal$', () => {
        it('should open CookieConsentDialogComponent', () => {
            const action = CookiesActions.openConfigureModal();

            actions$ = hot('-a--', { a: action });

            const effects = TestBed.inject(CookiesEffects);

            expect(effects.openConfigureModal$).toSatisfyOnFlush(() => {
                expect(dialog.open).toHaveBeenCalledWith(CookieConsentDialogComponent, expect.anything());
            });
        });

        it('should dispatch accept action if selection', () => {
            const selection: CookieConsentSelection = {
                ads: true,
                performance: false,
                service: true,
                technical: true,
            };

            jest.spyOn(dialog, 'open').mockImplementation(component => {
                return {
                    afterClosed: () => of(selection),
                } as MatDialogRef<typeof component>;
            });

            const action = CookiesActions.openConfigureModal();

            actions$ = hot('-a--', { a: action });

            const effects = TestBed.inject(CookiesEffects);

            const accept = CookiesActions.accept({ selection });
            const expected = cold('-a--', { a: accept });

            expect(effects.openConfigureModal$).toBeObservable(expected);
        });

        it('should not dispatch accept action if no selection', () => {
            jest.spyOn(dialog, 'open').mockImplementation(component => {
                return {
                    afterClosed: () => of(undefined),
                } as MatDialogRef<typeof component>;
            });

            const action = CookiesActions.openConfigureModal();

            actions$ = hot('-a--', { a: action });

            const effects = TestBed.inject(CookiesEffects);

            const expected = cold('----');

            expect(effects.openConfigureModal$).toBeObservable(expected);
        });
    });

    describe('acceptAll$', () => {
        it('should not dispatch actions', () => {
            const effects = TestBed.inject(CookiesEffects);
            const metadata: EffectsMetadata<CookiesEffects> = getEffectsMetadata(effects);

            expect(metadata.acceptAll$!.dispatch).toBeFalsy();
        });

        it('should accept all cookies', () => {
            const action = CookiesActions.acceptAll();

            actions$ = hot('-a--', { a: action });

            const effects = TestBed.inject(CookiesEffects);

            expect(effects.acceptAll$).toSatisfyOnFlush(() => {
                expect(cookieConsentService.acceptAll).toHaveBeenCalled();
            });
        });
    });

    describe('accept$', () => {
        it('should not dispatch actions', () => {
            const effects = TestBed.inject(CookiesEffects);
            const metadata: EffectsMetadata<CookiesEffects> = getEffectsMetadata(effects);

            expect(metadata.accept$!.dispatch).toBeTruthy();
        });

        it('should accept some cookies', () => {
            const selection: CookieConsentSelection = {
                ads: true,
                performance: false,
                service: true,
                technical: true,
            };

            const action = CookiesActions.accept({ selection });

            actions$ = hot('-a--', { a: action });

            const effects = TestBed.inject(CookiesEffects);

            expect(effects.accept$).toSatisfyOnFlush(() => {
                expect(cookieConsentService.accept).toHaveBeenCalledWith(selection);
            });
        });
    });

    describe('denyAll$', () => {
        it('should not dispatch actions', () => {
            const effects = TestBed.inject(CookiesEffects);
            const metadata: EffectsMetadata<CookiesEffects> = getEffectsMetadata(effects);

            expect(metadata.denyAll$!.dispatch).toBeFalsy();
        });

        it('should deny all cookies', () => {
            const action = CookiesActions.denyAll();

            actions$ = hot('-a--', { a: action });

            const effects = TestBed.inject(CookiesEffects);

            expect(effects.denyAll$).toSatisfyOnFlush(() => {
                expect(cookieConsentService.denyAll).toHaveBeenCalled();
            });
        });
    });

    describe('consentForPerformance$', () => {
        beforeEach(() => {
            jest.spyOn(cookieConsentService, 'hasConsentedFor$').mockReturnValue(cold('-t', { t: true }));
        });

        it('should not dispatch actions', () => {
            const effects = TestBed.inject(CookiesEffects);
            const metadata: EffectsMetadata<CookiesEffects> = getEffectsMetadata(effects);

            expect(metadata.consentForPerformance$!.dispatch).toBeFalsy();
        });

        it('should start trackers', () => {
            const effects = TestBed.inject(CookiesEffects);

            expect(effects.consentForPerformance$).toSatisfyOnFlush(() => {
                expect(googleAnalyticsService.startTrackerWithId).toHaveBeenCalled();
                expect(googleTagService.startTrackerWithIds).toHaveBeenCalled();
                expect(facebookPixelService.activateTracker).toHaveBeenCalled();
            });
        });

        it('should track visit', () => {
            const effects = TestBed.inject(CookiesEffects);

            expect(effects.consentForPerformance$).toSatisfyOnFlush(() => {
                expect(googleTagService.trackVisit).toHaveBeenCalled();
            });
        });

        it('should activate Facebook Pixel tracker', () => {
            const effects = TestBed.inject(CookiesEffects);

            expect(effects.consentForPerformance$).toSatisfyOnFlush(() => {
                expect(facebookPixelService.activateTracker).toHaveBeenCalled();
            });
        });
    });

    describe('deleteAdsCookies$', () => {
        beforeEach(() => {
            jest.spyOn(cookieConsentService, 'hasConsentedFor$').mockReturnValue(cold('-f', { f: false }));
        });

        it('should not dispatch actions', () => {
            const effects = TestBed.inject(CookiesEffects);
            const metadata: EffectsMetadata<CookiesEffects> = getEffectsMetadata(effects);

            expect(metadata.deleteAdsCookies$!.dispatch).toBeFalsy();
        });

        it('should delete ads cookies', () => {
            const effects = TestBed.inject(CookiesEffects);

            expect(effects.deleteAdsCookies$).toSatisfyOnFlush(() => {
                expect(cookieConsentService.deleteAdsCookies).toHaveBeenCalled();
            });
        });

        it('should disable Facebook Pixel tracker', () => {
            const effects = TestBed.inject(CookiesEffects);

            expect(effects.deleteAdsCookies$).toSatisfyOnFlush(() => {
                expect(facebookPixelService.stopTracker).toHaveBeenCalled();
            });
        });
    });

    describe('deleteServicesCookies$', () => {
        beforeEach(() => {
            jest.spyOn(cookieConsentService, 'hasConsentedFor$').mockReturnValue(cold('-f', { f: false }));
        });

        it('should not dispatch actions', () => {
            const effects = TestBed.inject(CookiesEffects);
            const metadata: EffectsMetadata<CookiesEffects> = getEffectsMetadata(effects);

            expect(metadata.deleteServicesCookies$!.dispatch).toBeFalsy();
        });

        it('should delete services cookies', () => {
            const effects = TestBed.inject(CookiesEffects);

            expect(effects.deleteServicesCookies$).toSatisfyOnFlush(() => {
                expect(cookieConsentService.deleteServicesCookies).toHaveBeenCalled();
            });
        });
    });

    describe('deletePerformanceCookies$', () => {
        beforeEach(() => {
            jest.spyOn(cookieConsentService, 'hasConsentedFor$').mockReturnValue(cold('-f', { f: false }));
        });

        it('should not dispatch actions', () => {
            const effects = TestBed.inject(CookiesEffects);
            const metadata: EffectsMetadata<CookiesEffects> = getEffectsMetadata(effects);

            expect(metadata.deletePerformanceCookies$!.dispatch).toBeFalsy();
        });

        it('should delete performance cookies', () => {
            const effects = TestBed.inject(CookiesEffects);

            expect(effects.deletePerformanceCookies$).toSatisfyOnFlush(() => {
                expect(cookieConsentService.deletePerformanceCookies).toHaveBeenCalled();
            });
        });
    });
});
