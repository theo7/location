import { Injectable, NgZone } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Capacitor } from '@capacitor/core';
import { LocalNotifications } from '@capacitor/local-notifications';
import { PushNotifications } from '@capacitor/push-notifications';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { TranslateService } from '@ngx-translate/core';
import { BehaviorSubject, EMPTY, from, of } from 'rxjs';
import { catchError, filter, map, switchMap, tap, withLatestFrom } from 'rxjs/operators';
import { filterDefined } from 'src/app/shared/functions/operators.functions';
import { NativeNotificationService } from 'src/app/shared/services/native-notification.service';
import { NativePushService } from 'src/app/shared/services/native-push.service';
import { NotificationsService } from '../../shared/services/notifications.service';
import * as NotificationsActions from '../actions/notifications.actions';
import * as SettingsActions from '../actions/settings.actions';
import { RouterSelectors } from '../services/router-selectors.service';
import { UserProfileSelectors } from '../services/user-selectors.service';

@Injectable()
export class NativeNotificationsEffects {
    token$ = new BehaviorSubject<string | undefined>(undefined);

    constructor(
        private readonly actions$: Actions,
        private readonly notificationsService: NotificationsService,
        private readonly snackBar: MatSnackBar,
        private readonly translateService: TranslateService,
        private readonly userProfileSelectors: UserProfileSelectors,
        private readonly router: Router,
        private readonly routerSelectors: RouterSelectors,
        private readonly zone: NgZone,
        private readonly nativePushService: NativePushService,
        private readonly nativeNotificationService: NativeNotificationService,
    ) {}

    registerAtLogin$ = createEffect(() =>
        this.userProfileSelectors.isLogged$.pipe(
            filter(isLogged => isLogged),
            switchMap(() => from(PushNotifications.checkPermissions())),
            switchMap(status => {
                if (status.receive === 'granted') {
                    return of(status);
                }
                return from(PushNotifications.requestPermissions());
            }),
            filter(status => status.receive === 'granted'),
            switchMap(() => this.nativePushService.getPushNotificationToken()),
            filterDefined(),
            switchMap(token =>
                this.notificationsService.isRegistered(token).pipe(
                    filter(isRegistered => !isRegistered),
                    map(_ => {
                        if (!token) {
                            return NotificationsActions.requestPermissionFailed({ error: 'No token provided...' });
                        }
                        return NotificationsActions.requestPermissionSuccess({ token });
                    }),
                    catchError(error => of(NotificationsActions.requestPermissionFailed({ error }))),
                ),
            ),
        ),
    );

    redirectOnPushNotificationClicked$ = createEffect(
        () =>
            this.nativePushService.listenPushNotificationPerformed().pipe(
                tap(notification => {
                    this.zone.run(() => {
                        let absoluteUri: string | undefined = undefined;

                        if (Capacitor.getPlatform() === 'ios') {
                            absoluteUri = notification.data?.aps?.click_action;
                        } else if (Capacitor.getPlatform() === 'android') {
                            absoluteUri = notification.data?.click_action;
                        }

                        // get relative url from uri
                        const uri = absoluteUri ? new URL('', absoluteUri).pathname : undefined;

                        if (uri) {
                            this.router.navigate(uri.split('/'));
                        }
                    });
                }),
            ),
        { dispatch: false },
    );

    redirectOnLocalNotificationClicked$ = createEffect(
        () =>
            this.nativeNotificationService.listenLocalNotificationPerformed().pipe(
                tap(notification => {
                    this.zone.run(() => {
                        const absoluteUri = notification.extra?.data?.click_action;

                        // get relative url from uri
                        const uri = absoluteUri ? new URL('', absoluteUri).pathname : undefined;
                        if (uri) {
                            this.router.navigate(uri.split('/'));
                        }
                    });
                }),
            ),
        { dispatch: false },
    );

    displayNotificationForegroundMode$ = createEffect(
        () =>
            this.nativePushService.listenPushNotificationReceived().pipe(
                filter(() => Capacitor.getPlatform() === 'android'),
                withLatestFrom(this.routerSelectors.url$),
                tap(([notification, currentUrl]) => {
                    const absoluteUri = notification.data?.click_action;

                    // get relative url from uri
                    const uri = absoluteUri ? new URL('', absoluteUri).pathname : undefined;

                    const shouldDisplayNotification = uri !== currentUrl;

                    if (shouldDisplayNotification) {
                        LocalNotifications.schedule({
                            notifications: [
                                {
                                    id: new Date().getUTCMilliseconds(),
                                    title: notification.title || '',
                                    body: notification.body || '',
                                    smallIcon: 'res://ic_notification',
                                    extra: {
                                        data: notification.data,
                                    },
                                },
                            ],
                        });
                    }
                }),
            ),
        { dispatch: false },
    );

    requestPermission$ = createEffect(() =>
        this.actions$.pipe(
            ofType(NotificationsActions.requestPermission),
            switchMap(() =>
                this.nativePushService.getPushNotificationToken().pipe(
                    map(token => {
                        if (!token) {
                            return NotificationsActions.requestPermissionFailed({ error: 'No token found...' });
                        }
                        return NotificationsActions.requestPermissionSuccess({ token });
                    }),
                    catchError(error => of(NotificationsActions.requestPermissionFailed({ error }))),
                ),
            ),
        ),
    );

    revokePermission$ = createEffect(() =>
        this.actions$.pipe(
            ofType(NotificationsActions.revokePermission),
            switchMap(() =>
                this.nativePushService.getPushNotificationToken().pipe(
                    map(token => {
                        if (!token) {
                            return NotificationsActions.revokePermissionFailed({ error: 'No token found...' });
                        }
                        return NotificationsActions.revokePermissionSuccess({ token });
                    }),
                    catchError(error => of(NotificationsActions.revokePermissionFailed({ error }))),
                ),
            ),
        ),
    );

    register$ = createEffect(
        () =>
            this.actions$.pipe(
                ofType(NotificationsActions.requestPermissionSuccess),
                switchMap(({ token }) => this.notificationsService.register(token).pipe(catchError(() => EMPTY))),
            ),
        { dispatch: false },
    );

    unregister$ = createEffect(
        () =>
            this.actions$.pipe(
                ofType(NotificationsActions.revokePermissionSuccess),
                switchMap(({ token }) => this.notificationsService.unregister(token).pipe(catchError(() => EMPTY))),
            ),
        { dispatch: false },
    );

    displayMessageWhenRequestFailed$ = createEffect(
        () =>
            this.actions$.pipe(
                ofType(NotificationsActions.requestPermissionFailed),
                tap(() => {
                    this.snackBar.open(
                        this.translateService.instant('notifications.permissions.request.failed'),
                        undefined,
                        {
                            duration: 2500,
                        },
                    );
                }),
            ),
        { dispatch: false },
    );

    displayMessageWhenRevokeFailed$ = createEffect(
        () =>
            this.actions$.pipe(
                ofType(NotificationsActions.revokePermissionFailed),
                tap(() => {
                    this.snackBar.open(
                        this.translateService.instant('notifications.permissions.revoke.failed'),
                        undefined,
                        {
                            duration: 2500,
                        },
                    );
                }),
            ),
        { dispatch: false },
    );

    setNotificationSettingToTrue$ = createEffect(() =>
        this.actions$.pipe(
            ofType(NotificationsActions.requestPermissionSuccess),
            map(() => SettingsActions.setNotificationSetting({ value: true })),
        ),
    );

    setNotificationSettingToFalse$ = createEffect(() =>
        this.actions$.pipe(
            ofType(NotificationsActions.revokePermissionSuccess),
            map(() => SettingsActions.setNotificationSetting({ value: false })),
        ),
    );
}
