import { TestBed } from '@angular/core/testing';
import { cold } from 'jest-marbles';
import { ConnectionService } from 'src/app/shared/services/connection.service';
import * as ConnectionActions from '../actions/connection.actions';
import { ConnectionEffects } from './connection.effects';

describe('connection.effects', () => {
    let effects: ConnectionEffects;
    let connectionService: ConnectionService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [
                ConnectionEffects,
                {
                    provide: ConnectionService,
                    useValue: {
                        online$: jest.fn(),
                        offline$: jest.fn(),
                    },
                },
            ],
        });

        connectionService = TestBed.inject(ConnectionService);
    });

    describe('init$', () => {
        it('set online state if online', () => {
            jest.spyOn(connectionService, 'online$').mockReturnValue(cold('-e', { e: {} }));
            jest.spyOn(connectionService, 'offline$').mockReturnValue(cold('--'));

            effects = TestBed.inject(ConnectionEffects);

            const action = ConnectionActions.setConnection({ connectionState: 'online' });
            const expected = cold('-a-', { a: action });
            expect(effects.init$).toBeObservable(expected);
        });

        it('set offline state if offline', () => {
            jest.spyOn(connectionService, 'online$').mockReturnValue(cold('--'));
            jest.spyOn(connectionService, 'offline$').mockReturnValue(cold('-e', { e: {} }));

            effects = TestBed.inject(ConnectionEffects);

            const action = ConnectionActions.setConnection({ connectionState: 'offline' });
            const expected = cold('-a-', { a: action });
            expect(effects.init$).toBeObservable(expected);
        });

        it('set online state if going back online', () => {
            jest.spyOn(connectionService, 'online$').mockReturnValue(cold('---e', { e: {} }));
            jest.spyOn(connectionService, 'offline$').mockReturnValue(cold('-e--', { e: {} }));

            effects = TestBed.inject(ConnectionEffects);

            const actionOnline = ConnectionActions.setConnection({ connectionState: 'online' });
            const actionOffline = ConnectionActions.setConnection({ connectionState: 'offline' });
            const expected = cold('-a-b', { a: actionOffline, b: actionOnline });
            expect(effects.init$).toBeObservable(expected);
        });
    });
});
