import { TestBed } from '@angular/core/testing';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Actions, EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { TranslateService } from '@ngx-translate/core';
import { cold, hot } from 'jest-marbles';
import { Observable, of } from 'rxjs';
import {
    ModerationStatus,
    Product,
    ProductStatusEnum,
    SearchRouterParams,
    Seasonality,
    UserProfile,
} from 'src/app/shared/models/models';
import { ImageService } from 'src/app/shared/services/image.service';
import { ProductsService } from 'src/app/shared/services/products.service';
import { ShareService } from 'src/app/shared/services/share.service';
import { HomeService } from '../../pages/home/home.service';
import { ConfirmationDialogComponent } from '../../shared/components/confirmation-dialog/confirmation-dialog.component';
import { DeviceService } from '../../shared/services/device.service';
import { NavigationHistoryService } from '../../shared/services/navigation-history.service';
import { UserService } from '../../shared/services/user.service';
import { filtersActions } from '../actions/filters.actions';
import * as ProductsActions from '../actions/products.actions';
import { CategoriesSelectors } from '../services/categories-selectors.service';
import { FilterSelectors } from '../services/filter-selectors.service';
import { ProductsSelectors } from '../services/products-selectors.service';
import { RepositoriesSelectors } from '../services/repositories-selectors.service';
import { RouterSelectors } from '../services/router-selectors.service';
import { UserProfileSelectors } from '../services/user-selectors.service';
import { ProductsEffects } from './products.effects';

describe('products.effects', () => {
    let actions$: Observable<Actions>;
    let imageService: ImageService;
    let shareService: ShareService;
    let productsSelectors: ProductsSelectors;
    let productsService: ProductsService;
    let filterSelectors: FilterSelectors;
    let routerSelectors: RouterSelectors;
    let repositoriesSelectors: RepositoriesSelectors;
    let categoriesSelectors: CategoriesSelectors;
    let userService: UserService;
    let userProfileSelectors: UserProfileSelectors;
    let router: Router;
    let dialog: MatDialog;
    let translateService: TranslateService;
    let homeService: HomeService;
    let deviceService: DeviceService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [
                ProductsEffects,
                provideMockActions(() => actions$),
                {
                    provide: ProductsService,
                    useValue: {
                        getProduct: jest.fn(),
                        getProductViews: jest.fn(),
                        getUserProducts: jest.fn(),
                        deactivateProduct: jest.fn(),
                    },
                },
                {
                    provide: MatDialog,
                    useValue: {
                        open: jest.fn(),
                    },
                },
                {
                    provide: TranslateService,
                    useValue: {
                        instant: jest.fn(),
                    },
                },
                {
                    provide: ImageService,
                    useValue: {
                        fetchImageAsFile: jest.fn(),
                        getUrl: jest.fn(),
                    },
                },
                {
                    provide: ShareService,
                    useValue: {
                        canShare: jest.fn(),
                        share: jest.fn(),
                    },
                },
                {
                    provide: ProductsSelectors,
                    useValue: {},
                },
                {
                    provide: HomeService,
                    useValue: {
                        removeFromList: jest.fn(),
                    },
                },
                {
                    provide: NavigationHistoryService,
                    useValue: {
                        back: jest.fn(),
                    },
                },
                {
                    provide: FilterSelectors,
                    useValue: {
                        current$: jest.fn(),
                        currentRouterParams$: jest.fn(),
                    },
                },
                {
                    provide: RouterSelectors,
                    useValue: {},
                },
                {
                    provide: RepositoriesSelectors,
                    useValue: {},
                },
                {
                    provide: CategoriesSelectors,
                    useValue: {},
                },
                {
                    provide: UserService,
                    useValue: {
                        findRatingSummary: jest.fn(),
                    },
                },
                {
                    provide: UserProfileSelectors,
                    useValue: {},
                },
                {
                    provide: Router,
                    useValue: {
                        navigate: jest.fn(),
                    },
                },
                {
                    provide: DeviceService,
                    useValue: {},
                },
            ],
        });

        productsService = TestBed.inject(ProductsService);
        dialog = TestBed.inject(MatDialog);
        translateService = TestBed.inject(TranslateService);
        imageService = TestBed.inject(ImageService);
        shareService = TestBed.inject(ShareService);
        productsSelectors = TestBed.inject(ProductsSelectors);
        filterSelectors = TestBed.inject(FilterSelectors);
        routerSelectors = TestBed.inject(RouterSelectors);
        repositoriesSelectors = TestBed.inject(RepositoriesSelectors);
        categoriesSelectors = TestBed.inject(CategoriesSelectors);
        userService = TestBed.inject(UserService);
        userProfileSelectors = TestBed.inject(UserProfileSelectors);
        router = TestBed.inject(Router);
        homeService = TestBed.inject(HomeService);
        deviceService = TestBed.inject(DeviceService);

        // basic implementation to avoid null pointer
        routerSelectors.url$ = cold('f', { f: '/' });
    });

    describe('listenForFirstUrlChange$', () => {
        it('should dispatch set filter action when product page is active', () => {
            // mock
            routerSelectors.url$ = cold('-u', { u: '/product/1' });
            routerSelectors.isProductRouteActive$ = cold('-t', { t: true });

            const urlParams: SearchRouterParams = {
                seasonality: Seasonality.WINTER,
                minPrice: 10,
                searchText: 'test',
            };
            routerSelectors.pageParams$ = cold('-p', { p: urlParams });
            categoriesSelectors.allCategories$ = cold('-c', { c: [] });
            repositoriesSelectors.brands$ = cold('-b', { b: [] });
            repositoriesSelectors.colors$ = cold('-c', { c: [] });
            repositoriesSelectors.conditions$ = cold('-c', { c: [] });
            categoriesSelectors.allSizes$ = cold('-s', { s: [] });

            // call
            const effects = TestBed.inject(ProductsEffects);

            // asserts
            expect(effects.listenForFirstUrlChange$).toBeObservable(
                cold('--a', {
                    a: filtersActions.set({
                        key: 'dressing',
                        filter: {
                            seasonality: Seasonality.WINTER,
                            minPrice: 10,
                            searchText: 'test',
                        },
                    }),
                }),
            );
        });

        it('should do nothing when product page is not active', () => {
            // mock
            routerSelectors.url$ = cold('-u', { u: '/search' });
            routerSelectors.isProductRouteActive$ = cold('-f', { f: false });

            // call
            const effects = TestBed.inject(ProductsEffects);

            // asserts
            expect(effects.listenForFirstUrlChange$).toBeObservable(cold('--'));
        });
    });

    describe('changeDressingWhenFilterChangedAndProductActive$', () => {
        it('should dispatch get dressing actions when product route is active & product loaded', () => {
            // mock
            actions$ = hot('-a-', { a: filtersActions.set({ key: 'dressing', filter: {} }) });
            routerSelectors.isProductRouteActive$ = cold('t-', { t: true });
            productsSelectors.product$ = cold('p-', { p: { id: 1, owner: { id: 2 } } });

            // call
            const effects = TestBed.inject(ProductsEffects);

            // asserts
            expect(effects.changeDressingWhenFilterChangedAndProductActive$).toBeObservable(
                cold('-a', {
                    a: ProductsActions.getDressing({ userId: 2 }),
                }),
            );
        });

        it('should do nothing when product route is not active', () => {
            // mock
            actions$ = hot('-a-', { a: filtersActions.set({ key: 'dressing', filter: {} }) });
            routerSelectors.isProductRouteActive$ = cold('f-', { f: false });
            productsSelectors.product$ = cold('p-', { p: { id: 1 } });

            // call
            const effects = TestBed.inject(ProductsEffects);

            // asserts
            expect(effects.changeDressingWhenFilterChangedAndProductActive$).toBeObservable(cold('---'));
        });

        it('should do nothing when product is not loaded', () => {
            // mock
            actions$ = hot('-a-', { a: filtersActions.set({ key: 'dressing', filter: {} }) });
            routerSelectors.isProductRouteActive$ = cold('t-', { t: true });
            productsSelectors.product$ = cold('p-', { p: null });

            // call
            const effects = TestBed.inject(ProductsEffects);

            // asserts
            expect(effects.changeDressingWhenFilterChangedAndProductActive$).toBeObservable(cold('---'));
        });

        it('should do nothing when other filter changed', () => {
            // mock
            actions$ = hot('-a-', { a: filtersActions.set({ key: 'search', filter: {} }) });
            routerSelectors.isProductRouteActive$ = cold('t-', { t: true });
            productsSelectors.product$ = cold('p-', { p: { id: 1 } });

            // call
            const effects = TestBed.inject(ProductsEffects);

            // asserts
            expect(effects.changeDressingWhenFilterChangedAndProductActive$).toBeObservable(cold('---'));
        });
    });

    describe('loadProduct$', () => {
        it('should dispatch load product success action', () => {
            // given
            const product: Product = {
                id: 1,
                label: 'test',
                description: 'test',
                productStatus: ProductStatusEnum.AVAILABLE,
                labelStatus: ModerationStatus.OK,
                descriptionStatus: ModerationStatus.OK,
                price: 10,
                pictures: [],
                owner: {
                    id: 1,
                    firebaseId: 'id',
                    nickname: 'test',
                    creationDate: 0,
                    lastModifiedDate: 0,
                },
                productType: undefined,
                selectedTransportModes: [],
                lastModifiedDate: 0,
                creationDate: 0,
            } as any as Product;

            // mock
            actions$ = hot('-a-', { a: ProductsActions.loadProduct({ productId: 1 }) });
            jest.spyOn(productsService, 'getProduct').mockReturnValue(
                cold('-(p|)', {
                    p: product,
                }),
            );
            jest.spyOn(userService, 'findRatingSummary').mockReturnValue(
                cold('-(r|)', {
                    r: {
                        totalRating: 4,
                        averageRating: 3,
                        detailedRating: {},
                    },
                }),
            );
            userProfileSelectors.userProfile$ = cold('u-', {
                u: null,
            });
            userProfileSelectors.isLogged$ = cold('f-', {
                f: false,
            });

            // call
            const effects = TestBed.inject(ProductsEffects);

            // asserts
            expect(effects.loadProduct$).toBeObservable(
                cold('---a', {
                    a: ProductsActions.loadProductSuccess({
                        product: {
                            ...product,
                            owner: {
                                ...product.owner,
                                rate: 3,
                                totalRate: 4,
                                hasBlockedMe: false,
                            },
                        },
                    }),
                }),
            );
        });

        it('should redirect to 404 when product is moderated & no current user', () => {
            // mock
            actions$ = hot('-a-', { a: ProductsActions.loadProduct({ productId: 1 }) });
            jest.spyOn(productsService, 'getProduct').mockReturnValue(
                cold('-(p|)', {
                    p: {
                        id: 1,
                        productStatus: ProductStatusEnum.MODERATED,
                        owner: {
                            id: 1,
                        },
                    },
                }),
            );
            jest.spyOn(userService, 'findRatingSummary').mockReturnValue(
                cold('-(r|)', {
                    r: {
                        totalRating: 4,
                        averageRating: 3,
                        detailedRating: {},
                    },
                }),
            );
            jest.spyOn(router, 'navigate').mockReturnValue(cold('-(u|)', { u: undefined }) as any);
            userProfileSelectors.userProfile$ = cold('u-', {
                u: null,
            });
            userProfileSelectors.isLogged$ = cold('f-', {
                f: false,
            });

            // call
            const effects = TestBed.inject(ProductsEffects);

            // asserts
            expect(effects.loadProduct$).toSatisfyOnFlush(() => {
                expect(router.navigate).toHaveBeenCalledTimes(1);
            });
        });
    });

    describe('loadProductSuccess$', () => {
        it('should dispatch get dressing & get product views actions', () => {
            // mock
            actions$ = hot('-a-', {
                a: ProductsActions.loadProductSuccess({
                    product: {
                        id: 1,
                        label: 'test',
                        description: 'test',
                        productStatus: ProductStatusEnum.AVAILABLE,
                        labelStatus: ModerationStatus.OK,
                        descriptionStatus: ModerationStatus.OK,
                        price: 10,
                        pictures: [],
                        owner: {
                            id: 1,
                            firebaseId: 'id',
                            nickname: 'test',
                            creationDate: 0,
                            lastModifiedDate: 0,
                        },
                        productType: undefined,
                        selectedTransportModes: [],
                        lastModifiedDate: 0,
                        creationDate: 0,
                    } as any as Product,
                }),
            });

            // call
            const effects = TestBed.inject(ProductsEffects);

            // asserts
            expect(effects.loadProductSuccess$).toBeObservable(
                cold('-(ab)', {
                    a: ProductsActions.getDressing({ userId: 1 }),
                    b: ProductsActions.getProductViews({ productId: 1 }),
                }),
            );
        });
    });

    describe('shareDressing$', () => {
        it('should not dispatch actions', () => {
            const effects = TestBed.inject(ProductsEffects);
            const metadata: EffectsMetadata<ProductsEffects> = getEffectsMetadata(effects);

            expect(metadata.shareDressing$!.dispatch).toBeFalsy();
        });

        it('should share dressing', () => {
            const user = {
                nickname: 'Alpha',
            } as UserProfile;

            const effects = TestBed.inject(ProductsEffects);

            const action = ProductsActions.shareDressing({ user });

            jest.spyOn(shareService, 'canShare').mockReturnValue(true);
            jest.spyOn(translateService, 'instant').mockReturnValue('');

            actions$ = hot('-a-', { a: action });

            expect(effects.shareDressing$).toSatisfyOnFlush(() => {
                expect(shareService.share).toHaveBeenCalledTimes(1);
            });
        });

        it('cannot share dressing if share deactivated', () => {
            const user = {
                nickname: 'Alpha',
            } as UserProfile;

            const effects = TestBed.inject(ProductsEffects);

            const action = ProductsActions.shareDressing({ user });

            jest.spyOn(shareService, 'canShare').mockReturnValue(false);
            jest.spyOn(translateService, 'instant').mockReturnValue('');

            actions$ = hot('-a-', { a: action });

            expect(effects.shareDressing$).toSatisfyOnFlush(() => {
                expect(shareService.share).not.toHaveBeenCalled();
            });
        });
    });

    describe('shareProduct$', () => {
        beforeEach(() => {
            jest.spyOn(imageService, 'getUrl').mockReturnValue('https://images.com/123456.jpg');
        });

        it('should not dispatch actions', () => {
            const effects = TestBed.inject(ProductsEffects);
            const metadata: EffectsMetadata<ProductsEffects> = getEffectsMetadata(effects);

            expect(metadata.shareProduct$!.dispatch).toBeFalsy();
        });

        it('should share dressing', () => {
            const product = {
                id: 1,
                pictures: [{ fileName: '1.jpg' }, { fileName: '2.jpg' }, { fileName: '3.jpg' }],
            } as Product;

            const effects = TestBed.inject(ProductsEffects);

            const action = ProductsActions.shareProduct({ product });

            jest.spyOn(shareService, 'canShare').mockReturnValue(true);
            jest.spyOn(imageService, 'fetchImageAsFile').mockImplementation(url => {
                const file = { filename: url };
                return cold('-(f|)', { f: file });
            });
            jest.spyOn(translateService, 'instant').mockReturnValue('');

            actions$ = hot('-a-', { a: action });

            expect(effects.shareProduct$).toSatisfyOnFlush(() => {
                expect(shareService.share).toHaveBeenCalledTimes(1);
            });
        });

        it('cannot share dressing if share deactivated', () => {
            const product = {
                id: 1,
                pictures: [{ fileName: '1.jpg' }, { fileName: '2.jpg' }, { fileName: '3.jpg' }],
            } as Product;

            const effects = TestBed.inject(ProductsEffects);

            const action = ProductsActions.shareProduct({ product });

            jest.spyOn(shareService, 'canShare').mockReturnValue(false);
            jest.spyOn(imageService, 'fetchImageAsFile').mockImplementation(url => {
                const file = { filename: url };
                return cold('-(f|)', { f: file });
            });
            jest.spyOn(translateService, 'instant').mockReturnValue('');

            actions$ = hot('-a-', { a: action });

            expect(effects.shareProduct$).toSatisfyOnFlush(() => {
                expect(shareService.share).not.toHaveBeenCalled();
            });
        });

        it('should share even if image download fails', () => {
            const product = {
                id: 1,
                pictures: [{ fileName: '1.jpg' }, { fileName: '2.jpg' }, { fileName: '3.jpg' }],
            } as Product;

            const effects = TestBed.inject(ProductsEffects);

            const action = ProductsActions.shareProduct({ product });

            jest.spyOn(shareService, 'canShare').mockReturnValue(true);
            jest.spyOn(imageService, 'fetchImageAsFile').mockImplementation(url => {
                if (url === '2.jpg') {
                    return cold('-#', undefined, 'Cannot fetch image...');
                }

                const file = { filename: url };
                return cold('-(f|)', { f: file });
            });
            jest.spyOn(translateService, 'instant').mockReturnValue('');

            actions$ = hot('-a-', { a: action });

            expect(effects.shareProduct$).toSatisfyOnFlush(() => {
                expect(shareService.share).toHaveBeenCalledTimes(1);
            });
        });
    });

    describe('getProductViews$', () => {
        it('should return getProductViewsSuccess in case of success', () => {
            const productId = 4;
            const views = 233;

            const effects = TestBed.inject(ProductsEffects);

            jest.spyOn(productsService, 'getProductViews').mockReturnValue(cold('-(v|)', { v: views }));

            const action = ProductsActions.getProductViews({ productId });
            actions$ = hot('-a-', { a: action });

            const getProductViewsSuccess = ProductsActions.getProductViewsSuccess({ views });

            const expected = cold('--b', { b: getProductViewsSuccess });
            expect(effects.getProductViews$).toBeObservable(expected);
        });

        it('should return getProductViewsError in case of error', () => {
            const productId = 4;
            const error = 'Cannot get product views...';

            const effects = TestBed.inject(ProductsEffects);

            jest.spyOn(productsService, 'getProductViews').mockReturnValue(cold('-#', undefined, error));

            const action = ProductsActions.getProductViews({ productId });
            actions$ = hot('-a-', { a: action });

            const getProductViewsError = ProductsActions.getProductViewsError({ error });

            const expected = cold('--b', { b: getProductViewsError });
            expect(effects.getProductViews$).toBeObservable(expected);
        });
    });

    describe('getDressing$', () => {
        it('should get user dressing with current filter', () => {
            // mock
            actions$ = hot('-a-', {
                a: ProductsActions.getDressing({ userId: 1 }),
            });
            routerSelectors.productId$ = cold('p-', {
                p: '1',
            });
            jest.spyOn(filterSelectors, 'current$').mockReturnValue(
                cold('f-', {
                    f: {},
                }),
            );
            jest.spyOn(productsService, 'getUserProducts').mockReturnValue(
                cold('-(r|)', {
                    r: {
                        content: [],
                        totalElements: 0,
                        numberOfElements: 0,
                    },
                }),
            );

            // call
            const effects = TestBed.inject(ProductsEffects);

            // asserts
            expect(effects.getDressing$).toBeObservable(
                cold('--a', {
                    a: ProductsActions.getDressingSuccess({
                        products: [],
                        count: 0,
                        total: 0,
                    }),
                }),
            );
        });
    });

    describe('deleteProduct$', () => {
        type DialogRef = MatDialogRef<ConfirmationDialogComponent>;

        it('should do nothing when no confirmation', () => {
            // mock
            actions$ = hot('-a-', {
                a: ProductsActions.deleteProduct({ productId: 1 }),
            });
            const openModalWithoutResult = () => {
                return {
                    afterClosed: () => of(undefined),
                } as DialogRef;
            };
            jest.spyOn(dialog, 'open').mockImplementation(openModalWithoutResult);

            // call
            const effects = TestBed.inject(ProductsEffects);

            // asserts
            expect(effects.deleteProduct$).toBeObservable(cold('---'));
        });

        it('should delete product & dispatch delete product success action', () => {
            // mock
            actions$ = hot('-a-', {
                a: ProductsActions.deleteProduct({ productId: 1 }),
            });
            const openModalWithResult = () => {
                return {
                    afterClosed: () => of(true),
                } as DialogRef;
            };
            jest.spyOn(dialog, 'open').mockImplementation(openModalWithResult);
            jest.spyOn(productsService, 'deactivateProduct').mockReturnValue(
                cold('-(p|)', {
                    p: null,
                }),
            );

            // call
            const effects = TestBed.inject(ProductsEffects);

            // asserts
            expect(effects.deleteProduct$).toBeObservable(
                cold('--a', {
                    a: ProductsActions.deleteProductSuccess({ productId: 1 }),
                }),
            );
        });
    });

    describe('deleteProductSuccess$', () => {
        it('should not dispatch action', () => {
            // call
            const effects = TestBed.inject(ProductsEffects);
            const metadata = getEffectsMetadata(effects);

            // asserts
            expect(metadata.deleteProductSuccess$!.dispatch).toBeFalsy();
        });

        it('should call home service & go back', () => {
            // mock
            actions$ = hot('-a-', {
                a: ProductsActions.deleteProductSuccess({ productId: 1 }),
            });
            jest.spyOn(homeService, 'removeFromList').mockReturnValue();

            // call
            const effects = TestBed.inject(ProductsEffects);

            // asserts
            expect(effects.deleteProductSuccess$).toSatisfyOnFlush(() => {
                expect(homeService.removeFromList).toHaveBeenCalledTimes(1);
            });
        });
    });

    describe('navigateWhenFilterChangedOnDesktop$', () => {
        it('should not dispatch action', () => {
            // call
            const effects = TestBed.inject(ProductsEffects);
            const metadata = getEffectsMetadata(effects);

            // asserts
            expect(metadata.navigateWhenFilterChangedOnDesktop$!.dispatch).toBeFalsy();
        });

        it('should do nothing when product route is not active', () => {
            // mock
            routerSelectors.isProductRouteActive$ = cold('-f', { f: false });
            routerSelectors.url$ = cold('-u', { u: '/search' });
            routerSelectors.productId$ = cold('-p', { p: 1 });

            // call
            const effects = TestBed.inject(ProductsEffects);

            // asserts
            expect(effects.navigateWhenFilterChangedOnDesktop$).toSatisfyOnFlush(() => {
                expect(router.navigate).not.toHaveBeenCalled();
            });
        });

        it('should do nothing when filter changed on handset', () => {
            // mock
            routerSelectors.isProductRouteActive$ = cold('-t', { t: true });
            routerSelectors.url$ = cold('-u', { u: '/product/1' });
            routerSelectors.productId$ = cold('-p', { p: 1 });
            jest.spyOn(filterSelectors, 'currentRouterParams$').mockReturnValue(
                cold('---f', {
                    f: {},
                }),
            );
            deviceService.isDesktop$ = cold('-f', { f: false });

            // call
            const effects = TestBed.inject(ProductsEffects);

            // asserts
            expect(effects.navigateWhenFilterChangedOnDesktop$).toSatisfyOnFlush(() => {
                expect(router.navigate).not.toHaveBeenCalled();
            });
        });
    });
});
