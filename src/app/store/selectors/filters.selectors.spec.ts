import { Search, Seasonality } from '../../shared/models/models';
import { RootState } from '../models';
import { FiltersState, initialFiltersState, initialProductFilterState } from '../reducers/filters.reducer';
import { filtersSelectors } from './filters.selectors';

describe('filters.selectors', () => {
    const state: FiltersState = {
        ...initialFiltersState,
        search: {
            searchText: 'Test',
            category: { id: 1, labelKey: 'category1', orderNumber: 1 },
            subCategory: undefined,
            productsTypes: [],
            sizes: [],
            colors: [
                { id: 1, labelKey: 'red', orderNumber: 1 },
                { id: 2, labelKey: 'blue', orderNumber: 2 },
            ],
            brands: [{ id: 1, label: 'test', slug: 'test', luxury: false, orderNumber: 1 }],
            conditions: [],
            seasonality: Seasonality.WINTER,
            minPrice: 20,
            maxPrice: 100,
            sort: {
                field: 'price',
                order: 'desc',
            },
        },
        searchHistory: {
            recent: [createFakeSearch(1), createFakeSearch(2), createFakeSearch(3)],
            saved: [createFakeSearch(2)],
            loading: true,
        },
    };
    const rootState = {
        entityCache: {
            filters: state,
        },
    } as RootState;

    describe('count', () => {
        it('should count filter', () => {
            const searchResult = filtersSelectors.count('search')(rootState);
            const dressingResult = filtersSelectors.count('dressing')(rootState);

            expect(searchResult).toBeDefined();
            expect(searchResult).toEqual(9);
            expect(dressingResult).toBeDefined();
            expect(dressingResult).toEqual(0);
        });
    });

    describe('empty', () => {
        it('should return true if there is no active filter', () => {
            const searchResult = filtersSelectors.isEmpty('search')(rootState);
            const dressingResult = filtersSelectors.isEmpty('dressing')(rootState);

            expect(searchResult).toBeFalsy();
            expect(dressingResult).toBeTruthy();
        });
    });

    describe('hasSort', () => {
        it('should return true if there is an active sorting', () => {
            const searchResult = filtersSelectors.hasSort('search')(rootState);
            const dressingResult = filtersSelectors.hasSort('dressing')(rootState);

            expect(searchResult).toBeTruthy();
            expect(dressingResult).toBeFalsy();
        });
    });

    describe('getKeyword', () => {
        it('should return filter keyword', () => {
            const searchResult = filtersSelectors.getKeyword('search')(rootState);
            const dressingResult = filtersSelectors.getKeyword('dressing')(rootState);

            expect(searchResult).toEqual('Test');
            expect(dressingResult).toEqual('');
        });
    });

    describe('getCategory', () => {
        it('should return filter keyword', () => {
            const searchResult = filtersSelectors.getCategory('search')(rootState);
            const dressingResult = filtersSelectors.getCategory('dressing')(rootState);

            expect(searchResult?.id).toEqual(1);
            expect(dressingResult).toBeUndefined();
        });
    });

    describe('getBrands', () => {
        it('should return filter brands', () => {
            const searchResult = filtersSelectors.getBrands('search')(rootState);
            const dressingResult = filtersSelectors.getBrands('dressing')(rootState);

            expect(searchResult?.length).toEqual(1);
            expect(dressingResult?.length).toEqual(initialProductFilterState.brands?.length);
        });
    });

    describe('getColors', () => {
        it('should return filter colors', () => {
            const searchResult = filtersSelectors.getColors('search')(rootState);
            const dressingResult = filtersSelectors.getColors('dressing')(rootState);

            expect(searchResult?.length).toEqual(2);
            expect(dressingResult?.length).toEqual(initialProductFilterState.colors?.length);
        });
    });

    describe('getConditions', () => {
        it('should return filter conditions', () => {
            const searchResult = filtersSelectors.getConditions('search')(rootState);
            const dressingResult = filtersSelectors.getConditions('dressing')(rootState);

            expect(searchResult?.length).toEqual(0);
            expect(dressingResult?.length).toEqual(initialProductFilterState.conditions?.length);
        });
    });

    describe('getMinPrice', () => {
        it('should return filter minimum price', () => {
            const searchResult = filtersSelectors.getMinPrice('search')(rootState);
            const dressingResult = filtersSelectors.getMinPrice('dressing')(rootState);

            expect(searchResult).toEqual(20);
            expect(dressingResult).toBeUndefined();
        });
    });

    describe('getMaxPrice', () => {
        it('should return filter maximum price', () => {
            const searchResult = filtersSelectors.getMaxPrice('search')(rootState);
            const dressingResult = filtersSelectors.getMaxPrice('dressing')(rootState);

            expect(searchResult).toEqual(100);
            expect(dressingResult).toBeUndefined();
        });
    });

    describe('getSeasonality', () => {
        it('should return filter keyword', () => {
            const searchResult = filtersSelectors.getSeasonality('search')(rootState);
            const dressingResult = filtersSelectors.getSeasonality('dressing')(rootState);

            expect(searchResult).toEqual(Seasonality.WINTER);
            expect(dressingResult).toEqual(initialProductFilterState.seasonality);
        });
    });

    describe('getSort', () => {
        it('should return filter sort', () => {
            const searchResult = filtersSelectors.getSort('search')(rootState);
            const dressingResult = filtersSelectors.getSort('dressing')(rootState);

            expect(searchResult?.field).toEqual('price');
            expect(searchResult?.order).toEqual('desc');
            expect(dressingResult?.field).toEqual(initialProductFilterState.sort?.field);
            expect(dressingResult?.order).toEqual(initialProductFilterState.sort?.order);
        });
    });

    describe('getRecentSearch', () => {
        it('should return recent search list', () => {
            const recentSearch = filtersSelectors.getRecentSearch(rootState);

            expect(recentSearch.length).toBe(3);
        });
    });

    describe('getSavedSearch', () => {
        it('should return saved search list', () => {
            const savedSearch = filtersSelectors.getSavedSearch(rootState);

            expect(savedSearch.length).toBe(1);
        });
    });

    describe('isSearchLoading', () => {
        it('should return search history loading state', () => {
            const loading = filtersSelectors.isSearchLoading(rootState);

            expect(loading).toBeTruthy();
        });
    });
});

function createFakeSearch(id: number, categoryId?: number, seasonality?: Seasonality): Search {
    return {
        id,
        categoryId,
        seasonality,
        userId: 1,
        keyword: 'test',
        saved: false,
    } as Search;
}
