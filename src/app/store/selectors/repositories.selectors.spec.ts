import { Brand } from 'src/app/shared/models/models';
import { RootState } from '../models';
import { getBrandBySlug } from './repositories.selectors';

describe('repositories.selectors', () => {
    describe('getBrandBySlug', () => {
        const state = {
            entityCache: {
                repositories: {
                    brands: [
                        {
                            id: 1,
                            slug: 'kiabi',
                        },
                    ],
                },
            },
        } as RootState;

        it('should find a brand related to slug', () => {
            const result = getBrandBySlug('kiabi')(state);

            expect(result).toEqual({
                id: 1,
                slug: 'kiabi',
            } as Brand);
        });

        it('should not find a brand if no slug match', () => {
            const result = getBrandBySlug('hello')(state);

            expect(result).toBeUndefined();
        });
    });
});
