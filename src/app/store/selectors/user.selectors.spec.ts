import { Address, UserProfileStatus, UserType } from 'src/app/shared/models/models';
import { RootState } from '../models';
import {
    canCreateLot,
    getAddresses,
    getAvatarURL,
    getCurrentUserId,
    getDefaultAddress,
    getFavoriteProductsIds,
    getHasAddresses,
    getIsCurrentUser,
    isBlocked,
    isBlockedByAdmin,
    isIndividual,
    isNotBlockedByAdmin,
    isProductFavorite,
    isProfessional,
} from './user.selectors';

describe('user.selectors', () => {
    describe('isCurrentUser', () => {
        it('should be false if no profile', () => {
            const state = {
                entityCache: {
                    userProfile: {
                        userProfile: undefined,
                    },
                },
            } as RootState;

            const result = getIsCurrentUser(12)(state);

            expect(result).toBeFalsy();
        });

        it('should be true if current profile', () => {
            const state = {
                entityCache: {
                    userProfile: {
                        userProfile: {
                            id: 12,
                        },
                    },
                },
            } as RootState;

            const result = getIsCurrentUser(12)(state);

            expect(result).toBeTruthy();
        });

        it('should be false if not matching current profile', () => {
            const state = {
                entityCache: {
                    userProfile: {
                        userProfile: {
                            id: 44,
                        },
                    },
                },
            } as RootState;

            const result = getIsCurrentUser(12)(state);

            expect(result).toBeFalsy();
        });
    });

    describe('getCurrentUserId', () => {
        it('should be undefined if no profile', () => {
            const state = {
                entityCache: {
                    userProfile: {
                        userProfile: undefined,
                    },
                },
            } as RootState;

            const result = getCurrentUserId(state);

            expect(result).toBeUndefined();
        });

        it('should be 4', () => {
            const state = {
                entityCache: {
                    userProfile: {
                        userProfile: {
                            id: 4,
                        },
                    },
                },
            } as RootState;

            const result = getCurrentUserId(state);

            expect(result).toBe(4);
        });
    });

    describe('getAddresses', () => {
        it('should be empty array if no profile', () => {
            const state = {
                entityCache: {
                    userProfile: {
                        userProfile: undefined,
                    },
                },
            } as RootState;

            const result = getAddresses(state);

            expect(result).toEqual([]);
        });

        it('should return addresses of user', () => {
            const addresses: Address[] = [
                {
                    id: 1,
                    addressName: 'ABC',
                    isDefault: true,
                },
                {
                    id: 2,
                    addressName: 'XYZ',
                    isDefault: false,
                },
            ] as Address[];

            const state = {
                entityCache: {
                    userProfile: {
                        userProfile: {
                            addresses,
                        },
                    },
                },
            } as RootState;

            const result = getAddresses(state);

            expect(result).toBe(addresses);
        });
    });

    describe('getHasAddresses', () => {
        it('should be false if no profile', () => {
            const state = {
                entityCache: {
                    userProfile: {
                        userProfile: undefined,
                    },
                },
            } as RootState;

            const result = getHasAddresses(state);

            expect(result).toBeFalsy();
        });

        it('should be false if no addresses', () => {
            const state = {
                entityCache: {
                    userProfile: {
                        userProfile: {
                            addresses: [] as Address[],
                        },
                    },
                },
            } as RootState;

            const result = getHasAddresses(state);

            expect(result).toBeFalsy();
        });

        it('should be true if has addresses', () => {
            const addresses: Address[] = [
                {
                    id: 1,
                    addressName: 'ABC',
                    isDefault: true,
                },
                {
                    id: 2,
                    addressName: 'XYZ',
                    isDefault: false,
                },
            ] as Address[];

            const state = {
                entityCache: {
                    userProfile: {
                        userProfile: {
                            addresses,
                        },
                    },
                },
            } as RootState;

            const result = getHasAddresses(state);

            expect(result).toBeTruthy();
        });
    });

    describe('getDefaultAddress', () => {
        it('should not have default address if no profile', () => {
            const state = {
                entityCache: {
                    userProfile: {
                        userProfile: undefined,
                    },
                },
            } as RootState;

            const result = getDefaultAddress(state);

            expect(result).toBeUndefined();
        });

        it('should not have default address if no addresses', () => {
            const state = {
                entityCache: {
                    userProfile: {
                        userProfile: {
                            addresses: [] as Address[],
                        },
                    },
                },
            } as RootState;

            const result = getDefaultAddress(state);

            expect(result).toBeUndefined();
        });

        it('should have default address', () => {
            const addresses: Address[] = [
                {
                    id: 1,
                    addressName: 'ABC',
                    isDefault: true,
                },
                {
                    id: 2,
                    addressName: 'XYZ',
                    isDefault: false,
                },
            ] as Address[];

            const state = {
                entityCache: {
                    userProfile: {
                        userProfile: {
                            addresses,
                        },
                    },
                },
            } as RootState;

            const result = getDefaultAddress(state);

            expect(result).toBeDefined();
            expect(result?.addressName).toBe('ABC');
        });
    });

    describe('getAvatarURL', () => {
        it('should be static url "assets/images/avatar.png"', () => {
            const state = {
                entityCache: {
                    userProfile: {
                        userProfile: {
                            id: 14,
                            picture: {
                                id: 1,
                                fileName: 'avatar.png',
                                static: true,
                            },
                        },
                    },
                },
            } as RootState;

            const result = getAvatarURL(state);

            expect(result).toBe('assets/images/avatar.png');
        });

        it('should be distant url "https://pictures-dev.second-hand.aws.kiabi.pro/avatar.png"', () => {
            const state = {
                entityCache: {
                    userProfile: {
                        userProfile: {
                            id: 14,
                            picture: {
                                id: 1,
                                fileName: 'avatar.png',
                                static: false,
                            },
                        },
                    },
                },
            } as RootState;

            const result = getAvatarURL(state);

            expect(result).toBe('https://pictures-dev.second-hand.aws.kiabi.pro/avatar.png');
        });

        it('should be empty if no picture', () => {
            const state = {
                entityCache: {
                    userProfile: {
                        userProfile: {
                            id: 14,
                        },
                    },
                },
            } as RootState;

            const result = getAvatarURL(state);

            expect(result).toBe('');
        });
    });

    describe('isProfessional', () => {
        const testCases = [
            { type: UserType.INDIVIDUAL, expected: false },
            { type: UserType.PROFESSIONAL, expected: true },
        ];

        testCases.forEach(({ type, expected }) => {
            it(`should be ${expected} if user is ${type}`, () => {
                const state = {
                    entityCache: {
                        userProfile: {
                            userProfile: {
                                id: 14,
                                type,
                            },
                        },
                    },
                } as RootState;

                const result = isProfessional(state);

                expect(result).toBe(expected);
            });
        });
    });

    describe('isIndividual', () => {
        const testCases = [
            { type: UserType.INDIVIDUAL, expected: true },
            { type: UserType.PROFESSIONAL, expected: false },
        ];

        testCases.forEach(({ type, expected }) => {
            it(`should be ${expected} if user is ${type}`, () => {
                const state = {
                    entityCache: {
                        userProfile: {
                            userProfile: {
                                id: 14,
                                type,
                            },
                        },
                    },
                } as RootState;

                const result = isIndividual(state);

                expect(result).toBe(expected);
            });
        });
    });

    describe('isBlockedByAdmin', () => {
        it('should be false if no profile', () => {
            const state = {
                entityCache: {
                    userProfile: {
                        userProfile: undefined,
                    },
                },
            } as RootState;

            const result = isBlockedByAdmin(state);

            expect(result).toBeFalsy();
        });

        const cases = [
            { status: UserProfileStatus.ACTIVE, expected: false },
            { status: UserProfileStatus.CLOSED, expected: false },
            { status: UserProfileStatus.BLOCKED, expected: true },
        ];

        cases.forEach(({ status, expected }) => {
            it(`should be ${expected} if status is ${status}`, () => {
                const state = {
                    entityCache: {
                        userProfile: {
                            userProfile: {
                                status,
                            },
                        },
                    },
                } as RootState;

                const result = isBlockedByAdmin(state);

                expect(result).toBe(expected);
            });
        });
    });

    describe('isNotBlockedByAdmin', () => {
        it('should be true if no profile', () => {
            const state = {
                entityCache: {
                    userProfile: {
                        userProfile: undefined,
                    },
                },
            } as RootState;

            const result = isNotBlockedByAdmin(state);

            expect(result).toBeTruthy();
        });

        const cases = [
            { status: UserProfileStatus.ACTIVE, expected: true },
            { status: UserProfileStatus.CLOSED, expected: true },
            { status: UserProfileStatus.BLOCKED, expected: false },
        ];

        cases.forEach(({ status, expected }) => {
            it(`should be ${expected} if status is ${status}`, () => {
                const state = {
                    entityCache: {
                        userProfile: {
                            userProfile: {
                                status,
                            },
                        },
                    },
                } as RootState;

                const result = isNotBlockedByAdmin(state);

                expect(result).toBe(expected);
            });
        });
    });

    describe('getFavoriteProductIds', () => {
        it('should be empty if no profile', () => {
            const state = {
                entityCache: {
                    userProfile: {
                        userProfile: undefined,
                    },
                },
            } as RootState;

            const result = getFavoriteProductsIds(state);

            expect(result).toEqual([]);
        });

        it('should be favoriteProductsIds array if profile is provided', () => {
            const state = {
                entityCache: {
                    userProfile: {
                        userProfile: {
                            favoriteProductsIds: [14, 228, 1047],
                        },
                    },
                },
            } as RootState;

            const result = getFavoriteProductsIds(state);

            expect(result).toEqual([14, 228, 1047]);
        });
    });

    describe('isProductFavorite', () => {
        it('should be falsy if no profile', () => {
            const state = {
                entityCache: {
                    userProfile: {
                        userProfile: undefined,
                    },
                },
            } as RootState;

            const result = isProductFavorite(23)(state);

            expect(result).toBeFalsy();
        });

        it('should be falsy if not in favoriteProductsIds', () => {
            const state = {
                entityCache: {
                    userProfile: {
                        userProfile: {
                            favoriteProductsIds: [14, 228, 1047],
                        },
                    },
                },
            } as RootState;

            const result = isProductFavorite(23)(state);

            expect(result).toBeFalsy();
        });

        it('should be truthy if in favoriteProductsIds', () => {
            const state = {
                entityCache: {
                    userProfile: {
                        userProfile: {
                            favoriteProductsIds: [14, 228, 1047],
                        },
                    },
                },
            } as RootState;

            const result = isProductFavorite(14)(state);

            expect(result).toBeTruthy();
        });
    });

    describe('isBlocked', () => {
        it('should be false if no profile', () => {
            const state = {
                entityCache: {
                    userProfile: {
                        userProfile: undefined,
                    },
                },
            } as RootState;

            const result = isBlocked(12)(state);

            expect(result).toBeFalsy();
        });

        it('should be false if blockedUserIds is undefined', () => {
            const state = {
                entityCache: {
                    userProfile: {
                        userProfile: {
                            blockedUserIds: undefined,
                        },
                    },
                },
            } as RootState;

            const result = isBlocked(12)(state);

            expect(result).toBeFalsy();
        });

        it('should be true if user blocked', () => {
            const state = {
                entityCache: {
                    userProfile: {
                        userProfile: {
                            blockedUserIds: [8, 12],
                        },
                    },
                },
            } as RootState;

            const result = isBlocked(12)(state);

            expect(result).toBeTruthy();
        });

        it('should be false if not user blocked', () => {
            const state = {
                entityCache: {
                    userProfile: {
                        userProfile: {
                            blockedUserIds: [8, 12],
                        },
                    },
                },
            } as RootState;

            const result = isBlocked(13)(state);

            expect(result).toBeFalsy();
        });
    });

    describe('canCreateLot', () => {
        it('should be true if no profile', () => {
            const state = {
                entityCache: {
                    userProfile: {
                        userProfile: undefined,
                    },
                },
            } as RootState;

            const result = canCreateLot(12)(state);

            expect(result).toBeTruthy();
        });

        it('should be false if current profile', () => {
            const state = {
                entityCache: {
                    userProfile: {
                        userProfile: {
                            id: 12,
                        },
                    },
                },
            } as RootState;

            const result = canCreateLot(12)(state);

            expect(result).toBeFalsy();
        });

        it('should be false if blocked by admin', () => {
            const state = {
                entityCache: {
                    userProfile: {
                        userProfile: {
                            id: 44,
                            status: UserProfileStatus.BLOCKED,
                        },
                    },
                },
            } as RootState;

            const result = canCreateLot(12)(state);

            expect(result).toBeFalsy();
        });

        it('should be true if not matching current profile and not blocked by admin', () => {
            const state = {
                entityCache: {
                    userProfile: {
                        userProfile: {
                            id: 44,
                            status: UserProfileStatus.ACTIVE,
                        },
                    },
                },
            } as RootState;

            const result = canCreateLot(12)(state);

            expect(result).toBeTruthy();
        });
    });
});
