import { RouterReducerState } from '@ngrx/router-store';
import { BreadcrumbLink } from 'src/app/shared/models/models';
import { RootState } from '../models';
import {
    brandSlug,
    getBreadcrumbLinks,
    isChatRoomActive,
    isChatRouteActive,
    isCookiesRouteActive,
    isDressingPageVisible,
    isHomeRouteActive,
    isMarqueRouteActive,
    isProfileRouteActive,
    isSearchRouteActive,
    isSellRouteActive,
} from './router.selectors';

describe('router.selectors', () => {
    describe('isHomeRouteActive', () => {
        it('should be "/" route', () => {
            const state = {
                router: {
                    state: {
                        url: '/',
                    },
                } as RouterReducerState<any>,
            } as RootState;

            const result = isHomeRouteActive(state);

            expect(result).toBeTruthy();
        });
    });

    describe('isSearchRouteActive', () => {
        it('should be "/search" route', () => {
            const state = {
                router: {
                    state: {
                        url: '/search',
                    },
                } as RouterReducerState<any>,
            } as RootState;

            const result = isSearchRouteActive(state);

            expect(result).toBeTruthy();
        });

        it('should be "/search?search=test" route', () => {
            const state = {
                router: {
                    state: {
                        url: '/search?search=test',
                    },
                } as RouterReducerState<any>,
            } as RootState;

            const result = isSearchRouteActive(state);

            expect(result).toBeTruthy();
        });
    });

    describe('isMarqueRouteActive', () => {
        it('should not be "/marque" route', () => {
            const state = {
                router: {
                    state: {
                        url: '/marque',
                    },
                } as RouterReducerState<any>,
            } as RootState;

            const result = isMarqueRouteActive(state);

            expect(result).toBeFalsy();
        });

        it('should be "/marque/kiabi" route', () => {
            const state = {
                router: {
                    state: {
                        url: '/marque/kiabi',
                    },
                } as RouterReducerState<any>,
            } as RootState;

            const result = isMarqueRouteActive(state);

            expect(result).toBeTruthy();
        });
    });

    describe('isSellRouteActive', () => {
        it('should be "/add" route', () => {
            const state = {
                router: {
                    state: {
                        url: '/add',
                    },
                } as RouterReducerState<any>,
            } as RootState;

            const result = isSellRouteActive(state);

            expect(result).toBeTruthy();
        });
    });

    describe('isChatRouteActive', () => {
        it('should be "/chat" route', () => {
            const state = {
                router: {
                    state: {
                        url: '/chat',
                    },
                } as RouterReducerState<any>,
            } as RootState;

            const result = isChatRouteActive(state);

            expect(result).toBeTruthy();
        });

        it('should be "/chat/4" route', () => {
            const state = {
                router: {
                    state: {
                        url: '/chat/4',
                    },
                } as RouterReducerState<any>,
            } as RootState;

            const result = isChatRouteActive(state);

            expect(result).toBeTruthy();
        });
    });

    describe('isChatRoomActive', () => {
        it('should not be "/search" route', () => {
            const state = {
                router: {
                    state: {
                        url: '/search',
                    },
                } as RouterReducerState<any>,
            } as RootState;

            const result = isChatRoomActive(state);

            expect(result).toBeFalsy();
        });

        it('should not be "/chat" route', () => {
            const state = {
                router: {
                    state: {
                        url: '/chat',
                    },
                } as RouterReducerState<any>,
            } as RootState;

            const result = isChatRoomActive(state);

            expect(result).toBeFalsy();
        });

        it('should be "/chat/4" route', () => {
            const state = {
                router: {
                    state: {
                        url: '/chat/4',
                    },
                } as RouterReducerState<any>,
            } as RootState;

            const result = isChatRouteActive(state);

            expect(result).toBeTruthy();
        });
    });

    describe('isProfileRouteActive', () => {
        it('should be "/profile" route', () => {
            const state = {
                router: {
                    state: {
                        url: '/profile',
                    },
                } as RouterReducerState<any>,
            } as RootState;

            const result = isProfileRouteActive(state);

            expect(result).toBeTruthy();
        });
    });

    describe('isDressingPageVisible', () => {
        it('should be "/profile" route', () => {
            const state = {
                router: {
                    state: {
                        url: '/profile',
                    },
                } as RouterReducerState<any>,
            } as RootState;

            const result = isDressingPageVisible(state);

            expect(result).toBeTruthy();
        });

        it('should be "/profile/14" route', () => {
            const state = {
                router: {
                    state: {
                        url: '/profile/14',
                    },
                } as RouterReducerState<any>,
            } as RootState;

            const result = isDressingPageVisible(state);

            expect(result).toBeTruthy();
        });

        it('should not be "/profile/infos" route', () => {
            const state = {
                router: {
                    state: {
                        url: '/profile/infos',
                    },
                } as RouterReducerState<any>,
            } as RootState;

            const result = isDressingPageVisible(state);

            expect(result).toBeFalsy();
        });

        it('should not be "/home" route', () => {
            const state = {
                router: {
                    state: {
                        url: '/home',
                    },
                } as RouterReducerState<any>,
            } as RootState;

            const result = isDressingPageVisible(state);

            expect(result).toBeFalsy();
        });
    });

    describe('isCookiesRouteActive', () => {
        it('should be "/other/cookies" route', () => {
            const state = {
                router: {
                    state: {
                        url: '/other/cookies',
                    },
                } as RouterReducerState<any>,
            } as RootState;

            const result = isCookiesRouteActive(state);

            expect(result).toBeTruthy();
        });
    });

    describe('brandSlug', () => {
        it('should return undefined when not on marque page', () => {
            const state = {
                router: {
                    state: {
                        url: '/profile',
                        root: {
                            params: {},
                        },
                    },
                } as RouterReducerState<any>,
            } as RootState;

            const result = brandSlug(state);

            expect(result).toBe(undefined);
        });

        it('should return undefined when not on marque page and with "brands" params', () => {
            const state = {
                router: {
                    state: {
                        url: '/search/kiabi',
                        root: {
                            params: {
                                brands: 'kiabi',
                            },
                        },
                    },
                } as RouterReducerState<any>,
            } as RootState;

            const result = brandSlug(state);

            expect(result).toBe(undefined);
        });

        it('should return "kiabi" when on marque page of "kiabi"', () => {
            const state = {
                router: {
                    state: {
                        url: '/marque/kiabi',
                        root: {
                            params: {
                                brands: 'kiabi',
                            },
                        },
                    },
                } as RouterReducerState<any>,
            } as RootState;

            const result = brandSlug(state);

            expect(result).toBe('kiabi');
        });
    });

    type GetBreadcrumbLinksTestCase = {
        name: string;
        url: string;
        routeParams?: any;
        expected: BreadcrumbLink[];
    };

    describe('getBreadcrumbLinks', () => {
        const testCases: GetBreadcrumbLinksTestCase[] = [
            {
                name: 'home',
                url: '',
                expected: [
                    {
                        commands: undefined,
                        labelKey: 'breadcrumb.home',
                    },
                ],
            },
            {
                name: 'profile',
                url: '/profile',
                expected: [
                    {
                        commands: [''],
                        labelKey: 'breadcrumb.home',
                    },
                    {
                        commands: undefined,
                        labelKey: 'breadcrumb.profile.home',
                    },
                ],
            },
            {
                name: 'my rating',
                url: '/profile/167/rating',
                routeParams: {
                    userId: '167',
                },
                expected: [
                    {
                        commands: [''],
                        labelKey: 'breadcrumb.home',
                    },
                    {
                        commands: ['', 'profile'],
                        labelKey: 'breadcrumb.profile.home',
                    },
                    {
                        commands: undefined,
                        labelKey: 'breadcrumb.profile.my-rating',
                    },
                ],
            },
            {
                name: 'rating',
                url: '/profile/217/rating',
                routeParams: {
                    userId: '217',
                },
                expected: [
                    {
                        commands: [''],
                        labelKey: 'breadcrumb.home',
                    },
                    {
                        commands: ['', 'profile', 217],
                        labelKey: 'breadcrumb.profile.dressing',
                        translateParams: { username: 'Jean' },
                    },
                    {
                        commands: undefined,
                        labelKey: 'breadcrumb.profile.rating',
                        translateParams: { username: 'Jean' },
                    },
                ],
            },
            {
                name: 'edit profile',
                url: '/profile/edit',
                expected: [
                    {
                        commands: [''],
                        labelKey: 'breadcrumb.home',
                    },
                    {
                        commands: ['', 'profile'],
                        labelKey: 'breadcrumb.profile.home',
                    },
                    {
                        commands: undefined,
                        labelKey: 'breadcrumb.profile.edit',
                    },
                ],
            },
            {
                name: 'my dressing',
                url: '/profile/167',
                routeParams: {
                    userId: '167',
                },
                expected: [
                    {
                        commands: [''],
                        labelKey: 'breadcrumb.home',
                    },
                    {
                        commands: undefined,
                        labelKey: 'breadcrumb.profile.home',
                    },
                ],
            },
            {
                name: 'dressing',
                url: '/profile/217',
                routeParams: {
                    userId: '217',
                },
                expected: [
                    {
                        commands: [''],
                        labelKey: 'breadcrumb.home',
                    },
                    {
                        commands: undefined,
                        labelKey: 'breadcrumb.profile.dressing',
                        translateParams: { username: 'Jean' },
                    },
                ],
            },
            {
                name: 'wishlist',
                url: '/profile/wishlist',
                expected: [
                    {
                        commands: [''],
                        labelKey: 'breadcrumb.home',
                    },
                    {
                        commands: ['', 'profile'],
                        labelKey: 'breadcrumb.profile.home',
                    },
                    {
                        commands: undefined,
                        labelKey: 'breadcrumb.profile.wishlist',
                    },
                ],
            },
            {
                name: 'credits',
                url: '/profile/credits',
                expected: [
                    {
                        commands: [''],
                        labelKey: 'breadcrumb.home',
                    },
                    {
                        commands: ['', 'profile'],
                        labelKey: 'breadcrumb.profile.home',
                    },
                    {
                        commands: undefined,
                        labelKey: 'breadcrumb.profile.credits',
                    },
                ],
            },
            {
                name: 'orders',
                url: '/profile/orders',
                expected: [
                    {
                        commands: [''],
                        labelKey: 'breadcrumb.home',
                    },
                    {
                        commands: ['', 'profile'],
                        labelKey: 'breadcrumb.profile.home',
                    },
                    {
                        commands: undefined,
                        labelKey: 'breadcrumb.profile.orders',
                    },
                ],
            },
            {
                name: 'addresses',
                url: '/profile/addresses',
                expected: [
                    {
                        commands: [''],
                        labelKey: 'breadcrumb.home',
                    },
                    {
                        commands: ['', 'profile'],
                        labelKey: 'breadcrumb.profile.home',
                    },
                    {
                        commands: undefined,
                        labelKey: 'breadcrumb.profile.addresses',
                    },
                ],
            },
            {
                name: 'bank-details',
                url: '/profile/bank-details',
                expected: [
                    {
                        commands: [''],
                        labelKey: 'breadcrumb.home',
                    },
                    {
                        commands: ['', 'profile'],
                        labelKey: 'breadcrumb.profile.home',
                    },
                    {
                        commands: undefined,
                        labelKey: 'breadcrumb.profile.bank-details',
                    },
                ],
            },
            {
                name: 'change-password',
                url: '/profile/change-password',
                expected: [
                    {
                        commands: [''],
                        labelKey: 'breadcrumb.home',
                    },
                    {
                        commands: ['', 'profile'],
                        labelKey: 'breadcrumb.profile.home',
                    },
                    {
                        commands: undefined,
                        labelKey: 'breadcrumb.profile.change-password',
                    },
                ],
            },
            {
                name: 'notifications',
                url: '/profile/notifications',
                expected: [
                    {
                        commands: [''],
                        labelKey: 'breadcrumb.home',
                    },
                    {
                        commands: ['', 'profile'],
                        labelKey: 'breadcrumb.profile.home',
                    },
                    {
                        commands: undefined,
                        labelKey: 'breadcrumb.profile.notifications',
                    },
                ],
            },
            {
                name: 'infos',
                url: '/profile/infos',
                expected: [
                    {
                        commands: [''],
                        labelKey: 'breadcrumb.home',
                    },
                    {
                        commands: ['', 'profile'],
                        labelKey: 'breadcrumb.profile.home',
                    },
                    {
                        commands: undefined,
                        labelKey: 'breadcrumb.profile.infos',
                    },
                ],
            },
        ];

        testCases.forEach(({ name, url, expected, routeParams }) => {
            it(`should be ${name} page`, () => {
                const state = {
                    router: {
                        state: {
                            url,
                            root: {
                                params: routeParams || {},
                            },
                        },
                    } as RouterReducerState<any>,
                    entityCache: {
                        userProfile: {
                            userProfile: {
                                id: 167,
                                nickname: 'Me',
                            },
                        },
                        profile: {
                            current: {
                                id: 217,
                                nickname: 'Jean',
                            },
                        },
                    },
                } as RootState;

                const result = getBreadcrumbLinks(state);

                expect(result).toEqual(expected);
            });
        });
    });
});
