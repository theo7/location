import { createSelector } from '@ngrx/store';
import { isDefined } from 'src/app/shared/functions/filter.functions';
import { toNumber } from 'src/app/shared/functions/number.functions';
import { Discussion, Lot, LotStatusEnum } from '../../shared/models/models';
import { getEntityState } from './entity.selectors';
import { getCurrentUserId } from './user.selectors';

export const getDiscussionsState = createSelector(getEntityState, state => state.discussions);
export const getDiscussions = createSelector(getDiscussionsState, state => state.discussions);
const getLots = createSelector(getDiscussions, discussions => discussions.map(d => d.lot).filter(isDefined));
const getLotsOpen = createSelector(getLots, lots => lots.filter(l => l.status === LotStatusEnum.OPEN));

export const getDiscussion = (id: number | string) =>
    createSelector(getDiscussions, (discussions: Discussion[]) => discussions.find(discussion => discussion.id === id));

export const getDiscussionByProductId = (productId: number) =>
    createSelector(getDiscussions, (discussions: Discussion[]) =>
        discussions.find(d => d.lot?.products?.find(product => product.id === toNumber(productId))),
    );

export const getDiscussionsBySellerId = (sellerId: number) =>
    createSelector(getDiscussions, (discussions: Discussion[]) => discussions.filter(d => d.seller?.id === sellerId));

export const getDiscussionBySellerId = (sellerId: number) =>
    createSelector(getDiscussions, (discussions: Discussion[]) => discussions.find(d => d.seller?.id === sellerId));

export const getDiscussionByLotId = (lotId: number) =>
    createSelector(getDiscussions, (discussions: Discussion[]) =>
        discussions.find(discussion => discussion.lot?.id === lotId),
    );

export const getProductIsInLot = (productId: number) =>
    createSelector(getDiscussionByProductId(productId), discussion =>
        discussion ? discussion.lot?.products?.some(product => product.id === toNumber(productId)) : false,
    );

const getLotById = (lotId: number) => createSelector(getLots, lots => lots.find(lot => lot.id === lotId));

const getLotStatusById = (lotId: number) => createSelector(getLotById(lotId), lot => lot?.status);

export const isCancelSellEnabled = (lotId: number) =>
    createSelector(getCurrentUserId, getLotById(lotId), (currentUserId, lot) => {
        if (lot) {
            if (lot.buyer?.id === currentUserId) {
                return lot.status === LotStatusEnum.PREPAID;
            }
            if (lot.seller?.id === currentUserId) {
                return lot.status === LotStatusEnum.PREPAID || lot.status === LotStatusEnum.PAID;
            }
        }
        return false;
    });

const saleInProgressStatus = [
    LotStatusEnum.PREPAID,
    LotStatusEnum.PAID,
    LotStatusEnum.SENT,
    LotStatusEnum.IN_RELAIS,
    LotStatusEnum.RECEIVED,
    LotStatusEnum.COMPLIANT,
    LotStatusEnum.BLOCKED,
];

export const isSaleInProgress = (lotId: number) =>
    createSelector(getLotStatusById(lotId), status => saleInProgressStatus.some(s => s === status));

export const isSaleInProgressWith = (userId: number) =>
    createSelector(getDiscussions, (discussions: Discussion[]) => {
        return discussions
            .filter(d => d.seller?.id === userId || d.buyer?.id === userId)
            .some(d => saleInProgressStatus.some(s => s === d.lot?.status));
    });

const getLotsBySellerId = (sellerId: number) =>
    createSelector(getDiscussionsBySellerId(sellerId), (discussions: Discussion[]) =>
        discussions.map(d => d.lot).filter(isDefined),
    );

export const getLotOpenBySellerId = (sellerId: number) =>
    createSelector(getLotsBySellerId(sellerId), lots => lots.find(l => l.status === LotStatusEnum.OPEN));

const getLotCarts = createSelector(getLotsOpen, getCurrentUserId, (lots, currentUserId) =>
    currentUserId ? lots.filter(l => l.buyer?.id === currentUserId) : [],
);

const getCartForSellerId = (sellerId: number) =>
    createSelector(
        getLotCarts,
        getDiscussionsState,
        (lots: Lot[], state) =>
            lots.filter(l => l.id !== state.instantBuyLotId).find(l => l.seller?.id === sellerId)?.products || [],
    );

export const hasCartForSellerId = (sellerId: number) =>
    createSelector(getCartForSellerId(sellerId), cart => cart.length > 0);

export const getLotOpenByProductId = (productId: number) =>
    createSelector(getLotsOpen, (lots: Lot[]) => lots.find(l => l.products?.map(p => p.id)?.includes(productId)));

export const isProductInLot = (productId: number) => createSelector(getLotOpenByProductId(productId), lot => !!lot);

/**
 * When there is no lot for the seller id (and it is not your own products)
 */
export const canCreateLot = (sellerId: number) =>
    createSelector(hasCartForSellerId(sellerId), cartForSellerId => !cartForSellerId);

/**
 * When there is already a lot (and this product is not in it)
 */
export const canAddToLot = (sellerId: number, productId: number) =>
    createSelector(
        hasCartForSellerId(sellerId),
        isProductInLot(productId),
        (cartForSellerId, productInLot) => cartForSellerId && !productInLot,
    );

/**
 * When there is already a lot (and this product is already in it)
 */
export const canRemoveFromLot = (sellerId: number, productId: number) =>
    createSelector(
        hasCartForSellerId(sellerId),
        isProductInLot(productId),
        (cartForSellerId, productInLot) => cartForSellerId && !!productInLot,
    );

export const isLoading = createSelector(getDiscussionsState, state => state.loading);
