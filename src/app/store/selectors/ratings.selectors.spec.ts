import { UserRating } from 'src/app/shared/models/models';
import { RootState } from '../models';
import { getRatings, getSummary, isLoading, page, totalRatings } from './ratings.selectors';

const ratings: UserRating[] = [
    {
        id: 1,
        value: 5,
        comment: 'Test',
    } as UserRating,
    {
        id: 2,
        value: 3,
        comment: 'Test',
    } as UserRating,
    {
        id: 3,
        value: 4,
        comment: 'Test',
    } as UserRating,
];

describe('ratings.selectors', () => {
    describe('isLoading', () => {
        it('should be loading', () => {
            const state = {
                entityCache: {
                    ratings: {
                        loading: true,
                    },
                },
            } as RootState;

            const result = isLoading(state);

            expect(result).toBeTruthy();
        });
    });

    describe('getSummary', () => {
        it('should return the summary', () => {
            const state = {
                entityCache: {
                    ratings: {
                        summary: {
                            totalRating: 4,
                            averageRating: 3,
                        },
                    },
                },
            } as RootState;

            const result = getSummary(state);

            expect(result).toEqual({
                totalRating: 4,
                averageRating: 3,
            });
        });
    });

    describe('getRatings', () => {
        it('should get ratings', () => {
            const state = {
                entityCache: {
                    ratings: {
                        ratings,
                    },
                },
            } as RootState;

            const result = getRatings(state);

            expect(result.length).toBe(3);
        });
    });

    describe('totalRatings', () => {
        it('should get number of total ratings', () => {
            const state = {
                entityCache: {
                    ratings: {
                        ratings,
                        total: 3,
                    },
                },
            } as RootState;

            const result = totalRatings(state);

            expect(result).toBe(3);
        });
    });

    describe('page', () => {
        it('should get current page', () => {
            const state = {
                entityCache: {
                    ratings: {
                        ratings,
                        total: 3,
                        page: 0,
                    },
                },
            } as RootState;

            const result = page(state);

            expect(result).toBe(0);
        });
    });
});
