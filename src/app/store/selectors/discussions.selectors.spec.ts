import { Discussion, DiscussionStatus, LotStatusEnum, Product } from 'src/app/shared/models/models';
import { RootState } from '../models';
import {
    canAddToLot,
    canCreateLot,
    canRemoveFromLot,
    getDiscussionByLotId,
    getDiscussions,
    getLotOpenByProductId,
    getLotOpenBySellerId,
    hasCartForSellerId,
    isCancelSellEnabled,
    isProductInLot,
} from './discussions.selectors';

const initialDiscussions = [
    {
        id: 1,
        status: DiscussionStatus.DISCUSSION_PENDING,
        buyer: {
            id: 144,
        },
        seller: {
            id: 1,
        },
        currentUserBuyer: true,
        lot: {
            id: 1,
            status: LotStatusEnum.SENT,
        },
    },
    {
        id: 2,
        status: DiscussionStatus.DISCUSSION_PENDING,
        buyer: {
            id: 200,
        },
        seller: {
            id: 2,
        },
        currentUserBuyer: false,
    },
    {
        id: 3,
        status: DiscussionStatus.DISCUSSION_PENDING,
        buyer: {
            id: 200,
        },
        seller: {
            id: 3,
        },
        currentUserBuyer: false,
        lot: {
            id: 4,
            status: LotStatusEnum.PREPAID,
        },
    },
] as Discussion[];

describe('discussions.selectors', () => {
    describe('getDiscussions', () => {
        it('should get all discussions', () => {
            const state = {
                entityCache: {
                    discussions: {
                        discussions: initialDiscussions,
                    },
                },
            } as RootState;

            const result = getDiscussions(state);

            expect(result).toBe(initialDiscussions);
        });
    });

    describe('getDiscussionByLotId', () => {
        it('should get discussion of the first lot', () => {
            const state = {
                entityCache: {
                    discussions: {
                        discussions: initialDiscussions,
                    },
                },
            } as RootState;

            const result = getDiscussionByLotId(1)(state);

            expect(result).toBe(initialDiscussions[0]);
        });
    });

    describe('isCancelSellEnabled', () => {
        const buyerId = 200;
        const sellerId = 3;

        describe('as a buyer', () => {
            const testCases = [
                { lotStatus: LotStatusEnum.BLOCKED, expected: false },
                { lotStatus: LotStatusEnum.CANCELLED, expected: false },
                { lotStatus: LotStatusEnum.COMPLIANT, expected: false },
                { lotStatus: LotStatusEnum.IN_RELAIS, expected: false },
                { lotStatus: LotStatusEnum.OPEN, expected: false },
                { lotStatus: LotStatusEnum.PAID, expected: false },
                { lotStatus: LotStatusEnum.PREPAID, expected: true },
                { lotStatus: LotStatusEnum.RECEIVED, expected: false },
                { lotStatus: LotStatusEnum.RESOLVED, expected: false },
                { lotStatus: LotStatusEnum.SENT, expected: false },
                { lotStatus: LotStatusEnum.SOLD, expected: false },
            ];

            testCases.forEach(({ lotStatus, expected }) => {
                it(`should be ${expected} if lot status is ${lotStatus}`, () => {
                    const state = {
                        entityCache: {
                            userProfile: {
                                userProfile: {
                                    id: buyerId,
                                },
                            },
                            discussions: {
                                discussions: [
                                    {
                                        id: 3,
                                        status: DiscussionStatus.DISCUSSION_PENDING,
                                        buyer: {
                                            id: 200,
                                        },
                                        seller: {
                                            id: 3,
                                        },
                                        currentUserBuyer: false,
                                        lot: {
                                            id: 4,
                                            status: lotStatus,
                                            buyer: {
                                                id: 200,
                                            },
                                            seller: {
                                                id: 3,
                                            },
                                        },
                                    },
                                ],
                            },
                        },
                    } as RootState;

                    const result = isCancelSellEnabled(4)(state);

                    expect(result).toBe(expected);
                });
            });
        });

        describe('as a seller', () => {
            const testCases = [
                { lotStatus: LotStatusEnum.BLOCKED, expected: false },
                { lotStatus: LotStatusEnum.CANCELLED, expected: false },
                { lotStatus: LotStatusEnum.COMPLIANT, expected: false },
                { lotStatus: LotStatusEnum.IN_RELAIS, expected: false },
                { lotStatus: LotStatusEnum.OPEN, expected: false },
                { lotStatus: LotStatusEnum.PAID, expected: true },
                { lotStatus: LotStatusEnum.PREPAID, expected: true },
                { lotStatus: LotStatusEnum.RECEIVED, expected: false },
                { lotStatus: LotStatusEnum.RESOLVED, expected: false },
                { lotStatus: LotStatusEnum.SENT, expected: false },
                { lotStatus: LotStatusEnum.SOLD, expected: false },
            ];

            testCases.forEach(({ lotStatus, expected }) => {
                it(`should be ${expected} if lot status is ${lotStatus}`, () => {
                    const state = {
                        entityCache: {
                            userProfile: {
                                userProfile: {
                                    id: sellerId,
                                },
                            },
                            discussions: {
                                discussions: [
                                    {
                                        id: 3,
                                        status: DiscussionStatus.DISCUSSION_PENDING,
                                        buyer: {
                                            id: 200,
                                        },
                                        seller: {
                                            id: 3,
                                        },
                                        currentUserBuyer: false,
                                        lot: {
                                            id: 4,
                                            status: lotStatus,
                                            buyer: {
                                                id: 200,
                                            },
                                            seller: {
                                                id: 3,
                                            },
                                        },
                                    },
                                ],
                            },
                        },
                    } as RootState;

                    const result = isCancelSellEnabled(4)(state);

                    expect(result).toBe(expected);
                });
            });
        });
    });

    describe('getLotOpenBySellerId', () => {
        it('should not find a lot in initial discussions', () => {
            const state = {
                entityCache: {
                    discussions: {
                        discussions: initialDiscussions,
                    },
                },
            } as RootState;

            const result = getLotOpenBySellerId(1)(state);

            expect(result).toBeUndefined();
        });

        it('should find a lot OPEN by seller id', () => {
            const discussion = {
                id: 1,
                status: DiscussionStatus.DISCUSSION_PENDING,
                seller: {
                    id: 1,
                },
                lot: {
                    id: 1,
                    status: LotStatusEnum.OPEN,
                },
            } as Discussion;

            const state = {
                entityCache: {
                    discussions: {
                        discussions: [discussion],
                    },
                },
            } as RootState;

            const result = getLotOpenBySellerId(1)(state);

            expect(result).toBe(discussion.lot);
        });
    });

    describe('hasCartForSellerId', () => {
        it('should be false if nothing in cart', () => {
            const state = {
                entityCache: {
                    userProfile: {
                        userProfile: {
                            id: 144,
                        },
                    },
                    discussions: {
                        discussions: initialDiscussions,
                    },
                },
            } as RootState;

            const result = hasCartForSellerId(1)(state);

            expect(result).toBeFalsy();
        });

        it('should be true if items in cart for seller 1', () => {
            const products = [
                {
                    id: 1,
                    label: 'Product #1',
                },
                {
                    id: 2,
                    label: 'Product #2',
                },
            ] as Product[];

            const discussion = {
                id: 1,
                status: DiscussionStatus.DISCUSSION_PENDING,
                lot: {
                    id: 1,
                    status: LotStatusEnum.OPEN,
                    buyer: {
                        id: 21,
                    },
                    seller: {
                        id: 1,
                    },
                    products,
                },
            } as Discussion;

            const state = {
                entityCache: {
                    userProfile: {
                        userProfile: {
                            id: 21,
                        },
                    },
                    discussions: {
                        discussions: [discussion],
                    },
                },
            } as RootState;

            const result = hasCartForSellerId(1)(state);

            expect(result).toBeTruthy();
        });
    });

    describe('getLotOpenByProductId', () => {
        it('should not find a lot in initial discussions', () => {
            const state = {
                entityCache: {
                    discussions: {
                        discussions: initialDiscussions,
                    },
                },
            } as RootState;

            const result = getLotOpenByProductId(1)(state);

            expect(result).toBeUndefined();
        });

        it('should find a lot OPEN by product id', () => {
            const discussion = {
                id: 1,
                status: DiscussionStatus.DISCUSSION_PENDING,
                seller: {
                    id: 1,
                },
                lot: {
                    id: 1,
                    status: LotStatusEnum.OPEN,
                    products: [{ id: 1 }, { id: 2 }],
                },
            } as Discussion;

            const state = {
                entityCache: {
                    discussions: {
                        discussions: [discussion],
                    },
                },
            } as RootState;

            const result = getLotOpenByProductId(1)(state);

            expect(result).toBe(discussion.lot);
        });
    });

    describe('isProductInLot', () => {
        it('should be false from initial discussions', () => {
            const state = {
                entityCache: {
                    discussions: {
                        discussions: initialDiscussions,
                    },
                },
            } as RootState;

            const result = isProductInLot(1)(state);

            expect(result).toBeFalsy();
        });

        it('should be true', () => {
            const discussion = {
                id: 1,
                status: DiscussionStatus.DISCUSSION_PENDING,
                seller: {
                    id: 1,
                },
                lot: {
                    id: 1,
                    status: LotStatusEnum.OPEN,
                    products: [{ id: 1 }, { id: 2 }],
                },
            } as Discussion;

            const state = {
                entityCache: {
                    discussions: {
                        discussions: [discussion],
                    },
                },
            } as RootState;

            const result = isProductInLot(1)(state);

            expect(result).toBeTruthy();
        });
    });

    describe('canCreateLot', () => {
        it('should be false if there is already a lot for seller id', () => {
            const products = [
                {
                    id: 1,
                    label: 'Product #1',
                },
                {
                    id: 2,
                    label: 'Product #2',
                },
            ] as Product[];

            const discussion = {
                id: 1,
                status: DiscussionStatus.DISCUSSION_PENDING,
                lot: {
                    id: 1,
                    status: LotStatusEnum.OPEN,
                    buyer: {
                        id: 21,
                    },
                    seller: {
                        id: 1,
                    },
                    products,
                },
            } as Discussion;

            const state = {
                entityCache: {
                    userProfile: {
                        userProfile: {
                            id: 21,
                        },
                    },
                    discussions: {
                        discussions: [discussion],
                    },
                },
            } as RootState;

            const result = canCreateLot(1)(state);

            expect(result).toBeFalsy();
        });
    });

    describe('canAddToLot', () => {
        it('should be true if there is already a lot and the product is not in it', () => {
            const discussion = {
                id: 1,
                status: DiscussionStatus.DISCUSSION_PENDING,
                seller: {
                    id: 1,
                },
                buyer: {
                    id: 21,
                },
                lot: {
                    id: 1,
                    status: LotStatusEnum.OPEN,
                    products: [{ id: 1 }, { id: 2 }],
                    seller: {
                        id: 1,
                    },
                    buyer: {
                        id: 21,
                    },
                },
            } as Discussion;

            const state = {
                entityCache: {
                    userProfile: {
                        userProfile: {
                            id: 21,
                        },
                    },
                    discussions: {
                        discussions: [discussion],
                    },
                },
            } as RootState;

            const result = canAddToLot(1, 3)(state);

            expect(result).toBeTruthy();
        });
    });

    describe('canRemoveFromLot', () => {
        it('should be true if there is already a lot and the product is in it', () => {
            const discussion = {
                id: 1,
                status: DiscussionStatus.DISCUSSION_PENDING,
                seller: {
                    id: 1,
                },
                buyer: {
                    id: 21,
                },
                lot: {
                    id: 1,
                    status: LotStatusEnum.OPEN,
                    products: [{ id: 1 }, { id: 2 }],
                    seller: {
                        id: 1,
                    },
                    buyer: {
                        id: 21,
                    },
                },
            } as Discussion;

            const state = {
                entityCache: {
                    userProfile: {
                        userProfile: {
                            id: 21,
                        },
                    },
                    discussions: {
                        discussions: [discussion],
                    },
                },
            } as RootState;

            const result = canRemoveFromLot(1, 1)(state);

            expect(result).toBeTruthy();
        });
    });
});
