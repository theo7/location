import { createSelector } from '@ngrx/store';
import { AccountStatus, DocumentStatus, IbanStatus, SimplifiedDocumentStatus } from 'src/app/shared/models/models';
import { getEntityState } from './entity.selectors';

export const getBankDetailsState = createSelector(getEntityState, state => state.bankDetails);

export const getIsLoading = createSelector(getBankDetailsState, state => state.loading);

export const getAccount = createSelector(getBankDetailsState, state => state.account);
export const getAccountStatus = createSelector(getBankDetailsState, state => state.account?.status);

export const getIbanDocument = createSelector(getBankDetailsState, state => state.ibanDocument);
export const getIbanDocumentSimplifiedStatus = createSelector(getIbanDocument, document => {
    if (!document) {
        return SimplifiedDocumentStatus.NOT_FILLED;
    }

    switch (document.status) {
        case IbanStatus.WAITING_VERIFICATION:
            return SimplifiedDocumentStatus.TO_BE_VERIFIED;
        case IbanStatus.ACTIVATED:
            return SimplifiedDocumentStatus.ACCEPTED;
        case IbanStatus.REJECTED:
            return SimplifiedDocumentStatus.REJECTED;
        default:
            return SimplifiedDocumentStatus.REJECTED;
    }
});

export const getRibDocument = createSelector(getBankDetailsState, state => state.ribDocument);
export const getRibDocumentSimplifiedStatus = createSelector(getRibDocument, document => {
    if (!document) {
        return SimplifiedDocumentStatus.NOT_FILLED;
    }

    switch (document.status) {
        case DocumentStatus.TO_BE_VERIFIED:
            return SimplifiedDocumentStatus.TO_BE_VERIFIED;
        case DocumentStatus.ACCEPTED:
            return SimplifiedDocumentStatus.ACCEPTED;
        case DocumentStatus.REJECTED:
            return SimplifiedDocumentStatus.REJECTED;
        default:
            return SimplifiedDocumentStatus.REJECTED;
    }
});

export const getIdentityDocument = createSelector(getBankDetailsState, state => state.identityDocument);
export const getIdentityDocumentSimplifiedStatus = createSelector(getIdentityDocument, document => {
    if (!document) {
        return SimplifiedDocumentStatus.NOT_FILLED;
    }

    switch (document.status) {
        case DocumentStatus.TO_BE_VERIFIED:
            return SimplifiedDocumentStatus.TO_BE_VERIFIED;
        case DocumentStatus.ACCEPTED:
            return SimplifiedDocumentStatus.ACCEPTED;
        case DocumentStatus.REJECTED:
            return SimplifiedDocumentStatus.REJECTED;
        default:
            return SimplifiedDocumentStatus.REJECTED;
    }
});

export const isIbanAndRibProcessStarted = createSelector(
    getIbanDocument,
    getRibDocument,
    (ibanDocument, ribDocument) => !!ibanDocument && !!ribDocument,
);

export const isBankAccountInError = createSelector(
    getAccountStatus,
    status => status !== AccountStatus.REGISTERED_KYC1 && status !== AccountStatus.REGISTERED_KYC2,
);
export const isBankAccountCompleted = createSelector(
    getAccountStatus,
    getIbanDocumentSimplifiedStatus,
    getRibDocumentSimplifiedStatus,
    getIdentityDocumentSimplifiedStatus,
    (status, ibanStatus, ribStatus, identityStatus) =>
        status === AccountStatus.REGISTERED_KYC2 &&
        ibanStatus === SimplifiedDocumentStatus.ACCEPTED &&
        ribStatus === SimplifiedDocumentStatus.ACCEPTED &&
        identityStatus === SimplifiedDocumentStatus.ACCEPTED,
);
export const isBankAccountPendingValidation = createSelector(
    getAccountStatus,
    getIbanDocumentSimplifiedStatus,
    getRibDocumentSimplifiedStatus,
    getIdentityDocumentSimplifiedStatus,
    (status, ibanStatus, ribStatus, identityStatus) =>
        status === AccountStatus.REGISTERED_KYC1 &&
        (ibanStatus === SimplifiedDocumentStatus.ACCEPTED || ibanStatus === SimplifiedDocumentStatus.TO_BE_VERIFIED) &&
        (ribStatus === SimplifiedDocumentStatus.ACCEPTED || ribStatus === SimplifiedDocumentStatus.TO_BE_VERIFIED) &&
        (identityStatus === SimplifiedDocumentStatus.ACCEPTED ||
            identityStatus === SimplifiedDocumentStatus.TO_BE_VERIFIED),
);
export const isBankAccountIncomplete = createSelector(
    isBankAccountInError,
    isBankAccountPendingValidation,
    isBankAccountCompleted,
    (bankAccountInError, bankAccountPendingValidation, bankAccountCompleted) =>
        !bankAccountInError && !bankAccountPendingValidation && !bankAccountCompleted,
);
