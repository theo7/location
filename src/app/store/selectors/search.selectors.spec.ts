import { RootState } from '../models';
import { isLastPage, noProductFound } from './search.selectors';

const FIRST_PAGE = 0;
const PAGE_LIMIT = 20;

describe('search.selectors', () => {
    describe('isLastPage', () => {
        it('should be last page if no element', () => {
            const state = {
                entityCache: {
                    search: {
                        page: FIRST_PAGE,
                        limit: PAGE_LIMIT,
                        results: {
                            totalElements: 0,
                        },
                    },
                },
            } as RootState;

            const result = isLastPage(state);

            expect(result).toBeTruthy();
        });

        it('should be last page if on first page', () => {
            const state = {
                entityCache: {
                    search: {
                        page: FIRST_PAGE,
                        limit: PAGE_LIMIT,
                        results: {
                            totalElements: PAGE_LIMIT - 6,
                        },
                    },
                },
            } as RootState;

            const result = isLastPage(state);

            expect(result).toBeTruthy();
        });

        it('should be last page if full page loaded', () => {
            const state = {
                entityCache: {
                    search: {
                        page: FIRST_PAGE,
                        limit: PAGE_LIMIT,
                        results: {
                            totalElements: PAGE_LIMIT,
                        },
                    },
                },
            } as RootState;

            const result = isLastPage(state);

            expect(result).toBeTruthy();
        });

        it('should not be last page if over page limit', () => {
            const state = {
                entityCache: {
                    search: {
                        page: FIRST_PAGE,
                        limit: PAGE_LIMIT,
                        results: {
                            totalElements: PAGE_LIMIT + 1,
                        },
                    },
                },
            } as RootState;

            const result = isLastPage(state);

            expect(result).toBeFalsy();
        });
    });

    describe('noProductFound', () => {
        it('should not be "noProductFound" if loading data', () => {
            const state = {
                entityCache: {
                    search: {
                        loading: true,
                    },
                },
            } as RootState;

            const result = noProductFound(state);

            expect(result).toBeFalsy();
        });

        it('should not be "noProductFound" if have search results', () => {
            const state = {
                entityCache: {
                    search: {
                        loading: false,
                        results: {
                            totalElements: 14,
                        },
                    },
                },
            } as RootState;

            const result = noProductFound(state);

            expect(result).toBeFalsy();
        });

        it('should be "noProductFound" if not loading and no search results', () => {
            const state = {
                entityCache: {
                    search: {
                        loading: false,
                        results: {
                            totalElements: 0,
                        },
                    },
                },
            } as RootState;

            const result = noProductFound(state);

            expect(result).toBeTruthy();
        });
    });
});
