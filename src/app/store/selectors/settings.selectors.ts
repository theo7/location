import { createSelector } from '@ngrx/store';
import { getEntityState } from './entity.selectors';

export const getSettingsState = createSelector(getEntityState, state => state.settings);

export const isLoading = createSelector(getSettingsState, state => state.loading);
export const getSettings = createSelector(getSettingsState, state => state.settings);
export const getShippingFees = createSelector(getSettingsState, state => state.shippingsFees);
