import { UserSettings } from '../actions/settings.actions';
import { RootState } from '../models';
import { getSettings, isLoading } from './settings.selectors';

describe('settings.selectors', () => {
    describe('isLoading', () => {
        it('should be false', () => {
            const state = {
                entityCache: {
                    settings: {
                        loading: false,
                    },
                },
            } as RootState;

            const result = isLoading(state);

            expect(result).toBeFalsy();
        });
    });

    describe('getSettings', () => {
        it('should be settings', () => {
            const settings: UserSettings = {
                acceptCommercialContact: true,
                acceptMailNotification: false,
                acceptPushNotification: true,
            };

            const state = {
                entityCache: {
                    settings: {
                        settings,
                    },
                },
            } as RootState;

            const result = getSettings(state);

            expect(result).toBe(settings);
        });
    });
});
