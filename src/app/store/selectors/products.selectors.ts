import { createSelector } from '@ngrx/store';
import { environment } from '../../../environments/environment';
import { Product, ProductStatusEnum } from '../../shared/models/models';
import { getEntityState } from './entity.selectors';
import { getIsCurrentUser } from './user.selectors';

const getProductState = createSelector(getEntityState, state => state.product);

// basic selectors
const loading = createSelector(getProductState, state => state.loading);
const product = createSelector(getProductState, state => state.product);

// views selectors
const getViewsState = createSelector(getProductState, state => state.views);

const viewsLoading = createSelector(getViewsState, views => views.loading);
const viewsValue = createSelector(getViewsState, views => views.value);

// dressing selectors
const getDressingState = createSelector(getProductState, state => state.dressing);

const dressingProducts = createSelector(getDressingState, dressing => dressing.products);
const dressingTotalProducts = createSelector(getDressingState, dressing => dressing.total);
const dressingCountProducts = createSelector(getDressingState, dressing => dressing.count);
const dressingLoading = createSelector(getDressingState, dressing => dressing.loading);
const dressingPage = createSelector(getDressingState, dressing => dressing.page);

const hasProductInDressing = createSelector(getDressingState, dressing => dressing.count > 0);
const hasOneProductInDressing = createSelector(getDressingState, dressing => dressing.total > 0);
const hasMoreThanOneProductInDressing = createSelector(getDressingState, dressing => dressing.total > 1);
const showDressingFilter = createSelector(dressingTotalProducts, count => count > environment.minimumDressingProducts);
const noProductInDressingFound = createSelector(
    getDressingState,
    dressing => dressing.count === 0 && !dressing.loading,
);

export const isCurrentUserWithoutProduct = (userId: number) =>
    createSelector(
        getIsCurrentUser(userId),
        hasProductInDressing,
        (_isCurrentUser, _productsInDressing) => _isCurrentUser && !_productsInDressing,
    );

export const productsInDressingToDisplay = (userId: number, hiddenProducts: Product[] = []) =>
    createSelector(getIsCurrentUser(userId), dressingProducts, (_isCurrentUser, _productsInDressing) =>
        _productsInDressing
            .filter(p => !hiddenProducts.some(hiddenProduct => hiddenProduct.id === p.id))
            .filter(p => getIsCurrentUser || p.productStatus === ProductStatusEnum.AVAILABLE),
    );

export const productSelectors = {
    loading,
    product,
    viewsLoading,
    viewsValue,
    dressingProducts,
    dressingTotalProducts,
    dressingCountProducts,
    dressingPage,
    dressingLoading,
    hasProductInDressing,
    hasOneProductInDressing,
    hasMoreThanOneProductInDressing,
    showDressingFilter,
    noProductInDressingFound,
};
