import { getSelectors, RouterReducerState } from '@ngrx/router-store';
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { isEmpty } from 'lodash-es';
import { BreadcrumbLink } from 'src/app/shared/models/models';
import { toNumber } from '../../shared/functions/number.functions';
import { RootState } from '../models';
import { profileSelectors } from './profile.selectors';
import { getCurrentUserId } from './user.selectors';

export const selectRouter = createFeatureSelector<RootState, RouterReducerState<any>>('router');

export const {
    selectCurrentRoute, // select the current route
    selectQueryParams, // select the current route query params
    selectQueryParam, // factory function to select a query param
    selectRouteParams, // select the current route params
    selectRouteParam, // factory function to select a route param
    selectRouteData, // select the current route data
    selectUrl, // select the current url
} = getSelectors(selectRouter);

export const isEmptyQueryParams = createSelector(selectQueryParams, params => isEmpty(params));

export const selectPageParams = createSelector(selectRouteParams, selectQueryParams, (routeParams, queryParams) => ({
    ...queryParams,
    ...routeParams,
}));

export const isHomeRouteActive = createSelector(selectUrl, url => /^\/([?]=[\d]*)?$/.test(url));
export const isSearchRouteActive = createSelector(selectUrl, url => /^\/search/.test(url));
export const isMarqueRouteActive = createSelector(selectUrl, url => /^\/marque\/.+/.test(url));
export const isSellRouteActive = createSelector(selectUrl, url => url === '/add');
export const isChatRouteActive = createSelector(selectUrl, url => /^\/chat/.test(url));
export const isProfileRouteActive = createSelector(selectUrl, url => url === '/profile');
export const isUserProfileRouteActive = createSelector(selectUrl, url => /^\/profile\/[\d]+/.test(url));
export const isRatingRouteActive = createSelector(selectUrl, url => /^\/profile\/[\d]+\/rating$/.test(url));
export const isDressingPageVisible = createSelector(
    isProfileRouteActive,
    isUserProfileRouteActive,
    (profileRouteActive, userProfileRouteActive) => profileRouteActive || userProfileRouteActive,
);
export const isMaintenanceRouteActive = createSelector(selectUrl, url => url === '/maintenance');
export const isChatRoomActive = createSelector(selectUrl, url => /^\/chat\/(\d+|kiabi)$/.test(url));
export const isProductRouteActive = createSelector(selectUrl, url => /^\/product\/[\d]+/.test(url));
export const isCookiesRouteActive = createSelector(selectUrl, url => /^\/other\/cookies$/.test(url));
export const isSearchEnabled = createSelector(
    isSearchRouteActive,
    isMarqueRouteActive,
    (searchRouteActive, marqueRouteActive) => searchRouteActive || marqueRouteActive,
);
export const isOneOfFilterRoutes = createSelector(
    isSearchRouteActive,
    isUserProfileRouteActive,
    isProductRouteActive,
    (isSearchRoute, isUserRoute, isProductRoute) => isSearchRoute || isUserRoute || isProductRoute,
);

// return either string or number
export const roomId = createSelector(selectRouteParam('roomId'), roomIdStr => {
    const result = toNumber(roomIdStr);
    return result !== undefined && !isNaN(result) ? result : roomIdStr;
});
export const lotId = createSelector(selectRouteParam('lotId'), toNumber);
export const productId = createSelector(selectRouteParam('productId'), toNumber);
export const getUserId = createSelector(selectRouteParam('userId'), toNumber);
const brands = selectRouteParam('brands');
export const getTabParam = createSelector(selectQueryParam('tab'), toNumber);

export const brandSlug = createSelector(isMarqueRouteActive, brands, (marqueRouteActive, slug) =>
    marqueRouteActive ? slug : undefined,
);

export const redirectUrl = selectQueryParam('redirectUrl');
export const token = createSelector(
    selectRouteParam('token'),
    selectQueryParam('token'),
    (tokenFromRoute, tokenFromQuery) => tokenFromRoute || tokenFromQuery,
);
export const responseWkToken = selectQueryParam('response_wkToken');

const NO_FOOTER_PAGES = [/^\/payment-callback/, /^\/maintenance/, /^\/profile\/credits/, /^\/chat\/[\d]+$/];
export const displayBottomNavigation = createSelector(selectUrl, url =>
    NO_FOOTER_PAGES.every(regex => !regex.test(url)),
);

export const getBreadcrumbLinks = createSelector(
    selectUrl,
    isUserProfileRouteActive,
    isRatingRouteActive,
    getUserId,
    getCurrentUserId,
    profileSelectors.current,
    (url, userProfileRouteActive, ratingRouteActive, userId, currentUserId, currentProfile) => {
        const splittedUrl = url.split('/');
        const lastIndex = splittedUrl.length - 1;

        if (userProfileRouteActive) {
            if (ratingRouteActive) {
                if (userId === currentUserId) {
                    return [
                        {
                            commands: [''],
                            labelKey: 'breadcrumb.home',
                        },
                        {
                            commands: ['', 'profile'],
                            labelKey: 'breadcrumb.profile.home',
                        },
                        {
                            commands: undefined,
                            labelKey: 'breadcrumb.profile.my-rating',
                        },
                    ];
                }

                return [
                    {
                        commands: [''],
                        labelKey: 'breadcrumb.home',
                    },
                    {
                        commands: ['', 'profile', userId || ''],
                        labelKey: 'breadcrumb.profile.dressing',
                        translateParams: { username: currentProfile?.nickname },
                    },
                    {
                        commands: undefined,
                        labelKey: 'breadcrumb.profile.rating',
                        translateParams: { username: currentProfile?.nickname },
                    },
                ];
            }

            if (userId === currentUserId) {
                return [
                    {
                        commands: [''],
                        labelKey: 'breadcrumb.home',
                    },
                    {
                        commands: undefined,
                        labelKey: 'breadcrumb.profile.home',
                    },
                ];
            }

            return [
                {
                    commands: [''],
                    labelKey: 'breadcrumb.home',
                },
                {
                    commands: undefined,
                    labelKey: 'breadcrumb.profile.dressing',
                    translateParams: { username: currentProfile?.nickname },
                },
            ];
        }

        return splittedUrl.map((path, index) => {
            const commands = lastIndex === index ? undefined : splittedUrl.slice(0, index + 1);

            let labelKey = 'breadcrumb.';
            if (index === 0) {
                labelKey += 'home';
            }
            if (index === 1) {
                labelKey += path + '.home';
            }
            if (index > 1) {
                labelKey += splittedUrl.slice(1, index + 1).join('.');
            }

            return {
                commands,
                labelKey,
            } as BreadcrumbLink;
        });
    },
);
