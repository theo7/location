import { RootState } from '../models';
import { productSelectors } from './products.selectors';

describe('products.selectors', () => {
    describe('getViewsLoading', () => {
        it('should be false', () => {
            const state = {
                entityCache: {
                    product: {
                        views: {
                            loading: false,
                        },
                    },
                },
            } as RootState;

            const result = productSelectors.viewsLoading(state);

            expect(result).toBeFalsy();
        });

        it('should be true', () => {
            const state = {
                entityCache: {
                    product: {
                        views: {
                            loading: true,
                        },
                    },
                },
            } as RootState;

            const result = productSelectors.viewsLoading(state);

            expect(result).toBeTruthy();
        });
    });

    describe('getViews', () => {
        it('should be undefined', () => {
            const state = {
                entityCache: {
                    product: {
                        views: {},
                    },
                },
            } as RootState;

            const result = productSelectors.viewsValue(state);

            expect(result).toBeUndefined();
        });

        it('should be 233', () => {
            const state = {
                entityCache: {
                    product: {
                        views: {
                            value: 233,
                        },
                    },
                },
            } as RootState;

            const result = productSelectors.viewsValue(state);

            expect(result).toBe(233);
        });
    });
});
