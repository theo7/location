import { createSelector } from '@ngrx/store';
import { getEntityState } from './entity.selectors';

export const getSearchState = createSelector(getEntityState, state => state.search);
export const getLoading = createSelector(getSearchState, state => state.loading);
export const getFilterResults = createSelector(getSearchState, state => state.results);

export const getTotalElements = createSelector(getFilterResults, results => results.totalElements);
export const isFirstPage = createSelector(getSearchState, search => search.page === 0);
export const isLastPage = createSelector(
    getSearchState,
    search => search.results.totalElements <= (search.page + 1) * search.limit,
);

export const noProductFound = createSelector(
    getLoading,
    getFilterResults,
    (loading, results) => !loading && results.totalElements <= 0,
);

export const getPreSearchTotalElements = createSelector(getSearchState, search => search.preSearchTotalElements);

export const isShowingFilter = createSelector(getSearchState, state => state.showFilter);
export const isRefresherActive = createSelector(getSearchState, state => state.activateRefresher);
