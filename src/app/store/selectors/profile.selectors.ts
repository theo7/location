import { createSelector } from '@ngrx/store';
import { getEntityState } from './entity.selectors';

const getProfileState = createSelector(getEntityState, state => state.profile);

const current = createSelector(getProfileState, profile => profile.current);
const loading = createSelector(getProfileState, profile => profile.loading);

const isFollowing = createSelector(current, current => !!current?.isFollowing);

export const profileSelectors = {
    current,
    loading,
    isFollowing,
};
