import { createSelector } from '@ngrx/store';
import { environment } from 'src/environments/environment';
import { ModerationStatus, ProductId, UserProfileStatus, UserRole, UserType } from '../../shared/models/models';
import { getEntityState } from './entity.selectors';

export const getUserProfileState = createSelector(getEntityState, state => state.userProfile);
export const getUserProfile = createSelector(getUserProfileState, state => state.userProfile);

export const getIsCurrentUser = (userId: number) =>
    createSelector(getUserProfile, userProfile => userProfile?.id === userId);

export const getUserIsModerated = createSelector(getUserProfile, userProfile => {
    if (userProfile) {
        return (
            userProfile.presentationStatus === ModerationStatus.MODERATED ||
            userProfile.picture?.status === ModerationStatus.MODERATED ||
            userProfile.dressingModerated
        );
    }
    return false;
});
export const getUserProfileIsModerated = createSelector(getUserProfile, userProfile => {
    if (userProfile) {
        return (
            userProfile.presentationStatus === ModerationStatus.MODERATED ||
            userProfile.picture?.status === ModerationStatus.MODERATED
        );
    }
    return false;
});
export const getUserDressingIsModerated = createSelector(getUserProfile, userProfile => {
    return userProfile && userProfile.dressingModerated;
});
export const getCurrentUserId = createSelector(getUserProfile, userProfile => userProfile?.id);
export const getMail = createSelector(getUserProfile, userProfile => userProfile?.email);
export const getAddresses = createSelector(getUserProfile, userProfile => userProfile?.addresses || []);
export const getHasAddresses = createSelector(getAddresses, addresses => addresses.length > 0);
export const getDefaultAddress = createSelector(getAddresses, addresses => addresses.find(a => a.isDefault));
export const isLogged = createSelector(getUserProfile, userProfile => !!userProfile);
export const isNotLogged = createSelector(isLogged, logged => !logged);
export const isLoading = createSelector(getUserProfileState, state => state.loading);
export const isError = createSelector(getUserProfileState, state => !!state.error);
export const getError = createSelector(getUserProfileState, state => state.errorReason);

const getPicture = createSelector(getUserProfile, userProfile => userProfile?.picture);
export const getAvatarURL = createSelector(getPicture, picture => {
    const avatarFileName = picture?.fileName;

    // TODO : refactor
    const getStaticUrl = (fileName: string | undefined) => {
        return !!fileName ? `assets/images/${fileName}` : '';
    };

    // TODO : refactor
    const getUrl = (fileName: string | undefined) => {
        const s3Url = environment.s3url;
        return !!fileName ? `${s3Url}${fileName}` : '';
    };

    if (!!picture?.static) {
        return getStaticUrl(avatarFileName);
    }
    if (avatarFileName) {
        return getUrl(avatarFileName);
    }
    return '';
});

export const isProfessional = createSelector(
    getUserProfile,
    userProfile => userProfile?.type === UserType.PROFESSIONAL,
);
export const isAdmin = createSelector(getUserProfile, userProfile => userProfile?.role === UserRole.ADMIN);
export const isIndividual = createSelector(isProfessional, professional => !professional);

export const isBlockedByAdmin = createSelector(
    getUserProfile,
    userProfile => userProfile?.status === UserProfileStatus.BLOCKED,
);
export const isNotBlockedByAdmin = createSelector(
    getUserProfile,
    userProfile => userProfile?.status !== UserProfileStatus.BLOCKED,
);

export const getFavoriteProductsIds = createSelector(
    getUserProfile,
    userProfile => userProfile?.favoriteProductsIds || [],
);

export const isProductFavorite = (id: ProductId) =>
    createSelector(
        getFavoriteProductsIds,
        (favoriteProductsIds: ProductId[]) => favoriteProductsIds && favoriteProductsIds.includes(id),
    );

const getBlockedUserIds = createSelector(getUserProfile, userProfile => userProfile?.blockedUserIds || []);

export const isBlocked = (_id: number) =>
    createSelector(getBlockedUserIds, (blockedUserIds: number[]) => blockedUserIds.some(id => id === _id));

export const canCreateLot = (userId: number) =>
    createSelector(
        getIsCurrentUser(userId),
        isNotBlockedByAdmin,
        (currentUser: boolean, notBlockedByAdmin: boolean) => !currentUser && notBlockedByAdmin,
    );

export const isInVacationMode = createSelector(
    getUserProfile,
    userProfile => userProfile?.status === UserProfileStatus.VACATION,
);

export const isNotInVacationMode = createSelector(
    getUserProfile,
    userProfile => userProfile?.status !== UserProfileStatus.VACATION,
);
