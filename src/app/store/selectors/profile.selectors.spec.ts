import { RootState } from '../models';
import { profileSelectors } from './profile.selectors';

describe('profile.selectors', () => {
    const state = {
        entityCache: {
            profile: {
                current: { id: 1 },
                loading: false,
            },
        },
    } as RootState;

    describe('getCurrent', () => {
        it('should return viewed user', () => {
            const result = profileSelectors.current(state);

            expect(result?.id).toBe(1);
        });
    });

    describe('isLoading', () => {
        it('should get loading state', () => {
            const result = profileSelectors.loading(state);

            expect(result).toBeFalsy();
        });
    });
});
