import { Params } from '@angular/router';
import { createSelector } from '@ngrx/store';
import { countFilter, isSearchEqual } from '../../shared/functions/product-filter.functions';
import { CreateSearch, FilterType } from '../../shared/models/models';
import { FiltersState, initialProductFilterState } from '../reducers/filters.reducer';
import { getEntityState } from './entity.selectors';

const getState = createSelector(getEntityState, state => state.filters);
const getFilter = (key: FilterType) => createSelector(getState, (state: FiltersState) => state[key]);

// GETTER
const getKeyword = (key: FilterType) => createSelector(getFilter(key), filter => filter.searchText);
const getCategory = (key: FilterType) => createSelector(getFilter(key), filter => filter.category);
const getSubCategory = (key: FilterType) => createSelector(getFilter(key), filter => filter.subCategory);
const getProductTypes = (key: FilterType) => createSelector(getFilter(key), filter => filter.productsTypes);
const getSizes = (key: FilterType) => createSelector(getFilter(key), filter => filter.sizes);
const getBrands = (key: FilterType) => createSelector(getFilter(key), filter => filter.brands);
const getColors = (key: FilterType) => createSelector(getFilter(key), filter => filter.colors);
const getConditions = (key: FilterType) => createSelector(getFilter(key), filter => filter.conditions);
const getMinPrice = (key: FilterType) => createSelector(getFilter(key), filter => filter.minPrice);
const getMaxPrice = (key: FilterType) => createSelector(getFilter(key), filter => filter.maxPrice);
const getSeasonality = (key: FilterType) => createSelector(getFilter(key), filter => filter.seasonality);
const getSort = (key: FilterType) => createSelector(getFilter(key), filter => filter.sort);
const getSearchHistory = createSelector(getState, state => state.searchHistory);
const getRecentSearch = createSelector(getSearchHistory, searchHistory => searchHistory.recent);
const getSavedSearch = createSelector(getSearchHistory, searchHistory => searchHistory.saved);
const isSearchLoading = createSelector(getSearchHistory, searchHistory => searchHistory.loading);

// UTILITY
const count = (key: FilterType) => createSelector(getFilter(key), filter => countFilter(filter));

const isEmpty = (key: FilterType) => createSelector(count(key), counter => counter === 0);

const hasFilter = (key: FilterType) => createSelector(count(key), counter => counter > 0);

const hasSort = (key: FilterType) =>
    createSelector(getFilter(key), filter => {
        const sort = filter.sort;
        const initialSort = initialProductFilterState.sort;

        return sort?.field !== initialSort?.field || sort?.order !== initialSort?.order;
    });

const hasKeyword = (key: FilterType) => createSelector(getKeyword(key), keyword => !!keyword && keyword.length > 0);
const hasCategory = (key: FilterType) => createSelector(getCategory(key), category => !!category);
const hasSubCategory = (key: FilterType) => createSelector(getSubCategory(key), subCategory => !!subCategory);
const hasProductTypes = (key: FilterType) => createSelector(getProductTypes(key), types => !!types && types.length > 0);

const asRouterParams = (key: FilterType) =>
    createSelector(getFilter(key), filter => {
        const queryParams: Params = {
            category: filter.category?.id,
            subCategory: filter.subCategory?.id,
            productsTypes: filter.productsTypes?.map(type => type.id).join(',') || undefined,
            brands: filter.brands?.map(brand => brand.id).join(',') || undefined,
            sizes: filter.sizes?.map(size => size.id).join(',') || undefined,
            colors: filter.colors?.map(color => color.id).join(',') || undefined,
            conditions: filter.conditions?.map(condition => condition.id).join(',') || undefined,
            seasonality: filter.seasonality !== 'ALL' ? filter.seasonality : undefined,
            minPrice: filter.minPrice,
            maxPrice: filter.maxPrice,
            searchText: filter.searchText || undefined,
        };

        if (filter.sort?.field !== 'creationDate' || filter.sort?.order !== 'desc') {
            const fieldKey = 'sortField';
            const sortKey = 'sortOrder';
            queryParams[fieldKey] = filter.sort?.field;
            queryParams[sortKey] = filter.sort?.order;
        }

        return queryParams;
    });

const hasCategoryAttribute = (key: FilterType) =>
    createSelector(
        hasCategory(key),
        hasSubCategory(key),
        hasProductTypes(key),
        (hasCategoryFilter, hasSubCategoryFilter, hasProductTypesFilter) => {
            return hasCategoryFilter || hasSubCategoryFilter || hasProductTypesFilter;
        },
    );

const isCurrentCategorySelected = (key: FilterType, id: number) =>
    createSelector(getCategory(key), category => {
        return category?.id === id;
    });

const isCurrentSubCategorySelected = (key: FilterType, id: number) =>
    createSelector(getSubCategory(key), subCategory => {
        return subCategory?.id === id;
    });

const isSearchAlreadyInHistory = (search: CreateSearch) =>
    createSelector(getSearchHistory, ({ recent, saved }) => {
        return recent.some(r => isSearchEqual(search, r)) || saved.some(s => isSearchEqual(search, s));
    });

const getSearchInHistory = (search: CreateSearch) =>
    createSelector(getSearchHistory, ({ recent, saved }) => {
        return [...saved, ...recent].find(r => isSearchEqual(search, r));
    });

export const filtersSelectors = {
    getFilter,
    asRouterParams,
    count,
    isEmpty,
    hasFilter,
    hasSort,
    getKeyword,
    hasKeyword,
    getCategory,
    hasCategory,
    getSubCategory,
    hasSubCategory,
    getProductTypes,
    hasProductTypes,
    hasCategoryAttribute,
    isCurrentCategorySelected,
    isCurrentSubCategorySelected,
    getSizes,
    getBrands,
    getColors,
    getConditions,
    getMinPrice,
    getMaxPrice,
    getSeasonality,
    getSort,
    getRecentSearch,
    getSavedSearch,
    isSearchLoading,
    isSearchAlreadyInHistory,
    getSearchInHistory,
};
