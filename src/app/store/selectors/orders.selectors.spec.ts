import { Lot } from 'src/app/shared/models/models';
import { RootState } from '../models';
import { loading, purchases, sales } from './orders.selectors';

const lots = [
    {
        id: 1,
        lastModifiedDate: 1613656245000,
    },
    {
        id: 2,
        lastModifiedDate: 1613656245020,
    },
    {
        id: 3,
        lastModifiedDate: 1613656245010,
    },
] as Lot[];

describe('orders.selectors', () => {
    describe('sales', () => {
        it('should be empty array', () => {
            const state = {
                entityCache: {
                    orders: {
                        sales: {
                            values: [] as Lot[],
                        },
                    },
                },
            } as RootState;

            const result = sales(state);

            expect(result).toEqual([]);
        });

        it('should be lots provided', () => {
            const state = {
                entityCache: {
                    orders: {
                        sales: {
                            values: lots,
                        },
                    },
                },
            } as RootState;

            const result = sales(state);

            expect(result).toEqual([
                {
                    id: 2,
                    lastModifiedDate: 1613656245020,
                },
                {
                    id: 3,
                    lastModifiedDate: 1613656245010,
                },
                {
                    id: 1,
                    lastModifiedDate: 1613656245000,
                },
            ] as Lot[]);
        });
    });

    describe('purchases', () => {
        it('should be empty array', () => {
            const state = {
                entityCache: {
                    orders: {
                        purchases: {
                            values: [] as Lot[],
                        },
                    },
                },
            } as RootState;

            const result = purchases(state);

            expect(result).toEqual([]);
        });

        it('should be lots provided', () => {
            const state = {
                entityCache: {
                    orders: {
                        purchases: {
                            values: lots,
                        },
                    },
                },
            } as RootState;

            const result = purchases(state);

            expect(result).toEqual([
                {
                    id: 2,
                    lastModifiedDate: 1613656245020,
                },
                {
                    id: 3,
                    lastModifiedDate: 1613656245010,
                },
                {
                    id: 1,
                    lastModifiedDate: 1613656245000,
                },
            ] as Lot[]);
        });
    });

    describe('loading', () => {
        it('should be false if not loading sales nor loading purchases', () => {
            const state = {
                entityCache: {
                    orders: {
                        sales: {
                            loading: false,
                        },
                        purchases: {
                            loading: false,
                        },
                    },
                },
            } as RootState;

            const result = loading(state);

            expect(result).toBeFalsy();
        });

        it('should be true if only loading sales', () => {
            const state = {
                entityCache: {
                    orders: {
                        sales: {
                            loading: true,
                        },
                        purchases: {
                            loading: false,
                        },
                    },
                },
            } as RootState;

            const result = loading(state);

            expect(result).toBeTruthy();
        });

        it('should be true if only loading purchases', () => {
            const state = {
                entityCache: {
                    orders: {
                        sales: {
                            loading: false,
                        },
                        purchases: {
                            loading: true,
                        },
                    },
                },
            } as RootState;

            const result = loading(state);

            expect(result).toBeTruthy();
        });

        it('should be true if loading both sales an purchases', () => {
            const state = {
                entityCache: {
                    orders: {
                        sales: {
                            loading: true,
                        },
                        purchases: {
                            loading: true,
                        },
                    },
                },
            } as RootState;

            const result = loading(state);

            expect(result).toBeTruthy();
        });
    });
});
