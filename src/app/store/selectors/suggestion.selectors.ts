import { createSelector } from '@ngrx/store';
import { getEntityState } from './entity.selectors';

const getSuggestionState = createSelector(getEntityState, state => state.suggestion);

export const isLoading = createSelector(getSuggestionState, state => state.loading);

export const getSuggestion = createSelector(getSuggestionState, state => state.products);
export const totalProducts = createSelector(getSuggestionState, state => state.total);
export const page = createSelector(getSuggestionState, state => state.page);
