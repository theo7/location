import { ProductType } from '../../shared/models/models';
import { RootState } from '../models';
import { searchFilterSelectors } from './search-filters.selectors';

describe('search-filters.selectors', () => {
    describe('getSelectableConditions', () => {
        it('should get selectable conditions', () => {
            // Given
            const state = {
                entityCache: {
                    repositories: { conditions: [{ id: 1 }, { id: 2 }, { id: 3 }] },
                    filters: { search: { conditions: [{ id: 2 }] } },
                },
            } as RootState;

            // Call
            const result = searchFilterSelectors.getSelectableConditions('search')(state);

            // Then
            expect(result).toEqual([
                { id: 1, selected: false },
                { id: 2, selected: true },
                { id: 3, selected: false },
            ]);
        });
    });

    describe('getSelectableColors', () => {
        it('should get selectable colors', () => {
            // Given
            const state = {
                entityCache: {
                    repositories: { colors: [{ id: 1 }, { id: 2 }, { id: 3 }] },
                    filters: { search: { colors: [{ id: 2 }] } },
                },
            } as RootState;

            // Call
            const result = searchFilterSelectors.getSelectableColors('search')(state);

            // Then
            expect(result).toEqual([
                { id: 1, selected: false },
                { id: 2, selected: true },
                { id: 3, selected: false },
            ]);
        });
    });

    describe('getSelectableSizeGrids', () => {
        it('should get selectable size grids', () => {
            // Given
            const productTypes = [
                {
                    id: 100,
                    sizeGrid: {
                        id: 1000,
                        sizes: [{ id: 10000 }, { id: 10001 }, { id: 10002 }],
                    },
                },
            ] as ProductType[];

            const state = {
                entityCache: {
                    categories: {
                        categories: [
                            {
                                id: 1,
                                subCategories: [
                                    {
                                        id: 10,
                                        productTypes,
                                    },
                                ],
                            },
                        ],
                    },
                    repositories: { colors: [{ id: 1 }, { id: 2 }, { id: 3 }] },
                    filters: {
                        search: {
                            subCategory: { id: 10, productTypes },
                            sizes: [{ id: 10001 }],
                        },
                    },
                },
            } as RootState;

            // Call
            const result = searchFilterSelectors.getSelectableSizeGrids('search')(state);

            // Then
            expect(result).toEqual([
                {
                    id: 1000,
                    category: { id: 1 },
                    sizes: [
                        { id: 10000, selected: false },
                        { id: 10001, selected: true },
                        { id: 10002, selected: false },
                    ],
                },
            ]);
        });
    });

    describe('getSelectableSubCategories', () => {
        it('should get selectable sub-categories', () => {
            // Given
            const state = {
                entityCache: {
                    filters: {
                        search: {
                            category: {
                                id: 1,
                                subCategories: [{ id: 1 }, { id: 2 }, { id: 3 }],
                            },
                            subCategory: { id: 2 },
                        },
                    },
                },
            } as RootState;

            // Call
            const result = searchFilterSelectors.getSelectableSubCategories('search')(state);

            // Then
            expect(result).toEqual([
                { id: 1, selected: false },
                { id: 2, selected: true },
                { id: 3, selected: false },
            ]);
        });
    });

    describe('getSelectableProductsTypes', () => {
        it('should get selectable products types', () => {
            // Given
            const state = {
                entityCache: {
                    filters: {
                        search: {
                            subCategory: { id: 1, productTypes: [{ id: 1 }, { id: 2 }, { id: 3 }] },
                            productsTypes: [{ id: 2 }],
                        },
                    },
                },
            } as RootState;

            // Call
            const result = searchFilterSelectors.getSelectableProductsTypes('search')(state);

            // Then
            expect(result).toEqual([
                { id: 1, selected: false },
                { id: 2, selected: true },
                { id: 3, selected: false },
            ]);
        });
    });
});
