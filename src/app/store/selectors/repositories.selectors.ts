import { createSelector } from '@ngrx/store';
import { Brand } from 'src/app/shared/models/models';
import { getEntityState } from './entity.selectors';

export const getRepositoriesState = createSelector(getEntityState, state => state.repositories);
export const getBrands = createSelector(getRepositoriesState, state => state.brands);
export const getDefaultBrand = createSelector(getRepositoriesState, state => state.defaultBrand);

export const getBrandBySlug = (slug: string) =>
    createSelector(getBrands, (brands: Brand[]) => brands.find(b => b.slug === slug));
export const getConditions = createSelector(getRepositoriesState, state => state.conditions);
export const getColors = createSelector(getRepositoriesState, state =>
    [...state.colors].sort((c1, c2) => {
        if (c1.hexCode && c2.hexCode) {
            return (
                parseInt(c2.hexCode.slice(0, 2), 16) +
                parseInt(c2.hexCode.slice(2, 4), 16) +
                parseInt(c2.hexCode.slice(4, 6), 16) -
                parseInt(c1.hexCode.slice(0, 2), 16) -
                parseInt(c1.hexCode.slice(2, 4), 16) -
                parseInt(c1.hexCode.slice(4, 6), 16)
            );
        }

        return 0;
    }),
);

export const getColorById = (id: number) => createSelector(getColors, colors => colors.find(c => c.id === id));
export const getConditionById = (id: number) =>
    createSelector(getConditions, conditions => conditions.find(c => c.id === id));
export const getBrandById = (id: number) => createSelector(getBrands, brands => brands.find(b => b.id === id));
export const getPopularBrands = createSelector(getRepositoriesState, state => state.popularBrands);
