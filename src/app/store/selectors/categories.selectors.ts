import { createSelector } from '@ngrx/store';
import { uniqBy } from 'lodash-es';
import { Category, Size, SizeGrid } from '../../shared/models/models';
import { getEntityState } from './entity.selectors';

export const getCategoriesState = createSelector(getEntityState, state => state.categories);

export const getAllCategories = createSelector(getCategoriesState, state => state.categories);

export const getSubCategories = (categoryId: number) =>
    createSelector(getAllCategories, categories => {
        return categories.find(c => c.id === categoryId)?.subCategories || [];
    });

export const getProductTypes = (categoryId: number, subCategoryId: number) =>
    createSelector(getSubCategories(categoryId), subCategories => {
        return subCategories.find(sc => sc.id === subCategoryId)?.productTypes || [];
    });

export const getAllSizeGrids = createSelector(getAllCategories, (categories): SizeGrid[] => {
    const sizeGridsWithDuplicates = categories
        .flatMap(category =>
            (category.subCategories || []).map(sc => ({
                ...sc,
                category: { id: category.id, labelKey: category.labelKey, orderNumber: category.orderNumber },
            })),
        )
        .flatMap(subCategory => (subCategory.productTypes || []).map(pt => ({ ...pt, category: subCategory.category })))
        .flatMap(productType => ({ ...productType.sizeGrid, category: productType.category }));
    return uniqBy(sizeGridsWithDuplicates, 'id');
});

export const getAllSizes = createSelector(getAllSizeGrids, (sizeGrids): Size[] => {
    return sizeGrids.flatMap(sizeGrid => sizeGrid.sizes || []);
});

export const getIndexedCategories = createSelector(getCategoriesState, state =>
    state.categories.filter(category => category.indexed),
);

export const getCategory = (id: number) =>
    createSelector(getAllCategories, (categories: Category[]) => categories.find(category => category.id === id));

export const getSubCategory = (categoryId: number, id: number) =>
    createSelector(getCategory(categoryId), category => category?.subCategories?.find(sc => sc.id === id));

export const getProductType = (categoryId: number, subCategoryId: number, id: number) =>
    createSelector(getSubCategory(categoryId, subCategoryId), subCategory =>
        subCategory?.productTypes?.find(t => t.id === id),
    );

export const getSize = (categoryId: number, id: number) =>
    createSelector(getCategory(categoryId), category => {
        return category?.sizeGrids
            ?.map(g => g.sizes)
            .flatMap(s => s)
            .find(s => s?.id === id);
    });
