import { createSelector } from '@ngrx/store';
import { getEntityState } from './entity.selectors';

const getFollowState = createSelector(getEntityState, state => state.follow);

const getFollowingState = createSelector(getFollowState, state => state.following);
const getFollowersState = createSelector(getFollowState, state => state.followers);

export const isFollowingLoading = createSelector(getFollowingState, state => state.loading);
export const getFollowing = createSelector(getFollowingState, state => state.users);
export const getFollowingMaxPage = createSelector(getFollowingState, state => state.maxPage);
export const getFollowingPage = createSelector(getFollowingState, state => state.page);

export const isFollowersLoading = createSelector(getFollowersState, state => state.loading);
export const getFollowers = createSelector(getFollowersState, state => state.users);
export const getFollowersMaxPage = createSelector(getFollowersState, state => state.maxPage);
export const getFollowersPage = createSelector(getFollowersState, state => state.page);
