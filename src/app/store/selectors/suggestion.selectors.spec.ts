import { Product } from 'src/app/shared/models/models';
import { RootState } from '../models';
import { getSuggestion, isLoading, page, totalProducts } from './suggestion.selectors';

const products: Product[] = [
    {
        id: 1,
        label: 'Product 1',
    } as Product,
    {
        id: 2,
        label: 'Product 2',
    } as Product,
    {
        id: 3,
        label: 'Product 3',
    } as Product,
];

describe('suggestion.selectors', () => {
    describe('isLoading', () => {
        it('should be loading', () => {
            const state = {
                entityCache: {
                    suggestion: {
                        loading: true,
                    },
                },
            } as RootState;

            const result = isLoading(state);

            expect(result).toBeTruthy();
        });

        it('should not loading', () => {
            const state = {
                entityCache: {
                    suggestion: {
                        loading: false,
                    },
                },
            } as RootState;

            const result = isLoading(state);

            expect(result).toBeFalsy();
        });
    });

    describe('getSuggestion', () => {
        it('should get products', () => {
            const state = {
                entityCache: {
                    suggestion: {
                        products,
                    },
                },
            } as RootState;

            const result = getSuggestion(state);

            expect(result.length).toBe(3);
        });
    });

    describe('totalProducts', () => {
        it('should get number of total products', () => {
            const state = {
                entityCache: {
                    suggestion: {
                        products,
                        total: 3,
                    },
                },
            } as RootState;

            const result = totalProducts(state);

            expect(result).toBe(3);
        });
    });

    describe('page', () => {
        it('should get current page', () => {
            const state = {
                entityCache: {
                    suggestion: {
                        products,
                        total: 3,
                        page: 1,
                    },
                },
            } as RootState;

            const result = page(state);

            expect(result).toBe(1);
        });
    });
});
