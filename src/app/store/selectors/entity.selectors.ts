import { createFeatureSelector } from '@ngrx/store';
import { EntityState, RootState } from '../models';

export const getEntityState = createFeatureSelector<RootState, EntityState>('entityCache');
