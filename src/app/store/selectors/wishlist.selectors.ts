import { createSelector } from '@ngrx/store';
import { getEntityState } from './entity.selectors';

const getWishlistState = createSelector(getEntityState, state => state.wishlist);

export const isLoading = createSelector(getWishlistState, state => state.loading);

export const getWishlist = createSelector(getWishlistState, state => state.products);
export const totalProducts = createSelector(getWishlistState, state => state.total);
export const page = createSelector(getWishlistState, state => state.page);
