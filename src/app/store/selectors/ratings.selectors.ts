import { createSelector } from '@ngrx/store';
import { getEntityState } from './entity.selectors';

const getRatingsState = createSelector(getEntityState, state => state.ratings);

export const isLoading = createSelector(getRatingsState, state => state.loading);

export const getSummary = createSelector(getRatingsState, state => state.summary);
export const getRatings = createSelector(getRatingsState, state => state.ratings);
export const totalRatings = createSelector(getRatingsState, state => state.total);
export const page = createSelector(getRatingsState, state => state.page);
