import { createSelector } from '@ngrx/store';
import { getEntityState } from './entity.selectors';

export const getConnectionState = createSelector(getEntityState, state => state.connection);
export const getIsOnline = createSelector(getConnectionState, state => state === 'online');
export const getIsOffline = createSelector(getConnectionState, state => state === 'offline');
