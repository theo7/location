import { createSelector } from '@ngrx/store';
import { getEntityState } from './entity.selectors';

export const getNotificationState = createSelector(getEntityState, state => state.notifications);

export const getNotificationHistory = createSelector(
    getNotificationState,
    notificationsState => notificationsState.notifications,
);
export const isNotificationsRead = createSelector(getNotificationState, notificationsState => notificationsState.read);
export const lastNotificationDate = createSelector(
    getNotificationState,
    notificationsState => notificationsState.lastModifiedDate,
);
export const loading = createSelector(getNotificationState, notificationsState => notificationsState.loading);
