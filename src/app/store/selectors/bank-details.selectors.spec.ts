import { AccountStatus, DocumentStatus, IbanStatus } from 'src/app/shared/models/models';
import { RootState } from '../models';
import {
    isBankAccountCompleted,
    isBankAccountIncomplete,
    isBankAccountInError,
    isBankAccountPendingValidation,
} from './bank-details.selectors';

const accountStatuses = Object.values(AccountStatus);

describe('bank-details.selectors', () => {
    describe('isBankAccountInError', () => {
        it('should not be in error if KYC1', () => {
            const state = {
                entityCache: {
                    bankDetails: {
                        account: {
                            status: AccountStatus.REGISTERED_KYC1,
                        },
                    },
                },
            } as RootState;

            const result = isBankAccountInError(state);

            expect(result).toBeFalsy();
        });

        it('should not be in error if KYC2', () => {
            const state = {
                entityCache: {
                    bankDetails: {
                        account: {
                            status: AccountStatus.REGISTERED_KYC2,
                        },
                    },
                },
            } as RootState;

            const result = isBankAccountInError(state);

            expect(result).toBeFalsy();
        });

        const otherStatuses = accountStatuses.filter(
            s => s !== AccountStatus.REGISTERED_KYC1 && s !== AccountStatus.REGISTERED_KYC2,
        );

        otherStatuses.forEach(s => {
            it(`should be in error for status '${s}'`, () => {
                const state = {
                    entityCache: {
                        bankDetails: {
                            account: {
                                status: s,
                            },
                        },
                    },
                } as RootState;

                const result = isBankAccountInError(state);

                expect(result).toBeTruthy();
            });
        });
    });

    describe('isBankAccountCompleted', () => {
        it('should not be completed if only KYC2', () => {
            const state = {
                entityCache: {
                    bankDetails: {
                        account: {
                            status: AccountStatus.REGISTERED_KYC2,
                        },
                    },
                },
            } as RootState;

            const result = isBankAccountCompleted(state);

            expect(result).toBeFalsy();
        });

        it('should be completed if KYC2 and all document status accepted', () => {
            const state = {
                entityCache: {
                    bankDetails: {
                        account: {
                            status: AccountStatus.REGISTERED_KYC2,
                        },
                        ibanDocument: {
                            status: IbanStatus.ACTIVATED,
                        },
                        ribDocument: {
                            status: DocumentStatus.ACCEPTED,
                        },
                        identityDocument: {
                            status: DocumentStatus.ACCEPTED,
                        },
                    },
                },
            } as RootState;

            const result = isBankAccountCompleted(state);

            expect(result).toBeTruthy();
        });
    });

    describe('isBankAccountPendingValidation', () => {
        it('should be pending validation if KYC1 and all document status accepted', () => {
            const state = {
                entityCache: {
                    bankDetails: {
                        account: {
                            status: AccountStatus.REGISTERED_KYC1,
                        },
                        ibanDocument: {
                            status: IbanStatus.ACTIVATED,
                        },
                        ribDocument: {
                            status: DocumentStatus.ACCEPTED,
                        },
                        identityDocument: {
                            status: DocumentStatus.ACCEPTED,
                        },
                    },
                },
            } as RootState;

            const result = isBankAccountPendingValidation(state);

            expect(result).toBeTruthy();
        });

        it('should not be pending validation if KYC2 and all document status accepted', () => {
            const state = {
                entityCache: {
                    bankDetails: {
                        account: {
                            status: AccountStatus.REGISTERED_KYC2,
                        },
                        ibanDocument: {
                            status: IbanStatus.ACTIVATED,
                        },
                        ribDocument: {
                            status: DocumentStatus.ACCEPTED,
                        },
                        identityDocument: {
                            status: DocumentStatus.ACCEPTED,
                        },
                    },
                },
            } as RootState;

            const result = isBankAccountPendingValidation(state);

            expect(result).toBeFalsy();
        });

        it('should be pending validation if KYC1 and all documents status TO_BE_VERIFIED', () => {
            const state = {
                entityCache: {
                    bankDetails: {
                        account: {
                            status: AccountStatus.REGISTERED_KYC1,
                        },
                        ibanDocument: {
                            status: IbanStatus.WAITING_VERIFICATION,
                        },
                        ribDocument: {
                            status: DocumentStatus.TO_BE_VERIFIED,
                        },
                        identityDocument: {
                            status: DocumentStatus.TO_BE_VERIFIED,
                        },
                    },
                },
            } as RootState;

            const result = isBankAccountPendingValidation(state);

            expect(result).toBeTruthy();
        });
    });

    describe('isBankAccountIncomplete', () => {
        it('should not be incomplete if KYC1 and all document status accepted', () => {
            const state = {
                entityCache: {
                    bankDetails: {
                        account: {
                            status: AccountStatus.REGISTERED_KYC1,
                        },
                        ibanDocument: {
                            status: IbanStatus.ACTIVATED,
                        },
                        ribDocument: {
                            status: DocumentStatus.ACCEPTED,
                        },
                        identityDocument: {
                            status: DocumentStatus.ACCEPTED,
                        },
                    },
                },
            } as RootState;

            const result = isBankAccountIncomplete(state);

            expect(result).toBeFalsy();
        });

        it('should be incomplete if KYC2 and at least one document to be verified', () => {
            const state = {
                entityCache: {
                    bankDetails: {
                        account: {
                            status: AccountStatus.REGISTERED_KYC2,
                        },
                        ibanDocument: {
                            status: IbanStatus.ACTIVATED,
                        },
                        ribDocument: {
                            status: DocumentStatus.TO_BE_VERIFIED,
                        },
                        identityDocument: {
                            status: DocumentStatus.ACCEPTED,
                        },
                    },
                },
            } as RootState;

            const result = isBankAccountIncomplete(state);

            expect(result).toBeTruthy();
        });

        it('should be incomplete if KYC2 and at least one document rejected', () => {
            const state = {
                entityCache: {
                    bankDetails: {
                        account: {
                            status: AccountStatus.REGISTERED_KYC2,
                        },
                        ibanDocument: {
                            status: IbanStatus.ACTIVATED,
                        },
                        ribDocument: {
                            status: DocumentStatus.ACCEPTED,
                        },
                        identityDocument: {
                            status: DocumentStatus.REJECTED,
                        },
                    },
                },
            } as RootState;

            const result = isBankAccountIncomplete(state);

            expect(result).toBeTruthy();
        });
    });
});
