import { createSelector } from '@ngrx/store';
import { orderBy } from 'lodash-es';
import { getEntityState } from './entity.selectors';

export const getOrdersState = createSelector(getEntityState, state => state.orders);

const getSalesState = createSelector(getOrdersState, orders => orders.sales);
export const sales = createSelector(getSalesState, s => orderBy(s.values, v => v.lastModifiedDate, 'desc'));
const salesLoading = createSelector(getSalesState, s => s.loading);

const getPurchasesState = createSelector(getOrdersState, orders => orders.purchases);
export const purchases = createSelector(getPurchasesState, p => orderBy(p.values, v => v.lastModifiedDate, 'desc'));
const purchasesLoading = createSelector(getPurchasesState, p => p.loading);

export const loading = createSelector(salesLoading, purchasesLoading, (sl, pl) => sl || pl);
