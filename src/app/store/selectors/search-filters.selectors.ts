import { createSelector } from '@ngrx/store';
import { orderBy, uniqBy } from 'lodash-es';
import { isDefined } from 'src/app/shared/functions/filter.functions';
import {
    FilterType,
    ProductType,
    Seasonality,
    Selectable,
    Size,
    SizeGrid,
    SubCategory,
} from '../../shared/models/models';
import { getAllSizeGrids } from './categories.selectors';
import { filtersSelectors } from './filters.selectors';
import { getColors, getConditions } from './repositories.selectors';

const flagSelected = <T extends { id: number }>(all: T[], selected: T[] | undefined): Selectable<T>[] => {
    return all.map(o => ({
        ...o,
        selected: !!selected && selected.some(s => s?.id === o.id),
    }));
};

const getSelectableConditions = (key: FilterType) =>
    createSelector(filtersSelectors.getConditions(key), getConditions, (filterConditions, repositoryConditions) =>
        flagSelected(repositoryConditions, filterConditions),
    );

const getSelectableColors = (key: FilterType) =>
    createSelector(filtersSelectors.getColors(key), getColors, (filterColors, repositoryColors) =>
        flagSelected(repositoryColors, filterColors),
    );

const getFilteredSizeGrids = (key: FilterType) =>
    createSelector(getAllSizeGrids, filtersSelectors.getFilter(key), (allSizeGrids, filter): SizeGrid[] => {
        if (filter.subCategory) {
            const sizeGridsWithDuplicates =
                filter.subCategory.productTypes
                    ?.flatMap(productType => productType.sizeGrid)
                    .map(sizeGrid => allSizeGrids.find(sg => sg.id === sizeGrid.id))
                    .filter(isDefined) || [];

            return uniqBy(sizeGridsWithDuplicates, 'id');
        }
        if (filter.category) {
            const sizeGridsWithDuplicates =
                filter.category.subCategories
                    ?.flatMap(subCategory => subCategory.productTypes)
                    .filter(isDefined)
                    .flatMap(productType => productType.sizeGrid)
                    .map(sizeGrid => allSizeGrids.find(sg => sg.id === sizeGrid.id))
                    .filter(isDefined) || [];

            return uniqBy(sizeGridsWithDuplicates, 'id');
        }
        return [];
    });

const getSelectableSizeGrids = (key: FilterType) =>
    createSelector(
        getAllSizeGrids,
        getFilteredSizeGrids(key),
        filtersSelectors.getSizes(key),
        (allSizeGrids, filteredSizeGrids, selectedSizes) => {
            const sizeGridsToFlag = filteredSizeGrids.length ? filteredSizeGrids : allSizeGrids;
            return flagSelectedSizes(sizeGridsToFlag, selectedSizes || []);
        },
    );

const flagSelectedSizes = (
    sizeGrids: SizeGrid[],
    sizesInFilter: Size[],
): (SizeGrid & { sizes: Selectable<Size>[] })[] => {
    return sizeGrids.map(sizeGrid => ({
        ...sizeGrid,
        sizes: (sizeGrid.sizes || []).map(size =>
            sizesInFilter.some(sizeInFilter => sizeInFilter.id === size.id)
                ? { ...size, selected: true }
                : { ...size, selected: false },
        ),
    }));
};

const getSelectableSubCategories = (key: FilterType) =>
    createSelector(
        filtersSelectors.getCategory(key),
        filtersSelectors.getSubCategory(key),
        (filterCategory, filterSubCategory): Selectable<SubCategory>[] => {
            if (!filterCategory) {
                return [];
            }
            const subCategories = flagSelected(
                filterCategory.subCategories || [],
                filterSubCategory ? [filterSubCategory] : [],
            );
            return orderBy(subCategories, 'orderNumber');
        },
    );

const getSelectableProductsTypes = (key: FilterType) =>
    createSelector(
        filtersSelectors.getSubCategory(key),
        filtersSelectors.getProductTypes(key),
        (filterSubCategory, filterProductsTypes): Selectable<ProductType>[] => {
            if (!filterSubCategory) {
                return [];
            }
            const productsTypes = flagSelected(filterSubCategory.productTypes || [], filterProductsTypes);
            return orderBy(productsTypes, 'orderNumber');
        },
    );

const getSelectableSeasonalityList = (key: FilterType) =>
    createSelector(filtersSelectors.getSeasonality(key), seasonality =>
        Object.values(Seasonality)
            .filter(v => isNaN(Number(v)))
            .map(s => ({
                value: s,
                selected: seasonality === s,
            })),
    );

export const searchFilterSelectors = {
    getSelectableColors,
    getSelectableConditions,
    getSelectableSubCategories,
    getSelectableProductsTypes,
    getSelectableSizeGrids,
    getSelectableSeasonalityList,
};
