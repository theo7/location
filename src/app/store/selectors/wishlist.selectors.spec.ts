import { Product } from 'src/app/shared/models/models';
import { RootState } from '../models';
import { getWishlist, isLoading, page, totalProducts } from './wishlist.selectors';

const products: Product[] = [
    {
        id: 1,
        label: 'Test product',
    } as Product,
    {
        id: 2,
        label: 'Test product',
    } as Product,
    {
        id: 3,
        label: 'Test product',
    } as Product,
];

describe('wishlist.selectors', () => {
    describe('isLoading', () => {
        it('should be loading', () => {
            const state = {
                entityCache: {
                    wishlist: {
                        loading: true,
                    },
                },
            } as RootState;

            const result = isLoading(state);

            expect(result).toBeTruthy();
        });
    });

    describe('getWishlist', () => {
        it('should get products', () => {
            const state = {
                entityCache: {
                    wishlist: {
                        products,
                    },
                },
            } as RootState;

            const result = getWishlist(state);

            expect(result.length).toBe(3);
        });
    });

    describe('totalProducts', () => {
        it('should get number of total products', () => {
            const state = {
                entityCache: {
                    wishlist: {
                        products,
                        total: 3,
                    },
                },
            } as RootState;

            const result = totalProducts(state);

            expect(result).toBe(3);
        });
    });

    describe('page', () => {
        it('should get current page', () => {
            const state = {
                entityCache: {
                    wishlist: {
                        products,
                        total: 3,
                        page: 0,
                    },
                },
            } as RootState;

            const result = page(state);

            expect(result).toBe(0);
        });
    });
});
