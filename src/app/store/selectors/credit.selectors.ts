import { createSelector } from '@ngrx/store';
import { getEntityState } from './entity.selectors';

export const getCashState = createSelector(getEntityState, state => state.cashState);
export const getMaxPages = createSelector(getCashState, state => state.maxPages);
export const getHistory = createSelector(getCashState, state => state.history);
export const cashLoading = createSelector(getCashState, state => state.loading);
export const getPageLoad = createSelector(getCashState, state => state.pageLoad);
