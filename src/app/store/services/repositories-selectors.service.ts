import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { RootState } from '../models';
import {
    getBrandById,
    getBrandBySlug,
    getBrands,
    getColorById,
    getColors,
    getConditionById,
    getConditions,
    getDefaultBrand,
    getPopularBrands,
    getRepositoriesState,
} from '../selectors/repositories.selectors';

@Injectable()
export class RepositoriesSelectors {
    constructor(private readonly store: Store<RootState>) {}

    repositoriesState$ = this.store.select(getRepositoriesState);
    brands$ = this.store.select(getBrands);
    defaultBrand$ = this.store.select(getDefaultBrand);
    colors$ = this.store.select(getColors);
    conditions$ = this.store.select(getConditions);
    getPopularBrands$ = this.store.select(getPopularBrands);
    brandBySlug$ = (slug: string) => this.store.select(getBrandBySlug(slug));
    brandById$ = (id: number) => this.store.select(getBrandById(id));
    conditionById$ = (id: number) => this.store.select(getConditionById(id));
    colorById$ = (id: number) => this.store.select(getColorById(id));
}
