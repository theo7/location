import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { RootState } from '../models';
import {
    getAccountStatus,
    getIbanDocument,
    getIbanDocumentSimplifiedStatus,
    getIdentityDocument,
    getIdentityDocumentSimplifiedStatus,
    getIsLoading,
    getRibDocument,
    getRibDocumentSimplifiedStatus,
    isBankAccountCompleted,
    isBankAccountIncomplete,
    isBankAccountInError,
    isBankAccountPendingValidation,
    isIbanAndRibProcessStarted,
} from '../selectors/bank-details.selectors';

@Injectable()
export class BankDetailsSelectors {
    constructor(private readonly store: Store<RootState>) {}

    loading$ = this.store.select(getIsLoading);
    accountStatus$ = this.store.select(getAccountStatus);
    ibanDocument$ = this.store.select(getIbanDocument);
    ibanDocumentSimplifiedStatus$ = this.store.select(getIbanDocumentSimplifiedStatus);
    ribDocument$ = this.store.select(getRibDocument);
    ribDocumentSimplifiedStatus$ = this.store.select(getRibDocumentSimplifiedStatus);
    identityDocument$ = this.store.select(getIdentityDocument);
    identityDocumentSimplifiedStatus$ = this.store.select(getIdentityDocumentSimplifiedStatus);
    isIbanAndRibProcessStarted$ = this.store.select(isIbanAndRibProcessStarted);
    isBankAccountIncomplete$ = this.store.select(isBankAccountIncomplete);
    isBankAccountPendingValidation$ = this.store.select(isBankAccountPendingValidation);
    isBankAccountCompleted$ = this.store.select(isBankAccountCompleted);
    isBankAccountInError$ = this.store.select(isBankAccountInError);
}
