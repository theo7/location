import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { RootState } from '../models';
import { getRatings, getSummary, isLoading, page, totalRatings } from '../selectors/ratings.selectors';

@Injectable()
export class RatingsSelectors {
    constructor(private readonly store: Store<RootState>) {}

    isLoading$ = this.store.select(isLoading);
    summary$ = this.store.select(getSummary);
    ratings$ = this.store.select(getRatings);
    totalRatings$ = this.store.select(totalRatings);
    page$ = this.store.select(page);
}
