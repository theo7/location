import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import * as WishlistActions from '../actions/wishlist.actions';
import { RootState } from '../models';

@Injectable()
export class WishlistDispatchers {
    constructor(private readonly store: Store<RootState>) {}

    load(): void {
        this.store.dispatch(WishlistActions.load());
    }
    loadMore() {
        this.store.dispatch(WishlistActions.loadMore());
    }
}
