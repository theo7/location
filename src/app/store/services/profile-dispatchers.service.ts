import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { profileActions } from '../actions/profile.actions';
import { RootState } from '../models';

@Injectable()
export class ProfileDispatchers {
    constructor(private readonly store: Store<RootState>) {}

    load(userId: number): void {
        this.store.dispatch(profileActions.load({ userId }));
    }

    openDeleteAccountModal(): void {
        this.store.dispatch(profileActions.openDeleteAccountModal());
    }
}
