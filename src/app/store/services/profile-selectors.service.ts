import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { RootState } from '../models';
import { profileSelectors } from '../selectors/profile.selectors';

@Injectable()
export class ProfileSelectors {
    constructor(private readonly store: Store<RootState>) {}

    current$ = this.store.select(profileSelectors.current);
    loading$ = this.store.select(profileSelectors.loading);
    isFollowing$ = this.store.select(profileSelectors.isFollowing);
}
