import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Discussion, NewChatMessage, Product, RemunerationMode } from '../../shared/models/models';
import * as DiscussionsAction from '../actions/discussions.actions';
import { RootState } from '../models';

@Injectable()
export class DiscussionsDispatchers {
    constructor(private store: Store<RootState>) {}

    startDiscussion(productId): void {
        this.store.dispatch(DiscussionsAction.startDiscussion({ productId }));
    }

    startListDiscussion(productIds: number[]): void {
        this.store.dispatch(DiscussionsAction.startListDiscussion({ productIds }));
    }

    getDiscussions(): void {
        this.store.dispatch(DiscussionsAction.getUserDiscussions());
    }

    removeProductFromLot(lotId: number, productId: number): void {
        this.store.dispatch(DiscussionsAction.removeProductFromLot({ lotId, productId }));
    }

    addProductToLot(lotId: number, productId: number): void {
        this.store.dispatch(DiscussionsAction.addProductToLot({ lotId, productId }));
    }

    setLotProducts(lotId: number, products: Product[]): void {
        this.store.dispatch(DiscussionsAction.setLotProducts({ lotId, products }));
    }

    confirmSell(lotId: number, remunerationMode: RemunerationMode): void {
        this.store.dispatch(DiscussionsAction.confirmSell({ lotId, remunerationMode }));
    }

    confirmProfessionalSell(lotId: number, trackingNumber: number): void {
        this.store.dispatch(DiscussionsAction.confirmProfessionalSell({ lotId, trackingNumber }));
    }

    declineProfessionalSell(lotId: number): void {
        this.store.dispatch(DiscussionsAction.declineProfessionalSell({ lotId }));
    }

    cancelSell(lotId: number): void {
        this.store.dispatch(DiscussionsAction.cancelSell({ lotId }));
    }

    declineSell(lotId: number): void {
        this.store.dispatch(DiscussionsAction.declineSell({ lotId }));
    }

    confirmPackageCompliant(lotId: number): void {
        this.store.dispatch(DiscussionsAction.confirmPackageCompliant({ lotId }));
    }

    confirmPackageNonCompliant(lotId: number): void {
        this.store.dispatch(DiscussionsAction.confirmPackageNonCompliant({ lotId }));
    }

    sendMessage(message: NewChatMessage) {
        this.store.dispatch(DiscussionsAction.sendMessage({ message }));
    }

    updateDiscussion(discussion: Discussion) {
        this.store.dispatch(DiscussionsAction.updateDiscussion({ discussion }));
    }

    discussionRead(discussion: Discussion) {
        this.store.dispatch(
            DiscussionsAction.readDiscussion({
                roomId: discussion.id!,
                isCurrentBuyer: discussion.currentUserBuyer!,
            }),
        );
    }

    archiveTempDiscussion(discussionId: number): void {
        this.store.dispatch(DiscussionsAction.archiveTemporaryDiscussion({ discussionId }));
    }

    setInstantBuy(lotId: number | null): void {
        this.store.dispatch(DiscussionsAction.setInstantBuy({ lotId }));
    }
}
