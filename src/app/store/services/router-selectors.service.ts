import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { filter, map, pairwise } from 'rxjs/operators';
import { RootState } from '../models';
import {
    brandSlug,
    displayBottomNavigation,
    getBreadcrumbLinks,
    getTabParam,
    getUserId,
    isChatRoomActive,
    isChatRouteActive,
    isCookiesRouteActive,
    isDressingPageVisible,
    isEmptyQueryParams,
    isHomeRouteActive,
    isMarqueRouteActive,
    isOneOfFilterRoutes,
    isProductRouteActive,
    isProfileRouteActive,
    isSearchEnabled,
    isSearchRouteActive,
    isSellRouteActive,
    isUserProfileRouteActive,
    lotId,
    productId,
    redirectUrl,
    responseWkToken,
    roomId,
    selectCurrentRoute,
    selectPageParams,
    selectQueryParams,
    selectUrl,
    token,
} from '../selectors/router.selectors';

@Injectable()
export class RouterSelectors {
    constructor(private readonly store: Store<RootState>) {}

    url$ = this.store.select(selectUrl);
    currentRoute$ = this.store.select(selectCurrentRoute);
    queryParams$ = this.store.select(selectQueryParams);
    isEmptyQueryParams$ = this.store.select(isEmptyQueryParams);
    pageParams$ = this.store.select(selectPageParams);
    isHomeRouteActive$ = this.store.select(isHomeRouteActive);
    isSearchRouteActive$ = this.store.select(isSearchRouteActive);
    isMarqueRouteActive$ = this.store.select(isMarqueRouteActive);
    isSellRouteActive$ = this.store.select(isSellRouteActive);
    isChatRouteActive$ = this.store.select(isChatRouteActive);
    isProfileRouteActive$ = this.store.select(isProfileRouteActive);
    isUserProfileRouteActive$ = this.store.select(isUserProfileRouteActive);
    isDressingPageVisible$ = this.store.select(isDressingPageVisible);
    isChatRoomActive$ = this.store.select(isChatRoomActive);
    isProductRouteActive$ = this.store.select(isProductRouteActive);
    isCookiesRouteActive$ = this.store.select(isCookiesRouteActive);
    isOneOfFilterRoutes$ = this.store.select(isOneOfFilterRoutes);
    roomId$ = this.store.select(roomId);
    lotId$ = this.store.select(lotId);
    productId$ = this.store.select(productId);
    userId$ = this.store.select(getUserId);
    brandSlug$ = this.store.select(brandSlug);
    redirectUrl$ = this.store.select(redirectUrl);
    token$ = this.store.select(token);
    responseWkToken$ = this.store.select(responseWkToken);
    displayBottomNavigation$ = this.store.select(displayBottomNavigation);
    leftHomePage$ = this.isHomeRouteActive$.pipe(
        pairwise(),
        map(([oldIsHomeRoute, newIsHomeRoute]) => oldIsHomeRoute && !newIsHomeRoute),
        filter(leftHomePage => leftHomePage),
    );
    leftSearchPage$ = this.isSearchRouteActive$.pipe(
        pairwise(),
        map(([oldIsSearchRoute, newIsSearchRoute]) => oldIsSearchRoute && !newIsSearchRoute),
        filter(leftSearchPage => leftSearchPage),
    );
    isSearchEnabled$ = this.store.select(isSearchEnabled);
    breadcrumbLinks$ = this.store.select(getBreadcrumbLinks);
    getTabParam$ = this.store.select(getTabParam);
}
