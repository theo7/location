import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { RootState } from '../models';
import { getWishlist, isLoading, page, totalProducts } from '../selectors/wishlist.selectors';

@Injectable()
export class WishlistSelectors {
    constructor(private readonly store: Store<RootState>) {}

    isLoading$ = this.store.select(isLoading);
    wishlist$ = this.store.select(getWishlist);
    totalProducts$ = this.store.select(totalProducts);
    page$ = this.store.select(page);
}
