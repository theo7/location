import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { RootState } from '../models';
import { cashLoading, getHistory, getMaxPages, getPageLoad } from '../selectors/credit.selectors';

@Injectable()
export class CreditSelectors {
    constructor(private readonly store: Store<RootState>) {}

    maxPages$ = this.store.select(getMaxPages);
    getHistory$ = this.store.select(getHistory);
    loading$ = this.store.select(cashLoading);
    pageLoad$ = this.store.select(getPageLoad);
}
