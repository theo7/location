import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Discussion, Product, RemunerationMode } from '../../shared/models/models';
import * as LotActions from '../actions/lot.actions';
import { RootState } from '../models';

@Injectable()
export class LotDispatchers {
    constructor(private readonly store: Store<RootState>) {}

    openMoreActionsModal(discussion: Discussion, userId: number): void {
        this.store.dispatch(LotActions.openMoreActionsModal({ discussion, userId }));
    }

    confirmCancelSell(lotId: number): void {
        this.store.dispatch(LotActions.confirmCancelSell({ lotId }));
    }

    openConfirmCancelDialog(lotId: number) {
        this.store.dispatch(LotActions.openConfirmCancelSellModal({ lotId }));
    }

    unblockUser(userId: number) {
        this.store.dispatch(LotActions.openConfirmUnblockUserModal({ userId }));
    }

    startDiscussion(productId: number): void {
        this.store.dispatch(LotActions.startDiscussion({ productId }));
    }

    startPaymentProcess(productId: number): void {
        this.store.dispatch(LotActions.startPaymentProcess({ productId }));
    }

    createLot(product: Product): void {
        this.store.dispatch(LotActions.createLot({ product }));
    }

    changeRemunerationMode(lotId: number, remunerationMode: RemunerationMode): void {
        this.store.dispatch(LotActions.changeRemunerationMode({ lotId, remunerationMode }));
    }
}
