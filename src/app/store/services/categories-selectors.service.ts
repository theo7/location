import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { RootState } from '../models';
import {
    getAllCategories,
    getAllSizeGrids,
    getAllSizes,
    getCategoriesState,
    getCategory,
    getIndexedCategories,
    getProductType,
    getProductTypes,
    getSize,
    getSubCategories,
    getSubCategory,
} from '../selectors/categories.selectors';

@Injectable()
export class CategoriesSelectors {
    constructor(private readonly store: Store<RootState>) {}

    categoriesState$ = this.store.select(getCategoriesState);
    allCategories$ = this.store.select(getAllCategories);
    allSizeGrids$ = this.store.select(getAllSizeGrids);
    allSizes$ = this.store.select(getAllSizes);
    indexedCategories$ = this.store.select(getIndexedCategories);
    category$ = (id: number) => this.store.select(getCategory(id));
    subCategories$ = (categoryId: number) => this.store.select(getSubCategories(categoryId));
    subCategory$ = (categoryId: number, id: number) => this.store.select(getSubCategory(categoryId, id));
    productType$ = (categoryId: number, subCategoryId: number, id: number) =>
        this.store.select(getProductType(categoryId, subCategoryId, id));
    productTypes$ = (categoryId: number, subCategoryId: number) =>
        this.store.select(getProductTypes(categoryId, subCategoryId));
    size$ = (categoryId: number, id: number) => this.store.select(getSize(categoryId, id));
}
