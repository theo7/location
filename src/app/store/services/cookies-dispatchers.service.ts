import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { CookieConsentSelection } from 'src/app/features/cookie-consent/services/cookie-consent.service';
import * as CookiesActions from '../actions/cookies.actions';
import { RootState } from '../models';

@Injectable()
export class CookiesDispatchers {
    constructor(private readonly store: Store<RootState>) {}

    openConfigureModal(): void {
        this.store.dispatch(CookiesActions.openConfigureModal());
    }

    acceptAll(): void {
        this.store.dispatch(CookiesActions.acceptAll());
    }

    accept(selection: CookieConsentSelection): void {
        this.store.dispatch(CookiesActions.accept({ selection }));
    }

    denyAll(): void {
        this.store.dispatch(CookiesActions.denyAll());
    }
}
