import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import * as OrdersActions from '../actions/orders.actions';
import { RootState } from '../models';

@Injectable()
export class OrdersDispatchers {
    constructor(private readonly store: Store<RootState>) {}

    loadSales(): void {
        this.store.dispatch(OrdersActions.loadSales());
    }

    loadPurchases(): void {
        this.store.dispatch(OrdersActions.loadPurchases());
    }
}
