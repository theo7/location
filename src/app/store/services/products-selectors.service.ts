import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Product } from '../../shared/models/models';
import { RootState } from '../models';
import {
    isCurrentUserWithoutProduct,
    productSelectors,
    productsInDressingToDisplay,
} from '../selectors/products.selectors';

@Injectable()
export class ProductsSelectors {
    constructor(private readonly store: Store<RootState>) {}

    productsInDressing$ = this.store.select(productSelectors.dressingProducts);
    totalProducts$ = this.store.select(productSelectors.dressingTotalProducts);
    dressingCount$ = this.store.select(productSelectors.dressingCountProducts);
    hasProductInDressing$ = this.store.select(productSelectors.hasProductInDressing);
    hasOneProductInDressing$ = this.store.select(productSelectors.hasOneProductInDressing);
    hasMoreThanOneProductInDressing$ = this.store.select(productSelectors.hasMoreThanOneProductInDressing);
    showDressingFilter$ = this.store.select(productSelectors.showDressingFilter);
    loading$ = this.store.select(productSelectors.loading);
    dressingLoading$ = this.store.select(productSelectors.dressingLoading);
    page$ = this.store.select(productSelectors.dressingPage);
    viewsLoading$ = this.store.select(productSelectors.viewsLoading);
    views$ = this.store.select(productSelectors.viewsValue);
    product$ = this.store.select(productSelectors.product);
    noProductInDressingFound$ = this.store.select(productSelectors.noProductInDressingFound);
    isCurrentUserWithoutProduct$ = (userId: number) => this.store.select(isCurrentUserWithoutProduct(userId));
    productsInDressingToDisplay$ = (userId: number, hiddenProducts: Product[] = []) =>
        this.store.select(productsInDressingToDisplay(userId, hiddenProducts));
}
