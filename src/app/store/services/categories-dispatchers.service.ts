import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import * as CategoriesAction from '../actions/categories.actions';
import { RootState } from '../models';

@Injectable()
export class CategoriesDispatchers {
    constructor(private store: Store<RootState>) {}

    getCategories(): void {
        this.store.dispatch(CategoriesAction.getCategories());
    }
}
