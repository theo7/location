import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import * as FollowActions from '../actions/follow.actions';
import { RootState } from '../models';

@Injectable()
export class FollowDispatchers {
    constructor(private readonly store: Store<RootState>) {}

    follow(userId: number): void {
        this.store.dispatch(FollowActions.userFollow({ userId }));
    }
    unfollow(userId: number) {
        this.store.dispatch(FollowActions.userUnfollow({ userId }));
    }

    getUserFollowing(userId: number, page: number) {
        this.store.dispatch(FollowActions.getUserFollowing({ userId, page }));
    }

    getUserFollower(userId: number, page: number) {
        this.store.dispatch(FollowActions.getUserFollower({ userId, page }));
    }

    resetFollowing() {
        this.store.dispatch(FollowActions.resetFollowing());
    }

    resetFollowers() {
        this.store.dispatch(FollowActions.resetFollowers());
    }
}
