import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { RootState } from '../models';
import { getSettings, getShippingFees, isLoading } from '../selectors/settings.selectors';

@Injectable()
export class SettingsSelectors {
    constructor(private readonly store: Store<RootState>) {}

    isLoading$ = this.store.select(isLoading);
    settings$ = this.store.select(getSettings);
    shippingFees$ = this.store.select(getShippingFees);
}
