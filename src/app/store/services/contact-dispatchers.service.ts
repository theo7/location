import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { ContactSupportInfo } from 'src/app/shared/models/models';
import * as ContactActions from '../actions/contact.actions';
import { RootState } from '../models';

@Injectable({
    providedIn: 'root',
})
export class ContactDispatchers {
    constructor(private store: Store<RootState>) {}

    sendEmailContactSupport(contactSupportInfo: ContactSupportInfo): void {
        this.store.dispatch(ContactActions.sendEmailContactSupport({ contactSupportInfo }));
    }
}
