import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import * as SearchAction from '../actions/search.actions';
import { RootState } from '../models';

@Injectable()
export class SearchDispatchers {
    constructor(private readonly store: Store<RootState>) {}

    executeSearch(): void {
        this.store.dispatch(SearchAction.executeSearch());
    }

    executeSearchNext(): void {
        this.store.dispatch(SearchAction.executeSearchNext());
    }

    refreshSearch(): void {
        this.store.dispatch(SearchAction.startSearchFromUrl());
    }

    setShowFilter(showFilter: boolean): void {
        this.store.dispatch(SearchAction.setShowFilter({ showFilter }));
    }

    setActivateRefresher(activateRefresher: boolean): void {
        this.store.dispatch(SearchAction.setActivateRefresher({ activateRefresher }));
    }
}
