import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Address, CguType, UserProfile } from '../../shared/models/models';
import * as UserAction from '../actions/user.actions';
import { RootState } from '../models';

@Injectable()
export class UserProfileDispatchers {
    constructor(private readonly store: Store<RootState>) {}

    openLoginPage() {
        this.store.dispatch(UserAction.openLoginPage());
    }

    login(email: string, password: string): void {
        this.store.dispatch(UserAction.login({ email, password }));
    }

    logout(): void {
        this.store.dispatch(UserAction.logout());
    }

    updateUserProfile(userProfile: UserProfile): void {
        this.store.dispatch(UserAction.updateUserProfile({ userProfile }));
    }

    postUserAddress(address: Address): void {
        this.store.dispatch(UserAction.postUserAddress({ address }));
    }

    deleteUserAddress(address: Address): void {
        this.store.dispatch(UserAction.deleteUserAddress({ address }));
    }

    updateUserAddress(address: Address): void {
        this.store.dispatch(UserAction.updateUserAddress({ address }));
    }

    getUserProfile(): void {
        this.store.dispatch(UserAction.getUserProfile());
    }

    signUp(signUpData: any): void {
        this.store.dispatch(UserAction.signUp({ signUpData }));
    }

    resetPassword(email: string): void {
        this.store.dispatch(UserAction.resetPassword({ email }));
    }

    changePassword(currentPassword: string, newPassword: string): void {
        this.store.dispatch(UserAction.changePassword({ currentPassword, newPassword }));
    }

    updateCguModificationType(cguType: CguType): void {
        this.store.dispatch(UserAction.updateCgu({ cguType }));
    }

    switchVacationMode(): void {
        this.store.dispatch(UserAction.switchVacationMode());
    }
}
