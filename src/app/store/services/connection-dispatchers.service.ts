import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import * as ConnectionAction from '../actions/connection.actions';
import { RootState } from '../models';
import { ConnectionState } from '../reducers/connection.reducer';

@Injectable()
export class ConnectionDispatchers {
    constructor(private store: Store<RootState>) {}

    setConnection(connectionState: ConnectionState): void {
        this.store.dispatch(ConnectionAction.setConnection({ connectionState }));
    }
}
