import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { ProductId } from 'src/app/shared/models/models';
import { RootState } from '../models';
import {
    canCreateLot,
    getAddresses,
    getAvatarURL,
    getCurrentUserId,
    getDefaultAddress,
    getError,
    getHasAddresses,
    getIsCurrentUser,
    getMail,
    getUserDressingIsModerated,
    getUserIsModerated,
    getUserProfile,
    getUserProfileIsModerated,
    getUserProfileState,
    isAdmin,
    isBlocked,
    isBlockedByAdmin,
    isError,
    isIndividual,
    isInVacationMode,
    isLoading,
    isLogged,
    isNotBlockedByAdmin,
    isNotInVacationMode,
    isNotLogged,
    isProductFavorite,
    isProfessional,
} from '../selectors/user.selectors';

@Injectable()
export class UserProfileSelectors {
    constructor(private readonly store: Store<RootState>) {}

    userProfileState$ = this.store.select(getUserProfileState);
    userProfile$ = this.store.select(getUserProfile);
    isLogged$ = this.store.select(isLogged);
    isNotLogged$ = this.store.select(isNotLogged);
    isLoading$ = this.store.select(isLoading);
    isError$ = this.store.select(isError);
    error$ = this.store.select(getError);
    avatarURL$ = this.store.select(getAvatarURL);
    isProfessional$ = this.store.select(isProfessional);
    isAdmin$ = this.store.select(isAdmin);
    isIndividual$ = this.store.select(isIndividual);
    isModerated$ = this.store.select(getUserIsModerated);
    isProfileModerated$ = this.store.select(getUserProfileIsModerated);
    isDressingModerated$ = this.store.select(getUserDressingIsModerated);
    mail$ = this.store.select(getMail);
    addresses$ = this.store.select(getAddresses);
    hasAddresses$ = this.store.select(getHasAddresses);
    defaultAddress$ = this.store.select(getDefaultAddress);
    isBlockedByAdmin$ = this.store.select(isBlockedByAdmin);
    isNotBlockedByAdmin$ = this.store.select(isNotBlockedByAdmin);
    isInVacationMode$ = this.store.select(isInVacationMode);
    isNotInVacationMode$ = this.store.select(isNotInVacationMode);
    currentUserId$ = this.store.select(getCurrentUserId);
    isCurrentUser$ = (userId: number) => this.store.select(getIsCurrentUser(userId));
    isProductFavorite$ = (id: ProductId) => this.store.select(isProductFavorite(id));
    isBlocked$ = (id: number) => this.store.select(isBlocked(id));
    canCreateLot$ = (userId: number) => this.store.select(canCreateLot(userId));
}
