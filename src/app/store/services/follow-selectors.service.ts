import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { RootState } from '../models';
import {
    getFollowers,
    getFollowersMaxPage,
    getFollowersPage,
    getFollowing,
    getFollowingMaxPage,
    getFollowingPage,
    isFollowersLoading,
    isFollowingLoading,
} from '../selectors/follow.selectors';

@Injectable()
export class FollowSelectors {
    constructor(private readonly store: Store<RootState>) {}

    isFollowingLoading$ = this.store.select(isFollowingLoading);
    following$ = this.store.select(getFollowing);
    followingMaxPage$ = this.store.select(getFollowingMaxPage);
    followingPage$ = this.store.select(getFollowingPage);
    isFollowersLoading$ = this.store.select(isFollowersLoading);
    followers$ = this.store.select(getFollowers);
    followersMaxPage$ = this.store.select(getFollowersMaxPage);
    followersPage$ = this.store.select(getFollowersPage);
}
