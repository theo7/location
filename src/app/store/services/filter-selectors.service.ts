import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { CreateSearch, FilterType } from '../../shared/models/models';
import { RootState } from '../models';
import { filtersSelectors } from '../selectors/filters.selectors';

@Injectable({
    providedIn: 'root',
})
export class FilterSelectors {
    constructor(private readonly store: Store<RootState>) {}

    // getter
    current$ = (key: FilterType) => this.store.select(filtersSelectors.getFilter(key));
    keyword$ = (key: FilterType) => this.store.select(filtersSelectors.getKeyword(key));
    brands$ = (key: FilterType) => this.store.select(filtersSelectors.getBrands(key));
    category$ = (key: FilterType) => this.store.select(filtersSelectors.getCategory(key));
    subCategory$ = (key: FilterType) => this.store.select(filtersSelectors.getSubCategory(key));
    productTypes$ = (key: FilterType) => this.store.select(filtersSelectors.getProductTypes(key));
    sizes$ = (key: FilterType) => this.store.select(filtersSelectors.getSizes(key));
    colors$ = (key: FilterType) => this.store.select(filtersSelectors.getColors(key));
    conditions$ = (key: FilterType) => this.store.select(filtersSelectors.getConditions(key));
    seasonality$ = (key: FilterType) => this.store.select(filtersSelectors.getSeasonality(key));
    minPrice$ = (key: FilterType) => this.store.select(filtersSelectors.getMinPrice(key));
    maxPrice$ = (key: FilterType) => this.store.select(filtersSelectors.getMaxPrice(key));
    sort$ = (key: FilterType) => this.store.select(filtersSelectors.getSort(key));
    recentSearch$ = this.store.select(filtersSelectors.getRecentSearch);
    savedSearch$ = this.store.select(filtersSelectors.getSavedSearch);
    searchLoading$ = this.store.select(filtersSelectors.isSearchLoading);

    // utility
    count$ = (key: FilterType) => this.store.select(filtersSelectors.count(key));
    isEmpty$ = (key: FilterType) => this.store.select(filtersSelectors.isEmpty(key));
    hasFilter$ = (key: FilterType) => this.store.select(filtersSelectors.hasFilter(key));
    hasKeyword$ = (key: FilterType) => this.store.select(filtersSelectors.hasKeyword(key));
    hasCategory$ = (key: FilterType) => this.store.select(filtersSelectors.hasCategory(key));
    hasSubCategory$ = (key: FilterType) => this.store.select(filtersSelectors.hasSubCategory(key));
    hasProductTypes$ = (key: FilterType) => this.store.select(filtersSelectors.hasProductTypes(key));
    hasSort$ = (key: FilterType) => this.store.select(filtersSelectors.hasSort(key));
    currentRouterParams$ = (key: FilterType) => this.store.select(filtersSelectors.asRouterParams(key));
    hasCategoryAttribute$ = (key: FilterType) => this.store.select(filtersSelectors.hasCategoryAttribute(key));
    isCurrentCategorySelected$ = (key: FilterType, id: number) =>
        this.store.select(filtersSelectors.isCurrentCategorySelected(key, id));
    isCurrentSubCategorySelected$ = (key: FilterType, id: number) =>
        this.store.select(filtersSelectors.isCurrentSubCategorySelected(key, id));
    isSearchAlreadyInHistory$ = (search: CreateSearch) =>
        this.store.select(filtersSelectors.isSearchAlreadyInHistory(search));
    getSearchInHistory$ = (search: CreateSearch) => this.store.select(filtersSelectors.getSearchInHistory(search));
}
