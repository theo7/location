import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { UserRating } from 'src/app/shared/models/models';
import * as RatingsActions from '../actions/ratings.actions';
import { RootState } from '../models';

@Injectable()
export class RatingsDispatchers {
    constructor(private readonly store: Store<RootState>) {}

    load(): void {
        this.store.dispatch(RatingsActions.load());
    }
    loadMore() {
        this.store.dispatch(RatingsActions.loadMore());
    }

    update(userRating: UserRating) {
        this.store.dispatch(RatingsActions.update({ userRating }));
    }

    answerRating(userId: number, rateId: number, answer: string) {
        this.store.dispatch(RatingsActions.answerRating({ userId, rateId, answer }));
    }
}
