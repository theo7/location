import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { RootState } from '../models';
import { getIsOffline, getIsOnline } from '../selectors/connection.selectors';

@Injectable()
export class ConnectionSelectors {
    constructor(private readonly store: Store<RootState>) {}

    isOnline$ = this.store.select(getIsOnline);
    isOffline$ = this.store.select(getIsOffline);
}
