import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { RootState } from '../models';
import {
    canAddToLot,
    canCreateLot,
    canRemoveFromLot,
    getDiscussion,
    getDiscussionByLotId,
    getDiscussionByProductId,
    getDiscussionBySellerId,
    getDiscussions,
    getLotOpenByProductId,
    getLotOpenBySellerId,
    getProductIsInLot,
    hasCartForSellerId,
    isCancelSellEnabled,
    isLoading,
    isProductInLot,
    isSaleInProgress,
    isSaleInProgressWith,
} from '../selectors/discussions.selectors';

@Injectable()
export class DiscussionsSelectors {
    constructor(private readonly store: Store<RootState>) {}

    discussions$ = this.store.select(getDiscussions);
    isLoading$ = this.store.select(isLoading);
    discussion$ = (id: number | string) => this.store.select(getDiscussion(id));
    discussionByProductId$ = (productId: number) => this.store.select(getDiscussionByProductId(productId));
    discussionBySellerId$ = (sellerId: number) => this.store.select(getDiscussionBySellerId(sellerId));
    discussionByLotId$ = (lotId: number) => this.store.select(getDiscussionByLotId(lotId));
    productIsInLot$ = (productId: number) => this.store.select(getProductIsInLot(productId));
    isCancelSellEnabled$ = (lotId: number) => this.store.select(isCancelSellEnabled(lotId));
    isSaleInProgress$ = (lotId: number) => this.store.select(isSaleInProgress(lotId));
    isSaleInProgressWith$ = (userId: number) => this.store.select(isSaleInProgressWith(userId));
    lotOpenBySellerId$ = (sellerId: number) => this.store.select(getLotOpenBySellerId(sellerId));
    hasCartForSellerId$ = (sellerId: number) => this.store.select(hasCartForSellerId(sellerId));
    isProductInLot$ = (productId: number) => this.store.select(isProductInLot(productId));
    canCreateLot$ = (sellerId: number) => this.store.select(canCreateLot(sellerId));
    canAddToLot$ = (sellerId: number, productId: number) => this.store.select(canAddToLot(sellerId, productId));
    canRemoveFromLot$ = (sellerId: number, productId: number) =>
        this.store.select(canRemoveFromLot(sellerId, productId));
    lotOpenByProductId$ = (productId: number) => this.store.select(getLotOpenByProductId(productId));
}
