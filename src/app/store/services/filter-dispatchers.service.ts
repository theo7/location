import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import {
    Brand,
    Category,
    Color,
    Condition,
    FilterType,
    ProductFilterSort,
    ProductType,
    Search,
    Seasonality,
    Size,
    SubCategory,
} from '../../shared/models/models';
import { filtersActions } from '../actions/filters.actions';

@Injectable()
export class FilterDispatchers {
    constructor(private readonly store: Store) {}

    search(key: FilterType): void {
        this.store.dispatch(filtersActions.search({ key }));
    }

    openDialog(key: FilterType): void {
        this.store.dispatch(filtersActions.openDialog({ key }));
    }

    reset(key: FilterType): void {
        this.store.dispatch(filtersActions.reset({ key }));
    }

    setKeyword(key: FilterType, keyword: string) {
        this.store.dispatch(filtersActions.setKeyword({ keyword, key }));
    }

    setMinPrice(key: FilterType, price: number): void {
        this.store.dispatch(filtersActions.setMinPrice({ price, key }));
    }

    resetMinPrice(key: FilterType): void {
        this.store.dispatch(filtersActions.resetMinPrice({ key }));
    }

    setMaxPrice(key: FilterType, price: number): void {
        this.store.dispatch(filtersActions.setMaxPrice({ price, key }));
    }

    resetMaxPrice(key: FilterType): void {
        this.store.dispatch(filtersActions.resetMaxPrice({ key }));
    }

    setSort(key: FilterType, sort: ProductFilterSort): void {
        this.store.dispatch(filtersActions.setSort({ key, sort }));
    }

    resetSort(key: FilterType): void {
        this.store.dispatch(filtersActions.resetSort({ key }));
    }

    setSeasonality(key: FilterType, seasonality: Seasonality): void {
        this.store.dispatch(filtersActions.setSeasonality({ key, seasonality }));
    }

    resetSeasonality(key: FilterType): void {
        this.store.dispatch(filtersActions.resetSeasonality({ key }));
    }

    setCategory(key: FilterType, category: Category): void {
        this.store.dispatch(filtersActions.setCategory({ key, category }));
    }

    resetCategory(key: FilterType): void {
        this.store.dispatch(filtersActions.resetCategory({ key }));
    }

    setSubCategory(key: FilterType, subCategory: SubCategory, category: Category): void {
        this.store.dispatch(filtersActions.setSubCategory({ key, subCategory, category }));
    }

    resetSubCategory(key: FilterType): void {
        this.store.dispatch(filtersActions.resetSubCategory({ key }));
    }

    addProductType(key: FilterType, productType: ProductType, subCategory: SubCategory, category: Category): void {
        this.store.dispatch(filtersActions.addProductType({ key, productType, subCategory, category }));
    }

    removeProductType(key: FilterType, productType: ProductType): void {
        this.store.dispatch(filtersActions.removeProductType({ key, productType }));
    }

    addBrand(key: FilterType, brand: Brand): void {
        this.store.dispatch(filtersActions.addBrand({ key, brand }));
    }

    removeBrand(key: FilterType, brand: Brand): void {
        this.store.dispatch(filtersActions.removeBrand({ key, brand }));
    }

    addColor(key: FilterType, color: Color): void {
        this.store.dispatch(filtersActions.addColor({ key, color }));
    }

    removeColor(key: FilterType, color: Color): void {
        this.store.dispatch(filtersActions.removeColor({ key, color }));
    }

    addCondition(key: FilterType, condition: Condition): void {
        this.store.dispatch(filtersActions.addCondition({ key, condition }));
    }

    removeCondition(key: FilterType, condition: Condition): void {
        this.store.dispatch(filtersActions.removeCondition({ key, condition }));
    }

    addSize(key: FilterType, size: Size): void {
        this.store.dispatch(filtersActions.addSize({ key, size }));
    }

    removeSize(key: FilterType, size: Size): void {
        this.store.dispatch(filtersActions.removeSize({ key, size }));
    }

    saveCurrentSearch(): void {
        this.store.dispatch(filtersActions.saveCurrentSearch());
    }

    saveSearch(id: number): void {
        this.store.dispatch(filtersActions.saveSearch({ id }));
    }

    unSaveSearch(id: number): void {
        this.store.dispatch(filtersActions.unSaveSearch({ id }));
    }

    applySearchFilter(search: Search): void {
        this.store.dispatch(filtersActions.applySearchFilter({ search }));
    }

    openSearchHistoryDialog(): void {
        this.store.dispatch(filtersActions.openSearchHistoryDialog());
    }
}
