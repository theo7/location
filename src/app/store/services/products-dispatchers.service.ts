import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Product, ProductFilter, ProductId, UserProfile } from 'src/app/shared/models/models';
import * as ProductsAction from '../actions/products.actions';
import { RootState } from '../models';

@Injectable()
export class ProductsDispatchers {
    constructor(private readonly store: Store<RootState>) {}

    report(productId: number, reason: string, content?: string): void {
        this.store.dispatch(ProductsAction.reportProduct({ productId, reason, content }));
    }

    getDressing(userId: number): void {
        this.store.dispatch(ProductsAction.getDressing({ userId }));
    }

    getOwnDressing(): void {
        this.store.dispatch(ProductsAction.getOwnDressing());
    }

    getNextDressing(userId: number): void {
        this.store.dispatch(ProductsAction.getNextDressing({ userId }));
    }

    getOwnNextDressing(): void {
        this.store.dispatch(ProductsAction.getOwnNextDressing());
    }

    like(productId: ProductId): void {
        this.store.dispatch(ProductsAction.like({ productId }));
    }

    deleteLike(productId: ProductId): void {
        this.store.dispatch(ProductsAction.deleteLike({ productId }));
    }

    shareDressing(user: UserProfile): void {
        this.store.dispatch(ProductsAction.shareDressing({ user }));
    }

    shareProduct(product: Product): void {
        this.store.dispatch(ProductsAction.shareProduct({ product }));
    }

    getProductViews(productId: ProductId): void {
        this.store.dispatch(ProductsAction.getProductViews({ productId }));
    }

    deleteProduct(productId: number): void {
        this.store.dispatch(ProductsAction.deleteProduct({ productId }));
    }

    loadProduct(productId: number): void {
        this.store.dispatch(ProductsAction.loadProduct({ productId }));
    }

    getPriceAverage(filter: ProductFilter) {
        this.store.dispatch(ProductsAction.getPriceAverage({ filter }));
    }
}
