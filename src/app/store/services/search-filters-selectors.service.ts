import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { FilterType } from '../../shared/models/models';
import { RootState } from '../models';
import { searchFilterSelectors } from '../selectors/search-filters.selectors';

@Injectable({
    providedIn: 'root',
})
export class SearchFiltersSelectors {
    constructor(private readonly store: Store<RootState>) {}

    selectableConditions$ = (key: FilterType) => this.store.select(searchFilterSelectors.getSelectableConditions(key));
    selectableColors$ = (key: FilterType) => this.store.select(searchFilterSelectors.getSelectableColors(key));
    selectableSizeGrids$ = (key: FilterType) => this.store.select(searchFilterSelectors.getSelectableSizeGrids(key));
    selectableSubCategories$ = (key: FilterType) =>
        this.store.select(searchFilterSelectors.getSelectableSubCategories(key));
    selectableProductsTypes$ = (key: FilterType) =>
        this.store.select(searchFilterSelectors.getSelectableProductsTypes(key));
    selectableSeasonalityList$ = (key: FilterType) =>
        this.store.select(searchFilterSelectors.getSelectableSeasonalityList(key));
}
