import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { ErrorLog } from 'src/app/features/logger/logger.models';
import * as LoggingActions from '../actions/logging.actions';
import { RootState } from '../models';

@Injectable()
export class LoggingDispatchers {
    constructor(private readonly store: Store<RootState>) {}

    logError(errorLog: ErrorLog): void {
        this.store.dispatch(LoggingActions.logError({ errorLog }));
    }
}
