import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import * as SettingsActions from '../actions/settings.actions';
import { GlobalSettings } from '../actions/settings.actions';
import { RootState } from '../models';

@Injectable()
export class SettingsDispatchers {
    constructor(private readonly store: Store<RootState>) {}

    getSettings(): void {
        this.store.dispatch(SettingsActions.getSettings());
    }

    setGlobalSettings(settings: GlobalSettings): void {
        this.store.dispatch(SettingsActions.setGlobalSettings({ settings }));
    }

    setNotificationSetting(value: boolean): void {
        this.store.dispatch(SettingsActions.setNotificationSetting({ value }));
    }

    getShippingFees() {
        this.store.dispatch(SettingsActions.getShippingsFeesSettings());
    }
}
