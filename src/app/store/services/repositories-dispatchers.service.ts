import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import * as RepositoriesAction from '../actions/repositories.actions';
import { RootState } from '../models';

@Injectable()
export class RepositoriesDispatchers {
    constructor(private store: Store<RootState>) {}

    getRepositories(): void {
        this.store.dispatch(RepositoriesAction.getRepositories());
    }
}
