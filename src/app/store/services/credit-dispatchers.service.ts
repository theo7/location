import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import * as CreditAction from '../actions/credit.actions';
import { RootState } from '../models';

@Injectable({
    providedIn: 'root',
})
export class CreditDispatchersService {
    constructor(private readonly store: Store<RootState>) {}

    getCashHistory(page: number): void {
        this.store.dispatch(CreditAction.getCashHistory({ page }));
    }

    resetCashHistory(): void {
        this.store.dispatch(CreditAction.resetCashHistory());
    }
}
