import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { RootState } from '../models';
import {
    getFilterResults,
    getLoading,
    getPreSearchTotalElements,
    getSearchState,
    getTotalElements,
    isFirstPage,
    isLastPage,
    isRefresherActive,
    isShowingFilter,
    noProductFound,
} from '../selectors/search.selectors';

@Injectable()
export class SearchSelectors {
    constructor(private readonly store: Store<RootState>) {}

    getSearchState$ = this.store.select(getSearchState);
    loading$ = this.store.select(getLoading);
    getFilterResults$ = this.store.select(getFilterResults);
    showFilter$ = this.store.select(isShowingFilter);
    activeRefresher$ = this.store.select(isRefresherActive);

    getTotalElements$ = this.store.select(getTotalElements);
    isFirstPage$ = this.store.select(isFirstPage);
    isLastPage$ = this.store.select(isLastPage);
    getPreSearchTotalElements$ = this.store.select(getPreSearchTotalElements);

    noProductFound$ = this.store.select(noProductFound);
}
