import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import * as LegalContentActions from '../actions/legal-content.actions';
import { RootState } from '../models';

@Injectable()
export class LegalContentDispatchers {
    constructor(private readonly store: Store<RootState>) {}

    openDialog(legalContentProp) {
        this.store.dispatch(LegalContentActions.openDialog({ legalContentProp }));
    }
}
