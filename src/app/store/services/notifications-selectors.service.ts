import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { RootState } from '../models';
import {
    getNotificationHistory,
    isNotificationsRead,
    lastNotificationDate,
    loading,
} from '../selectors/notifications.selectors';

@Injectable()
export class NotificationsSelectors {
    constructor(private readonly store: Store<RootState>) {}

    getNotificationHistory$ = this.store.select(getNotificationHistory);
    isNotificationsRead$ = this.store.select(isNotificationsRead);
    lastNotificationDate$ = this.store.select(lastNotificationDate);
    loading$ = this.store.select(loading);
}
