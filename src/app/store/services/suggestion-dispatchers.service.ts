import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import * as SuggestionActions from '../actions/suggestion.actions';
import { RootState } from '../models';

@Injectable()
export class SuggestionDispatchers {
    constructor(private readonly store: Store<RootState>) {}

    loadSuggestion(): void {
        this.store.dispatch(SuggestionActions.loadSuggestion());
    }
    loadMoreSuggestion() {
        this.store.dispatch(SuggestionActions.loadMoreSuggestion());
    }
}
