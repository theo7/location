import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import * as NotificationsActions from '../actions/notifications.actions';
import { RootState } from '../models';

@Injectable()
export class NotificationsDispatchers {
    constructor(private readonly store: Store<RootState>) {}

    requestPermission(): void {
        this.store.dispatch(NotificationsActions.requestPermission());
    }

    revokePermission(): void {
        this.store.dispatch(NotificationsActions.revokePermission());
    }

    loadNotificationHistory() {
        this.store.dispatch(NotificationsActions.loadNotificationHistory());
    }

    setNotificationRead() {
        this.store.dispatch(NotificationsActions.setNotificationRead());
    }
}
