import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { RootState } from '../models';
import { getSuggestion, isLoading, page, totalProducts } from '../selectors/suggestion.selectors';

@Injectable()
export class SuggestionSelectors {
    constructor(private readonly store: Store<RootState>) {}

    isLoading$ = this.store.select(isLoading);
    suggestion$ = this.store.select(getSuggestion);
    totalProducts$ = this.store.select(totalProducts);
    page$ = this.store.select(page);
}
