import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { AccountStatus, DocumentStatus, IbanStatus } from 'src/app/shared/models/models';
import * as BankDetailsAction from '../actions/bank-details.actions';
import { DocumentType } from '../actions/bank-details.actions';
import { RootState } from '../models';

@Injectable()
export class BankDetailsDispatchers {
    constructor(private readonly store: Store<RootState>) {}

    loadBankDetails(): void {
        this.store.dispatch(BankDetailsAction.loadBankDetails());
    }

    documentUploaded(type: DocumentType, status: DocumentStatus | IbanStatus) {
        this.store.dispatch(BankDetailsAction.documentUploaded({ documentType: type, status }));
    }

    accountUpdated(status: AccountStatus) {
        this.store.dispatch(BankDetailsAction.accountUpdated({ status }));
    }
}
