import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { RootState } from '../models';
import { loading, purchases, sales } from '../selectors/orders.selectors';

@Injectable()
export class OrdersSelectors {
    constructor(private readonly store: Store<RootState>) {}

    loading$ = this.store.select(loading);
    sales$ = this.store.select(sales);
    purchases$ = this.store.select(purchases);
}
