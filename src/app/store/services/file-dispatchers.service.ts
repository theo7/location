import { Injectable } from '@angular/core';
import { Photo as CameraPhoto } from '@capacitor/camera';
import { Store } from '@ngrx/store';
import { FileType, PictureType } from '../../shared/models/models';
import { fileActions } from '../actions/file.actions';
import { RootState } from '../models';

@Injectable()
export class FileDispatchers {
    constructor(private readonly store: Store<RootState>) {}

    selectFile(fileType: FileType): void {
        this.store.dispatch(fileActions.selectFile({ fileType }));
    }

    uploadRawPicture(file: Blob, index: number, pictureType: PictureType): void {
        this.store.dispatch(fileActions.uploadRawPicture({ file, index, pictureType }));
    }

    uploadProductPicture(picture: CameraPhoto, index: number, pictureType: PictureType): void {
        this.store.dispatch(fileActions.uploadProductPicture({ picture, index, pictureType }));
    }

    uploadChatPicture(picture: CameraPhoto, discussionId: number, index: number): void {
        this.store.dispatch(fileActions.uploadChatPicture({ picture, discussionId, index }));
    }

    uploadKycFile(file: CameraPhoto, fileType: FileType): void {
        this.store.dispatch(fileActions.uploadKycFile({ file, fileType }));
    }
}
