import { RouterReducerState } from '@ngrx/router-store';
import { BankDetailsState } from './reducers/bank-details.reducer';
import { CategoriesState } from './reducers/categories.reducer';
import { ConnectionState } from './reducers/connection.reducer';
import { CashState } from './reducers/credit.reducer';
import { DiscussionsState } from './reducers/discussions.reducer';
import { FiltersState } from './reducers/filters.reducer';
import { FollowState } from './reducers/follow.reducer';
import { NotificationState } from './reducers/notifications.reducer';
import { OrdersState } from './reducers/orders.reducer';
import { ProductState } from './reducers/product.reducer';
import { ProfileState } from './reducers/profile.reducer';
import { RatingsState } from './reducers/ratings.reducer';
import { RepositoriesState } from './reducers/repositories.reducer';
import { SearchState } from './reducers/search.reducer';
import { SettingsState } from './reducers/settings.reducer';
import { SuggestionState } from './reducers/suggestion.reducer';
import { UserProfileState } from './reducers/user.reducer';
import { WishlistState } from './reducers/wishlist.reducer';

export interface EntityState {
    userProfile: UserProfileState;
    discussions: DiscussionsState;
    categories: CategoriesState;
    search: SearchState;
    repositories: RepositoriesState;
    connection: ConnectionState;
    product: ProductState;
    bankDetails: BankDetailsState;
    wishlist: WishlistState;
    orders: OrdersState;
    settings: SettingsState;
    filters: FiltersState;
    cashState: CashState;
    profile: ProfileState;
    notifications: NotificationState;
    follow: FollowState;
    suggestion: SuggestionState;
    ratings: RatingsState;
}

export type RootState = {
    entityCache: EntityState;
    router: RouterReducerState<any>;
};
