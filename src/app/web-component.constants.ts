const customLink = 'custom-link';
const internalLink = 'internal-link';

export const WebComponentConstants = {
    customLink,
    internalLink,
};
