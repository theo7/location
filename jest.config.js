module.exports = {
    collectCoverage: true,
    collectCoverageFrom: ['src/**/*.ts'],
    coverageDirectory: 'coverage',
    coverageReporters: ['lcov'],
    testResultsProcessor: 'jest-sonar-reporter',
    silent: true,
    verbose: false,
    setupFiles: ["./src/setupJest.ts"],
    moduleNameMapper: {
        "^lodash-es$": "lodash"
    }
};
