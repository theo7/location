# Contributing to SecondHand

## Commit Message Format

```
<gitmoji> : <short summary>
    │             │
    │             └─⫸ Summary in present tense. Not capitalized. No period at the end.
    │
    └─⫸ Gitmoji list : https://gitmoji.dev/
```

The `<gitmoji>` and `<short summary>` fields are mandatory.

### Exemple

```
:sparkles: : add user page
```
