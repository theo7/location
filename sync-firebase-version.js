const firebasePackage = require('./node_modules/firebase/package.json');
const fs = require('fs');
const { exit } = require('process');

const serviceWorkerFilePath = "./src/service-worker.js";
const input = fs.readFileSync(serviceWorkerFilePath, { encoding: "utf8" }); 

const regex = /var firebaseVersion = ['|"](.+)['|"];/;

const regexResult = regex.exec(input);
const versionInSw = regexResult && regexResult[1];

const currentVersion = firebasePackage.version;

if (!versionInSw) {
    console.error("Oo Service Worker does not have the firebase version. WTF happened?!");
    exit(404);
}

if (currentVersion !== versionInSw) {
    const output = input.replace(regex, `var firebaseVersion = '${currentVersion}';`);
    fs.writeFileSync(serviceWorkerFilePath, output);

    console.log("Service Worker updated successfully!");
}