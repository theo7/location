const { I, commonPage, productPage } = inject();

const assert = require('assert');

// Add in your custom step files
var CurrentUser;
var Price;
var Size;
var Brand;
var Order;
var NbImages = 0;
var User;
var Pwd;
var LastPrice;
var CashAmount;
var VoucherAmount;
var IdDiscussion;

Given("je ne peux pas l'acheter", async () => {
    var buyButton = productPage.buyButton; // buyButton
    I.dontSeeElement(buyButton);
});

Given("je vois le bon nombre d'images", async () => {
    let imgs = locate({ css: '[data-qa=picture]' });
    I.seeElement(imgs);
    var nbItems = await I.grabNumberOfVisibleElements(imgs);
    console.log(nbItems);
    NbImages = commonPage.getProductInfos('nbImages');
    console.log(NbImages);
    if (nbItems == NbImages) {
        assert(nbItems == NbImages);
        console.assert('Nb Images OK');
    } else {
        assert.fail('Toutes les images ne sont pas présentes');
    }
    NbImages = 0;
});

Given("je vois le bon nombre d'images sur mobile", async () => {
    let imgs = locate({ css: '[data-qa=carouselPicture]' });
    I.seeElement(imgs);
    var nbItems = await I.grabNumberOfVisibleElements(imgs);
    console.log(nbItems);
    console.log(NbImages);
    if (nbItems == NbImages) {
        assert(nbItems == NbImages);
        console.assert('Nb Images OK');
    } else {
        assert.fail('Toutes les images ne sont pas présentes');
    }
});

Given('je démarre une conversation', async () => {
    let btn = locate({ css: '[data-qa=btnStartDiscussion]' });
    I.seeElement(btn);
    I.click(btn);
});

Given("j'ajoute un produit au lot à partir du chat", async () => {
    btn = locate('mat-expansion-panel-header');
    I.seeElement(btn);
    I.click(btn);
    btn = locate({ css: '[data-qa=btnAddProduct]' });
    I.seeElement(btn);
    I.click(btn);
    btn = locate({ css: '[data-qa=btnAddToBundle]' });
    I.seeElement(btn);
    I.click(btn);
    btn = locate({ css: '[data-qa=btnContinue]' });
    I.seeElement(btn);
    I.click(btn);
});

module.exports = {
    CurrentUser: CurrentUser,
    Price: Price,
    Size: Size,
    Brand: Brand,
    Order: Order,
    NbImages: NbImages,
    User: User,
    Pwd: Pwd,
    LastPrice: LastPrice,
    CashAmount: CashAmount,
    VoucherAmount: VoucherAmount,
    IdDiscussion: IdDiscussion,
};
