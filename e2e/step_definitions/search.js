const { I, commonPage, searchFormPage } = inject();

// dev dependencies
const env = require('../dev/environment');
var index = 1;
Given('je remplis la fenetre de recherche avec {string}', async value => {
    I.fillField(commonPage.searchInputBrand, value);
    var field = locate('span').withText(value);
    I.wait(1);
    I.forceClick(field);

    I.runOnAndroid(async () => {
        I.hideDeviceKeyboard();
    });
    I.runOnIOS(async () => {
        I.hideDeviceKeyboard('tapOutside');
    });
    Brand = value;
    commonPage.feedProductInfos('brand', value);
    if (env.vars.device == 'desktop') {
        I.pressKey('Escape');
    }
});

Given("j'ouvre la fenêtre des filtres", async value => {
    if (env.vars.device != 'desktop') {
        let elmtFilter = locate('button').withChild(locate('span').withText('Trier & Filtrer'));
        I.click(elmtFilter);
    } else {
        let elmt = searchFormPage.filterList;
        I.seeElement(elmt);
        I.click(elmt);
    }
});

Given('je lance la recherche', async value => {
    let elmt = commonPage.btnSubmitSearch;
    I.seeElement(elmt);
    I.click(elmt);
});

Given('je clique sur le champ {string} et je choisis {string}', async (field, value) => {
    if (field == 'productBrands') {
        I.seeElement(searchFormPage.productBrandField);
        I.click(searchFormPage.productBrandField);
        I.seeElement(commonPage.searchInput);
        I.fillField(commonPage.searchInput, value);
        I.seeElement(locate('mat-option').first());
    }
    I.runOnIOS(async () => {
        I.click(locate('mat-option').first());
        I.hideDeviceKeyboard('tapOutside');
        I.touchPerform([
            {
                action: 'press',
                options: {
                    x: 370,
                    y: 259,
                },
            },
            { action: 'release' },
        ]);
    });
    I.runOnAndroid(async () => {
        I.forceClick(locate('mat-option').first());
        I.hideDeviceKeyboard();
        I.pressKey('Escape');
    });
});

Given('je veux préciser la section {string}', async section => {
    let btn;
    if (section == 'catégories') {
        btn = searchFormPage.categories;
    }
    if (section == 'marques') {
        btn = searchFormPage.brands;
    }
    if (section == 'tailles') {
        btn = searchFormPage.sizes;
    }
    if (section == 'couleurs') {
        btn = searchFormPage.colors;
    }
    if (section == 'conditions') {
        btn = searchFormPage.conditions;
    }
    within(btn, () => {
        btn = locate('button');
        I.click(btn);
    });
});

Given("je choisis pour la section {string} l'item {string}", async (section, item) => {
    I.wait(2);
    let inputValues = item.split(';');
    if (section == 'catégories' || section == 'sous-catégories') {
        for (let i = 0; i < inputValues.length; i++) {
            let elmt = locate({
                css: '[data-qa=' + item + ']',
            });
            elmt = locate('button').inside(elmt);
            I.click(elmt);
        }
    } else if (section == 'marques') {
        for (let i = 0; i < inputValues.length; i++) {
            let elmt = locate({
                css: '[data-qa=' + item + ']',
            });
            I.seeElement(elmt);
            if (env.vars.device == 'samsung' || env.vars.device == 'desktop') {
            } else {
                within(elmt, () => {
                    let mat = locate('input').withAttr({ type: 'checkbox' });
                    I.click(mat);
                });
            }
        }
    } else if (section == 'couleurs' || section == 'conditions') {
        for (let i = 0; i < inputValues.length; i++) {
            let elmt = locate('span').withText(inputValues[i]);
            let scope = locate('app-tab-filter-item-selectable').withChild(elmt);
            I.click(elmt);
        }
    } else {
        for (let i = 0; i < inputValues.length; i++) {
            let elmt = locate('app-tab-filter-item-selectable').withText(item);
            I.seeElement(elmt);
            within(elmt, () => {
                let checkbox = locate('mat-checkbox');
                I.seeElement(checkbox);
                I.click(checkbox);
            });
        }
    }
});

Given('je valide cette section', async () => {
    let btn = locate('span').withText('Valider').at(index);
    I.seeElement(btn);
    I.click(btn);
    index++;
});

Given('je tape dans le champ de recherche {string}', async value => {
    let scope = locate('div').withAttr({ class: 'filter-container' });
    let input = locate('input').inside(scope);
    I.seeElement(input);
    I.fillField(input, value);
});

module.exports = {};
