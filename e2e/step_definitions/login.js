const { I, commonPage, loginPage } = inject();

const env = require('../dev/environment');

var CurrentUser;

Given("je me connecte à l'environnement", () => {
    I.amOnPage(env.urls.staging);
    I.seeInCurrentUrl(env.urls.staging);
    I.seeElement(commonPage.popup);
    // I.seeElement(commonPage.popup);

    var pwdField = locate('input').withAttr({ type: 'password' });
    I.fillField(pwdField, 'shkiabi2020');

    let btn = locate('span').withText('OK');
    I.seeElement(btn);
    I.click(btn);
    // I.visualCheck('homePageNotLogedIn.png', 40);
});

Given('je me connecte avec le {string}', async user => {
    let btnMenu = commonPage.goProfil;
    I.seeElement(btnMenu);
    I.click(btnMenu);
    CurrentUser = user;
    I.click(loginPage.btnLogin);
    I.wait(1);
    let field = loginPage.fieldEmail;
    I.seeElement(field);
    I.fillField(field, env.users[CurrentUser]['email']);
    I.wait(1);
    field = loginPage.fieldPassword;
    I.seeElement(field);
    I.fillField(field, env.users[CurrentUser]['password']);
    I.wait(1);
    if (env.vars.device != 'desktop') {
        I.runOnAndroid(() => {
            I.hideDeviceKeyboard();
        });
    }
    let btn = loginPage.btnLogin;
    I.seeElement(btn);
    I.click(btn);
    if (env.vars.device != 'desktop') {
        I.runOnAndroid(() => {
            I.hideDeviceKeyboard();
        });
        I.wait(1);
        I.click(commonPage.goHome);
    }
    let firstPost = locate('app-product-card').first();
    I.seeElement(firstPost);
    // I.visualCheck('homePageLogedIn.png', 20);
});

module.exports = {
    CurrentUser: CurrentUser,
};
