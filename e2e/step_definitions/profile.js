const { I, commonPage, profilePage, chatPage } = inject();

const assert = require('assert');

// Add in your custom step files
var CurrentUser;
var Price;
var Size;
var Brand;
var Order;
var NbImages = 0;
var User;
var Pwd;
var LastPrice;
var CashAmount;
var VoucherAmount;
var IdDiscussion;

Given("je vérifie l'état de ma commande {string}", async status => {
    let orders = profilePage.myPurchasesSales;
    I.click(orders);
    let elmt = locate('div').withAttr({ class: 'order-card-container' }).first();
    I.seeElement(elmt);
    within(elmt, () => {
        I.see(status);
    });

    I.seeElement(elmt);
});

Given('je vérifie le montant de mon BA en attente {string}', async user => {
    let elmt = commonPage.goProfil;
    I.seeElement(elmt);
    I.click(elmt);
    let amountVoucherExpected;
    let pot = profilePage.myJackpots;
    I.click(pot);
    let tab2 = locate('div').withAttr({ role: 'tab' }).at(2);
    I.seeElement(tab2);
    I.click(tab2);
    let holdingCredit = profilePage.voucherHoldingCredit;
    holdingCredit = await I.grabTextFrom(holdingCredit);
    console.log(holdingCredit);
    if (user == 'acheteur') {
        amountVoucherExpected = commonPage.seeBundleInfos('amountVoucherExpectedBuyer');
        holdingCredit.includes(amountVoucherExpected);
    } else if (user == 'vendeur') {
        amountVoucherExpected = commonPage.seeBundleInfos('amountVoucherExpectedSeller');
        if (holdingCredit.includes(amountVoucherExpected)) {
            holdingCredit.includes(amountVoucherExpected);
        } else {
            assert.fail('Le montant de la cagnotte est de ' + holdingCredit);
        }
    }
});

Given('je vérifie le montant de mon BA {string}', async user => {
    let amountVoucherExpected;
    let pot = profilePage.myJackpots;
    I.click(pot);
    let tab2 = locate('div').withAttr({ role: 'tab' }).at(2);
    I.seeElement(tab2);
    I.click(tab2);
    let actualCredit = profilePage.voucherCreditAmount;
    actualCredit = await I.grabTextFrom(actualCredit);
    console.log(actualCredit);
    if (user == 'acheteur') {
        amountVoucherExpected = commonPage.seeBundleInfos('amountVoucherExpectedBuyer');
        actualCredit.includes(amountVoucherExpected);
    } else if (user == 'vendeur') {
        amountVoucherExpected = commonPage.seeBundleInfos('amountVoucherExpectedSeller');

        if (actualCredit.includes(amountVoucherExpected)) {
            actualCredit.includes(amountVoucherExpected);
        } else {
            assert.fail('Le montant de la cagnotte est de ' + actualCredit + ' au lieu de ' + amountVoucherExpected);
        }
    }
});

Given('je vérifie que le BA a été émis automatiquement', async () => {
    let pot = profilePage.myJackpots;
    I.click(pot);
    let tab2 = locate('div').withAttr({ role: 'tab' }).at(2);
    I.seeElement(tab2);
    I.click(tab2);
    let voucherLine = locate('a').withAttr({ 'data-qa': 'voucherEmittedLine' });
    I.seeElement(voucherLine);
});

Given('je me déconnecte', async () => {
    let btn = commonPage.goProfil;
    I.seeElement(btn);
    I.click(btn);
    I.wait(1);
    btn = profilePage.btnLogOut;
    I.seeElement(btn);
    I.forceClick(btn);
});

Given('je quitte le chat et me déconnecte', () => {
    I.runOnIOS(() => {
        let btn = chatPage.btnHeaderReturn;
        I.seeElement(btn);
        I.click(btn);
    });
    I.runOnAndroid(() => {
        let btn = chatPage.btnHeaderReturn;
        I.seeElement(btn);
        I.click(btn);
    });

    btn = commonPage.goProfil;
    I.seeElement(btn);
    I.click(btn);
    I.wait(1);
    btn = profilePage.btnLogOut;
    I.seeElement(btn);
    I.forceClick(btn);
});

Given('je me rend sur mon profil', async () => {
    I.wait(1);
    let btn = commonPage.goProfil;
    I.seeElement(btn);
    I.seeElement(btn);
    I.click(btn);
    I.wait(1);
});

module.exports = {
    CurrentUser: CurrentUser,
    Price: Price,
    Size: Size,
    Brand: Brand,
    Order: Order,
    NbImages: NbImages,
    User: User,
    Pwd: Pwd,
    LastPrice: LastPrice,
    CashAmount: CashAmount,
    VoucherAmount: VoucherAmount,
    IdDiscussion: IdDiscussion,
};
