const { I, commonPage, chatPage } = inject();

// dev dependencies
const assert = require('assert');
const env = require('../dev/environment');

// Add in your custom step files
var Price;
var LastPrice;
var VoucherAmount;
var IdDiscussion;
var voucherAmount;
var CurrentUser;

Given('je calcule une offre inférieure au dernier prix', async () => {
    I.wait(3);
    let price = chatPage.chatPrice; // new locator
    price = price.first();
    price = await I.grabTextFrom(price);
    price = price.replace(',', '.');
    price = price.replace('-', '');
    price = price.replace(/[^\d.-]/g, '');
    price = parseFloat(price);
    console.log(price);
    price = price - price * 0.1;
    price = Number.parseFloat(price).toFixed(2);
    console.log(price);
    LastPrice = price;
    commonPage.feedProductInfos('price', price);
});

Given('je calcule une contre-offre supérieure au dernier prix', async () => {
    let price = chatPage.lastPriceMsg; // new
    I.seeElement(price);
    price = await I.grabTextFrom(price);
    price = price.replace(',', '.');
    price = price.replace('-', '');
    price = price.replace(/[^\d.-]/g, '');
    price = parseFloat(price);
    console.log(price);
    price = price + price * 0.1;
    price = Number.parseFloat(price).toFixed(2);
    console.log(price);
    LastPrice = price;
    commonPage.feedProductInfos('price', price);
});

Given('je calcule une offre < 50% du dernier prix', async () => {
    let price = chatPage.chatPrice; // new locator

    I.seeElement(price);
    price = await I.grabTextFrom(price);
    price = price.replace(',', '.');
    price = price.replace('-', '');
    price = price.replace(/[^\d.-]/g, '');
    price = parseFloat(price);
    console.log(price);
    price = price - price * 0.51;
    price = Number.parseFloat(price).toFixed(2);
    console.log(price);
    LastPrice = price;
});

Given('je valide la conformité du colis', async () => {
    I.wait(1);
    I.click(chatPage.btnConfirmPackageCompliance);
    I.seeElement(chatPage.radioConfirm);
    I.forceClick(chatPage.radioConfirm);
    I.click(chatPage.btnValidate);
    let message = locate('app-chat-message-package-compliant').withAttr({ 'data-qa': 'messageBundleCompliant' });
    I.wait(3);
    I.seeElement(message);
    // I.visualCheck('chatPackageCompliant.png', 10);
});

Given('je refuse la conformité du colis', async () => {
    I.wait(1);
    I.click(chatPage.btnConfirmPackageCompliance);
    I.seeElement(commonPage.popup);

    within(commonPage.popup, () => {
        I.seeElement(chatPage.radioRefuse);
        I.forceClick(chatPage.radioRefuse);
        I.click(commonPage.btnValidate);
    });
    let message = locate('app-chat-message-package-not-compliant');
    I.seeElement(message);
    // I.visualCheck('chatPackageNotCompliant.png', 10);
});

Given("j'essaie d'envoyer mon offre sans succès", async () => {
    let tempPrice = JSON.stringify(LastPrice);
    tempPrice = tempPrice.split('');
    for (let i = 0; i < tempPrice.length; i++) {
        I.pressKey(tempPrice[i]);
    }
    within(commonPage.popup, () => {
        let btn = locate('button').withAttr({ disabled: 'true' });
        I.seeElement(btn);
    });
});

Given('je clique sur la conversation avec {string}', user => {
    let scope = locate('mat-list-option');
    I.seeElement(scope);
    let conversationOld = locate('div').withAttr({ class: 'content' }).withText(env.users[user].pseudo).inside(scope);
    let conversation = locate('div').withAttr({ 'data-qa': 'nickName' }).withText(env.users[user].pseudo).inside(scope);
    I.seeElement(conversation);
    I.click(conversation);
});

Given('je me rend sur le chat', () => {
    I.wait(1);
    let btn = commonPage.goChat;
    I.seeElement(btn);
    I.seeElement(btn);
    I.click(btn);
});

Given("j'envoie mon offre", () => {
    I.click(chatPage.btnMakeOffer);
    I.seeElement(chatPage.writeAmount);
    I.fillField(chatPage.writeAmount, LastPrice);
    I.click(chatPage.btnSendOffer);
    let offerLine = locate('app-chat-message-offer-made');
    I.seeElement(offerLine);
    // I.visualCheck('chatOfferMade.png', 10);
    LastPrice = null;
});

Given("j'annule la popup", () => {
    within(commonPage.popup, () => {
        let elmt = locate('span').withText('Annuler');
        I.click(elmt);
    });
});

Given("j'envoie ma première offre", async () => {
    I.click(chatPage.btnMakeOffer);
    I.seeElement(commonPage.popup);
    let btn = locate('span').withText("OK, c'est noté !");
    I.click(btn);
    btn = chatPage.btnSendMessage;
    // let tempPrice = JSON.stringify(LastPrice);
    // tempPrice = tempPrice.split('');
    // for (let i = 0; i < tempPrice.length; i++) {
    //     I.pressKey(tempPrice[i]);
    // }
    if (env.vars.device != 'desktop') {
        I.seeElement(chatPage.writeAmount);
        I.wait(2);
        I.fillField(chatPage.writeAmount, LastPrice);
        I.wait(2);
        I.click(chatPage.btnSendOffer);
    } else {
        let field = locate('input').withAttr({ 'data-placeholder': 'Montant' });
        I.fillField(field, LastPrice);
        btn = locate('span').withText('Envoyer');
        I.click(btn);
    }
    I.wait(5);
    let offerLine = locate('app-chat-message-offer-made');
    I.seeElement(offerLine);
    // I.visualCheck('chatFirstOfferMade.png', 10);

    LastPrice = null;
});

Given("j'envoie ma contre-offre", async () => {
    I.click(chatPage.btnCounterOffer);
    I.runOnIOS(() => {
        I.seeElement(chatPage.writeAmount);
        I.wait(2);
        I.fillField(chatPage.writeAmount, LastPrice);
        I.wait(2);
        I.click(chatPage.btnSendOffer);
    });
    I.runOnAndroid(() => {
        I.seeElement(chatPage.writeAmount);
        I.wait(2);
        I.fillField(chatPage.writeAmount, LastPrice);
        I.wait(2);
        I.click(chatPage.btnSendOffer);
    });
    let field = locate('input').withAttr({ 'data-placeholder': 'Montant' });
    let res = await tryTo(() => I.fillField(field, LastPrice));
    if (res == true) {
        let btn = locate('span').withText('Envoyer');
        I.click(btn);
    }
    I.wait(5);
    let counterOfferLine = locate('app-chat-message-counter-offer-made');
    I.seeElement(counterOfferLine);
    // I.visualCheck('chatCounterOfferMade.png', 10);
    I.feedBundleInfos('counterOffer', true);
    LastPrice = null;
});

Given("j'accepte la dernière offre", async () => {
    let lastPrice = chatPage.lastPriceMsg;
    lastPrice = await I.grabTextFrom(lastPrice);
    lastPrice = lastPrice.replace(',', '.');
    lastPrice = lastPrice.replace('-', '');
    console.log(lastPrice);
    lastPrice = lastPrice.replace(/[^\d.-]/g, '');
    console.log(lastPrice);
    lastPrice = lastPrice.replace('.', ',');
    console.log(lastPrice);
    I.see(lastPrice);
    LastPrice = lastPrice;
    Price = LastPrice;
    let elmt = chatPage.btnAcceptOffer;
    I.seeElement(elmt);
    I.click(elmt);
    elmt = chatPage.reserveProduct;
    I.wait(5);
    I.seeElement(elmt);
    I.click(elmt);
});

Given('la vente est confirmée', () => {
    I.wait(5);
    let messageLine = locate('app-chat-message-lot-prepaid');
    I.seeElement(messageLine);
    // I.visualCheck('chatPackagePrepaid.png', 10);
});

Given("j'envoie le lot et je récupère le montant en BA", async () => {
    let elmt = chatPage.openApproveSellDialog;
    I.seeElement(elmt);
    I.click(elmt);
    I.seeElement(commonPage.popup);
    elmt = chatPage.btnConfirmRecep;
    I.seeElement(elmt);
    I.click(elmt);

    let elmtPrice = chatPage.bundlePrice;
    LastPrice = await I.grabTextFrom(elmtPrice);
    console.log(LastPrice);
    let radio = chatPage.radioVoucher;
    I.seeElement(radio);
    // let res = await tryTo(() => I.forceClick(radio));

    I.runOnIOS(() => {
        let radio = chatPage.radioVoucher;
        I.seeElement(radio);
        I.click(radio);
    });
    I.runOnAndroid(() => {
        radio = chatPage.radioVoucher;
        I.seeElement(radio);
        I.forceClick(radio);
    });

    let amount = locate('span').withAttr({ class: 'amount' }).first();
    amount = await I.grabTextFrom(amount);
    let voucherAmountTx = amount;
    console.log(voucherAmountTx);
    voucherAmountTx = voucherAmountTx.replace(',', '.');
    voucherAmountTx = voucherAmountTx.replace(/[^\d.-]/g, '');
    voucherAmount = parseFloat(voucherAmount);
    commonPage.feedBundleInfos('amountVoucherExpectedSeller', voucherAmountTx);
    console.log(voucherAmountTx);
    VoucherAmount = voucherAmount;

    elmt = chatPage.btnConfirm;
    I.seeElement(elmt);
    I.click(elmt);
    I.wait(5);

    elmt = locate('app-chat-message-lot-prepaid');
    I.seeElement(elmt);
    // I.visualCheck('chatPackagePrepaid.png', 10);

    let temp = await I.grabCurrentUrl();
    temp = temp.split('/');
    let nb = temp.length - 1;
    temp = temp[nb];
    console.log(temp);
    IdDiscussion = temp;

    let id = await I.grabLotId(IdDiscussion);
    console.log(id);
    id = id.lot_id;
    console.log(id);
    let response = await I.postSent(id);
    console.log('reponse: ', response);
    let bundleSentLine = locate('app-chat-message-package-sent');
    if (response == 200) {
        I.seeElement(bundleSentLine);
        await I.postInRelais(id);
        I.wait(3);
        let bundleInRelayLine = locate('app-chat-message-package-sent');
        I.seeElement(bundleInRelayLine);
        // I.visualCheck('chatPackageSent.png', 10);
    } else {
        I.dontSeeElement(bundleSentLine);
        assert.fail("Le colis n'a pas pu être envoyé, erreur API = " + response);
    }
    console.log(LastPrice);
    I.runOnIOS(() => {
        elmt = chatPage.btnHeaderReturn;
        I.seeElement(elmt);
        I.click(elmt);
    });
    I.runOnAndroid(() => {
        elmt = chatPage.btnHeaderReturn;
        I.seeElement(elmt);
        I.click(elmt);
    });
});

Given('je quitte le chat', () => {
    let btn = chatPage.btnHeaderReturn;
    I.runOnIOS(() => {
        I.seeElement(btn);
        I.click(btn);
    });
    I.runOnAndroid(() => {
        I.seeElement(btn);
        I.click(btn);
    });
});

Given('je supprime le dernier produit', async () => {
    let amountToPay = locate('div').withAttr({ 'data-qa': 'productPrice' }).first();
    let amountToPayBeforeDeleting = await I.grabTextFrom(amountToPay);

    let scope = locate('div').withAttr({ class: 'product-line' }).last();
    I.seeElement(scope);
    within(scope, () => {
        let elmt = locate('img').withAttr({ alt: 'delete button' });
        let btn = locate('button').withChild(elmt);
        I.seeElement(btn);
        I.click(btn);
    });
    let popin = locate('mat-dialog-container');
    I.seeElement(popin);
    within(popin, () => {
        let btn = locate('button').withAttr({ type: 'submit' });
        I.seeElement(btn);
        I.click(btn);
    });
    let amountToPayAfterDeleting = await I.grabTextFrom(amountToPay);
    if (amountToPayBeforeDeleting > amountToPayAfterDeleting) {
        assert(amountToPayBeforeDeleting > amountToPayAfterDeleting);
    } else
        assert.fail("Le montant total après suppression n'est pas inférieur au prix avant la suppression de l'article");
});

module.exports = {
    CurrentUser: CurrentUser,
    Price: Price,
    LastPrice: LastPrice,
    VoucherAmount: VoucherAmount,
    IdDiscussion: IdDiscussion,
};
