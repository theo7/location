const { I, commonPage } = inject();

const env = require('../dev/environment');

// Add in your custom step files
var CurrentUser;
var Price;
var Size;
var Brand;
var Order;
var NbImages = 0;
var User;
var Pwd;
var LastPrice;
var CashAmount;
var VoucherAmount;
var IdDiscussion;

Given("j'ajoute 1 image", async () => {
    I.wait(5);

    if (env.vars.device != 'desktop') {
        // The following xPaths have been created using UiAutomator from Android Studio
        // Todo: Make the same step using same tool for iOS (I.runOnIos...)

        I.runOnAndroid(() => {
            // let ctxts = await I.grabAllContexts();
            // console.log(ctxts);
            let scope2 = locate('div').withAttr({ class: 'images-uploader-container' });
            I.seeElement(scope2);
            I.click(scope2);
            I.wait(2);
            I.switchToNative('NATIVE_APP');
            I.seeElement('ALLOW');
            I.wait(2);
            I.tap('ALLOW');
            I.seeElement('#com.android.chrome:id/bitmap_view');
            I.wait(2);
            I.tap('#com.android.chrome:id/bitmap_view');
            I.seeElement('ALLOW');
            I.tap('ALLOW');
            I.wait(2);

            I.tap('TURN ON');
            I.seeElement('Shutter');
            I.tap('Shutter');
            I.seeElement('OK');
            I.tap('OK');
            I.switchToWeb('CHROMIUM');
        });
    } else {
        I.attachFile(btnAttachFile, 'dev/images/img1.jpg');
    }
});

Given('je renseigne le champ {string} avec {string}', async (field, value) => {
    let initialField = field;
    console.log(initialField);
    field = locate({ css: '[data-qa=' + field + ']' });
    I.fillField(field, value);
    if (initialField == 'price') {
        value = value.replace('.', ',');
    }
    if (env.vars.device != 'desktop') {
        I.runOnAndroid(async () => {
            await tryTo(() => I.hideDeviceKeyboard());
        });
        I.runOnIOS(async () => {
            await tryTo(() => I.hideDeviceKeyboard());
        });
    }
    commonPage.feedProductInfos(initialField, value);
});

Given('dans le champ {string} je choisis {string}', async (field, values) => {
    let arrayValues = values.split(';');
    let elmt = locate({
        css: '[data-qa=' + field + ']',
    });
    I.seeElement(elmt);
    I.click(elmt);
    for (var i = 0; i < arrayValues.length; i++) {
        let option = locate('mat-option').withText(arrayValues[i]);
        I.seeElement(option);
        I.click(option);
    }
    if (env.vars.device != 'desktop') {
        I.runOnAndroid(async () => {
            if (
                field == 'productSizes' ||
                field == 'productColors' ||
                field == 'productBrands' ||
                field == 'productConditions'
            ) {
                I.pressKey('Escape');
            }
            I.hideDeviceKeyboard();
        });
    }
    if (field == 'productSizes' || field == 'productColors' || field == 'productBrands') {
        I.pressKey('Escape');
    }
    commonPage.feedProductInfos(field, values);
});

Given("je met l'annonce en ligne", async () => {
    let elmt = commonPage.btnSubmit;
    I.seeElement(elmt);
    I.click(elmt);
    console.log('Post = ' + commonPage.Post);
    I.see('en cours de modération');
    let btn = locate('span').withText('Continuer');
    I.seeElement(btn);
    I.click(btn);
});

module.exports = {
    CurrentUser: CurrentUser,
    Price: Price,
    Size: Size,
    Brand: Brand,
    Order: Order,
    NbImages: NbImages,
    User: User,
    Pwd: Pwd,
    LastPrice: LastPrice,
    CashAmount: CashAmount,
    VoucherAmount: VoucherAmount,
    IdDiscussion: IdDiscussion,
};
