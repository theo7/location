const { I, commonPage, profilePage, chatPage, searchFormPage, homePage } = inject();

const env = require('../dev/environment');

// Add in your custom step files
var CurrentUser;
var Price;
var Size;
var Brand;
var Order;
var NbImages = 0;
var User;
var Pwd;
var LastPrice;
var CashAmount;
var VoucherAmount;
var IdDiscussion;

Given('je vois une popup apparaitre', () => {
    I.seeElement(commonPage.popup);
    I.seeElement(commonPage.popup);
    I.seeElement(commonPage.popup);
});

Given('je valide la popup', () => {
    I.seeElement(commonPage.popup);
    let elmt = locate('span').withText(" OK, c'est noté ! ");
    I.seeElement(elmt);
    I.click(elmt);
});

Given('je clique sur {string}', label => {
    let elmt = locate({ css: '[data-qa=' + label + ']' });
    I.seeElement(elmt);
    if (env.vars.device == 'desktop') {
        I.forceClick(chatPage.radioConfirm);
    } else {
        I.click(chatPage.btnValidate);
    }
});

Given("j'attends {string} secondes", nbSec => {
    I.wait(nbSec);
});

Given('je clique sur le champ {string}', async field => {
    let temp = '[data-qa=' + field + ']';
    let elmt = locate({ css: temp });
    I.seeElement(elmt);
    I.click(elmt);
});

Given('je vois tous les elements de la page {string} sur mobile', (page, commonPage) => {
    I.seeElement(commonPage.topMenu);
    I.seeElement(commonPage.bottomMenu);
    if (page == 'accueil') {
        I.seeElement(homePage.lastPosts);
        I.seeElement(homePage.carousel);
    } else if (page == 'search') {
        I.seeElement(searchFormPage.btnFilters);
        I.seeElement(searchFormPage.btnGoogle);
        I.seeElement(searchFormPage.searchArea);
    } else if (page == 'chat') {
        I.seeElement(chatPage.headerChat);
        I.seeElement(chatPage.listMsg);
    } else if (page == 'profil') {
        I.seeElement(profilePage.profilInfos);
    }
});

Given("j'accepte les cookies", async () => {
    let cookies = locate('a').withAttr({ 'aria-label': 'allow cookies' });
    I.seeElement(cookies);
    I.click(cookies);
});

Given('je recherche par mots clefs {string}', async text => {
    if (env.vars.device == 'desktop') {
        let elmt = locate({ css: '[data-qa=searchInput]' });
        I.fillField(elmt, text);
        I.wait(3);
    } else {
        let elmt = commonPage.goSearch;
        I.seeElement(elmt);
        I.click(elmt);

        let field = locate('input');
        I.fillField(field, text);
        // I.runOnAndroid(() => {
        //     I.hideDeviceKeyboard();
        // });
    }
});

Given("je vérifie l'intégrité de la page {string} sur desktop", async pageName => {
    I.checkPageIntegrityDesktop(pageName);
});

Given("je vérifie l'intégrité de la page {string} sur mobile", async pageName => {
    I.runOnIOS(() => {
        I.checkPageIntegrityMobile(pageName);
    });
    I.runOnAndroid(() => {
        I.checkPageIntegrityMobile(pageName);
    });
});

Given("je vois un message d'erreur {string}", async msg => {
    let toast = locate('div').withAttr({ class: 'message' }).withText(msg);
    I.seeElement(toast);
});

Given('je prépare une annonce', async () => {
    I.createNewProduct();
});

Given('je clean la db pour les deux utilisateurs', async () => {
    I.cleanUserData(env.users['user1']['id']);
    I.cleanUserData(env.users['user2']['id']);
});

module.exports = {
    CurrentUser: CurrentUser,
    Price: Price,
    Size: Size,
    Brand: Brand,
    Order: Order,
    NbImages: NbImages,
    User: User,
    Pwd: Pwd,
    LastPrice: LastPrice,
    CashAmount: CashAmount,
    VoucherAmount: VoucherAmount,
    IdDiscussion: IdDiscussion,
};
