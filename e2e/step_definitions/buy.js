const { I, commonPage, buyPage, chatPage } = inject();

// dev dependencies
const env = require('../dev/environment');

// Add in your custom step files
var CurrentUser;
var Price;
var Size;
var Brand;
var Order;
var NbImages = 0;
var User;
var Pwd;
var LastPrice;
var CashAmount;
var VoucherAmount;
var IdDiscussion;

Given('je paye le lot avec la carte {string}', async cb => {
    let breadcrumb = buyPage.breadcrumb;
    let bonusKiabi = buyPage.bonusKiabi;
    let item = buyPage.item;
    let PaymentPart = buyPage.paymentPart;
    I.wait(5);
    I.seeElement(breadcrumb);
    I.seeElement(bonusKiabi);
    I.seeElement(item);
    I.seeElement(PaymentPart);
    let elmt = commonPage.btnValidate;
    I.seeElement(elmt);
    I.click(elmt);
    let deliveryPlace = locate('div').withAttr({ class: 'PR-Name' });
    deliveryPlace = deliveryPlace.at(1);
    I.seeElement(deliveryPlace);
    I.click(deliveryPlace);

    elmt = commonPage.btnConfirm;
    I.seeElement(elmt);
    I.click(elmt);

    commonPage.seeProductInfos('price');
    let itemsPriceTotal = buyPage.itemsPriceTotal;
    itemsPriceTotal = await I.grabTextFrom(itemsPriceTotal);
    let leftToPay = buyPage.leftToPay;
    leftToPay = await I.grabTextFrom(leftToPay);
    let shippingFees = buyPage.shippingFees;
    shippingFees = await I.grabTextFrom(shippingFees);
    let handlingFees = buyPage.handlingFees;
    handlingFees = await I.grabTextFrom(handlingFees);
    let totalToPay = buyPage.totalToPay;
    totalToPay = await I.grabTextFrom(totalToPay);
    commonPage.feedBundleInfos('itemsPriceTotal', itemsPriceTotal);
    commonPage.feedBundleInfos('shippingFees', shippingFees);
    commonPage.feedBundleInfos('handlingFees', handlingFees);
    commonPage.feedBundleInfos('totalToPay', totalToPay);
    commonPage.feedBundleInfos('leftToPayAllInclude', leftToPay);

    elmt = buyPage.btnProceedPayment;
    I.seeElement(elmt);
    I.click(elmt);
    I.wait(12);

    let scope = locate('div').withAttr({ class: 'contentWrapper' });
    I.seeElement(scope);
    I.waitInUrl('https://payment-web-mercanet', 10);
    let serialField = buyPage.serialField;
    let monthSelectField = buyPage.monthSelectField;
    let yearSelectField = buyPage.yearSelectField;
    let cvvField = buyPage.cvvField;
    let btn = buyPage.btnFormSubmit;
    let fields = buyPage.fields;

    I.waitSpinnerHides();
    I.seeElement(fields);
    I.fillField(serialField, env.cb[cb].serial);
    I.selectOption(monthSelectField, env.cb[cb].month);
    I.selectOption(yearSelectField, env.cb[cb].year);
    I.fillField(cvvField, env.cb[cb].cvv);
    I.click(btn);
});

Given("j'achète le produit avec la {string}", async cb => {
    let elmt = buyPage.btnBuy;
    I.seeElement(elmt);
    I.click(elmt);
    I.wait(1);
    elmt = commonPage.btnContinue;
    I.seeElement(elmt);
    I.click(elmt);
    I.wait(1);

    let breadcrumb = buyPage.breadcrumb;
    let bonusKiabi = buyPage.bonusKiabi;
    let item = buyPage.item;
    let PaymentPart = buyPage.paymentPart;
    I.seeElement(breadcrumb);
    I.seeElement(bonusKiabi);
    I.seeElement(item);
    I.seeElement(PaymentPart);
    // I.visualCheck('productsPaymentPage.png', 5);

    elmt = commonPage.btnValidate;
    I.seeElement(elmt);
    I.click(elmt);
    I.wait(1);

    // I.visualCheck('deliveryPaymentPage.png', 5);
    let deliveryPlace = locate('div').withAttr({ class: 'PR-Name' }).at(2);
    deliveryPlace = deliveryPlace.at(1);
    I.seeElement(deliveryPlace);
    I.click(deliveryPlace);

    elmt = commonPage.btnConfirm;
    I.seeElement(elmt);
    I.click(elmt);
    I.wait(1);

    // I.visualCheck('recapPaymentPage.png', 10);
    commonPage.seeProductInfos('price');
    let itemsPriceTotal = buyPage.itemsPriceTotal;
    itemsPriceTotal = await I.grabTextFrom(itemsPriceTotal);
    let leftToPay = buyPage.leftToPay;
    leftToPay = await I.grabTextFrom(leftToPay);
    let shippingFees = buyPage.shippingFees;
    shippingFees = await I.grabTextFrom(shippingFees);
    let handlingFees = buyPage.handlingFees;
    handlingFees = await I.grabTextFrom(handlingFees);
    let totalToPay = buyPage.totalToPay;
    totalToPay = await I.grabTextFrom(totalToPay);
    commonPage.feedBundleInfos('itemsPriceTotal', itemsPriceTotal);
    commonPage.feedBundleInfos('shippingFees', shippingFees);
    commonPage.feedBundleInfos('handlingFees', handlingFees);
    commonPage.feedBundleInfos('totalToPay', totalToPay);
    commonPage.feedBundleInfos('leftToPayAllInclude', leftToPay);

    elmt = buyPage.btnProceedPayment;
    I.seeElement(elmt);
    I.click(elmt);
    I.wait(12);

    let scope = locate('div').withAttr({ class: 'contentWrapper' });
    I.wait(3);
    I.seeElement(scope);
    I.waitInUrl('https://payment-web-mercanet', 10);
    let serialField = buyPage.serialField;
    let monthSelectField = buyPage.monthSelectField;
    let yearSelectField = buyPage.yearSelectField;
    let cvvField = buyPage.cvvField;
    let btn = buyPage.btnFormSubmit;
    let fields = buyPage.fields;
    // I.visualCheck('finalPaymentPage.png', 10);

    I.waitSpinnerHides();
    I.seeElement(fields);
    I.seeElement(fields);
    I.fillField(serialField, env.cb[cb].serial);
    I.selectOption(monthSelectField, env.cb[cb].month);
    I.selectOption(yearSelectField, env.cb[cb].year);
    I.fillField(cvvField, env.cb[cb].cvv);
    I.click(btn);

    I.wait(15);
    I.seeElement(chatPage.lastMsg);
});
module.exports = {
    CurrentUser: CurrentUser,
    Price: Price,
    Size: Size,
    Brand: Brand,
    Order: Order,
    NbImages: NbImages,
    User: User,
    Pwd: Pwd,
    LastPrice: LastPrice,
    CashAmount: CashAmount,
    VoucherAmount: VoucherAmount,
    IdDiscussion: IdDiscussion,
};
