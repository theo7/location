const { I, commonPage, productPage, homePage } = inject();

var Price;
var Size;
var Label;

Given('je vois mon annonce dans le fil des annonces', async () => {
    let newPost = locate('app-product-card').first();
    I.seeElement(newPost);
    within(newPost, () => {
        commonPage.seeProductInfos('size');
        commonPage.seeProductInfos('price');
        commonPage.seeProductInfos('condition');
    });
    I.click(newPost);
});

Given("je vois l'annonce précedemment créée", async () => {
    var postItems = homePage.postItems;
    I.wait(3);
    var postItem = postItems.at(1);
    console.log(postItem);
    var brandItem = homePage.brandPostItem;
    var sizeItem = homePage.sizePostItem;
    var priceItem = homePage.pricePostItem;

    I.seeElement(brandItem);
    I.seeElement(sizeItem);
    I.seeElement(priceItem);
    brandItem = await I.grabTextFrom(brandItem);
    brandItem = brandItem[0];
    sizeItem = await I.grabTextFrom(sizeItem);
    sizeItem = sizeItem[0];
    priceItem = await I.grabTextFrom(priceItem);
    priceItem = priceItem[0];

    console.log(brandItem);
    console.log(sizeItem);
    console.log(priceItem);
});

Given("je clique sur l'annonce", async () => {
    var post = homePage.post;
    I.click(post);
    I.wait(5);
    Price = productPage.productPrice;
    Price = await I.grabTextFrom(Price);
    Label = productPage.productTitle;
    Label = await I.grabTextFrom(Label);
    commonPage.feedProductInfos('price', Price);
    commonPage.feedProductInfos('label', Label);
    I.wait(3);
});

Given('je me rend sur la recherche', async () => {
    I.runOnIOS(() => {
        let elmt = commonPage.goSearch;
        I.seeElement(elmt);
        I.click(elmt);
    });
    I.runOnAndroid(() => {
        let elmt = commonPage.goSearch;
        I.seeElement(elmt);
        I.click(elmt);
    });
});

Given("je me rend sur la création d'annonce", async () => {
    let elmt = commonPage.goSell;
    I.seeElement(elmt);
    I.click(elmt);
    // I.visualCheck('postProductForm.png', 2);
});

Given("je me rend sur la page d'accueil", async () => {
    I.runOnIOS(() => {
        let elmt = commonPage.goHome;
        I.seeElement(elmt);
        I.click(elmt);
    });
    I.runOnAndroid(() => {
        let elmt = commonPage.goHome;
        I.seeElement(elmt);
        I.click(elmt);
    });
    await tryTo(() => I.click(commonPage.logoSecondhand));
});

module.exports = {
    Price: Price,
    Size: Size,
    Label: Label,
};
