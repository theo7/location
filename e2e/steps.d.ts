/// <reference types='codeceptjs' />
type steps_file = typeof import('./steps_file.js');
type commonPage = typeof import('./pages/common.js');
type productPage = typeof import('./pages/product.js');
type profilePage = typeof import('./pages/profile.js');
type chatPage = typeof import('./pages/chat.js');
type sellFormPage = typeof import('./pages/sellForm.js');
type searchFormPage = typeof import('./pages/searchForm.js');
type homePage = typeof import('./pages/home.js');
type PostgreConnector = import('./helpers/PostgreConnector.js');
type ApiConnector = import('./helpers/ApiConnector.js');

declare namespace CodeceptJS {
    interface SupportObject {
        I: I;
        current: any;
        commonPage: commonPage;
        productPage: productPage;
        profilePage: profilePage;
        chatPage: chatPage;
        sellFormPage: sellFormPage;
        searchFormPage: searchFormPage;
        homePage: homePage;
    }
    interface Methods extends Playwright, PostgreConnector, ApiConnector {}
    interface I extends ReturnType<steps_file>, WithTranslation<PostgreConnector>, WithTranslation<ApiConnector> {}
    namespace Translation {
        interface Actions {}
    }
}
