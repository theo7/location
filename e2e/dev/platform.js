var capabilities = {
    WDcapabilitiesBSFirefox: {
        acceptSslCerts: true,
        os: 'Windows',
        os_version: '10',
        browserName: 'Firefox',
        browser_version: '74',
        name: "dimitrimasschele1's First Test",
    },

    WDcapabilitiesBSChrome: {
        browserName: 'Chrome',
        os: 'Windows',
        os_version: '10',
        acceptSslCerts: true,
        name: "dimitrimasschele1's First Test",
    },

    APcapabilitiesBSSamsung: {
        'appium:chromeOptions': {
            w3c: false,
        },
        browserName: 'chrome',
        device: 'Samsung Galaxy S8',
        realMobile: 'true',
        os_version: '7.0',
        'browserstack.user': 'manche2',
        'browserstack.key': '7a2i8os5R4FzTottYBXk',
        'browserstack.local': false,
        name: 'Bstack-[Node] Sample Test',
    },

    WDcapabilitiesLocal: {
        browserName: 'chrome',
        chromeOptions: {
            args: [
                '--user-agent=Mozilla/5.0 (Linux; Android 7.0; SM-G892A Build/NRD90M; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/60.0.3112.107 Mobile Safari/537.36',
            ],
        },
        /*proxy: {
      proxyType: "pac",
      proxyAutoconfigUrl: "http://pac.zscaler.net/kiabi.com/kiabiDSI.pac"
    }*/
    },

    APlocalCapabilities: {
        'appium:chromeOptions': {
            w3c: false,
        },
        port: 4444,
        w3c: false,
        browserName: 'chrome',
        device: 'Samsung Galaxy S8',
    },
};

module.exports = {
    capabilities: capabilities,
};
