/*jshint esversion: 8 */

const { I } = inject();

const ppt = require('../dev/environment');

//Common
let popup = locate('mat-dialog-container');
let bottomMenu = locate('app-bottom-navigation');
let topMenu = locate('app-top-navigation');

//Add
let btnAddImages = locate('input')
    .withAttr({
        alt: 'add images',
    })
    .inside(
        locate('div').withAttr({
            class: 'images-uploader-container',
        }),
    );

//Search
let btnFilters = locate('mat-icon').withText('filter_list');
let btnGoogle = locate('mat-icon').withText('search');
let searchArea = locate('div').withAttr({ class: 'search-filter-container' });

//Home
let carousel = locate('section').withAttr({ class: 'carousel-wrapper' });
let lastPosts = locate('h3').withText('Les dernières annonces publiées');

// Chat
let headerChat = locate('div').withAttr({ class: 'header' });
let listMsg = locate('mat-selection-list');

// Profil

let parent = locate('div').withAttr({ class: 'profil-container' });
let profilInfos = locate('div').withAttr({ class: 'pofil-infos' }).inside(parent);
let profilInfosHeader = locate('div').withAttr({ class: 'profil-infos_header' }).inside(parent);
let profilInfosDetails = locate('div').withAttr({ class: 'profil-infos_details' }).inside(parent);
let profilInfosEdit = locate('div').withAttr({ class: 'profil-infos_edit' }).inside(parent);
let btnList = locate('mat-selection-list');
let btnLogOut = locate('div').withAttr({ class: 'logout' });

//SelledItem
let selledItemLabel = locate('div').withText('Achat validé');
let keepBuyingSpan = locate('span').withText('Continuer mes achats');

//ItemList
let productList = locate('app-product-list');
let postItems = locate('div').withAttr({
    class: 'product',
});
let imgPostItem = locate('img').withAttr({
    alt: 'product image',
});
let brandPostItem = locate('p').withAttr({
    class: 'product-card_details_left_brand',
});
let sizePostItem = locate('p').withAttr({
    class: 'product-card_details_left_size',
});
let pricePostItem = locate('p').withAttr({
    class: 'product-card_details_left_price',
});
let firstPost = locate('div').withAttr({
    class: 'product-card',
});

// PostDetails
let buyButton = locate('button').withChild(locate('span').withText('Acheter'));
let carrouselImgs = locate('div').withAttr({
    class: 'carousel-item',
});
let priceDetails = locate('div').withAttr({
    class: 'product-details_price',
});
let titleDetails = locate('h1').withAttr({
    class: 'product-details_title',
});

//CreationPage
let btnAttachFile = locate('div').withAttr({
    class: 'images-uploader_input',
});
btnAttachFile = locate('input')
    .withAttr({
        alt: 'add images',
    })
    .inside(locate('div'))
    .inside(btnAttachFile);
let btnSell = locate('span').withText('Vendre mon article');

module.exports = {
    // Page by page
    Common: {
        popup: popup,
        bottomMenu: bottomMenu,
        topMenu: topMenu,
    },
    Add: {
        btnAddImages: btnAddImages,
    },
    SelledItem: {
        selledItemLabel: selledItemLabel,
        keepBuyingSpan: keepBuyingSpan,
    },
    ItemsList: {
        productList: productList,
        postItems: postItems,
        imgPostItem: imgPostItem,
        brandPostItem: brandPostItem,
        sizePostItem: sizePostItem,
        pricePostItem: pricePostItem,
        firstPost: firstPost,
    },
    PostDetails: {
        buyButton: buyButton,
        carrouselImgs: carrouselImgs,
        priceDetails: priceDetails,
        titleDetails: titleDetails,
    },
    CreationPage: {
        btnAttachFile: btnAttachFile,
        btnSell: btnSell,
    },
    Search: {
        btnFilters: btnFilters,
        btnGoogle: btnGoogle,
        searchArea: searchArea,
    },
    Home: {
        lastPosts: lastPosts,
        carousel: carousel,
    },
    Chat: {
        headerChat: headerChat,
        listMsg: listMsg,
    },
    Profil: {
        profilInfos: profilInfos,
        profilInfosHeader: profilInfosHeader,
        profilInfosDetails: profilInfosDetails,
        profilInfosEdit: profilInfosEdit,
        btnList: btnList,
        btnLogOut: btnLogOut,
    },
};
