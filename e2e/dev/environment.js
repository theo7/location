/* --- properties --- */

module.exports = {
    cb: {
        cb1: {
            serial: '5017670000006700',
            year: '2025',
            month: '05',
            cvv: '000',
        },
    },
    users: {
        user1: {
            email: 'dimitri.masschelein@gmail.com',
            password: 'Exploit1',
            pseudo: 'Mike',
            id: '9',
        },
        user2: {
            email: 'dimitri.masschelein@kiabi.com',
            password: 'Exploit1',
            pseudo: 'Jojoo',
            id: '10',
        },
    },
    urls: {
        staging: 'https://staging.second-hand.aws.kiabi.pro/',
    },
    postGre: {
        local: {
            url: 'postgresql://postgres:Exploit01$@127.0.0.1:5656/seconde_main',
        },
        gitlab: {
            url: 'postgresql://postgres:Exploit01$@secondhand-postgres-staging.cw72fjzewk14.eu-west-1.rds.amazonaws.com/seconde_main',
        },
    },
    apiBack: {
        mondialRelay: {
            url: 'https://api-staging.second-hand.aws.kiabi.pro/batch/lots/',
        },
        createProduct: {
            url: 'https://api-staging.second-hand.aws.kiabi.pro/api/products',
        },
        createPicture: {
            url: 'https://api-staging.second-hand.aws.kiabi.pro/api/pictures',
        },
    },
    config: {
        firebase: {
            apiKey: 'AIzaSyCNXXPm5S5jm2s8R9RP4iGo2wGgBX5ReIo',
            authDomain: 'secondhand-recette.firebaseapp.com',
            databaseURL: 'https://secondhand-recette-chat.europe-west1.firebasedatabase.app/',
            projectId: 'secondhand-recette',
            storageBucket: 'secondhand-recette.appspot.com',
            messagingSenderId: '549584416003',
            appId: '1:549584416003:web:0055634fb6ed0cad3e8d36',
            measurementId: 'G-BD57BEH03K',
            vapidKey: 'BAKhfhG12bvR_XlAzwmQ_MyrlpP4fjCayuMBAHlbaAsCdeAcvxKcHaK9xftLql44H9-gizEitE0YXv45vsPMq5Q',
        },
    },
    reportBaseUrl: {
        local: {
            url: 'D:/Pjts/location/e2e/output/',
        },
        gitlab: {
            url: 'https://secondhand-e2e-reports.s3-eu-west-1.amazonaws.com/',
        },
    },
    testomat: {
        local: {
            apiKey: '',
        },
        gitlab: {
            apiKey: 'a66e4cb8a4wk',
        },
    },
    reporting: {
        local: {
            reportUrl: 'output/' + process.env.BEGIN_TEST_SUITE + '/' + process.env.DEVICE,
        },
        gitlab: {
            reportUrl: 'e2e/output/' + process.env.BEGIN_TEST_SUITE + '/' + process.env.DEVICE,
        },
    },
    vars: {
        device: process.env.DEVICE,
        source: process.env.SOURCE,
        browser: process.env.BROWSER,
    },
};
