const { I } = inject();

let fieldPassword = locate('input').withAttr({ 'data-qa': 'password' });
let fieldEmail = locate('input').withAttr({ 'data-qa': 'email' });
let fieldEmailOld = locate({ css: '[data-qa=email]' });
let fieldPasswordOld = locate({ css: '[data-qa=password]' });
let btnLogin = locate({ css: '[data-qa=btnLogin]' });

module.exports = {
    fieldEmail: fieldEmail,
    fieldPassword: fieldPassword,
    btnLogin: btnLogin,
};
