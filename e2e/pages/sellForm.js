const { I } = inject();
// let btnAttachFile = locate('input').withAttr({
//     'data-qa': 'btnAddImages',
// });
let imageUploaderCmpnt = locate('app-image-uploader');
// let btnAttachFile = locate({ css: '[data-qa=btnAddImages]' });
let btnAttachFile = locate('input').withAttr({ 'data-qa': 'btnAddImages' });

module.exports = {
    btnAttachFile: btnAttachFile,
    imageUploaderCmpnt: imageUploaderCmpnt,
    checkIntegrity() {
        I.seeElement(locate({ css: '[data-qa=title]' }));
        I.seeElement(locate({ css: '[data-qa=description]' }));
        I.seeElement(locate({ css: '[data-qa=btnAddImages]' }));
        I.seeElement(locate({ css: '[data-qa=category]' }));
        I.seeElement(locate({ css: '[data-qa=subCategory]' }));
        I.seeElement(locate({ css: '[data-qa=productType]' }));
        I.seeElement(locate({ css: '[data-qa=size]' }));
        I.seeElement(locate({ css: '[data-qa=color]' }));
        I.seeElement(locate({ css: '[data-qa=brand]' }));
        I.seeElement(locate({ css: '[data-qa=condition]' }));
        I.seeElement(locate({ css: '[data-qa=price]' }));
        I.seeElement(locate({ css: '[data-qa=condition]' }));
        I.seeElement(locate({ css: '[data-qa=condition]' }));
        I.seeElement(locate({ css: '[data-qa=btnSubmit]' }));
        // Missing bonus-price
    },
};
