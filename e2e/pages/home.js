const { I } = inject();

let carousel = locate('section').withAttr({ class: 'carousel-wrapper' });
let lastPosts = locate('h3').withText('Les dernières annonces publiées');
let post = locate('app-product-card').withAttr({ 'data-qa': 'productCard' });
let postOld = locate({ css: '[data-qa=productCard]' });
let productList = locate('app-product-list');
let postItems = locate('div').withAttr({
    class: 'product',
});
let imgPostItem = locate('img').withAttr({
    alt: 'product image',
});
let brandPostItem = locate({ css: '[data-qa=productBrand]' });
let sizePostItem = locate({ css: '[data-qa=productSize]' });
let pricePostItem = locate({ css: '[data-qa=productPrice]' });

module.exports = {
    carousel: carousel,
    lastPosts: lastPosts,
    post: post,
    productList: productList,
    postItems: postItems,
    imgPostItem: imgPostItem,
    brandPostItem: brandPostItem,
    sizePostItem: sizePostItem,
    pricePostItem: pricePostItem,
};
