const { I } = inject();

let breadcrumb = locate('div').withAttr({ class: 'payment-stepper' });
let bonusKiabi = locate('div').withAttr({ class: 'incentive' });
let item = locate('div').withAttr({ class: 'product-line' });
let paymentPart = locate('div').withAttr({ class: 'payment-lines' });
let itemsPriceTotal = locate({ css: '[data-qa=strikedPrice' });
let leftToPay = locate({ css: '[data-qa=leftToPay]' });
let shippingFees = locate({ css: '[data-qa=shippingFees]' });
let handlingFees = locate({ css: '[data-qa=handlingFees]' });
let totalToPay = locate({ css: '[data-qa=totalToPay]' });
let btnBuy = locate({ css: '[data-qa=btnBuy]' });
let btnProceedPayment = locate({ css: '[data-qa=btnProceedPayment]' });
let serialField = '#cardNumberField';
let monthSelectField = '#expirydatefield-month';
let yearSelectField = '#expirydatefield-year';
let cvvField = '#cvvfield';
let btnFormSubmit = '#form_submit';
let fields = locate('fieldset');

module.exports = {
    breadcrumb: breadcrumb,
    bonusKiabi: bonusKiabi,
    item: item,
    paymentPart: paymentPart,
    itemsPriceTotal: itemsPriceTotal,
    leftToPay: leftToPay,
    shippingFees: shippingFees,
    handlingFees: handlingFees,
    totalToPay: totalToPay,
    btnBuy: btnBuy,
    btnProceedPayment: btnProceedPayment,
    serialField: serialField,
    monthSelectField: monthSelectField,
    yearSelectField: yearSelectField,
    cvvField: cvvField,
    btnFormSubmit: btnFormSubmit,
    fields: fields,
};
