const { I } = inject();

let buyButton = locate('button').withChild(locate('span').withText('Acheter'));
let carrouselImgs = locate('div').withAttr({
    class: 'picture',
});
let carrouselImgsMobile = locate('div').withAttr({
    class: 'carousel-item',
});
let priceDetails = locate('div').withAttr({
    class: 'product-price',
});
let titleDetails = locate('div').withAttr({
    class: 'label',
});

let selledItemLabel = locate('div').withText('Achat validé');
let keepBuyingSpan = locate('span').withText('Continuer mes achats');
let productTitle = locate({ css: '[data-qa=productTitle]' });
let productPrice = locate({ css: '[data-qa=productPrice]' });
module.exports = {
    buyButton: buyButton,
    carrouselImgs: carrouselImgs,
    carrouselImgsMobile: carrouselImgsMobile,
    priceDetails: priceDetails,
    titleDetails: titleDetails,
    selledItemLabel: selledItemLabel,
    keepBuyingSpan: keepBuyingSpan,
    productTitle: productTitle,
    productPrice: productPrice,
};
