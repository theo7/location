const { I } = inject();

let parent = locate('div').withAttr({ class: 'profil-container' });
let profilInfos = locate('div').withAttr({ class: 'pofil-infos' }).inside(parent);
let profilInfosHeader = locate('div').withAttr({ class: 'profil-infos_header' }).inside(parent);
let profilInfosDetails = locate('div').withAttr({ class: 'profil-infos_details' }).inside(parent);
let profilInfosEdit = locate('div').withAttr({ class: 'profil-infos_edit' }).inside(parent);
let btnList = locate('mat-selection-list');
let btnLogOut = locate({ css: '[data-qa=btnLogout]' });
let myPurchasesSales = locate({ css: '[data-qa=myPurchasesSales]' });
let myJackpots = locate({ css: '[data-qa=myJackpots]' });
let voucherHoldingCredit = locate({ css: '[data-qa=voucherHoldingCredit]' });
let voucherCreditAmount = locate({ css: '[data-qa=voucherCreditAmount]' });

module.exports = {
    profilInfos: profilInfos,
    profilInfosHeader: profilInfosHeader,
    profilInfosDetails: profilInfosDetails,
    profilInfosEdit: profilInfosEdit,
    btnList: btnList,
    btnLogOut: btnLogOut,
    myPurchasesSales: myPurchasesSales,
    myJackpots: myJackpots,
    voucherHoldingCredit: voucherHoldingCredit,
    voucherCreditAmount: voucherCreditAmount,
};
