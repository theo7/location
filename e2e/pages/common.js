const { I } = inject();

let popup = locate('mat-dialog-container');
let bottomMenu = locate('app-bottom-navigation');
let topMenu = locate('app-top-navigation');
let btnFilters = locate('mat-icon').withText('filter_list');
let btnGoogle = locate('mat-icon').withText('search');
let searchArea = locate('div').withAttr({ class: 'search-filter-container' });
let desktopMenu = locate('app-navigation-desktop');
let Post = {};
let Bundle = {};
let goSearch = locate('a').inside(locate('li').withAttr({ 'data-qa': 'goSearch' }));
let goSearchOld = locate({ css: '[data-qa=goSearch]' });
let goSell = locate('a').inside(locate('li').withAttr({ 'data-qa': 'goSell' }));
let goSellOld = locate({ css: '[data-qa=goSell]' });
let goHome = locate('a').inside(locate('li').withAttr({ 'data-qa': 'goHome' }));
let goHomeOld = locate({ css: '[data-qa=goHome]' });
let goProfilFirst = locate('a').withAttr({ 'data-qa': 'goProfil' });
let goProfil = locate('a').inside(locate('li').withAttr({ 'data-qa': 'goProfil' }));
let goProfilOld = locate({ css: '[data-qa=goProfil]' });
let goChat = locate('a').inside(locate('li').withAttr({ 'data-qa': 'goChat' }));
let btnValidate = locate({ css: '[data-qa=btnValidate]' });
let btnConfirm = locate({ css: '[data-qa=btnConfirm]' });
let btnContinue = locate({ css: '[data-qa=btnContinue]' });
let btnSubmit = locate({ css: '[data-qa=btnSubmit]' });
let btnSubmitSearch = locate({ css: '[data-qa=btnSubmitSearch]' });
let searchInput = locate('input').withAttr({ 'data-qa': 'searchInput' });
let searchInputBrand = locate('input').withAttr({ placeholder: 'Rechercher une marque' });
let logoSecondhand = locate({ css: '[data-qa=logoSecondHand]' });

module.exports = {
    feedProductInfos(key, value) {
        Post[key] = value;
        console.log('--- Post --- ');
        console.log(Post);
        return Post;
    },
    feedBundleInfos(key, value) {
        value = value.replace(',', '.');
        value = value.replace(/[^\d.-]/g, '');
        value = parseFloat(value).toFixed(2);
        if (key == 'itemsPriceTotal') {
            let amountValue = parseFloat(value * 0.2).toFixed(2);
            Bundle['amountVoucherExpectedBuyer'] = amountValue;
        }
        Bundle[key] = value;
        console.log('--- Bundle --- ');
        console.log(Bundle);
        return Bundle;
    },
    seeProductInfos(elmt) {
        if (elmt == 'price') {
            Post[elmt] = Post[elmt].replace('.', ',');
        }
        console.log(Post);
        I.see(Post[elmt]);
        return Post[elmt];
    },
    seeBundleInfos(elmt) {
        if (elmt == 'amountVoucherExpectedBuyer') {
            if (Bundle[elmt] > 15) {
                while (Bundle[elmt] < 15) {
                    Bundle[elmt] -= 15;
                    console.log(Bundle[elmt]);
                }
            }
        }
        Bundle[elmt] = Bundle[elmt].replace('.', ',');
        console.log(Bundle);
        I.see(Bundle[elmt]);
        return Bundle[elmt];
    },
    getProductInfos(elmt) {
        return Post[elmt];
    },
    getBundleInfos(elmt) {
        return Bundle[elmt];
    },
    checkIntegrity() {
        I.seeElement(goChat);
        I.seeElement(goHome);
        I.seeElement(goProfil);
        I.seeElement(goSell);
        I.runOnIOS(() => {
            I.seeElement(goSearch);
        });
        I.runOnAndroid(() => {
            I.seeElement(goSearch);
        });
    },

    Post: Post,
    Bundle: Bundle,
    popup: popup,
    bottomMenu: bottomMenu,
    topMenu: topMenu,
    btnFilters: btnFilters,
    btnGoogle: btnGoogle,
    searchArea: searchArea,
    desktopMenu: desktopMenu,
    goSearch: goSearch,
    goSell: goSell,
    goHome: goHome,
    goProfil: goProfil,
    goChat: goChat,
    goProfilFirst: goProfilFirst,
    btnValidate: btnValidate,
    btnConfirm: btnConfirm,
    btnContinue: btnContinue,
    btnSubmit: btnSubmit,
    searchInput: searchInput,
    searchInputBrand: searchInputBrand,
    logoSecondhand: logoSecondhand,
    btnSubmitSearch: btnSubmitSearch,
};
