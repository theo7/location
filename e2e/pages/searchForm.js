const { I } = inject();

let btnFilters = locate('mat-icon').withText('filter_list');
let btnGoogle = locate('mat-icon').withText('search');
let searchArea = locate('div').withAttr({ class: 'search-filter-container' });
let filterList = locate('span').withText('filter_list');
let productBrandField = locate('mat-select').withAttr({ 'data-qa': 'productBrands' });
let sizes = locate({ css: '[data-qa=goToTabFilterSize]' });
let categories = locate({ css: '[data-qa=goToTabFilterCategory]' });
let brands = locate({ css: '[data-qa=goToTabFilterBrand]' });
let colors = locate({ css: '[data-qa=goToTabFilterColor]' });
let conditions = locate({ css: '[data-qa=goToTabFilterCondition]' });
let validateSection = locate('span').withText('Valider');

module.exports = {
    btnFilters: btnFilters,
    btnGoogle: btnGoogle,
    searchArea: searchArea,
    filterList: filterList,
    productBrandField: productBrandField,
    categories: categories,
    sizes: sizes,
    brands: brands,
    colors: colors,
    conditions: conditions,
    validateSection: validateSection,
};
