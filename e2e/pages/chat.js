const { I } = inject();

let headerChat = locate('div').withAttr({ class: 'header' });
let listMsg = locate('mat-selection-list');
let chatPrice = locate({ css: '[data-qa=productPrice]' });
let lastPriceMsg = locate('span').withAttr({ class: 'message-highlight-blue' });
lastPriceMsg = lastPriceMsg.last();
let btnConfirmPackageCompliance = locate({ css: '[data-qa=btnConfirmPackageCompliance]' });
let radioConfirm = locate('input').inside(locate('mat-radio-button').withAttr({ 'data-qa': 'radioConfirm' }));
let radioConfirmOld = locate({ css: '[data-qa=radioConfirm]' });
let radioRefuse = locate('input').inside(locate('mat-radio-button').withAttr({ 'data-qa': 'radioRefuse' }));
let radioRefuseOld = locate({ css: '[data-qa=radioRefuse]' });
let btnValidate = locate({ css: '[data-qa=btnValidate]' }); // to common
let btnReturn = locate({ css: '[data-qa=btnReturn]' });
let btnGoChat = locate({ css: '[data-qa=goChat]' });
let btnMakeOffer = locate({ css: '[data-qa=btnMakeOffer]' });
let fieldWriteMessage = locate({ css: '[data-qa=writeMessage]' });
let btnSendMessage = locate({ css: '[data-qa=btnSendMessage]' });
let btnSendOffer = locate({ css: '[data-qa=btnSendOffer]' });
let btnCounterOffer = locate({ css: '[data-qa=btnCounterOffer]' });
let reserveProduct = locate({ css: '[data-qa=reserveProduct]' });
let openApproveSellDialog = locate({ css: '[data-qa=openApproveSellDialog]' });
let btnConfirmRecep = locate({ css: '[data-qa=btnConfirmRecep]' });
let bundlePrice = locate({ css: '[data-qa=price]' });
let radioVoucher = locate('input').inside(locate('mat-radio-button').withAttr({ 'data-qa': 'radioVoucher' }));
let radioVoucherAndroid = locate({ css: '[data-qa=radioVoucher]' });
let btnConfirm = locate({ css: '[data-qa=btnConfirm]' }); // to common
let btnHeaderReturn = locate({ css: '[data-qa=btnHeaderReturn]' });
let btnAcceptOffer = locate({ css: '[data-qa=btnAcceptOffer]' });
let writeAmount = locate('input').withAttr({ 'data-qa': 'writeAmount' });
let writeAmountDesktop = locate('input').withAttr({ 'data-placeholder': 'Montant' });
let lastMsg = locate({ css: '[data-qa=msgContent]' }).last();

module.exports = {
    headerChat: headerChat,
    listMsg: listMsg,
    chatPrice: chatPrice,
    lastPriceMsg: lastPriceMsg,
    btnConfirmPackageCompliance: btnConfirmPackageCompliance,
    radioConfirm: radioConfirm,
    radioRefuse: radioRefuse,
    btnValidate: btnValidate,
    btnReturn: btnReturn,
    btnGoChat: btnGoChat,
    btnMakeOffer: btnMakeOffer,
    fieldWriteMessage: fieldWriteMessage,
    btnSendMessage: btnSendMessage,
    btnSendOffer: btnSendOffer,
    btnCounterOffer: btnCounterOffer,
    reserveProduct: reserveProduct,
    openApproveSellDialog: openApproveSellDialog,
    btnConfirmRecep: btnConfirmRecep,
    bundlePrice: bundlePrice,
    radioVoucher: radioVoucher,
    btnConfirm: btnConfirm,
    btnHeaderReturn: btnHeaderReturn,
    btnAcceptOffer: btnAcceptOffer,
    radioVoucherAndroid: radioVoucherAndroid,
    writeAmount: writeAmount,
    writeAmountDesktop: writeAmountDesktop,
    lastMsg: lastMsg,
};
