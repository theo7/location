// in this file you can append custom step methods to 'I' object

const { I, commonPage } = inject();

module.exports = function () {
    return actor({
        waitSpinnerHides() {
            let spinner = locate('circle');
            I.waitForInvisible(spinner, 10);
        },

        visualCheck(img, tolIndex) {
            I.wait(5);
            I.saveScreenshot(img);
            I.seeVisualDiff(img, { tolerance: tolIndex, prepareBaseImage: false });
            if (img == 'chatPackagePrepaid') {
                let isCounterOffer = commonPage.getBundleInfos('counterOffer');
                if (isCounterOffer == true) {
                    I.seeVisualDiff(img, { tolerance: tolIndex, prepareBaseImage: false });
                } else {
                    I.seeVisualDiff('chatPackagePrepaidWithCounter', { tolerance: tolIndex, prepareBaseImage: false });
                }
            }
        },
    });
};
