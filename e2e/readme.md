# SecondHand

## Running end-to-end tests

-   See package.json to run E2E tests commands depending on the tested device
-   Before running locally make sure you saved these values: DEVICE=yourDevice, SOURCE=local; and make sure you have a SSH connetion enabled
-   Before running remotely (gitlab) make sure you saved these values: DEVICE=yourDevice, SOURCE=gitlab, BEGIN*TEST_SUITE=$(date "+%d%m%y*%H-%M")
-   To get help on CodeceptJS, visit https://codecept.io/
-   1 feature file = 1 E2E scenario
-   1 .conf file = 1 device configuration
-   Each locator has to be declared in `e2e/pages` folder and used in `e2e/step_definitions` folder
-   API Back is reached by `e2e/helpers/ApiConnector.js`, functions are used through `I` object
-   PostGre Database is by through `e2e/helpers/PostGreConnector.js`, functions are used through `I` object
-   Running 9 tests for 1 device on gitlab takes about 40 minutes
-   To see running history visit `https://app.testomat.io/projects/seconde-main-gitlab/runs` (each report title is a link to see mocha reporter)
