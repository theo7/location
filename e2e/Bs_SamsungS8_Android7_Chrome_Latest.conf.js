const env = require('./dev/environment');
require('ts-node/register');

const urlReportAttachments = 'output/' + process.env.BEGIN_TEST_SUITE + '/' + process.env.DEVICE;
const urlReport = env.reporting[process.env.SOURCE]['reportUrl'];
const apiKeyTestomat = env.testomat[process.env.SOURCE]['apiKey'];
var smartwaitTime = 20000;

var userName = 'manche2';
var accessKey = '7a2i8os5R4FzTottYBXk';
var browserstackURL = 'hub-cloud.browserstack.com';
var BScapabilities = {
    'goog:chromeOptions': {
        w3c: false,
    },
    'browserstack.user': userName,
    'browserstack.key': accessKey,
    acceptSslCert: true,
    os_version: '7.0',
    name: 'Android Seconde main',
    browserName: 'Chrome',
    browser_version: 'latest',
    'browserstack.console': 'errors',
    'browserstack.local': 'false',
    smartwait: smartwaitTime,
    acceptSslCert: true,
    'browserstack.geoLocation': 'FR',
    'browserstack.timezone': 'Paris',
    'browserstack.idleTimeout': 5,
    'browserstack.autoWait': 10,
    'browserstack.console': 'errors',
    'appium:chromeOptions': {
        w3c: false,
    },
    'bstack:options': {
        local: false,
        realMobile: 'true',
        browserName: 'chrome',
        idleTimeout: 300,
        networkLogs: 'true',
        consoleLogs: 'info',
    },
};

exports.config = {
    output: urlReportAttachments,
    helpers: {
        Appium: {
            host: browserstackURL,
            port: 4444,
            user: userName,
            key: accessKey,
            device: 'Samsung Galaxy S8',
            platform: 'Android',
            browser: 'Chrome',
            restart: true,
            timeouts: {
                script: 5000,
                'page load': 10000,
            },
            desiredCapabilities: BScapabilities,
        },
        PostgreConnector: {
            require: './helpers/PostgreConnector.js',
        },
        ApiConnector: {
            require: './helpers/ApiConnector.js',
        },
        Mochawesome: {
            uniqueScreenshotNames: 'true',
        },
        EnvManager: {
            require: './helpers/EnvManager.js',
        },
        OnFailure: {
            require: './helpers/OnFailure.js',
        },
        // ResembleHelper: {
        //     require: 'codeceptjs-resemblehelper',
        //     screenshotFolder: urlReport,
        //     baseFolder: './dev/visual/base',
        //     diffFolder: './dev/visual/diff',
        //     prepareBaseImage: false,
        //     skipFailure: true,
        // },
    },
    include: {
        I: './steps_file.js',
        commonPage: './pages/common.js',
        productPage: './pages/product.js',
        profilePage: './pages/profile.js',
        chatPage: './pages/chat.js',
        sellFormPage: './pages/sellForm.js',
        searchFormPage: './pages/searchForm.js',
        homePage: './pages/home.js',
        buyPage: './pages/buy.js',
        loginPage: './pages/login.js',
    },
    mocha: {
        reporter: 'mocha-multi',
        reporterOptions: {
            'codeceptjs-cli-reporter': {
                stdout: '-',
                options: {
                    verbose: true,
                    steps: true,
                },
            },
            mochawesome: {
                stdout: urlReport + '/console.log',
                options: {
                    reportDir: urlReport,
                    reportFilename: 'report',
                },
            },
        },
    },
    bootstrap: null,
    teardown: null,
    hooks: [],
    gherkin: {
        features: './features/*.feature',
        steps: [
            './step_definitions/common.js',
            './step_definitions/home.js',
            './step_definitions/product.js',
            './step_definitions/profile.js',
            './step_definitions/search.js',
            './step_definitions/login.js',
            './step_definitions/sell.js',
            './step_definitions/buy.js',
            './step_definitions/chat.js',
            './step_definitions/login.js',
        ],
    },
    plugins: {
        tryTo: {
            enabled: true,
        },
        customLocator: {
            enabled: true,
            attribute: 'data-qa',
        },
        screenshotOnFail: {
            enabled: true,
        },
        retryFailedStep: {
            enabled: true,
        },
        autoDelay: {
            enabled: true,
        },
        testomat: {
            enabled: false,
            require: '@testomatio/reporter/lib/adapter/codecept',
            apiKey: apiKeyTestomat,
        },
    },
    name: 'SecondHand S8 Chrome',
};
