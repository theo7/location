const { devices, firefox } = require('playwright');
var dimensions = '360x800';
var browserName = 'chrome';
var urlLocal = 'http://localhost';

var userName = 'manche2';
var accessKey = '7a2i8os5R4FzTottYBXk';
var browserstackURL = 'https://' + userName + ':' + accessKey + '@hub-cloud.browserstack.com/wd/hub';

exports.config = {
    output: './output',
    helpers: {
        // Playwright: {
        //     url: 'http://localhost',
        //     show: true,
        //     browser: 'webkit',
        // },
        Playwright: {
            url: 'http://localhost',
            smartWait: 10000,
            waitForNavigation: 'networkidle0',
            show: true,
            browser: 'chromium',
            chromium: {
                args: ['--no-sandbox' /*, "--proxy-server=http://gateway.zscaler.net:80"*/],
            },
        },
        PostgreConnector: {
            require: './helpers/PostgreConnector.js',
        },
        ApiConnector: {
            require: './helpers/ApiConnector.js',
        },
    },
    include: {
        I: './steps_file.js',
        commonPage: './pages/common.js',
        productPage: './pages/product.js',
        profilePage: './pages/profile.js',
        chatPage: './pages/chat.js',
        sellFormPage: './pages/sellForm.js',
        searchFormPage: './pages/searchForm.js',
        homePage: './pages/home.js',
    },
    bootstrap: null,
    teardown: null,
    hooks: [],
    gherkin: {
        features: './features/*.feature',
        steps: [
            './step_definitions/common.js',
            './step_definitions/home.js',
            './step_definitions/product.js',
            './step_definitions/profile.js',
            './step_definitions/search.js',
            './step_definitions/login.js',
            './step_definitions/sell.js',
            './step_definitions/buy.js',
            './step_definitions/chat.js',
            './step_definitions/login.js',
        ],
    },
    plugins: {
        tryTo: {
            enabled: true,
        },
        customLocator: {
            enabled: true,
            attribute: 'data-qa',
        },
        allure: {
            enabled: true,
        },
        screenshotOnFail: {
            enabled: true,
        },
        retryFailedStep: {
            enabled: true,
        },
        autoDelay: {
            enabled: true,
        },
        testomat: {
            enabled: true,
            require: '@testomatio/reporter/lib/adapter/codecept',
            apiKey: 'lz7de7e80xbj',
        },
    },
    name: 'SecondHand',
};
