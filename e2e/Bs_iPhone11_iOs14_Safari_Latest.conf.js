const env = require('./dev/environment');
// const fullUrlReport =
//     env.reportBaseUrl[env.vars.source]['url'] + process.env.BEGIN_TEST_SUITE + '/' + process.env.DEVICE;
// console.log('url ->>>>>>>>> ' + fullUrlReport);
const urlReport = 'output/' + process.env.BEGIN_TEST_SUITE + '/' + process.env.DEVICE;
var smartwaitTime = 5000;
var userName = 'manche2';
var accessKey = '7a2i8os5R4FzTottYBXk';
var browserstackURL = 'hub-cloud.browserstack.com';
var BScapabilities = {
    smartwait: smartwaitTime,
    local: false,
    device: 'iPhone 11',
    real_mobile: 'true',
    os_version: '14',
    name: 'IOS Seconde main',
    os: 'ios',
    'browserstack.user': userName,
    'browserstack.key': accessKey,
    browserName: 'iPhone',
    acceptSslCert: true,
    'browserstack.geoLocation': 'FR',
    'browserstack.timezone': 'Paris',
    'browserstack.idleTimeout': 5,
    'browserstack.autoWait': 10,
    'browserstack.console': 'errors',
    'appium:chromeOptions': {
        w3c: false,
    },
    'bstack:options': {
        local: false,
        realMobile: 'true',
        browserName: 'chrome',
        idleTimeout: 300,
        networkLogs: 'true',
        consoleLogs: 'info',
    },
};

exports.config = {
    output: urlReport,
    helpers: {
        Appium: {
            host: browserstackURL,
            port: 4444,
            user: userName,
            key: accessKey,
            device: 'iPhone X',
            platform: 'Ios',
            browser: 'Safari',
            restart: true,
            desiredCapabilities: BScapabilities,
        },
        PostgreConnector: {
            require: './helpers/PostgreConnector.js',
        },
        ApiConnector: {
            require: './helpers/ApiConnector.js',
        },
        Mochawesome: {
            uniqueScreenshotNames: 'true',
        },
        EnvManager: {
            require: './helpers/EnvManager.js',
        },
    },
    include: {
        I: './steps_file.js',
        commonPage: './pages/common.js',
        productPage: './pages/product.js',
        profilePage: './pages/profile.js',
        chatPage: './pages/chat.js',
        sellFormPage: './pages/sellForm.js',
        searchFormPage: './pages/searchForm.js',
        homePage: './pages/home.js',
        buyPage: './pages/buy.js',
        loginPage: './pages/login.js',
    },
    mocha: {
        reporter: 'mocha-multi',
        reporterOptions: {
            'codeceptjs-cli-reporter': {
                stdout: '-',
                options: {
                    verbose: true,
                    steps: true,
                },
            },
            mochawesome: {
                //stdout: 'e2e/' + urlReport + '/console.log',
                stdout: urlReport + '/console.log',
                options: {
                    //reportDir: 'e2e/' + urlReport,
                    reportDir: urlReport,
                    reportFilename: 'report',
                },
            },
        },
    },

    bootstrap: null,
    teardown: null,
    hooks: [],
    gherkin: {
        features: './features/*.feature',
        steps: [
            './step_definitions/common.js',
            './step_definitions/home.js',
            './step_definitions/product.js',
            './step_definitions/profile.js',
            './step_definitions/search.js',
            './step_definitions/login.js',
            './step_definitions/sell.js',
            './step_definitions/buy.js',
            './step_definitions/chat.js',
            './step_definitions/login.js',
        ],
    },
    plugins: {
        tryTo: {
            enabled: true,
        },
        customLocator: {
            enabled: true,
            attribute: 'data-qa',
        },
        screenshotOnFail: {
            enabled: true,
        },
        retryFailedStep: {
            enabled: true,
        },
        autoDelay: {
            enabled: true,
        },
        testomat: {
            enabled: true,
            require: '@testomatio/reporter/lib/adapter/codecept',
            apiKey: 'lz7de7e80xbj',
        },
    },
    name: 'SecondHand iPhone11 Safari',
};
