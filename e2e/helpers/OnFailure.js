/*jshint esversion: 8 */
const Helper = require('@codeceptjs/helper');
class OnFailure extends Helper {
    _init() {}
    /**
     * @protected
     */
    _failed() {}

    _passed() {}

    // add custom methods here
    // If you need to access other helpers
    // use: this.helpers['helperName']
}

module.exports = OnFailure;
