const Helper = require('@codeceptjs/helper');
const env = require('../dev/environment');

class EnvManager extends Helper {
    _init() {
        this.getEnvVariables();
        // this.setOutputUrlReport();
    }

    _before() {
        // const mocha = this.helpers.Mochawesome;
        // mocha.addMochawesomeContext('your context info here');
    }

    getEnvVariables() {
        env.vars.source = process.env.SOURCE;
        env.vars.device = process.env.DEVICE;
    }

    setOutputUrlReport() {
        let fullUrlReport =
            env.reportBaseUrl[env.vars.source].url + '/' + process.env.BEGIN_TEST_SUITE + '/' + process.env.DEVICE;
        let urlReport = process.env.BEGIN_TEST_SUITE + '/' + process.env.DEVICE;
        process.env.FULL_URL_REPORT = fullUrlReport;
        process.env.URL_REPORT = urlReport;
    }
}

module.exports = EnvManager;
