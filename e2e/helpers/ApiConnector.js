const Helper = require('@codeceptjs/helper');
const env = require('../dev/environment');
const firebase = require('firebase');
var FormData = require('form-data');
var fs = require('fs');
var Body = require('../dev/body.json');
const axios = require('axios');

var FireBaseToken;
var UploadedImg;
var UploadedItem;

class ApiConnector extends Helper {
    async getFirebaseToken(mail, pwd) {
        let fbToken;
        try {
            firebase.initializeApp(env.config.firebase);
            await firebase.auth().signInWithEmailAndPassword(mail, pwd);
            fbToken = firebase.auth().currentUser.getIdToken();
            // console.log(fbToken);
        } catch (error) {
            console.error('error getting Firebase token !');
            console.error('error: ', error);
        }
        return fbToken;
    }

    async postSent(id) {
        var body = JSON.stringify('SENT');
        var requestUrl = env.apiBack.mondialRelay.url + id;

        try {
            var res = await axios.post(requestUrl, body, {
                headers: {
                    Authorization: 'Basic c2Vjb25kaGFuZC1iYXRjaDozeHAxMGlUNDN2M3I=',
                    'Content-Type': 'application/json',
                },
            });
        } catch (err) {
            console.error(err);
        }
        return res.status;
    }

    async postInRelais(id) {
        var body = JSON.stringify('IN_RELAIS');
        var requestUrl = env.apiBack.mondialRelay.url + id;

        try {
            var res = await axios.post(requestUrl, body, {
                headers: {
                    Authorization: 'Basic c2Vjb25kaGFuZC1iYXRjaDozeHAxMGlUNDN2M3I=',
                    'Content-Type': 'application/json',
                },
            });
        } catch (err) {
            console.error(err);
        }
        return res.status;
    }

    async postNewItem() {
        var requestUrl = env.apiBack.createProduct.url;
        Body.pictures.push(UploadedImg);
        const json = JSON.stringify(Body);
        try {
            var res = await axios.post(requestUrl, json, {
                headers: {
                    'X-Auth-Token': FireBaseToken,
                    'Content-Type': 'application/json',
                },
            });
        } catch (err) {
            console.error(err);
        }
        return res.data;
    }

    async postNewImg() {
        var fileToUpload = '../e2e/dev/images/img1.jpg';
        var requestUrl = env.apiBack.createPicture.url;
        var formData = new FormData();
        formData.append('file', fs.createReadStream(fileToUpload), 'img1.jpg');
        try {
            var res = await axios.post(requestUrl, formData, {
                headers: {
                    'X-Auth-Token': FireBaseToken,
                    'Content-Type': 'multipart/form-data; boundary=' + formData.getBoundary() + '; charset=UTF-8',
                },
            });
        } catch (err) {
            console.error(err);
        }
        return res.data;
    }

    async createNewProduct() {
        try {
            FireBaseToken = await this.getFirebaseToken(env.users.user1.email, env.users.user1.password);
            UploadedImg = await this.postNewImg();
            if (UploadedImg) {
                try {
                    UploadedItem = await this.postNewItem();
                } catch (err) {
                    console.error(err);
                }
            } else {
                console.log("L'image n'a pas pu être uploadée");
            }
        } catch (err) {
            console.error(err);
        }
    }
}

module.exports = ApiConnector;
