const Helper = require('@codeceptjs/helper');
const { Client } = require('pg');
const env = require('../dev/environment');
let connectionStringSchema = env.postGre[env.vars.source].url;

class PostgreConnector extends Helper {
    _init() {
        this.updateStoreCredit(env.users['user1']['id'], 0);
        this.updateStoreCredit(env.users['user2']['id'], 0);
        this.cleanUserData(env.users['user1']['id']);
        this.cleanUserData(env.users['user2']['id']);
        this.cleanVouchers(env.users['user1']['id']);
        this.cleanVouchers(env.users['user2']['id']);
    }

    async grabLotId(id) {
        var result = null;
        let connectionString = connectionStringSchema;
        const client = new Client({
            connectionString: connectionString,
        });
        client.connect();
        let request = 'SELECT lot_id FROM public.discussion WHERE id=' + id + ';';

        try {
            const res = await client.query(request);
            //console.log(res.rows);
            result = res.rows[0];
        } catch (err) {
            console.log(err.stack);
        }
        await client.end();
        return result;
    }

    async cleanUserData(idUser) {
        var result = null;
        let connectionString = connectionStringSchema;
        const client = new Client({
            connectionString: connectionString,
        });
        client.connect();
        let request = 'SELECT id FROM public.lot WHERE buyer_id=' + idUser + ';';

        try {
            const res = await client.query(request);
            //console.log(res.rows);
            result = res.rows;
            if (result.length > 0) {
                for (let i = 0; i < result.length; i++) {
                    console.log(result[i].id);
                    let deleteOffers =
                        'DELETE FROM offer WHERE (discussion_id IN (SELECT id FROM discussion WHERE lot_id = ' +
                        result[i].id +
                        '));';
                    let deleteDiscussions = 'DELETE FROM discussion WHERE lot_id =' + result[i].id;
                    let deleteLotPaymentLines = 'DELETE FROM lot_payment_lines WHERE lot_id =' + result[i].id;
                    let deleteLotProducts = 'DELETE FROM lot_products WHERE lot_id =' + result[i].id;
                    let deleteMatchingContribution = 'DELETE FROM matching_contribution WHERE lot_id =' + result[i].id;
                    let deleteLot = 'DELETE FROM lot WHERE id =' + result[i].id;
                    let deleteVouchers = 'DELETE FROM voucher WHERE user_id=' + idUser + ';';
                    await client.query(deleteOffers);
                    await client.query(deleteDiscussions);
                    await client.query(deleteLotPaymentLines);
                    await client.query(deleteLotProducts);
                    await client.query(deleteMatchingContribution);
                    await client.query(deleteLot);
                    await client.query(deleteVouchers);
                    await client.query('COMMIT');
                    console.log('  -- SQL -- Postgre data cleaning ---> Clean completed');
                }
            } else {
                console.log('  -- SQL --  Postgre data cleaning ---> Nothing to clean');
            }
        } catch (err) {
            console.log(err.stack);
        }
        await client.end();
        return result;
    }

    async updateStoreCredit(idUser) {
        var result = null;
        let connectionString = connectionStringSchema;
        const client = new Client({
            connectionString: connectionString,
        });
        client.connect();
        let request = 'Update store_credit set voucher_amount=0 where user_id =' + idUser + ';';

        try {
            const res = await client.query(request);
            //console.log(res.rows);
            result = res.rows;
        } catch (err) {
            console.log(err.stack);
        }
        await client.end();
        return result;
    }

    async cleanVouchers(idUser) {
        var result = null;
        let connectionString = connectionStringSchema;
        const client = new Client({
            connectionString: connectionString,
        });
        client.connect();
        let request = 'DELETE FROM voucher WHERE user_id=' + idUser + ';';

        try {
            const res = await client.query(request);
            //console.log(res.rows);
            result = res.rows;
        } catch (err) {
            console.log(err.stack);
        }
        await client.end();
        return result;
    }
}

module.exports = PostgreConnector;
