# language: fr
@iPhone11_Os14_SafariLatest
@samsungS8_Os7_Chromelatest
@windows10_ChromeLatest

Fonctionnalité: Parcours Produit /acheteur
Pour acheter un produit
En tant qu'acheteur
Je veux rechercher un produit

Scénario: Rechercher un produit
Etant donné que je me connecte à l'environnement
Et que j'accepte les cookies

Et que je me connecte avec le "user2"
Et que je clean la db pour les deux utilisateurs
Quand je me rend sur la recherche
Alors j'ouvre la fenêtre des filtres

Et je veux préciser la section "catégories"
Et je choisis pour la section "catégories" l'item "girl"
Et je choisis pour la section "sous-catégories" l'item "accessories"
Et je choisis pour la section "types de produit" l'item "Echarpe, gants, bonnet"
Alors je valide cette section

Et je veux préciser la section "tailles"
Et je choisis pour la section "tailles" l'item "10 ans, 138/143cm"
Alors je valide cette section

Et je veux préciser la section "marques"
Et je tape dans le champ de recherche "Element"
Alors je choisis pour la section "marques" l'item "Element"
Alors je valide cette section

Et je veux préciser la section "couleurs"
Alors je choisis pour la section "couleurs" l'item "Kaki"
Alors je valide cette section

Et je veux préciser la section "conditions"
Alors je choisis pour la section "conditions" l'item "Très bon état (aucun défaut)"
Alors je valide cette section

Et je lance la recherche
Et je clique sur l'annonce
