# language: fr
@samsungS8_Os7_Chromelatest

Fonctionnalité: Vérifier la génération auto d'un BA à partir de 15€
Pour acheter un produit
En tant qu'acheteur et vendeur
Je veux que le BA se génère automatiquement

Scénario: Acheter un produit /acheteur 
Etant donné que je me connecte à l'environnement
Et que j'accepte les cookies
Et que je prépare une annonce
Et que je me connecte avec le "user2"
Alors j'attends '2' secondes
Et je clean la db pour les deux utilisateurs
Alors je me rend sur la recherche
Et je recherche par mots clefs "article test auto"
Et je clique sur l'annonce
Alors j'achète le produit avec la "cb1"
Et je quitte le chat
Et je me rend sur mon profil
Et je vérifie l'état de ma commande "En attente de validation"
Et je me rend sur mon profil
Et je vérifie le montant de mon BA "acheteur"

Scénario: Confirmer la vente et envoyer le lot /vendeur 
Etant donné que je me connecte à l'environnement
Et que j'accepte les cookies
Et que je me connecte avec le "user1"
Alors j'attends '2' secondes
Alors je me rend sur le chat
Et je clique sur la conversation avec "user2"
Et j'envoie le lot et je récupère le montant en BA
Et je vérifie le montant de mon BA en attente "vendeur"

Scénario: Confirmation reception colis /acheteur 
Etant donné que je me connecte à l'environnement
Et que j'accepte les cookies
Et que je me connecte avec le "user2"
Alors je me rend sur le chat
Et je clique sur la conversation avec "user1"
Et je valide la conformité du colis
Et je quitte le chat
Et je me rend sur mon profil
Et je vérifie le montant de mon BA "acheteur"

Scénario: Vérifier que la cagnotte est bien alimentée /vendeur
Etant donné que je me connecte à l'environnement
Et que j'accepte les cookies
Et que je me connecte avec le "user1"
Et je me rend sur mon profil
Et je vérifie que le BA a été émis automatiquement