# language: fr
@samsungS8_Os7_Chromelatest
@iPhone11_Os14_SafariLatest

Fonctionnalité: Créer et négocier un lot, pas plus de 3 offres
Pour créer un lot
En tant qu'acheteur
Je veux pouvoir créer un lot puis négocier son prix avant de l'acheter

Scénario: Créer un lot et envoyer 4 offres
#Connexion
Etant donné que je me connecte à l'environnement
Et que j'accepte les cookies
Et que je prépare une annonce
Et que je me connecte avec le "user2"
Et que je clean la db pour les deux utilisateurs
Quand je recherche par mots clefs "article test auto" sur mobile
Et que je clique sur l'annonce
Alors je démarre une conversation
# Première offre
Et je calcule une offre inférieure au dernier prix
Et j'envoie ma première offre
# Seconde offre
Et je calcule une offre inférieure au dernier prix
Et j'envoie mon offre
# Troisième offre
Et je calcule une offre inférieure au dernier prix
Et j'envoie mon offre
# Quatrième offre
Et je calcule une offre inférieure au dernier prix
Et j'envoie mon offre
Et je vois un message d'erreur "Le nombre d'offre maximal a été atteint"
Alors je quitte le chat et me déconnecte

