# language: fr
@samsungS8_Os7_Chromelatest
@iPhone11_Os14_SafariLatest
@windows10_ChromeLatest

Fonctionnalité: Créer et négocier un lot
Pour créer un lot
En tant qu'acheteur
Je veux pouvoir créer un lot puis négocier son prix avant de l'acheter

Scénario: Créer un lot et faire une offre 
Etant donné que je me connecte à l'environnement
Et que j'accepte les cookies
Et que je prépare une annonce
Et que je me connecte avec le "user2"
Et que je clean la db pour les deux utilisateurs
Quand je recherche par mots clefs "article test auto"
Et que je clique sur l'annonce
Alors je démarre une conversation
Et j'ajoute un produit au lot à partir du chat
Et je calcule une offre inférieure au dernier prix
Et j'envoie ma première offre

Scénario: Faire une contre-offre
Etant donné que je me connecte à l'environnement
Et que j'accepte les cookies
Et que je me connecte avec le "user1"
Alors je me rend sur le chat
Et je clique sur la conversation avec "user2"
Et je calcule une contre-offre supérieure au dernier prix
Et j'envoie ma contre-offre


Scénario: Accepter une contre-offre
Etant donné que je me connecte à l'environnement
Et que j'accepte les cookies
Et que je me connecte avec le "user2"
Alors je me rend sur le chat
Et je clique sur la conversation avec "user1"
Et j'accepte la dernière offre
Et je paye le lot avec la carte "cb1"
Alors la vente est confirmée

Scénario: Confirmer la vente et envoyer le lot (vendeur)
Etant donné que je me connecte à l'environnement
Et que j'accepte les cookies
Et que je me connecte avec le "user1"
Alors je me rend sur le chat
Et je clique sur la conversation avec "user2"
Et j'envoie le lot et je récupère le montant en BA
Et je vérifie le montant de mon BA en attente "vendeur"

Scénario: Confirmation reception colis (acheteur)
Etant donné que je me connecte à l'environnement
Et que j'accepte les cookies
Et que je me connecte avec le "user2"
Alors je me rend sur le chat
Et je clique sur la conversation avec "user1"
Et je valide la conformité du colis
Et je quitte le chat
Et je me rend sur mon profil
Et je vérifie le montant de mon BA "acheteur"