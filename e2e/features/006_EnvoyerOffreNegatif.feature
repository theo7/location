# language: fr
@samsungS8_Os7_Chromelatest
@iPhone11_Os14_SafariLatest
@windows10_ChromeLatest

Fonctionnalité: Envoyer offre < 50% du prix
En tant qu'acheteur
Je ne peux pas envoyer une offre < 50% du prix

Scénario: Créer un lot et faire une offre incorrecte
#Connexion
Etant donné que je me connecte à l'environnement
Et que j'accepte les cookies
Et que je prépare une annonce

Et que je me connecte avec le "user2"
Et que je clean la db pour les deux utilisateurs
Quand je recherche par mots clefs "aticle test auto" sur mobile
Et que je clique sur l'annonce
Alors je démarre une conversation
Et j'ajoute un produit au lot à partir du chat
Et je calcule une offre < 50% du dernier prix
Et j'envoie ma première offre
Et je vois une popup apparaitre
Alors je valide la popup