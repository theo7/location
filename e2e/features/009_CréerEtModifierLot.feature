# language: fr
@samsungS8_Os7_Chromelatest

Fonctionnalité: Créer et modifier un lot
Pour acheter un produit
En tant qu'acheteur et vendeur
Je veux que le BA se génère automatiquement

Scénario: Créer un lot
Etant donné que je me connecte à l'environnement
Et que j'accepte les cookies
Et que je prépare une annonce
Et que je me connecte avec le "user2"
Et que je clean la db pour les deux utilisateurs
Quand je recherche par mots clefs "article test auto"
Et que je clique sur l'annonce
Alors je démarre une conversation
Et j'ajoute un produit au lot à partir du chat
Et je supprime le dernier produit
Et je calcule une offre inférieure au dernier prix
Et j'envoie ma première offre
