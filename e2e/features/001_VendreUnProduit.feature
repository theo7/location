            # language: fr
            @samsungS8_Os7_Chromelatest
            Fonctionnalité: Parcours Produit /vendeur
            Pour vendre un produit
            En tant que vendeur
            Je veux pouvoir vendre un produit sur mobile en précisant toutes ses caractéristiques

            Plan du Scénario: Vendre un produit
            Etant donné que je me connecte à l'environnement
            Et que j'accepte les cookies
            Et que je me connecte avec le "user1"
            Et je me rend sur la création d'annonce
            Et que j'ajoute 1 image
            Et que je renseigne le champ "title" avec "<title>"
            Et que je renseigne le champ "description" avec "<description>"
            Et que dans le champ "category" je choisis "<category>"
            Et que dans le champ "subCategory" je choisis "<subCategory>"
            Et que dans le champ "productType" je choisis "<productType>"
            Et que dans le champ "size" je choisis "<size>"
            Et que dans le champ "color" je choisis "<color>"
            Et que dans le champ "condition" je choisis "<condition>"
            Et que je renseigne le champ "price" avec "<price>"
            Et que je clique sur le champ "brand"
            Et que je remplis la fenetre de recherche avec "<brand>"
            Alors je met l'annonce en ligne
            Et je ne peux pas l'acheter
            Et je me rend sur la page d'accueil
            Alors je vois mon annonce dans le fil des annonces

            Exemples:
            | title             | description                  | category | subCategory        | productType            | size              | color      | brand   | condition                                 | price |
            | Article test auto | Il est très très cool        | Femme    | Pull, sweat, gilet | Gilet                  | 46 / XXL          | Bleu;Jaune | Disney  | Neuf sans étiquette                       | 19.99 |
            | T shirt large     | t shirt trop large           | Homme    | Tshirt, polo       | Tshirt manches courtes | 40 / M            | Kaki;Vert  | Adidas  | Bon état (pas de défaut apparent)         | 15.00 |
            | Moufles           | Belles moufles en vison      | Fille    | Accessoires        | Echarpe, gants, bonnet | 10 ans, 138/143cm | Bleu;Beige | Dolce   | Très bon état (aucun défaut)              | 60.00 |
            | Pantalon lin      | Une belle pantalonnade oui ! | Femme    | Pantalon, legging  | Pantalon               | 50 / 4XL          | Gris;Noir  | Elle    | Neuf avec étiquette                       | 45.50 |
            | Bracelets         | Bracelets jeunesse           | Garçon   | Bijoux             | Autres                 | TU                | Noir;Gris  | Element | État correct (défaut ou usure à préciser) | 10.00 |
            