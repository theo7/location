const env = require('./dev/environment');
// const fullUrlReport =
//     env.reportBaseUrl[env.vars.source]['url'] + process.env.BEGIN_TEST_SUITE + '/' + process.env.DEVICE;
// console.log('url ->>>>>>>>> ' + fullUrlReport);
const urlReport = 'output/' + process.env.BEGIN_TEST_SUITE + '/' + process.env.DEVICE;
var dimensions = '360x800';
var urlLocal = 'http://localhost';
var smartwaitTime = 10000;

var userName = 'manche2';
var accessKey = '7a2i8os5R4FzTottYBXk';
var browserstackURL = 'hub-cloud.browserstack.com';
var BScapabilities = {
    smartwait: smartwaitTime,
    local: false,
    device: 'Samsung Galaxy S8',
    realMobile: 'true',
    os_version: '7.0',
    'browserstack.local': 'false',
    'browserstack.user': 'manche2',
    'browserstack.key': '7a2i8os5R4FzTottYBXk',
    browserName: 'chrome',
    acceptSslCert: true,
    'browserstack.geoLocation': 'FR',
    'browserstack.timezone': 'Paris',
    'browserstack.idleTimeout': 5,
    'browserstack.autoWait': 10,
    'browserstack.console': 'errors',
    'appium:chromeOptions': {
        w3c: false,
    },
};
var capabilities = {
    noReset: 'true',
    browserName: 'chrome',
    'appium:chromeOptions': {
        w3c: false,
    },
};

exports.config = {
    output: urlReport,
    helpers: {
        Appium: {
            user: userName,
            key: accessKey,
            device: 'Samsung Galaxy S8',
            platform: 'Android',
            browser: 'Chrome',
            smartWait: 5000,
            timeouts: {
                script: 5000,
                'page load': 10000,
            },
            desiredCapabilities: capabilities,
        },
        PostgreConnector: {
            require: './helpers/PostgreConnector.js',
        },
        ApiConnector: {
            require: './helpers/ApiConnector.js',
        },
        Mochawesome: {
            uniqueScreenshotNames: 'true',
        },
        EnvManager: {
            require: './helpers/EnvManager.js',
        },
    },
    include: {
        I: './steps_file.js',
        commonPage: './pages/common.js',
        productPage: './pages/product.js',
        profilePage: './pages/profile.js',
        chatPage: './pages/chat.js',
        sellFormPage: './pages/sellForm.js',
        searchFormPage: './pages/searchForm.js',
        homePage: './pages/home.js',
        buyPage: './pages/buy.js',
        loginPage: './pages/login.js',
    },
    mocha: {
        reporter: 'mocha-multi',
        reporterOptions: {
            'codeceptjs-cli-reporter': {
                stdout: '-',
                options: {
                    verbose: true,
                    steps: true,
                },
            },
            mochawesome: {
                stdout: urlReport + '/console.log',
                options: {
                    reportDir: urlReport,
                    reportFilename: 'report',
                },
            },
        },
    },
    bootstrap: null,
    teardown: null,
    hooks: [],
    gherkin: {
        features: './features/*.feature',
        steps: [
            './step_definitions/common.js',
            './step_definitions/home.js',
            './step_definitions/product.js',
            './step_definitions/profile.js',
            './step_definitions/search.js',
            './step_definitions/login.js',
            './step_definitions/sell.js',
            './step_definitions/buy.js',
            './step_definitions/chat.js',
            './step_definitions/login.js',
        ],
    },
    plugins: {
        tryTo: {
            enabled: true,
        },
        customLocator: {
            enabled: true,
            attribute: 'data-qa',
        },
        // allure: {
        //     enabled: true,
        // },
        screenshotOnFail: {
            enabled: true,
        },
        retryFailedStep: {
            enabled: true,
        },
        autoDelay: {
            enabled: true,
        },
        testomat: {
            enabled: true,
            require: '@testomatio/reporter/lib/adapter/codecept',
            apiKey: 'lz7de7e80xbj',
        },
    },
    name: 'SecondHand Local Appium',
};
