import { TargetOptions } from "@angular-builders/custom-webpack";
import * as cheerio from "cheerio";
import * as minifier from "html-minifier";

export default (targetOptions: TargetOptions, indexHtml: string) => {
    const $ = cheerio.load(indexHtml);

    if (targetOptions.configuration?.includes("production")) {
        $('meta[name=robots]').remove();
    }

    return minifier.minify($.html(), {
        removeComments: true,
        removeAttributeQuotes: true,
        collapseWhitespace: true,
        minifyJS: true
    });
};
