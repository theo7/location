import { CustomWebpackBrowserSchema, TargetOptions } from '@angular-builders/custom-webpack';
import * as webpack from 'webpack';
import * as pkg from './package.json';

export default (config: webpack.Configuration, options: CustomWebpackBrowserSchema, targetOptions: TargetOptions) => {
    // define env variables
    config.plugins!.push(
        new webpack.DefinePlugin({
            'process.env': {
                APP_VERSION: JSON.stringify(pkg.version),
                APP_FIREBASE_DATABASE_URL: JSON.stringify(process.env.APP_FIREBASE_DATABASE_URL),
                SECOND_HAND_LOCAL_DOMAIN: JSON.stringify(process.env.SECOND_HAND_LOCAL_DOMAIN)
            }
        })
    );

    if (targetOptions.configuration?.includes("production")) {
        // set min size for chunks (to limit the number of parallel chunk files to download)
        config.plugins!.push(
            new webpack.optimize.MinChunkSizePlugin({
                minChunkSize: 40000
            })
        );
    }

    return config;
}
