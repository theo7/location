import * as fs from 'fs';
import { SitemapStream, streamToPromise } from 'sitemap';
import { Readable } from 'stream';
import { Brand } from '../src/app/shared/models/models';

export const checkEmptySlug = (brands: Brand[]) => {
    if (brands.some(b => !b.slug)) {
        return Promise.reject<Brand[]>('💀 A brand must have a slug! 💀');
    }

    return brands;
};

export const getBrandUrl = (brand: Brand, origin: string) => new URL(`marque/${brand.slug}`, origin).href;

export const getBrandsUrls = (origin: string) => (brands: Brand[]) => brands.map(brand => getBrandUrl(brand, origin));

export const generateSitemap = (urls: string[]) => {
    const links = urls.map(url => ({ url, changefreq: 'daily', priority: 0.6 }));
    return streamToPromise(Readable.from(links).pipe(new SitemapStream())).then(data => data.toString());
};

export const writeSitemap = (sitemap: string) => {
    fs.mkdirSync('dist/location', { recursive: true });
    fs.writeFile('dist/location/sitemap-brands.xml', sitemap, () => {});
};
