import fetch from 'node-fetch';
import { exit } from 'process';
import { environment } from '../src/environments/environment.prod';
import { checkEmptySlug, generateSitemap, getBrandsUrls, writeSitemap } from './generate-sitemap.lib';

console.log('Generate Sitemap3: ⏳');

const originProd = 'https://secondemain.kiabi.com';

fetch(`${environment.apiUrl}/attributes/brands?indexedOnly=false`)
    .then(res => res.json())
    .then(checkEmptySlug)
    .then(getBrandsUrls(originProd))
    .then(generateSitemap)
    .then(writeSitemap)
    .then(() => console.log('Generate Sitemap: ✅'))
    .catch(e => {
        console.log('Generate Sitemap: 🚨');
        console.error(e);
        exit(501);
    });
