import { Brand } from '../src/app/shared/models/models';
import { getBrandUrl } from './generate-sitemap.lib';

describe('sitemap', () => {
    it('getBrandUrl', () => {
        // given
        const brand: Brand = { id: 1, label: 'Zumba!', orderNumber: 123, luxury: false, slug: 'zumba' };
        const origin = 'https://secondemain.kiabi.com';

        // when
        const brandUrl = getBrandUrl(brand, origin);

        // then
        expect('https://secondemain.kiabi.com/marque/zumba').toBe(brandUrl);
    });
});
